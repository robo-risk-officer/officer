package com.roborisk.officer;

import com.roborisk.officer.filters.OAuthProxyFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * The main entry class of the ROBO Risk Officer application.
 */
@EnableZuulProxy
@SpringBootApplication
@EnableScheduling
public class OfficerApplication {

    /**
     * The main entry method of the ROBO Risk Officer.
     *
     * @param args The command line arguments.
     */
    public static void main(String[] args) {
        SpringApplication.run(OfficerApplication.class, args);
        // new command which I now changed
    }

    /**
     * Adds a Zuul filter for OAuth request forwarding.
     */
    @Bean
    public OAuthProxyFilter oauthProxyFilter() {
        return new OAuthProxyFilter();
    }
}
