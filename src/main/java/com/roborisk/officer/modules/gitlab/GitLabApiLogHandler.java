package com.roborisk.officer.modules.gitlab;

import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.ConsoleHandler;
import java.util.logging.LogRecord;

/**
 * Handler for the GitLab API. It logs all messages in a one-liner.
 * The original log contained the entire HTTP Response.
 */
@Slf4j
public class GitLabApiLogHandler extends ConsoleHandler {

    /**
     * Amount of requests sent in this interval.
     */
    private static AtomicInteger COUNTER = new AtomicInteger(0);

    /**
     * Current epoch.
     */
    private static AtomicInteger EPOCH = new AtomicInteger((int) Instant.now().getEpochSecond());

    /**
     * The upper bound for our test.
     */
    private static final int UPPER_BOUND = 30;

    /**
     * Get the prefix and reset the counters if needed.
     *
     * @return  The prefix to use for the log.
     */
    private static String getPrefix() {
        int currentEpoch = (int) Instant.now().getEpochSecond();
        int epoch = GitLabApiLogHandler.EPOCH.get();

        if (currentEpoch != epoch) {
            GitLabApiLogHandler.COUNTER = new AtomicInteger(0);
            GitLabApiLogHandler.EPOCH = new AtomicInteger(currentEpoch);
        }

        GitLabApiLogHandler.COUNTER.incrementAndGet();

        String prefix = "[REQUEST][" + GitLabApiLogHandler.COUNTER + "]";

        if (GitLabApiLogHandler.COUNTER.get() > GitLabApiLogHandler.UPPER_BOUND) {
            prefix += "[ABOVE 30 req/s]";
        }

        return prefix;
    }

    @Override
    public void publish(LogRecord logRecord) {
        String prefix = "[RESPONSE]";

        // If it is a request a counter is included.
        if (!logRecord.getMessage().contains("Received server response on thread main")) {
            prefix = getPrefix();
        }

        String[] parts = logRecord.getMessage().split(System.lineSeparator());

        // Do not adjust if it is a one line log.
        if (parts.length == 1) {
            return;
        }

        // Re-structure the log if this is reached.
        String msg = prefix + ":  " + parts[0] + " " +  parts[1];
        logRecord.setMessage(msg);
    }

    /**
     * Register a customer request.
     *
     * @param url   The URL used for the request.
     */
    public static void registerCustom(String url) {
        String prefix = getPrefix();
        log.info(prefix + " " + url);
    }

    @Override
    public void flush() {
        super.flush();
    }

    @Override
    public void close() throws SecurityException {
        super.close();
    }
}
