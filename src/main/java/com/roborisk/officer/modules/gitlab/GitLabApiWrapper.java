package com.roborisk.officer.modules.gitlab;

import com.roborisk.officer.helpers.KeyFileHelper;
import lombok.extern.slf4j.Slf4j;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.User;
import org.gitlab4j.api.utils.ISO8601;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This wrapper provides an authenticated instance of the GitLab4j API.
 * Spring only creates one instance of this class.
 */
@Component
@Scope("singleton")
@Slf4j
@SuppressWarnings({"ClassFanOutComplexity"})
public class GitLabApiWrapper {

    /**
     * Single instance of GitLabApi to interact via GitLab4j.
     */
    private final GitLabApi gitLabApi;

    /**
     * Base url of the GitLab server.
     */
    private final String gitLabBaseUrl;

    /**
     * RestTemplate to interact directly with the GitLab API.
     */
    private final RestTemplate restTemplate = new RestTemplate();

    /**
     * HttpEntity containing a header with the access token.
     */
    private final HttpEntity<String> entity;

    /**
     * Creates a GitLabApiWrapper object. This object is used to interact with the GitLab API.
     *
     * @param baseUrl                   Url of the GitLab server,
     *                                      given by spring from the properties file.
     * @param keyFileHelper             Object that helps with validating and reading the key file.
     * @param tokenFileName             Name of the file that contains the GitLab token.
     * @throws IOException              If the key file cannot be read.
     * @throws IllegalAccessException   If GitLab initialization or getting the current
     *                                      user goes wrong.
     */
    @Autowired
    public GitLabApiWrapper(@Value("${officer.gitlab.baseurl}") String baseUrl,
                            @Autowired KeyFileHelper keyFileHelper,
                            @Value("${officer.gitlab.token_file:gitlab_token}")
                                        String tokenFileName,
                            @Value("${officer.gitlab.logger.enabled:true}") Boolean enableLog)
            throws IOException, IllegalAccessException {
        // Set the gitLabBaseUrl
        this.gitLabBaseUrl = baseUrl;

        // Get the access token from the local system
        String accessToken;
        try {
            accessToken = keyFileHelper.getKeyLine(tokenFileName);
        } catch (IOException e) {
            throw new IllegalStateException("GitLabApiWrapper.GitLabApiWrapper: "
                    + "Was not able to get gitlab token. Reason: "
                    + e.getMessage());
        }

        // Initialize GitLab4j instance
        this.gitLabApi = new GitLabApi(this.gitLabBaseUrl, accessToken);

        if (enableLog) {
            // Enable the logger.
            Logger logger = Logger.getLogger("GitLabApiLogger");
            // Register handler.
            logger.addHandler(new GitLabApiLogHandler());

            this.gitLabApi.enableRequestResponseLogging(logger, Level.INFO);
        }

        // Initialize HTTP header
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + accessToken);
        this.entity = new HttpEntity<>(headers);

        // Check if the instance has access
        try {
            // Get the list of projects the account has access to
            User currentUser = this.gitLabApi.getUserApi().getCurrentUser();

            // Logging username
            log.info("Logged into the GitLab4j instance as " + currentUser.getName());
        } catch (GitLabApiException e) {
            throw new IllegalAccessException("Failed to establish connection with GitLab server "
                + this.gitLabBaseUrl + ". Reason: " + e.getMessage());
        }
    }

    /**
     * Returns the instance of the GitLabApi.
     *
     * @return GitLabApi object.
     */
    public GitLabApi getGitLabApi() {
        return this.gitLabApi;
    }

    /**
     * Returns the response from GitLab given the full method and returnClass.
     * Automatically authenticates requests to GitLab.
     *
     * @param url           The url, excluding base url, that the request is sent to.
     * @param method        The http method of the request.
     * @param returnClass   The return type class of the request.
     * @return              ResponseEntity containing the data from GitLab.
     */
    public <T> ResponseEntity<T> exchangeRequest(
            String url, HttpMethod method, Class<T> returnClass) {
        String query = String.join("/", this.gitLabBaseUrl, url);

        GitLabApiLogHandler.registerCustom(query);

        return this.restTemplate.exchange(query, method, this.entity, returnClass);
    }

    /**
     * Convert a GitLab date to an epoch.
     *
     * @param date              The date to convert.
     * @throws ParseException   When the date cannot be converted to an Instant.
     * @return                  The integer representing the epoch with second precision.
     */
    public static Integer getEpochFromString(String date) throws ParseException {
        try {
            // This is valid, however we will get some issues when the integer runs out.
            // which won't be during this sep tho ^_^.
            return (int) ISO8601.toInstant(date).getEpochSecond();
        } catch (ParseException e) {
            String error = String.format("GitLabApiWrapper.getEpochFromString: Parse exception "
                    + "occurred when converting date to posix: %s", e.getMessage());

            throw new ParseException(error, e.getErrorOffset());
        }
    }
}
