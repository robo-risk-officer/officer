package com.roborisk.officer.modules.git;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.util.FS;

/**
 * This is a helper class for the {@link GitModule} to create an {@code SshConfigSessionFactory}.
 * The SshConfigSessionFactory configures transport options for performing git operations over SSH.
 */
public class SshConfigSessionFactory extends JschConfigSessionFactory {

    /**
     * Variable to store the path to the private SSH key file to use for authentication.
     */
    private String keyFilePath;

    /**
     * Constructs a SshConfigSessionFactory based on a default {@link JschConfigSessionFactory}.
     *
     * @param keyFilePath The path to the SSH private key file to use for authentication.
     */
    public SshConfigSessionFactory(String keyFilePath) {
        // First, the default JschConfigSessionFactory is called to ensure that none of the default
        // constructor behavious is skipped
        super();

        // Set the key file path for the private SSH key file
        this.keyFilePath = keyFilePath;
    }

    @Override
    protected void configure(OpenSshConfig.Host hc, Session session) {
        // Firstly, host key checking is disabled to ensure user interaction is not required
        session.setConfig("StrictHostKeyChecking", "no");
    }

    @Override
    protected JSch createDefaultJSch(FS fs) throws JSchException {
        // In this method the appropriate key file is set after initialising a default
        // Java Secure Channel
        JSch jsch = super.createDefaultJSch(fs);

        // As only the identity specified in the keyFilePath argument is going to be used,
        // all existing identities are removed.
        jsch.removeAllIdentity();

        // Add the identity
        jsch.addIdentity(this.keyFilePath);

        // Finally, return the Java Secure Channel
        return jsch;
    }
}
