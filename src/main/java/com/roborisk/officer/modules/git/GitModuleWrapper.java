package com.roborisk.officer.modules.git;

import com.roborisk.officer.helpers.KeyFileHelper;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import lombok.extern.slf4j.Slf4j;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.SshKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * This class provides an authenticated instance of the GitModule.
 * Ssh authentication is used using a key file with the correct permissions.
 */
@Component
@Scope("singleton")
@Slf4j
public class GitModuleWrapper {

    /**
     * The location of the private ssh key file on the file system.
     */
    private String privateKeyLocation;

    /**
     * Initializes the GitModuleWrapper. Imports the ssh key from the system and checks if
     * it is valid. Also makes sure the ssh key is ready to be used.
     * Creates a temporary GitModule to test if authentication works.
     *
     * @param keyFileHelper             Helper that takes care of validating and getting key files.
     * @param gitLabApiWrapper          Object that allows for interaction with the GitLab API.
     * @param privateKeyName            The name of the private ssh  key file.
     * @throws IllegalStateException    If the GitModuleWrapper failed to initialize.
     */
    @Autowired
    public GitModuleWrapper(@Autowired KeyFileHelper keyFileHelper,
                            @Autowired GitLabApiWrapper gitLabApiWrapper,
                            @Value("${officer.git.key_file_pri:id_rsa}") String privateKeyName,
                            @Value("${officer.git.key_file_pub:id_rsa.pub}") String publicKeyName)
            throws IllegalStateException {
        this.privateKeyLocation = keyFileHelper.getKeyFilePath(privateKeyName);

        // Get ssh keys
        String privateKey = "";
        String publicKey = "";
        try {
            privateKey = keyFileHelper.getKeyLine(privateKeyName, 1);
            publicKey = keyFileHelper.getKeyLine(publicKeyName);
        } catch (IOException exception) {
            throw new IllegalStateException("GitModuleWrapper.GitModuleWrapper: "
                    + "Was not able to get ssh key. Reason: "
                    + exception.getMessage());
        }

        // Get the rro GitLab users public ssh keys
        List<SshKey> gitlabUserKeys;
        try {
            gitlabUserKeys = gitLabApiWrapper.getGitLabApi().getUserApi().getSshKeys();
        } catch (GitLabApiException e) {
            throw new IllegalStateException("GitModuleWrapper.GitModuleWrapper: "
                    + "Was not able to get the rro gitlab user ssh keys. Reason: "
                    + e.getMessage());
        }

        boolean hasSshKey = false;

        // Check if ssh key file is already added to the gitlab user
        for (SshKey key: gitlabUserKeys) {
            if (key.getKey().equals(publicKey)) {
                hasSshKey = true;
            }
        }

        // Add the ssh key to GitLab user (owning the GitLab token read by the rro) if they
        // do not have this key yet. On production this will the the robo user and a key pair
        // generated for the robo user.
        // Note that only the robo user should have this key pair on its account.
        if (!hasSshKey) {
            try {
                gitLabApiWrapper.getGitLabApi().getUserApi().addSshKey(
                        "rro_ssh_key", publicKey
                );
            } catch (GitLabApiException e) {
                // Crash when we cannot add the ssh key. Note that this keypair can only be used
                // for one account.
                throw new IllegalStateException("GitModuleWrapper.GitModuleWrapper: "
                        + "Was not able to add ssh key to gitlab rro user. Reason: "
                        + e.getMessage());
            }
        }

        log.info("Authentication for Git through SSH succeeded.");
    }

    /**
     * Creates a new GitModule object for the given repository ${@code repositoryUri}
     * and authenticates it with the key file at {@code keyLocation}.
     *
     * @param repositoryUri     The uri of the repository for which to get a GitModule instance.
     * @throws IOException      If the GitModule failed to initialize.
     * @return                  An authenticated GitModule instance for the given repository.
     */
    public GitModule getGitModule(String repositoryUri) throws IOException {
        // Create a new authenticated git module
        GitModule newGitModule = new GitModule(repositoryUri);
        newGitModule.authSsh(this.privateKeyLocation);

        // Get newly created or cached git module
        return newGitModule;
    }
}
