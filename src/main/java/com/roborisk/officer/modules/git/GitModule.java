package com.roborisk.officer.modules.git;

import lombok.Getter;
import org.eclipse.jgit.api.CreateBranchCommand;
import org.eclipse.jgit.api.FetchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 * This class serves as a utility class to perform git operation relevant to the Robo Risk Officer.
 * Instances of this class will be bound to a specific repository specified at object creation.
 * Invariant: {@code this.git != null && this.repository != null <==> GitModule is correctly
 * authenticated and initialised with the repository @this.repository}
 */
public class GitModule implements AutoCloseable {

    /**
     * Variable to store the repository URI.
     */
    private String repositoryUri;

    /**
     * Variable to store the git object.
     */
    @Getter private Git git;

    /**
     * Variable to store the repository belonging to the git object.
     */
    @Getter private Repository repository;

    /**
     * Temporary directory to use as git working directory.
     */
    @Getter private File workingDirectory;

    /**
     * Enumeration type to keep track of the used authentication type.
     */
    private enum AuthType {
        UNAUTH,
        HTTP,
        SSH
    }

    /**
     * Variable to keep track of the current authentication state.
     */
    private AuthType authState;

    /**
     * Variable to store the credentials provider in case of HTTP authentication.
     */
    private UsernamePasswordCredentialsProvider httpAuthProvider;

    /**
     * Constructs a new GitModule for a specific repository.
     *
     * @param repositoryUri             The URI of the repository that will be accessible through
     *                                      {@code this}.
     * @pre                             {@code repositoryURI != null && repositoryURI != ""}
     * @throws NullPointerException     If {@code repositoryURI == null}.
     * @throws IllegalArgumentException If {@code repositoryURI == ""}.
     * @throws IOException              If no working directory can be created on the file system.
     */
    public GitModule(String repositoryUri) throws NullPointerException, IllegalArgumentException,
            IOException {
        // Check pre-condition
        if (repositoryUri == null) {
            throw new NullPointerException("GitModule.GitModule: repo uri cannot be null");
        }
        if (repositoryUri.isEmpty()) {
            throw new IllegalArgumentException("GitModule.GitModule: repo uri cannot be empty");
        }
        // Pre-conditions satisfied

        // Set the repository URI
        this.repositoryUri = repositoryUri;

        // Try to extract the repository name
        // Split the repository uri on "/"
        String[] splitRepositoryUri = this.repositoryUri.split("/");

        // Get the last part, as that contains the reponame.git part
        String repositoryNameExtension = splitRepositoryUri[splitRepositoryUri.length - 1];

        // Remove the extension
        String repositoryName = repositoryNameExtension
                .substring(0, repositoryNameExtension.lastIndexOf('.'));

        // Sanitise the name so it does not include spaces or capitals
        repositoryName = repositoryName.toLowerCase().replace(' ', '_');

        // Based on the repository name, construct a working directory with a time stamp
        String workingDirectory = "RRO_" + repositoryName + "_" + System.currentTimeMillis();
        try {
            GitModuleHelper.cleanWorkingDir(this.workingDirectory);
            this.workingDirectory = Files.createTempDirectory(workingDirectory).toFile();
        } catch (IOException e) {
            // Something went wrong while creating the working directory, so throw an exception
            throw new IOException(
                    "GitModule.GitModule: could not initialise working directory: \n"
                            + e.getMessage());
        }

        // Indicate that currently the GitModule has not been authenticated yet
        this.authState = AuthType.UNAUTH;
    }

    /**
     * Method to clean up temporary resources when the object is no longer of value.
     * Enables objects of GitModule class to be used with try-with-resources idiom.
     *
     * @throws IOException  If {@code this.workingDirectory} cannot be deleted
     * @post                {@code this.workingDirectory == null &&
     *                          \old(this.workingDirectory).exists() == false}
     */
    @Override
    public void close() throws IOException {
        // Remove the temporary working directory
        GitModuleHelper.cleanWorkingDir(this.workingDirectory);
    }

    /**
     * Initialises {@code this.gitRepository} to use HTTP password authentication for the repo.
     *
     * @param username                  The username to use for HTTP password authentication.
     * @param password                  The password to use for HTTP password authentication.
     * @pre                             {@code this.git == null && this.repository == null &&
     *                                      this.repositoryUri.startsWith("http") &&
     *                                      !(username == null || password == null) &&
     *                                      !(username == "" || password == "")
     *                                      && this.authState == AuthType.UNAUTH }.
     * @throws IllegalStateException    If {@code this.git != null || this.repository != null ||
     *                                      !this.repositoryUri.startsWith("http") ||
     *                                      this.authState != AuthType.UNAUTH}.
     *                                      or on unrecoverable failure
     * @throws IllegalArgumentException If {@code username == null || password == null ||
     *                                      username == "" || password == ""}.
     * @return                          {@code true <==> this.git != null &&
     *                                      this.repository != null}.
     * @post                            {@code this.git != null && this.repository != null <==>
     *                                          GitModule is correctly authenticated and initialised
     *                                          with the repository @this.repository}.
     */
    public boolean authHttp(String username, String password) throws
            IllegalStateException, IllegalArgumentException {
        // Check pre-condition
        // Check the repository has not yet been initialised
        if (this.git != null || this.repository != null || this.authState != AuthType.UNAUTH) {
            throw new IllegalStateException("GitModule.authHttp: git has already been initialised");
        }
        // Check the repository URI is of the right type
        if (!this.repositoryUri.startsWith("http")) {
            throw new IllegalStateException(
                "GitModule.authHttp: the repository does not have an HTTP URI");
        }
        // Check the username and password are not empty
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            throw new IllegalArgumentException("GitModule.authHttp: credentials cannot be empty");
        }
        // Pre-conditions satisfied

        // Create a credentials provider for the git repository
        this.httpAuthProvider = new UsernamePasswordCredentialsProvider(username, password);

        // Create and configure the Git object for @this
        try {
            this.git = Git.cloneRepository()                // Start clone command
                .setURI(this.repositoryUri)                 // Set repository uri
                .setCredentialsProvider(this.httpAuthProvider)   // Set credentials
                .setDirectory(this.workingDirectory)        // Set working directory
                .call();                                    // Execute actual clone
        } catch (Exception e) {
            // Something went wrong while cloning the repository
            // Maintain the invariant
            this.git = null;
            // Throw an exception since it is impossible to recover.
            throw new IllegalStateException("GitModule.authHttp: "
                    + "could not perform initial repository clone: \n" + e.getMessage());
        }

        // Authentication has succeeded, so lets set the authentication state
        this.authState = AuthType.HTTP;

        // @this.git should be correctly initialised.
        // Therefore, extract the repository
        return this.extractRepositoryFromGit();
    }

    /**
     * Initialises {@code this.gitRepository} to use SSH key file authentication for the repo.
     *
     * @param keyFilePath               The path of the key file to use for SSH authentication.
     * @pre                             {@code this.git == null && this.repository == null &&
     *                                      this.repositoryUri.startsWith("ssh") &&
     *                                      !(keyFilePath == null) && !(keyFilePath == "") &&
     *                                      File(keyFilePath).exists() &&
     *                                      this.authState == AuthType.UNAUTH}.
     * @throws IllegalStateException    If {@code this.git != null || this.repository != null ||
     *                                      !this.repositoryUri.startsWith("ssh")}
     *                                      or on unrecoverable failure.
     * @throws IllegalArgumentException If {@code keyFilePath == null || keyFilePath == "" ||
     *                                      !File(keyFilePath).exists() ||
     *                                      this.authState != AuthType.UNAUTH}.
     * @return                          {@code true <==> this.git != null &&
     *                                      this.repository != null}.
     * @post                            {@code this.git != null && this.repository != null <==>
     *                                      GitModule is correctly authenticated and initialised
     *                                      with the repository @this.repository}.
     */
    public boolean authSsh(String keyFilePath) throws
            IllegalStateException, IllegalArgumentException {
        // Check pre-conditions
        // Check the repository has not yet been initialised
        if (this.git != null || this.repository != null || this.authState != AuthType.UNAUTH) {
            throw new IllegalStateException("GitModule.authSsh: git has already been initialised");
        }
        // Check the repository URI is of the right type
        if (!GitModuleHelper.isSshUri(this.repositoryUri)) {
            throw new IllegalStateException(
                "GitModule.authSsh: the repository does not have an SSH URI");
        }
        // Check the key file path is not empty
        if (keyFilePath == null || keyFilePath.isEmpty()) {
            throw new IllegalArgumentException("GitModule.authSsh: key file path cannot be empty");
        }

        // Initialise file object for key file path
        File keyFile = new File(keyFilePath);

        // And test it exists
        if (!keyFile.exists()) {
            throw new IllegalArgumentException("GitModule.authSsh: key file has to exist");
        }
        // Pre-conditions satisfied

        // Create an Transport Config Callback for the wanted SSH transport
        // using the SshSessionFactory
        TransportConfigCallback sshTransportCallback =
                new SshTransportConfigCallback(new SshConfigSessionFactory(keyFilePath));

        // Create and configure the Git object for @this
        try {
            this.git = Git.cloneRepository()                        // Start clone command
                .setURI(this.repositoryUri)                         // Set repository uri
                .setTransportConfigCallback(sshTransportCallback)   // Set credentials
                .setDirectory(this.workingDirectory)                // Set working directory
                .call();                                            // Execute actual clone
        } catch (Exception e) {
            // Something went wrong while cloning the repository
            // Maintain the invariant
            this.git = null;
            // Throw an exception as it is impossible to recover.
            throw new IllegalStateException("GitModule.authSsh: "
                + "could not perform initial repository clone: \n" + e.getMessage());
        }

        // Authentication succeeded so lets set the authentication state
        this.authState = AuthType.SSH;

        // @this.git should be correctly initialised.
        // Therefore, extract the repository
        return this.extractRepositoryFromGit();
    }

    /**
     * Extracts repository after git object is initialised in {@code authHttp} or {@code authSsh}.
     * This method should not be called by itself.
     *
     * @pre     Method is called by either {@code authHttp} or {@code authSsh}
     *              and {@code this.git != null}.
     * @return  {@code true <==> this.repository != null &&
     *              false <==> this.repository == null && this.git == null}.
     */
    private boolean extractRepositoryFromGit() {
        this.repository = this.git.getRepository();

        // Check the repository is not null
        if (this.repository != null) {
            // Everything seems to have been initialised correctly, therefore return true
            return true;
        } else {
            // The repository is null, so something must have gone wrong.
            // Take care to maintain the invariant.
            this.git = null;

            // Then, return false to indicate that something went wrong
            return false;
        }
    }

    /**
     * Method to checkout a branch.
     *
     * @param branchName                The name of the branch to checkout
     * @pre                             {@code branchName} should exist on the remote repository and
     *                                      {@code branchName != null} and {@code branchName != ""}
     *                                      and authentication should have been performed.
     * @throws IllegalArgumentException If {@code branchName == null || branchName == ""}.
     * @throws IllegalStateException    If {@code this.authState == AuthType.UNAUTH}
     *                                      or on unrecoverable failure.
     */
    public void checkout(String branchName) throws IllegalArgumentException, IllegalStateException {
        // Check pre conditions
        if (branchName == null || branchName.isEmpty()) {
            throw new IllegalArgumentException("GitModule.checkout: branchName cannot be empty");
        }

        if (this.authState == AuthType.UNAUTH) {
            throw new IllegalStateException("GitModule.checkout: git module not authenticated");
        }
        // Preconditions checked
        // Try to checkout the branch and link it to the remote upstream
        try {
            // Figure out if the branch already exists locally, because if so, it should not be
            // recreated.
            boolean existsLocally = GitModuleHelper.branchExistsLocally(branchName, this.git);
            // Checkout the branch
            this.git.checkout()
                    .setCreateBranch(!existsLocally)
                    .setName(branchName)
                    .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.SET_UPSTREAM)
                    .setStartPoint("origin/" + branchName)
                    .call();
        } catch (Exception e) {
            // Something went wrong during the checkout, so throw an exception as it is impossible
            // to recover
            throw new IllegalStateException("GitModule.checkout: checkout of "
                    + branchName + " failed: \n" + e.getMessage());
        }
    }

    /**
     * Method that can be used to get the diff between two branches or two commits.
     *
     * @param a                         The name of the first branch/the first commit's SHA1 hash.
     * @param b                         The name of the second branch/the second commit's SHA1 hash.
     * @param isBranch                  {@code true <==> compare branches &&
     *                                      false <==> compare commits}.
     * @pre                             {@code a != null && a != "" && b != null && b != ""} and
     *                                      authentication should have been performed.
     * @throws IllegalArgumentException If {@code a != null || a != "" || b != null || b != ""}
     * @throws IllegalStateException    If {@code this.authState == AuthType.UNAUTH}
     *                                      or on unrecoverable failure.
     * @throws IOException              If one of getBranchTreeParser, getComitTreeParser,
     *                                      resolveCommitObjectId or computeDiff throws an
     *                                      IOException when called.
     * @return                          The list of diff entries between {@code a} and {@code b} or
     *                                      {@code null} if the diff fails.
     */
    public List<DiffEntry> diff(String a, String b, boolean isBranch) throws
            IllegalArgumentException, IllegalStateException, IOException {
        // Check pre-conditions
        if (a == null || a.isEmpty() || b == null || b.isEmpty()) {
            throw new IllegalArgumentException(
                "GitModule.diff: it is not allowed to have empty parameters");
        }

        if (this.authState == AuthType.UNAUTH) {
            throw new IllegalStateException("GitModule.diff: git module not authenticated");
        }
        // Preconditions checked
        // Need to create tree parsers for the trees of a and b, which is different depending on
        // whether commits or branches are compared
        CanonicalTreeParser treeParserA;
        CanonicalTreeParser treeParserB;

        // Handle initialisation of treeParsers based on whether commits or branches are compared
        if (isBranch) {
            // Compare two branches
            // Firstly, check out the a branch, will throw exception if branch does not exist
            this.checkout(a);
            // Construct a tree parser for it
            treeParserA = GitModuleHelper.getBranchTreeParser(a, this.repository);

            // Check out the b branch, will throw exception if branch does not exist
            this.checkout(b);
            // And also construct a tree parser for it
            treeParserB = GitModuleHelper.getBranchTreeParser(b, this.repository);
        } else {
            // Compare two commits
            // Convert the commit SHA1 hashes of a and b into ObjectIds
            ObjectId commitA = GitModuleHelper.resolveCommitObjectId(a, this.repository);
            ObjectId commitB = GitModuleHelper.resolveCommitObjectId(b, this.repository);

            // Then, check that they are both not-null, else one of the commits does not exist
            if (commitA == null || commitB == null) {
                // Cannot recover from this, so throw an exception
                throw new IllegalStateException(
                        "GitModule.diff: could not resolve one or more of the commits");
            }

            // Otherwise, build the commits into their CanonicalTreeParsers
            treeParserA = GitModuleHelper.getCommitTreeParser(commitA, this.repository);
            treeParserB = GitModuleHelper.getCommitTreeParser(commitB, this.repository);
        }

        // The tree parsers were correctly initialised, so perform the diff and return its result
        return GitModuleHelper.computeDiff(treeParserA, treeParserB, this.git);
    }

    /**
     * Method to get the changes in a single commit.
     * i.e. a method to get the diff between a commit's parent and itself.
     *
     * @param commitId                  The SHA1 hash of the commit.
     * @pre                             {@code commitId != null && commitId != ""} and
     *                                      authentication should have been performed.
     * @throws IllegalArgumentException If {@code commitId != null || commitId != ""}.
     * @throws IllegalStateException    If {@code this.authState == AuthType.UNAUTH}
     *                                      or on unrecoverable failure.
     * @throws IOException              If one of resolveCommitObjectId, getCommitTreeParser,
     *                                      findParentCommit or computeDiff throws an
     *                                      IOException when called.
     * @return                          The list of diff entries in {@code commitId} or
     *                                      {@code null} if the diff fails.
     */
    public List<DiffEntry> changes(String commitId) throws IllegalArgumentException,
            IllegalStateException, IOException {
        // Firstly, check the pre conditions
        if (commitId == null || commitId.isEmpty()) {
            throw new IllegalArgumentException("GitModule.changes: commitId cannot be empty");
        }

        if (this.authState == AuthType.UNAUTH) {
            throw new IllegalStateException("GitModule.changes: git module not authenticated");
        }
        // Pre conditions checked
        // Resolve the commitId to an ObjectId
        ObjectId commitObjectId = GitModuleHelper.resolveCommitObjectId(commitId, this.repository);

        // And check it is not null
        if (commitObjectId == null) {
            // Something went wrong while resolving the commit, so throw an exception as it is
            // impossible to recover from this
            throw new IllegalStateException("GitModule.changes: could not resolve commit");
        }

        // If not null, prepare a CanonicalTreeParser for the commit
        CanonicalTreeParser commitParser = GitModuleHelper.getCommitTreeParser(commitObjectId,
                this.repository);

        // Locate the parent of the commit
        ObjectId parentCommitObjectId = GitModuleHelper.findParentCommit(commitObjectId,
                this.repository);

        // Then, check the parent is not null, and if not, initialise the CanonicalTreeParser
        CanonicalTreeParser parentCommitParser;

        if (parentCommitObjectId == null) {
            // Apparently, this commit can be resolved, but it does not have a parent
            // Likely this commit is the first commit of the repository
            // Compare the commit against an empty branch so create empty parser
            parentCommitParser = new CanonicalTreeParser();
        } else {
            // Found the parent, so lets prepare the CanonicalTreeParser
            parentCommitParser = GitModuleHelper.getCommitTreeParser(parentCommitObjectId,
                    this.repository);
        }

        // Compute and return the diff
        return GitModuleHelper.computeDiff(parentCommitParser, commitParser, this.git);
    }

    /**
     * Method to execute the git fetch command.
     *
     * @pre                             Authentication should have been performed.
     * @throws IllegalStateException    If {@code this.authState == AuthType.UNAUTH} or on
     *                                      unrecoverable failure.
     */
    public void fetch() throws IllegalStateException {
        this.fetch(null);
    }

    /**
     * Method to execute the git fetch command.
     *
     * @param remote                    Used to specify a remote to fetch.
     * @pre                             Authentication should have been performed.
     * @throws IllegalStateException    If {@code this.authState == AuthType.UNAUTH}
     *                                      or on unrecoverable failure.
     */
    public void fetch(String remote) throws IllegalStateException {
        // First, check the preconditions
        if (this.authState == AuthType.UNAUTH) {
            throw new IllegalStateException("GitModule.fetch: git module not authenticated");
        }
        // Preconditions checked
        // Initialise the fetch command
        FetchCommand fc = this.git.fetch();

        // If the remote is not null
        if (remote != null && !remote.isEmpty()) {
            fc.setRemote(remote);
        }

        // If the authentication type is HTTP, set a credentials provider
        if (this.authState == AuthType.HTTP) {
            fc.setCredentialsProvider(this.httpAuthProvider);
        }

        // Call the fetch command. If it does not throw an exception, assume it is successful
        try {
            fc.call();
        } catch (Exception e) {
            // Otherwise, throw an exception to indicate unrecoverable failure
            throw new IllegalStateException("GitModule.fetch: "
                    + "exception occurred during fetch operation:\n" + e.getMessage());
        }
    }

    /**
     * Method to execute the git pull command.
     *
     * @pre                             Authentication should have been performed.
     * @throws IllegalStateException    If {@code this.authState == AuthType.UNAUTH} or on
     *                                      unrecoverable failure.
     */
    public void pull() throws IllegalStateException {
        this.pull(null);
    }

    /**
     * Method to execute the git pull command.
     *
     * @param branchName                Used to specify the branch to pull. Set to null to use
     *                                      current branch.
     * @pre                             Authentication should have been performed.
     * @throws IllegalStateException    If {@code this.authState == AuthType.UNAUTH} or on
     *                                      unrecoverable failure.
     */
    public void pull(String branchName) throws IllegalStateException {
        this.pull(null, branchName);
    }

    /**
     * Method to execute the git pull command.
     *
     * @param remote                    Used to specify remote. Set to null to use current remote.
     * @param branchName                Used to specify the branch to pull. Set to null to use
     *                                      current branch.
     * @pre                             Authentication should have been performed.
     * @throws IllegalStateException    If {@code this.authState == AuthType.UNAUTH}
     *                                      or on unrecoverable failure.
     */
    public void pull(String remote, String branchName) throws IllegalStateException {
        // First, check the preconditions
        if (this.authState == AuthType.UNAUTH) {
            throw new IllegalStateException("GitModule.pull: git module not authenticated");
        }
        // Preconditions checked
        // Initialise the pull command
        PullCommand pc = this.git.pull();

        // If the authentication type is HTTP, set a credentials provider
        if (this.authState == AuthType.HTTP) {
            pc.setCredentialsProvider(this.httpAuthProvider);
        }

        // If the remote is specified, set it
        if (remote != null && !remote.isEmpty()) {
            pc.setRemote(remote);
        }

        // If the branchName is specified, set it
        if (branchName != null && !branchName.isEmpty())  {
            pc.setRemoteBranchName(branchName);
        }

        // Call the pull command. If it does not throw an exception, assume it is successful
        try {
            pc.call();
        } catch (Exception e) {
            // Otherwise, throw an exception to indicate unrecoverable failure
            throw new IllegalStateException("GitModule.pull:"
                    + " exception occurred during pull operation:\n" + e.getMessage());
        }
    }
}
