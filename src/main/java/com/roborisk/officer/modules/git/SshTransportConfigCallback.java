package com.roborisk.officer.modules.git;

import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.Transport;

/**
 * This is a helper class for the {@link GitModule} to create an {@code SshTransportConfigCallback}.
 * The SshTransportConfigCallback is used to provide an {@link SshConfigSessionFactory} to perform
 * git operations over SSH.
 */
public class SshTransportConfigCallback implements TransportConfigCallback {

    /**
     * Variable to store the {@link SshSessionFactory} to be used to configure transport options.
     */
    private SshSessionFactory sshSessionFactory;

    /**
     * Constructs a SshConfigSessionFactory based on a default {@link JschConfigSessionFactory}.
     *
     * @param sshSessionFactory The sshSessionFactory that should be used to configure transport
     */
    public SshTransportConfigCallback(SshSessionFactory sshSessionFactory) {
        // Set the sshSessionFactory variable for use later on
        this.sshSessionFactory = sshSessionFactory;
    }

    @Override
    public void configure(Transport transport) {
        // Set the SshSessionFactory
        // Firstly, cast the transport to SshTransport, else the setter cannot be used
        SshTransport sshTransport = (SshTransport) transport;

        // And then, set the SshSessionFactory to the one stored during construction
        sshTransport.setSshSessionFactory(this.sshSessionFactory);
    }
}
