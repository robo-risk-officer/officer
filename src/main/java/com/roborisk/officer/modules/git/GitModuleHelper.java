package com.roborisk.officer.modules.git;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * This class provides static helper methods for the {@link GitModule} class.
 */
public class GitModuleHelper {

    /**
     * Method to create a CanonicalTreeParser for the tree of the head of a branch.
     *
     * @param branchName                The name of the branch to create the parser for.
     * @param repo                      The git repository to operate on.
     * @pre                             {@code branchName != null && branchName != "" &&
     *                                      repo != null}.
     * @throws IllegalArgumentException If {@code branchName == null || branchName == ""}.
     * @throws NullPointerException     If {@code repo == null}.
     * @throws IllegalStateException    On unrecoverable failure.
     * @throws IOException              If a JGit operation causes an IOException.
     * @return                          The CanonicalTreeParser for the head of {@code branchName}.
     */
    public static CanonicalTreeParser getBranchTreeParser(String branchName, Repository repo)
            throws IllegalArgumentException, NullPointerException, IllegalStateException,
            IOException {
        // Check the preconditions
        if (branchName == null || branchName.isEmpty()) {
            throw new IllegalArgumentException(
                    "GitModuleHelper.getBranchTreeParser: branchName cannot be empty");
        }

        if (repo == null) {
            throw new NullPointerException(
                    "GitModuleHelper.getBranchTreeParser: repo cannot be null");
        }
        // Pre conditions checked

        // Construct an empty parser
        CanonicalTreeParser treeParser = new CanonicalTreeParser();

        // Look up the ObjectId for the tree of branchName
        try {
            ObjectId treeId = repo.resolve("refs/heads/" + branchName + "^{tree}");

            // Check whether treeId is not null, to ensure the tree actually exists
            if (treeId == null) {
                // The tree does not exist, so throw an exception to indicate unrecoverable failure
                throw new IllegalStateException(
                        "GitModuleHelper.getBranchTreeParser: unable to locate commit tree");
            }

            // The tree should exist, hence try to reset the treeParser to take use information
            try (ObjectReader reader = repo.newObjectReader()) {
                treeParser.reset(reader, treeId);
            }

            // Return the treeParser
            return treeParser;
        } catch (IOException e) {
            // Something went wrong while searching for the reference, so lets forward the exception
            throw new IOException("GitModuleHelper.getBranchTreeParser: resolving reference failed:"
                    + "\n" + e.getMessage());
        }
    }

    /**
     * Method to create a CanonicalTreeParser for the tree of a commit.
     *
     * @param commitObjectId            The ObjectId of the commit to create a parser for.
     * @param repo                      The repository to operate on.
     * @pre                             {@code commitObjectId != null && repo != null}.
     * @throws NullPointerException     If {@code commitObjectId != null || repo != null}.
     * @throws IllegalStateException    On unrecoverable failure.
     * @throws IOException              If a JGit operation throws an IOException.
     * @return                          The CanonicalTreeParser for the tree of the commit.
     */
    public static CanonicalTreeParser getCommitTreeParser(
            ObjectId commitObjectId, Repository repo) throws
            NullPointerException, IllegalStateException, IOException {
        // Check pre conditions
        if (commitObjectId == null) {
            throw new NullPointerException(
                    "GitModuleHelper.getCommitTreeParser: commitObjectId cannot be null");
        }
        if (repo == null) {
            throw new NullPointerException(
                "GitModuleHelper.getCommitTreeParser: repo cannot be null");
        }
        // Pre conditions checked
        try {
            // Try to retrieve the retrieve the tree for the commit
            try (RevWalk walk = new RevWalk(repo)) {
                RevCommit commit = walk.parseCommit(commitObjectId);
                ObjectId treeId = commit.getTree().getId();

                // The tree should exist, so create an empty CanonicalTree
                CanonicalTreeParser treeParser = new CanonicalTreeParser();

                // Try to reset the treeParser to take use information
                try (ObjectReader reader = repo.newObjectReader()) {
                    treeParser.reset(reader, treeId);
                }

                // Return the treeParser
                return treeParser;
            } catch (NullPointerException e) {
                // Null pointer exception occurred while resolving the tree. As there is no way
                // to recover, throw exception.
                throw new IllegalStateException(
                        "GitModuleHelper.getCommitTreeParser: unable to locate commit tree");
            }

        } catch (IOException e) {
            // Resolving the commitId failed, so lets forward the exception
            throw new IOException("GitModuleHelper.getCommitTreeParser: "
                    + "exception while resolving commitId:\n" + e.getMessage());
        }
    }

    /**
     * Method to locate the parent of a commit.
     *
     * @param commitObjectId        The ObjectId of the commit to find the parent for.
     * @param repo                  The git repository to operate on.
     * @pre                         {@code commitObjectId != null}.
     * @throws NullPointerException If {@code commitObjectId == null || repo == null}.
     * @throws IOException          If a JGit operation throws an IOException.
     * @return                      The ObjectId of the parent of the commit or {@code null}
     *                                  if that cannot be found.
     */
    public static ObjectId findParentCommit(ObjectId commitObjectId, Repository repo)
            throws NullPointerException, IOException {
        // Check pre conditions
        if (commitObjectId == null) {
            throw new NullPointerException(
                "GitModuleHelper.findParentCommit: commitObjectId cannot be null");
        }

        if (repo == null) {
            throw new NullPointerException(
                "GitModuleHelper.findParentCommit: repo cannot be null");
        }
        // Pre conditions checked
        // Try to find the parent of the commit
        try {
            // Create RevWalk and RevCommit objects for ability to access the commit information
            RevWalk revWalk = new RevWalk(repo);
            RevCommit commit = revWalk.parseCommit(commitObjectId);

            // Check if the commit has a parent
            if (commit.getParentCount() >= 1) {
                // Commit has at least one parent, so lets return the first parent
                return commit.getParent(0);
            } else {
                // Commit does not have a parent so return null
                return null;
            }

        } catch (IOException e) {
            // Something went wrong while locating the parent so forward the exception
            throw new IOException("GitModuleHelper.findParentCommit: "
                    + "exception occurred while locating parent:\n" + e.getMessage());
        }
    }

    /**
     * Method to resolve a commit's SHA1 into its JGit ObjectId.
     *
     * @param commitId                  The SHA1 hash of the commit.
     * @param repo                      The git repository to operate on.
     * @pre                             {@code commitId != null && commitId != "" && repo != null}.
     * @throws IllegalArgumentException If {@code commitId == null || commitId == ""}.
     * @throws NullPointerException     If {@code repo == null}.
     * @throws IOException              If a JGit operation results in an IOException.
     * @return                          The ObjectId of {@code commitId} or {@code null}
     *                                      if the {@code commitId} cannot be resolved.
     */
    public static ObjectId resolveCommitObjectId(String commitId, Repository repo) throws
            IllegalArgumentException, NullPointerException, IOException {
        // Check preconditions
        if (commitId == null || commitId.isEmpty()) {
            throw new IllegalArgumentException(
                    "GitModuleHelper.resolveCommitObjectId: commitId cannot be empty");
        }

        if (repo == null) {
            throw new NullPointerException(
                    "GitModuleHelper.resolveCommitObjectId: repo cannot be null");
        }
        // Preconditions checked
        // Try to resolve the commitId to its ObjectId in JGit and return it
        try {
            return repo.resolve(commitId);
        } catch (IOException e) {
            // Something went wrong while resolving the objectId, so forward the exception
            throw new IOException("GitModuleHelper.resolveCommitObjectId: "
                    + "exception while resolving commitId:\n" + e.getMessage());
        }
    }

    /**
     * Method to compute the diff based on two CanonicalTreeParsers.
     *
     * @param parserA                   The CanonicalTreeParser of the source branch.
     * @param parserB                   The CanonicalTreeParser of the target branch.
     * @param git                       The git object to operate on.
     * @pre                             {@code parserA != null && parserB != null && git != null}.
     * @throws NullPointerException     If {@code parserA == null ||
     *                                      parserB == null || git == null}.
     * @throws IllegalStateException    If a JGit operation throws a GitAPIException.
     * @return                          The list of diff entries between {@code parserA} and
     *                                      {@code parserB} or {@code null} if the diff fails.
     */
    public static List<DiffEntry> computeDiff(CanonicalTreeParser parserA,
            CanonicalTreeParser parserB, Git git) throws NullPointerException {
        // Check pre condition
        if (parserA == null || parserB == null || git == null) {
            throw new NullPointerException("GitModuleHelper.computeDiff: arguments cannot be null");
        }
        // Pre condition checked
        try {
            return git.diff()
                .setOldTree(parserA)
                .setNewTree(parserB)
                .call();
        } catch (GitAPIException e) {
            // Something went wrong while computing the diff, so forward the exception
            throw new IllegalStateException("GitModuleHelper.computeDiff: "
                    + "exception while computing the diff: \n" + e.getMessage());
        }
    }

    /**
     * Method to check if a branch exists locally in a git repository.
     *
     * @param branchName                The name of the branch to check for its existence.
     * @param git                       The git object to operate on.
     * @pre                             {@code branchName != null && branchName != "" &&
     *                                      git != null}.
     * @throws IllegalArgumentException If {@code branchName == null || branchName == ""}.
     * @throws NullPointerException     If {@code git == null}.
     * @throws GitAPIException          If lookup for the existence of the branch results in
     *                                      this exception.
     * @return                          {@code true <==> branchName exists in repository}.
     */
    public static boolean branchExistsLocally(String branchName, Git git) throws
            IllegalArgumentException, NullPointerException, GitAPIException {
        // Check preconditions
        if (branchName == null || branchName.isEmpty()) {
            throw new IllegalArgumentException(
                    "GitModuleHelper.branchExistsLocally: branchName cannot be empty");
        }

        if (git == null) {
            throw new NullPointerException(
                    "GitModuleHelper.branchExistsLocally: git cannot be null");
        }
        // Preconditions checked
        return git.branchList()
                    .call()
                    .stream()
                    .map(Ref::getName)
                    .collect(Collectors.toList())
                    .contains("refs/heads/" + branchName);
    }

    /**
     * Method to clean up the temporary working directory.
     *
     * @param workingDirectory  The temporary working directory.
     * @throws IOException      If IOException occurs while cleaning up the temporary directory.
     */
    public static void cleanWorkingDir(File workingDirectory) throws IOException {
        if (workingDirectory != null && workingDirectory.exists()) {
            if (!FileSystemUtils.deleteRecursively(workingDirectory)) {
                throw new IOException(
                        "GitModuleHelper.cleanWorkingDir: could not remove working directory");
            }
        }
    }

    /**
     * Method to check if a URI is an SSH git URI.
     *
     * @param   uri The URI to check.
     * @return  {@code true <==> uri is an SSH git URI}.
     */
    public static boolean isSshUri(String uri) {
        Pattern p = Pattern.compile("^"
                                    + "(.*?)@"
                                    + "(.*?):"
                                    + "(?:(.*?)/)?"
                                    + "(.*?/.*?)"
                                    + "$");
        Matcher m = p.matcher(uri);
        return m.matches();
    }
}
