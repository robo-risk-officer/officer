package com.roborisk.officer.modules.metrics.sonar;

import com.fasterxml.jackson.databind.JsonNode;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.SonarAnalysisTracker;
import com.roborisk.officer.models.repositories.SonarAnalysisTrackerRepository;
import com.roborisk.officer.modules.git.GitModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

/**
 * This singleton helper class contains helper methods for handling sonar project related
 * operations.
 */
@Component
@Scope("singleton")
public class SonarProjectHelper {

    /**
     * The base url of the Robo Risk Officer.
     */
    @Value("${officer.sonar.officer_baseurl}")
    private String roboBaseUrl;

    /**
     * Secret Key to ensure secure communication with Sonar.
     */
    @Value("${officer.sonar.webhook_key}")
    private String webhookKey;

    /**
     * Helper class to interact with the SonarQube web server.
     */
    @Autowired
    private SonarWebHelper sonarWebHelper;

    /**
     * Repository to interact with {@link SonarAnalysisTracker} database objects.
     */
    @Autowired
    private SonarAnalysisTrackerRepository sonarAnalysisTrackerRepository;

    /**
     * Creates a helper object used to initialize SonarQube projects and set them up corrently
     * so they can be analyzed.
     */
    @Autowired
    public SonarProjectHelper() {
    }

    /**
     * Creates a SonarQube project with the given projectName. The projectName should be unique
     * to all projects on the SonarQube server.
     *
     * @param projectName               Name of the SonarQube project to be made.
     * @throws RestClientException      When project was not created.
     * @return                          JsonNode with the response from SonarQube upon project
     *                                  creation.
     */
    public JsonNode createSonarQubeProject(String projectName) throws RestClientException {
        // Get api url to create project
        String projectCreateApiUrl = "api/projects/create";
        String createProjectUrl = projectCreateApiUrl + "?name="
                + projectName + "&project=" + projectName + "&visibility=private";

        JsonNode response;
        response = this.sonarWebHelper.exchangeRequest(
                createProjectUrl, HttpMethod.POST, JsonNode.class).getBody();

        return response;
    }

    /**
     * Deletes a SonarQube project with the given projectName. The projectName should be unique
     * to all projects on the SonarQube server.
     *
     * @param projectName           Name of the SonarQube project to be deleted.
     * @throws RestClientException  When project was not deleted.
     * @return                      JsonNode with the response from SonarQube upon project deletion.
     */
    public JsonNode deleteSonarQubeProject(String projectName) throws RestClientException {
        // Get api url to create project
        String projectCreateApiUrl = "api/projects/delete";
        String createProjectUrl = projectCreateApiUrl + "?project=" + projectName;

        JsonNode response;
        response = this.sonarWebHelper.exchangeRequest(
                createProjectUrl, HttpMethod.POST, JsonNode.class).getBody();

        return response;
    }

    /**
     * Adds a webhook to the SonarQube project with name projectName. This webhook will contain
     * a secret to make sure that we only process webhooks that actually come from
     * SonarQube.
     *
     * @param projectName           Name of the SonarQube project.
     * @throws RestClientException  When webhook was not added.
     * @return                      JsonNode with the response from the SonarQube server upon
     *                              webhook addition.
     */
    public JsonNode setupWebhook(String projectName) throws RestClientException {
        // Add webhook to projects
        String addWebhookUrl = "api/webhooks/create";
        String projectWebhookUrl = addWebhookUrl
                + "?name=" + projectName + "_webhook&project="
                + projectName + "&secret=" + this.webhookKey
                + "&url=" + this.roboBaseUrl + "/webhook/sonar";

        return this.sonarWebHelper.exchangeRequest(
                projectWebhookUrl, HttpMethod.POST, JsonNode.class).getBody();
    }

    /**
     * This initialises the tracker objects which track the SonarQube projects.
     *
     * @param module                {@link GitModule} containing the project source code.
     * @param mergeRequestReview    The {@link MergeRequestReview} which this SonarQube analysis
     *                              is a part of.
     * @param isSourceBranch        Whether the SonarQube project is of to a
     *                              source or target branch.
     */
    public void initialiseTracker(GitModule module, MergeRequestReview mergeRequestReview,
                                  boolean isSourceBranch, String name) {
        // Initialise tracker
        SonarAnalysisTracker tracker = new SonarAnalysisTracker();

        // Set all attributes of the tracker
        tracker.setName(name);
        tracker.setSourceBranch(isSourceBranch);
        tracker.setMergeRequestReview(mergeRequestReview);
        tracker.setIsCompleted(false);

        // Save tracker in database
        this.sonarAnalysisTrackerRepository.save(tracker);
    }
}
