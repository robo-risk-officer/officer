package com.roborisk.officer.modules.metrics.codechurn;

import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.StaticJavaParser;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Abstract class for implementations for the code churn metric for specific artefacts in files.
 * Extended by {@link ClassCodeChurnComputer} and {@link FunctionCodeChurnComputer}.
 */
@Slf4j
abstract class AbstractArtefactCodeChurnComputer extends AbstractCodeChurnComputer {

    /**
     * Repository interface for {@link BranchFile} objects.
     */
    @Autowired
    BranchFileRepository branchFileRepository;

    /**
     * Autowired singleton instance of the {@link ArtefactCodeChurnHelper} helper class.
     */
    @Autowired
    ArtefactCodeChurnHelper artefactCodeChurnHelper;

    /**
     * Autowired singleton instance of the {@link CodeChurnOverMergeHelper} helper class.
     */
    @Autowired
    CodeChurnOverMergeHelper codeChurnOverMergeHelper;

    @Override
    List<String> getSupportedFileTypes() {
        return new ArrayList<>(List.of(".java"));
    }

    /**
     * Constructor that sets the language level.
     */
    public AbstractArtefactCodeChurnComputer() {
        StaticJavaParser.getConfiguration()
                .setLanguageLevel(ParserConfiguration.LanguageLevel.JAVA_14);
    }

    /**
     * Function for analyzing for all artefacts of a given type in a single file whether adaptations
     * were made.
     *
     * @param fileOld       Contents of the file before the commit.
     * @param fileNew       Contents of the file after the commit.
     * @param branchFile    {@link BranchFile} representing the file.
     * @param commit        {@link GitCommit} representing the analyzed commit.
     * @param metric        {@link Metric} concerning the Class Code Churn Metric.
     * @param settings      {@link MetricSettings} for the Class Code Churn Metric.
     */
    abstract void analyzeFileInCommit(String fileOld, String fileNew, BranchFile branchFile,
            GitCommit commit, Metric metric, MetricSettings settings);

    @Override
    void handleSingleAddition(BranchFile addition, Branch branch, GitCommit commit,
            Metric metric, MetricSettings settings) {
        Optional<BranchFile> optional = this.branchFileRepository.findByBranchIdAndFilePath(
                branch.getId(),
                addition.getFilePath()
        );

        // Nothing can be done with a non-existing file that is added.
        // Should not occur, as FileChurn should have been run first.
        if (optional.isEmpty()) {
            return;
        }

        BranchFile file = optional.get();

        // Check whether the file is of a type that should be processed.
        if (!this.shouldBeProcessed(file.getFileName())) {
            return;
        }

        String fileAsString = this.artefactCodeChurnHelper.getFileAsString(file);

        try {
            this.analyzeFileInCommit("", fileAsString, file, commit, metric, settings);
        } catch (Exception e) {
            log.info("AbstractArtefactCodeChurnComputer.handleSingleAddition: Ignored "
                    + file.getFilePath() + " during analysis because it could not be parsed.");
        }
    }

    @Override
    void handleDelete(List<Pair<GitCommit, List<BranchFile>>> deletions, Branch branch,
            Metric metric, MetricSettings settings) {
    }

    @Override
    void handleSingleModification(BranchFile file, Branch branch, GitCommit commit,
            Metric metric, MetricSettings settings) {
        // Check whether the file is of a type that should be processed.
        if (!this.shouldBeProcessed(file.getFileName())) {
            return;
        }

        // Retrieve list of hashes of parent commits.
        List<String> parentsOfCommit = this.codeChurnOverMergeHelper
                .getHashesOfParentCommits(commit, branch.getRepository().getGitLabId());

        String previousCommitHash;

        switch (parentsOfCommit.size()) {
            case 0:
                // Root commit, should not be handled.
                return;
            case 1:
                // Regular commit, should be handled normally.
                previousCommitHash = parentsOfCommit.get(0);
                break;
            default:
                // Merge commit, first parent on same branch should be determined.
                previousCommitHash = this.codeChurnOverMergeHelper
                    .getLatestParentCommitOnBranch(commit, branch).getHash();
        }

        String fileAsString = this.artefactCodeChurnHelper.getFileAsString(file);
        String oldFileAsString = this.artefactCodeChurnHelper
                .getPreviousVersionFileAsString(file, previousCommitHash);

        try {
            this.analyzeFileInCommit(oldFileAsString, fileAsString, file, commit, metric,
                    settings);
        } catch (Exception e) {
            log.info("AbstractArtefactCodeChurnComputer.handleSingleModification: Ignored "
                    + file.getFilePath() + " during analysis because it could not be parsed.");
        }
    }
}
