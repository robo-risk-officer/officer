package com.roborisk.officer.modules.metrics.sonar;

import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.SonarAnalysisTracker;
import com.roborisk.officer.models.repositories.SonarAnalysisTrackerRepository;
import com.roborisk.officer.models.web.SonarIssueWeb;
import com.roborisk.officer.models.web.SonarUnresolvedIssuesWeb;
import com.roborisk.officer.modules.git.GitModule;
import com.roborisk.officer.modules.git.GitModuleWrapper;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import com.roborisk.officer.modules.metrics.sonar.commands.SonarCommand;
import com.roborisk.officer.modules.metrics.sonar.commands.SonarJavaCommand;
import com.roborisk.officer.modules.metrics.sonar.commands.SonarJavascriptCommand;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This wrapper provides an authenticated connection to the instance of SonarQube
 * specified in the application properties.
 */
@Component
@Scope("singleton")
@Slf4j
@SuppressWarnings({"ClassFanOutComplexity"})
public class SonarApiWrapper {

    /**
     * The {@link GitLabApiWrapper} to communicate with GitLab.
     */
    @Autowired
    private GitLabApiWrapper gitLabApiWrapper;

    /**
     * The {@link GitModuleWrapper} to get authenticated instances of {@link GitModule} for
     * cloning.
     */
    @Autowired
    private GitModuleWrapper gitModuleWrapper;

    /**
     * Helper class to initialise SonarQube projects.
     */
    @Autowired
    private SonarProjectHelper sonarProjectHelper;

    /**
     * Helper class to interact with the SonarQube web server.
     */
    @Autowired
    private SonarWebHelper sonarWebHelper;

    /**
     * Repository to interact with {@link SonarAnalysisTracker} objects in the database.
     */
    @Autowired
    private SonarAnalysisTrackerRepository sonarAnalysisTrackerRepository;

    /**
     * HashMap to keep track of the Git projects currently being analysed
     * and their associated {@link GitModule} objects.
     */
    private HashMap<String, GitModule> openGitModules = new HashMap<>();

    private final List<String> supportedLanguages = List.of("Java", "JavaScript");

    /**
     * Returns a list of unresolved issues of a project based on the parameters given.
     * The parameters map is not allowed to contain the key "p", this entry will be ignored.
     *
     * @param parameters                HashMap containing all the parameters of the issues
     *                                  that are wanted. Documentation of what parameters
     *                                  are possible can be found at /web_api/api/issues
     *                                  under api/issues/search.
     *                                  https://demo.sourcemeter.com/web_api/api/issues
     * @throws IllegalStateException    When the function fails to get all issues.
     * @return                          A list of JsonNode objects containing all unresolved issues.
     */
    public List<SonarIssueWeb> getUnresolvedIssues(HashMap<String, String> parameters) {
        // Format parameters into url.
        // Initialize StringBuilder.
        StringBuilder url = new StringBuilder("/api/issues/search?");

        // Add parameters to url of request.
        for (Map.Entry<String, String> pair : parameters.entrySet()) {
            if (!pair.getKey().equals("p")) {
                url.append(pair.getKey()).append("=").append(pair.getValue()).append("&");
            }
        }

        // Initialize page number and page size.
        Pair<Integer, Integer> pageInfo = this.getPageInfo(url.toString());
        int pageSize = pageInfo.getLeft();
        int numPages = pageInfo.getRight();

        // Return list of issues.
        return this.getPagedIssues(url.toString(), pageSize, numPages);
    }

    /**
     * Creates and returns a list of unresolved issues. These unresolved issues are issues
     * that SonarQube found in its analysis of a project. Automatically deals with paging response.
     *
     * @param baseUrl       Url to make the query request to.
     * @param pageSize      Number of issues per page gotten in the request.
     * @param numPages      The number of pages to expect.
     * @return              A list of all unresolved issues.
     */
    private List<SonarIssueWeb> getPagedIssues(String baseUrl,
                                               int pageSize,
                                               int numPages) {
        // Initialise list to return
        List<SonarIssueWeb> unresolvedIssues = new ArrayList<>();

        // Get all pages.
        int page = 1;
        while ((page - 1) < numPages) {
            // Format URL
            String pageUrl = baseUrl + "p=" + page++;

            // Get Page
            SonarUnresolvedIssuesWeb node = this.sonarWebHelper.exchangeRequest(pageUrl,
                    HttpMethod.GET, SonarUnresolvedIssuesWeb.class).getBody();

            if (node == null) {
                throw new IllegalStateException("SonarApiWrapper.getPagedIssues: "
                        + "Failed to get a page. Could not get issues.");
            }

            // Add issues to list
            unresolvedIssues.addAll(node.getIssues());
        }

        // return list
        return unresolvedIssues;
    }

    /**
     * Gets information about the page size and number of pages that SonarQube returns
     * when we query for issues.
     *
     * @param requestUrl    Url to make the request to.
     * @return              A pair containing the pageSize and the number of pages.
     */
    private Pair<Integer, Integer> getPageInfo(String requestUrl) {
        String pageUrl = requestUrl + "p=1";
        // Make initial request to get the number of pages.
        SonarUnresolvedIssuesWeb initialNode = this.sonarWebHelper.exchangeRequest(
                pageUrl, HttpMethod.GET, SonarUnresolvedIssuesWeb.class).getBody();

        if (initialNode == null) {
            throw new IllegalStateException("SonarApiWrapper.getNumberOfPages: "
                    + "Failed to get the number of pages. Could not get issues.");
        }

        int pageSize = initialNode.getPageSize();
        int numPages = initialNode.getTotalPages();

        return new ImmutablePair<>(pageSize, numPages);
    }


    /**
     * Starts the SonarQube analysis of the source branch of a merge request review.
     *
     * @param mergeRequestReview    The {@link MergeRequestReview}.
     * @throws GitLabApiException   If the languages of the repository cannot be determined.
     * @throws IOException          If the {@link GitModule} throws an error or if the
     *                              {@link ProcessBuilder} throws this exception.
     */
    public void initialiseProjectForAnalysis(MergeRequestReview mergeRequestReview)
            throws GitLabApiException, IOException {
        MergeRequest mergeRequest = mergeRequestReview.getMergeRequest();

        // Clone source branch using GitModule
        GitModule sourceModule = this.gitModuleWrapper.getGitModule(
                mergeRequest.getSourceBranch().getRepository().getSshUrl());

        sourceModule.checkout(mergeRequest.getSourceBranch().getName());

        // Get language of project
        Map<String, Float> languages = this.gitLabApiWrapper.getGitLabApi()
                .getProjectApi().getProjectLanguages(mergeRequest.getSourceBranch()
                        .getRepository().getGitLabId());

        if (Collections.disjoint(this.supportedLanguages, languages.keySet())) {
            throw new IllegalStateException("SonarApiWrapper.initialiseProjectForAnalysis: "
                    + "Unsupported programming language.");
        }

        // Get the names of the SonarQube project
        String sourceProjectName = "RRO_source_" + java.util.UUID.randomUUID();

        // Initialize SonarQube project
        try {
            this.sonarProjectHelper.createSonarQubeProject(sourceProjectName);
        } catch (Exception e) {
            throw new IllegalStateException("SonarApiWrapper.initialiseProjectForAnalysis: "
                    + "Failed to create SonarQube project. Reason: " + e.getMessage());
        }

        // Setup webhook for the project
        try {
            this.sonarProjectHelper.setupWebhook(sourceProjectName);
        } catch (Exception e) {
            this.deleteSonarProject(sourceProjectName);
            sourceModule.close();

            throw new IllegalStateException("SonarApiWrapper.initialiseProjectForAnalysis: "
                    + "Failed to add webhooks to SonarQube project. Reason: "
                    + e.getMessage());
        }


        // Prepare command object to create command strings
        SonarCommand command = null;
        try {
            // Create different command objects per language
            if (languages.containsKey("Java")) {
                command = new SonarJavaCommand(this.sonarWebHelper, sourceModule);
            } else if (languages.containsKey("JavaScript")) {
                command = new SonarJavascriptCommand(this.sonarWebHelper);
            } else {
                throw new IllegalStateException();
            }
        } catch (Exception e) {
            // Cleanup of what was created so far
            this.deleteSonarProject(sourceProjectName);
            sourceModule.close();

            throw new IllegalStateException("SonarApiWrapper.initialiseProjectForAnalysis: "
                    + "Failed to create commands for analysing the project.");
        }

        // Initialise tracker in database
        this.sonarProjectHelper.initialiseTracker(sourceModule, mergeRequestReview,
                true, sourceProjectName);

        // Save module to not let them go out of scope
        this.openGitModules.put(sourceProjectName, sourceModule);

        // Run command as a separate process
        String commandForSource = command.getCommand(sourceProjectName);
        ProcessBuilder sourceProcess = new ProcessBuilder("bash", "-c", commandForSource);
        sourceProcess.directory(sourceModule.getWorkingDirectory())
                .start();
    }

    /**
     * Closes the {@link GitModule} objects and trackers related to a given
     * {@link MergeRequestReview}.
     *
     * @param mergeRequestReview    The given {@link MergeRequestReview}.
     */
    public void closeModulesAndTrackers(MergeRequestReview mergeRequestReview) {
        // Log problem with empty mergeRequestReview.
        List<SonarAnalysisTracker> trackers = this.sonarAnalysisTrackerRepository
                .findAllByMergeRequestReviewId(mergeRequestReview.getId());
        if (trackers.isEmpty()) {
            log.warn("Function closeModulesAndTrackers got a mergeRequestReview that"
                    + " has not trackers related to it. Nothing will be deleted.");
        }

        for (SonarAnalysisTracker tracker : trackers) {
            // Log problem with removing uncompleted tracker.
            if (!tracker.getIsCompleted()) {
                log.warn("Function closeModulesAndTrackers got a tracker that is not yet"
                        + " completed. It is not guaranteed that it will not be recreated after"
                        + " deletion. The name of the tracker is " + tracker.getName() + ".");
            }

            // Delete the SonarQube project related to this tracker.
            this.deleteSonarProject(tracker.getName());

            // Try to remove the temporary working directory for the GitModule.
            try {
                this.openGitModules.get(tracker.getName()).close();
            } catch (Exception e) {
                log.error("Function closeModulesAndTrackers failed to remove temporary "
                        + "working directories. \nReason: " + e.getMessage());
            }

            // Remove the module from the list.
            this.openGitModules.remove(tracker.getName());

            // Delete database object for the tracker.
            this.sonarAnalysisTrackerRepository.delete(tracker);
        }
    }

    /**
     * Deletes a SonarQube project from SonarQube server.
     *
     * @param projectName The name of the project.
     */
    private void deleteSonarProject(String projectName) {
        try {
            this.sonarProjectHelper.deleteSonarQubeProject(projectName);
        } catch (Exception e) {
            log.error("Function deleteSonarProject failed to delete the SonarQube "
                    + "project with name " + projectName + ".\n Reason: " + e.getMessage());
        }
    }

}
