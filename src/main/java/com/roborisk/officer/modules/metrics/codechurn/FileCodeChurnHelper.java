package com.roborisk.officer.modules.metrics.codechurn;

import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.GitCommitWeb;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Implements the file churn metrics helper.
 * The helper assists in getting modified, added and removed files from CommitWebhookData.
 */
@Component
public class FileCodeChurnHelper {

    /**
     * Repository to handle {@link GitCommit} models.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Returns an ordered list of files, as BranchFile objects, that are changed in the push event.
     *
     * @param data  Parsed webhook push event.
     * @return      Map of commit hashes linked to {@link BranchFile} that are changed in the push
     *              event.
     */
    public List<Pair<GitCommit, List<BranchFile>>> getModifiedFiles(CommitWebhookData data) {
        List<Pair<GitCommit, List<BranchFile>>> files = new ArrayList<>();

        for (GitCommitWeb commit : data.getCommits()) {
            List<BranchFile> modifiedFiles = new ArrayList<>();

            for (String modifiedFilePath : commit.getModified()) {
                modifiedFiles.add(BranchFile.fromString(modifiedFilePath));
            }

            files.add(new ImmutablePair<>(this.getCommit(commit.getHash()), modifiedFiles));
        }

        Collections.sort(files, (Comparator.comparing(p -> p.getLeft().getTimestamp())));
        return files;
    }

    /**
     * Returns an ordered list of files, as BranchFile objects, that are new in the push event.
     *
     * @param data  Parsed webhook push event.
     * @return      Map of commit hashes linked to {@link BranchFile} that are added in the push
     *              event.
     */
    public List<Pair<GitCommit, List<BranchFile>>> getAddedFiles(CommitWebhookData data) {
        List<Pair<GitCommit, List<BranchFile>>> files = new ArrayList<>();

        for (GitCommitWeb commit : data.getCommits()) {
            List<BranchFile> addedFiles = new ArrayList<>();

            for (String addedFilePath : commit.getAdded()) {
                addedFiles.add(BranchFile.fromString(addedFilePath));
            }

            files.add(new ImmutablePair<>(this.getCommit(commit.getHash()), addedFiles));
        }

        Collections.sort(files, (Comparator.comparing(p -> p.getLeft().getTimestamp())));
        return files;
    }

    /**
     * Returns an ordered list of files, as BranchFile objects, that are deleted in the push event.
     *
     * @param data  Parsed webhook push event.
     * @return      Map of commit hashes linked to {@link BranchFile} that
     *              are removed in the push event.
     */
    public List<Pair<GitCommit, List<BranchFile>>> getRemovedFiles(CommitWebhookData data) {
        List<Pair<GitCommit, List<BranchFile>>> files = new ArrayList<>();

        for (GitCommitWeb commit : data.getCommits()) {
            List<BranchFile> removedFiles = new ArrayList<>();

            for (String removedFilePath : commit.getRemoved()) {
                removedFiles.add(BranchFile.fromString(removedFilePath));
            }

            files.add(new ImmutablePair<>(this.getCommit(commit.getHash()), removedFiles));
        }

        Collections.sort(files, (Comparator.comparing(p -> p.getLeft().getTimestamp())));
        return files;
    }

    /**
     * Helper function to query the {@link GitCommitRepository} for a commit, based on the hash.
     *
     * @param  hash                     The hash of the commit.
     * @throws IllegalStateException    If no commit based on the {@code hash} could be found.
     * @return                          {@link GitCommit} with hash {@code hash}
     */
    private GitCommit getCommit(String hash) throws IllegalStateException {
        return this.gitCommitRepository.findByHash(hash).orElseThrow(() -> {
            String msg = "BranchFileChangeEvent: Cannot find commit with hash " + hash;

            throw new IllegalStateException(msg);
        });
    }
}
