package com.roborisk.officer.modules.metrics.codechurn;

import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.GitCommitWeb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Controller providing a single access point for the {@link FileCodeChurnComputer},
 * {@link ClassCodeChurnComputer} and {@link FunctionCodeChurnComputer} implementations.
 */
@Component
public class CodeChurnComputerController {

    /**
     * Link to the {@link FileCodeChurnComputer} class.
     */
    @Autowired
    private FileCodeChurnComputer fileCodeChurnComputer;

    /**
     * Link to the {@link ClassCodeChurnComputer} class.
     */
    @Autowired
    private ClassCodeChurnComputer classCodeChurnComputer;

    /**
     * Link to the {@link FunctionCodeChurnComputer} class.
     */
    @Autowired
    private FunctionCodeChurnComputer functionCodeChurnComputer;

    /**
     * Repository for {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Autowired constructor to make Spring scan it and inject other autowires.
     */
    @Autowired
    public CodeChurnComputerController() {
    }

    /**
     * Method for handling the contents of a {@link CommitWebhookData} in regards to the various
     * code churn computers.
     *
     * @param branch            {@link Branch} on which the data is changed.
     * @param data              {@link CommitWebhookData} containing all data regarding the
     *                          modifications.
     * @param fileMetric        {@link Metric} for the file code churn.
     * @param fileSettings      {@link MetricSettings} for the file code churn metric.
     * @param classMetric       {@link Metric} for the class code churn.
     * @param classSettings     {@link MetricSettings} for the class code churn metric.
     * @param functionMetric    {@link Metric} for the function code churn.
     * @param functionSettings  {@link MetricSettings} for the function code churn metric.
     */
    public void handle(Branch branch, CommitWebhookData data,
                       Metric fileMetric, MetricSettings fileSettings,
                       Metric classMetric, MetricSettings classSettings,
                       Metric functionMetric, MetricSettings functionSettings) {
        // Handle the code churn metrics.
        this.fileCodeChurnComputer.handle(
                fileSettings,
                fileMetric,
                branch,
                data
        );
        this.classCodeChurnComputer.handle(
                classSettings,
                classMetric,
                branch,
                data
        );
        this.functionCodeChurnComputer.handle(
                functionSettings,
                functionMetric,
                branch,
                data
        );

        // Loop over all commits in the CommitWebhookData.
        for (GitCommitWeb commitWeb : data.getCommits()) {
            // Retrieve commit by hash.
            String hash = commitWeb.getHash();
            Optional<GitCommit> optionalCommit = this.gitCommitRepository.findByHash(hash);

            if (optionalCommit.isEmpty()) {
                continue;
            }

            GitCommit commit = optionalCommit.get();

            // Update processed status.
            commit.setFinishedProcessing(true);

            this.gitCommitRepository.save(commit);
        }
    }
}
