package com.roborisk.officer.modules.metrics.codechurn;

import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.CommitRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Class which provides methods for correctly computing and handling the data for the code churn
 * metric over merges. Adheres to the Singleton design pattern.
 */
@Component
@Scope("singleton")
public class CodeChurnOverMergeHelper {

    /**
     * Repository interface for {@link Branch} objects.
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Repository interface for {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * Wrapper for the GitLab API.
     */
    @Autowired
    GitLabApiWrapper gitLabApiWrapper;

    /**
     * Empty autowired constructor to make Spring realize this exists.
     */
    @Autowired
    private CodeChurnOverMergeHelper() {}

    /**
     * Method for retrieving the hashes for the parent commits of a provided {@link GitCommit}.
     *
     * @param   commit                  The {@link GitCommit} to retrieve the parent commits' hashes
     *                                  for.
     * @param   repositoryId            The GitLab ID of the {@link Repository} containing the
     *                                  commits.
     * @throws  IllegalStateException   If the commit cannot be retrieved from GitLab.
     * @return                          A {@link List} of hashes of parent commits as
     *                                  {@link String}s.
     */
    public List<String> getHashesOfParentCommits(GitCommit commit, int repositoryId)
            throws IllegalStateException {
        try {
            return this.gitLabApiWrapper.getGitLabApi().getCommitsApi().getCommit(
                repositoryId,
                commit.getHash()).getParentIds();
        } catch (GitLabApiException e) {
            throw new IllegalStateException("Exception in retrieving commit from GitLab: "
                + e.toString());
        }
    }

    /**
     * Method for retrieving the {@link GitCommit} objects for the parent commits of a provided
     * {@link GitCommit} if they are present in the database.
     *
     * @param   commit                  The {@link GitCommit} to retrieve the parents for.
     * @param   repositoryId            The GitLab ID of the {@link Repository} containing the
     *                                  commit.
     * @return                          {@link List} of {@link GitCommit} objects of the found
     *                                  parents.
     */
    public List<GitCommit> getParentCommits(GitCommit commit, int repositoryId) {
        // Retrieve hashes of parent commits from GitLab.
        List<String> parentIds = this.getHashesOfParentCommits(commit, repositoryId);

        // Retrieve parent commits from the database if possible.
        List<GitCommit> parentCommits = new ArrayList<>();
        for (String hash : parentIds) {
            Optional<GitCommit> commitOptional = this.gitCommitRepository.findByHash(hash);

            if (commitOptional.isPresent()) {
                parentCommits.add(commitOptional.get());
            }
        }

        // Return the found parent commits.
        return parentCommits;
    }

    /**
     * Method for finding all {@link Branch} objects which contain the commit.
     *
     * @param   commit                  The {@link GitCommit} for which the branches are requested.
     * @param   repository              The {@link Repository} containing the commit and the
     *                                  branches.
     * @throws  IllegalStateException   If the commit references cannot be retrieved from GitLab.
     * @return                          {@link List} of found {@link Branch} objects.
     */
    public List<Branch> getBranches(GitCommit commit, Repository repository)
            throws IllegalStateException {
        // Retrieve CommitRefs objects associated to the commit from GitLab.
        List<CommitRef> commitRefs;
        try {
            commitRefs = this.gitLabApiWrapper.getGitLabApi().getCommitsApi().getCommitRefs(
                    repository.getGitLabId(), commit.getHash(), CommitRef.RefType.BRANCH);
        } catch (GitLabApiException e) {
            String msg = "CodeChurnOverMergeHelper.getBranches: Failed to retrieve"
                    + " commit references from GitLab - " + e.toString();

            throw new IllegalStateException(msg);
        }

        // Translate the list of CommitRef objects to a list of Branch objects in our database.
        List<Branch> branches = new ArrayList<>();
        for (CommitRef commitRef : commitRefs) {
            Optional<Branch> branch = this.branchRepository.findByNameAndRepositoryId(
                    commitRef.getName(), repository.getId());

            // Add the branch if it is present.
            if (branch.isPresent()) {
                branches.add(branch.get());
            }
        }

        return branches;
    }

    /**
     * Method for retrieving the most recent parent {@link GitCommit} of a merge {@link GitCommit}
     * on the {@link Branch} which was merged into.
     *
     * @param   commit                  The {@link GitCommit} for which the most recent parent
     *                                  commit is to be retrieved.
     * @param   branch                  The target {@link Branch} of the merge commit.
     * @throws  IllegalStateException   If no suitable commit can be found.
     * @return                          The most recent parent {@link GitCommit} on the
     *                                  {@link Branch}.
     */
    public GitCommit getLatestParentCommitOnBranch(GitCommit commit,
                                                   Branch branch) throws IllegalStateException {
        // Retrieve parent commits.
        List<GitCommit> parentCommits = this.getParentCommits(commit,
                branch.getRepository().getGitLabId());

        // Variable eventually storing the most recent parent commit on the target branch.
        GitCommit mergeParent = new GitCommit();
        mergeParent.setTimestamp(0);

        // Identify all parent commits which are present on the target branch.
        for (GitCommit parentCommit : parentCommits) {
            List<Branch> parentBranches = this.getBranches(parentCommit, branch.getRepository());

            // Check whether the parent commit is present on the target branch.
            for (Branch parentBranch : parentBranches) {
                if (parentBranch.getId().equals(branch.getId())) {
                    // Check whether the parent commit is more recent than all the previously found
                    // parent commits present on the target branch.
                    if (parentCommit.getTimestamp() > mergeParent.getTimestamp()) {
                        mergeParent = parentCommit;
                    }
                    break;
                }
            }
        }

        // Throw an error if no potential parent commit was found present on the target branch.
        if (mergeParent.getTimestamp().equals(0)) {
            String msg = "CodeChurnOverMergeHelper.getLatestParentCommitOnBranch: commit not found";

            throw new IllegalStateException(msg);
        }

        // Return most recent candidate.
        return mergeParent;
    }
}
