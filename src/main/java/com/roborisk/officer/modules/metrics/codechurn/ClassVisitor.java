package com.roborisk.officer.modules.metrics.codechurn;

import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import org.apache.commons.codec.digest.MurmurHash3;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;

/**
 * Class for visiting all classes and interfaces in a given file.
 */
public class ClassVisitor
        extends VoidVisitorAdapter<Map<String, Pair<Long, Integer>>> {

    /**
     * Method for storing the class names with hashes of associated class bodies and the line
     * numbers at which the classes start of all class in a scope in a record.
     * Should initially be called on the CompilationUnit of the selected file.
     *
     * @param classDeclaration  node in the abstract syntax tree to analyze.
     * @param record            record to store class names and pair of hashed bodies and
     *                          line numbers in.
     */
    @Override
    public void visit(ClassOrInterfaceDeclaration classDeclaration,
                      Map<String, Pair<Long, Integer>> record) {
        // Recursively visit entire subtree of classDeclaration.
        super.visit(classDeclaration, record);

        // Retrieve name of the class, preferably the fully qualified name if available.
        String className = classDeclaration.getFullyQualifiedName()
                .orElse(classDeclaration.getNameAsString());
        // Compute hash of the entire class.
        long hash = MurmurHash3.hash128(classDeclaration.toString().getBytes())[0];
        // Retrieve the line number where the class starts.
        if (classDeclaration.getBegin().isEmpty()) {
            String msg = "metrics.churn.ClassVisitor: Begin of class not found";

            throw new IllegalStateException(msg);
        }
        int lineNumber = classDeclaration.getBegin().get().line;

        // Put the new tuple in the record.
        record.put(className, new ImmutablePair<>(hash, lineNumber));
    }
}
