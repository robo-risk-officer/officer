package com.roborisk.officer.modules.metrics.sonar;

import com.roborisk.officer.helpers.KeyFileHelper;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Base64;

/**
 * This class can be used to interact with the SonarQube server.
 * It takes care of authentication automatically.
 */
@Component
@Scope("singleton")
public class SonarWebHelper {

    /**
     * {@link RestTemplate} to interact directly with the GitLab API.
     */
    private final RestTemplate restTemplate = new RestTemplate();

    /**
     * Base url of the SonarQube server.
     */
    @Getter
    private String sonarBaseUrl;

    /**
     * SonarQube authentication token.
     */
    @Getter
    private final String sonarAccessToken;

    /**
     * HttpEntity containing a header with the SonarQube access token.
     */
    private final HttpEntity<String> entity;

    /**
     * Creates a SonarWebHelper object. Sets the accessToken and creates an authentication header.
     *
     * @param keyFileHelper     Helper used to read keyFiles.
     * @param tokenFileName     Name of the file containing the SonarQube access token.
     * @param sonarBaseUrl      Url of the SonarQube server,
     *                          given by spring from the properties file.
     * @param sslEnabled        Whether we communicate with SonarQube over https or http.
     */
    public SonarWebHelper(@Autowired KeyFileHelper keyFileHelper,
                          @Value("${officer.sonar.token_file:sonar_token}") String tokenFileName,
                          @Value("${officer.sonar.baseurl}") String sonarBaseUrl,
                          @Value("${officer.sonar.ssl_enabled}") Boolean sslEnabled,
                          @Value("${officer.sonar.enabled:true}") Boolean sonarEnabled) {
        // Allow RRO to startup when SonarQube is disabled (and no token is present)
        if (!sonarEnabled) {
            this.sonarAccessToken = null;
            this.entity = null;

            return;
        }

        // Get the token from the local system
        try {
            this.sonarAccessToken = keyFileHelper.getKeyLine(tokenFileName);
        } catch (IOException exception) {
            throw new IllegalStateException("SonarWebHelper.SonarWebHelper: "
                    + "Was not able to get token. Reason: "
                    + exception.getMessage());
        }

        // Base64 encode the token in the form of "<token>:"
        String usernamePassword = this.sonarAccessToken + ":";
        String base64Token = Base64.getEncoder().encodeToString(usernamePassword.getBytes());

        // Initialize HTTP header
        HttpHeaders header = new HttpHeaders();
        // Set required authentication header
        header.set("Authorization", "Basic " + base64Token);
        this.entity = new HttpEntity<>(header);

        // Set the sonarBaseUrl based on the sslEnabled setting
        if (sslEnabled) {
            this.sonarBaseUrl = "https://" + sonarBaseUrl;
        } else {
            this.sonarBaseUrl = "http://" + sonarBaseUrl;
        }
    }

    /**
     * Returns the response from SonarQube given the full method and returnClass.
     * Automatically authenticates requests to SonarQube.
     *
     * @param url           The url, excluding base url, that the request is sent to.
     * @param method        The http method of the request.
     * @param returnClass   The return type class of the request.
     * @return              ResponseEntity containing the data from SonarQube.
     */
    public <T> ResponseEntity<T> exchangeRequest(
            String url, HttpMethod method, Class<T> returnClass) {
        String query = String.join("/", this.sonarBaseUrl, url);

        return this.restTemplate.exchange(query, method, this.entity, returnClass);
    }
}
