package com.roborisk.officer.modules.metrics.codechurn;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.visitor.VoidVisitor;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.BranchFunctionObservation;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.metrics.BranchFunctionMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.BranchFunctionMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchFunctionObservationRepository;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Implementation for computing the Code Churn of function artefacts upon a new commit to a branch.
 */
@Component
@SuppressWarnings({"ClassFanOutComplexity"})
// Fanout is 16 instead of 15.
public class FunctionCodeChurnComputer extends AbstractArtefactCodeChurnComputer {

    /**
     * Repository interface for {@link BranchFunctionObservation} objects.
     */
    @Autowired
    BranchFunctionObservationRepository branchFunctionObservationRepository;

    /**
     * Repository interface for {@link BranchFunctionMetricResult} objects.
     */
    @Autowired
    BranchFunctionMetricResultRepository branchFunctionMetricResultRepository;

    /**
     * Autowired constructor to make Spring scan it and inject other autowires.
     */
    @Autowired
    public FunctionCodeChurnComputer() {
    }

    /**
     * Method which handles storing the code churn for a single function artefact in the database.
     *
     * @param functionIdentifier    Identifier of the altered function.
     * @param className             Name of the class containing the function.
     * @param lineNumber            Line number at which the function starts.
     * @param modified              Whether the artefact was changed in the commit.
     * @param branchFile            {@link BranchFile} representing the file containing the
     *                              artefact.
     * @param commit                {@link GitCommit} representing the analyzed commit.
     * @param metric                {@link Metric} concerning the Function Code Churn Metric.
     * @param settings              {@link MetricSettings} for the Function Code Churn Metric.
     */
    private void handleFunctionObservation(String functionIdentifier, String className,
                                           int lineNumber, boolean modified, BranchFile branchFile,
                                           GitCommit commit, Metric metric,
                                           MetricSettings settings) {
        // Fetch the previous BranchFunctionObservation if it exists.
        Optional<BranchFunctionObservation> optionalObservation
                = this.branchFunctionObservationRepository
                .findFirstByBranchFileIdAndFunctionNameOrderByIdDesc(
                        branchFile.getId(),
                        functionIdentifier
                );

        // Stores the number of times the class has been changed.
        int churnCount = 0;

        // Check whether a previous BranchFunctionObservation was found.
        if (optionalObservation.isPresent()) {
            BranchFunctionObservation previousObservation = optionalObservation.get();
            // Fetch the previous BranchFunctionMetricResult if it exists.
            Optional<BranchFunctionMetricResult> optionalResult =
                    this.branchFunctionMetricResultRepository
                            .findFirstByBranchFunctionObservationIdAndMetricIdOrderByIdDesc(
                                        previousObservation.getId(), metric.getId()
                            );

            // Check whether the previous BranchFunctionMetricResult was found.
            if (optionalResult.isPresent()) {
                BranchFunctionMetricResult previousResult = optionalResult.get();

                // Set churnCount to previous result.
                churnCount = previousResult.getScore().intValue();
            }
        }

        // Update churnCount if the artefact was altered.
        if (modified) {
            churnCount++;
        }

        // Construct new BranchFunctionObservation object.
        BranchFunctionObservation newObservation = new BranchFunctionObservation();
        newObservation.setFunctionName(functionIdentifier);
        newObservation.setClassName(className);
        newObservation.setLineNumber(lineNumber);
        newObservation.setBranchFile(branchFile);
        newObservation.setGitCommit(commit);

        // Save new BranchFunctionObservation in the database.
        this.branchFunctionObservationRepository.save(newObservation);

        // Construct new BranchFunctionMetricResult object.
        BranchFunctionMetricResult newResult = new BranchFunctionMetricResult();
        newResult.setBranchFunctionObservation(newObservation);
        newResult.setMetric(metric);
        newResult.setScore((double) churnCount);
        newResult.setIsLowQuality(settings.isLowQuality((double) churnCount));

        // Save new BranchFunctionMetricResult in the database.
        this.branchFunctionMetricResultRepository.save(newResult);
    }

    /**
     * Function for analyzing for all functions in a single file whether adaptations were made.
     *
     * @param fileOld           Contents of the file before the commit.
     * @param fileNew           Contents of the file after the commit.
     * @param branchFile        {@link BranchFile} representing the file.
     * @param commit            {@link GitCommit} representing the analyzed commit.
     * @param metric            {@link Metric} concerning the Function Code Churn Metric.
     * @param settings          {@link MetricSettings} for the Function Code Churn Metric.
     */
    @Override
    public void analyzeFileInCommit(String fileOld, String fileNew, BranchFile branchFile,
            GitCommit commit, Metric metric, MetricSettings settings) {
        // Parse contents of the old version of the file to create the abstract syntax tree.
        CompilationUnit compilationUnitOld = StaticJavaParser.parse(fileOld);

        // Create new HashMap to store the results of traversing the abstract syntax tree.
        Map<String, Triple<Long, Integer, String>> recordOld = new HashMap<>();
        // Instantiate the FunctionVisitor object for traversing the abstract syntax tree.
        VoidVisitor<Map<String, Triple<Long, Integer, String>>> functionVisitor
                = new FunctionVisitor();
        // Visit all functions in the old file and store function identifiers and hashes of
        // corresponding entire functions in the record.
        functionVisitor.visit(compilationUnitOld, recordOld);

        // Parse contents of the new version of the file to create the abstract syntax tree.
        CompilationUnit compilationUnitNew = StaticJavaParser.parse(fileNew);

        // Create HashMap to store the results of traversing the new abstract syntax tree.
        Map<String, Triple<Long, Integer, String>> recordNew = new HashMap<>();
        // Visit all functions in the new file and store function identifiers, hashes and line
        // numbers of corresponding entire functions in the record.
        functionVisitor.visit(compilationUnitNew, recordNew);

        // Loop over all functions found in the new version of the file.
        for (String functionIdentifier : recordNew.keySet()) {
            boolean modified;
            // Check whether the function has been newly introduced or has changed in comparison to
            // the previous version.
            if (!recordOld.containsKey(functionIdentifier)) {
                // Function has been newly introduced.
                modified = true;
            } else {
                // Check whether function has changed in comparison to the previous version.
                modified = !recordOld.get(functionIdentifier).getLeft()
                    .equals(recordNew.get(functionIdentifier).getLeft());
            }

            // Handle the individual observation.
            this.handleFunctionObservation(functionIdentifier,
                    recordNew.get(functionIdentifier).getRight(),
                    recordNew.get(functionIdentifier).getMiddle(),
                    modified, branchFile, commit, metric, settings);
        }
    }
}
