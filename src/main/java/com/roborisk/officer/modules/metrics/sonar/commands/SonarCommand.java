package com.roborisk.officer.modules.metrics.sonar.commands;

import com.roborisk.officer.modules.metrics.sonar.SonarWebHelper;

/**
 * This is an abstract class which is used to create command line commands to run
 * a SonarQube analysis. Subclasses are specified for each supported programming language.
 */
public abstract class SonarCommand {

    /**
     * Helper class used to communicate with SonarQube server.
     */
    protected SonarWebHelper sonarWebHelper;

    public SonarCommand(SonarWebHelper sonarWebHelper) {
        this.sonarWebHelper = sonarWebHelper;
    }

    /**
     * Creates a string holding a commandline argument which starts a sonar analysis for a
     * project. The subclass defines which language this project has.
     *
     * @param projectName   SonarQube name of the project to analyse.
     * @return              A string containing the command.
     */
    public abstract String getCommand(String projectName);

}
