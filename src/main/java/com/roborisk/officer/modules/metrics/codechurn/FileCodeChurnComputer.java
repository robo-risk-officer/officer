package com.roborisk.officer.modules.metrics.codechurn;

import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.metrics.BranchFileMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.BranchFileMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import com.roborisk.officer.models.repositories.MetricRepository;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * FileCodeChurnComputer stores all the information for the metric that might be needed
 * in a GitLab merge request. This part of the metric will only store the information needed to
 * be able to place the comment. This metric is there only used for Push event,
 * rather than Merge Request change events or comment events.
 */
@Component
public class FileCodeChurnComputer extends AbstractCodeChurnComputer {

    /**
     * Database repository to handle {@link BranchFile} objects.
     */
    @Autowired
    private BranchFileRepository branchFileRepository;

    /**
     * Database repository to handle {@link BranchFileMetricResult} objects.
     */
    @Autowired
    private BranchFileMetricResultRepository branchFileMetricResultRepository;

    /**
     * Database repository to handle {@link Metric} objects.
     */
    @Autowired
    private MetricRepository metricRepository;

    @Override
    List<String> getSupportedFileTypes() {
        return new ArrayList<>(List.of(".java", ".js"));
    }

    /**
     * Autowired constructor to make Spring scan it and inject other autowires.
     */
    @Autowired
    public FileCodeChurnComputer() {
    }

    /**
     * Fetches/creates a {@link BranchFileMetricResult} linked to a
     * {@link Metric}, a {@link GitCommit} and a {@link BranchFile}. If it exists an identical one
     * will be created with an adjusted last change commit and score. The score will be increased by
     * one to match the adjustment. If it is a new item, the score will be set to 1.
     *
     * @param file      The {@link BranchFile} to link to the {@link BranchFileMetricResult}.
     * @param metric    The {@link Metric} to link to the {@link BranchFileMetricResult}.
     * @param commit    The {@link GitCommit} to link to the {@link BranchFileMetricResult}.
     * @param settings  The metric settings to use for the {@link BranchFileMetricResult}.
     */
    private void storeFileChurnMetricResult(BranchFile file, Metric metric, GitCommit commit,
                                                              MetricSettings settings) {

        // Fetch the previous file metric or create a new one.
        BranchFileMetricResult result = this.branchFileMetricResultRepository
                .findFirstByBranchFileIdAndMetricIdOrderByIdDesc(
                        file.getId(),
                        metric.getId()
                ).orElseGet(() -> {
                    BranchFileMetricResult newResult = new BranchFileMetricResult();
                    newResult.setBranchFile(file);
                    newResult.setMetric(metric);
                    newResult.setLastChangeCommit(commit);
                    newResult.setScore(0D);

                    return newResult;
                });

        // Check if this is the first commit for a branch
        if (result.getLastChangeCommit().equals(commit)) {
            // Since the result already exists, this must be the first commit on a new branch
            // with the current state of the result being the one copied over from master
            // so the result status simply should be updated.
            result.setScore(result.getScore() + 1);
            result.setIsLowQuality(settings.isLowQuality(result.getScore()));
            this.branchFileMetricResultRepository.save(result);
        } else {
            // This is not the first commit, hence we should create a new metric result for the
            // new commit
            BranchFileMetricResult copy = new BranchFileMetricResult(result);

            copy.setScore(result.getScore() + 1);
            copy.setLastChangeCommit(commit);
            copy.setIsLowQuality(settings.isLowQuality(copy.getScore()));

            this.branchFileMetricResultRepository.save(copy);
        }
    }

    /**
     * {@inheritDoc}
     * @throws  IllegalStateException if the File Churn Metric cannot be found in the database.
     * @throws  IllegalStateException if the added file cannot be found in the database.
     */
    @Override
    void handleSingleAddition(BranchFile addition, Branch branch, GitCommit commit, Metric metric,
                              MetricSettings settings) {
        // Check whether file is of a type that should be processed.
        if (!this.shouldBeProcessed(addition.getFileName())) {
            return;
        }

        Metric fileChurnMetric = this.metricRepository.findByName(metric.getName())
                .orElseThrow(() -> {
                    String msg = "FileCodeChurnComputer.handleSingleAddition: Metric not found";

                    throw new IllegalStateException(msg);
                });

        int metricId = fileChurnMetric.getId();

        // If the file already exists, then we ignore it.
        // This can happen when a merge commit is made.
        boolean exists = this.branchFileMetricResultRepository
                .existsByBranchFileIdAndMetricIdAndLastChangeCommitId(
                        addition.getId(),
                        metricId,
                        commit.getId()
        );

        if (exists) {
            return;
        }

        BranchFile file = this.branchFileRepository.findByBranchIdAndFilePath(
                branch.getId(),
                addition.getFilePath()
        ).orElseThrow(() -> {
            String msg = "FileCodeChurnComputer.handleSingleAddition: File not found";

            throw new IllegalStateException(msg);
        });

        // Adds the metric result.
        this.storeFileChurnMetricResult(file, metric, commit, settings);
    }

    /**
     * Transform a list of {@link BranchFile} to a {@link BranchFileMetricResult}
     * based on {@link Branch} and {@link Metric}. The deletions are grouped by commit hash.
     *
     * @param   deletions The files that got added in the commit, grouped on commit hash.
     * @param   branch    The branch the additions are part of.
     * @param   metric    The metric that was executed.
     * @param   settings  The metric settings to use for the {@link BranchFileMetricResult}.
     * @throws  IllegalStateException if a file that was deleted cannot be found in the database.
     */
    @Override
    void handleDelete(List<Pair<GitCommit, List<BranchFile>>> deletions,
                              Branch branch, Metric metric,
                              MetricSettings settings) {

        // Loop over the commits.
        deletions.forEach(deletionRecord -> {
            // Loop over the files within the commit.
            for (BranchFile deletion : deletionRecord.getRight()) {
                // Check whether the file is of a type that should be processed.
                if (!this.shouldBeProcessed(deletion.getFileName())) {
                    continue;
                }

                // Retrieve existing branch file.
                BranchFile file = this.branchFileRepository.findByBranchIdAndFilePath(
                        branch.getId(),
                        deletion.getFilePath()
                ).orElseThrow(() -> {
                    String msg = "FileCodeChurnComputer.handleDelete: File not found";

                    throw new IllegalStateException(msg);
                });

                // Stores the metric result.
                this.storeFileChurnMetricResult(file, metric, deletionRecord.getLeft(), settings);
            }
        });
    }

    @Override
    void handleSingleModification(BranchFile file, Branch branch, GitCommit commit,
                                  Metric metric, MetricSettings settings) {
        // Check whether the file is of a type that should be processed.
        if (!this.shouldBeProcessed(file.getFileName())) {
            return;
        }

        this.storeFileChurnMetricResult(file, metric, commit, settings);
    }
}
