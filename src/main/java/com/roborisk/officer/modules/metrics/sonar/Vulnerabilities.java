package com.roborisk.officer.modules.metrics.sonar;

import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.SonarAnalysisTracker;
import com.roborisk.officer.models.database.metrics.MergeRequestFileMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.MergeRequestFileMetricResultRepository;
import com.roborisk.officer.models.web.SonarIssueWeb;
import lombok.extern.slf4j.Slf4j;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * This metric gets all the issues from the SonarQube source branch analysis labeled by SonarQube
 * as 'Vulnerability' and comments them nicely formatted on the {@link MergeRequest}.
 */
@Slf4j
@Component
public class Vulnerabilities {

    /**
     * Wrapper that is used to get information about the SonarQube projects.
     */
    @Autowired
    private SonarApiWrapper sonarApiWrapper;

    /**
     * Database repository to handle {@link MergeRequestFileMetricResult} objects.
     */
    @Autowired
    private MergeRequestFileMetricResultRepository mergeRequestFileMetricResultRepository;

    /**
     * This function places comments on a {@link MergeRequest} on a per file basis based on the
     * vulnerabilities analysis by SonarQube.
     *
     * @param settings              The {@link MetricSettings} for this {@link Metric}.
     * @param metric                The {@link Metric} for which we are posting this review.
     * @param sourceBranchAnalysis  The {@link SonarAnalysisTracker} that contains information
     *                              about the source branch of the analysed merge request.
     * @throws GitLabApiException   When no comment can be placed on the {@link MergeRequest}.
     */
    public void handle(MetricSettings settings,
                       Metric metric,
                       SonarAnalysisTracker sourceBranchAnalysis) throws GitLabApiException {
        // Initialise parameters.
        HashMap<String, String> params = new HashMap<>();

        // Set parameters.
        params.put("componentKeys", sourceBranchAnalysis.getName());
        params.put("s", "FILE_LINE");
        params.put("resolved", "false");
        params.put("types", "VULNERABILITY");

        // Get issues from Sonar.
        List<SonarIssueWeb> issues = this.sonarApiWrapper.getUnresolvedIssues(params);

        // Sort issues per file.
        List<List<SonarIssueWeb>> issuesPerFile = this.getIssuesPerFile(issues);

        // Keep track of how many files we comment on
        int commentedFiles = 0;

        // Loop over each file.
        for (List<SonarIssueWeb> file : issuesPerFile) {
            // Initialise description.
            StringBuilder description = new StringBuilder();

            // Get issues per line within the file.
            List<List<SonarIssueWeb>> issuesPerLine = this.getIssuesPerLine(file);

            // Add all the vulnerabilities to the description.
            for (List<SonarIssueWeb> line : issuesPerLine) {
                this.addLineIssuesToDescription(line, description);
            }

            // Get the path of the file.
            String path = file.get(0).getComponent().split(":")[1];

            // Get file metric result from the database.
            Optional<MergeRequestFileMetricResult> optionalResult = this
                    .mergeRequestFileMetricResultRepository
                    .getByFilePathAndMergeRequestReviewAndMetricId(
                            path,
                            sourceBranchAnalysis.getMergeRequestReview().getId(),
                            metric.getId());

            // If the result is in the database, then the file was changed in the merge request.
            // Therefore, adjust it and save the new result.
            // If it is not in the database, then the file was not changed, so do not comment on
            // it.
            if (optionalResult.isPresent()) {
                MergeRequestFileMetricResult result = optionalResult.get();

                if (result.getScore() != null) {
                    String msg = "Vulnerabilities.handle: Processed an MrFileMetricResult with a"
                            + " not null score.";

                    throw new IllegalStateException(msg);
                }

                // Set attributes correctly
                result.setDescription(description.toString());
                result.setScore((double) issuesPerLine.size());
                result.setIsLowQuality(settings.isLowQuality(result.getScore()));

                // Update result in database.
                this.mergeRequestFileMetricResultRepository.save(result);

                commentedFiles++;
            }
        }

        List<MergeRequestFileMetricResult> results =
                this.mergeRequestFileMetricResultRepository
                    .findAllNullScoreLinkedToReviewAndMetric(
                        sourceBranchAnalysis.getMergeRequestReview().getId(),
                        metric.getId()
                    );

        log.info("Found " + results.size() + " null metric results.");

        results.forEach(r -> {
            r.setScore(0D);
            r.setIsLowQuality(false);
            this.mergeRequestFileMetricResultRepository.save(r);
        });

        if (commentedFiles == 0) {
            log.info("No SonarQube vulnerabilities found. No comments placed.");
        }
    }

    /**
     * Formats a set of issues and adds it to a {@link StringBuilder} description.
     *
     * @param lineIssues  The set of issues.
     * @param description The {@link StringBuilder}.
     */
    private void addLineIssuesToDescription(List<SonarIssueWeb> lineIssues,
                                            StringBuilder description) {
        // Add line number to description
        description.append("At line ")
                .append(lineIssues.get(0).getLine())
                .append(": ");

        // Add the messages of each issue to the description.
        for (SonarIssueWeb issue : lineIssues) {
            description.append(issue.getMessage())
                    .append(issue.getMessage().endsWith(".") ? "" : ".")
                    .append(" ");
        }

        // Add a next line character to the description.
        description.append("\n");
    }

    /**
     * Given a list of SonarQube issues over a file, this method returns for each line in these
     * SonarQube issues a list containing all the SonarQube issues of that line.
     *
     * @param issuesPerFile A list of SonarQube issues over a file.
     * @return              A list of lists, where each sublist contains SonarQube issues
     *                          related to on specific line.
     */
    private List<List<SonarIssueWeb>> getIssuesPerLine(List<SonarIssueWeb> issuesPerFile) {
        // Initialise list to be returned.
        List<List<SonarIssueWeb>> issuesPerLine = new ArrayList<>();

        // Initialise hashmap for fast lookup.
        Map<Integer, List<SonarIssueWeb>> presentLines = new HashMap<>();

        // Loop over each issue.
        for (SonarIssueWeb issue : issuesPerFile) {
            // Get line number
            Integer lineNumber = issue.getLine();

            // Get the list from the hashmap.
            // If it does not exist, create it and add it to the hashmap.
            List<SonarIssueWeb> lineIssues = presentLines.get(lineNumber);

            if (!presentLines.containsKey(lineNumber)) {
                lineIssues = new ArrayList<>();
                issuesPerLine.add(lineIssues);
                presentLines.put(lineNumber, lineIssues);
            }

            // Add issue to the list
            lineIssues.add(issue);
        }

        // Return the list of lists.
        return issuesPerLine;
    }

    /**
     * Given a list with SonarQube issues over the whole codebase, this method returns for each
     * file in these SonarQube issues a list containing all the SonarQube issues of that file.
     *
     * @param issues    A list of SonarQube issues
     * @return          A list of lists, where each sublist contains SonarQube issues related to
     *                      one specific file.
     */
    private List<List<SonarIssueWeb>> getIssuesPerFile(List<SonarIssueWeb> issues) {
        // Initialise list to be returned
        List<List<SonarIssueWeb>> issuesPerFile = new ArrayList<>();

        // Initialise hashmap for fast lookup
        Map<String, List<SonarIssueWeb>> presentFiles = new HashMap<>();

        // Loop over each issue
        for (SonarIssueWeb issue : issues) {
            // Get the file name
            String fileName = issue.getComponent();

            // Get the list from the hashmap.
            // If it does not exist, create it and add it to the hashmap.
            List<SonarIssueWeb> fileIssues = presentFiles.get(fileName);

            if (!presentFiles.containsKey(fileName)) {
                fileIssues = new ArrayList<>();
                issuesPerFile.add(fileIssues);
                presentFiles.put(fileName, fileIssues);
            }

            // Add the issue to the list
            fileIssues.add(issue);
        }

        // Return the list of lists
        return issuesPerFile;
    }
}
