package com.roborisk.officer.modules.metrics.codechurn;

import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.RepositoryFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Class with static methods used in {@link AbstractArtefactCodeChurnComputer},
 * {@link ClassCodeChurnComputer} and {@link FunctionCodeChurnComputer} for the retrieval of files
 * from GitLab as strings. Adheres to the Singleton design pattern.
 */
@Component
@Scope("singleton")
public class ArtefactCodeChurnHelper {

    /**
     * Wrapper for the GitLab API.
     */
    @Autowired
    GitLabApiWrapper gitLabApiWrapper;

    /**
     * Empty autowired constructor to make Spring realize this exists.
     */
    @Autowired
    public ArtefactCodeChurnHelper() {}

    /**
     * Method for retrieving the content of a file described by a {@link BranchFile} object.
     *
     * @param   branchFile              {@link BranchFile} describing the file to return.
     * @throws  IllegalStateException   If the file cannot be retrieved from GitLab.
     * @return                          {@link String} of the contents of the file.
     */
    public String getFileAsString(BranchFile branchFile) {
        // Retrieve the GitLab id for the repository.
        int repositoryId = branchFile.getBranch().getRepository().getGitLabId();
        // Retrieve the filepath of the relevant file.
        String filePath = branchFile.getFilePath();
        // Get GitLab id of the last commit for the file to be taken into account for the analysis.
        String commitId = branchFile.getLastChangeCommit().getHash();

        // Object storing file retrieved from GitLab.
        RepositoryFile repositoryFile = null;

        // Retrieve the file from GitLab.
        try {
            repositoryFile = this.gitLabApiWrapper.getGitLabApi().getRepositoryFileApi()
                .getFile(repositoryId, filePath, commitId);
        } catch (GitLabApiException e) {
            throw new IllegalStateException("Error in GitLabApi when retrieving file: "
                    + e.toString());
        }

        // Retrieve and return the content from the repository file.
        return repositoryFile.getDecodedContentAsString();
    }

    /**
     * Method for retrieving the content of a file described by a {@link BranchFile} object from a
     * version before the last commit modifying this file, indicated by a {@link GitCommit} hash.
     *
     * @param   branchFile          {@link BranchFile} describing the new version of the file to
     *                              return.
     * @param   commitHash          The hash of the {@link GitCommit} at which the file should be
     *                              retrieved.
     * @return                      {@link String} of the contents of the file.
     */
    public String getPreviousVersionFileAsString(BranchFile branchFile, String commitHash) {
        BranchFile previousVersion = new BranchFile();

        previousVersion.setBranch(branchFile.getBranch());
        previousVersion.setFilePath(branchFile.getFilePath());

        GitCommit previousCommit = new GitCommit();
        previousCommit.setHash(commitHash);

        previousVersion.setLastChangeCommit(previousCommit);

        return this.getFileAsString(previousVersion);
    }
}
