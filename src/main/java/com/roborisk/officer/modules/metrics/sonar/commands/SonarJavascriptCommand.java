package com.roborisk.officer.modules.metrics.sonar.commands;

import com.roborisk.officer.modules.metrics.sonar.SonarWebHelper;

/**
 * This class implements the {@link SonarCommand} class.
 * This class is used to create command line commands used to analyse SonarQube projects
 * containing Javascript source code.
 */
public class SonarJavascriptCommand extends SonarCommand {

    /**
     * Creates a SonarJavascriptCommand object.
     * Fails and throws an exception on windows systems, since sonar-scanner does not work
     * on windows.
     */
    public SonarJavascriptCommand(SonarWebHelper sonarWebHelper) {
        super(sonarWebHelper);

        // Check for Windows OS because it is not supported
        if (System.getProperty("os.name").contains("Windows")) {
            // unsupported thus throw exception
            throw new IllegalStateException("SonarProjectHelper.getJavascriptCommands: SonarQube"
                    + " analysis for JavaScript is not possible when SonarQube is running on "
                    + "windows");
        }
    }

    @Override
    public String getCommand(String projectName) {
        return "sonar-scanner -Dsonar.projectKey=" + projectName
                + " -Dsonar.sources=. -Dsonar.host.url=" + this.sonarWebHelper.getSonarBaseUrl()
                + " -Dsonar.login=" + this.sonarWebHelper.getSonarAccessToken();
    }
}
