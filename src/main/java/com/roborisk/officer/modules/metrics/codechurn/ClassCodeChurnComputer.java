package com.roborisk.officer.modules.metrics.codechurn;

import com.github.javaparser.ParseException;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.visitor.VoidVisitor;
import com.roborisk.officer.models.database.BranchClassObservation;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.metrics.BranchClassMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.BranchClassMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchClassObservationRepository;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Implementation for computing the Code Churn of class artefacts upon a new commit to a branch.
 */
@Component
@SuppressWarnings({"ClassFanOutComplexity"})
// Fanout is 16 instead of 15.
public class ClassCodeChurnComputer extends AbstractArtefactCodeChurnComputer {

    /**
     * Repository interface for {@link BranchClassObservation} objects.
     */
    @Autowired
    BranchClassObservationRepository branchClassObservationRepository;

    /**
     * Repository interface for {@link BranchClassMetricResult} objects.
     */
    @Autowired
    BranchClassMetricResultRepository branchClassMetricResultRepository;

    /**
     * Autowired constructor to make Spring scan it and inject other autowires.
     */
    @Autowired
    public ClassCodeChurnComputer() {
    }

    /**
     * Method which handles storing the code churn for a single class artefact in the database.
     *
     * @param classIdentifier   Identifier of the altered class.
     * @param lineNumber        Line number at which the class starts.
     * @param modified          Whether the artefact was changed in the commit.
     * @param branchFile        {@link BranchFile} representing the file containing the artefact.
     * @param commit            {@link GitCommit} representing the analyzed commit.
     * @param metric            {@link Metric} concerning the Class Code Churn Metric.
     * @param settings          {@link MetricSettings} for the Class Code Churn Metric.
     * @throws ParseException   If a syntactically incorrect file is analyzed.
     */
    private void handleClassObservation(String classIdentifier, int lineNumber, boolean modified,
                                        BranchFile branchFile, GitCommit commit, Metric metric,
                                        MetricSettings settings) {
        // Fetch the previous BranchClassObservation if it exists.
        Optional<BranchClassObservation> optionalObservation = this.branchClassObservationRepository
                .findFirstByBranchFileIdAndClassNameOrderByIdDesc(
                    branchFile.getId(),
                    classIdentifier
                );

        // Stores the number of times the class has been changed.
        int churnCount = 0;

        // Check whether a previous BranchClassObservation was found.
        if (optionalObservation.isPresent()) {
            BranchClassObservation previousObservation = optionalObservation.get();
            // Fetch the previous BranchClassMetricResult if it exists.
            Optional<BranchClassMetricResult> optionalResult =
                    this.branchClassMetricResultRepository
                    .findFirstByBranchClassObservationIdAndMetricIdOrderByIdDesc(
                            previousObservation.getId(), metric.getId()
                    );

            // Check whether the previous BranchClassMetricResult was found.
            if (optionalResult.isPresent()) {
                BranchClassMetricResult previousResult = optionalResult.get();

                // Set churnCount to previous result.
                churnCount = previousResult.getScore().intValue();
            }
        }

        // Update churnCount if the artefact was altered.
        if (modified) {
            churnCount++;
        }

        // Construct new BranchClassObservation object.
        BranchClassObservation newObservation = new BranchClassObservation();
        newObservation.setClassName(classIdentifier);
        newObservation.setLineNumber(lineNumber);
        newObservation.setBranchFile(branchFile);
        newObservation.setGitCommit(commit);

        // Save new BranchClassObservation in the database.
        this.branchClassObservationRepository.save(newObservation);

        // Construct new BranchClassMetricResult object.
        BranchClassMetricResult newResult = new BranchClassMetricResult();
        newResult.setBranchClassObservation(newObservation);
        newResult.setMetric(metric);
        newResult.setScore((double) churnCount);
        newResult.setIsLowQuality(settings.isLowQuality((double) churnCount));

        // Save new BranchClassMetricResult in the database.
        this.branchClassMetricResultRepository.save(newResult);
    }

    /**
     * Function for analyzing for all classes and interfaces in a single file whether adaptations
     * were made.
     *
     * @param fileOld           Contents of the file before the commit.
     * @param fileNew           Contents of the file after the commit.
     * @param branchFile        {@link BranchFile} representing the file.
     * @param commit            {@link GitCommit} representing the analyzed commit.
     * @param metric            {@link Metric} concerning the Class Code Churn Metric.
     * @param settings          {@link MetricSettings} for the Class Code Churn Metric.
     */
    @Override
    public void analyzeFileInCommit(String fileOld, String fileNew, BranchFile branchFile,
            GitCommit commit, Metric metric, MetricSettings settings) {
        // Parse contents of the old version of the file to create the abstract syntax tree.
        CompilationUnit compilationUnitOld = StaticJavaParser.parse(fileOld);

        // Create new HashMap to store the results of traversing the abstract syntax tree.
        Map<String, Pair<Long, Integer>> recordOld = new HashMap<>();
        // Instantiate the ClassVisitor object for traversing the abstract syntax tree.
        VoidVisitor<Map<String, Pair<Long, Integer>>> classVisitor = new ClassVisitor();
        // Visit all classes and interfaces in the old file and store class identifiers and
        // hashes of corresponding entire classes in the record.
        classVisitor.visit(compilationUnitOld, recordOld);

        // Parse contents of the new version of the file to create the abstract syntax tree.
        CompilationUnit compilationUnitNew = StaticJavaParser.parse(fileNew);

        // Create HashMap to store the results of traversing the new abstract syntax tree.
        Map<String, Pair<Long, Integer>> recordNew = new HashMap<>();
        // Visit all classes and interfaces in the new file and store class identifiers, hashes and
        // line numbers of corresponding entire classes in the record.
        classVisitor.visit(compilationUnitNew, recordNew);

        // Loop over all classes found in the new version of the file.
        for (String classIdentifier : recordNew.keySet()) {
            boolean modified;
            // Check whether the class has been newly introduced or has changed in comparison to
            // the previous version.
            if (!recordOld.containsKey(classIdentifier)) {
                // Class has been newly introduced.
                modified = true;
            } else {
                // Check whether class has changed in comparison to the previous version.
                modified = !recordOld.get(classIdentifier).getKey()
                    .equals(recordNew.get(classIdentifier).getKey());
            }

            // Handle the individual observation.
            this.handleClassObservation(classIdentifier, recordNew.get(classIdentifier).getValue(),
                    modified, branchFile, commit, metric, settings);
        }
    }
}
