package com.roborisk.officer.modules.metrics.codechurn;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import org.apache.commons.codec.digest.MurmurHash3;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import java.util.Map;
import java.util.Optional;

/**
 * Class for visiting all functions in a given file.
 */
public class FunctionVisitor
        extends VoidVisitorAdapter<Map<String, Triple<Long, Integer, String>>> {

    /**
     * Method for storing the function names with hashes of associated function bodies and the line
     * numbers at which the functions start of all functions in a scope in a record.
     * Should initially be called on the CompilationUnit of the selected file.
     *
     * @param functionDeclaration   node in the abstract syntax tree to analyze.
     * @param record                record to store function names and pair of hashed bodies and
     *                              line numbers in.
     */
    @Override
    public void visit(MethodDeclaration functionDeclaration,
            Map<String, Triple<Long, Integer, String>> record) {
        // Recursively visit entire subtree of functionDeclaration.
        super.visit(functionDeclaration, record);

        // Retrieve name of the function.
        final String functionName = functionDeclaration.getDeclarationAsString(true,
                false, false);
        // Compute hash of the entire function.
        final long hash = MurmurHash3.hash128(functionDeclaration.toString().getBytes())[0];
        // Retrieve the line number where the function starts.
        if (functionDeclaration.getBegin().isEmpty()) {
            String msg = "metrics.churn.FunctionVisitor: Begin of method not found";

            throw new IllegalStateException(msg);
        }
        final int lineNumber = functionDeclaration.getBegin().get().line;

        String className = null;

        // Retrieve the parent node in the abstract syntax tree.
        Optional<Node> parentNode = functionDeclaration.getParentNode();

        // Check whether the parent node exists.
        if (parentNode.isPresent()) {
            if (parentNode.get() instanceof ClassOrInterfaceDeclaration) {
                // Retrieve containing class or interface.
                ClassOrInterfaceDeclaration containingClass = (ClassOrInterfaceDeclaration)
                        parentNode.get();

                // Retrieve the name of the class containing the function.
                className = containingClass.getNameAsString();
            }

            // Check if the className is still null, because in that case, the containing
            // class is anonymous, and hence, this function need not be inserted into the
            // database as we treat it as part of the body of the containing method.
            if (className == null) {
                return;
            }
        }

        // Check that the class name was indeed set
        if (className == null) {
            // Otherwise, insert a dummy value to prevent database errors
            className = "Unidentifiable Class";
        }

        // Put the new tuple in the record.
        record.put(functionName, new ImmutableTriple<>(hash, lineNumber, className));
    }
}
