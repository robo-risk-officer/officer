package com.roborisk.officer.modules.metrics.sonar.commands;

import com.roborisk.officer.modules.git.GitModule;
import com.roborisk.officer.modules.metrics.sonar.SonarWebHelper;

import java.io.File;
import java.util.Objects;

/**
 * This class implements the {@link SonarCommand} class.
 * This class is used to create command line commands used to analyse SonarQube projects
 * containing Java source code.
 */
public class SonarJavaCommand extends SonarCommand {

    /**
     * Enum containing the different build types that the officer supports.
     */
    enum JavaBuildType {
        MAVEN,
        NONE
    }

    private final JavaBuildType buildType;

    /**
     * Creates a SonarJavaCommand object. Sets up a {@link SonarWebHelper}.
     * Checks what type of Java project is to be analysed.
     *
     * @param sourceModule      {@link GitModule} used to find out the type of java project.
     */
    public SonarJavaCommand(SonarWebHelper sonarWebHelper, GitModule sourceModule) {
        super(sonarWebHelper);

        // Set the build type for the commands
        File[] sourceProjectFiles = sourceModule.getWorkingDirectory().listFiles();
        this.buildType = this.getJavaProjectBuildType(
                Objects.requireNonNull(sourceProjectFiles)
        );

        if (this.buildType == JavaBuildType.NONE) {
            throw new IllegalStateException("SonarProjectHelper.getJavacommands: "
                    + "Failed to identify Maven java project. Only Maven is supported.");
        }
    }

    @Override
    public String getCommand(String projectName) {
        if (this.buildType == JavaBuildType.MAVEN) {
            return this.getMavenCommand(projectName);
        }

        return null;
    }

    /**
     * Creates and returns a String containing a command for a Maven project.
     *
     * @param projectName   Name of the project for which a command is returned.
     * @return              A string containing the command.
     */
    private String getMavenCommand(String projectName) {
        return "mvn compile; mvn sonar:sonar -Dsonar.projectKey=" + projectName
                + " -Dsonar.host.url=" + this.sonarWebHelper.getSonarBaseUrl()
                + " -Dsonar.login=" + this.sonarWebHelper.getSonarAccessToken();
    }

    /**
     * Function to check whether a directory of files contains a pom.xml file.
     *
     * @param projectFiles Array of files.
     * @return JavaBuildType of the file directory.
     */
    private JavaBuildType getJavaProjectBuildType(File[] projectFiles) {
        // Loop over all files project files.
        for (File file : projectFiles) {
            // If file is a directory, then do a recursive call on subdirectory.
            if (file.isDirectory()) {
                File[] subdir = file.listFiles();
                JavaBuildType subResult = this.getJavaProjectBuildType(subdir);
                if (subResult != JavaBuildType.NONE) {
                    return subResult;
                }
            }

            // If file is named pom.xml, then return MAVEN build type.
            if (file.getName().equals("pom.xml")) {
                return JavaBuildType.MAVEN;
            }
        }

        // No maven discovered, so no recognized build type.
        return JavaBuildType.NONE;
    }
}
