package com.roborisk.officer.modules.metrics.codechurn;

import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.metrics.AbstractMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import com.roborisk.officer.models.web.CommitWebhookData;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Abstract class for implementations for the code churn metric.
 * Extended by {@link FileCodeChurnComputer} and {@link AbstractArtefactCodeChurnComputer}.
 */
abstract class AbstractCodeChurnComputer {

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private BranchFileRepository branchFileRepository;

    /**
     * Helper class to get lists of added/modified/deleted {@link BranchFile} objects.
     */
    @Autowired
    private FileCodeChurnHelper fileCodeChurnHelper;

    /**
     * Autowired constructor to make Spring scan it and inject other autowires.
     */
    @Autowired
    public AbstractCodeChurnComputer() {
    }

    /**
     * Returns an array of the file types on which the code churn should be computed.
     *
     * @return  List of strings describing extensions of file types.
     */
    abstract List<String> getSupportedFileTypes();

    /**
     * Checks whether a file must be processed for code churn based on the file type.
     *
     * @param   fileName    The name of the file to check for.
     * @return              Whether the file should be processed.
     */
    boolean shouldBeProcessed(String fileName) {
        // Check whether file is of a type that should be processed.
        for (String fileExtension : this.getSupportedFileTypes()) {
            if (fileName.endsWith(fileExtension)) {
                return true;
            }
        }

        // File not of one of the supported file types.
        return false;
    }

    /**
     * Handles a single added {@link BranchFile} in a specific {@link GitCommit}.
     *
     * @param addition  The added {@link BranchFile} to be handled.
     * @param branch    The {@link Branch} to which the file was added.
     * @param commit    The {@link GitCommit} in which the file was added.
     * @param metric    The {@link Metric} for which the file should be handled.
     * @param settings  The {@link MetricSettings} for the to be analyzed {@link Metric}.
     */
    abstract void handleSingleAddition(BranchFile addition, Branch branch, GitCommit commit,
                                       Metric metric, MetricSettings settings);

    /**
     * Transform a list of {@link BranchFile} to a {@link AbstractMetricResult}
     * based on {@link Branch} and {@link Metric}. The additions are grouped by commit hash.
     *
     * @param additions The files that got added in the commit, grouped on commit hash.
     * @param branch    The branch the additions are part of.
     * @param metric    The metric that was executed.
     * @param settings  The metric settings to use for the {@link AbstractMetricResult}.
     */
    void handleAddition(List<Pair<GitCommit, List<BranchFile>>> additions,
                                Branch branch, Metric metric,
                                MetricSettings settings) {
        // Loop over the commits.
        additions.forEach(additionRecord -> {
            // loop over the files within that commit.
            for (BranchFile addition : additionRecord.getRight()) {
                this.handleSingleAddition(
                        addition, branch, additionRecord.getLeft(), metric, settings);

                // Give the CPU some rest.
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Transform a list of {@link BranchFile} to a {@link AbstractMetricResult}
     * based on {@link Branch} and {@link Metric}. The deletions are grouped by commit hash.
     *
     * @param deletions The files that got added in the commit, grouped on commit hash.
     * @param branch    The branch the additions are part of.
     * @param metric    The metric that was executed.
     * @param settings  The metric settings to use for the {@link AbstractMetricResult}.
     */
    abstract void handleDelete(List<Pair<GitCommit, List<BranchFile>>> deletions,
                              Branch branch, Metric metric,
                              MetricSettings settings);

    /**
     * Handles a single modified {@link BranchFile} in a specific {@link GitCommit}.
     *
     * @param file      The added {@link BranchFile} to be handled.
     * @param branch    The {@link Branch} to which the file was modified.
     * @param commit    The {@link GitCommit} in which the file was modified.
     * @param metric    The {@link Metric} for which the file should be handled.
     * @param settings  The {@link MetricSettings} for the to be analyzed {@link Metric}.
     */
    abstract void handleSingleModification(BranchFile file, Branch branch, GitCommit commit,
                                           Metric metric, MetricSettings settings);

    /**
     * Transform a list of {@link BranchFile} to a {@link AbstractMetricResult}
     * based on {@link Branch} and {@link Metric}. The modified files are grouped by commit hash.
     *
     * @param   modified                The files that got added in the commit, grouped on commit
     *                                  hash.
     * @param   branch                  The branch the additions are part of.
     * @param   metric                  The metric that was executed.
     * @param   settings                The metric settings to use for the
     *                                  {@link AbstractMetricResult}.
     * @throws  IllegalStateException   If file cannot be retrieved from GitLab.
     */
    void handleModifications(List<Pair<GitCommit, List<BranchFile>>> modified,
                                     Branch branch, Metric metric,
                                     MetricSettings settings) throws IllegalStateException {
        modified.forEach(modification -> {
            for (BranchFile adjusted : modification.getRight()) {
                BranchFile file = this.branchFileRepository.findByBranchIdAndFilePath(
                        branch.getId(),
                        adjusted.getFilePath()
                ).orElseThrow(() -> {
                    String msg
                            = "AbstractCodeChurnComputer.handleModifications: Could not find file";

                    return new IllegalStateException(msg);
                });

                this.handleSingleModification(
                        file, branch, modification.getLeft(), metric, settings);

                // Give the CPU some rest.
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Handle the Code Churn Metric. This will call all the events needed to
     * keep track of all changing elements.
     *
     * @param settings  The {@link MetricSettings} to use.
     * @param metric    The {@link Metric} to link everything to.
     * @param branch    The {@link Branch} this commit was part of.
     * @param data      The webhook data that initially triggered this event.
     */
    public void handle(MetricSettings settings, Metric metric, Branch branch,
                       CommitWebhookData data) {
        // Handle all additions, deletions and modifications.
        this.handleAddition(this.fileCodeChurnHelper.getAddedFiles(data), branch, metric, settings);
        this.handleDelete(this.fileCodeChurnHelper.getRemovedFiles(data), branch, metric, settings);
        this.handleModifications(this.fileCodeChurnHelper.getModifiedFiles(data), branch, metric,
                settings);
    }
}
