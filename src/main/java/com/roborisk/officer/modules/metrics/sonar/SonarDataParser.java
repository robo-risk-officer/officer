package com.roborisk.officer.modules.metrics.sonar;

import com.roborisk.officer.handlers.Dispatcher;
import com.roborisk.officer.models.database.SonarAnalysisTracker;
import com.roborisk.officer.models.repositories.SonarAnalysisTrackerRepository;
import com.roborisk.officer.models.web.InternalSonarWebhookData;
import com.roborisk.officer.models.web.SonarWebhookData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * This class implements the handler for the incoming webhooks from SonarQube. As SonarQube sends
 * a webhook for a project once it is done analysing that project, and the Robo Risk Officer needs
 * to wait until both the analysis of the source branch and target branch are finished, this handler
 * updates the database record of the tracker and checks whether this is the second request to come
 * in. If it is the second webhook, a {@link InternalSonarWebhookData} object is constructed and
 * passed to the {@link Dispatcher}.
 */
@Component
@Slf4j
public class SonarDataParser {

    /**
     * AutoWired {@link Dispatcher} singleton instance.
     */
    @Autowired
    private Dispatcher dispatcher;

    /**
     * Repository to interact with {@link SonarAnalysisTracker} database objects.
     */
    @Autowired
    private SonarAnalysisTrackerRepository sonarAnalysisTrackerRepository;

    /**
     * This function handles the webhook data incoming from sonar.
     * It updates the {@link SonarAnalysisTracker} that corresponds to the webhook in the database
     * and checks whether the analysis of the other branch is also done.  If both are done, the
     * results can be parsed.
     *
     * @param data  The webhook data from Sonar
     */
    public void handleSonarWebhook(SonarWebhookData data) {
        // Get project name
        String projectName = data.getProjectKey();

        // Get tracker from database.
        SonarAnalysisTracker tracker = this.sonarAnalysisTrackerRepository
                .findByName(projectName)
                .orElseThrow(() -> {
                    // As all webhooks that come back from SonarQube have a tracker and the webhooks
                    // contain secrets to identify that they come from the actual SonarQube
                    // instance, this can throw an exception.
                    String msg = "SonarDataParser.handleSonarWebhook: Database instance"
                            + " corresponding to the SonarQube webhook could not be found.";

                    throw new IllegalStateException(msg);
                });

        // Update tracker in database.
        tracker.setIsCompleted(true);
        this.sonarAnalysisTrackerRepository.save(tracker);

        // Get the other tracker of the MergeRequestReview.
        List<SonarAnalysisTracker> trackers = this.sonarAnalysisTrackerRepository
                .findAllByMergeRequestReviewId(tracker.getMergeRequestReview().getId());

        // MergeRequestReview should have 1 tracker
        if (trackers.size() != 1) {
            String msg = "SonarDataParser.handleSonarWebhook: Failed to handle SonarQube webhook,"
                    + " because corresponding MergeRequestReview did not have 1 tracker";

            throw new IllegalStateException(msg);
        }

        // Create internal webhook data to be processed by the event system
        InternalSonarWebhookData swd = new InternalSonarWebhookData();
        // Set attributes of SonarWebhookData.
        swd.setRepository(tracker.getMergeRequestReview()
                .getMergeRequest().getSourceBranch().getRepository().getWebObject());
        swd.setType("SonarQube Webhook");
        swd.setSourceBranchAnalysis(tracker);

        // Send webhook to the dispatcher.
        this.dispatcher.handle(swd);
    }
}
