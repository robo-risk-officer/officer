package com.roborisk.officer.helpers;

import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.MetricRepository;
import com.roborisk.officer.models.repositories.MetricSettingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper that provides functions to get metrics and their settings seamlessly.
 */
@Component
public class MetricModelHelper {
    /**
     * Database repository to handle {@link Metric} objects.
     */
    private static MetricRepository metricRepository;

    /**
     * Database repository to handle {@link MetricSettings} objects.
     */
    private static MetricSettingsRepository metricSettingsRepository;

    /**
     * Method that is automatically called by Spring to set the static repository.
     *
     * @param metricRepository  implementation of
     *                          the repository to handle {@link Metric} models.
     */
    @Autowired
    public void setMetricRepository(MetricRepository metricRepository) {
        MetricModelHelper.metricRepository = metricRepository;
    }

    /**
     * Method that is automatically called by Spring to set the repository statically.
     *
     * @param metricSettingsRepository  Implementation of
     *                                  the repository to handle {@link MetricSettings} models.
     */
    @Autowired
    public void setMetricSettingsRepository(MetricSettingsRepository metricSettingsRepository) {
        MetricModelHelper.metricSettingsRepository = metricSettingsRepository;
    }

    /**
     * Provides the metric database model, given the metric name.
     *
     * @param   metricName             The identifying name of the metric.
     * @throws  IllegalStateException  If metric was not in the database.
     * @return                         Database model {@link Metric} represented by
     *                                 {@code metricName}.
     */
    public static Metric getMetric(String metricName) throws IllegalStateException {
        return metricRepository.findByName(metricName)
            .orElseThrow(() -> {
                String msg = "MetricModelHelper.getMetric: Cannot find "
                        + "metric with name " + metricName;

                return new IllegalStateException(msg);
            });
    }

    /**
     * Provides current settings for a repository of a specific metric.
     *
     * @param  metricId                  The id of the metric to fetch settings for.
     * @param  repositoryId              The id of the repository for which to fetch settings.
     * @throws IllegalStateException     If database seeder has failed to supply default settings.
     * @return                           The settings of the repository, for the metric
     *                                   or default settings if
     *                                   none are specified for the repository.
     */
    public static MetricSettings getMetricSettings(Integer metricId, Integer repositoryId)
            throws IllegalStateException {
        return metricSettingsRepository.findByRepositoryIdAndMetricId(
            repositoryId, metricId
        ).orElse(getMetricDefaultSettings(metricId));
    }

    /**
     * Provides all default {@link MetricSettings}.
     *
     * @return  List of default {@link MetricSettings}, provided by the seeder.
     */
    public static List<MetricSettings> getAllDefaultMetricSettings() {
        return MetricModelHelper.getAllMetricSettingsOfRepo(null);
    }

    /**
     * Provides all {@link MetricSettings} linked to the repository with id {@code repositoryId}.
     *
     * @param repositoryId  The id of the repository to query the settings for.
     * @return              List of {@link MetricSettings} that are linked to
     *                      a repository with id {@code repositoryId}.
     */
    public static List<MetricSettings> getAllMetricSettingsOfRepo(Integer repositoryId) {
        return metricSettingsRepository.findByRepositoryId(repositoryId)
                    .orElse(new ArrayList<>());
    }

    /**
     * Provides the default settings of the metric.
     * It's intended to use this whenever the repository specific settings are not found.
     *
     * @param  metricId                  Metric id for which to find the default settings.
     * @throws IllegalStateException     If database seeder has failed to supply default settings.
     * @return                           The default settings of the metric.
     */
    private static MetricSettings getMetricDefaultSettings(Integer metricId)
            throws IllegalStateException {
        return metricSettingsRepository
            .findByRepositoryIdAndMetricId(null, metricId)
            .orElseThrow(() -> {
                String msg = "MetricModelHelper.getMetricDefaultSettings:"
                        + " Could not find default settings for metric with id: " + metricId;

                return new IllegalStateException(msg);
            });
    }
}
