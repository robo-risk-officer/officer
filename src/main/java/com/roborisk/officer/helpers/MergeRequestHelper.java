package com.roborisk.officer.helpers;

import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * The MergeRequestHelper implements methods to help executed tasks on {@link MergeRequest} objects.
 */
@Component
public class MergeRequestHelper {

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Returns a Page of entities meeting the paging restriction provided in the Pageable object.
     *
     * @param mrRepository  A database repository to handle {@link MergeRequest} objects.
     * @param pageNo        The number of the page that should be retrieved.
     * @param pageSize      The number of merge requests to be displayed per page.
     * @param sortBy        The name of the attribute to sort the merge requests on.
     * @return              A {@link List} of {@link MergeRequest} objects for the requested page.
     */
    public List<MergeRequest> getAllMergeRequests(MergeRequestRepository mrRepository,
            Integer pageNo, Integer pageSize, String sortBy) {
        // Define how the MergeRequest objects should be paged
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        // Get all merge requests from repository in a paged manner
        Page<MergeRequest> pagedResult = mrRepository.findAll(paging);

        // Check that the requested page indeed has merge requests on it
        if (pagedResult.hasContent()) {
            // If so, return it
            return pagedResult.getContent();
        }

        // Otherwise, return an empty list
        return new ArrayList<>();
    }

    /**
     * Find a {@link MergeRequest} object by GitLab ID.
     *
     * @param gitLabId                  The GitLab ID of the merge request.
     * @throws IllegalStateException    When the merge request does not exist.
     * @return                          The {@link MergeRequest} object linked to
     *                                  the {@code gitLabId}.
     */
    public MergeRequest findMergeRequestByGitLabId(Integer gitLabId) throws IllegalStateException {
        return this.mergeRequestRepository.findByGitLabId(
                gitLabId
        ).orElseThrow(() -> {
            String msg = "MergeRequestHelper.findMergeRequestByGitLabId: Cannot find merge "
                        + " request with GitLab ID " + gitLabId;

            return new IllegalStateException(msg);
        });
    }
}
