package com.roborisk.officer.helpers;

import com.roborisk.officer.models.database.BranchClassObservation;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.BranchFunctionObservation;
import com.roborisk.officer.models.repositories.BranchClassObservationRepository;
import com.roborisk.officer.models.repositories.BranchFunctionObservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@link BranchFileObservationHelper} class implements methods to get the latest
 * {@link BranchClassObservation} and {@link BranchFunctionObservation} objects that are associated
 * to a {@link BranchFile}.
 */
@Component
public class BranchFileObservationHelper {

    /**
     * Database repository to handle {@link BranchClassObservation} objects.
     */
    @Autowired
    private BranchClassObservationRepository branchClassObservationRepository;

    /**
     * Database repository to handle {@link BranchFunctionObservation} objects.
     */
    @Autowired
    private BranchFunctionObservationRepository branchFunctionObservationRepository;

    /**
     * Function to retrieve the latest {@link BranchClassObservation} objects per class for a
     * specific {@link BranchFile}. That is, for each class in a {@link BranchFile} it gets the
     * latest observation that is about that class.
     *
     * @param file  The {@link BranchFile} to find the latest {@link BranchClassObservation}
     *              objects for.
     * @return      A list of the latest {@link BranchClassObservation} objects.
     */
    public List<BranchClassObservation> getLatestClassObservations(BranchFile file) {
        // Then get all observations linked to the source file
        List<BranchClassObservation> observations =
                this.branchClassObservationRepository.findAllByBranchFile(file);

        // Loop over them and for each class in the file, get the latest observation
        Map<String, BranchClassObservation> latestObservationPerClass = new HashMap<>();

        for (BranchClassObservation observation : observations) {
            // Check if this class already is present in the map
            if (!latestObservationPerClass.containsKey(observation.getClassName())) {
                // It is not yet, so currently, this is the latest observation
                latestObservationPerClass.put(observation.getClassName(), observation);
            } else {
                // It is already there, so check if this observation is newer
                BranchClassObservation mapObservation =
                        latestObservationPerClass.get(observation.getClassName());

                if (observation.getGitCommit().getTimestamp()
                        > mapObservation.getGitCommit().getTimestamp()) {
                    // This observation is newer, so replace the map value
                    latestObservationPerClass.replace(observation.getClassName(), observation);
                }
            }
        }

        return new ArrayList<>(latestObservationPerClass.values());
    }

    /**
     * Function to retrieve the latest {@link BranchFunctionObservation} objects per function for a
     * specific {@link BranchFile}. That is, for each function in a {@link BranchFile} it gets the
     * latest observation that is about that function.
     *
     * @param file  The {@link BranchFile} to find the latest {@link BranchFunctionObservation}
     *              objects for.
     * @return      A list of the latest {@link BranchFunctionObservation} objects.
     */
    public List<BranchFunctionObservation> getLatestFunctionObservations(BranchFile file) {
        // Get all BranchFunctionObservations of the source file
        List<BranchFunctionObservation> observations =
                this.branchFunctionObservationRepository.findAllByBranchFile(file);

        // Then, per function, find the latest observation
        // Loop over them and for each class in the file, get the latest observation
        Map<String, BranchFunctionObservation> latestObservationPerFunction = new HashMap<>();

        for (BranchFunctionObservation observation : observations) {
            // Check if this class already is present in the map
            String observationName = observation.getClass() + "." + observation.getFunctionName();

            if (!latestObservationPerFunction.containsKey(observationName)) {
                // It is not yet, so currently, this is the latest observation
                latestObservationPerFunction.put(observationName, observation);
            } else {
                // It is already there, so check if this observation is newer
                BranchFunctionObservation mapObservation =
                        latestObservationPerFunction.get(observationName);

                if (observation.getGitCommit().getTimestamp()
                        > mapObservation.getGitCommit().getTimestamp()) {
                    // This observation is newer, so replace the map value
                    latestObservationPerFunction.replace(observationName, observation);
                }
            }
        }

        return new ArrayList<>(latestObservationPerFunction.values());
    }
}
