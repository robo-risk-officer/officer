package com.roborisk.officer.helpers;

import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import com.roborisk.officer.models.repositories.BranchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Helper class for the {@link Branch} database model.
 */
@Component
public class BranchHelper {

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Helper for {@link com.roborisk.officer.models.database.Repository} objects.
     */
    @Autowired
    private RepositoryHelper repositoryHelper;

    /**
     * Database repository to handle {@link BranchFile} objects.
     */
    @Autowired
    private BranchFileRepository branchFileRepository;

    /**
     * Find the branch by name and repository ID.
     *
     * @param name                      The branch name.
     * @param repositoryId              The repository ID.
     * @throws IllegalStateException    When the branch cannot be found.
     * @return                          The {@link Branch} object linked to the {@code name} and
     *                                  {@code repositoryId}.
     */
    public Branch findBranchByNameAndRepositoryId(String name, Integer repositoryId)
            throws IllegalStateException {
        return this.branchRepository.findByNameAndRepositoryId(
                name,
                repositoryId
        ).orElseThrow(() -> {
            String msg = "BranchHelper.findBranchByNameAndRepositoryId: Cannot find "
                    + " branch name " + name + " and repository ID " + repositoryId;

            return new IllegalStateException(msg);
        });
    }

    /**
     * Find the branch by name and GitLab repository ID.
     *
     * @param name                      The branch name.
     * @param repositoryGitLabId        The GitLab ID of the repository.
     * @throws IllegalStateException    When the branch cannot be found.
     * @return                          The {@link Branch} object linked to the {@code name}
     *                                  and {@code repositoryGitLabId}.
     */
    public Branch findBranchByNameAndRepositoryGitLabId(String name, Integer repositoryGitLabId) {
        Integer repositoryId =
                this.repositoryHelper.findRepositoryByGitLabId(repositoryGitLabId).getId();

        return this.findBranchByNameAndRepositoryId(name, repositoryId);
    }

    /**
     * Finds all branch files, given branch information.
     *
     * @param branchName            Name of the branch.
     * @param repositoryGitLabId    The GitLab Id of the repository.
     * @return                      All branch files in the database related to the branch.
     */
    public List<BranchFile> findBranchFilesOfBranch(String branchName, Integer repositoryGitLabId) {
        Branch branch = this.findBranchByNameAndRepositoryGitLabId(branchName, repositoryGitLabId);

        return this.branchFileRepository.findAllByBranchId(branch.getId());
    }
}
