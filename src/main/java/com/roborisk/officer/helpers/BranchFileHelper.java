package com.roborisk.officer.helpers;

import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.metrics.BranchFileMetricResult;
import com.roborisk.officer.models.repositories.BranchFileMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Helper for the {@link BranchFile} model.
 */
@Component
public class BranchFileHelper {

    /**
     * Database repository to handle {@link BranchFile} objects.
     */
    @Autowired
    private BranchFileRepository branchFileRepository;

    /**
     * Database repository to handle {@link BranchFileMetricResult} objects.
     */
    @Autowired
    private BranchFileMetricResultRepository branchFileMetricResultRepository;

    /**
     * Get a branch file by a branch ID and file path.
     *
     * @param branchId                  The branch Id to use.
     * @param path                      The File Path to USE.
     * @throws IllegalStateException    When the {@link BranchFile} object does not exist.
     * @return                          The {@link BranchFile} object linked to
     *                                  the given branchId and path.
     */
    public BranchFile getBranchFile(Integer branchId, String path) {
        return this.branchFileRepository.findByBranchIdAndFilePath(
                branchId, path
        ).orElseThrow(() -> {
            String msg = "BranchFileHelper.getBranchFile cannot find BranchFile with branch ID "
                        + branchId + " and file path " + path;

            return new IllegalStateException(msg);
        });
    }

    /**
     * Get the latest {@link BranchFileMetricResult} given a {@link MergeRequestFile}
     * and id for the branch and metric.
     *
     * @param branchId  The id of the branch of the branch file in question.
     * @param metricId  The id of the metric for which to query the result for.
     * @param file      The merge request file associated with the branch file.
     * @return          An optional of the latest result.
     */
    public Optional<BranchFileMetricResult> getLatestMetricResultOfFile(
            Integer branchId,
            Integer metricId,
            MergeRequestFile file
    ) {
        BranchFile branchFile = this.getBranchFile(branchId, file.getFilePath());

        return this.branchFileMetricResultRepository
            .findFirstByBranchFileIdAndMetricIdOrderByIdDesc(branchFile.getId(), metricId);
    }
}
