package com.roborisk.officer.helpers;

import com.roborisk.officer.helpers.copy.BranchCodebaseMetricResultCopyHelper;
import com.roborisk.officer.helpers.copy.BranchFileCopyHelper;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import com.roborisk.officer.models.repositories.BranchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * The {@link BranchInitMetricLinkHelper} class can be executed on the
 * {@link com.roborisk.officer.events.BranchInitEventCommit} or
 * {@link com.roborisk.officer.events.BranchInitEvent}
 * events to copy over the metric results from the branch that is branched off in order to ensure
 * that for instance churn history does not start anew.
 */
@Component
@Scope("singleton")
public class BranchInitMetricLinkHelper {

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Database repository to handle {@link BranchFile} objects.
     */
    @Autowired
    private BranchFileRepository branchFileRepository;

    /**
     * Helper to copy objects related to {@link BranchFile} objects.
     */
    @Autowired
    private BranchFileCopyHelper branchFileCopyHelper;

    /**
     * Helper to copy
     * {@link com.roborisk.officer.models.database.metrics.BranchCodebaseMetricResult} objects.
     */
    @Autowired
    private BranchCodebaseMetricResultCopyHelper branchCodebaseMetricResultCopyHelper;

    /**
     * Empty autowired constructor to make spring realize this exists.
     */
    @Autowired
    public BranchInitMetricLinkHelper() {

    }

    /**
     * This method initialises metric results for new branches that are not the result of creating
     * a new repository, but rather that were created due to branching from an existing branch.
     * This is done by copying over branch files and branch metric results from the common ancestor
     * of the new branch and the master branch of the repository (including supporting objects).
     *
     * @param newBranch The new branch for which the metric should be initialised.
     */
    public void initialiseBranch(Branch newBranch) {
        // Check that the new branch was the result of branching from master (i.e. that it is not
        // the master branch of a new repo)
        if (newBranch.getName().equals("master")) {
            return;
        }

        // Find the master branch of the repository
        Optional<Branch> masterBranch = this.branchRepository.findByNameAndRepositoryId(
                "master", newBranch.getRepository().getId());

        // And check it exists
        if (masterBranch.isEmpty()) {
            throw new IllegalStateException("BranchInitMetricLinkHelper.initialiseBranch: could not"
                    + " locate the master branch for the repository with ID "
                    + newBranch.getRepository().getId());
        }

        // Then copy over the BranchFile objects related to the common ancestor commit to the new
        // branch's latest commit, which will also take care of copying BranchFileMetricResult,
        // BranchClassMetricResult and BranchFunctionMetricResult objects, along with their
        // supporting objects.
        this.copyBranchFiles(masterBranch.get(), newBranch);

        // Finally copy the BranchCodebaseMetricResult objects
        this.branchCodebaseMetricResultCopyHelper.copy(masterBranch.get(), newBranch);
    }

    /**
     * Method that copies all {@link BranchFile} objects from a source to a target branch.
     * The method also copies all associated
     * {@link com.roborisk.officer.models.database.metrics.BranchFileMetricResult},
     * {@link com.roborisk.officer.models.database.metrics.BranchClassMetricResult} and
     * {@link com.roborisk.officer.models.database.metrics.BranchFunctionMetricResult} objects
     * along with their supporting objects, while resetting the isLowQuality flag to prevent
     * accidental comments on the new branch.
     *
     * @param source    The {@link Branch} from which to copy the {@link BranchFile} objects.
     * @param target    The {@link Branch} to which to copy the {@link BranchFile} objects.
     */
    private void copyBranchFiles(Branch source, Branch target) {
        // Get the files that should be copied
        List<BranchFile> filesToCopy = this.branchFileRepository.findAllByBranchId(source.getId());

        // For each file that should be copied
        for (BranchFile original : filesToCopy) {
            // Create a new version
            BranchFile image = new BranchFile();

            // Set the correct properties
            image.setIsDeleted(original.getIsDeleted());
            image.setFileName(original.getFileName());
            image.setFilePath(original.getFilePath());

            image.setLastChangeCommit(source.getLatestCommit());
            image.setBranch(target);

            // And save the file into the database
            this.branchFileRepository.save(image);

            // Copy all BranchFileMetricResult, BranchClassObservation, BranchClassMetricResult
            // BranchFunctionObservation and BranchFunctionMetricResult objects while resetting
            // their isLowQualityFlag
            this.branchFileCopyHelper.copy(original, image);
        }
    }
}
