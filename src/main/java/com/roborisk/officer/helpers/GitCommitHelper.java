package com.roborisk.officer.helpers;

import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Commit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Helper class for {@link GitCommit} models.
 */
@Component
public class GitCommitHelper {

    /**
     * API wrapper for the GitLab V4 API.
     */
    @Autowired
    private GitLabApiWrapper gitLabApiWrapper;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Get a {@link GitCommit} based on the latest GitLab commit that was done on a branch.
     *
     * @return  The {@link GitCommit} representing the latest GitLab commit on the branch.
     */
    public GitCommit getLatestFromGitLab(Repository repository, String branch)
                throws GitLabApiException {
        Commit commit = this.gitLabApiWrapper.getGitLabApi()
                            .getRepositoryApi()
                            .getBranch(repository.getGitLabId(), branch)
                            .getCommit();

        return this.gitCommitRepository.findByHash(commit.getId())
            .orElseGet(() -> {
                GitCommit gc = new GitCommit();

                gc.setTimestamp((int) commit.getCommittedDate().toInstant().getEpochSecond());
                gc.setMessage(commit.getMessage());
                gc.setHash(commit.getId());
                gc.setFinishedProcessing(false);

                return this.gitCommitRepository.save(gc);
            });
    }
}
