package com.roborisk.officer.helpers.translate;

import com.roborisk.officer.models.database.MergeRequestFunctionObservation;
import com.roborisk.officer.models.database.metrics.BranchFunctionMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestFunctionMetricResult;
import com.roborisk.officer.models.repositories.MergeRequestFunctionMetricResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class to translate {@link BranchFunctionMetricResult} objects into
 * {@link MergeRequestFunctionMetricResult} objects, which are saved in the database and returned.
 * Additionally, the {@link MergeRequestFunctionMetricResult} objects are linked to a supporting
 * {@link MergeRequestFunctionObservation} object.
 */
@Component
public class BranchFunctionMetricResultTranslationHelper extends
        AbstractBranchToMergeRequestTranslationHelper<List<BranchFunctionMetricResult>,
                List<MergeRequestFunctionMetricResult>, MergeRequestFunctionObservation> {

    /**
     * Database repository to handle {@link MergeRequestFunctionMetricResult} objects.
     */
    @Autowired
    private MergeRequestFunctionMetricResultRepository mergeRequestFunctionMetricResultRepository;

    @Override
    public List<MergeRequestFunctionMetricResult> translate(
            List<BranchFunctionMetricResult> results, MergeRequestFunctionObservation mrfo) {
        // Create a list of converted metric results
        List<MergeRequestFunctionMetricResult> returnList = new ArrayList<>();

        // Loop over all metric results
        for (BranchFunctionMetricResult brMetricResult : results) {
            // For each BranchFunctionMetricResult create an equivalent
            // MergeRequestFunctionMetricResult
            MergeRequestFunctionMetricResult mrMetricResult =
                    new MergeRequestFunctionMetricResult(brMetricResult, mrfo);

            // and save it in the database
            this.mergeRequestFunctionMetricResultRepository.save(mrMetricResult);

            // and add it to the returnList
            returnList.add(mrMetricResult);
        }

        // Return the list of converted results
        return returnList;
    }
}
