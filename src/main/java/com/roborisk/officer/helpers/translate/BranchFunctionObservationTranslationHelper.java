package com.roborisk.officer.helpers.translate;

import com.roborisk.officer.models.database.BranchFunctionObservation;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestFunctionObservation;
import com.roborisk.officer.models.repositories.MergeRequestFunctionObservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Helper class to translate {@link BranchFunctionObservation} objects into
 * {@link MergeRequestFunctionObservation} objects, which are saved in the database and returned.
 * Additionally, the {@link MergeRequestFunctionObservation} objects are linked to a supporting
 * {@link MergeRequestFile} object.
 */
@Component
public class BranchFunctionObservationTranslationHelper extends
        AbstractBranchToMergeRequestTranslationHelper<
                BranchFunctionObservation, MergeRequestFunctionObservation, MergeRequestFile> {

    /**
     * Database repository to handle {@link MergeRequestFunctionObservation} objects.
     */
    @Autowired
    private MergeRequestFunctionObservationRepository mergeRequestFunctionObservationRepository;

    @Override
    public MergeRequestFunctionObservation translate(BranchFunctionObservation observation,
            MergeRequestFile mrFile) {
        // Check if an equivalent MergeRequestFunctionObservation object already exists
        // for this BranchFunctionObservation
        Optional<MergeRequestFunctionObservation> optMrfo =
                this.mergeRequestFunctionObservationRepository
                        .findByMergeRequestFileAndClassNameAndFunctionName(
                                mrFile,
                                observation.getClassName(),
                                observation.getFunctionName()
                        );

        // If it exists already, return it
        if (optMrfo.isPresent()) {
            return optMrfo.get();
        }

        // The MergeRequestFunctionObservation does not exist yet, so create it
        MergeRequestFunctionObservation mrfo =
                new MergeRequestFunctionObservation(observation, mrFile);

        // Finally save the MergeRequestFunctionObservation
        return this.mergeRequestFunctionObservationRepository.save(mrfo);
    }
}
