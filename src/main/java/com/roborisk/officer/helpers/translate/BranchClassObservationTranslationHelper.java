package com.roborisk.officer.helpers.translate;

import com.roborisk.officer.models.database.BranchClassObservation;
import com.roborisk.officer.models.database.MergeRequestClassObservation;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.repositories.MergeRequestClassObservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Helper class to translate {@link BranchClassObservation} objects into
 * {@link MergeRequestClassObservation} objects, which are saved in the database and returned.
 * Additionally, the {@link MergeRequestClassObservation} objects are linked to a supporting
 * {@link MergeRequestFile} object.
 */
@Component
public class BranchClassObservationTranslationHelper extends
        AbstractBranchToMergeRequestTranslationHelper<
                    BranchClassObservation, MergeRequestClassObservation, MergeRequestFile> {

    /**
     * Database repository to handle {@link MergeRequestClassObservation} objects.
     */
    @Autowired
    private MergeRequestClassObservationRepository mergeRequestClassObservationRepository;

    @Override
    public MergeRequestClassObservation translate(BranchClassObservation source,
            MergeRequestFile mrFile) {
        // Check if an equivalent MergeRequestClassObservation object already exists
        // for this BranchClassObservation
        Optional<MergeRequestClassObservation> optMrco =
                this.mergeRequestClassObservationRepository
                        .findByMergeRequestFileAndClassName(
                                mrFile,
                                source.getClassName()
                        );

        // If it exists already, return it
        if (optMrco.isPresent()) {
            return optMrco.get();
        }

        // The MergeRequestClassObservation does not exist yet, so create it
        MergeRequestClassObservation mrco = new MergeRequestClassObservation(source, mrFile);

        // Finally save the MergeRequestClassObservation
        return this.mergeRequestClassObservationRepository.save(mrco);
    }
}
