package com.roborisk.officer.helpers.translate;

import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.repositories.MergeRequestFileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Helper class to translate {@link BranchFile} objects into {@link MergeRequestFile} objects,
 * which are saved in the database and returned. Additionally, the {@link MergeRequestFile} objects
 * are linked to a supporting {@link MergeRequestReview} object.
 */
@Component
public class BranchFileTranslationHelper
        extends AbstractBranchToMergeRequestTranslationHelper<
                BranchFile, MergeRequestFile, MergeRequestReview> {

    /**
     * Database repository to handle {@link MergeRequestFile} objects.
     */
    @Autowired
    private MergeRequestFileRepository mergeRequestFileRepository;

    @Override
    public MergeRequestFile translate(BranchFile source, MergeRequestReview mergeRequestReview) {
        // Check if a MergeRequestFile equivalent to the branch file already exists
        Optional<MergeRequestFile> optMrFile = this.mergeRequestFileRepository
                .findByFileNameAndFilePathAndMergeRequestReviewId(
                        source.getFileName(),
                        source.getFilePath(),
                        mergeRequestReview.getId()
                );

        // It exists already, so return it
        if (optMrFile.isPresent()) {
            return optMrFile.get();
        }

        // No such file exists already, so create it
        MergeRequestFile mrFile = new MergeRequestFile(source, mergeRequestReview);

        // And save and return the file.
        return this.mergeRequestFileRepository.save(mrFile);
    }
}
