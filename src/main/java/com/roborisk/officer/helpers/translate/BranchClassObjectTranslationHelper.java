package com.roborisk.officer.helpers.translate;

import com.roborisk.officer.models.database.BranchClassObservation;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.MergeRequestClassObservation;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.metrics.BranchClassMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestClassMetricResult;
import com.roborisk.officer.models.repositories.BranchClassMetricResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Helper class to translate a list of {@link BranchClassObservation} objects their
 * {@link MergeRequestClassObservation} counterparts which are saved in the database and
 * returned. Additionally, the {@link MergeRequestClassObservation} objects are linked to a
 * supporting {@link MergeRequestReview} object, and the supporting objects like {@link BranchFile}
 * and {@link BranchClassMetricResult} are also translated.
 */
@Component
public class BranchClassObjectTranslationHelper extends
        AbstractBranchToMergeRequestTranslationHelper<
                List<BranchClassObservation>, Void, MergeRequestReview> {

    /**
     * Helper class to translate {@link BranchClassObservation} objects into
     * {@link MergeRequestClassObservation} objects.
     */
    @Autowired
    private BranchClassObservationTranslationHelper branchClassObservationTranslationHelper;

    /**
     * Helper class to translate {@link BranchFile} objects into {@link MergeRequestFile} objects.
     */
    @Autowired
    private BranchFileTranslationHelper branchFileTranslationHelper;

    /**
     * Database repository to handle {@link BranchClassMetricResult} objects.
     */
    @Autowired
    private BranchClassMetricResultRepository branchClassMetricResultRepository;

    /**
     * Helper class to translate {@link BranchClassMetricResult} objects into
     * {@link MergeRequestClassMetricResult} objects.
     */
    @Autowired
    private BranchClassMetricResultTranslationHelper branchClassMetricResultTranslationHelper;

    @Override
    public Void translate(List<BranchClassObservation> observations,
            MergeRequestReview mergeRequestReview) {
        // Loop over all observations to translate them
        for (BranchClassObservation observation : observations) {
            // Get a MergeRequestFile equivalent to the BranchFile of the observation
            MergeRequestFile mrFile = this.branchFileTranslationHelper
                    .translate(observation.getBranchFile(), mergeRequestReview);

            // Translate the BranchClassObservation to an equivalent MergeRequestClassObservation
            MergeRequestClassObservation mrco =
                    this.branchClassObservationTranslationHelper.translate(observation, mrFile);

            // Translate all BranchClassMetricResults linked to this BranchClassObservation
            List<BranchClassMetricResult> resultsToTranslate =
                    this.branchClassMetricResultRepository
                            .findAllByBranchClassObservation(observation);

            this.branchClassMetricResultTranslationHelper.translate(resultsToTranslate, mrco);
        }

        // Because return type is Void, we need to return something
        return null;
    }
}
