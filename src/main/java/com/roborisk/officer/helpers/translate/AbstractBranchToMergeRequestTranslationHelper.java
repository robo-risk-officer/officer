package com.roborisk.officer.helpers.translate;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Abstract helper class to translate branch based source objects into their equivalent merge
 * request based target object. If necessary, the translated object can also be associated to
 * a supporting object.
 *
 * <p>
 * For example, a concrete class of this type could translate
 * {@link com.roborisk.officer.models.database.BranchFile} objects into
 * {@link com.roborisk.officer.models.database.MergeRequestFile objects} and linked to a
 * {@link com.roborisk.officer.models.database.MergeRequestReview}
 * </p>
 *
 * @param <S>   The type of the source objects.
 * @param <T>   The type of the target objects.
 * @param <L>   The type of the supporting objects.
 */
public abstract class AbstractBranchToMergeRequestTranslationHelper<S, T, L> {

    /**
     * Autowired constructor to make spring realise this exists.
     */
    @Autowired
    public AbstractBranchToMergeRequestTranslationHelper() {

    }

    /**
     * Method that translates a source object and returns and saves the target object in the
     * database repository for that target object.
     * For example, a concrete method could translate, return and store
     * {@link com.roborisk.officer.models.database.BranchFile} objects into
     * {@link com.roborisk.officer.models.database.MergeRequestFile} objects which are linked to
     * {@link com.roborisk.officer.models.database.MergeRequestReview} objects.
     *
     * @param source    The source object.
     * @param support   The supporting object that the target object should be associated to.
     */
    public abstract T translate(S source, L support);
}
