package com.roborisk.officer.helpers.translate;

import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.BranchFunctionObservation;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestFunctionObservation;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.metrics.BranchFunctionMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestFunctionMetricResult;
import com.roborisk.officer.models.repositories.BranchFunctionMetricResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Helper class to translate a list of {@link BranchFunctionObservation} objects their
 * {@link MergeRequestFunctionObservation} counterparts which are saved in the database and
 * returned. Additionally, the {@link MergeRequestFunctionObservation} objects are linked to a
 * supporting {@link MergeRequestReview} object, and the supporting objects like {@link BranchFile}
 * and {@link BranchFunctionMetricResult} are also translated.
 */
@Component
public class BranchFunctionObjectTranslationHelper extends
        AbstractBranchToMergeRequestTranslationHelper<
                List<BranchFunctionObservation>, Void, MergeRequestReview> {

    /**
     * Helper class to translate {@link BranchFile} objects into {@link MergeRequestFile} objects.
     */
    @Autowired
    private BranchFileTranslationHelper branchFileTranslationHelper;

    /**
     * Helper class to translate {@link BranchFunctionObservation} objects into
     * {@link MergeRequestFunctionObservation} objects.
     */
    @Autowired
    private BranchFunctionObservationTranslationHelper branchFunctionObservationTranslationHelper;

    /**
     * Database repository to handle {@link BranchFunctionMetricResult} objects.
     */
    @Autowired
    private BranchFunctionMetricResultRepository branchFunctionMetricResultRepository;

    /**
     * Helper class to translate {@link BranchFunctionMetricResult} objects into
     * {@link MergeRequestFunctionMetricResult} objects.
     */
    @Autowired
    private BranchFunctionMetricResultTranslationHelper branchFunctionMetricResultTranslationHelper;

    @Override
    public Void translate(List<BranchFunctionObservation> observations,
            MergeRequestReview mergeRequestReview) {
        // Loop over all observations to translate them
        for (BranchFunctionObservation observation : observations) {
            // Get a MergeRequestFile equivalent to the BranchFile of the observation
            MergeRequestFile mrFile = this.branchFileTranslationHelper
                    .translate(observation.getBranchFile(), mergeRequestReview);

            // Translate the BranchFunctionObservation to an equivalent
            // MergeRequestFunctionObservation
            MergeRequestFunctionObservation mrfo =
                    this.branchFunctionObservationTranslationHelper.translate(observation, mrFile);

            // Translate all BranchFunctionMetricResults linked to this BranchFunctionObservation
            List<BranchFunctionMetricResult> resultsToTranslate =
                    this.branchFunctionMetricResultRepository
                            .findAllByBranchFunctionObservation(observation);

            this.branchFunctionMetricResultTranslationHelper.translate(resultsToTranslate, mrfo);
        }

        // Because return type is Void, we need to return something
        return null;
    }
}
