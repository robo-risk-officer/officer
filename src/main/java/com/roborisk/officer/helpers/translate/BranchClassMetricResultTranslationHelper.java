package com.roborisk.officer.helpers.translate;

import com.roborisk.officer.models.database.MergeRequestClassObservation;
import com.roborisk.officer.models.database.metrics.BranchClassMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestClassMetricResult;
import com.roborisk.officer.models.repositories.MergeRequestClassMetricResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class to translate {@link BranchClassMetricResult} objects into
 * {@link MergeRequestClassMetricResult} objects, which are saved in the database and returned.
 * Additionally, the {@link MergeRequestClassMetricResult} objects are linked to a supporting
 * {@link MergeRequestClassObservation} object.
 */
@Component
public class BranchClassMetricResultTranslationHelper extends
        AbstractBranchToMergeRequestTranslationHelper<List<BranchClassMetricResult>,
                List<MergeRequestClassMetricResult>, MergeRequestClassObservation> {

    /**
     * Database repository to handle {@link MergeRequestClassMetricResult} objects.
     */
    @Autowired
    private MergeRequestClassMetricResultRepository mergeRequestClassMetricResultRepository;

    @Override
    public List<MergeRequestClassMetricResult> translate(List<BranchClassMetricResult> results,
            MergeRequestClassObservation mrco) {
        // Create a list of converted metric results
        List<MergeRequestClassMetricResult> returnList = new ArrayList<>();

        // Loop over all metric results
        for (BranchClassMetricResult brMetricResult : results) {
            // For each BranchClassMetricResult create an equivalent
            // MergeRequestClassMetricResult
            MergeRequestClassMetricResult mrMetricResult =
                    new MergeRequestClassMetricResult(brMetricResult, mrco);

            // and save it in the database
            this.mergeRequestClassMetricResultRepository.save(mrMetricResult);

            // and add it to the returnList
            returnList.add(mrMetricResult);
        }

        // Return the returnList
        return returnList;
    }
}
