package com.roborisk.officer.helpers;

import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.FileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * Wrapper to abstract the repository to which the upload is done from the call to the
 * upload function.
 */
@Component
public abstract class UploadWrapper {

    /**
     * Abstract function for getting the GitLabId of the repository to which should be uploaded.
     */
    protected abstract int getGitLabId();

    /**
     * {@link CommentHelper} which will eventually post the file.
     */
    @Autowired
    protected abstract CommentHelper getCommentHelper();

    /**
     * Function which uploads a file to GitLab and returns the FileUpload.
     *
     * @param file                  The file which should be uploaded.
     * @throws GitLabApiException   If any exception occurs within the {@link GitLabApiWrapper}.
     * @return                      The FileUpload returned by the CommentHelper.
     */
    public FileUpload upload(File file) throws GitLabApiException {
        return this.getCommentHelper().uploadImage(this.getGitLabId(), file);
    }
}
