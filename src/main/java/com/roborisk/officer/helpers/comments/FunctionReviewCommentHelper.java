package com.roborisk.officer.helpers.comments;

import com.roborisk.officer.helpers.CommentHelper;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestFunctionObservation;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.metrics.AbstractMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestFunctionMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.MergeRequestFileRepository;
import com.roborisk.officer.models.repositories.MergeRequestFunctionMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestFunctionObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.views.ArtefactEvaluationView;
import com.roborisk.officer.views.metrics.GeneralMetricEvaluationView;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Helper, which extracts the necessary data for the final review comments, passes it to the
 * appropriate views and posts the comments in GitLab.
 * Can only be used for Function reviews which are represented by a
 * {@link MergeRequestFunctionObservation}.
 */
@Component
@SuppressWarnings({"ClassFanOutComplexity"})
public class FunctionReviewCommentHelper {

    /**
     * The repository used to get {@link MergeRequestFunctionMetricResult} objects.
     */
    @Autowired
    private MergeRequestFunctionMetricResultRepository mergeRequestFunctionMetricResultRepository;

    /**
     * The repository used to get {@link MergeRequestReview} objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * The repository used to get {@link MergeRequestFile} objects.
     */
    @Autowired
    private MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * The repository used to get {@link MergeRequestFunctionObservation} objects.
     */
    @Autowired
    private MergeRequestFunctionObservationRepository mergeRequestFunctionObservationRepository;

    /**
     * The {@link CommentHelper} used to upload comments when leaving the review.
     */
    @Autowired
    private CommentHelper commentHelper;

    /**
     * The {@link ArtefactReviewCommentHelper} helps with getting information needed to post
     * comments.
     */
    @Autowired
    ArtefactReviewCommentHelper artefactReviewCommentHelper;

    /**
     * Post a review on the a single function. Similar to the other postReviewOn...(...) functions,
     * this function can be divided into three parts;
     * 1. check if we need to post a review in the first place - this only needs to be done if
     *      there is a metric of low quality.
     * 2. get all the data required by the view
     * 3. get the string from the view and post it towards GitLab (either as a new thread or an
     *      update if it has previously been reviewed).
     *
     * <p>The function has to be called per function that has been changed since the latest
     * review.</p>
     *
     * @param mergeRequest          The {@link MergeRequest} which is being reviewed.
     * @param functionObservation   The {@link MergeRequestFunctionObservation} that is being
     *                              reviewed.
     * @throws IOException          If an exception occurs in the {@link ArtefactEvaluationView}.
     * @throws GitLabApiException   If an exception occurs when posting or uploading towards GitLab.
     */
    private void postReviewOnSingleFunction(MergeRequest mergeRequest,
                                            MergeRequestFunctionObservation functionObservation)
            throws GitLabApiException, IOException {
        // 1. check if we even need to leave a comment on the latest review
        List<MergeRequestFunctionMetricResult> functionResults =
                this.mergeRequestFunctionMetricResultRepository
                        .findAllByMergeRequestFunctionObservation(functionObservation);

        boolean foundLowQuality =
                functionResults.stream().anyMatch(AbstractMetricResult::getIsLowQuality);

        if (!foundLowQuality) {
            // if non of the metric results associated to the function are of low quality, we don't
            // need a separate review.
            return;
        }

        // 2. generate all the data we need
        List<MergeRequestReview> mergeRequestReviews =
                this.mergeRequestReviewRepository.findAllByMergeRequest(mergeRequest);

        // A {@link Set} to eventually store all {@link Metric}s which have been used to evaluate
        // the codebase of this merge request.
        Set<Metric> allMetrics = new HashSet<>();

        // A {@link List} that will eventually store all the
        // {@MergeRequestCodebaseMetricResult}s which have ever been
        // left on the entire codebase in this merge request.
        List<MergeRequestFunctionMetricResult> results = new ArrayList<>();

        // fill both the objects by looping over all merge request reviews, checking if the file
        // that contains this class exists and if so grabbing the  the class and adding
        // the result to the data we need.
        for (MergeRequestReview mergeRequestReview : mergeRequestReviews) {
            Optional<MergeRequestFile> currentReviewFile =
                    this.mergeRequestFileRepository
                            .findByMergeRequestReviewIdAndFilePath(
                                    mergeRequestReview.getId(),
                                    functionObservation.getMergeRequestFile().getFilePath()
                            );

            if (currentReviewFile.isEmpty()) {
                // in this review the file did not change thus the class will also not be present.
                continue;
            }

            Optional<MergeRequestFunctionObservation> currentFunctionObservation =
                    this.mergeRequestFunctionObservationRepository
                            .findByMergeRequestFileAndClassNameAndFunctionName(
                                    currentReviewFile.get(),
                                    functionObservation.getClassName(),
                                    functionObservation.getFunctionName()
                            );

            if (currentFunctionObservation.isEmpty()) {
                // the file does exist but the class itself doesn't (which means at this point in
                // time the class either had a different name or didn't exist at all)
                continue;
            }

            List<MergeRequestFunctionMetricResult> currentClassMetricResults =
                    this.mergeRequestFunctionMetricResultRepository
                            .findAllByMergeRequestFunctionObservation(
                                    currentFunctionObservation.get()
                            );

            for (MergeRequestFunctionMetricResult result : currentClassMetricResults) {
                allMetrics.add(result.getMetric());
            }

            results.addAll(currentClassMetricResults);
        }

        // the map from Metric to GeneralMatricEvaluationViews as required by the
        // ArtefactEvaluationView
        Map<Metric, GeneralMetricEvaluationView> views =
                this.artefactReviewCommentHelper.getViewsForMetrics(allMetrics,
                        mergeRequest.getTargetBranch().getRepository().getGitLabId());

        // all MetricSettings associated to any of the metrics in allMetrics for this repository
        List<MetricSettings> metricSettings = this.artefactReviewCommentHelper
                .getMetricsSettings(allMetrics, mergeRequest.getTargetBranch()
                        .getRepository().getId());

        // 3. get evaluation string and post comment
        String review =
                new ArtefactEvaluationView<MergeRequestFunctionMetricResult>().generateComment(
                        results, metricSettings, views, mergeRequest);

        if (functionObservation.getDiscussionThread() == null) {
            this.commentHelper.postNoteForMergeRequestFunctionObservation(
                    functionObservation, review);
        } else {
            // if there is already a thread, there has already been a review that was posted.
            // Instead of posting a new one, that should be updated.
            this.commentHelper.modifyDiscussionThread(functionObservation.getDiscussionThread(),
                    review);
        }

    }

    /**
     * Iterate through all functions that have been changed since the last review and class
     * this.postReviewOnSingleFunction(...) on them.
     *
     * @param mergeRequest          The {@link MergeRequest} which is being reviewed.
     * @throws IOException          If an exception occurs in the {@link ArtefactEvaluationView}.
     * @throws GitLabApiException   If an exception occurs when posting or uploading towards GitLab.
     */
    public void postReviewOnAllFunctionsChanged(MergeRequest mergeRequest)
            throws GitLabApiException, IOException {
        // loop over all files, then loop over all function observations associated to those
        // files and post a review on those.
        List<MergeRequestFile> allReviewFiles = this.artefactReviewCommentHelper
                .getAllFilesForLatestReview(mergeRequest);

        for (MergeRequestFile file : allReviewFiles) {
            List<MergeRequestFunctionObservation> allFileFunctions =
                    this.mergeRequestFunctionObservationRepository.findAllByMergeRequestFile(file);

            for (MergeRequestFunctionObservation mergeRequestFunctionObservation :
                    allFileFunctions) {
                this.postReviewOnSingleFunction(mergeRequest, mergeRequestFunctionObservation);
            }
        }
    }
}
