package com.roborisk.officer.helpers.comments;

import com.roborisk.officer.helpers.CommentHelper;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.metrics.AbstractMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestFileMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.MergeRequestFileMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestFileRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.views.ArtefactEvaluationView;
import com.roborisk.officer.views.metrics.GeneralMetricEvaluationView;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Helper, which extracts the necessary data for the final review comments, passes it to the
 * appropriate views and posts the comments in GitLab.
 * Can only be used for File reviews which are represented by a {@link MergeRequestFile}.
 */
@Component
public class FileReviewCommentHelper {

    /**
     * The repository used to get {@link MergeRequestFileMetricResult} objects.
     */
    @Autowired
    private MergeRequestFileMetricResultRepository mergeRequestFileMetricResultRepository;

    /**
     * The repository used to get {@link MergeRequestReview} objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * The repository used to get {@link MergeRequestFile} objects.
     */
    @Autowired
    private MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * The {@link CommentHelper} used to upload comments when leaving the review.
     */
    @Autowired
    private CommentHelper commentHelper;

    /**
     * The {@link ArtefactReviewCommentHelper} helps with getting information needed to post
     * comments.
     */
    @Autowired
    private ArtefactReviewCommentHelper artefactReviewCommentHelper;

    /**
     * Post a review on the a single file. Similar to the other postReviewOn...(...) functions,
     * this function can be divided into three parts;
     * 1. check if we need to post a review in the first place - this only needs to be done if
     *      there is a file metric that evaluates to low quality.
     * 2. get all the data required by the view.
     * 3. get the string from the view and post it towards GitLab (either as a new thread or an
     *      update if it has previously been reviewed).
     *
     * <p>The function has to be called per file that has been changed in the since the latest
     * review.</p>
     *
     * @param mergeRequest          The {@link MergeRequest} which is being reviewed.
     * @param file                  The {@link MergeRequestFile} which is being reviewed.
     * @throws IOException          If an exception occurs in the {@link ArtefactEvaluationView}.
     * @throws GitLabApiException   If an exception occurs when posting or uploading towards GitLab.
     */
    private void postReviewOnSingleFile(MergeRequest mergeRequest, MergeRequestFile file)
            throws GitLabApiException, IOException {
        // 1. check if we even need to leave a comment on the latest review
        List<MergeRequestFileMetricResult> fileResults =
                this.mergeRequestFileMetricResultRepository.findAllByMergeRequestFile(file);

        boolean foundLowQualityFileMetric =
                fileResults.stream().anyMatch(AbstractMetricResult::getIsLowQuality);

        if (!foundLowQualityFileMetric) {
            // if non of the metric results associated to the file are of low quality, we don't
            // need a separate review.
            return;
        }

        // 2. generate all the data we need
        List<MergeRequestReview> mergeRequestReviews =
                this.mergeRequestReviewRepository.findAllByMergeRequest(mergeRequest);

        // A {@link Set} to eventually store all {@link Metric}s which have been used to evaluate
        // the codebase of this merge request.
        Set<Metric> allMetrics = new HashSet<>();

        // A {@link List} that will eventually store all the
        // {@MergeRequestCodebaseMetricResult}s which have ever been
        // left on the entire codebase in this merge request.
        List<MergeRequestFileMetricResult> results = new ArrayList<>();

        // fill both the objects
        for (MergeRequestReview mergeRequestReview : mergeRequestReviews) {
            Optional<MergeRequestFile> currentFile =
                    this.mergeRequestFileRepository.findByMergeRequestReviewIdAndFilePath(
                            mergeRequestReview.getId(), file.getFilePath());

            if (currentFile.isEmpty()) {
                // in this review the file did not change
                continue;
            }

            List<MergeRequestFileMetricResult> currentFileMetricResults =
                    this.mergeRequestFileMetricResultRepository
                            .findAllByMergeRequestFile(currentFile.get());

            for (MergeRequestFileMetricResult result : currentFileMetricResults) {
                allMetrics.add(result.getMetric());
            }

            results.addAll(currentFileMetricResults);
        }

        // all MetricSettings associated to any metric in allMetrics for this repository
        List<MetricSettings> metricSettings = this.artefactReviewCommentHelper
                .getMetricsSettings(allMetrics, mergeRequest.getTargetBranch()
                        .getRepository().getId());

        // Map from Metric to GeneralMetricEvaluationViews as required by ArtefactEvaluationView
        Map<Metric, GeneralMetricEvaluationView> views = this.artefactReviewCommentHelper
                .getViewsForMetrics(allMetrics, mergeRequest.getTargetBranch()
                        .getRepository().getGitLabId());

        // 3. get eval string and post comment
        String review =
                new ArtefactEvaluationView<MergeRequestFileMetricResult>().generateComment(
                        results, metricSettings, views, mergeRequest);

        if (file.getDiscussionThread() == null) {
            this.commentHelper.postNoteForMergeRequestFile(file, review);
        } else {
            // if there is already a thread, there has already been a review that was posted.
            // Instead of posting a new one, that should be updated.
            this.commentHelper.modifyDiscussionThread(file.getDiscussionThread(), review);
        }
    }

    /**
     * Iterate through all files that have been changed since the last review and class
     * this.postReviewOnSingleFile(...) on them.
     *
     * @param mergeRequest          The {@link MergeRequest} which is being reviewed.
     * @throws IOException          If an exception occurs in the {@link ArtefactEvaluationView}.
     * @throws GitLabApiException   If an exception occurs when posting or uploading towards GitLab.
     */
    public void postReviewOnAllFilesChanged(MergeRequest mergeRequest)
            throws GitLabApiException, IOException {
        List<MergeRequestFile> allReviewFiles = this.artefactReviewCommentHelper
                .getAllFilesForLatestReview(mergeRequest);

        for (MergeRequestFile file : allReviewFiles) {
            this.postReviewOnSingleFile(mergeRequest, file);
        }
    }
}
