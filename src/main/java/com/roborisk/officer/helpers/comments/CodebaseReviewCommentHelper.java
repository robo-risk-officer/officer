package com.roborisk.officer.helpers.comments;

import com.roborisk.officer.helpers.CommentHelper;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.metrics.AbstractMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestCodebaseMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.MergeRequestCodebaseMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.views.ArtefactEvaluationView;
import com.roborisk.officer.views.metrics.GeneralMetricEvaluationView;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Helper, which extracts the necessary data for the final review comments, passes it to the
 * appropriate views and posts the comments in GitLab.
 * Can only be used for Codebase reviews which are represented by a
 * {@link MergeRequestReview}.
 */
@Component
public class CodebaseReviewCommentHelper {

    /**
     * The repository used to get {@link MergeRequestReview} objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * The repository used to get {@link MergeRequestCodebaseMetricResult} objects.
     */
    @Autowired
    private MergeRequestCodebaseMetricResultRepository mergeRequestCodebaseMetricResultRepository;

    /**
     * The {@link CommentHelper} used to upload comments when leaving the review.
     */
    @Autowired
    private CommentHelper commentHelper;

    /**
     * The {@link ArtefactReviewCommentHelper} helps with getting information needed to post
     * comments.
     */
    @Autowired
    private ArtefactReviewCommentHelper artefactReviewCommentHelper;

    /**
     * Post a review on the entire codebase. Similar to the other postReviewOn...(...) functions,
     * this function can be divided into three parts;
     * 1. check if we need to post a review in the first place - this only needs to be done if
     *      there is a codebase metric evaluating to low quality.
     * 2. get all the data required by the view
     * 3. get the string from the view and post it towards GitLab (either as a new thread or an
     *      update if it has previously been reviewed).
     *
     * @param mergeRequest          The {@link MergeRequest} which is being reviewed.
     * @throws IOException          If an exception occurs in the {@link ArtefactEvaluationView}.
     * @throws GitLabApiException   If an exception occurs when posting or uploading towards GitLab.
     */
    public void postReviewOnCodebase(MergeRequest mergeRequest)
            throws IOException, GitLabApiException {
        MergeRequestReview latestReview = this.mergeRequestReviewRepository
                .findFirstByMergeRequestOrderByIdDesc(mergeRequest).orElseThrow(
                        () -> {
                            String msg = String.format("ArtefactReviewCommentHelper"
                                    + ".postReviewOnCodebase: could not retrieve latest review for "
                                    + "merge request with id %d", mergeRequest.getId());
                            throw new IllegalStateException(msg);
                        });

        // 1. check if we even need to leave a comment on the latest review
        List<MergeRequestCodebaseMetricResult> codebaseResults =
                this.mergeRequestCodebaseMetricResultRepository
                        .findAllByMergeRequestReviewId(latestReview.getId());

        boolean foundLowQualityCodebaseMetric =
                codebaseResults.stream().anyMatch(AbstractMetricResult::getIsLowQuality);

        // if non of the metric results associated to the codebase are of low quality, we don't
        // need a separate review.
        if (!foundLowQualityCodebaseMetric) {
            return;
        }

        // 2. generate all the data we need
        List<MergeRequestReview> mergeRequestReviews =
                this.mergeRequestReviewRepository.findAllByMergeRequest(mergeRequest);

        // A {@link Set} to eventually store all {@link Metric}s which have been used to evaluate
        // the codebase of this merge request.
        Set<Metric> allMetrics = new HashSet<>();

        // A {@link List} that will eventually store all the
        // {@MergeRequestCodebaseMetricResult}s which have ever been
        // left on the entire codebase in this merge request.
        List<MergeRequestCodebaseMetricResult> results = new ArrayList<>();

        // fill both the objects
        for (MergeRequestReview mergeRequestReview : mergeRequestReviews) {
            List<MergeRequestCodebaseMetricResult> currentCodebaseResults =
                    this.mergeRequestCodebaseMetricResultRepository
                            .findAllByMergeRequestReviewId(mergeRequestReview.getId());

            for (MergeRequestCodebaseMetricResult result : currentCodebaseResults) {
                allMetrics.add(result.getMetric());
            }

            results.addAll(currentCodebaseResults);
        }

        int repoGitLabId = mergeRequest.getTargetBranch().getRepository().getGitLabId();

        // the map from Metric to GeneralMatricEvaluationViews as required by the
        // ArtefactEvaluationView
        Map<Metric, GeneralMetricEvaluationView> views =
                this.artefactReviewCommentHelper.getViewsForMetrics(allMetrics, repoGitLabId);

        // all MetricSettings associated to any of the metrics in allMetrics for this repository
        List<MetricSettings> metricSettings = this.artefactReviewCommentHelper
                .getMetricsSettings(allMetrics, mergeRequest.getTargetBranch()
                        .getRepository().getId());

        // 3. get evaluation string and post comment
        String review = new ArtefactEvaluationView<MergeRequestCodebaseMetricResult>()
                .generateComment(results, metricSettings, views, mergeRequest);

        if (latestReview.getDiscussionThread() == null) {
            this.commentHelper.postNoteForMergeRequestReview(latestReview, review);
        } else {
            // if there is already a thread, there has already been a review that was posted.
            // Instead of posting a new one, that should be updated.
            this.commentHelper.modifyDiscussionThread(latestReview.getDiscussionThread(), review);
        }
    }
}
