package com.roborisk.officer.helpers.comments;

import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.MergeRequestFileRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.repositories.MetricSettingsRepository;
import com.roborisk.officer.views.ViewBindings;
import com.roborisk.officer.views.metrics.GeneralMetricEvaluationView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Helper, which helps the review comment classes to get the necessary information to place
 * comments.
 * appropriate views and posts the comments in GitLab.
 */
@Component
public class ArtefactReviewCommentHelper {

    /**
     * The {@link ViewBindings} which creates the correct {@link GeneralMetricEvaluationView}s
     * for the {@link Metric}.
     */
    @Autowired
    private ViewBindings viewBindings;

    /**
     * The repository used to get {@link MetricSettings} objects.
     */
    @Autowired
    private MetricSettingsRepository metricSettingsRepository;

    /**
     * The repository used to get {@link MergeRequestFile} objects.
     */
    @Autowired
    private MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * The repository used to get {@link MergeRequestReview} objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Creates a map from {@link Metric} to {@link GeneralMetricEvaluationView} which defines
     * which view should be used for each metric. The view is equipped with an
     * {@link com.roborisk.officer.helpers.UploadWrapper} that contains the GitLab id of the
     * repository such that it can upload photos.
     *
     * @param metrics   A {@link Set} of the metrics for which a
     *                  {@link GeneralMetricEvaluationView} should be created.
     * @param gitLabId  The GitLab Id of the repository which is being reviewed.
     * @return          A {@link Map} form a {@link Metric} to their corresponding
     *                  {@link GeneralMetricEvaluationView}.
     */
    public Map<Metric, GeneralMetricEvaluationView> getViewsForMetrics(Set<Metric> metrics,
                                                                        int gitLabId) {
        Map<Metric, GeneralMetricEvaluationView> views = new HashMap<>();

        for (Metric metric : metrics) {
            views.put(metric, this.viewBindings.getViewForMetric(metric, gitLabId));
        }

        return views;
    }

    /**
     * Get a list of all {@link MetricSettings} which are linked to the given {@link Metric}s in
     * the repository with the specified id.
     *
     * @param metrics       A {@link Set} of the {@link Metric}s for which settings should be
     *                      obtained.
     * @param repositoryId  The Id of the GitLab repository for which the {@link MetricSettings}
     *                      should be obtained.
     * @return              A {@link List} of the requested {@link MetricSettings}.
     */
    public List<MetricSettings> getMetricsSettings(Set<Metric> metrics, int repositoryId) {
        List<MetricSettings> metricSettings = new ArrayList<>();

        for (Metric metric : metrics) {
            MetricSettings settings = this.metricSettingsRepository.findByRepositoryIdAndMetricId(
                    repositoryId, metric.getId()).orElseThrow(
                        () -> {
                            String msg = String.format("ArtefactReviewCommentHelper"
                                    + ".getMetricsSettings: could not retrieve metric settings for "
                                    + "repository id %d and metric id %d",
                                        repositoryId, metric.getId());
                            throw new IllegalStateException(msg);
                        }
            );

            metricSettings.add(settings);
        }

        return metricSettings;
    }

    /**
     * Fetch all {@link MergeRequestFile} associated to the latest review of the specified
     * {@link MergeRequest}.
     *
     * @param mergeRequest  The specified {@link MergeRequest}.
     * @return              A {@link List} of the specified {@link MergeRequestFile}.
     */
    public List<MergeRequestFile> getAllFilesForLatestReview(MergeRequest mergeRequest) {
        MergeRequestReview latestReview = this.mergeRequestReviewRepository
                .findFirstByMergeRequestOrderByIdDesc(mergeRequest).orElseThrow(
                    () -> {
                        String msg = String.format("ArtefactReviewCommentHelper"
                                + ".getAllFilesForLatestReview: could not retrieve latest review "
                                + "for merge request with id %d", mergeRequest.getId());
                        throw new IllegalStateException(msg);
                    });

        return this.mergeRequestFileRepository
                .findAllByMergeRequestReviewId(latestReview.getId());
    }
}
