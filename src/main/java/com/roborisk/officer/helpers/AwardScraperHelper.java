package com.roborisk.officer.helpers;

import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.RobotSettings;
import com.roborisk.officer.models.web.AwardWeb;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.AccessLevel;
import org.gitlab4j.api.models.Discussion;
import org.gitlab4j.api.models.Member;
import org.gitlab4j.api.models.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * This class implements helper functions for scraping awards.
 */
@Component
public class AwardScraperHelper {

    /**
     * Defines the name of the GitLab emoji that counts as a false positive marking.
     */
    private static final String falsePositiveEmoji = "thumbsdown";

    /**
     * GitLabApiWrapper for making calls to GitLab to verify user access levels.
     */
    @Autowired
    private GitLabApiWrapper gitLabApiWrapper;

    /**
     * Stores the group name/id used to determine access rights of users using the GitLab server.
     */
    @Value("${officer.gitlab.robogroup}")
    private String gitLabGroup;

    /**
     * The environment contains variables like GitLab user id of the RRO.
     */
    @Value("${officer.gitlab.robo_risk_officer.id}")
    private int roboRiskOfficerId;

    /**
     * Checks that the user who place the award at least has the access level of developer.
     *
     * @param award                     The award to check the user's access level for.
     * @throws GitLabApiException       If a GitLabApiException occurs during verification.
     * @return                          {@code true <==>} the user who placed the award has
     *                                          developer access level or higher.
     */
    private boolean awardUserAtLeastDeveloper(AwardWeb award) throws GitLabApiException {
        // Get the list of users in the GitLab group
        List<Member> groupMembers = this.gitLabApiWrapper.getGitLabApi()
                .getGroupApi()
                .getAllMembers(this.gitLabGroup);

        // Get the user id of the user who placed the award
        Integer awardUserId = award.getUser().getGitLabId();

        // Loop over the members in the group to find the award user's access level
        Optional<Member> awardAuthor = groupMembers.stream().filter(
                member -> member.getId().equals(awardUserId)
        ).findFirst();

        if (awardAuthor.isEmpty()) {
            // The author of the award could not be found
            // From this, infer the user does not have sufficient permissions
            return false;
        }

        // Award author was found, so return if the author has sufficient permissions
        return awardAuthor.get().getAccessLevel().compareTo(AccessLevel.DEVELOPER) >= 0;
    }

    /**
     * Function that retrieves the awards that were left on a note in a GitLab merge request.
     *
     * @param note                  The {@link Note} for which to fetch the awards.
     * @param gitLabRepositoryId    The GitLab ID of the repository of the note.
     * @param gitLabMergeRequestIid The GitLab IID of the merge request of the note.
     * @return                      A list of {@link AwardWeb} objects representing awards left on
     *                              the note.
     */
    private List<AwardWeb> getAwardsForNote(Note note, int gitLabRepositoryId,
                                            int gitLabMergeRequestIid) {
        // Set string for getting the awards from this particular note
        String getNoteAwardsUrl = "/api/v4/projects/" + gitLabRepositoryId + "/merge_requests/"
                + gitLabMergeRequestIid + "/notes/" + note.getId() + "/award_emoji";

        // Get the awards via the gitLabApiWrapper
        AwardWeb[] responseAwards = this.gitLabApiWrapper.exchangeRequest(
            getNoteAwardsUrl, HttpMethod.GET, AwardWeb[].class
        ).getBody();

        // Create a list for the result
        List<AwardWeb> awards = new ArrayList<>();

        // Add the awards to the list
        if (responseAwards != null) {
            awards.addAll(Arrays.asList(responseAwards));
        }

        // Finally return the awards
        return awards;
    }

    /**
     * Method to update (i.e. open/close) a GitLab discussion to a certain false positive status.
     *
     * @param discussion            The {@link Discussion} to update.
     * @param repositoryId          The GitLab ID of the repository that the discussion is in.
     * @param mergeRequestIid       The GitLab IID of the merge request that the discussion is in.
     * @param shouldBeFalsePositive The new false positive state to set for the {@link Discussion}.
     * @throws GitLabApiException   If a {@link GitLabApiException} occurs while setting the false
     *                              positive status of a discussion.
     */
    private void updateGitLabDiscussion(Discussion discussion, int repositoryId,
                                        int mergeRequestIid, boolean shouldBeFalsePositive)
            throws GitLabApiException {
        // Check if the false positive state of the discussion should be changed
        if (discussion.getNotes().size() > 0
                && discussion.getNotes().get(0).getResolved() != null
                && discussion.getNotes().get(0).getResolved() != shouldBeFalsePositive) {
            // Check if the discussion should be opened or closed and post a comment accordingly
            if (shouldBeFalsePositive) {
                // The discussion should be closed so first post a message that the discussion
                // will be closed
                this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi()
                        .addMergeRequestThreadNote(repositoryId, mergeRequestIid,
                            discussion.getId(), "I will close this thread as it has been"
                                + " marked as a false positive.",
                            null); // set to null to use current time as postedAt time

                // and then close the discussion
                this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi()
                        .resolveMergeRequestDiscussion(repositoryId, mergeRequestIid,
                            discussion.getId(), shouldBeFalsePositive);
            }
            // The thread is not automatically reopened after markings are removed, because
            // it interferes with the future commenting system
        }
    }

    /**
     * Scrapes the awards of a {@link MergeRequest} and updates the {@link MergeRequest}
     * accordingly.
     *
     * @param repositoryGitLabId        GitLab ID of the repository of the {@link MergeRequest}.
     * @param mergeRequestGitLabIid     GitLab IiD of the {@link MergeRequest}.
     * @param settings                  Settings of the {@link MergeRequest} for the Robo Risk
     *                                  Officer.
     * @throws GitLabApiException       If GitLab throws an error.
     */
    public void scrapeMergeRequest(int repositoryGitLabId, int mergeRequestGitLabIid,
                                   RobotSettings settings) throws GitLabApiException {
        // Get all discussion threads on the merge request
        List<Discussion> discussionList = this.gitLabApiWrapper
                .getGitLabApi()
                .getDiscussionsApi()
                .getMergeRequestDiscussions(repositoryGitLabId, mergeRequestGitLabIid);

        // Loop over each discussion to see if it should be marked as false positive
        // A discussion is marked as false positive if at least one of its notes has more false
        // positive markings than the threshold
        for (Discussion discussion : discussionList) {
            // Get the notes for the discussion
            List<Note> discussionNotes = discussion.getNotes();

            // Filter the notes that were left by the Robo Risk Officer
            discussionNotes.removeIf(
                    note -> !note.getAuthor().getId().equals(this.roboRiskOfficerId)
            );

            // Keep a variable for the false positive status of this discussion
            boolean discussionIsFalsePositive = false;

            // Loop over the notes that are left, and check if one has enough markings
            // to warrant flagging the discussion as a false positive
            for (Note note : discussionNotes) {
                // Fetch the awards that were left on the note
                List<AwardWeb> noteAwards = this.getAwardsForNote(
                        note,
                        repositoryGitLabId,
                        mergeRequestGitLabIid
                );

                // Filter all awards that are not false positive markings
                noteAwards.removeIf(
                        award -> !award.getName().equals(AwardScraperHelper.falsePositiveEmoji)
                );

                // After that, the access level of the users who placed awards should be checked
                // Only false positive markings of users that have the access level of developer
                // or higher should be considered
                noteAwards.removeIf(award -> {
                    try {
                        return !this.awardUserAtLeastDeveloper(award);
                    } catch (GitLabApiException e) {
                        // Throw IllegalStateException so the method signature can stay the
                        // same (IllegalStateException need not be part of the signature,
                        // GitLabApiException does)
                        throw new IllegalStateException(
                            "UpdateDiscussionFalsePositiveEvent.handle: "
                                + " GitLabApiException occurred while verify award's "
                                + " user access level\n" + e.getMessage());
                    }
                });

                // Check if there are sufficient false positive markings for this note
                if (noteAwards.size() > settings.getFalsePositiveThreshold()) {
                    // There are enough false positive markings to flag the thread
                    discussionIsFalsePositive = true;
                    break;
                }
            }

            // Update the discussion thread in the GitLab UI according to the new false
            // positive state if necessary
            this.updateGitLabDiscussion(discussion, repositoryGitLabId,
                    mergeRequestGitLabIid, discussionIsFalsePositive);
        }
    }
}
