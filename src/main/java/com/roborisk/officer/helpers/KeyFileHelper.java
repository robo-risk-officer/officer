package com.roborisk.officer.helpers;

import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * This class contains static functions that help with verifying whether certain key files
 * are set up correctly with the right permissions.
 */
@Component
@Scope("singleton")
public class KeyFileHelper {

    /**
     * Stores the directory which contains all key files for the rro.
     */
    @Value("${officer.authentication.key_directory:/.rro/}")
    private String keyFileDirectory;

    /**
     * Gets a key of the rro from a single line of a file inside the
     * HOME_DIR/${@code tokenLocation} directory.
     * The permissions check is not executed on windows.
     *
     * @param keyFileName            Name of the key file.
     * @throws IllegalStateException When the key file does not have the correct permissions.
     * @throws IOException           When the key file does not exist or when the permissions
     *                                  of the key file cannot be read.
     * @return                       String of the key contents.
     */
    public String getKeyLine(String keyFileName)
            throws IllegalStateException, IOException {
        return this.getKeyLine(keyFileName, 0);
    }

    /**
     * Gets a key of the rro from a single line of a file inside the
     * HOME_DIR/${@code tokenLocation} directory.
     * The permissions check is not executed on windows.
     * The variable ${@code lineSkips} is used for key files that do not contain the actual
     * key on the first line.
     *
     * @param keyFileName            Name of the key file.
     * @param lineSkips              Number of lines to skip before reading key.
     * @throws IllegalStateException When the key file does not have the correct permissions.
     * @throws IOException           When the key file does not exist or when the permissions
     *                                  of the key file cannot be read.
     * @return                       String of the key contents.
     */
    public String getKeyLine(String keyFileName, int lineSkips)
            throws IllegalStateException, IOException {
        // Get key file location
        String keyFilePath = this.getKeyFilePath(keyFileName);
        File keyFile = new File(keyFilePath);

        // A key file is needed
        if (!keyFile.exists()) {
            throw new FileNotFoundException("KeyFileHelper.getAccessToken: "
                    + "Key file at " + keyFilePath + " was not"
                    + "found.");
        }

        if (!System.getProperty("os.name").contains("Windows")) {
            // Get file permissions on linux system
            PosixFileAttributes keyFileAttributes = Files.readAttributes(keyFile.toPath(),
                    PosixFileAttributes.class);
            Set<PosixFilePermission> keyFilePermissions = keyFileAttributes.permissions();

            // Set the wanted permissions
            Set<PosixFilePermission> requiredPermissions = new HashSet<>();
            requiredPermissions.add(PosixFilePermission.OWNER_READ);

            // If the permissions of the file is not 400 throw an exception
            if (!Sets.symmetricDifference(requiredPermissions, keyFilePermissions).isEmpty()) {
                throw new IllegalStateException("KeyFileHelper.getAccessToken: "
                        + "Key file at " + keyFilePath + " does not have 400 permissions.");
            }
        }

        // Extract key from file
        Scanner keyScanner = new Scanner(keyFile);

        // Skip the given number of lines
        for (int i = 0; i < lineSkips; i++) {
            if (keyScanner.hasNextLine()) {
                keyScanner.nextLine();
            }
        }

        if (!keyScanner.hasNextLine()) {
            throw new IllegalStateException(("KeyFileHelper.getAccessToken: "
                    + "Key file at " + keyFilePath + " has no more lines."));
        }

        // Get the key and close the scanner
        String key = keyScanner.nextLine();
        keyScanner.close();

        return key;
    }

    /**
     * Computes the absolute path to a key file with name ${@code keyFileName}.
     *
     * @param keyFileName   The name of the key file to find the path for.
     * @return              The absolute path to this key file.
     */
    public String getKeyFilePath(String keyFileName) {
        return System.getProperty("user.home") + this.keyFileDirectory + keyFileName;
    }
}
