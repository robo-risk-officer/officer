package com.roborisk.officer.helpers;

import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.metrics.MergeRequestFileMetricResult;
import com.roborisk.officer.events.SonarVulnerabilitiesEvent;
import com.roborisk.officer.models.repositories.MergeRequestFileMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestFileRepository;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Diff;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * This class helps the SonarReviewEvent to create empty metric results for all
 * merge request review files in the review.
 */
@Component
public class SonarReviewEventHelper {

    /**
     * Database repositoryu to handle {@link MergeRequestFileMetricResult} objects.
     */
    @Autowired
    private MergeRequestFileMetricResultRepository mergeRequestFileMetricResultRepository;

    /**
     * Database repository to handle {@link MergeRequestFile} objects.
     */
    @Autowired
    private MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * Wrapper to interact with GitLab.
     */
    @Autowired
    private GitLabApiWrapper gitLabApiWrapper;

    /**
     * Creates and stores a MetricResult object for a the given MergeRequestFile.
     *
     * @param mergeRequestFile  The mergeRequestFile to create a result for.
     */
    private void createAndSaveEmptyMetricResults(MergeRequestFile mergeRequestFile) {
        // Create a metric result with a score of null.
        MergeRequestFileMetricResult sonarMetricResult = new MergeRequestFileMetricResult();

        sonarMetricResult.setMetric(MetricModelHelper
                .getMetric(SonarVulnerabilitiesEvent.METRIC_NAME));
        sonarMetricResult.setScore(null);
        sonarMetricResult.setIsLowQuality(false);
        sonarMetricResult.setMergeRequestFile(mergeRequestFile);

        this.mergeRequestFileMetricResultRepository.save(sonarMetricResult);
    }

    /**
     * Creates and stores an empty metric result for sonar for all files that have been changed
     * in the {@link com.roborisk.officer.models.database.MergeRequest}.
     *
     * @param mergeRequestReview    The MergeRequestReview for which metric results are made.
     * @throws GitLabApiException   When the GitLabAPI raises an exception upon getting the
     *                              changed files from the merge request.
     */
    public void createAndSaveAllMetricResults(MergeRequestReview mergeRequestReview)
            throws GitLabApiException {
        // Get all changed files from GitLab.
        List<Diff> diffs = this.gitLabApiWrapper.getGitLabApi().getMergeRequestApi()
                .getMergeRequestChanges(
                    mergeRequestReview.getMergeRequest().getSourceBranch()
                        .getRepository().getGitLabId(),
                    mergeRequestReview.getMergeRequest().getGitLabIid()
                ).getChanges();

        // Get from the diffs, the strings of the new paths
        List<String> changedFiles = diffs.stream().map(Diff::getNewPath)
                .collect(Collectors.toList());

        for (String file : changedFiles) {
            // Query the db for the existence of the file
            Optional<MergeRequestFile> optionalMrFile = this.mergeRequestFileRepository
                    .findByMergeRequestReviewIdAndFilePath(mergeRequestReview.getId(), file);

            if (optionalMrFile.isPresent()) {
                // If it is present, create an empty metric result with that file.
                this.createAndSaveEmptyMetricResults(optionalMrFile.get());
            } else {
                // Else, create a new merge request file and the corresponding result.
                MergeRequestFile mrFile = new MergeRequestFile(
                        BranchFile.fromString(file), mergeRequestReview);

                mrFile = this.mergeRequestFileRepository.save(mrFile);

                this.createAndSaveEmptyMetricResults(mrFile);
            }
        }
    }


}
