package com.roborisk.officer.helpers;

import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.MergeRequestWeb;
import com.roborisk.officer.models.web.MergeRequestWebhookData;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Helper for the {@link MergeRequestWeb} model.
 */
@Component
@Scope("singleton")
public class MergeRequestWebHelper {

    /**
     * Extract the {@link MergeRequestWeb} from a {@link MergeRequestWebhookData} or
     * a {@link CommentWebhookData}.
     *
     * @param webhook The webhook data, should be either a {@link CommentWebhookData} or
     *                {@link MergeRequestWebhookData}.
     * @return        The {@link MergeRequestWeb} object.
     */
    public MergeRequestWeb extractMergeRequest(AbstractWebhookData webhook)
            throws IllegalArgumentException {

        if (webhook instanceof CommentWebhookData) {
            CommentWebhookData data = (CommentWebhookData) webhook;

            if (data.getMergeRequest() == null) {
                String msg = "MergeRequestWebHelper.extractMergeRequest "
                        + "CommentWebhookData does not have a merge request.";

                throw new IllegalArgumentException(msg);
            }

            return data.getMergeRequest();
        } else if (webhook instanceof MergeRequestWebhookData) {
            return ((MergeRequestWebhookData) webhook).getMergeRequest();
        }

        String msg = "MergeRequestWebHelper.extractMergeRequest "
                        + "only supports CommentWebhookData and MergeRequestWebhookData.";
        throw new IllegalArgumentException(msg);
    }
}
