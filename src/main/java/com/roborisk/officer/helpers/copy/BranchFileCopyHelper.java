package com.roborisk.officer.helpers.copy;

import com.roborisk.officer.models.database.BranchFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Helper class to copy all {@link com.roborisk.officer.models.database.BranchClassObservation},
 * {@link com.roborisk.officer.models.database.metrics.BranchClassMetricResult},
 * {@link com.roborisk.officer.models.database.BranchFunctionObservation}, and
 * {@link com.roborisk.officer.models.database.metrics.BranchFunctionMetricResult} objects
 * associated to a {@link BranchFile} and link them to another {@link BranchFile} while resetting
 * the isLowQuality flag.
 */
@Component
public class BranchFileCopyHelper extends AbstractBranchCopyHelper<BranchFile> {

    /**
     * Helper to copy {@link com.roborisk.officer.models.database.metrics.BranchFileMetricResult}
     * objects.
     */
    @Autowired
    private BranchFileMetricResultCopyHelper branchFileMetricResultCopyHelper;

    /**
     * Helper to copy {@link com.roborisk.officer.models.database.BranchClassObservation} and
     * {@link com.roborisk.officer.models.database.metrics.BranchClassMetricResult} objects.
     */
    @Autowired
    private BranchClassObservationAndMetricResultCopyHelper
            branchClassObservationAndMetricResultCopyHelper;

    /**
     * Helper to copy {@link com.roborisk.officer.models.database.BranchFunctionObservation} and
     * {@link com.roborisk.officer.models.database.metrics.BranchFunctionMetricResult} objects.
     */
    @Autowired
    private BranchFunctionObservationAndMetricResultCopyHelper
            branchFunctionObservationAndMetricResultCopyHelper;

    @Override
    public void copy(BranchFile source, BranchFile target) {
        // Find BranchFileMetricResult objects for the original file and create a copy for
        // the new file with a reset isLowQuality flag
        this.branchFileMetricResultCopyHelper.copy(source, target);

        // Also copy the BranchClassObservation and BranchClassMetricResult objects
        this.branchClassObservationAndMetricResultCopyHelper.copy(source, target);

        // Next copy the BranchFunctionObservation and BranchFunctionMetricResult objects
        this.branchFunctionObservationAndMetricResultCopyHelper.copy(source, target);
    }
}
