package com.roborisk.officer.helpers.copy;

import com.roborisk.officer.helpers.BranchFileObservationHelper;
import com.roborisk.officer.models.database.BranchClassObservation;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.metrics.BranchClassMetricResult;
import com.roborisk.officer.models.repositories.BranchClassMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchClassObservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Helper class to copy all {@link BranchClassObservation} and {@link BranchClassMetricResult}
 * objects associated to a {@link BranchFile} and link them to another {@link BranchFile} while
 * resetting the isLowQuality flag.
 */
@Component
public class BranchClassObservationAndMetricResultCopyHelper
        extends AbstractBranchCopyHelper<BranchFile> {

    /**
     * Database repository to handle {@link BranchClassObservation} objects.
     */
    @Autowired
    private BranchClassObservationRepository branchClassObservationRepository;

    /**
     * Database repository to handle {@link BranchClassMetricResult} objects.
     */
    @Autowired
    private BranchClassMetricResultRepository branchClassMetricResultRepository;

    /**
     * Helper class to retrieve the latest {@link BranchClassObservation} objects for a
     * {@link BranchFile}.
     */
    @Autowired
    private BranchFileObservationHelper branchFileObservationHelper;

    @Override
    public void copy(BranchFile source, BranchFile target) {
        // Get the latest class observations for the source file
        List<BranchClassObservation> latestObservations =
                this.branchFileObservationHelper.getLatestClassObservations(source);

        // Now, copy all the newest observations and connect them to the commit of the target
        // to keep all history and to avoid non-unique records.
        for (BranchClassObservation observation : latestObservations) {
            // First copy the observation
            BranchClassObservation newObservation = new BranchClassObservation();
            newObservation.setClassName(observation.getClassName());
            newObservation.setLineNumber(observation.getLineNumber());

            newObservation.setBranchFile(target);
            newObservation.setGitCommit(observation.getGitCommit());

            // Save the new observation
            this.branchClassObservationRepository.save(newObservation);

            // Then, copy over all the metric results associated to the original observation
            List<BranchClassMetricResult> originalResults = this.branchClassMetricResultRepository
                    .findAllByBranchClassObservation(observation);

            for (BranchClassMetricResult metricResult : originalResults) {
                // Create a new result
                BranchClassMetricResult newResult = new BranchClassMetricResult();
                newResult.setScore(metricResult.getScore());
                newResult.setMetric(metricResult.getMetric());

                newResult.setBranchClassObservation(newObservation);
                newResult.setIsLowQuality(false);

                // Save the new result
                this.branchClassMetricResultRepository.save(newResult);
            }
        }
    }
}
