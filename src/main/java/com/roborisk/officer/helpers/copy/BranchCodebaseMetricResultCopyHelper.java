package com.roborisk.officer.helpers.copy;

import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.metrics.BranchCodebaseMetricResult;
import com.roborisk.officer.models.repositories.BranchCodebaseMetricResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Helper class to copy all {@link BranchCodebaseMetricResult}  objects associated to a
 * {@link Branch} and link them to another {@link Branch} while resetting the isLowQuality flag.
 */
@Component
public class BranchCodebaseMetricResultCopyHelper extends AbstractBranchCopyHelper<Branch> {

    /**
     * Database repository to handle {@link BranchCodebaseMetricResult} objects.
     */
    @Autowired
    private BranchCodebaseMetricResultRepository branchCodebaseMetricResultRepository;

    @Override
    public void copy(Branch source, Branch target) {
        // Get the original BranchCodebaseMetricResults
        List<BranchCodebaseMetricResult> results =
                this.branchCodebaseMetricResultRepository.findAllByBranch(source);

        // For each of these results
        for (BranchCodebaseMetricResult metricResult : results) {
            // Create a new result
            BranchCodebaseMetricResult newResult = new BranchCodebaseMetricResult();
            newResult.setScore(metricResult.getScore());
            newResult.setMetric(metricResult.getMetric());

            newResult.setIsLowQuality(false);
            newResult.setLastChangeCommit(target.getLatestCommit());
            newResult.setBranch(target);

            // Save the new result
            this.branchCodebaseMetricResultRepository.save(newResult);
        }
    }
}
