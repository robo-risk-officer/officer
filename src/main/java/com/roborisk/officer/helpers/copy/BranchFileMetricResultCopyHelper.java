package com.roborisk.officer.helpers.copy;

import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.metrics.BranchFileMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.enums.MetricTypeEnum;
import com.roborisk.officer.models.repositories.BranchFileMetricResultRepository;
import com.roborisk.officer.models.repositories.MetricRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Helper class to copy all {@link BranchFileMetricResult} objects associated to a
 * {@link BranchFile} and link them to another {@link BranchFile} while resetting the isLowQuality
 * flag.
 */
@Component
public class BranchFileMetricResultCopyHelper extends AbstractBranchCopyHelper<BranchFile> {

    /**
     * Database repository to handle {@link BranchFileMetricResult} objects.
     */
    @Autowired
    private BranchFileMetricResultRepository branchFileMetricResultRepository;

    /**
     * Database repository to handle {@link Metric} objects.
     */
    @Autowired
    private MetricRepository metricRepository;

    @Override
    public void copy(BranchFile source, BranchFile target) {
        for (Metric metric : this.metricRepository.findAll()) {
            if (metric.getMetricType() != MetricTypeEnum.FILE) {
                continue;
            }

            Optional<BranchFileMetricResult> optMetricResult =
                    this.branchFileMetricResultRepository
                            .findFirstByBranchFileIdAndMetricIdOrderByIdDesc(
                                source.getId(),
                                metric.getId()
                            );

            if (optMetricResult.isEmpty()) {
                continue;
            }

            BranchFileMetricResult metricResult = optMetricResult.get();

            // First check if a file metric result already exists for the target file
            Optional<BranchFileMetricResult> optDbResult = this.branchFileMetricResultRepository
                    .findFirstByBranchFileIdAndMetricIdOrderByIdDesc(
                        target.getId(),
                        metricResult.getMetric().getId()
                    );

            if (optDbResult.isPresent()) {
                // It exists already, so return
                return;
            }

            // Create a new metric result
            BranchFileMetricResult newMetricResult = new BranchFileMetricResult();

            // Set the correct properties
            newMetricResult.setScore(metricResult.getScore());
            newMetricResult.setMetric(metricResult.getMetric());
            newMetricResult.setIsLowQuality(false);
            newMetricResult.setBranchFile(target);
            newMetricResult.setLastChangeCommit(target.getBranch().getLatestCommit());

            // And save the result into the database
            this.branchFileMetricResultRepository.save(newMetricResult);
        }
    }
}
