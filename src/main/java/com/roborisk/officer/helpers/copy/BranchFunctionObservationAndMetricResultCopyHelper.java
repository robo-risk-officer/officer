package com.roborisk.officer.helpers.copy;

import com.roborisk.officer.helpers.BranchFileObservationHelper;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.BranchFunctionObservation;
import com.roborisk.officer.models.database.metrics.BranchFunctionMetricResult;
import com.roborisk.officer.models.repositories.BranchFunctionMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchFunctionObservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Helper class to copy all {@link BranchFunctionObservation} and {@link BranchFunctionMetricResult}
 * objects associated to a {@link BranchFile} and link them to another {@link BranchFile} while
 * resetting the isLowQuality flag.
 */
@Component
public class BranchFunctionObservationAndMetricResultCopyHelper
        extends AbstractBranchCopyHelper<BranchFile> {

    /**
     * Database repository to handle {@link BranchFunctionObservation} objects.
     */
    @Autowired
    private BranchFunctionObservationRepository branchFunctionObservationRepository;

    /**
     * Database repository to handle {@link BranchFunctionMetricResult} objects.
     */
    @Autowired
    private BranchFunctionMetricResultRepository branchFunctionMetricResultRepository;

    /**
     * Helper class to retrieve the latest {@link BranchFunctionObservation} objects for a
     * {@link BranchFile}.
     */
    @Autowired
    private BranchFileObservationHelper branchFileObservationHelper;

    @Override
    public void copy(BranchFile source, BranchFile target) {
        List<BranchFunctionObservation> latestObservations =
                this.branchFileObservationHelper.getLatestFunctionObservations(source);

        // Now, copy all the newest observations and connect them to the commit of the target
        // to keep all history and to avoid non-unique records.
        for (BranchFunctionObservation observation : latestObservations) {
            // First copy the observation
            BranchFunctionObservation newObservation = new BranchFunctionObservation();
            newObservation.setClassName(observation.getClassName());
            newObservation.setFunctionName(observation.getFunctionName());
            newObservation.setLineNumber(observation.getLineNumber());

            newObservation.setBranchFile(target);
            newObservation.setGitCommit(observation.getGitCommit());

            // Save the new observation
            this.branchFunctionObservationRepository.save(newObservation);

            // Then, copy over all the metric results associated to the original observation
            List<BranchFunctionMetricResult> originalResults =
                    this.branchFunctionMetricResultRepository
                    .findAllByBranchFunctionObservation(observation);

            for (BranchFunctionMetricResult metricResult : originalResults) {
                // Create a new result
                BranchFunctionMetricResult newResult = new BranchFunctionMetricResult();
                newResult.setScore(metricResult.getScore());
                newResult.setMetric(metricResult.getMetric());

                newResult.setIsLowQuality(false);
                newResult.setBranchFunctionObservation(newObservation);

                // Save the new result
                this.branchFunctionMetricResultRepository.save(newResult);
            }
        }
    }
}
