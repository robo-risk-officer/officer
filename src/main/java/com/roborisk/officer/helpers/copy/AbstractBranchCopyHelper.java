package com.roborisk.officer.helpers.copy;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Abstract helper class to copy objects related to a source object and associate them
 * to another target object of the same type.
 *
 * <p>
 * For example, a concrete class of this type could copy
 * {@link com.roborisk.officer.models.database.metrics.BranchFileMetricResult} objects for
 * {@link com.roborisk.officer.models.database.BranchFile} objects.
 * </p>
 */
public abstract class AbstractBranchCopyHelper<T> {

    /**
     * Autowired constructor to make spring realise this exists.
     */
    @Autowired
    public AbstractBranchCopyHelper() {

    }

    /**
     * Method that copies objects related to a source object and associates them to a target
     * object of the same type, while resetting their isLowQuality flag.
     * For example, a concrete method could copy and reset the isLowQuality flag of
     * {@link com.roborisk.officer.models.database.metrics.BranchFileMetricResult}  objects for
     * {@link com.roborisk.officer.models.database.BranchFile} objects.
     *
     * @param source    The source object.
     * @param target    The target object.
     */
    public abstract void copy(T source, T target);

}
