package com.roborisk.officer.helpers;

import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.RobotSettings;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.repositories.RobotSettingsRepository;
import com.roborisk.officer.models.web.RepositoryWeb;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Helper for querying for {@link RobotSettings}.
 */
public class RobotSettingsHelper {

    /**
     * Database repository to handle {@link RobotSettings} objects.
     */
    private static RobotSettingsRepository robotSettingsRepository;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    private static RepositoryRepository repositoryRepository;

    /**
     * Method that is automatically called by Spring to set the static repository.
     *
     * @param repository Implementation of the repository to handle {@link RobotSettings} models.
     */
    @Autowired
    public void setRobotSettingsRepository(RobotSettingsRepository repository) {
        RobotSettingsHelper.robotSettingsRepository = repository;
    }

    /**
     * Method that is automatically called by Spring to set the static repository.
     *
     * @param repository Implementation of the repository to handle {@link Repository} models.
     */
    @Autowired
    public void setRepositoryRepository(RepositoryRepository repository) {
        RobotSettingsHelper.repositoryRepository = repository;
    }

    /**
     * Get settings directly linked to the {@code repository}.
     *
     * @param  repository               The repository to query the settings for.
     * @throws IllegalStateException    If default settings are not in the database.
     * @return                          {@link RobotSettings} of {@code repository}
     */
    public static RobotSettings getRepositorySettings(Repository repository)
            throws IllegalStateException {
        return robotSettingsRepository
            .findByRepositoryId(repository.getId())
            .orElse(robotSettingsRepository.getDefault()
                    .orElseThrow(() -> {
                        String msg = "RobotSettingsHelper.getRepositorySettings: "
                                + "Default robot settings not in the database";

                        return new IllegalStateException(msg);
                    })
            );
    }

    /**
     * Get settings linked to {@code repositoryWeb}, assuming it is already in the database.
     *
     * @param  repositoryWeb            The {@link RepositoryWeb} to query the settings for.
     * @throws IllegalArgumentException If {@link Repository} representation of
     *                                  {@code repositoryWeb} was not
     *                                  in the {@link RepositoryRepository}.
     * @throws IllegalStateException    If default settings are not in the database.
     * @return                          {@link RobotSettings} linked to the equivalent
     *                                  {@link Repository} model of {@code repositoryWeb}.
     */
    public static RobotSettings getRepositorySettings(RepositoryWeb repositoryWeb)
            throws IllegalArgumentException, IllegalStateException {
        Repository repository = repositoryRepository.findByGitLabId(repositoryWeb.getGitLabId())
                                    .orElseThrow(() -> {
                                        String msg = "RobotSettingsHelper.getRepositorySettings: "
                                                + " Repository with gitlab id "
                                                + repositoryWeb.getGitLabId() + " was not found.";

                                        return new IllegalArgumentException(msg);
                                    });

        return getRepositorySettings(repository);
    }
}
