package com.roborisk.officer.helpers;

import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.RobotSettings;
import com.roborisk.officer.models.repositories.MetricSettingsRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.repositories.RobotSettingsRepository;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implements the Repository List Api Controller Helper.
 * The Helper helps with pagination.
 */
@Component
public class RepositoryHelper {

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link RobotSettings} objects.
     */
    @Autowired
    private RobotSettingsRepository robotSettingsRepository;

    /**
     * Database repository to handle {@link MetricSettings} objects.
     */
    @Autowired
    private MetricSettingsRepository metricSettingsRepository;

    /**
     * GitLabApiWrapper for making calls to GitLab.
     */
    @Autowired
    private GitLabApiWrapper gitLabApiWrapper;

    /**
     * Acts as a paging service between the repository and the controller.
     *
     * @param pageNo            Number of first page.
     * @param pageSize          Amount of elements per page.
     * @param sortBy            Value to sort by.
     * @return                  Paged list of repositories.
     */
    public List<Repository> getAllRepositoryRequests(
                                                     Integer pageNo,
                                                     Integer pageSize,
                                                     String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        // get all merge requests from repository in a paged manner
        Page<Repository> pagedResult = this.repositoryRepository.findAll(paging);
        // to change page, add e.g. "?pageSize=5&pageNo=1" at end of URL
        if (pagedResult.hasContent()) {
            return pagedResult.getContent();
        }

        return new ArrayList<>();
    }

    /**
     * Returns the number of total pages.
     *
     * @param pageNo            Number of first page.
     * @param pageSize          Amount of elements per page.
     * @param sortBy            Value to sort by.
     * @return                  Paged list of repositories.
     */
    public int getTotalPages(Integer pageNo,
                              Integer pageSize,
                              String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        // get all merge requests from repository in a paged manner
        Page<Repository> pagedResult = this.repositoryRepository.findAll(paging);

        return pagedResult.getTotalPages();
    }

    /**
     * Checks if there are new repositories in GitLab that are not in the database
     * and saves the new ones.
     *
     * @throws                        GitLabApiException if there is a problem with
     *                                the GitLabApiWrapper.
     */
    public void checkAndStoreNewRepositories() throws GitLabApiException {
        List<Repository> repositories = this.repositoryRepository.findAll();
        List<Project> projects = this.gitLabApiWrapper.getGitLabApi().getProjectApi().getProjects();

        // Create a set with all the IDs of the repositories in the DB.
        Set<Integer> ids = repositories.stream()
                .map(Repository::getGitLabId)
                .collect(Collectors.toSet());

        // Save every project from GitLab that does not have a corresponding ID in the DB.
        List<Project> nonPres = projects.stream()
                .filter(project -> !ids.contains(project.getId()))
                .collect(Collectors.toList());

        // Save projects that are not present in the DB.
        nonPres.forEach(s -> {
            Repository repository = new Repository();

            repository.setGitLabId(s.getId());
            repository.setHttpUrl(s.getHttpUrlToRepo());
            repository.setSshUrl(s.getSshUrlToRepo());
            repository.setName(s.getName());
            repository.setNamespace(s.getNamespace().getName());
            repository.setPathWithNamespace(s.getPathWithNamespace());
            repository.setIsEnabled(false);

            this.repositoryRepository.save(repository);
            this.copyOverDefaultRobotSettings(repository);
            this.copyOverDefaultMetricSettings(repository);
        });
    }

    /**
     * Helper method that copies the default settings and sets them as the
     * settings of a specific repository.
     *
     * @param repository the {@link Repository} for which the settings are to be copied.
     */
    public void copyOverDefaultRobotSettings(Repository repository) {
        // Get the settings of the repository.
        // This is done to see if this repository already has a settings object linked to it.
        // In case that it has settings linked to it, stop.
        Optional<RobotSettings> optionalSettings = this.robotSettingsRepository
                .findByRepositoryId(repository.getId());

        if (optionalSettings.isPresent()) {
            return;
        }

        // Fetch the default settings from the database.
        optionalSettings = this.robotSettingsRepository.getDefault();

        if (optionalSettings.isEmpty()) {
            throw new IllegalStateException("No default settings found!");
        }

        // Copy the default settings over to a new robot settings object.
        // Therefore, keep the information but lose the Repository and ID because these are unique.
        RobotSettings settings = new RobotSettings(optionalSettings.get());
        settings.setRepository(repository);

        this.robotSettingsRepository.save(settings);
    }

    /**
     * Copies the default metric settings and links them to the repository.
     * Ignoring the metric settings that have already been copied.
     *
     * @param repository The repository that should be linked to the new settings.
     */
    public void copyOverDefaultMetricSettings(Repository repository) {
        // get all default settings
        List<MetricSettings> defaultSettings = MetricModelHelper.getAllDefaultMetricSettings();
        // get the settings arleady set for this repository (can be empty list)
        List<MetricSettings> repositorySettings = MetricModelHelper
                .getAllMetricSettingsOfRepo(repository.getId());

        for (MetricSettings defaultSetting : defaultSettings) { // process every default setting
            // check if this default setting has already been set
            boolean settingHasBeenSet = repositorySettings.stream()
                    .anyMatch(repositorySetting ->
                        repositorySetting.getMetric().getId()
                            .equals(defaultSetting.getMetric().getId())
                    );

            if (settingHasBeenSet) {
                continue;
            }

            // this is a default setting not set for this repository
            // clone the default setting
            MetricSettings clone = new MetricSettings(defaultSetting);
            // link it to the repository
            clone.setRepository(repository);
            // save it in the DB
            this.metricSettingsRepository.save(clone);
        }
    }

    /**
     * Find a {@link Repository} by a GitlabID.
     *
     * @param gitLabID                  The GitLab ID to use for the search.
     * @throws IllegalStateException    When the {@link Repository} cannot be found.
     * @return                          The {@link Repository} object linked
     *                                  to the {@code gitLabID}.
     */
    public Repository findRepositoryByGitLabId(Integer gitLabID) throws IllegalStateException {
        return this.repositoryRepository.findByGitLabId(
                gitLabID
        ).orElseThrow(() -> {
            String msg = "RepositoryHelper.findRepositoryByGitLabId: Cannot find "
                    + " repository with gitlab ID " + gitLabID;

            return new IllegalStateException(msg);
        });
    }
}
