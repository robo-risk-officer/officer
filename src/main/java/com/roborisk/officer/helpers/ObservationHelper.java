package com.roborisk.officer.helpers;

import com.roborisk.officer.models.database.BranchClassObservation;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.BranchFunctionObservation;
import com.roborisk.officer.models.database.MergeRequestClassObservation;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestFunctionObservation;
import com.roborisk.officer.models.repositories.BranchClassObservationRepository;
import com.roborisk.officer.models.repositories.BranchFunctionObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestClassObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestFunctionObservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Helper for copying observations from branch level to merge request level.
 */
@Component
public class ObservationHelper {

    /**
     * Database repository to handle {@link BranchFunctionObservation} objects.
     */
    @Autowired
    private BranchFunctionObservationRepository branchFunctionObservationRepository;

    /**
     * Database repository to handle {@link MergeRequestFunctionObservation} objects.
     */
    @Autowired
    private MergeRequestFunctionObservationRepository mergeRequestFunctionObservationRepository;

    /**
     * Database repository to handle {@link BranchFunctionObservation} objects.
     */
    @Autowired
    private BranchClassObservationRepository branchClassObservationRepository;

    /**
     * Database repository to handle {@link MergeRequestFunctionObservation} objects.
     */
    @Autowired
    private MergeRequestClassObservationRepository mergeRequestClassObservationRepository;

    /**
     * Copy branch function observations to merge request function observations.
     *
     * @param branchFile The branch file whose observations to copy.
     * @param mrFile     The merge request file whose observations to copy to.
     */
    public void copyFunctionObservations(BranchFile branchFile, MergeRequestFile mrFile) {

        List<BranchFunctionObservation> functionObservations =
                this.branchFunctionObservationRepository.findAllByBranchFile(branchFile);

        functionObservations.forEach(o -> {
            boolean doesntExist = this.mergeRequestFunctionObservationRepository
                    .findByMergeRequestFileAndClassNameAndFunctionName(
                        mrFile,
                        o.getClassName(),
                        o.getFunctionName())
                    .isEmpty();

            if (doesntExist) {
                this.mergeRequestFunctionObservationRepository.save(
                        new MergeRequestFunctionObservation(o, mrFile));
            }
        });
    }

    /**
     * Copy branch class observations to merge request function observations.
     *
     * @param branchFile The branch file whose observations to copy.
     * @param mrFile     The merge request file whose observations to copy to.
     */
    public void copyClassObservations(BranchFile branchFile, MergeRequestFile mrFile) {
        List<BranchClassObservation> classObservations =
                this.branchClassObservationRepository.findAllByBranchFile(branchFile);

        classObservations.forEach(o -> {
            boolean doesntExist = this.mergeRequestClassObservationRepository
                    .findByMergeRequestFileAndClassName(
                        mrFile,
                        o.getClassName())
                    .isEmpty();

            if (doesntExist) {
                this.mergeRequestClassObservationRepository.save(
                        new MergeRequestClassObservation(o, mrFile)
                );
            }
        });
    }
}
