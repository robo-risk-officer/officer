package com.roborisk.officer.helpers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.LinkedHashMap;

/**
 * JsonConverter helps with converting json objects to java objects.
 */
public class JsonConverter {

    /**
     * Converts a LinkedHashMap object representing a json to a java object of choice using spring.
     *
     * @param toConvert     LinkedHashMap representing the json.
     * @param objectClass   class to be converted to.
     * @param <T>           Generic Type class.
     * @throws IOException  If json cannot be parsed to objectClass by spring.
     * @return              Object of class objectClass containing the data in
     *                          the json parsed by spring.
     */
    public static <T> T convertFromMap(
            LinkedHashMap<String, Object> toConvert, Class<T> objectClass) throws IOException {
        try {
            // Initialize object mapper
            ObjectMapper om = new ObjectMapper();
            om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            // Transpose Map to String for jackson
            String jsonString = om.writerWithDefaultPrettyPrinter()
                    .writeValueAsString(toConvert);

            // Generate object of objectClass from jsonString and return
            return om.readerFor(objectClass).readValue(jsonString);
        } catch (IOException e) {
            String error = String.format("The JSON could not be parsed %s, %s. Full error :%s",
                    toConvert.toString(), objectClass.toString(), e.toString());
            throw new IOException(error);
        }
    }
}
