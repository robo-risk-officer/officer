package com.roborisk.officer.helpers;

import com.roborisk.officer.models.database.AbstractSettings;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.RobotSettings;
import com.roborisk.officer.models.repositories.MetricSettingsRepository;
import com.roborisk.officer.models.repositories.RobotSettingsRepository;
import com.roborisk.officer.models.web.MetricSettingsWeb;
import com.roborisk.officer.models.web.RobotSettingsWeb;
import com.roborisk.officer.models.web.SettingsApiData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Provides various helper functions to extract or update Settings information
 * given an {@link SettingsApiData} instance.
 */
@Component
public class SettingsApiDataHelper {

    /**
     * Database repository to handle {@link RobotSettings} objects.
     */
    private static RobotSettingsRepository robotSettingsRepository;

    /**
     * Database repository to handle {@link MetricSettings} objects.
     */
    private static MetricSettingsRepository metricSettingsRepository;

    /**
     * Setting the static robotSettingsRepository as this can not be done directly by an
     * autowire. This function is automatically called by Spring.
     *
     * @param robotSettingsRepository The {@link RobotSettingsRepository} which is set as
     *                                the static object (automatically supplied by spring)
     */
    @Autowired
    public void setRobotSettingsRepository(RobotSettingsRepository robotSettingsRepository) {
        SettingsApiDataHelper.robotSettingsRepository = robotSettingsRepository;
    }

    /**
     * Setting the static metricSettingsRepository as this can not be done directly by an
     * autowire. This function is automatically called by Spring.
     *
     * @param metricSettingsRepository The {@link MetricSettingsRepository} which is set as
     *                                the static object (automatically supplied by spring)
     */
    @Autowired
    public void setMetricSettingsRepository(MetricSettingsRepository metricSettingsRepository) {
        SettingsApiDataHelper.metricSettingsRepository = metricSettingsRepository;
    }

    /**
     * Given Api data, provides the current representation of the metric settings in the database.
     *
     * @param data                   data provided by the api.
     * @param repositoryId           the id of the repository the settings are associated to.
     * @throws IllegalStateException if metric is not associated with the database.
     * @return                       mapping of id's to the metric settings db model.
     */
    public static Map<Integer, MetricSettings> getMetricSettings(SettingsApiData data,
                                                              Integer repositoryId)
            throws IllegalStateException {
        HashMap<Integer, MetricSettings> allMetricSettings = new HashMap<>();

        for (MetricSettingsWeb newMetricSettingWeb : data.getMetricSettings()) {
            MetricSettings databaseMetricSettings =
                    (MetricSettings) SettingsApiDataHelper.getSettingsFromDatabase(
                        SettingsApiDataHelper.metricSettingsRepository
                            .findById(newMetricSettingWeb.getId()), repositoryId
                    );

            allMetricSettings.put(newMetricSettingWeb.getId(), databaseMetricSettings);
        }

        return allMetricSettings;
    }

    /**
     * Given Api data, provides the current representation of the robot settings in the
     * database.
     *
     * @param data                      data provided by the api.
     * @param repositoryId              the id of the repository the settings are associated to.
     * @throws IllegalStateException    if provided settings are not associate with the repository.
     * @return {@link RobotSettings}    the appropriate robot setting database model associated.
     */
    public static Optional<RobotSettings> getRobotSettings(SettingsApiData data,
                                                           Integer repositoryId)
            throws IllegalStateException {
        // fetch the corresponding database objects and check if the update is valid
        RobotSettingsWeb newRobotSettingsWeb = data.getRobotSettings();
        RobotSettings databaseRobotSettings = null;

        if (newRobotSettingsWeb != null) {
            databaseRobotSettings =
                (RobotSettings) SettingsApiDataHelper.getSettingsFromDatabase(
                    SettingsApiDataHelper.robotSettingsRepository
                        .findById(newRobotSettingsWeb.getId()), repositoryId
                );
        }

        return Optional.ofNullable(databaseRobotSettings);
    }

    /**
     * Given a repository, provide a web model representation of the data stored in the
     * database.
     *
     * @param repositoryId  repository with which the setting is associated.
     * @return              a list of web models
     *                      representing metric settings associated with the repository.
     */
    public static List<MetricSettingsWeb> getCurrentMetricSettingsAsWeb(Integer repositoryId) {
        // get all the metric settings associated to the repo
        Optional<List<MetricSettings>> metricSettingsList =
                SettingsApiDataHelper.metricSettingsRepository.findByRepositoryId(repositoryId);

        List<MetricSettingsWeb> allMetricSettingsWeb = new ArrayList<>();

        if (metricSettingsList.isPresent()) {
            for (MetricSettings metricSettings : metricSettingsList.get()) {
                allMetricSettingsWeb.add(metricSettings.getWebObject());
            }
        }

        return allMetricSettingsWeb;
    }

    /**
     * Given a repository, provide a web model representation of the data stored in the
     * database.
     *
     * @param repositoryId  repository with which the setting is associated.
     * @return              null if no robot setting was associated with the repository,
     *                      appropriate web model otherwise.
     */
    public static Optional<RobotSettingsWeb> getCurrentRobotSettingsAsWeb(Integer repositoryId) {
        // get the robotSettings associated to the repository
        Optional<RobotSettings> robotSettingsOptional =
                SettingsApiDataHelper.robotSettingsRepository.findByRepositoryId(repositoryId);

        if (robotSettingsOptional.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(robotSettingsOptional.get().toWebObject());
    }

    /**
     * Given an web model and database model, the database model is updated using the web
     * model. This changes the {@link RobotSettings} object databaseRobotSettings.
     *
     * @param newRobotSettingsWeb       the new robot settings that need to be adopted.
     * @param databaseRobotSettings     the current database model that should be updated.
     */
    public static void updateRobotSettings(RobotSettingsWeb newRobotSettingsWeb,
                                    RobotSettings databaseRobotSettings) {
        databaseRobotSettings.setAllFromRobotSettingsWeb(newRobotSettingsWeb);
        SettingsApiDataHelper.robotSettingsRepository.save(databaseRobotSettings);
    }

    /**
     * Given an web model and database model, the database model is updated using the web
     * model. This changes the {@link MetricSettings} objects specified in currentSettings.
     *
     * @param newSettings       list of new settings, as web models, that should be adopted.
     * @param currentSettings   map of current database models as values, and their id's as keys.
     */
    public static void updateMetricSettings(List<MetricSettingsWeb> newSettings,
            Map<Integer, MetricSettings> currentSettings) {

        for (MetricSettingsWeb newMetricSettingWeb : newSettings) {
            Integer settingId = newMetricSettingWeb.getId();
            MetricSettings databaseMetricSetting = currentSettings.get(settingId);

            databaseMetricSetting.setAllFromMetricSettingsWeb(newMetricSettingWeb);
            SettingsApiDataHelper.metricSettingsRepository.save(databaseMetricSetting);
        }
    }

    /**
     * Extracts a {@link AbstractSettings} object from the {@link Optional} and checks if it is
     * indeed specified for the give repositoryID.
     *
     * @param currentSettings The {@link Optional} containing the {@link AbstractSettings}
     *                     object to be checked.
     * @param repositoryId Id of the repository the {@link AbstractSettings} object should be
     *                     associated to.
     * @throws IllegalStateException if provided settings are not associate with the repository.
     * @return AbstractSettings returns the extracted {@link AbstractSettings} object or
     *                      null if there was a problem.
     */
    public static AbstractSettings getSettingsFromDatabase(
            Optional<? extends AbstractSettings> currentSettings,
            Integer repositoryId) throws IllegalStateException {
        // check if optional is valid
        if (currentSettings.isEmpty()) {
            throw new IllegalStateException("No abstract setting for repositoryId: "
                                                + repositoryId);
        }

        Repository repository = currentSettings.get().getRepository();

        // check if repository is valid
        if ((repository == null && repositoryId != null)
                || (repository != null && !repository.getId().equals(repositoryId))) {
            throw new IllegalStateException("No abstract setting for repositoryId: "
                                                + repositoryId);
        }

        return currentSettings.get();
    }
}
