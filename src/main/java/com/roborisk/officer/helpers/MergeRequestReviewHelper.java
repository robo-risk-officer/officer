package com.roborisk.officer.helpers;

import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.repositories.MergeRequestFileRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.web.MergeRequestWeb;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import lombok.extern.slf4j.Slf4j;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Discussion;
import org.gitlab4j.api.models.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * The MergeRequestReviewHelper implements a method to checks whether, given a
 * {@link com.roborisk.officer.models.web.CommentWebhookData},
 * a review has already been given on the merge request in the same state as it is now.
 */
@Slf4j
@Component
public class MergeRequestReviewHelper {

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link MergeRequestReview} objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Helper for the {@link MergeRequest} model.
     */
    @Autowired
    private MergeRequestHelper mergeRequestHelper;

    /**
     * Database repository to handle {@link MergeRequestFile} objects.
     */
    @Autowired
    private MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * Wrapper to interact with GitLab.
     */
    @Autowired
    private GitLabApiWrapper gitLabApiWrapper;

    /**
     * Find a {@link MergeRequestReview} object by the given {@link MergeRequest}.
     *
     * @param mergeRequest              The {@link MergeRequest} to use for the search.
     * @throws IllegalStateException    When the {@link MergeRequestReview} cannot be found.
     * @return                          The {@link MergeRequestReview} object linked to the
     *                                  {@link MergeRequest} object.
     */
    public MergeRequestReview findLastByMergeRequest(MergeRequest mergeRequest)
            throws IllegalStateException {
        return this.mergeRequestReviewRepository.findFirstByMergeRequestOrderByIdDesc(mergeRequest)
                .orElseThrow(() -> {
                    String msg = "MergeRequestReviewHelper.MergeRequestReview: "
                            + " Cannot find merge request review with merge request ID "
                            + mergeRequest.getId();

                    return new IllegalStateException(msg);
                });
    }

    /**
     * Finds latest {@link MergeRequestReview} given a web model of the merge request.
     *
     * @param  mergeRequestWeb   The web model equivalent to the database model.
     * @return                   The latest review of the merge request.
     */
    public MergeRequestReview findLastByMergeRequestWeb(MergeRequestWeb mergeRequestWeb) {
        // Find the merge request.
        MergeRequest mergeRequest = this.mergeRequestHelper.findMergeRequestByGitLabId(
                mergeRequestWeb.getGitLabId()
        );

        return this.findLastByMergeRequest(mergeRequest);
    }

    /**
     * Finds files of the latest merge request review.
     *
     * @param mergeRequestWeb The merge request in question.
     * @return                The files of the latest review for the merge request.
     */
    public List<MergeRequestFile> findFilesOfLatestMergeRequestReview(
            MergeRequestWeb mergeRequestWeb
    ) {
        MergeRequestReview review = this.findLastByMergeRequestWeb(mergeRequestWeb);

        return this.mergeRequestFileRepository.findAllByMergeRequestReviewId(review.getId());
    }

    /**
     * Checks whether any modifications have been made in the merge request.
     *
     * @param   mergeRequestWeb         The {@link MergeRequestWeb} object that the comment was
     *                                  placed on.
     * @param   repositoryGitLabId      The GitLab ID of the repository containing the merge
     *                                  request.
     * @param   commentGitLabId         The GitLab ID of the comment that was placed asking for a
     *                                  review.
     * @throws  IllegalStateException   If the commits of the merge request cannot be retrieved from
     *                                  GitLab.
     * @throws  IllegalStateException   If the comment cannot be posted to GitLab.
     * @return                          Whether the merge request does not contain any commits.
     */
    public boolean mrWithoutCommits(MergeRequestWeb mergeRequestWeb, Integer repositoryGitLabId,
                                    Integer commentGitLabId) throws IllegalStateException {

        boolean noCommits;
        try {
            noCommits = this.gitLabApiWrapper.getGitLabApi().getMergeRequestApi()
                    .getCommits(repositoryGitLabId, mergeRequestWeb.getGitLabIid()).isEmpty();
        } catch (GitLabApiException e) {
            String msg = "MergeRequestReviewHelper.mrWithoutCommits: Error in retrieving the "
                    + "commits associated to the merge request from GitLab - " + e.toString();

            throw new IllegalStateException(msg);
        }

        // Commits found, continue handling the request.
        if (!noCommits) {
            return false;
        }

        // Merge request does not contain any changes.
        // Post a comment to reflect as such.
        try {
            // Set comment contents.
            String message = "I can't review this merge request as no changes were made :worried:";

            // Get discussion ID from GitLab note ID.
            Optional<Discussion> optionalDiscussion = this.getDiscussionId(
                    repositoryGitLabId, mergeRequestWeb.getGitLabIid(), commentGitLabId);

            // If discussion can not be found, post the comment in a new discussion.
            if (optionalDiscussion.isEmpty()) {
                this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi()
                        .createMergeRequestDiscussion(repositoryGitLabId,
                        mergeRequestWeb.getGitLabIid(), message, null, null, null);

                return true;
            }

            // Otherwise, find the discussion and add a comment to this.
            Discussion discussion = optionalDiscussion.get();

            // Post comment
            this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi()
                    .addMergeRequestThreadNote(repositoryGitLabId, mergeRequestWeb.getGitLabIid(),
                        discussion.getId(), message, null);

            // Comment has been placed, stop handling the request.
            return true;
        } catch (GitLabApiException e) {
            String msg = "MergeRequestReviewHelper.mrWithoutCommits: Error in posting the note to "
                    + "GitLab - " + e.toString();

            throw new IllegalStateException(msg);
        }
    }

    /**
     * Checks whether a review on the same state of the merge request was already given.
     *
     * @param mergeRequestWeb   The {@link MergeRequestWeb} object that the comment was placed on.
     * @param commentGitLabId   The GitLab ID of the comment that was placed asking for a review.
     * @return                  Whether the robot already reviewed the merge request in the same
     *                          state.
     */
    public boolean reviewedMrOnSameState(MergeRequestWeb mergeRequestWeb, Integer commentGitLabId) {
        // If the merge request was already reviewed, then a corresponding merge request should
        // be in the database.
        Optional<MergeRequest> optionalMr = this.mergeRequestRepository
                .findByGitLabId(mergeRequestWeb.getGitLabId());

        // If it is not, then return false.
        if (optionalMr.isEmpty()) {
            log.info("Review Requested: As the merge request is not in the database yet, the "
                    + "request will continue to be handled.");

            return false;
        }

        // If the merge request was already reviewed, then a corresponding review should be in the
        // database.
        Optional<MergeRequestReview> optionalLatestReview = this.mergeRequestReviewRepository
                .findFirstByMergeRequestOrderByIdDesc(optionalMr.get());

        // If it is not, then return false.
        if (optionalLatestReview.isEmpty()) {
            log.info("Review Requested: There is no review in the database associated to the "
                    + "merge request, the request will continue to be handled");

            return false;
        }

        // Get the review from the Optional object.
        MergeRequestReview latestReview = optionalLatestReview.get();

        // If the hash of the commit of the review is not equal to the hash of the last commit of
        // the merge request, then the state is not the same so return false.
        if (!latestReview.getCommit().getHash().equals(mergeRequestWeb.getLastCommit().getHash())) {
            log.info("Review Requested: As the state of the merge request is different from the "
                    + "state in which is was last reviewed, the request will continue to be "
                    + "handled.");

            return false;
        }

        log.info("Review Requested: As the merge request was already reviewed in the same state "
                + "the merge request will not be reviewed again.");

        // Merge request was already reviewed in the same state, so post a comment stating this.
        try {
            // Set attributes
            String message = "This merge request was already reviewed in the current state "
                    + "that it is in.";
            int repoId = latestReview.getMergeRequest().getSourceBranch().getRepository()
                    .getGitLabId();
            int mrIid = latestReview.getMergeRequest().getGitLabIid();

            // Get discussion ID from GitLab note ID
            Optional<Discussion> optionalDiscussion = this.getDiscussionId(
                    repoId, mrIid, commentGitLabId);

            // If discussion can not be found, then just post a comment on the merge request.
            if (optionalDiscussion.isEmpty()) {
                this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi()
                        .createMergeRequestDiscussion(repoId, mrIid, message,
                            null, null, null);

                return true;
            }

            // Otherwise, find the discussion and add a comment to this.
            Discussion discussion = optionalDiscussion.get();

            // Post comment
            this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi()
                    .addMergeRequestThreadNote(repoId, mrIid,
                        discussion.getId(), message,
                        null); // set to null to use current time as postedAt time

        } catch (GitLabApiException e) {
            // If the discussion cannot be found and a new discussion can also not be created
            // just return true.
        }

        return true;
    }

    /**
     * Uses the GitLab4j API to get the ID of the discussion thread of the comment
     * that triggered the review.
     *
     * @param   repositoryGitLabId      The GitLab ID of the containing repository.
     * @param   mergeRequestGitLabIid   The GitLab IID of the merge request in question.
     * @param   gitLabNoteId            The GitLab id of the comment that was placed.
     * @throws  GitLabApiException      If the GitLab4j API throws an error or when the id cannot be
     *                                  found.
     * @return                          An {@link Optional} of the {@link Discussion} on the
     *                                  {@link MergeRequest} that the comment is a part of.
     */
    private Optional<Discussion> getDiscussionId(Integer repositoryGitLabId,
                                                 Integer mergeRequestGitLabIid,
                                                 Integer gitLabNoteId) throws GitLabApiException {
        // Get all discussions on the merge request.
        List<Discussion> discussionsOnMr = this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi()
                .getMergeRequestDiscussions(
                    repositoryGitLabId,
                    mergeRequestGitLabIid
            );

        // For each discussion, check if it contains a note whose GitLab id matches that of the
        // comment.
        for (Discussion discussion : discussionsOnMr) {
            for (Note note : discussion.getNotes()) {
                if (note.getId().equals(gitLabNoteId)) {
                    return Optional.of(discussion);
                }
            }
        }

        return Optional.empty();
    }
}
