package com.roborisk.officer.helpers;

import com.roborisk.officer.models.database.DiscussionThread;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestClassObservation;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestFunctionObservation;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.RobotNote;
import com.roborisk.officer.models.repositories.DiscussionThreadRepository;
import com.roborisk.officer.models.repositories.MergeRequestClassObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestFileRepository;
import com.roborisk.officer.models.repositories.MergeRequestFunctionObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.repositories.RobotNoteRepository;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Discussion;
import org.gitlab4j.api.models.FileUpload;
import org.gitlab4j.api.models.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;

/**
 * Helper class that abstracts from the gitLabApiWrapper to simplify the posting of comments by
 * extracting the right information from database objects to post the wanted comments.
 */
@Component
@Scope("singleton")
@SuppressWarnings({"ClassFanOutComplexity"})
public class CommentHelper {

    /**
     * GitLabApiWrapper for making calls to GitLab to get the awards.
     */
    @Autowired
    private GitLabApiWrapper gitLabApiWrapper;

    /**
     * Database repository to handle {@link RobotNote} objects.
     */
    @Autowired
    private RobotNoteRepository robotNoteRepository;

    /**
     * Database repository to handle {@link DiscussionThread} objects.
     */
    @Autowired
    private DiscussionThreadRepository discussionThreadRepository;

    /**
     * Database repository to handle {@link MergeRequestReview} objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Database repository to handle {@link MergeRequestFile} objects.
     */
    @Autowired
    private MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * Database repository to handle {@link MergeRequestClassObservation} objects.
     */
    @Autowired
    private MergeRequestClassObservationRepository mergeRequestClassObservationRepository;

    /**
     * Database repository to handle {@link MergeRequestFunctionObservation} objects.
     */
    @Autowired
    private MergeRequestFunctionObservationRepository mergeRequestFunctionObservationRepository;

    /**
     * Empty autowired constructor to make spring realize this exists.
     */
    @Autowired
    public CommentHelper() { }

    /**
     * Create a GitLab merge request thread utilizing the {@link GitLabApiWrapper}.
     *
     * @param mergeRequest          The {@link MergeRequest} on which the thread should be left.
     * @param body                  The content of the first note in the thread.
     * @throws GitLabApiException   If any exception occurs within the {@link GitLabApiWrapper}.
     * @return                      The created {@link DiscussionThread} instance which is
     *                              associated to the created {@link DiscussionThread} instance.
     */
    private DiscussionThread postThread(MergeRequest mergeRequest, String body)
            throws GitLabApiException {
        int repoId = mergeRequest.getSourceBranch().getRepository().getGitLabId();
        int mrIid = mergeRequest.getGitLabIid();

        // create the discussion thread
        Discussion discussion = this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi()
                .createMergeRequestDiscussion(repoId, mrIid, body,
                    /*
                    The following values are needed when posting on a specific line. As we are
                    currently not supporting that, we can put them to null to just comment on a MR.
                     */
                    null, null, null);

        // Create the discussion thread in the DB
        DiscussionThread discussionThread = new DiscussionThread();
        discussionThread.setMergeRequest(mergeRequest);
        discussionThread.setGitLabId(discussion.getId());
        discussionThread.setIsResolved(discussion.getNotes().get(0).getResolved());
        this.discussionThreadRepository.save(discussionThread);

        // Create the note in the DB
        RobotNote robotNote = new RobotNote();
        robotNote.setGitLabId(discussion.getNotes().get(0).getId());
        robotNote.setDiscussionThread(discussionThread);
        this.robotNoteRepository.save(robotNote);

        return discussionThread;
    }

    /**
     * Creates a GitLab merge request thread utilizing the {@link GitLabApiWrapper} and posts a
     * note. This is different from the postThread method because it does not open a discussion
     * but only posts a note.
     *
     * @param mergeRequest          The {@link MergeRequest} on which the thread should be left.
     * @param body                  The content of the first note in the thread.
     * @throws GitLabApiException   If any exception occurs within the {@link GitLabApiWrapper}.
     * @return                      The created {@link DiscussionThread} instance which is
     *                              associated to the created {@link DiscussionThread} instance.
     */
    private DiscussionThread postNote(MergeRequest mergeRequest, String body)
            throws GitLabApiException {
        int repoId = mergeRequest.getSourceBranch().getRepository().getGitLabId();
        int mrIid = mergeRequest.getGitLabIid();

        // Create the note.
        Note note = this.gitLabApiWrapper.getGitLabApi().getNotesApi()
                .createMergeRequestNote(repoId, mrIid, body);

        // Query for all discussions to find the newly created discussion of the note.
        List<Discussion> discussions =
                this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi()
                    .getMergeRequestDiscussions(repoId, mrIid);

        // Go through all discussion and find the one that was just newly created.
        DiscussionThread discussionThread = new DiscussionThread();
        discussionThread.setMergeRequest(mergeRequest);
        discussionThread.setIsResolved(false);

        for (Discussion discussion : discussions) {
            boolean exists = discussion.getNotes().stream()
                    .anyMatch(n -> n.getId().equals(note.getId()));

            if (!exists) {
                continue;
            }

            discussionThread.setGitLabId(discussion.getId());
            discussionThread.setIsResolved(
                    note.getResolved() == null ? false : note.getResolved()
            );

            break;
        }

        // Generate and return {@link RobotNote} based on returned data.
        RobotNote robotNote = new RobotNote();
        robotNote.setGitLabId(note.getId());
        robotNote.setDiscussionThread(discussionThread);

        // Store discussion thread.
        this.discussionThreadRepository.save(discussionThread);

        // Store the note.
        this.robotNoteRepository.save(robotNote);

        return discussionThread;
    }

    /**
     * Add a note to a thread on a GitLab merge request utilizing the {@link GitLabApiWrapper}.
     * This can either be used when linking the result directly to the reviewed artifact but may
     * also be used for comments which are not linked to direct reviews, such as commenting upon
     * closing threads marked as false positives.
     *
     * @param discussionThread      The {@link DiscussionThread} on which the note should be left.
     * @param body                  The content of note.
     * @throws GitLabApiException   If any exception occurs within the {@link GitLabApiWrapper}.
     * @return                      The created {@link RobotNote} instance.
     */
    public RobotNote addNoteToThread(DiscussionThread discussionThread, String body)
            throws GitLabApiException {
        int repoId = discussionThread.getMergeRequest().getSourceBranch()
                .getRepository().getGitLabId();
        Note note = this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi()
                .addMergeRequestThreadNote(repoId,
                    discussionThread.getMergeRequest().getGitLabIid(),
                    discussionThread.getGitLabId(), body,
                    /*
                    The following value can be used to decide what data of creating the note
                    should display. If it is null, the current time is used.
                     */
                    null
            );

        // Generate and return {@link RobotNote} based on returned data.
        RobotNote robotNote = new RobotNote();
        robotNote.setGitLabId(note.getId());
        robotNote.setDiscussionThread(discussionThread);

        // Store the note.
        this.robotNoteRepository.save(robotNote);

        return robotNote;
    }

    /**
     * Add a note to a {@link MergeRequestReview}. If it is already associated to a
     * {@link DiscussionThread}, add a note to that thread, otherwise create a new one.
     * This is for codebase reviews and general conclusions and thus has to be associated to a
     * {@link MergeRequestReview}. The new {@link RobotNote} and {@link DiscussionThread}
     * will be created, saved and associated to the {@link MergeRequestReview}.
     *
     * @param mergeRequestReview    The {@link MergeRequestReview} on which the note should be left.
     * @param body                  The content of the note.
     * @throws GitLabApiException   If any exception occurs within the {@link GitLabApiWrapper}.
     */
    public void postNoteForMergeRequestReview(MergeRequestReview mergeRequestReview, String body)
            throws GitLabApiException {
        if (mergeRequestReview.getDiscussionThread() == null) {
            DiscussionThread thread = this.postThread(mergeRequestReview.getMergeRequest(), body);

            mergeRequestReview.setDiscussionThread(thread);
            this.mergeRequestReviewRepository.save(mergeRequestReview);

            return;
        }

        this.addNoteToThread(
                mergeRequestReview.getDiscussionThread(),
                body
        );
    }

    /**
     * Function to post a conclusion note, which contains the final conclusion of a review. This
     * is stored in the mergeRequestReview.conclusionThread which is different from the
     * mergeRequestReview.discussionThread where the latter stores the result of the CODEBASE
     * metrics.
     *
     * @param mergeRequestReview    {@link MergeRequestReview} that the conclusion is for.
     * @param body                  {@link String} comment to be placed.
     * @throws GitLabApiException   If any exception occurs within the {@link GitLabApiWrapper}.
     */
    public void postConclusionForMergeRequestReview(MergeRequestReview mergeRequestReview,
                                                    String body)
            throws GitLabApiException {
        if (mergeRequestReview.getConclusionThread() != null) {
            throw new IllegalArgumentException("There has already been a conclusion on this merge"
                    + " request review, which is not allowed.");
        }

        DiscussionThread thread = this.postNote(mergeRequestReview.getMergeRequest(), body);

        mergeRequestReview.setConclusionThread(thread);
        this.mergeRequestReviewRepository.save(mergeRequestReview);
    }

    /**
     * Add a note to a {@link MergeRequestFile}. If it is already associated to a
     * {@link DiscussionThread}, add a note to that thread, otherwise create a new one.
     * This is for file specific reviews and thus has to be associated to a
     * {@link MergeRequestFile}. The new {@link RobotNote} and {@link DiscussionThread}
     * will be created, saved and associated to the {@link MergeRequestFile}.
     *
     * @param mergeRequestFile      The {@link MergeRequestFile} on which the note should be left.
     * @param body                  The content of the note.
     * @throws GitLabApiException   If any exception occurs within the {@link GitLabApiWrapper}.
     */
    public void postNoteForMergeRequestFile(MergeRequestFile mergeRequestFile, String body)
            throws GitLabApiException {
        if (mergeRequestFile.getDiscussionThread() == null) {
            DiscussionThread thread = this.postThread(
                    mergeRequestFile.getMergeRequestReview().getMergeRequest(), body
            );

            mergeRequestFile.setDiscussionThread(thread);
            this.mergeRequestFileRepository.save(mergeRequestFile);

            return;
        }

        this.addNoteToThread(mergeRequestFile.getDiscussionThread(), body);
    }

    /**
     * Add a note to a {@link MergeRequestClassObservation}. If it is already associated to a
     * {@link DiscussionThread}, add a note to that thread, otherwise create a new one.
     * This is for class specific reviews and thus has to be associated to a
     * {@link MergeRequestClassObservation}. The new {@link RobotNote} and {@link DiscussionThread}
     * will be created, saved and associated to the {@link MergeRequestClassObservation}.
     *
     * @param mergeRequestClassObservation  The {@link MergeRequestClassObservation} on which the
     *                                      note should be left.
     * @param body                          The content of the note.
     * @throws GitLabApiException           If any exception occurs within the
     *                                      {@link GitLabApiWrapper}.
     */
    public void postNoteForMergeRequestClassObservation(
            MergeRequestClassObservation mergeRequestClassObservation, String body)
            throws GitLabApiException {
        if (mergeRequestClassObservation.getDiscussionThread() == null) {

            DiscussionThread thread = this.postThread(
                    mergeRequestClassObservation.getMergeRequestFile()
                        .getMergeRequestReview().getMergeRequest(), body
            );

            mergeRequestClassObservation.setDiscussionThread(thread);
            this.mergeRequestClassObservationRepository.save(mergeRequestClassObservation);

            return;
        }

        this.addNoteToThread(mergeRequestClassObservation.getDiscussionThread(), body);
    }

    /**
     * Add a note to a {@link MergeRequestFunctionObservation}. If it is already associated to a
     * {@link DiscussionThread}, add a note to that thread, otherwise create a new one.
     * This is for function specific reviews and thus has to be associated to a
     * {@link MergeRequestFunctionObservation}. The new {@link RobotNote} and
     * {@link DiscussionThread} will be created, saved and associated to the
     * {@link MergeRequestFunctionObservation}.
     *
     * @param mergeRequestFunctionObservation   The {@link MergeRequestFunctionObservation} on which
     *                                          the note should be left.
     * @param body                              The content of the note.
     * @throws GitLabApiException               If any exception occurs within the
     *                                          {@link GitLabApiWrapper}.
     */
    public void postNoteForMergeRequestFunctionObservation(
            MergeRequestFunctionObservation mergeRequestFunctionObservation, String body)
            throws GitLabApiException {
        if (mergeRequestFunctionObservation.getDiscussionThread() == null) {
            DiscussionThread thread = this.postThread(
                    mergeRequestFunctionObservation.getMergeRequestFile()
                            .getMergeRequestReview().getMergeRequest(), body
            );

            mergeRequestFunctionObservation.setDiscussionThread(thread);
            this.mergeRequestFunctionObservationRepository.save(mergeRequestFunctionObservation);

            return;
        }

        this.addNoteToThread(mergeRequestFunctionObservation.getDiscussionThread(), body);
    }

    /**
     * Modify the first note on an existing GitLab discussion thread using the GitLab API.
     *
     * @param discussionThread      The {@link DiscussionThread} in which a note should be
     *                              modified.
     * @param body                  The body that is supposed to be placed in the note.
     * @throws GitLabApiException   If any exception occurs within the {@link GitLabApiWrapper}.
     */
    public void modifyDiscussionThread(DiscussionThread discussionThread,
                                       String body) throws GitLabApiException {
        this.modifyDiscussionThread(discussionThread, null, body);
    }

    /**
     * Modify an existing note on a merge request using the GitLab API.
     *
     * @param discussionThread      The {@link DiscussionThread} in which a note should be
     *                              modified.
     * @param note                  The {@link RobotNote} which should be modified. If null the
     *                              very first note in the thread is going to be used.
     * @param body                  The body that is supposed to be placed in the note.
     * @throws GitLabApiException   If any exception occurs within the {@link GitLabApiWrapper}.
     */
    public void modifyDiscussionThread(DiscussionThread discussionThread, RobotNote note,
            String body) throws GitLabApiException {
        int repoId =
                discussionThread.getMergeRequest().getSourceBranch().getRepository().getGitLabId();
        int mrIid = discussionThread.getMergeRequest().getGitLabIid();

        RobotNote checkedNote = note;
        if (note == null) {
            checkedNote = this.robotNoteRepository
                    .findFirstByDiscussionThreadIdOrderByIdAsc(discussionThread.getId()).get();
        }

        // modify the existing discussion thread
        this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi().modifyMergeRequestThreadNote(
                    repoId,
                    mrIid,
                    discussionThread.getGitLabId(),
                    checkedNote.getGitLabId(),
                    body,
                    /*this function allows either to update the body or set the resolved state.
                     One of the two has to be null. As we are here updating the body, the
                     resolved has to be null.
                     */
                    null
        );
    }

    /**
     * Upload a file to GitLab and return the FileUpload object.
     *
     * @param gitLabId              Id of the GitLab project to which the file should be uploaded.
     * @param file                  The file which should be uploaded.
     * @throws GitLabApiException   If any exception occurs within the {@link GitLabApiWrapper}.
     * @return                      The returned UploadFile.
     */
    public FileUpload uploadImage(int gitLabId, File file) throws GitLabApiException {
        FileUpload upload =
                this.gitLabApiWrapper.getGitLabApi().getProjectApi().uploadFile(
                    gitLabId, file
                );

        return upload;
    }
}
