package com.roborisk.officer.scheduler;

import com.roborisk.officer.helpers.AwardScraperHelper;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.RobotSettings;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.RobotSettingsRepository;
import lombok.extern.slf4j.Slf4j;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * This scheduled tasks scrapes the awards of all merge requests that the robo risk officer
 * is currently analysing and checks for false positives.
 */
@Slf4j
@Component
public class AwardScraperScheduledTask {

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Helper class for scraping awards.
     */
    @Autowired
    private AwardScraperHelper awardScraperHelper;

    /**
     * Database repository to handle {@link RobotSettings} objects.
     */
    @Autowired
    private RobotSettingsRepository robotSettingsRepository;

    /**
     * The state of the merge requests to check for.
     */
    private static final String MERGE_REQUEST_STATE = "opened";

    /**
     * Scrape all false positives every 60 seconds (60000 ms).
     */
    @Scheduled(fixedRate = 60000)
    private void scrapeAwards() throws InterruptedException {
        log.info("Starting scheduled task for scraping awards.");

        // Fetch all open merge requests from the database
        List<MergeRequest> openMrs = this.mergeRequestRepository.findAllByState(
                AwardScraperScheduledTask.MERGE_REQUEST_STATE
        );

        for (MergeRequest mergeRequest : openMrs) {
            log.info("Running for MR " + mergeRequest.getId());

            // Collect the settings for this repository
            int repoId = mergeRequest.getSourceBranch().getRepository().getId();
            RobotSettings settings = this.robotSettingsRepository
                    .findByRepositoryId(repoId).orElseGet(() ->
                        this.robotSettingsRepository.getDefault().orElseThrow(
                            () -> {
                                String msg = "AwardScraperScheduledTask.scrapeAwards: "
                                        + "could not retrieve robot settings";
                                throw new IllegalStateException(msg);
                            }
                        )
                    );

            try {
                this.awardScraperHelper.scrapeMergeRequest(
                        mergeRequest.getSourceBranch().getRepository().getGitLabId(),
                        mergeRequest.getGitLabIid(),
                        settings);
            } catch (GitLabApiException e) {
                String msg = "AwardScraperScheduledTask.scrapeAwards: Could not scrape awards "
                        + "from Merge Request with ID " + mergeRequest.getId() + " and update the "
                        + "Merge Request accordingly.";

                throw new IllegalStateException(msg);
            }

            Thread.sleep(100);
        }

        log.info("Finished scheduled task for scraping awards.");
    }
}
