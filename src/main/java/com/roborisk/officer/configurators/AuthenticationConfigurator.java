package com.roborisk.officer.configurators;

import org.apache.commons.lang3.ArrayUtils;

/**
 * This class contains lists defining patterns for routes that are only accessible to users with
 * certain roles.
 * The @link{AuthIntercept} uses these lists to determine which authentication level is needed
 * for intercepted requests.
 * All new routes added to the application should be defined in here to not be blocked.
 */
public class AuthenticationConfigurator {

    /**
     * Url patterns for all url routes that do not require oauth authentication.
     * Currently, a whitelist approach is used, meaning that a list of urls is defined that need to
     * go through the authentication system. This list is ignored for now, but can be useful if we
     * want to change to a blacklist approach, where everything except these urls is check on
     * authentication.
     */
    public static String[] noAuthRoutes = {
            "/",
            "/error",
            "/webhook/comments",
            "/webhook/merge-request",
            "/webhook/commit",
            "/webhook/sonar",
            "/oauth",
    };

    /*
     * Users with role guest, reporter, and developer role cannot access the API end points.
     * For this reason there are currently no arrays defining urls for these end points.
     * If these arrays are added, the AuthIntercept class also needs to be modified.
     */

    /**
     * Url patterns for all routes matching these strings need maintainer access.
     */
    public static String[] maintainerRoutes = {
            "/settings/default",
            "/settings/repository/{id:[0-9]+}",
            "/settings/repository/{id:[0-9]+}/toggle",
            "/repository/list",
            "/merge-request/list",
    };

    /**
     * Url patterns for all routes matching these strings need owner access.
     */
    public static String[] ownerRoutes = {
    };

    /**
     * ALl routes matching these patterns will go through the authentication intercept.
     */
    public static String[] authenticatedRoutes =
            ArrayUtils.addAll(maintainerRoutes, ownerRoutes);
}
