package com.roborisk.officer.configurators;

import com.roborisk.officer.interceptors.AuthIntercept;
import com.roborisk.officer.interceptors.EventHistoryIntercept;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Class defining the configuration of the Model-View-Controller (MVC) framework of Spring.
 */
@Configuration
public class InterceptorConfigurator implements WebMvcConfigurer {

    /**
     * Method to force AutoWired injection by making it Spring managed.
     *
     * @return EventHistoryIntercept that is Spring managed.
     */
    @Bean
    public EventHistoryIntercept getEventHistoryIntercept() {
        return new EventHistoryIntercept();
    }

    /**
     * Creates an authIntercept object managed by spring.
     *
     * @return An AuthIntercept object.
     */
    @Bean
    public AuthIntercept getAuthIntercept() {
        return new AuthIntercept();
    }

    /**
     * Method configuring the interceptors for the MVC framework.
     *
     * @param registry interceptor registry to which the interceptors are added.
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // Add the Event History Intercept.
        registry.addInterceptor(this.getEventHistoryIntercept());
        // Add the Auth intercept to routes that need authentication.
        registry.addInterceptor(this.getAuthIntercept())
                .addPathPatterns(AuthenticationConfigurator.authenticatedRoutes);
    }
}
