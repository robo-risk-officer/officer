package com.roborisk.officer.configurators;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Injects CORS headers for OAuth paths.
 */
@Configuration
public class CorsOAuthConfigurator implements WebMvcConfigurer {

    /**
     * allowedOrigins lists allowed origins to add CORS headers to as defined in the configuration.
     */
    @Value("${officer.cors.allowed-origins}")
    private String allowedOrigins;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // Allow OAuth routes.
        registry.addMapping("/oauth/**")
                .allowedOrigins(this.allowedOrigins)
                .allowedMethods("GET");

        // Allow repository routes.
        registry.addMapping("/repository/**")
                .allowedOrigins(this.allowedOrigins)
                .allowedMethods("GET")
                .exposedHeaders("X-Total-Pages");

        // Allow settings routes.
        registry.addMapping("/settings/**")
                .allowedOrigins(this.allowedOrigins)
                .allowedMethods("GET", "PUT");

        // Allow merge-request routes.
        registry.addMapping("/merge-request/**")
                .allowedOrigins(this.allowedOrigins)
                .allowedMethods("GET", "DELETE");
    }
}
