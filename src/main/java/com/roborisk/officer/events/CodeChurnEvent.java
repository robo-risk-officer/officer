package com.roborisk.officer.events;

import com.roborisk.officer.helpers.MetricModelHelper;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.metrics.BranchFileMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import com.roborisk.officer.modules.metrics.codechurn.CodeChurnComputerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * Event handling the code churn metric.
 *
 * <p>
 * The event will make sure to keep track
 * of the amount of the times a certain file has been touched in a commit on a certain branch.
 * The amount of times it will change will be stored as the score in a
 * {@link BranchFileMetricResult}.
 * </p>
 *
 * <p>
 * Dependencies:
 * - {@link CommitInitEvent}
 * - {@link RepositoryInitEvent}
 * - {@link BranchInitEventCommit}
 * </p>
 */
@Component
public class CodeChurnEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the event.
     */
    private static final int PRIORITY = 180;

    /**
     * The name of the File Code Churn metric used in the database.
     */
    public static final String METRIC_NAME_FILE = "file_code_churn";

    /**
     * The name of the Class Code Churn metric used in the database.
     */
    public static final String METRIC_NAME_CLASS = "class_code_churn";

    /**
     * The name of the Function Code Churn metric used in the database.
     */
    public static final String METRIC_NAME_FUNCTION = "function_code_churn";

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Link to the {@link CodeChurnComputerController} class.
     */
    @Autowired
    private CodeChurnComputerController codeChurnComputerController;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData webhook)
            throws IllegalStateException, IllegalArgumentException {
        // It only accept CommitWebhookData.
        if (!(webhook instanceof CommitWebhookData)) {
            String msg = "CodeChurnEvent.handle: Only supports CommitWebhookData";

            throw new IllegalArgumentException(msg);
        }

        // Cast to the right format.
        CommitWebhookData data = (CommitWebhookData) webhook;

        // Fetch repository, if it does not exist raise an exception.
        Repository repository = this.repositoryRepository.findByGitLabId(data.getRepositoryId())
                .orElseThrow(() -> {
                    String msg = "CodeChurnEvent.handle: Repository cannot be found with GitLabID "
                            + data.getRepositoryId();

                    return  new IllegalStateException(msg);
                });

        // Fetch metrics, or throw an error if they do not exist.
        Metric fileMetric = MetricModelHelper.getMetric(CodeChurnEvent.METRIC_NAME_FILE);
        Metric classMetric = MetricModelHelper.getMetric(CodeChurnEvent.METRIC_NAME_CLASS);
        Metric functionMetric = MetricModelHelper.getMetric(CodeChurnEvent.METRIC_NAME_FUNCTION);

        // Fetch the metric settings, if it does not exist create it (for now).
        MetricSettings fileSettings = MetricModelHelper.getMetricSettings(
                fileMetric.getId(), repository.getId()
            );

        MetricSettings classSettings = MetricModelHelper.getMetricSettings(
                classMetric.getId(), repository.getId()
            );

        MetricSettings functionSettings = MetricModelHelper.getMetricSettings(
                functionMetric.getId(), repository.getId()
            );

        // Fetch the branch, if it does not exist raise an exception.
        Branch branch = this.branchRepository.findByNameAndRepositoryId(
                data.getBranchName(), repository.getId()
        ).orElseThrow(() -> {
            String msg = "CodeChurnEvent.handle: Cannot find branch with name "
                    + data.getBranchName() + " and repository ID " + repository.getId();

            return new IllegalStateException(msg);
        });

        // Handle the code churn metrics.
        this.codeChurnComputerController.handle(branch, data, fileMetric, fileSettings,
                                                classMetric, classSettings,
                                                functionMetric, functionSettings);

        return new EmptyWebhookData();
    }

    @Override
    public int getPriority() {
        return CodeChurnEvent.PRIORITY;
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Collections.singletonList(EventType.PUSH_EVENT);
    }
}
