package com.roborisk.officer.events;

import com.roborisk.officer.helpers.BranchHelper;
import com.roborisk.officer.helpers.MergeRequestReviewHelper;
import com.roborisk.officer.helpers.ObservationHelper;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.BranchClassObservation;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.BranchFunctionObservation;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.MergeRequestFileRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * This event copies all branch artefacts to a merge request review. The branch artefacts are:
 *
 * <p>
 * - {@link BranchFile}
 * - {@link BranchFunctionObservation}
 * - {@link BranchClassObservation}
 * </p>
 *
 * <p>
 * Dependencies:
 * - {@link MergeRequestReviewInitEvent}
 * - {@link MergeRequestReview}
 * </p>
 */
@Component
public class CopyBranchArtefactEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the event.
     */
    private static final int PRIORITY = 180;

    /**
     * Database repository to handle {@link MergeRequestFile} objects.
     */
    @Autowired
    private MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * Helper for the {@link MergeRequestReview} model.
     */
    @Autowired
    private MergeRequestReviewHelper mergeRequestReviewHelper;

    /**
     * Helper for copying observations.
     */
    @Autowired
    private ObservationHelper observationHelper;

    /**
     * Helper for the {@link Branch} model.
     */
    @Autowired
    private BranchHelper branchHelper;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData webhook) {
        if (!(webhook instanceof CommentWebhookData)) {
            String msg = "CodeChurnCopyResultEvent.handle: Only CommentWebhookData is supported";
            throw new IllegalArgumentException(msg);
        }

        // Cast to the right instance.
        CommentWebhookData data = (CommentWebhookData) webhook;

        // Fetch the review that was created before.
        MergeRequestReview review = this.mergeRequestReviewHelper
                .findLastByMergeRequestWeb(data.getMergeRequest());

        // Get all files of this branch.
        List<BranchFile> files = this.branchHelper.findBranchFilesOfBranch(
                data.getMergeRequest().getSource(),
                data.getRepositoryId()
        );

        for (BranchFile file : files) {
            MergeRequestFile mrFile =
                    this.mergeRequestFileRepository.save(new MergeRequestFile(file, review));

            this.observationHelper.copyFunctionObservations(file, mrFile);
            this.observationHelper.copyClassObservations(file, mrFile);
        }

        return new EmptyWebhookData();
    }

    @Override
    public int getPriority() {
        return CopyBranchArtefactEvent.PRIORITY;
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Collections.singletonList(EventType.REVIEW_REQUESTED);
    }
}
