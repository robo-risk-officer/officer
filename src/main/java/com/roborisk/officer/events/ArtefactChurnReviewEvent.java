package com.roborisk.officer.events;

import com.roborisk.officer.helpers.BranchFileObservationHelper;
import com.roborisk.officer.helpers.translate.BranchClassObjectTranslationHelper;
import com.roborisk.officer.helpers.translate.BranchFunctionObjectTranslationHelper;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.BranchClassObservation;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.BranchFunctionObservation;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This event is used to place comments about artifact code churn on a {@link MergeRequest} when
 * a {@link MergeRequestReview} is requested.
 *
 * <p>
 * Effectively, the goal is to associate
 * {@link com.roborisk.officer.models.database.BranchClassObservation},
 * {@link com.roborisk.officer.models.database.BranchFunctionObservation},
 * {@link com.roborisk.officer.models.database.metrics.BranchClassMetricResult} and
 * {@link com.roborisk.officer.models.database.metrics.BranchFunctionMetricResult}
 * of the latest commit in the source branch of the merge request to these objects'
 * {@link MergeRequest} counterparts via a
 * {@link com.roborisk.officer.models.database.metrics.Metric} object and leave comments via
 * {@link com.roborisk.officer.models.database.DiscussionThread} objects.
 * </p>
 */
@Component
@SuppressWarnings({"ClassFanOutComplexity"})
// Fanout is 18 instead of 15.
public class ArtefactChurnReviewEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of this event.
     * It should run after the event that initialises the {@link MergeRequestReview} object.
     */
    private static final int PRIORITY = 150;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link MergeRequestReview} objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Database repository to handle {@link BranchFile} objects.
     */
    @Autowired
    private BranchFileRepository branchFileRepository;

    /**
     * Helper class to translate {@link BranchFunctionObservation} and their related objects.
     */
    @Autowired
    private BranchFunctionObjectTranslationHelper branchFunctionObjectTranslationHelper;

    /**
     * Helper class to translate {@link BranchClassObservation} and their related objects.
     */
    @Autowired
    private BranchClassObjectTranslationHelper branchClassObjectTranslationHelper;

    /**
     * Helper class to find the latest {@link BranchFunctionObservation} and
     * {@link BranchClassObservation} objects for {@link BranchFile} objects.
     */
    @Autowired
    private BranchFileObservationHelper branchFileObservationHelper;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData data) {
        // Only support CommentWebhookData as that is used by the webhook to request a review
        if (!(data instanceof CommentWebhookData)) {
            String msg = "ArtefactChurnReviewEvent.handle: Only CommentWebhookData is supported";

            throw new IllegalArgumentException(msg);
        }

        // Cast the data
        CommentWebhookData commentData = (CommentWebhookData) data;

        // Get the merge request object for the comment
        MergeRequest mergeRequest = this.mergeRequestRepository.findByGitLabId(
                commentData.getMergeRequest().getGitLabId()).orElseThrow(() -> {
                    throw new IllegalStateException("ArtefactChurnReviewEvent.handle: MergeRequest"
                            + " object does not exist in database");
                });

        // Find the source branch in the database (as that contains the newest artifact versions)
        Branch sourceBranch = mergeRequest.getSourceBranch();

        // Get the latest commit for the source branch
        GitCommit latestSourceCommit = sourceBranch.getLatestCommit();

        // Check that the processing is now actually done
        if (!latestSourceCommit.isFinishedProcessing()) {
            throw new IllegalStateException("ArtefactChurnReviewEvent.handle: "
                    + "the artifact churn did not yet complete");
        }

        // Get the MergeRequestReview object for the merge request
        MergeRequestReview mergeRequestReview = this.mergeRequestReviewRepository
                .findFirstByMergeRequestOrderByIdDesc(mergeRequest)
                .orElseThrow(() -> {
                    String msg = "ArtefactChurnReviewEvent.handle: could not find a "
                            + "MergeRequestReview for MergeRequest with ID " + mergeRequest.getId();

                    throw new IllegalStateException(msg);
                });

        // Get the function and class observations for files on the branch
        List<BranchFunctionObservation> latestBranchFunctionObservations = new ArrayList<>();
        List<BranchClassObservation> latestBranchClassObservations = new ArrayList<>();

        for (BranchFile branchFile :
                this.branchFileRepository.findAllByBranchId(sourceBranch.getId())) {
            // Find the BranchFunctionObservations for the file and add them to the list
            latestBranchFunctionObservations.addAll(
                    this.branchFileObservationHelper.getLatestFunctionObservations(branchFile));

            // Find the BranchClassObservations for the file and add them to the list
            latestBranchClassObservations.addAll(
                    this.branchFileObservationHelper.getLatestClassObservations(branchFile));
        }

        // Translate the function observations to merge request ones
        this.branchFunctionObjectTranslationHelper.translate(
                latestBranchFunctionObservations,
                mergeRequestReview
        );

        // And translate the class observation to merge request ones
        this.branchClassObjectTranslationHelper.translate(
                latestBranchClassObservations,
                mergeRequestReview
        );

        return new EmptyWebhookData();
    }

    @Override
    public int getPriority() {
        return ArtefactChurnReviewEvent.PRIORITY;
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Collections.singletonList(EventType.REVIEW_REQUESTED);
    }
}
