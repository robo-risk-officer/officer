package com.roborisk.officer.events;

import com.roborisk.officer.helpers.AwardScraperHelper;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.RobotSettings;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.RobotSettingsRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import com.roborisk.officer.models.web.MergeRequestWebhookData;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Event that flags discussions on a merge request as false positive if sufficient markings exist.
 */
@Component
@SuppressWarnings({"ClassFanOutComplexity"})
public class UpdateDiscussionFalsePositiveEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the {@link UpdateDiscussionFalsePositiveEvent}.
     * It should run after initialisation events, but before code metrics.
     */
    private static final int PRIORITY = 190;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link RobotSettings} objects.
     */
    @Autowired
    private RobotSettingsRepository robotSettingsRepository;

    /**
     * Helper class for scraping awards.
     */
    @Autowired
    private AwardScraperHelper awardScraperHelper;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData data) {
        // The supported event for UpdateAwardsEvent is the merge request changed event type which
        // is fired for the commit/comment/merge request webhooks.
        // However, for this function to work, it is required to have a merge request object
        // which is only present on comment and merge request webhooks.
        // Filter for these types and perform early return if the type does not match
        if (!(data instanceof CommentWebhookData || data instanceof  MergeRequestWebhookData)) {
            throw new IllegalArgumentException("UpdateDiscussionFalsePositiveEvent.handle: "
                    + "unexpected input data type");
        }

        // The data is either of type MergeRequestWebhookData or CommentWebhookData
        // which follow different formats. Extract the required fields from the data.
        Integer gitLabRepositoryId = data.getRepository().getGitLabId();
        Integer gitLabMergeRequestId;
        Integer gitLabMergeRequestIid;

        if (data instanceof CommentWebhookData) {
            CommentWebhookData commentData = (CommentWebhookData) data;

            gitLabMergeRequestId = commentData.getMergeRequest().getGitLabId();
            gitLabMergeRequestIid = commentData.getMergeRequest().getGitLabIid();
        } else {
            MergeRequestWebhookData mrData = (MergeRequestWebhookData) data;

            gitLabMergeRequestId = mrData.getMergeRequest().getGitLabId();
            gitLabMergeRequestIid = mrData.getMergeRequest().getGitLabIid();
        }

        // Check that the merge request already exists in the database, otherwise this is a new
        // merge request, and the necessary database structures have not been created
        Optional<MergeRequest> databaseMR =
                this.mergeRequestRepository.findByGitLabId(gitLabMergeRequestId);
        if (databaseMR.isEmpty()) {
            // This situation should not occur due to initialisation events having higher priority
            // Throw an illegal state exception to signal this issue
            throw new IllegalStateException("UpdateDiscussionFalsePositiveEvent.handle: merge"
                    + " request has not yet been initialised in the database");
        }

        // Collect the settings for this repository
        int dbRepoId = databaseMR.get().getSourceBranch().getRepository().getId();
        RobotSettings repositorySettings = this.robotSettingsRepository
                .findByRepositoryId(dbRepoId).orElseGet(() ->
                        this.robotSettingsRepository.getDefault().orElseThrow(
                                () -> {
                                    String msg = "UpdateDiscussionFalsePositiveEvent.handle: "
                                            + "could not retrieve robot settings";
                                    throw new IllegalStateException(msg);
                                }
                        )
                );

        try {
            this.awardScraperHelper.scrapeMergeRequest(gitLabRepositoryId, gitLabMergeRequestIid,
                    repositorySettings);
        } catch (GitLabApiException e) {
            throw new IllegalStateException("UpdateDiscussionFalsePositiveEvent.handle: GitLab"
                    + " API Exception occurred: \n" + e.getMessage());
        }

        return new EmptyWebhookData();
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Arrays.asList(
                EventType.MERGE_REQUEST_CHANGED,
                EventType.REVIEW_REQUESTED
        );
    }

    @Override
    public int getPriority() {
        return UpdateDiscussionFalsePositiveEvent.PRIORITY;
    }
}
