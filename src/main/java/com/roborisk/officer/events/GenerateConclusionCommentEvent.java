package com.roborisk.officer.events;

import com.roborisk.officer.helpers.CommentHelper;
import com.roborisk.officer.models.database.DiscussionThread;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.RobotNote;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.repositories.RobotNoteRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import com.roborisk.officer.models.web.InternalSonarWebhookData;
import com.roborisk.officer.views.GeneralEvaluationView;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Generates the comment for the conclusion of a review.
 */
@Component
@SuppressWarnings({"ClassFanOutComplexity"})
public class GenerateConclusionCommentEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the current event.
     */
    private static final int PRIORITY = 30;

    /**
     * View that generates the comment.
     */
    @Autowired
    private GeneralEvaluationView generalEvaluationView;

    /**
     * Helper class to place comments.
     */
    @Autowired
    private CommentHelper commentHelper;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link MergeRequestReview} objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Database repository to handle {@link RobotNote} objects.
     */
    @Autowired
    private RobotNoteRepository robotNoteRepository;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData webhook)
            throws IllegalArgumentException, IllegalStateException {
        // Only accept CommentWebhookData.
        if (!(webhook instanceof CommentWebhookData
                || webhook instanceof InternalSonarWebhookData)) {
            String msg = "GenerateConclusionCommentEvent.handle: Only supports "
                    + "CommentWebhookData and InternalSonarWebhookData.";

            throw new IllegalArgumentException(msg);
        }

        // Get merge request
        MergeRequest mergeRequest;

        if (webhook instanceof CommentWebhookData) {
            CommentWebhookData data = (CommentWebhookData) webhook;

            mergeRequest = this.mergeRequestRepository
                    .findByGitLabId(data.getMergeRequest().getGitLabId())
                    .orElseThrow(() -> {
                        String msg = "GenerateConclusionCommentEvent.handle: Could not find"
                                + " MergeRequest with GitLab ID "
                                + data.getMergeRequest().getGitLabId()
                                + " from CommentWebhookData.";

                        throw new IllegalStateException(msg);
                    });
        } else {
            InternalSonarWebhookData data = (InternalSonarWebhookData) webhook;

            mergeRequest = this.mergeRequestRepository
                .findByGitLabId(data.getSourceBranchAnalysis().getMergeRequestReview()
                    .getMergeRequest().getGitLabId())
                .orElseThrow(() -> {
                    String msg = "GenerateConclusionCommentEvent.handle: Could not "
                            + "find MergeRequest with GitLab ID "
                            + data.getSourceBranchAnalysis().getMergeRequestReview()
                            .getMergeRequest().getGitLabId()
                            + " from InternalSonarWebhookData.";

                    throw new IllegalStateException(msg);
                });
        }

        // Fetch MergeRequestReview
        MergeRequestReview mergeRequestReview = this.mergeRequestReviewRepository
                .findFirstByMergeRequestOrderByIdDesc(mergeRequest)
                .orElseThrow(() -> {
                    String msg = "GenerateConclusionCommentEvent.handle: Cannot find "
                            + "MergeRequestReview with MergeRequest that has ID "
                            + mergeRequest.getId();

                    throw new IllegalStateException(msg);
                });


        // Generate conclusion comment.
        String comment = this.generalEvaluationView
                .generateComment(mergeRequest, mergeRequestReview);

        // If there is not a conclusion comment posted for this review, then post a new one.
        if (mergeRequestReview.getConclusionThread() == null) {
            // Post the comment.
            try {
                this.commentHelper
                        .postConclusionForMergeRequestReview(mergeRequestReview, comment);
            } catch (GitLabApiException e) {
                String msg = "GenerateConclusionCommentEvent.handle: Was not able to post "
                        + "conclusion comment for the merge request review with id "
                        + mergeRequestReview.getId();

                throw new IllegalStateException(msg);
            }
        } else {
            // Otherwise, metrics were still calculating when the first one was placed.
            // Therefore, edit the existing thread on GitLab.
            DiscussionThread conclusionThread = mergeRequestReview.getConclusionThread();

            // Get the note from the database.
            RobotNote conclusionNote = this.robotNoteRepository
                    .findFirstByDiscussionThreadIdOrderByIdAsc(conclusionThread.getId())
                    .orElseThrow(() -> {
                        String msg = "GenerateConclusionCommentEvent.handle: Was not able to find "
                                + "RobotNote comment corresponding to the conclusion thread with"
                                + " id " + conclusionThread.getId();

                        throw new IllegalStateException(msg);
                    });

            // Modify the comment.
            try {
                this.commentHelper
                        .modifyDiscussionThread(conclusionThread, conclusionNote, comment);
            } catch (GitLabApiException e) {
                String msg = "GenerateConclusionCommentEvent.handle: Was not able to edit "
                        + "conclusion note with id " + conclusionNote.getId()
                        + " in discussion with id " + conclusionThread.getId();

                throw new IllegalStateException(msg);
            }
        }

        return new EmptyWebhookData();
    }

    @Override
    public int getPriority() {
        return GenerateConclusionCommentEvent.PRIORITY;
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return List.of(EventType.REVIEW_REQUESTED, EventType.SONAR_ANALYSIS_FINISHED);
    }
}
