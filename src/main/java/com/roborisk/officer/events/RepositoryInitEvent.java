package com.roborisk.officer.events;

import com.roborisk.officer.helpers.RepositoryHelper;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import com.roborisk.officer.models.web.RepositoryWeb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Event for initializing new repositories.
 *
 * <p>
 * The event will check whether a repository already exists in the database.
 * If it does not exist, it will create it and store it.
 * </p>
 *
 * <p>
 * This event should run as one of the first in the Dispatcher. A lot of events will depend on
 * the repository, hence it has a high priority.
 * </p>
 */
@Component
public class RepositoryInitEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the current event.
     */
    private static final int PRIORITY = 280;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Helper for {@link Repository} objects.
     */
    @Autowired
    private RepositoryHelper repositoryHelper;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData data)
            throws IllegalStateException {
        RepositoryWeb web = data.getRepository();

        // Fetch the repository, or fallback on a default.
        Repository repository = this.repositoryRepository.findByGitLabId(web.getGitLabId())
                                                .orElse(new Repository(true));

        // Update the information of the repository. This can also be done when it already exists.
        // The reason for this is because the name can change etc.
        repository.update(web);

        this.repositoryRepository.save(repository);
        this.repositoryHelper.copyOverDefaultRobotSettings(repository);
        this.repositoryHelper.copyOverDefaultMetricSettings(repository);

        return new EmptyWebhookData();
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Arrays.asList(EventType.PUSH_EVENT,
                EventType.MERGE_REQUEST_CREATED,
                EventType.MERGE_REQUEST_CHANGED);
    }

    @Override
    public int getPriority() {
        return RepositoryInitEvent.PRIORITY;
    }
}
