package com.roborisk.officer.events;

import com.roborisk.officer.helpers.BranchFileHelper;
import com.roborisk.officer.helpers.BranchHelper;
import com.roborisk.officer.helpers.MergeRequestReviewHelper;
import com.roborisk.officer.helpers.MetricModelHelper;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.metrics.BranchFileMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestFileMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.MergeRequestFileMetricResultRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * This event copies the metric results from {@link com.roborisk.officer.models.database.BranchFile}
 * to results of a {@link MergeRequestFile}. This is only done for the {@link CodeChurnEvent}.
 */
@Component
@SuppressWarnings({"ClassFanOutComplexity"})
public class CodeChurnBranchMetricCopyEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the event.
     */
    private static final int PRIORITY = 170;

    /**
     * Helper for the {@link MergeRequestReview} model.
     */
    @Autowired
    private MergeRequestReviewHelper mergeRequestReviewHelper;

    /**
     * Helper for the {@link Branch} model.
     */
    @Autowired
    private BranchHelper branchHelper;

    /**
     * Helper for the {@link com.roborisk.officer.models.database.BranchFile} model.
     */
    @Autowired
    private BranchFileHelper branchFileHelper;

    /**
     * Database repository to handle {@link MergeRequestFileMetricResult} objects.
     */
    @Autowired
    private MergeRequestFileMetricResultRepository mergeRequestFileMetricResultRepository;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData webhook) {
        if (!(webhook instanceof CommentWebhookData)) {
            String msg = "CodeChurnBranchMetricCopy.handle: Only CommentWebhookData is supported";
            throw new IllegalArgumentException(msg);
        }

        // Cast to the right instance.
        CommentWebhookData data = (CommentWebhookData) webhook;

        // Fetch the branch.
        Branch branch = this.branchHelper.findBranchByNameAndRepositoryGitLabId(
                data.getMergeRequest().getSource(),
                data.getRepositoryId()
        );

        List<MergeRequestFile> files = this.mergeRequestReviewHelper
                    .findFilesOfLatestMergeRequestReview(data.getMergeRequest());

        Metric metric = MetricModelHelper.getMetric(CodeChurnEvent.METRIC_NAME_FILE);

        for (MergeRequestFile file : files) {
            Optional<BranchFileMetricResult> optional = this.branchFileHelper
                    .getLatestMetricResultOfFile(branch.getId(), metric.getId(), file);

            if (optional.isEmpty()) {
                continue;
            }

            BranchFileMetricResult result = optional.get();

            MergeRequestFileMetricResult mergeRequestMetricResult =
                    new MergeRequestFileMetricResult(
                            result,
                            file
                    );

            this.mergeRequestFileMetricResultRepository.save(mergeRequestMetricResult);
        }

        return new EmptyWebhookData();
    }

    @Override
    public int getPriority() {
        return CodeChurnBranchMetricCopyEvent.PRIORITY;
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Collections.singletonList(EventType.REVIEW_REQUESTED);
    }
}
