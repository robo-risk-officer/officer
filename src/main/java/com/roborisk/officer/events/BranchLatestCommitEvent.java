package com.roborisk.officer.events;

import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * This event sets the branch latest commit to the latest
 * commit of the {@link CommitWebhookData}.
 *
 * <p>
 * Dependencies:
 * - {@link CommitInitEvent}
 * - {@link RepositoryInitEvent}
 * </p>
 */
@Component
public class BranchLatestCommitEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the current event.
     */
    private static final int PRIORITY = 150;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData webhook)
            throws IllegalArgumentException, IllegalStateException {
        if (!(webhook instanceof CommitWebhookData)) {
            String msg = "BranchLatestCommitEvent.handle: Only supports CommitWebhookData";
            throw new IllegalArgumentException(msg);
        }

        CommitWebhookData data = (CommitWebhookData) webhook;

        // If no commits are found in this webhook, crash.
        if (data.getCommits().size() < 1) {
            String msg = "BranchLatestCommitEvent.handle: No commits received!";
            throw new IllegalStateException(msg);
        }

        data.getCommits().sort((a, b) -> -Integer.compare(a.getTimestamp(), b.getTimestamp()));

        Repository repository = this.repositoryRepository.findByGitLabId(
                data.getRepositoryId()
        ).orElseThrow(() -> {
            String msg = "BranchLatestCommitEvent.handle: "
                    + "Cannot find repository with GitLab ID " + data.getRepositoryId();

            return new IllegalStateException(msg);
        });

        Branch branch = this.branchRepository.findByNameAndRepositoryId(
                data.getBranchName(), repository.getId()
        ).orElseThrow(() -> {
            String msg = "BranchLatestCommitEvent.handle: Cannot find branch with name "
                        + data.getBranchName() + " and repository ID " + repository.getId();

            return new IllegalStateException(msg);
        });

        // Find the newest commit.
        String hash = data.getCommits().get(0).getHash();
        GitCommit commit = this.gitCommitRepository.findByHash(hash)
                .orElseThrow(() -> {
                    String msg = "BranchLatestCommitEvent.handle: Commit not found with hash "
                            + hash;

                    return new IllegalStateException(msg);
                });

        branch.setLatestCommit(commit);
        this.branchRepository.save(branch);

        return new EmptyWebhookData();
    }

    @Override
    public int getPriority() {
        return BranchLatestCommitEvent.PRIORITY;
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Collections.singletonList(EventType.PUSH_EVENT);
    }
}
