package com.roborisk.officer.events;

import com.roborisk.officer.helpers.SonarReviewEventHelper;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import com.roborisk.officer.modules.metrics.sonar.SonarApiWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * This {@link Event} ensures that SonarQube starts analysing the {@link MergeRequest} when a
 * review has been requested.
 */
@Slf4j
@Component
public class SonarReviewEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the event.
     */
    private static final int PRIORITY = 150;

    /**
     * Wrapper to interact with SonarQube.
     */
    @Autowired
    private SonarApiWrapper sonarApiWrapper;

    /**
     * Repository to interact with {@link MergeRequest} database objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Repository to interact with {@link MergeRequestReview} database objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    @Autowired
    private SonarReviewEventHelper sonarReviewEventHelper;

    /**
     * Stores whether SonarQube reviews are enabled or not.
     */
    @Value("${officer.sonar.enabled:true}")
    private boolean sonarEnabled;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData webhook) {
        // Only supports CommentWebhookData.
        if (!(webhook instanceof CommentWebhookData)) {
            String msg = "SonarReviewEvent.handle: Only CommentWebhookData is supported";

            throw new IllegalArgumentException(msg);
        }

        // If SonarQube is not enabled for the Robo Risk Officer, simply stop.
        if (!this.sonarEnabled) {
            return new EmptyWebhookData();
        }

        // Otherwise, parse the data to the correct type.
        CommentWebhookData data = (CommentWebhookData) webhook;

        // Fetch MergeRequest
        MergeRequest mergeRequest = this.mergeRequestRepository
                .findByGitLabId(data.getMergeRequest().getGitLabId())
                .orElseThrow(() -> {
                    String msg = "SonarReviewEvent.handle: Cannot find MergeRequest with GitLab Id"
                            + data.getMergeRequest().getGitLabId();

                    return new IllegalStateException(msg);
                });

        // Fetch latest MergeRequestReview on the MergeRequest
        MergeRequestReview mergeRequestReview = this.mergeRequestReviewRepository
                .findFirstByMergeRequestOrderByIdDesc(mergeRequest)
                .orElseThrow(() -> {
                    String msg = "SonarReviewEvent.handle: Cannot find MergeRequestReview "
                            + "corresponding to the MergeRequest with ID " + mergeRequest.getId();

                    throw new IllegalStateException(msg);
                });

        // Initialise projects for SonarQube using the SonarApiWrapper
        try {
            this.sonarApiWrapper.initialiseProjectForAnalysis(mergeRequestReview);
        } catch (Exception e) {
            String msg = "SonarReviewEvent.handle: Failed to create and analyse merge request"
                    + " with SonarQube. Reason: " + e.getMessage();

            throw new IllegalStateException(msg);
        }

        // Create empty MetricResults for all files inside the mergeRequestReview
        try {
            this.sonarReviewEventHelper.createAndSaveAllMetricResults(mergeRequestReview);
        } catch (Exception e) {
            String msg = "SonarReviewEvent.handle: Failed to initialise metric results.";

            throw new IllegalStateException(msg);
        }

        return new EmptyWebhookData();
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Collections.singletonList(EventType.REVIEW_REQUESTED);
    }

    @Override
    public int getPriority() {
        return SonarReviewEvent.PRIORITY;
    }
}
