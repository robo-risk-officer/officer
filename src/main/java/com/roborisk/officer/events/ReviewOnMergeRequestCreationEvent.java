package com.roborisk.officer.events;

import com.roborisk.officer.helpers.RobotSettingsHelper;
import com.roborisk.officer.models.database.RobotSettings;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import com.roborisk.officer.models.web.MergeRequestWebhookData;
import com.roborisk.officer.models.web.NoteWeb;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * Does an automatic review on MergeRequest creation if that is enabled in {@link RobotSettings}
 * of the repository. It only supports {@link MergeRequestWebhookData} and
 * is only executed on merge request creation as the last event (priority 0).
 *
 * <p>
 * It directly interacts with the dispatcher by creating a mock {@link CommentWebhookData}
 * by using some fields from the provided webhook. Then directly calling the dispatcher to
 * process the mocked comment webhook, which will trigger a review.
 * </p>
 */
@Component
public class ReviewOnMergeRequestCreationEvent implements Event<AbstractWebhookData> {

    /**
     * The priority of the current event.
     */
    private static final int PRIORITY = 110;

    @Override
    public AbstractWebhookData handle(EventType type, AbstractWebhookData webhook)
            throws IllegalStateException, IllegalArgumentException {
        if (!(webhook instanceof MergeRequestWebhookData)) {
            String msg = "ReviewOnMergeRequestCreationEvent.handle:"
                    + " Only supports CommitWebhookData";

            throw new IllegalStateException(msg);
        }

        RobotSettings robotSettings = RobotSettingsHelper
                .getRepositorySettings(webhook.getRepository());

        // check if setting for automatic reviews is enabled
        if (!robotSettings.getAutomaticReviewOnNewMr()) {
            return new EmptyWebhookData();
        }

        MergeRequestWebhookData data = (MergeRequestWebhookData) webhook;

        // mock comment asking for a review
        NoteWeb mockReviewNote = new NoteWeb();

        mockReviewNote.setGitLabId(-1);
        mockReviewNote.setRepositoryId(data.getRepository().getGitLabId());
        mockReviewNote.setNoteableId(data.getMergeRequest().getGitLabId());
        mockReviewNote.setNoteableType("MergeRequest");
        mockReviewNote.setNote("@" + CommentWebhookData.ROBOT_NAME + " review");

        // mock the comment webhook
        return new CommentWebhookData(data, mockReviewNote);
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Collections.singletonList(EventType.MERGE_REQUEST_CREATED);
    }

    @Override
    public int getPriority() {
        return ReviewOnMergeRequestCreationEvent.PRIORITY;
    }
}
