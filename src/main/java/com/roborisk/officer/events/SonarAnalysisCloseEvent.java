package com.roborisk.officer.events;

import com.roborisk.officer.models.database.SonarAnalysisTracker;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.SonarAnalysisTrackerRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import com.roborisk.officer.models.web.InternalSonarWebhookData;
import com.roborisk.officer.modules.git.GitModule;
import com.roborisk.officer.modules.metrics.sonar.SonarApiWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * This event is the last {@link Event} invoked no receipt of the SONAR_ANALYSIS_FINISHED
 * {@link EventType} and calls the {@link SonarApiWrapper} closeModulesAndTrackers function
 * to clean up the {@link GitModule} and the database trackers.
 */
@Slf4j
@Component
public class SonarAnalysisCloseEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the {@link SonarAnalysisCloseEvent} event.
     */
    private static final int PRIORITY = 10;

    /**
     * Database repositoryu to handle {@link SonarAnalysisTracker} objects.
     */
    @Autowired
    private SonarAnalysisTrackerRepository sonarAnalysisTrackerRepository;

    /**
     * Wrapper to interact with sonar.
     */
    @Autowired
    private SonarApiWrapper sonarApiWrapper;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData webhook) {
        // Only supports SonarWebhookData.
        if (!(webhook instanceof InternalSonarWebhookData)) {
            String msg = "SonarAnalysisCloseEvent.handle: Only supports SonarWebhookData";

            throw new IllegalArgumentException(msg);
        }

        // Cast to the right format.
        InternalSonarWebhookData data = (InternalSonarWebhookData) webhook;

        // Fetch sourceTracker
        Optional<SonarAnalysisTracker> sourceTracker = this.sonarAnalysisTrackerRepository
                .findById(data.getSourceBranchAnalysis().getId());

        // Close modules and trackers
        if (sourceTracker.isPresent()) {
            List<SonarAnalysisTracker> trackers = this.sonarAnalysisTrackerRepository
                    .findAllByMergeRequestReviewId(sourceTracker.get()
                            .getMergeRequestReview().getId());
            // Log warning if a there is more than one tracker in the MergeRequestReview.
            if (trackers.size() != 1) {
                log.warn("SonarAnalysisCloseEvent.handle: MergeRequestReview has "
                        + trackers.size()
                        + "trackers (not 1) which will all be closed.");
            }

            // Close modules and trackers via SonarApiWrapper.
            this.sonarApiWrapper.closeModulesAndTrackers(
                    sourceTracker.get().getMergeRequestReview());
        } else {
            String msg = "SonarAnalysisCloseEvent.handle: could not find the tracker with name "
                    + data.getSourceBranchAnalysis().getName() + ". Nothing was deleted";

            throw new IllegalStateException(msg);
        }

        return new EmptyWebhookData();
    }

    @Override
    public int getPriority() {
        return SonarAnalysisCloseEvent.PRIORITY;
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Collections.singletonList(EventType.SONAR_ANALYSIS_FINISHED);
    }
}
