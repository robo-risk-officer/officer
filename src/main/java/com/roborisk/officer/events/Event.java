package com.roborisk.officer.events;

import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.web.AbstractWebhookData;

import java.util.List;

/**
 * Event interface for all events that the robot executes based on the webhooks.
 */
public interface Event<T extends AbstractWebhookData> {

    /**
     * Handles the event.
     *
     * @param type The EventType of the event
     * @param data The webhook data for the event
     */
    public T handle(EventType type, AbstractWebhookData data);

    /**
     * Gets the priority of the event.
     *
     * @return The priority of the event.
     */
    public int getPriority();

    /**
     * Returns a list of event types this event can support.
     *
     * @return The list of supported event types.
     */
    public List<EventType> getCompatibleEvents();
}
