package com.roborisk.officer.events;

import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * This event is the first to be fired when a review is requested as it creates a
 * {@link MergeRequestReview} object and stores it in the database. It can be collected
 * by later events by getting the latest MergeRequestReview that corresponds to a MergeRequest
 * through the MergeRequestReviewRepository.
 */
@Slf4j
@Component
@SuppressWarnings({"ClassFanOutComplexity"})
public class MergeRequestReviewInitEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the current event.
     */
    private static final int PRIORITY = 240;

    /**
     * Repository to interact with {@link MergeRequest} database objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Repository to interact with {@link MergeRequestReview} database objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Repository to interact with {@link GitCommit} database objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData webhook)
            throws IllegalStateException, IllegalArgumentException {
        // Only accept CommentWebhookData
        if (!(webhook instanceof CommentWebhookData)) {
            String msg = "MergeRequestReviewInitEvent.handle: Only supports CommentWebhookData";

            throw new IllegalArgumentException(msg);
        }

        // Cast to right format
        CommentWebhookData data = (CommentWebhookData) webhook;

        // Fetch MergeRequest
        MergeRequest mergeRequest = this.mergeRequestRepository.findByGitLabId(
                    data.getMergeRequest().getGitLabId()
                ).orElseThrow(() -> {
                    String msg = "MergeRequestReviewInitEvent.handle: Cannot find MergeRequest "
                            + "with GitLab ID " + data.getMergeRequest().getGitLabId();

                    return new IllegalStateException(msg);
                });

        String hash = data.getMergeRequest().getLastCommit().getHash();

        // Get lastCommit from mergeRequest from database for the review.
        GitCommit lastCommit = this.gitCommitRepository.findByHash(hash)
                .orElseGet(() -> {
                    // if not in the database, construct from the webhook commit
                    GitCommit tmp = new GitCommit(data.getMergeRequest().getLastCommit());

                    // save it in the database
                    return this.gitCommitRepository.save(tmp);
                });

        Boolean reviewExists = this.mergeRequestReviewRepository
                .existsByCommitHashAndMergeRequest(hash, mergeRequest);

        // make sure to not do a duplicate review
        if (reviewExists) {
            log.warn("MergeRequestReviewInitEvent.handle: Was asked to process a merge request "
                    +   "with latest commit for which a review already exists");
            return new EmptyWebhookData();
        }

        // Create MergeRequestReview object and set attributes.
        MergeRequestReview mergeRequestReview = new MergeRequestReview();
        mergeRequestReview.setMergeRequest(mergeRequest);
        mergeRequestReview.setCommit(lastCommit);

        // Save MergeRequestReview object to the database.
        this.mergeRequestReviewRepository.save(mergeRequestReview);

        return new EmptyWebhookData();
    }

    @Override
    public int getPriority() {
        return MergeRequestReviewInitEvent.PRIORITY;
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Collections.singletonList(EventType.REVIEW_REQUESTED);
    }
}
