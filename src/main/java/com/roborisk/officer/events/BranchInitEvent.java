package com.roborisk.officer.events;

import com.roborisk.officer.helpers.BranchInitMetricLinkHelper;
import com.roborisk.officer.helpers.GitCommitHelper;
import com.roborisk.officer.helpers.MergeRequestWebHelper;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import com.roborisk.officer.models.web.MergeRequestWeb;
import com.roborisk.officer.models.web.MergeRequestWebhookData;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * This class implements a version of the {@link BranchInitEventCommit}
 * on a merge request changed event or a comment event.
 * Therefore, it requires a {@link MergeRequestWebhookData} or a {@link CommentWebhookData}.
 * It does not support the {@link com.roborisk.officer.models.web.CommitWebhookData}.
 *
 * <p>
 * The event will fetch the commit from GitLab if it does not exist.
 * If this is the case, the commit will only be created in the {@link GitCommit} table and
 * will not be linked to any files like the normal event pipeline would do.
 * This will be only temporary, on the next commit everything will be linked properly again.
 * </p>
 *
 * <p>
 * Dependencies:
 * - {@link CommitInitEvent} - only when it exists in DB.
 * - {@link RepositoryInitEvent}
 * </p>
 */
@Component
@SuppressWarnings({"ClassFanOutComplexity"})
public class BranchInitEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the current event.
     */
    private static final int PRIORITY = 270;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * {@link GitCommit} helper used to find the latest GitLab commit on a branch.
     */
    @Autowired
    private GitCommitHelper gitCommitHelper;

    /**
     * Helper class to copy over metric results for newly created branches.
     */
    @Autowired
    private BranchInitMetricLinkHelper branchInitMetricLinkHelper;

    /**
     * Helper class to extract {@link MergeRequestWeb} objects from {@link AbstractWebhookData}.
     */
    @Autowired
    private MergeRequestWebHelper mergeRequestWebHelper;

    /**
     * Stores a branch given a branch name.
     *
     * @param branch                The name of the branch to add.
     * @param repository            The repository to add the branch to.
     * @throws GitLabApiException   If the {@link GitLabApiWrapper} encounters an error.
     */
    private void storeBranch(String branch, Repository repository) throws GitLabApiException {
        // if it exists just continue.
        if (this.branchRepository.existsByNameAndRepositoryId(branch, repository.getId())) {
            return;
        }

        GitCommit gitCommit = this.gitCommitHelper.getLatestFromGitLab(repository, branch);

        // Create the new branch and save it
        Branch newBranch = new Branch(branch, repository, gitCommit);
        this.branchRepository.save(newBranch);

        // Call handler to initialise the metric results for the new branch (if it was branched off)
        this.branchInitMetricLinkHelper.initialiseBranch(newBranch);
    }

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData webhook)
            throws IllegalArgumentException, IllegalStateException {

        // Throws the correct exception when needed.
        MergeRequestWeb mergeRequestWeb = this.mergeRequestWebHelper.extractMergeRequest(webhook);

        // Extract info.
        String source = mergeRequestWeb.getSource();
        String target = mergeRequestWeb.getTarget();

        //Extract repository.
        Integer gitLabId = webhook.getRepository().getGitLabId();
        Repository repository = this.repositoryRepository.findByGitLabId(
                gitLabId
        ).orElseThrow(() -> {
            String msg = "BranchInitEventMergeRequest.handle: "
                    + "Cannot find repository with GitLab ID " + gitLabId;

            return new IllegalStateException(msg);
        });

        try {
            this.storeBranch(source, repository);
            this.storeBranch(target, repository);
        } catch (GitLabApiException e) {
            throw new IllegalStateException(e.getMessage());
        }

        return new EmptyWebhookData();
    }

    @Override
    public int getPriority() {
        return BranchInitEvent.PRIORITY;
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Arrays.asList(EventType.MERGE_REQUEST_CREATED,
                EventType.MERGE_REQUEST_CHANGED,
                EventType.REVIEW_REQUESTED);
    }
}
