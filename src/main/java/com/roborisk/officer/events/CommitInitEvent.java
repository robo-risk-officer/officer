package com.roborisk.officer.events;

import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * Event for initializing new commits into the DB.
 *
 * <p>
 * The event will make sure that all commits are stored in the database.
 * It can handle multiple incoming commit webhooks at the same time, it tests for existence
 * before storing the actual results.
 * </p>
 */
@Component
public class CommitInitEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the current event.
     */
    private static final int PRIORITY = 280;

    /**
     * Database repository to handle {@link GitCommitRepository} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData webhook)
            throws IllegalArgumentException {
        // It only accepts CommitWebhook Data.
        if (!(webhook instanceof CommitWebhookData)) {
            String msg = "CommitInitEvent.handle: Only supports CommitWebhookData";
            throw new IllegalArgumentException(msg);
        }

        //TODO: Figure out how to handle multiple parents.

        // Stores all GitCommitWeb objects as GitCommit objects.
        CommitWebhookData data = (CommitWebhookData) webhook;
        data.getCommits()
                .stream()
                .map(GitCommit::new)
                .forEach(c -> {
                    if (!this.gitCommitRepository.existsByHash(c.getHash())) {
                        this.gitCommitRepository.save(c);
                    }
                });

        return new EmptyWebhookData();
    }

    @Override
    public int getPriority() {
        return CommitInitEvent.PRIORITY;
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Collections.singletonList(EventType.PUSH_EVENT);
    }
}
