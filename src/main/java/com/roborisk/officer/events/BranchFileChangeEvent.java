package com.roborisk.officer.events;

import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import com.roborisk.officer.models.web.GitCommitWeb;
import com.roborisk.officer.modules.metrics.codechurn.FileCodeChurnHelper;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * After branches have been initialized in {@link BranchInitEventCommit}, this event processes
 * all the changes made to files (additions/modifications/deletions) and updates the state of those
 * respective {@link BranchFile} models.
 *
 * <p>
 * Dependencies:
 * - {@link RepositoryInitEvent}
 * - {@link BranchInitEventCommit}
 * - {@link CommitInitEvent}
 * </p>
 */
@Component
public class BranchFileChangeEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the current event.
     */
    private static final int PRIORITY = 260;

    /**
     * Repository to handle {@link Branch} models.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Repository to handle {@link Repository} models.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Repository to handle {@link BranchFile} models.
     */
    @Autowired
    private BranchFileRepository branchFileRepository;

    /**
     * Helper class to get lists of added/modified/deleted {@link BranchFile} objects.
     */
    @Autowired
    private FileCodeChurnHelper fileCodeChurnHelper;

    @Override
    public EmptyWebhookData handle(EventType type,
                       AbstractWebhookData data) {
        // It only accepts CommitWebhook Data.
        if (!(data instanceof CommitWebhookData)) {
            String msg = "BranchFileChangeEvent.handle: Only supports CommitWebhookData";

            throw new IllegalArgumentException(msg);
        }

        CommitWebhookData webhookData = (CommitWebhookData) data;

        webhookData.getCommits().sort(Comparator.comparing(GitCommitWeb::getTimestamp));

        // Fetch repository, if it does not exist raise an exception.
        Repository repository = this.repositoryRepository
                .findByGitLabId(webhookData.getRepositoryId())
                .orElseThrow(() -> {
                    String msg = "BranchFileChangeEvent.handle: Repository cannot "
                            + "be found with GitLabID " + webhookData.getRepositoryId();

                    return  new IllegalStateException(msg);
                });

        // Fetch the branch, if it does not exist raise an exception.
        Branch branch = this.branchRepository.findByNameAndRepositoryId(
                webhookData.getBranchName(), repository.getId()
        ).orElseThrow(() -> {
            String msg = "BranchFileChangeEvent.handle: Cannot find branch with name "
                    + webhookData.getBranchName() + " and repository ID " + repository.getId();

            return new IllegalStateException(msg);
        });

        this.handleAddition(this.fileCodeChurnHelper.getAddedFiles(webhookData), branch);
        this.handleRemovedOrModified(this.fileCodeChurnHelper.getRemovedFiles(webhookData),
                branch, true);
        this.handleRemovedOrModified(this.fileCodeChurnHelper.getModifiedFiles(webhookData),
                branch, false);

        return new EmptyWebhookData();
    }

    /**
     * Updates the database state for removed or modified files.
     *
     * @param commitToFiles The changed/deleted files in the commit, grouped on commit hash.
     * @param branch        Branch on which these changes are taking place.
     * @param removed       True if the files have been deleted.
     */
    private void handleRemovedOrModified(List<Pair<GitCommit, List<BranchFile>>> commitToFiles,
            Branch branch, Boolean removed) {
        // Loop over the commits.
        commitToFiles.forEach(commitToFile -> {
            // Loop over the files within the commit.
            for (BranchFile changed : commitToFile.getRight()) {
                // get an optional for the existing branch file.
                BranchFile file = this.branchFileRepository
                        .findByBranchIdAndFilePath(branch.getId(), changed.getFilePath())
                        .orElseGet(() -> {
                            BranchFile tmp = new BranchFile();

                            tmp.setBranch(branch);
                            tmp.setLastChangeCommit(commitToFile.getLeft());
                            tmp.setIsDeleted(removed);
                            tmp.setFilePath(changed.getFilePath());
                            tmp.setFileName(changed.getFileName());

                            return tmp;
                        });

                // Set the last changed commit.
                file.setLastChangeCommit(commitToFile.getLeft());
                // Set to deleted.
                file.setIsDeleted(removed);

                // Store the branch file.
                this.branchFileRepository.save(file);
            }
        });
    }

    /**
     * Updates the database state for new, added files.
     *
     * @param additions The added files in the commit, grouped on commit hash.
     * @param branch    Branch on which these changes are taking place.
     */
    private void handleAddition(List<Pair<GitCommit, List<BranchFile>>> additions, Branch branch) {
        // Loop over the commits.
        additions.forEach(singleAddition -> {

            // loop over the files within that commit.
            for (BranchFile addition : singleAddition.getRight()) {

                // Check if the file already exists (in case that a file was added back in)
                Optional<BranchFile> optExistingFile =
                        this.branchFileRepository.findByBranchIdAndFilePath(
                            branch.getId(),
                            addition.getFilePath()
                        );

                if (optExistingFile.isPresent()) {
                    // The file exists, so it was brought back after deletion
                    // Update the is deleted flag and the last changed commit
                    BranchFile existingFile = optExistingFile.get();
                    existingFile.setIsDeleted(false);
                    existingFile.setLastChangeCommit(singleAddition.getLeft());
                    this.branchFileRepository.save(existingFile);
                    continue;
                }

                // If the file did not yet exist, set its attributes
                addition.setIsDeleted(false);
                addition.setBranch(branch);
                addition.setLastChangeCommit(singleAddition.getLeft());

                // and store the file
                this.branchFileRepository.save(addition);
            }
        });
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Collections.singletonList(EventType.PUSH_EVENT);
    }

    @Override
    public int getPriority() {
        return BranchFileChangeEvent.PRIORITY;
    }
}
