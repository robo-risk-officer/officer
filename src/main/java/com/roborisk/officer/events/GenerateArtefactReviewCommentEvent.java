package com.roborisk.officer.events;

import com.roborisk.officer.helpers.comments.ClassReviewCommentHelper;
import com.roborisk.officer.helpers.comments.CodebaseReviewCommentHelper;
import com.roborisk.officer.helpers.comments.FileReviewCommentHelper;
import com.roborisk.officer.helpers.comments.FunctionReviewCommentHelper;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import com.roborisk.officer.models.web.InternalSonarWebhookData;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * The event which calls the helper to extract the necessary data for the final review comments,
 * pass it to the appropriate views and post the comments in GitLab.
 *
 * <p>
 * Dependencies:
 * - {@link RepositoryInitEvent}
 * - {@link CopyBranchArtefactEvent}
 * - {@link CodeChurnEvent}
 * - {@link SonarReviewEvent}
 * </p>
 */
@Component
@SuppressWarnings({"ClassFanOutComplexity"})
public class GenerateArtefactReviewCommentEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the current event.
     */
    private static final int PRIORITY = 40;

    /**
     * Repository for retriving {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Comment helper for posting reviews on classes.
     */
    @Autowired
    private ClassReviewCommentHelper classReviewCommentHelper;

    /**
     * Comment helper for posting reviews on codebases.
     */
    @Autowired
    private CodebaseReviewCommentHelper codebaseReviewCommentHelper;

    /**
     * Comment helper for posting reviews on files.
     */
    @Autowired
    private FileReviewCommentHelper fileReviewCommentHelper;

    /**
     * Comment helper for posting reviews on functions.
     */
    @Autowired
    private FunctionReviewCommentHelper functionReviewCommentHelper;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData webhookData) {
        // Only supports CommentWebhookData.
        if (!(webhookData instanceof CommentWebhookData
                || webhookData instanceof InternalSonarWebhookData)) {
            String msg = "GenerateArtefactReviewCommentEvent.handle: Only CommentWebhookData and "
                    + "InternalSonarWebhookData are supported";

            throw new IllegalArgumentException(msg);
        }

        // Get merge request
        MergeRequest mergeRequest;

        if (webhookData instanceof CommentWebhookData) {
            CommentWebhookData data = (CommentWebhookData) webhookData;

            mergeRequest = this.mergeRequestRepository
                    .findByGitLabId(data.getMergeRequest().getGitLabId())
                    .orElseThrow(() -> {
                        String msg = "GenerateArtefactReviewCommentEvent.hanlde: Could not find"
                                + " MergeRequest with GitLab ID "
                                + data.getMergeRequest().getGitLabId()
                                + " from CommentWebhookData.";

                        throw new IllegalStateException(msg);
                    });
        } else {
            InternalSonarWebhookData data = (InternalSonarWebhookData) webhookData;

            mergeRequest = this.mergeRequestRepository
                    .findByGitLabId(data.getSourceBranchAnalysis().getMergeRequestReview()
                        .getMergeRequest().getGitLabId())
                        .orElseThrow(() -> {
                            String msg = "GenerateArtefactReviewCommentEvent.handle: Could not "
                                    + "find MergeRequest with GitLab ID "
                                    + data.getSourceBranchAnalysis().getMergeRequestReview()
                                        .getMergeRequest().getGitLabId()
                                    + " from InternalSonarWebhookData.";

                            throw new IllegalStateException(msg);
                        });
        }

        try {
            // Post review on the individual artefacts.
            this.codebaseReviewCommentHelper.postReviewOnCodebase(mergeRequest);
            this.fileReviewCommentHelper.postReviewOnAllFilesChanged(mergeRequest);
            this.classReviewCommentHelper.postReviewOnAllClassesChanged(mergeRequest);
            this.functionReviewCommentHelper.postReviewOnAllFunctionsChanged(mergeRequest);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalStateException("GenerateArtefactReviewCommentEvent.handle: There was "
                    + "an IOException during the posting of the reviews.");
        } catch (GitLabApiException e) {
            e.printStackTrace();
            throw new IllegalStateException("GenerateArtefactReviewCommentEvent.handle: There was "
                    + "a GitLabApiException during the posting of the reviews.");
        }

        return new EmptyWebhookData();
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return List.of(EventType.REVIEW_REQUESTED, EventType.SONAR_ANALYSIS_FINISHED);
    }

    @Override
    public int getPriority() {
        return GenerateArtefactReviewCommentEvent.PRIORITY;
    }
}
