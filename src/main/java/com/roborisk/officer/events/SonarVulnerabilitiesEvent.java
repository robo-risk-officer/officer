package com.roborisk.officer.events;

import com.roborisk.officer.helpers.MetricModelHelper;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.SonarAnalysisTracker;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.SonarAnalysisTrackerRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import com.roborisk.officer.models.web.InternalSonarWebhookData;
import com.roborisk.officer.modules.metrics.sonar.Vulnerabilities;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * This event activates the SonarQube vulnerabilities analysis metric.
 */
@Component
@Slf4j
public class SonarVulnerabilitiesEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the {@link SonarVulnerabilitiesEvent} event.
     */
    private static final int PRIORITY = 150;

    /**
     * The metric name.
     */
    public static final String METRIC_NAME = "sonarqube_vulnerabilities";

    /**
     * Repository used to access {@link SonarAnalysisTracker} objects in the database.
     */
    @Autowired
    private SonarAnalysisTrackerRepository sonarAnalysisTrackerRepository;

    /**
     * Link to the {@link Vulnerabilities} class.
     */
    @Autowired
    private Vulnerabilities vulnerabilities;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData webhook) {
        // It only accept SonarWebhookData.
        if (!(webhook instanceof InternalSonarWebhookData)) {
            String msg = "VulnerabilitiesEvent.handle: Only supports SonarWebhookData";

            throw new IllegalArgumentException(msg);
        }

        // Cast to the right format.
        InternalSonarWebhookData data = (InternalSonarWebhookData) webhook;

        // Fetch sourceTracker
        SonarAnalysisTracker sourceTracker = this.sonarAnalysisTrackerRepository
                .findById(data.getSourceBranchAnalysis().getId())
                .orElseThrow(() -> {
                    String msg = "VulnerabilitiesEvent.handle: SourceBranchAnalysis cannot be "
                            + "found with ID " + data.getSourceBranchAnalysis().getId();

                    return new IllegalStateException(msg);
                });

        // Fetch the metric
        Metric metric = MetricModelHelper.getMetric(SonarVulnerabilitiesEvent.METRIC_NAME);

        Integer repositoryId = sourceTracker.getMergeRequestReview().getMergeRequest()
                .getSourceBranch().getRepository().getId();

        // Fetch the metric settings for the repository or the default if not found
        MetricSettings settings = MetricModelHelper.getMetricSettings(metric.getId(), repositoryId);

        try {
            this.vulnerabilities.handle(
                    settings,
                    metric,
                    sourceTracker
            );
        } catch (Exception e) {
            if (e.getMessage() == null) {
                e.printStackTrace();
            }

            throw new IllegalStateException(e.getMessage());
        }

        return new EmptyWebhookData();
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Collections.singletonList(EventType.SONAR_ANALYSIS_FINISHED);
    }

    @Override
    public int getPriority() {
        return SonarVulnerabilitiesEvent.PRIORITY;
    }
}
