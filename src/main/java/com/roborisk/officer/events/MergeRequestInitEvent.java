package com.roborisk.officer.events;

import com.roborisk.officer.helpers.MergeRequestWebHelper;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import com.roborisk.officer.models.web.MergeRequestWeb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Initializes and updates merge requests that have not been seen before by the RRO.
 *
 * <p>
 * Dependencies:
 * - {@link BranchInitEventCommit}
 * - {@link RepositoryInitEvent}
 * </p>
 */
@Component
public class MergeRequestInitEvent implements Event<EmptyWebhookData> {

    /**
     * The priority of the current event.
     */
    private static final int PRIORITY = 250;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Helper class to extract {@link MergeRequestWeb} objects from {@link AbstractWebhookData}.
     */
    @Autowired
    private MergeRequestWebHelper mergeRequestWebHelper;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData webhook)
            throws IllegalArgumentException, IllegalStateException {

        // Throws the correct exception for us.
        MergeRequestWeb mergeRequestWeb = this.mergeRequestWebHelper.extractMergeRequest(webhook);

        // Fetch the repository.
        Integer gitLabId = webhook.getRepository().getGitLabId();
        Repository repository = this.repositoryRepository.findByGitLabId(
                gitLabId
        ).orElseThrow(() -> {
            String msg = "MergeRequestInitEvent.handle: "
                    + "Cannot find repository with GitLab ID " + gitLabId;

            return new IllegalStateException(msg);
        });

        // Fetch the branches.
        String targetName = mergeRequestWeb.getTarget();
        String sourceName = mergeRequestWeb.getSource();

        Branch target = this.branchRepository.findByNameAndRepositoryId(
                targetName, repository.getId()
        ).orElseThrow(() -> {
            String msg = "MergeRequestInitEvent.handle: Cannot find branch with name "
                    + targetName + " and repository ID " + repository.getId();

            return new IllegalStateException(msg);
        });

        Branch source = this.branchRepository.findByNameAndRepositoryId(
                sourceName, repository.getId()
        ).orElseThrow(() -> {
            String msg = "MergeRequestInitEvent.handle: Cannot find branch with name "
                    + sourceName + " and repository ID " + repository.getId();

            return new IllegalStateException(msg);
        });

        MergeRequest mergeRequest =
                this.mergeRequestRepository.findByGitLabId(mergeRequestWeb.getGitLabId())
                    .orElseGet(() -> new MergeRequest(
                        mergeRequestWeb,
                        source,
                        target
                    ));

        mergeRequest.setState(mergeRequestWeb.getState());
        mergeRequest.setTitle(mergeRequestWeb.getTitle());
        mergeRequest.setMergeStatus(mergeRequest.getMergeStatus());

        this.mergeRequestRepository.save(mergeRequest);

        return new EmptyWebhookData();
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Arrays.asList(EventType.MERGE_REQUEST_CHANGED,
                    EventType.REVIEW_REQUESTED,
                    EventType.MERGE_REQUEST_CREATED);
    }

    @Override
    public int getPriority() {
        return MergeRequestInitEvent.PRIORITY;
    }
}
