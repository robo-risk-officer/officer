package com.roborisk.officer.events;

import com.roborisk.officer.helpers.BranchInitMetricLinkHelper;
import com.roborisk.officer.helpers.GitCommitHelper;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import com.roborisk.officer.models.web.GitCommitWeb;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Event for initialising non-existing branches on a push event.
 *
 * <p>
 * The event handles the creation of all non-existing branches that are received via a
 * {@link CommitWebhookData}. It will then create the branch and save it. A branch is uniquely
 * identified by a repository and branch name.
 * </p>
 *
 * <p>
 * This will set a new branch to the oldest commit from the web hook!
 * It is unknown what the other oldest commit is without traversing the entire commit tree.
 * </p>
 *
 * <p>
 * Dependencies:
 * - {@link CommitInitEvent}
 * - {@link RepositoryInitEvent}
 * </p>
 */
@Component
@SuppressWarnings({"ClassFanOutComplexity"})
public class BranchInitEventCommit implements Event<EmptyWebhookData> {

    /**
     * The priority of the current event.
     */
    private static final int PRIORITY = 270;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Helper class to copy over metric results for newly created branches.
     */
    @Autowired
    private BranchInitMetricLinkHelper branchInitMetricLinkHelper;

    /**
     * {@link GitCommit} helper used to find the latest GitLab commit on a branch.
     */
    @Autowired
    private GitCommitHelper gitCommitHelper;

    @Override
    public EmptyWebhookData handle(EventType type, AbstractWebhookData webhook)
            throws IllegalStateException, IllegalArgumentException {
        if (!(webhook instanceof CommitWebhookData)) {
            String msg = "BranchInitEventCommit.handle: Only supports CommitWebhookData";
            throw new IllegalArgumentException(msg);
        }

        CommitWebhookData data = (CommitWebhookData) webhook;

        // Get the repository.
        Integer gitLabId = data.getRepositoryId();
        Repository repository = this.repositoryRepository.findByGitLabId(
                gitLabId
        ).orElseThrow(() -> {
            String msg = "BranchInitEventCommit.handle: "
                    + "Cannot find repository with GitLab ID " + gitLabId;

            return new IllegalStateException(msg);
        });

        boolean exists = this.branchRepository.existsByNameAndRepositoryId(
                data.getBranchName(),
                repository.getId()
        );

        if (exists) {
            return new EmptyWebhookData();
        }

        // Get the last commit
        GitCommit commit = null;
        if (data.getCommits() == null || data.getCommits().size() < 1) {
            // Fetch the commit from GitLab.
            try {
                commit = this.gitCommitHelper.getLatestFromGitLab(repository, data.getBranchName());
            } catch (GitLabApiException e) {
                String msg = "BranchInitEventCommit.handle: Failed to get latest commit "
                        + "from GitLab. " + e.getMessage();
                throw new IllegalStateException(msg);
            }
        } else {
            // Get the oldest commit.
            data.getCommits().sort(Comparator.comparingInt(GitCommitWeb::getTimestamp));

            // Find the oldest commit in the database.
            String hash = data.getCommits().get(0).getHash();
            commit = this.gitCommitRepository.findByHash(hash)
                .orElseThrow(() -> {
                    String msg = "BranchInitEventCommit.handle: Commit not found with hash " + hash;

                    return new IllegalStateException(msg);
                });
        }

        // Create the new branch and save it
        Branch newBranch = new Branch(data.getBranchName(), repository, commit);
        this.branchRepository.save(newBranch);

        // Call handler to initialise the metric results for the new branch (if it was branched off)
        this.branchInitMetricLinkHelper.initialiseBranch(newBranch);

        return new EmptyWebhookData();
    }

    @Override
    public int getPriority() {
        return BranchInitEventCommit.PRIORITY;
    }

    @Override
    public List<EventType> getCompatibleEvents() {
        return Collections.singletonList(EventType.PUSH_EVENT);
    }
}
