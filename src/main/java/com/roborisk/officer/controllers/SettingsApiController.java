package com.roborisk.officer.controllers;

import com.roborisk.officer.helpers.SettingsApiDataHelper;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.RobotSettings;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.MetricSettingsWeb;
import com.roborisk.officer.models.web.RepositoryToggleWeb;
import com.roborisk.officer.models.web.RobotSettingsWeb;
import com.roborisk.officer.models.web.SettingsApiData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Controller for handling all API requests related to settings.
 */
@RestController
public class SettingsApiController {

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Handles GET requests to "/settings/repository/{repositoryId}". The control panel fires a
     * GET request to this resource to request the settings associated to a repository.
     *
     * @param repositoryId The repository.repositoryId of the repository for which the settings
     *                     are being requested.
     * @return             {@link ResponseEntity} containing {@link HttpStatus}
     *                     and a {@link SettingsApiData} object with all the settings associated
     *                     to the repository with repository.repositoryId=repositoryId.
     */
    @RequestMapping(value = "/settings/repository/{repositoryId}", method = RequestMethod.GET,
            produces = "application/json;charset=UTF-8")
    public ResponseEntity<SettingsApiData> getRepository(@PathVariable Integer repositoryId) {
        SettingsApiData settingsApiData = new SettingsApiData();

        // check if the repository exists. Otherwise sends NOT_FOUND
        if (repositoryId != null) {
            if (!this.repositoryRepository.existsById(repositoryId)) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }

        // grab current robot settings
        Optional<RobotSettingsWeb> robotSettingsWeb = SettingsApiDataHelper
                        .getCurrentRobotSettingsAsWeb(repositoryId);

        if (robotSettingsWeb.isEmpty()) {
            // this should never happen as each repository should always have
            // associated robotSettings
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        settingsApiData.setRobotSettings(robotSettingsWeb.get());

        // grab current metric settings
        List<MetricSettingsWeb> metricSettingsWeb = SettingsApiDataHelper
                        .getCurrentMetricSettingsAsWeb(repositoryId);

        settingsApiData.setMetricSettings(metricSettingsWeb);

        // set the name of the repository
        if (repositoryId != null) {
            Optional<Repository> repo = this.repositoryRepository.findById(repositoryId);
            settingsApiData.setName(repo.get().getName());
        } else {
            settingsApiData.setName("Default");
        }

        return new ResponseEntity<>(settingsApiData, HttpStatus.OK);
    }

    /**
     * Handles PUT requests to "/settings/repository/{repositoryId}". The control panel fires
     * a PUT request to this resource to update the settings associated to a repository.
     *
     * @param repositoryId The repository.repositoryId of the repository for which the settings
     *                     are being updated.
     * @param data         The {@link SettingsApiData} object containing the settings
     *                     which are to be updated.
     * @return             {@link ResponseEntity} containing {@link HttpStatus}.
     */
    @RequestMapping(value = "/settings/repository/{repositoryId}", method = RequestMethod.PUT,
            consumes = "application/json;charset=UTF-8")
    public ResponseEntity<HttpStatus> putRepository(
             @RequestBody SettingsApiData data,
             @PathVariable Integer repositoryId) {
        // check if the repository exists. Otherwise sends NOT_FOUND
        if (repositoryId != null) {
            if (!this.repositoryRepository.existsById(repositoryId)) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }

        Map<Integer, MetricSettings> allCurrentMetricSettings;
        RobotSettingsWeb newRobotSettingsWeb = data.getRobotSettings();
        Optional<RobotSettings> currentRobotSettings;

        try {
            allCurrentMetricSettings = SettingsApiDataHelper.getMetricSettings(data, repositoryId);

            currentRobotSettings = SettingsApiDataHelper.getRobotSettings(data, repositoryId);
        } catch (IllegalStateException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        // if update deemed valid, do update (otherwise this point is not reached)
        if (newRobotSettingsWeb != null) {
            SettingsApiDataHelper.updateRobotSettings(
                    newRobotSettingsWeb,
                    currentRobotSettings.get()
            );
        }

        SettingsApiDataHelper.updateMetricSettings(
                data.getMetricSettings(),
                allCurrentMetricSettings
        );

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Handles GET requests to "/settings/default". The control panel fires a GET request to
     * this resource to request the default settings.
     *
     * @return {@link ResponseEntity} containing HttpStatus and a {@link SettingsApiData}
     *     object with all the settings associated to the repository with repository.id = null.
     */
    @RequestMapping(value = "/settings/default", method = RequestMethod.GET, produces =
            "application/json;charset=UTF-8")
    public ResponseEntity<SettingsApiData> getDefault() {
        return this.getRepository(null);
    }

    /**
     * Handles PUT requests to "/settings/default". The control panel fires a PUT request
     * to this resource to update the default settings.
     *
     * @param data The {@link SettingsApiData} object containing
     *             the settings which are to be updated.
     * @return     {@link ResponseEntity} containing {@link HttpStatus}.
     */
    @RequestMapping(value = "/settings/default", method = RequestMethod.PUT, consumes =
            "application/json;charset=UTF-8")
    public ResponseEntity<HttpStatus> putDefault(@RequestBody SettingsApiData data) {
        return this.putRepository(data, null);
    }

    /**
     * Handles the resource "/settings/repository/{repositoryId}/toggle". The control panel
     * fires a PUT request to this resource to toggle the robot on a specific repository.
     *
     * @param repositoryId The repository.repositoryId of the repository on which the robot is
     *                     to be toggled.
     * @return             {@link ResponseEntity} containing HttpStatus and json indicating
     *                     whether the robot is enabled on the repository
     *                     after the toggle behind key "enabled".
     */
    @RequestMapping(value = "/settings/repository/{repositoryId}/toggle", method =
            RequestMethod.PUT, consumes = "application/json;charset=UTF-8")
    public ResponseEntity<RepositoryToggleWeb> toggleRepository(
                @PathVariable Integer repositoryId
    ) {
        Optional<Repository> repo = this.repositoryRepository.findById(repositoryId);

        if (repo.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Repository repository = repo.get();
        boolean isEnabled = !repository.getIsEnabled();
        repository.setIsEnabled(isEnabled);

        this.repositoryRepository.save(repository);

        RepositoryToggleWeb response = new RepositoryToggleWeb();
        response.setIsEnabled(isEnabled);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
