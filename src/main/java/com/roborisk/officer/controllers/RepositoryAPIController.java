package com.roborisk.officer.controllers;

import com.roborisk.officer.helpers.RepositoryHelper;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.RepositoryApiWeb;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Repository Controller for sending the repository
 * list to the front end.
 */
@RestController
public class RepositoryAPIController {

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Helper for the controller.
     */
    @Autowired
    private RepositoryHelper repositoryHelper;

    /**
     * Handles GET requests to "repository/list". The control panel fires a GET
     * request to this resource to request the repositories in the DB.
     *
     * @param pageNo                Page number.
     * @param pageSize              Page size.
     * @param sortBy                Parameter to sort by.
     * @param autoFill              Whether to use the auto fill functionality or not.
     * @throws                      GitLabApiException if there is a problem with
     *                              the GitLabApiWrapper.
     * @return                      ResponseEntity containing HttpStatus and a List of
     *                              {@link RepositoryApiWeb} objects containing all repositories.
     */
    @RequestMapping(value = "/repository/list", method = RequestMethod.GET,
            produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<Repository>> list(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "namespace") String sortBy,
            @RequestParam(defaultValue = "false") boolean autoFill
    ) throws GitLabApiException {
        List<RepositoryApiWeb> repositoryWebs = new ArrayList<>();

        // check if new repositories were created
        if (autoFill) {
            this.repositoryHelper.checkAndStoreNewRepositories();
        }

        // get all repositories from the database
        List<Repository> repositories = this.repositoryHelper
                .getAllRepositoryRequests(pageNo, pageSize, sortBy);

        // create a RepositoryWeb object for each Repository item and add it to the list
        for (Repository rep : repositories) {
            RepositoryApiWeb repositoryWeb = rep.getApiWebObject();

            repositoryWebs.add(repositoryWeb);
        }

        // Return 404 when nothing is found and page number is higher than 0.
        if (repositoryWebs.size() == 0 && pageNo > 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        // Add pagination header with the number of elements.
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Total", Long.toString(this.repositoryRepository.count()));
        headers.add("X-Total-Pages", Integer.toString(this.repositoryHelper
                .getTotalPages(pageNo, pageSize, sortBy)));

        return new ResponseEntity(repositoryWebs, headers, HttpStatus.OK);
    }
}
