package com.roborisk.officer.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Hello World Controller for displaying a welcome message.
 */
@RestController
public class HelloSpringController {

    /**
     * The welcome message to display.
     */
    public static final String WELCOME_MESSAGE = "Hello Spring Boot on Docker!!! (with api access)";

    /**
     * Handles the "/" resource.
     *
     * @return A welcome message.
     */
    @RequestMapping("/")
    public String index() {
        return WELCOME_MESSAGE;
    }
}
