package com.roborisk.officer.controllers;

import com.roborisk.officer.helpers.MergeRequestHelper;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.web.MergeRequestWeb;
import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * API Controller for handling all incoming MergeRequest related requests from the Control Panel.
 */
@RestController
public class MergeRequestApiController {

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Helper for the {@link MergeRequest} model.
     */
    @Autowired
    private MergeRequestHelper mergeRequestHelper;

    /**
     * Handles GET requests to "/merge-request/{mergeRequestId}".
     * The control panel fires a GET request to this resource to request the single merge request.
     *
     * @param mergeRequestId    The id of the merge request which is being requested.
     * @return                  {@link ResponseEntity} containing an {@link HttpStatus} and a
     *                              {@link MergeRequestWeb} object with all the details associated
     *                              to the merge request with mergeRequest.id == mergeRequestId.
     */
    @RequestMapping(value = "/merge-request/{mergeRequestId}", method = RequestMethod.GET,
            produces = "application/json;charset=UTF-8")
    public ResponseEntity<MergeRequestWeb> get(@PathVariable Integer mergeRequestId) {
        // Get the merge request from the database
        Optional<MergeRequest> mergeRequest = this.mergeRequestRepository.findById(mergeRequestId);

        // Handle request when the parameter id does not represent an existent merge request
        if (mergeRequest.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        // Create the mergeRequestWeb object from the merge request with given id
        MergeRequestWeb mergeRequestWeb = mergeRequest.get().getWebObject();

        return new ResponseEntity<>(mergeRequestWeb, HttpStatus.OK);
    }

    /**
     * Handles GET requests to "/merge-request/list".
     * The control panel fires a GET request to this resource to request the
     * paginated list of all merge requests.
     *
     * @param pageNo    The number of the page that merge requests should be listed for.
     * @param pageSize  The number of merge requests that is desired per page.
     * @param sortBy    The name of the attribute that merge requests should be sorted on.
     * @return          A {@link ResponseEntity} containing an {@link HttpStatus} and a {@link List}
     *                      of {@link MergeRequestWeb} objects for the requested page.
     */
    @RequestMapping(value = "/merge-request/list", method = RequestMethod.GET,
            produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<MergeRequestWeb>> list(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy) {
        // List storing MergeRequestWeb objects that should be returned.
        List<MergeRequestWeb> mergeRequestWebs = new ArrayList<>();
        // Get all relevant MergeRequest objects from the database
        List<MergeRequest> mergeRequests = this.mergeRequestHelper
                .getAllMergeRequests(this.mergeRequestRepository, pageNo, pageSize, sortBy);

        // Create the MergeRequestWeb objects that should be returned
        for (MergeRequest mr : mergeRequests) {
            // For each found merge request from the database, create a web object and add
            // it to the list to be passed to the returned response entity
            MergeRequestWeb mergeRequestWeb = mr.getWebObject();
            mergeRequestWebs.add(mergeRequestWeb);
        }

        // Return 404 when nothing is found and page number is higher than 0.
        if (mergeRequestWebs.size() == 0 && pageNo > 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(mergeRequestWebs, HttpStatus.OK);
    }

    /**
     * This is a "Could have", so it will not be implemented in a timely manner.
     *
     * @param id                            An id.
     * @throws NotImplementedException      When method is called.
     */
    @RequestMapping(value = "/merge-request/flag/{id}", method = RequestMethod.DELETE)
    public void deleteFlag(int id) throws NotImplementedException {
        throw new NotImplementedException("MergeRequestApiController.deleteFlag: "
                + "Not yet implemented");
    }
}
