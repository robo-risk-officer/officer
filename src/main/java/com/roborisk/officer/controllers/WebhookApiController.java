package com.roborisk.officer.controllers;

import com.roborisk.officer.handlers.Dispatcher;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.MergeRequestWebhookData;
import com.roborisk.officer.models.web.SonarWebhookData;
import com.roborisk.officer.modules.metrics.sonar.SonarDataParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The Webhook API Controller for parsing all incoming webhook data from GitLab and SonarQube.
 */
@Slf4j
@RestController
public class WebhookApiController {

    /**
     * AutoWired {@link Dispatcher} singleton instance.
     */
    @Autowired
    private Dispatcher dispatcher;

    /**
     * Autowired {@link SonarDataParser} instance to handle SonarQube webhooks.
     */
    @Autowired
    private SonarDataParser sonarDataParser;

    /**
     * Handles the "/webhook/comments" resource.
     * RRO repositories fire to this resource when a comment is placed.
     *
     * @param data  The body of the request, parsed to CommentWebhookData by Spring.
     * @return      ResponseEntity containing HttpStatus.
     */
    @RequestMapping(value = "/webhook/comments", method = RequestMethod.POST,
            consumes = "application/json;charset=UTF-8")
    public ResponseEntity<CommentWebhookData> comments(
            @RequestBody CommentWebhookData data) {

        this.dispatcher.handle(data);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Handles the "/webhook/merge-request" resource.
     * RRO repositories fire to this resource when a merge request is created/updated/merged.
     *
     * @param data  The body of the request, parsed to MergeRequestWebhookData by Spring.
     * @return      ResponseEntity containing HttpStatus.
     */
    @RequestMapping(value = "/webhook/merge-request", method = RequestMethod.POST,
            consumes = "application/json;charset=UTF-8")
    public ResponseEntity<MergeRequestWebhookData> mergeRequest(
                @RequestBody MergeRequestWebhookData data) {

        this.dispatcher.handle(data);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Handles the "/webhook/commit" resource.
     * RRO repositories fire to this resource when a push to a repository (except tags) occurs.
     *
     * @param data  The body of the request, parsed to CommitWebhookData by Spring.
     * @return      ResponseEntity containing HttpStatus.
     */
    @RequestMapping(value = "/webhook/commit", method = RequestMethod.POST,
            consumes = "application/json;charset=UTF-8")
    public ResponseEntity<CommitWebhookData> commit(
            @RequestBody CommitWebhookData data) {

        this.dispatcher.handle(data);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Handles the "/webhook/sonar" resource.
     * After SonarQube is done analysing a project, a webhook will be sent to notify the RRO
     * that the analysis is done such that the review can be completed.
     *
     * @param data  The body of the request, parsed to JsonNode by Spring.
     * @return      ResponseEntity containing HttpStatus.
     */
    @RequestMapping(value = "/webhook/sonar", method = RequestMethod.POST,
            consumes = "application/json;charset=UTF-8")
    public ResponseEntity<SonarWebhookData> sonarAnalysisCompleted(
                                                        @RequestBody SonarWebhookData data,
                                                        @RequestBody String bodyString,
                                                        @Value("${officer.sonar.webhook_key}")
                                                        String ourWebhookKey,
                                                        @RequestHeader(
                                                           "X-Sonar-Webhook-HMAC-SHA256"
                                                        ) String receivedSignature) {
        // Make sure that the request actually comes from SonarQube
        String expectedSignature = new HmacUtils(HmacAlgorithms.HMAC_SHA_256, ourWebhookKey)
                .hmacHex(bodyString);
        if (!expectedSignature.equals(receivedSignature)) {
            log.error("WebhookApiController.sonarAnalysisCompleted: "
                    + "An unauthorised request was made to the SonarQube webhook");

            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        // Handle SonarQube webhook
        try {
            this.sonarDataParser.handleSonarWebhook(data);
        } catch (Exception e) {
            log.error("Failed to handle SonarQube webhook. Reason: " + e.getMessage());
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
