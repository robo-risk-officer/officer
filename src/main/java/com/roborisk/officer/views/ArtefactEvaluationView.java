package com.roborisk.officer.views;

import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.metrics.MergeRequestMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import com.roborisk.officer.views.metrics.GeneralMetricEvaluationView;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Simple evaluation view which creates a comment for multiple metrics evaluated over multiple
 * merge request reviews for a single (!) code artefact.
 *
 * @param <T> The type of the results which are being commented upon. (It is important that the
 *           evaluated results are not only for a single artefact type but for a single artefact
 *           of that type. Otherwise the comment makes no sense.)
 */
@Component
public class ArtefactEvaluationView<T extends MergeRequestMetricResult> {

    /**
     * Generates a string for a single metric result with a generic score name.
     *
     * @param results               List of metric results for the artefact for which the string
     *                              should be generated.
     * @param metricSettings        List of {@link MetricSettings} for the artefact for which the
     *                              string should be generated.
     * @param views                 Map of {@link Metric} and {@link GeneralMetricEvaluationView}
     *                              representing the mapping between which metric needs to be
     *                              generated with which view.
     * @param mergeRequest          The {@link MergeRequest} to generate the comment for.
     * @throws IOException          When there is an exception happening with file handling.
     * @throws GitLabApiException   If any exception occurs within the {@link GitLabApiWrapper}.
     * @return                      {@link String} describing the change of the metric result in
     *                              human language.
     */
    public String generateComment(List<T> results, List<MetricSettings> metricSettings,
                                    Map<Metric, GeneralMetricEvaluationView> views,
                                    MergeRequest mergeRequest)
                                    throws IOException, GitLabApiException {
        // map that stores all results per commit in sorted order.
        Map<Metric, SortedMap<GitCommit, T>> resultsPerMetric = new HashMap<>();

        // all commits in sorted order
        SortedSet<GitCommit> allHistoricCommits = new TreeSet<>();

        // map that stores the settings associated to each metric.
        Map<Metric, MetricSettings> mappedSettings = new HashMap<>();

        for (T result : results) {
            if (!resultsPerMetric.containsKey(result.getMetric())) {
                resultsPerMetric.put(result.getMetric(), new TreeMap<>());
            }
            resultsPerMetric.get(result.getMetric()).put(result.getAssociatedCommit(), result);
            allHistoricCommits.add(result.getAssociatedCommit());

            if (!mappedSettings.containsKey(result.getMetric())) {
                mappedSettings.put(result.getMetric(), this.findMetricSetting(metricSettings,
                        result.getMetric()).orElse(null));
            }
        }

        StringBuilder builder = new StringBuilder();

        // append overall review
        builder.append(this.getOverallReview(resultsPerMetric, allHistoricCommits));
        builder.append("\n");

        // Sort metrics on name
        List<Metric> metrics = new ArrayList<>(resultsPerMetric.keySet());
        metrics.sort(Comparator.comparing(Metric::getDisplayName));

        // append metric specific review
        for (Metric metric : metrics) {
            builder.append(views.get(metric).getMetricReview(metric,
                    resultsPerMetric.get(metric), mappedSettings.get(metric), mergeRequest));
        }

        // Finally, return the comment
        return builder.toString();
    }

    /**
     * Returns the {@link MetricSettings} object which is associated to the specified metric.
     *
     * @param metricSettings A list of all {@link MetricSettings}.
     * @param metric         The {@link Metric} for which the {@link MetricSettings} object is
     *                       wanted.
     * @return               An {@link Optional} with (first) {@link MetricSettings} object which
     *                       is associated to the specified {@link Metric}.
     */
    private Optional<MetricSettings> findMetricSetting(List<MetricSettings> metricSettings,
                                                       Metric metric) {
        for (MetricSettings setting : metricSettings) {
            if (setting.getMetric().getId().equals(metric.getId())) {
                return Optional.of(setting);
            }
        }

        return Optional.empty();
    }

    /**
     * Returns the number of results associated to one commit that have been determined to be of
     * low quality.
     *
     * @param resultsPerMetric  Map from a metric to a map from all commits to the result which is
     *                          associated to the metric and commit.
     * @param commit            The commit for which we want to get the number of low quality
     *                          evaluations.
     * @return                  The number of low quality evaluations for the given commit.
     */
    private int getNoLowQualityResultsForCommit(Map<Metric,
                                                SortedMap<GitCommit, T>> resultsPerMetric,
                                                GitCommit commit) {
        int counter = 0;
        for (Metric metric : resultsPerMetric.keySet()) {
            if (resultsPerMetric.get(metric).containsKey(commit)
                        && resultsPerMetric.get(metric).get(commit).getIsLowQuality()) {
                counter++;
            }
        }

        return counter;
    }

    /**
     * Returns an overall review of all the results for the given observation.
     *
     * @param resultsPerMetric      Map from a metric to a map from all commits to the result which
     *                              is associated to the metric and commit.
     * @param allHistoricCommits    A {@link SortedSet} which contains all commits that are being
     *                              evaluated.
     * @return                      A string that can be posted in a GitLab comment.
     */
    private String getOverallReview(Map<Metric, SortedMap<GitCommit, T>> resultsPerMetric,
                                    SortedSet<GitCommit> allHistoricCommits) {
        if (allHistoricCommits.isEmpty()) {
            throw new IllegalArgumentException("EvaluationView.getOverallReview: An overall "
                    + "review cannot be generated without any commits. allHistoricCommits is "
                    + "empty.");
        }

        if (resultsPerMetric.isEmpty()) {
            throw new IllegalArgumentException("EvaluationView.getOverallReview: An overall "
                    + "review cannot be generated without any results. resultsPerMetric is "
                    + "empty.");
        }

        Iterator<GitCommit> it = allHistoricCommits.iterator();

        GitCommit firstCommit = it.next();
        int currentLowQuality = this.getNoLowQualityResultsForCommit(resultsPerMetric, firstCommit);

        Metric firstMetric = resultsPerMetric.keySet().iterator().next();

        StringBuilder comment = new StringBuilder();

        comment.append(String.format("# Review of %s \n ",
                resultsPerMetric.get(firstMetric).get(firstCommit).getArtefactName()));

        if (!it.hasNext()) {
            comment.append("Currently, the number of metrics evaluating to low quality is ")
                    .append(currentLowQuality)
                    .append(".");

            return comment.toString();
        }

        GitCommit secondCommit = it.next();
        int previousLowQuality = this.getNoLowQualityResultsForCommit(
                    resultsPerMetric, secondCommit
        );

        if (currentLowQuality != previousLowQuality) {
            comment.append("Overall the latest commits have");
            comment.append(currentLowQuality < previousLowQuality ? " reduced" : " increased");
            comment.append(String.format(" the number of metrics that evaluate to low quality from"
                    + " %d to %d.", previousLowQuality, currentLowQuality));
        } else {
            comment.append(String.format("The number of metrics that evaluate to low quality has "
                    + "not changed since the previous commit. It is still "
                    + "%d.", currentLowQuality));
        }

        return comment.toString();
    }
}
