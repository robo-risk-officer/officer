package com.roborisk.officer.views;

import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.repositories.MergeRequestClassMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestCodebaseMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestFileMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestFunctionMetricResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Helper for the {@link com.roborisk.officer.events.GenerateConclusionCommentEvent}.
 */
@Component
@Scope("singleton")
public class EvaluationViewHelper {

    /**
     * Repository to for the
     * {@link com.roborisk.officer.models.database.metrics.MergeRequestClassMetricResult}s.
     */
    @Autowired
    private MergeRequestClassMetricResultRepository mergeRequestClassMetricResultRepository;

    /**
     * Repository to for the
     * {@link com.roborisk.officer.models.database.metrics.MergeRequestCodebaseMetricResult}s.
     */
    @Autowired
    private MergeRequestCodebaseMetricResultRepository mergeRequestCodebaseMetricResultRepository;

    /**
     * Repository to for the
     * {@link com.roborisk.officer.models.database.metrics.MergeRequestFileMetricResult}s.
     */
    @Autowired
    private MergeRequestFileMetricResultRepository mergeRequestFileMetricResultRepository;

    /**
     * Repository to for the
     * {@link com.roborisk.officer.models.database.metrics.MergeRequestFunctionMetricResult}s.
     */
    @Autowired
    private MergeRequestFunctionMetricResultRepository mergeRequestFunctionMetricResultRepository;


    /**
     * Checks all results associated to the review and checks if there are any that have a score
     * of null. If that is the case the {@link MergeRequestReview} still has metrics that are
     * calculating.
     *
     * @param mergeRequestReview    The {@link MergeRequestReview} which should be checked.
     * @return                      Whether the given {@link MergeRequestReview} still has
     *                              calculating results.
     */
    public boolean mergeRequestReviewIsCalculating(MergeRequestReview mergeRequestReview) {
        boolean codebaseIsCalculating = this.mergeRequestCodebaseMetricResultRepository
                .existsByMergeRequestReviewAndScoreIsNull(mergeRequestReview);

        boolean fileIsCalculating = this.mergeRequestFileMetricResultRepository
                .existsByMergeRequestFileMergeRequestReviewAndScoreIsNull(mergeRequestReview);

        boolean classIsCalculating = this.mergeRequestClassMetricResultRepository
                .findAllNullScoreLinkedToReview(mergeRequestReview.getId()).size() > 0;

        boolean functionIsCalculating = this.mergeRequestFunctionMetricResultRepository
                .findAllNullScoreLinkedToReview(mergeRequestReview.getId()).size() > 0;

        return codebaseIsCalculating || fileIsCalculating || classIsCalculating
                || functionIsCalculating;
    }
}
