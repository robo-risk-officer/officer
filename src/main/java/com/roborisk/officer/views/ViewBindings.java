package com.roborisk.officer.views;

import com.roborisk.officer.helpers.CommentHelper;
import com.roborisk.officer.helpers.UploadWrapper;
import com.roborisk.officer.models.database.metrics.MergeRequestClassMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestCodebaseMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestFileMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestFunctionMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.enums.MetricTypeEnum;
import com.roborisk.officer.views.metrics.CodeChurnView;
import com.roborisk.officer.views.metrics.GeneralMetricEvaluationView;
import com.roborisk.officer.views.metrics.SonarVulnerabilitiesView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * This class translates {@link Metric}s to {@link GeneralMetricEvaluationView}s.
 * Each metric can (but doesn't have to) specify the specific view it wants to use in the database
 * under the field metric.view. The string in that field should correspond to the identifier which
 * is then put in the switch-case statement. Based on that this class can return a new instance of
 * the view that is required for the {@link Metric}.
 *
 * <p>As some {@link GeneralMetricEvaluationView}s need to upload files, they need an
 * {@link UploadWrapper} which specifies an {@link CommentHelper} to upload the files and the
 * GitLab repository id to which they should be uploaded. As the GitLab ID of the repository may
 * vary for queries which are executed in parallel, a new {@link GeneralMetricEvaluationView} has
 * mvto be created per request.</p>
 */
@Component
@Scope("singleton")
public class ViewBindings {

    /**
     * The {@link CommentHelper} which will be used in the {@link UploadWrapper} to allow the views
     * to upload files. For exact usage see class JavaDoc.
     */
    @Autowired
    private CommentHelper commentHelper;

    /**
     * Create a {@link GeneralMetricEvaluationView} of {@link MetricTypeEnum} type and pass in the
     * {@link UploadWrapper}.
     *
     * @param type                      The type of the {@link GeneralMetricEvaluationView}.
     * @param wrapper                   The {@link UploadWrapper} the view is going to get.
     * @throws IllegalArgumentException If the metric type is not supported.
     * @return                          The {@link GeneralMetricEvaluationView}.
     */
    private GeneralMetricEvaluationView createGeneralMetricEvaluationView(MetricTypeEnum type,
            UploadWrapper wrapper) {
        // Create the right GeneralMetricEvaluationView specifying the right generic type based
        // on the parameter type.
        switch (type) {
            case CODEBASE:
                return new GeneralMetricEvaluationView<MergeRequestCodebaseMetricResult>(wrapper);

            case FILE:
                return new GeneralMetricEvaluationView<MergeRequestFileMetricResult>(wrapper);

            case CLASS:
                return new GeneralMetricEvaluationView<MergeRequestClassMetricResult>(wrapper);

            case FUNCTION:
                return new GeneralMetricEvaluationView<MergeRequestFunctionMetricResult>(wrapper);

            default:
                String msg = "ViewBindings.createGeneralMetricEvaluationView: "
                        + "GeneralMetricEvaluationView requested for unsupported type "
                        + type.toString();

                throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Create a {@link CodeChurnView} of {@link MetricTypeEnum} type and pass in the
     * {@link UploadWrapper}.
     *
     * @param type                      The type of the {@link CodeChurnView}.
     * @param wrapper                   The {@link UploadWrapper} the view is going to get.
     * @throws IllegalArgumentException If the metric is of type CODEBASE which is not supported
     *                                  for code churn.
     * @return                          The {@link CodeChurnView}.
     */
    private CodeChurnView createCodeChurnView(MetricTypeEnum type, UploadWrapper wrapper) {
        // create the right GeneralMetricEvaluationView specifying the right generic type based
        // on the parameter type.
        switch (type) {
            case FILE:
                return new CodeChurnView<MergeRequestFileMetricResult>(wrapper);

            case CLASS:
                return new CodeChurnView<MergeRequestClassMetricResult>(wrapper);

            case FUNCTION:
                return new CodeChurnView<MergeRequestFunctionMetricResult>(wrapper);

            default:
                String msg = "ViewBindings.createCodeChurnView: CodeChurnView requested for "
                        + "unsupported type " + type.toString();

                throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Get a new {@link GeneralMetricEvaluationView} for a {@link Metric} based on the metrics view
     * and it's {@link MetricTypeEnum}.
     *
     * @param metric                    The {@link Metric} for which the
     *                                  {@link GeneralMetricEvaluationView}
     *                                  should be created.
     * @param gitLabId                  The ID of the GitLab repository to which the
     *                                  {@link GeneralMetricEvaluationView} may want to upload.
     * @throws IllegalArgumentException If a view type is requested which is not supported for
     *                                  the metric type.
     * @return                          A new {@link GeneralMetricEvaluationView} made for the
     *                                  specific {@link Metric}.
     */
    public GeneralMetricEvaluationView getViewForMetric(Metric metric, int gitLabId) {
        // Make sure the value we are checking is non-null.
        String requestedView = metric.getViewIdentifier();
        if (metric.getViewIdentifier() == null) {
            requestedView = "default";
        }

        // Create the upload wrapper used for the view.
        UploadWrapper wrapper = new UploadWrapper() {
            @Override
            protected int getGitLabId() {
                return gitLabId;
            }

            @Override
            protected CommentHelper getCommentHelper() {
                return ViewBindings.this.commentHelper;
            }
        };

        switch (requestedView) {
            case ("GeneralMetricReevaluationView"):
                // This is specified separately from the default as the default may change but
                // some views may specifically want to have the GeneralMetricEvaluationView
                // instead of a more specific one.
                return this.createGeneralMetricEvaluationView(metric.getMetricType(), wrapper);
            case ("CodeChurnView"):
                // The createCodeChurnView throws an exception if the metric is of type CODEBASE
                // as that is not supported for code churn.
                return this.createCodeChurnView(metric.getMetricType(), wrapper);
            case ("SonarVulnerabilitiesView"):
                if (metric.getMetricType() != MetricTypeEnum.FILE) {
                    String msg = "ViewBindings.getViewForMetric: Cannot return "
                            + "SonarVulnerabilitiesView for MetricTypeEnum "
                            + metric.getMetricType();

                    throw new IllegalArgumentException(msg);
                }

                return new SonarVulnerabilitiesView(wrapper);
            default:
                return this.createGeneralMetricEvaluationView(metric.getMetricType(), wrapper);
        }
    }
}
