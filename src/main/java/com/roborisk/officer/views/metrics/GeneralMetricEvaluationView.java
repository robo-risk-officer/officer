package com.roborisk.officer.views.metrics;

import com.roborisk.officer.helpers.UploadWrapper;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.metrics.MergeRequestMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import lombok.Setter;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.FileUpload;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYShapeAnnotation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.Range;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.SortedMap;

/**
 * General reevaluation view based on {@link MergeRequestMetricResult}.
 * It will generate a view that will display the quality over time for a general {@link Metric}.
 *
 * @param <T> The class extending the {@link MergeRequestMetricResult} that this view will be
 *            generated for.
 */
@SuppressWarnings({"ClassFanOutComplexity"})
public class GeneralMetricEvaluationView<T extends MergeRequestMetricResult> {

    /**
     * Badge to display that metric says the artefact is good quality.
     */
    protected static final String PASS_BADGE =
            "![pass](https://img.shields.io/badge/-pass-green)";

    /**
     * Badge to display that metric says the artefact is low quality.
     */
    protected static final String FAIL_BADGE =
            "![fail](https://img.shields.io/badge/-fail-red)";

    /**
     * Badge to display that metric results improved.
     */
    protected static final String IMPROVE_BADGE =
            "![improved](https://img.shields.io/badge/-improved-green)";

    /**
     * Badge to display that metric results worsened.
     */
    protected static final String WORSENED_BADGE =
            "![worsened](https://img.shields.io/badge/-worsened-red)";

    /**
     * Badge to display that metric results are being calculated.
     */
    protected static final String CALCULATING_BADGE =
            "![calculating](https://img.shields.io/badge/-calculating-lightgrey)";

    /**
     * {@link UploadWrapper} which is aware of the repository to which should be uploaded and can
     * thus upload the required images.
     */
    @Setter
    private UploadWrapper uploadWrapper;

    /**
     * Constructor which does not specify an {@link UploadWrapper} yet. This will have to be done
     * later via the setter.
     */
    public GeneralMetricEvaluationView() {}

    /**
     * Constructor which sets the {@link UploadWrapper}.
     *
     * @param uploadWrapper The {@link UploadWrapper} to be used to upload files.
     */
    public GeneralMetricEvaluationView(UploadWrapper uploadWrapper) {
        this.uploadWrapper = uploadWrapper;
    }

    /**
     * Returns a markdown/HTML string that can be posted towards the GitLab frontend as a comment
     * containing the review of the passed in results for a single metric over multiple reviews
     * of a single artefact.
     * It is important that all reviews are for one specific artefact based on the same metric
     * within one merge request!
     *
     * @param metric                The metric which is being reviewed.
     * @param results               Sorted map from {@link GitCommit} to the results which have been
     *                              evaluated by metric.
     * @param setting               The {@link MetricSettings} which currently holds for metric
     *                              on the repository which is being reviewed.
     * @param mergeRequest          The {@link MergeRequest} which is being reviewed.
     * @throws IOException          When there is an exception happening with file handling.
     * @throws GitLabApiException   If any exception occurs within the {@link GitLabApiWrapper}.
     * @return                      The string which contains a review.
     */
    public String getMetricReview(Metric metric, SortedMap<GitCommit, T> results,
                                  MetricSettings setting, MergeRequest mergeRequest)
            throws IOException, GitLabApiException {
        StringBuilder comment = new StringBuilder();
        Iterator<GitCommit> it = results.keySet().iterator();

        comment.append("<details close> \n")
                .append("<summary>\n    ")
                .append(this.getDetailsSummary(metric, results, setting, mergeRequest))
                .append("</summary>\n")
                .append("<table><tbody><tr><td>");

        if (results.get(it.next()).getScore() == null) {
            // Metric is still calculating
            comment.append("After calculation is finished, the scores will be updated.");
        }
        comment.append(this.drawGraph(metric, results, setting, mergeRequest, 800, 300)
                .getMarkdown())
                .append("\n \n")
                .append(this.getTextUnderGraph(metric, results, setting, mergeRequest));


        comment.append("\n<details close> \n")
                .append("    <summary> \n")
                .append("        Metric description")
                .append("    </summary> \n")
                .append(metric.getDescription())
                .append("\n</details> \n")
                .append("\n<details close> \n")
                .append("    <summary> \n")
                .append("        Exact data (from most recent to least recent)\n")
                .append("    </summary> \n")
                .append("    <table>\n         <tbody> \n")
                .append(this.getTableHeader(metric, results, setting, mergeRequest))
                .append(this.getTableBody(metric, results, setting, mergeRequest))
                .append("        </tbody>\n    </table> \n")
                .append("</details> \n");

        comment.append("</td></tr></tbody></table>")
                .append("</details>");

        return comment.toString();
    }

    /**
     * Get a text for the more extensive part of the review.
     *
     * @param metric        The {@link Metric} which is being reviewed.
     * @param results       A {@link SortedMap} from the {@link GitCommit}s in decreasing order
     *                      of timestamp to the result for that commit.
     * @param setting       The {@link MetricSettings} for the given {@link Metric} as active in
     *                      the repository for which the review is being made.
     * @param mergeRequest  The {@link MergeRequest} which is being reviewed.
     * @return              A string that can be added to the introduction of the review.
     */
    public String getTextUnderGraph(Metric metric, SortedMap<GitCommit, T> results,
                                    MetricSettings setting, MergeRequest mergeRequest) {
        StringBuilder comment = new StringBuilder();

        String aboveBelow = setting.getThresholdIsUpperBound() ? "below" : "above";

        comment.append(String.format("For this metric, the score is expected"
                + " to be %s the threshold of %.2f.<br>", aboveBelow, setting.getThreshold()));
        return comment.toString();
    }

    /**
     * Get the body for the table of the more extensive review. Some of the
     * parameters are currently not in use but might be used by a child of this class that
     * overrides this specific function.
     *
     * @param metric        The {@link Metric} which is being reviewed.
     * @param results       A {@link SortedMap} from the {@link GitCommit}s in decreasing order
     *                      of timestamp to the result for that commit.
     * @param setting       The {@link MetricSettings} for the given {@link Metric} as active in
     *                      the repository for which the review is being made.
     * @param mergeRequest  The {@link MergeRequest} which is being reviewed.
     * @return              A string that represents the HTML body of the table for the review.
     */
    protected String getTableBody(Metric metric, SortedMap<GitCommit, T> results,
                                  MetricSettings setting, MergeRequest mergeRequest) {
        Iterator<GitCommit> iterator = results.keySet().iterator();
        StringBuilder builder = new StringBuilder();

        while (iterator.hasNext()) {
            GitCommit currentCommit = iterator.next();
            String score;
            String badge;
            if (results.get(currentCommit).getScore() == null) {
                score = "calculating";
                badge = GeneralMetricEvaluationView.CALCULATING_BADGE;
            } else {
                score = String.format("%.2f", results.get(currentCommit).getScore());
                badge = this.getPassFailBadge(results.get(currentCommit).getIsLowQuality());
            }

            builder.append(
                    String.format(
                        "<tr><td><a href=%s>%s</a></t><td>%s</td><td>%s</td><td>%s</td></tr>\n",
                        this.getDiffLink(mergeRequest, currentCommit),
                        this.shortenString(currentCommit.getMessage(), 40),
                        this.getDate(currentCommit.getTimestamp()),
                        score,
                        badge
                    )
            );
        }

        return builder.toString();
    }

    /**
     * Get a summary for the markup detail in which the review will be added. Some of the
     * parameters are currently not in use but might be used by a child of this class that
     * overrides this specific function.
     *
     * @param metric        The {@link Metric} which is being reviewed.
     * @param results       A {@link SortedMap} from the {@link GitCommit}s in decreasing order
     *                      of timestamp to the result for that commit.
     * @param setting       The {@link MetricSettings} for the given {@link Metric} as active in
     *                      the repository for which the review is being made.
     * @param mergeRequest  The {@link MergeRequest} which is being reviewed.
     * @return              A string that can be added to the summary of the detail.
     */
    protected String getDetailsSummary(Metric metric,
                                     SortedMap<GitCommit, T> results,
                                     MetricSettings setting, MergeRequest mergeRequest) {
        Iterator<GitCommit> iterator = results.keySet().iterator();

        // Get the current quality according to the metric.
        GitCommit firstCommit = iterator.next();
        boolean currentQ = results.get(firstCommit).getIsLowQuality();

        if (results.get(firstCommit).getScore() == null) {
            // If the score is still null then the metric is still calculating.
            return metric.getDisplayName() + " "
                    + GeneralMetricEvaluationView.CALCULATING_BADGE;
        } else if (!iterator.hasNext()) {
            // If there is no history, the badge will be determined only on the current quality.
            // Get the corresponding badge.
            return metric.getDisplayName() + " "
                    + this.getPassFailBadge(currentQ);
        } else {
            // If there is a history, get the previous commit quality.
            boolean previousQ = results.get(iterator.next()).getIsLowQuality();

            // Get the corresponding badge.
            return metric.getDisplayName() + " "
                    + this.getPassFailImproveWorsenBadge(currentQ, previousQ);
        }
    }

    /**
     * Get the header for the table of the more extensive review. Some of the
     * parameters are currently not in use but might be used by a child of this class that
     * overrides this specific function.
     *
     * @param metric        The {@link Metric} which is being reviewed.
     * @param results       A {@link SortedMap} from the {@link GitCommit}s in decreasing order
     *                      of timestamp to the result for that commit.
     * @param setting       The {@link MetricSettings} for the given {@link Metric} as active in
     *                      the repository for which the review is being made.
     * @param mergeRequest  The {@link MergeRequest} which is being reviewed.
     * @return              A string that represents the HTML header of the table for the review.
     */
    protected String getTableHeader(Metric metric, SortedMap<GitCommit, T> results,
                                  MetricSettings setting, MergeRequest mergeRequest) {
        return "<tr><th>Message</th><th>Date</th><th>Score</th><th>Evaluation</th></tr>\n";
    }

    /**
     * Returns a relative link that links towards the diff of the given commit in the given
     * mergeRequest.
     *
     * @param mergeRequest  The {@link MergeRequest} for which the link is being created.
     * @param commit        The {@link GitCommit} for which the link is being created.
     * @return              A {@link String} containing the link.
     */
    private String getDiffLink(MergeRequest mergeRequest, GitCommit commit) {
        return String.format("../../merge_requests/%s/diffs?commit_id=%s",
                mergeRequest.getGitLabIid(), commit.getHash());
    }

    /**
     * Takes in a string and a max length and returns either the entire string or a shortened
     * form of the string that ends in ... if the string is too long.
     *
     * @param s         The {@link String} to be shortened.
     * @param maxLength The maxLength that the final string should have.
     * @return          A shortened form of the string that is at most maxLength long.
     */
    private String shortenString(String s, int maxLength) {
        int length = Math.min(s.length(), maxLength);

        if (length > maxLength - 3) {
            return s.substring(0, maxLength - 3) + "...";
        }

        return s;
    }

    /**
     * Converts a {@link Integer} timestamp into a nicely formatted date.
     *
     * @param timestamp The {@link Integer} to be formatted.
     * @return          A string of the format "dd.MM.yyyy, hh:mm:ss", representing the value of
     *                  the timestamp.
     */
    private String getDate(Integer timestamp) {
        // postgresql stores timestamps in seconds while java Timestamp expects milli seconds
        // (thus we multiply by 1000)
        Timestamp stamp = new Timestamp(Long.valueOf(timestamp) * 1000L);
        Date date = new Date(stamp.getTime());
        SimpleDateFormat dt = new SimpleDateFormat("dd.MM.yyyy, hh:mm:ss");

        return dt.format(date);
    }

    /**
     * Returns a string that compiles to a badge in the GitLab comment section that either
     * displays a red fail or a green pass.
     *
     * @param fail  Whether a fail badge should be returned. If false, a pass badge will be
     *              returned.
     * @return      The badge as defined by fail.
     */
    protected String getPassFailBadge(boolean fail) {
        return fail ? GeneralMetricEvaluationView.FAIL_BADGE
                      : GeneralMetricEvaluationView.PASS_BADGE;
    }

    /**
     * Returns a string that compiles to a badge in the GitLab comment section that either
     * displays a red fail, red worsened, green improved or a green pass.
     *
     * @param current   Whether the current evaluation failed.
     * @param previous  Whether the previous evaluation failed.
     * @return          The correct badge based on current and previous.
     */
    protected String getPassFailImproveWorsenBadge(boolean current, boolean previous) {
        if (current && previous) { // both failed
            return GeneralMetricEvaluationView.FAIL_BADGE;
        }

        if (current) { // only current failed
            return GeneralMetricEvaluationView.WORSENED_BADGE;
        }

        if (previous) { // only previous failed
            return GeneralMetricEvaluationView.IMPROVE_BADGE;
        }

        // both did not fail
        return GeneralMetricEvaluationView.PASS_BADGE;
    }

    /**
     * Function to create a XYDataset which can be plotted by the JFreeChart library.
     *
     * @param metric        The metric which is being reviewed.
     * @param results       Sorted map from {@link GitCommit} to the results which have been
     *                      evaluated by metric.
     * @param setting       The {@link MetricSettings} which currently holds for metric
     *                      on the repository which is being reviewed.
     * @param mergeRequest  The {@link MergeRequest} which is being reviewed.
     * @return              A {@link XYDataset} representing the data in results.
     */
    private XYDataset createDataset(Metric metric, SortedMap<GitCommit, T> results,
                                    MetricSettings setting, MergeRequest mergeRequest) {
        XYSeriesCollection result = new XYSeriesCollection();
        XYSeries series = new XYSeries(metric.getDisplayName() + " results");

        double firstTimestamp = results.lastKey().getTimestamp();

        for (GitCommit currentCommit : results.keySet()) {
            double timeframe = (currentCommit.getTimestamp() - firstTimestamp) / (60 * 60 * 24);
            if (results.get(currentCommit).getScore() != null) {
                series.add(timeframe, results.get(currentCommit).getScore());
            }
        }

        result.addSeries(series);

        return result;
    }

    /**
     * Function to create a graph based on the {@link XYDataset} returned by this.createDataset(.
     * ..) which is the uploaded to GitLab. The resulting FileUpload is then being returned.
     *
     * @param metric                The metric which is being reviewed.
     * @param results               Sorted map from {@link GitCommit} to the results which have been
     *                              evaluated by metric.
     * @param setting               The {@link MetricSettings} which currently holds for metric
     *                                  on the repository which is being reviewed.
     * @param mergeRequest          The {@link MergeRequest} which is being reviewed.
     * @param width                 The width that the chart is supposed to have.
     * @param height                The heigth that the chart is supposed to have.
     * @throws IOException          When there is an exception happening with file handling.
     * @throws GitLabApiException   If any exception occurs within the
     *                              {@link com.roborisk.officer.modules.gitlab.GitLabApiWrapper}.
     * @return                      A {@link FileUpload} of the uploaded png.
     */
    public FileUpload drawGraph(Metric metric, SortedMap<GitCommit, T> results,
                                MetricSettings setting, MergeRequest mergeRequest, int width,
                                int height) throws IOException, GitLabApiException {
        XYDataset dataset = this.createDataset(metric, results, setting, mergeRequest);

        // Create chart
        JFreeChart chart = ChartFactory.createXYLineChart(metric.getDisplayName() + " results",
                this.getGraphXAxisLabel(), this.getGraphYAxisLabel(), dataset,
                PlotOrientation.VERTICAL, false,false, false);

        XYPlot plot = chart.getXYPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        plot.setRenderer(renderer);

        // set range of axis
        NumberAxis rangeYAxis = (NumberAxis) chart.getXYPlot().getRangeAxis();
        double[] rangeY = this.getGraphYAxisBounds(metric, results, setting, mergeRequest);
        rangeYAxis.setRange(new Range(rangeY[0], rangeY[1]));

        NumberAxis rangeXAxis = (NumberAxis) chart.getXYPlot().getDomainAxis();
        double[] rangeX = this.getGraphXAxisBounds(metric, results, setting, mergeRequest);
        rangeXAxis.setRange(new Range(rangeX[0], rangeX[1]));

        // draw green rectangle to indicate the area which is accepted
        double wantedYValue = setting.getThresholdIsUpperBound() ? 0 : setting.getThreshold();
        double wantedHeight = setting.getThresholdIsUpperBound() ? setting.getThreshold() :
                rangeYAxis.getRange().getUpperBound() - setting.getThreshold();

        Rectangle2D.Double wantedArea = new Rectangle2D.Double(rangeX[0], wantedYValue,
                Integer.MAX_VALUE, wantedHeight);

        renderer.addAnnotation(new XYShapeAnnotation(
                wantedArea,
                new BasicStroke(2.0f),
                new Color(0,0, 0, 0f),
                new Color(0,1, 0, 0.15f))
        );

        // create file to export chart to and export chart
        File file = File.createTempFile("tmp", ".png");
        file.deleteOnExit();

        ChartUtils.saveChartAsPNG(file, chart, width, height);

        //upload file to GitLab
        FileUpload upload = this.uploadWrapper.upload(file);

        return upload;
    }

    /**
     * Returns the label of the Y axis of the graph displayed in the comment.
     *
     * @return  String containing the label.
     */
    public String getGraphYAxisLabel() {
        return "Quality";
    }

    /**
     * Returns the label of the X axis of the graph displayed in the comment.
     *
     * @return  String containing the label.
     */
    public String getGraphXAxisLabel() {
        return "Days since first review";
    }

    /**
     * Function that returns the min and max value that should be displayed on the x-axis of
     * the graph.
     *
     * @param metric        The metric which is being reviewed.
     * @param results       Sorted map from {@link GitCommit} to the results which have been
     *                      evaluated by metric.
     * @param setting       The {@link MetricSettings} which currently holds for metric
     *                      on the repository which is being reviewed.
     * @param mergeRequest  The {@link MergeRequest} which is being reviewed.
     * @return              A double array of length 2 containing the min and max value of the x-
     *                      axis.
     */
    public double[] getGraphXAxisBounds(Metric metric, SortedMap<GitCommit, T> results,
                                     MetricSettings setting, MergeRequest mergeRequest) {

        int firstTimestamp = results.lastKey().getTimestamp();
        int maxTime = 0;
        for (GitCommit commit : results.keySet()) {
            if (commit.getTimestamp() - firstTimestamp > maxTime) {
                maxTime = commit.getTimestamp()  - firstTimestamp;
            }
        }
        if (maxTime == 0) {
            return new double[] {-1, 1};
        }

        // return either the maxTime or the integer equivalent to 0.1 days = 2.4 hours for upper
        // bound and 0 for lower bound
        return new double[]{0, (maxTime * 1.05) / (60 * 60 * 24)};
    }

    /**
     * Function that returns the min and max value that should be displayed on the y-axis of
     * the graph.
     *
     * @param metric        The metric which is being reviewed.
     * @param results       Sorted map from {@link GitCommit} to the results which have been
     *                      evaluated by metric.
     * @param setting       The {@link MetricSettings} which currently holds for metric
     *                      on the repository which is being reviewed.
     * @param mergeRequest  The {@link MergeRequest} which is being reviewed.
     * @return              A double array of length 2 containing the min and max value of the y-
     *                      axis.
     */
    public double[] getGraphYAxisBounds(Metric metric, SortedMap<GitCommit, T> results,
                                     MetricSettings setting, MergeRequest mergeRequest) {

        double maxScore = 0;
        for (GitCommit commit : results.keySet()) {
            if (results.get(commit).getScore() != null
                    && results.get(commit).getScore() > maxScore) {
                maxScore = results.get(commit).getScore();
            }
        }

        double maxYValue = Math.max(maxScore, setting.getThreshold());

        if (maxYValue == 0) {
            maxYValue = 1;
        }

        return new double[]{0, maxYValue * 1.1};
    }
}
