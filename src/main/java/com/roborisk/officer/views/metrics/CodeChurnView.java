package com.roborisk.officer.views.metrics;

import com.roborisk.officer.helpers.UploadWrapper;
import com.roborisk.officer.models.database.metrics.MergeRequestMetricResult;

/**
 * View that generates comments for the CodeChurn metric.
 */
public class CodeChurnView<T extends MergeRequestMetricResult>
        extends GeneralMetricEvaluationView<T> {

    /**
     * Constructor which sets the {@link UploadWrapper}.
     *
     * @param uploadWrapper The {@link UploadWrapper} to be used to upload files.
     */
    public CodeChurnView(UploadWrapper uploadWrapper) {
        super(uploadWrapper);
    }

    @Override
    public String getGraphYAxisLabel() {
        return "Number of times modified";
    }
}
