package com.roborisk.officer.views.metrics;

import com.roborisk.officer.helpers.UploadWrapper;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.metrics.MergeRequestFileMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;

import java.util.SortedMap;

/**
 * View that generates comments for the sonar vulnerabilities metric.
 */
public class SonarVulnerabilitiesView
        extends GeneralMetricEvaluationView<MergeRequestFileMetricResult> {

    /**
     * Constructor which sets the {@link UploadWrapper}.
     *
     * @param uploadWrapper The {@link UploadWrapper} to be used to upload files.
     */
    public SonarVulnerabilitiesView(UploadWrapper uploadWrapper) {
        super(uploadWrapper);
    }

    @Override
    public String getTextUnderGraph(Metric metric,
                                    SortedMap<GitCommit, MergeRequestFileMetricResult> results,
                                    MetricSettings setting, MergeRequest mergeRequest) {
        GitCommit lastCommit = results.keySet().iterator().next();
        MergeRequestFileMetricResult result = results.get(lastCommit);

        if (result.getDescription() == null) {
            return "There are no vulnerabilities found.";
        }

        String[] lines = result.getDescription().split("\n");

        StringBuilder comment = new StringBuilder();

        comment.append("`")
                .append(result.getArtefactName())
                .append("`")
                .append("contains vulnerabilities on ")
                .append(lines.length);

        if (lines.length == 1) {
            comment.append(" line. <br>");
        } else {
            comment.append(" lines. <br>");
        }

        for (String line : lines) {
            comment.append(line)
                    .append("<br>");
        }

        return comment.toString();
    }

    @Override
    public String getGraphYAxisLabel() {
        return "Number of vulnerable lines";
    }
}
