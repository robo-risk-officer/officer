package com.roborisk.officer.views;

import com.roborisk.officer.models.database.DiscussionThread;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestClassObservation;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestFunctionObservation;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.RobotNote;
import com.roborisk.officer.models.database.metrics.MergeRequestMetricResult;
import com.roborisk.officer.models.repositories.MergeRequestClassObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestFileRepository;
import com.roborisk.officer.models.repositories.MergeRequestFunctionObservationRepository;
import com.roborisk.officer.models.repositories.RobotNoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Simple evaluation view which creates a general comment for the merge request summarysing
 * the results of the merge request review.
 *
 * @param <T> The type of the results that are being summarised.
 */
@Component
public class GeneralEvaluationView<T extends MergeRequestMetricResult> {

    /**
     * Stores the base url of the GitLab server. Value given by spring from application properties.
     */
    @Value("${officer.gitlab.baseurl}")
    private String gitLabBaseUrl;

    /**
     * {@link EvaluationViewHelper} for helping with querying the DB.
     */
    @Autowired
    private EvaluationViewHelper evaluationViewHelper;

    /**
     * Database repository to handle {@link MergeRequestFunctionObservation} objects.
     */
    @Autowired
    private MergeRequestFunctionObservationRepository mergeRequestFunctionObservationRepository;

    /**
     * Database repository to handle {@link MergeRequestFile} objects.
     */
    @Autowired
    private MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * Database repository to handle {@link MergeRequestClassObservation} objects.
     */
    @Autowired
    private MergeRequestClassObservationRepository mergeRequestClassObservationRepository;

    /**
     * Database repository to handle {@link RobotNote} objects.
     */
    @Autowired
    private RobotNoteRepository robotNoteRepository;

    /**
     * Generates a string for the start of a details comment.
     *
     * @param summaryContent    The content of the summary.
     * @return                  A string containing an HTML formatted summary.
     */
    private String getDetailsOpening(String summaryContent) {
        StringBuilder comment = new StringBuilder();
        comment.append("<details close>")
            .append("    <summary>")
            .append("        ")
            .append(summaryContent)
                .append("    </summary>");

        return comment.toString();
    }

    /**
     * Generates the general evaluation comment that states how many artefacts are of low quality
     * and links to all the discussion threads that are of low quality.
     *
     * @param mergeRequest  {@link MergeRequest} to be commented on.
     * @return              String containing the general overview comment.
     */
    public String generateComment(MergeRequest mergeRequest,
                                  MergeRequestReview mergeRequestReview) {
        // Fetch all MergeRequestFiles that were created for this review.
        List<MergeRequestFile> files = this.mergeRequestFileRepository
                .findAllByMergeRequestReviewId(mergeRequestReview.getId());

        // Fetch all MergeRequestClassObservations that were created for this review.
        List<MergeRequestClassObservation> classes = this.mergeRequestClassObservationRepository
                .findAllByMergeRequestFileMergeRequestReviewId(mergeRequestReview.getId());

        // Fetch all MergeRequestFunctionObservation that were created for this review.
        List<MergeRequestFunctionObservation> functions =
                this.mergeRequestFunctionObservationRepository
                    .findAllByMergeRequestFileMergeRequestReviewId(mergeRequestReview.getId());
        // Filter out all comments of low quality.
        // If the given artefacts do not have a discussion thread than the Robo Risk Officer
        // did not comment on it and thus it was not of low quality.
        List<MergeRequestFile> lowQualFiles = files.stream()
                .filter(file -> file.getDiscussionThread() != null).collect(Collectors.toList());
        List<MergeRequestClassObservation> lowQualClasses = classes.stream()
                .filter(classOb -> classOb.getDiscussionThread() != null)
                .collect(Collectors.toList());
        List<MergeRequestFunctionObservation> lowQualFunctions = functions.stream()
                .filter(functionOb -> functionOb.getDiscussionThread() != null)
                .collect(Collectors.toList());

        // Get number of artefacts that are of low quality.
        int lowQualArtefacts = lowQualFiles.size() + lowQualClasses.size()
                + lowQualFunctions.size();

        // Check if the overall codebase was also evaluated as low quality.
        if (mergeRequestReview.getDiscussionThread() != null) {
            lowQualArtefacts++;
        }

        // Initialise comment.
        StringBuilder comment = new StringBuilder();
        comment.append("# General overview \n");

        if (lowQualArtefacts == 0) {
            // If no artefacts are of low quality.
            comment.append("Amazing! Analysis has not discovered a single artefact")
                    .append(" of low quality!");
        } else {
            // Get first part of body.
            comment.append("Analysis suggests that ")
                    .append(lowQualArtefacts)
                    .append(lowQualArtefacts == 1 ? " artefact is " : " artefacts are ")
                    .append("of low quality.");

            if (mergeRequestReview.getDiscussionThread() != null) {
                String url = this.getUrl(mergeRequest,
                        this.getRobotNote(mergeRequestReview.getDiscussionThread().getId()));

                comment.append(this.getDetailsOpening(("Specifics on entire codebase")))
                    .append(String.format("</br><a href=\"%s\">Link to discussion</a>", url))
                        .append("</details>");
            }

            if (lowQualFiles.size() > 0) {
                comment.append(this.getDetailsOpening("Specifics on files ("
                    + lowQualFiles.size() + ")"))
                        .append("<table><thead><th>File name</th><th>Link to discussion</th>")
                        .append("</thead><tbody>");

                // Append all the rows of the table.
                for (MergeRequestFile file : lowQualFiles) {
                    RobotNote robotNote = this.getRobotNote(file.getDiscussionThread().getId());
                    this.addFileRowToTable(comment, file.getFileName(), mergeRequest, robotNote);
                }
            }

            if (lowQualClasses.size() > 0) {
                comment.append("</tbody></table></details>")
                    .append(this.getDetailsOpening("Specifics on classes ("
                        + lowQualClasses.size() + ")"))
                    .append("<table><thead><th>Class name</th><th>File name<th>Link to "
                        + "discussion</th>")
                        .append("</thead><tbody>");

                for (MergeRequestClassObservation classOb : lowQualClasses) {
                    RobotNote robotNote = this.getRobotNote(classOb.getDiscussionThread().getId());
                    this.addClassRowToTable(comment, classOb.getClassName(),
                            classOb.getMergeRequestFile().getFileName(), mergeRequest, robotNote);
                }
            }

            if (lowQualFunctions.size() > 0) {
                comment.append("</tbody></table></details>")
                    .append(this.getDetailsOpening(("Specifics on functions ("
                        + lowQualFunctions.size() + ")")))
                    .append("<table><thead><th>Function name</th><th>Class name</th><th>File "
                        + "name</th><th>Link to discussion</th>")
                        .append("</thead><tbody>");

                for (MergeRequestFunctionObservation functionOb : lowQualFunctions) {
                    RobotNote robotNote = this.getRobotNote(
                            functionOb.getDiscussionThread().getId());
                    this.addFunctionRowToTable(comment, functionOb.getFunctionName(),
                            functionOb.getClassName(),
                            functionOb.getMergeRequestFile().getFileName(), mergeRequest,
                            robotNote);
                }
            }

            // close the comment
            comment.append("</tbody></table></details>");
        }

        if (this.evaluationViewHelper.mergeRequestReviewIsCalculating(mergeRequestReview)) {
            comment.append("<br> (There are still metrics which are calculating. This overview "
                    + "and the results will be updated once all metrics have finished calculating"
                    + ".)");
        }

        // return comment string.
        return comment.toString();
    }

    /**
     * Gets the robot note from the database using a discussion id.
     *
     * @param discussionId  id of the {@link DiscussionThread}.
     * @return              {@link RobotNote} linked to the discussionId.
     */
    private RobotNote getRobotNote(Integer discussionId) {
        return this.robotNoteRepository
                .findFirstByDiscussionThreadIdOrderByIdAsc(discussionId)
                .orElseThrow(() -> {
                    throw new IllegalStateException();
                });
    }

    /**
     * Get the URL to a merge request note which will link to the note in the GitLab front-end
     * base on the {@link MergeRequest} and the {@link RobotNote}.
     *
     * @param mergeRequest  The {@link MergeRequest} in which the {@link RobotNote} was posted.
     * @param robotNote     The {@link RobotNote} to whcih the link should point.
     * @return              A string containing the URL of the {@link RobotNote}.
     */
    private String getUrl(MergeRequest mergeRequest, RobotNote robotNote) {
        return String.format("%s/%s/-/merge_requests/%d#note_%d",
                this.gitLabBaseUrl,
                mergeRequest.getSourceBranch().getRepository().getPathWithNamespace(),
                mergeRequest.getGitLabIid(),
                robotNote.getGitLabId()
        );
    }

    /**
     * Adds a HTML table row to a StringBuilder with two cells. First cell contains the given
     * artefact name and the second cell contains the link to the {@link RobotNote} given.
     *
     * @param comment       {@link StringBuilder} to append to.
     * @param artefactName  Name of the artefact.
     * @param mergeRequest  {@link MergeRequest} being commented on.
     * @param robotNote     {@link RobotNote} to be linked in the table.
     */
    private void addFileRowToTable(StringBuilder comment, String artefactName,
                                   MergeRequest mergeRequest, RobotNote robotNote) {
        String url = this.getUrl(mergeRequest, robotNote);

        comment.append("<tr><td>")
                .append(artefactName)
                .append("</td><td>")
                .append(String.format("<a href=\"%s\">Discussion</a>", url))
                .append("</td></tr>");
    }

    /**
     * Adds a HTML table row to a StringBuilder with two cells. First cell contains the given
     * artefact name and the second cell contains the link to the {@link RobotNote} given.
     *
     * @param comment       {@link StringBuilder} to append to.
     * @param artefactName  Name of the artefact.
     * @param mergeRequest  {@link MergeRequest} being commented on.
     * @param robotNote     {@link RobotNote} to be linked in the table.
     */
    private void addClassRowToTable(StringBuilder comment, String artefactName, String artefactFile,
                                   MergeRequest mergeRequest, RobotNote robotNote) {
        String url = this.getUrl(mergeRequest, robotNote);

        comment.append("<tr><td>")
            .append(artefactName)
            .append("</td><td>")
            .append(artefactFile)
            .append("</td><td>")
                .append(String.format("<a href=\"%s\">Discussion</a>", url))
                .append("</td></tr>");
    }

    /**
     * Adds a HTML table row to a StringBuilder with two cells. First cell contains the given
     * artefact name and the second cell contains the link to the {@link RobotNote} given.
     *
     * @param comment       {@link StringBuilder} to append to.
     * @param artefactName  Name of the artefact.
     * @param mergeRequest  {@link MergeRequest} being commented on.
     * @param robotNote     {@link RobotNote} to be linked in the table.
     */
    private void addFunctionRowToTable(StringBuilder comment, String artefactName,
                                        String artefactClass, String artefactFile,
                                        MergeRequest mergeRequest, RobotNote robotNote) {
        String url = this.getUrl(mergeRequest, robotNote);

        comment.append("<tr><td>")
                .append(artefactName)
                .append("</td><td>")
                .append(artefactClass)
                .append("</td><td>")
                .append(artefactFile)
                .append("</td><td>")
                .append(String.format("<a href=\"%s\">Discussion</a>", url))
                .append("</td></tr>");
    }
}
