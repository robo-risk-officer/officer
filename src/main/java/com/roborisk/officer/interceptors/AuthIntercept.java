package com.roborisk.officer.interceptors;

import com.roborisk.officer.configurators.AuthenticationConfigurator;
import com.roborisk.officer.models.web.GitLabMembersData;
import com.roborisk.officer.models.web.GitLabTokenData;
import lombok.extern.slf4j.Slf4j;
import org.gitlab4j.api.models.AccessLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;


/**
 * This class is used to intercept all requests to authenticated routes.
 * On interception, it is checked if the request contains a valid access token  and if the correct
 * privilege level is associated to it before either rejecting the request or letting it through.
 */
@Slf4j
public class AuthIntercept implements HandlerInterceptor {

    /**
     * Spring built-in simple http client.
     */
    private final RestTemplate restTemplate = new RestTemplate();

    /**
     * Spring way to match url patterns.
     */
    private final PathMatcher pathMatcher = new AntPathMatcher();

    /**
     * Environment variable containing whether the authentication is enabled.
     */
    @Value("${officer.interceptors.auth.enabled:true}")
    private Boolean authEnabled;

    /**
     * The environment contains variables like GitLab base url.
     */
    @Autowired
    private Environment env;

    /**
     * Stores the base url of the GitLab server. Value given by spring from application properties.
     */
    @Value("${officer.gitlab.baseurl}")
    private String gitLabBaseUrl;

    /**
     * Stores the group name/id of the GitLab server.
     * Value given by spring from application properties.
     */
    @Value("${officer.gitlab.robogroup}")
    private String gitLabGroup;

    /**
     * Checks authentication before passing on the request to a controller.
     * First checks if access token is set in header.
     * Then checks if this access token is valid using GitLab API (or the development fake token).
     * Then checks if the user owning this token has the correct permission for the request.
     *
     * @param request       The intercepted http request.
     * @param response      The http response that will be sent for the request.
     * @param handler       The handler that was chosen to execute.
     * @return              True if this request has been correctly authenticated.
     * @throws Exception    If something goes wrong with the sending of a response.
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) throws Exception {

        // Makes it possible to disable authentication during testing.
        if (!this.authEnabled) {
            return true;
        }

        // OPTIONS requests don't include and don't need authorization
        if (request.getMethod().equals("OPTIONS")) {
            return true;
        }

        /*
         * Contains the access needed from the user making a request to this page.
         * The numbers are defined by GitLab and linked to roles. More info:
         * https://docs.gitlab.com/ee/api/members.html.
         */
        int accessNeeded = this.getAccessLevelNeeded(request.getRequestURI());

        // The route has no defined access level in the AuthenicationDefinitions file
        if (accessNeeded == AccessLevel.INVALID.toValue()) {
            // Act like this route does not exist
            response.sendError(404);

            return false;
        }

        // This will hold the oauth token if a valid Authorization header is set
        String token;
        try {
            token = this.getAuthorizationToken(request);
        } catch (IllegalStateException e) {
            // There is an issue with the Autorization header. The exception message has more info
            log.warn("A request was made with an incorrect authentication header. Reason: "
                    + e.getMessage());
            response.sendError(403, e.getMessage());

            return false;
        }

        // get GitLab privileges for this access token
        GitLabTokenData tokenData = this.getTokenData(token);

        // GitLab token data could not be received. Assume incorrect token.
        if (tokenData == null) {
            response.sendError(403, "Incorrect access token");

            return false;
        }

        // Get the GitLab user id of the owner of this token
        int userId = tokenData.getResourceOwnerId();

        // Get the users access level
        try {
            int userAccessLevel = this.getUserAccessLevel(userId, token);

            // Proceed to requested page
            if (userAccessLevel >= accessNeeded) {
                return true;
            }

            // Not enough permissions, block request
            log.warn("A request was made by a user with insufficient permissions.");
            response.sendError(403, "User does not have the correct permissions");

            return false;
        } catch (IllegalStateException e) {
            // Block request when fail to get user access level
            response.sendError(403, "Failed to get member access level");

            return false;
        }
    }

    /**
     * Inspects a request and checks if the Authorization header is used correctly.
     *
     * @param request                   The request to get the token from.
     * @throws IllegalStateException    If the request does not contain the correct token format.
     *                                  The message inside the exception will contain
     *                                  details on how the token is wrong.
     * @return                          The oauth token encoded in the request if it is valid.
     */
    private String getAuthorizationToken(HttpServletRequest request) throws IllegalStateException {
        // Get access token from the request headers
        String authHeader = request.getHeader("Authorization");

        // There was no Authorization token in request, redirect to forbidden page
        if (authHeader == null || authHeader.equals("")) {
            throw new IllegalStateException("Header 'Authorization' was not set");
        }

        // Split authentication type and key
        String[] split = authHeader.split(" ");
        if (split.length != 2) {
            throw new IllegalStateException("Invalid Authorization header");
        }

        // Check if the correct key type is available.
        String authType = split[0];
        if (!authType.equals("Bearer")) {
            throw new IllegalStateException("Authorization type was not Bearer");
        }

        // Get token
        return split[1];
    }

    /**
     * Returns an integer representing the access level needed for a user to make
     * the request they made.
     * The mapping of GitLab roles and integers is defined at:
     * https://docs.gitlab.com/ee/api/members.html.
     *
     * @param uri   The URI of the request.
     * @return      -1 if the uri is not a valid route. The GitLab access level integer otherwise.
     */
    private int getAccessLevelNeeded(String uri) throws IllegalArgumentException {
        // Check if the route requested needs maintainer access
        for (String pattern : AuthenticationConfigurator.maintainerRoutes) {
            if (this.pathMatcher.match(pattern, uri)) {
                return AccessLevel.MAINTAINER.toValue();
            }
        }

        // Check if the route requested needs owner access
        for (String pattern : AuthenticationConfigurator.ownerRoutes) {
            if (this.pathMatcher.match(pattern, uri)) {
                return AccessLevel.OWNER.toValue();
            }
        }

        // The route has no access level defined
        return AccessLevel.INVALID.toValue();
    }

    /**
     * Gets a GitTokenData object from a token string if token is a valid token.
     *
     * @param token     A string containing an access_token.
     * @return          A GitTokenData object if token is a valid token, null otherwise.
     */
    private GitLabTokenData getTokenData(String token) {
        // GitLab api url for token information
        String gitLabUrl = this.gitLabBaseUrl + "/oauth/token/info?access_token=" + token;

        // Make request to GitLab
        try {
            ResponseEntity<GitLabTokenData> response = this.restTemplate.getForEntity(gitLabUrl,
                    GitLabTokenData.class);
            // If GitLab request was successful, return the TokenData
            if (response.getStatusCode() == HttpStatus.OK) {
                return response.getBody();
            } else {
                return null;
            }
        } catch (HttpClientErrorException e) {
            // Return null as GitLab returns a non 200 code.
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Get the access level of a specific user.
     * The GitLab group that is used to determine the access level of a user is defined
     * in the application properties file.
     * The mapping of GitLab roles and integers is defined at:
     * https://docs.gitlab.com/ee/api/members.html.
     *
     * @param userId                    The GitLab id of the user.
     * @throws IllegalStateException    If the access level for a user cannot be determined.
     * @return                          The access level of the user.
     */
    private int getUserAccessLevel(int userId, String oauthToken) throws IllegalStateException {
        // GitLab api url for member information
        String gitLabUrl = this.gitLabBaseUrl + "/api/v4/groups/"
                + this.gitLabGroup + "/members/" + userId
                + "?access_token=" + oauthToken;

        try {
            ResponseEntity<GitLabMembersData> response = this.restTemplate.getForEntity(gitLabUrl,
                    GitLabMembersData.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                return Objects.requireNonNull(response.getBody()).getAccessLevel();
            }
        } catch (HttpClientErrorException e) {
            // gitlab returns a non 200 code
        }

        // Throw exception when either not OK response or a client error
        throw new IllegalStateException("AuthIntercept.getUserAccessLevel: "
                + "Request to get member data failed.");

    }
}
