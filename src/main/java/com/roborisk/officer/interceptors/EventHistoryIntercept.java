package com.roborisk.officer.interceptors;

import com.roborisk.officer.models.database.EventHistoryItem;
import com.roborisk.officer.models.repositories.EventHistoryItemRepository;
import org.apache.commons.codec.digest.MurmurHash3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class which intercepts all requests to the server.
 * The incoming webhook requests hashed using the Murmur 3 hashing algorithm, to generate a hash of
 * 64 characters. This hash is than checked against the database. If the database already contains
 * said hash, the request is discarded. Otherwise the new hash is stored in the database, and the
 * request passed on to the rest of the application.
 */
public class EventHistoryIntercept implements HandlerInterceptor {

    /**
     * Environment variable containing whether the event history intercept is enabled.
     */
    @Value("${officer.interceptors.event.enabled:true}")
    private Boolean enabled;

    /**
     * Create repository to interface with the database.
     */
    @Autowired
    EventHistoryItemRepository repository;

    /**
     * Function intercepting each request. Upon intercepting the request, it is first checked
     * whether it concerns a webhook request. Other requests are immediately passed on. For webhook
     * requests, the request is hashed and checked against the database. If the database contains
     * the computed hash, the request is discarded. Otherwise, the hash is added to the database and
     * the request is passed on.
     *
     * @param   request   http request received.
     * @param   response  http response to be sent.
     * @param   handler   request handler.
     * @throws  Exception if any sub-process throws exception.
     * @return  whether the request has not yet been processed.
     */
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) throws Exception {
        // Disregard and pass on non-webhook requests.
        if (!request.getRequestURI().startsWith("/webhook/") || !this.enabled) {
            return true;
        }

        // Retrieve body from the request.
        Stream<String> stringStream = request.getReader().lines();
        String requestBody = stringStream.collect(Collectors.joining(System.lineSeparator()));

        // Compute 128-bit hash of the request body and store the first 64 bits.
        Long hash = MurmurHash3.hash128(requestBody.getBytes())[0];

        // Retrieve event from the database with the computed hash if it exists.
        Optional<EventHistoryItem> queriedEvent = this.repository.findByHash(hash);

        // Check whether the database contained an event with the computed hash.
        if (queriedEvent.isPresent()) {
            // Discard the event if the database already contains an event with the computed hash.
            return false;
        }

        // Create new EventHistoryItem and set hash & timestamp.
        EventHistoryItem event = new EventHistoryItem();
        event.setHash(hash);
        event.setTimestamp(Instant.now().toEpochMilli());

        // Save the event to the database.
        this.repository.save(event);

        // Pass the request on to the rest of the application.
        return true;
    }
}
