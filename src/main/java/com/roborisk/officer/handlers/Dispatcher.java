package com.roborisk.officer.handlers;

import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.events.Event;
import com.roborisk.officer.events.RepositoryInitEvent;
import com.roborisk.officer.models.web.AbstractWebhookData;
import com.roborisk.officer.models.web.EmptyWebhookData;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * The dispatcher links events to their corresponding triggers and fires them.
 */
@Slf4j
@Component
@Scope("singleton")
public class Dispatcher extends Thread {

    /**
     * Map that maps every event type to a list of events that have to be triggered.
     * The order in which events are triggered matters
     */
    private final Map<EventType, List<Event<? extends AbstractWebhookData>>> map;

    /**
     * Whether to disable events.
     * This property is used for testing.
     */
    @Value("${officer.event.enabled:true}")
    private boolean enabled;

    /**
     * Whether or not to start the thread.
     */
    private boolean threaded;

    /**
     * The job queue.
     */
    private ConcurrentLinkedQueue<Triple<EventType, AbstractWebhookData,
                                        Event<? extends AbstractWebhookData>>> jobs;

    /**
     * The AutoWired constructor with all elements that extend Event.
     * <p>
     * Spring will fill the list of Events, which are components by itself.
     * This way spring will autowire the fields in the classes.
     * The main reason that this works is that spring now has full control over
     * the events. The map can not be filled out in this class. This means that
     * the supported event types for an event have been moved to the event itself.
     * </p>
     *
     * <p>
     * To add an event to the dispatcher simply implement the {@link Event},
     * an example can be seen in {@link RepositoryInitEvent}.
     * </p>
     *
     * @param events    All Event handlers, this is handled by Spring.
     * @param threaded  Injected conf
     */
    @Autowired
    public Dispatcher(List<Event<? extends AbstractWebhookData>> events,
                      @Value("${officer.dispatcher.threaded:true}") boolean threaded) {
        this.map = new HashMap<>();
        this.threaded = threaded;

        // Add all supported event types to the map.
        Arrays.stream(EventType.values()).forEach(c -> this.map.put(c, new ArrayList<>()));
        // Map the events received to each event type.
        events.forEach(event -> event.getCompatibleEvents()
                    .forEach(eventType -> this.map.get(eventType).add(event))
        );

        // Sort based on priority in descending order.
        this.map.forEach((k, v) -> {
            v.sort((a, b) -> -Integer.compare(a.getPriority(), b.getPriority()));
        });

        this.jobs = new ConcurrentLinkedQueue<>();

        if (this.threaded) {
            this.start();
        }
    }

    /**
     * Run the jobs in the background in a synchronized fashion.
     */
    public void run() {
        do {
            while (!this.jobs.isEmpty()) {
                Triple<EventType, AbstractWebhookData, Event<? extends AbstractWebhookData>> job =
                        this.jobs.poll();

                EventType eventType = job.getLeft();
                AbstractWebhookData data = job.getMiddle();
                Event<? extends AbstractWebhookData> event = job.getRight();

                log.info("Started to handle " + event.getClass().getSimpleName()
                        + " event of type " + eventType.toString()
                        + " for webhook of type " + data.getType()
                        + " for repository " + data.getRepository().getName());

                try {
                    AbstractWebhookData next = event.handle(eventType, data);

                    if (!(next instanceof EmptyWebhookData)) {
                        this.handle(next);
                    }

                    log.info("Successfully handled " + event.getClass().getSimpleName()
                            + " event of type " + eventType.toString()
                            + " for webhook of type " + data.getType()
                            + " for repository " + data.getRepository().getName());
                } catch (Exception e) {
                    log.error("Failure handling " + event.getClass().getSimpleName()
                            + " event of type " + eventType.toString()
                            + " for webhook of type " + data.getType()
                            + " for repository " + data.getRepository().getName()
                            + ": " + e.getMessage());
                }
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                log.error("Dispatcher.run: interrupted while sleeping");
            }
        } while (!this.isInterrupted() && this.threaded);
    }

    /**
     * Based on the event type of the parsed webhook data,
     * this function triggers the corresponding event.
     *
     * @param data The parsed webhook data.
     */
    public void handle(AbstractWebhookData data) {
        if (!this.enabled) {
            return;
        }

        // Log arrival of webhook
        log.info("Received webhook of type " + data.getType()
                + " from GitLab for repository " + data.getRepository().getName());

        // Loop over all events that need to be triggered for the EventType of the data
        EventType eventType = data.getEventType();
        for (Event<? extends AbstractWebhookData> event : this.map.get(eventType)) {
            this.jobs.add(Triple.of(eventType, data, event));
        }
    }
}
