package com.roborisk.officer.models.enums;

import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.EnumType;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

/**
 * Class to translate between Java enums and PostgreSQL enums.
 * Based on: https://vladmihalcea.com/the-best-way-to-map-an-enum-type-with-jpa-and-hibernate/.
 */
public class PostgreSQLEnumType extends EnumType {

    /**
     * This method maps between Java enums and PostgresSQL enums.
     *
     * @param st                    The prepared statement received for the query.
     * @param value                 The value for include in the prepared statement.
     * @param index                 The index where the value should be placed in
     *                                  the prepared statement.
     * @param session               The current shared contract between a Session
     *                                  and StatelessSession.
     * @throws SQLException         When the value cannot be set on the index in
     *                                  the prepared statement.
     */
    public void nullSafeSet(PreparedStatement st, Object value, int index,
            SharedSessionContractImplementor session) throws SQLException {
        st.setObject(index, value != null ? ((Enum) value).name() : null, Types.OTHER);
    }
}
