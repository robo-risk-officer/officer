package com.roborisk.officer.models.enums;

/**
 * Implements the event type enum.
 */
public enum EventType {
    REVIEW_REQUESTED,
    MERGE_REQUEST_CHANGED,
    MERGE_REQUEST_CREATED,
    PUSH_EVENT,
    SONAR_ANALYSIS_FINISHED,
    NONE
}
