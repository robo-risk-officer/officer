package com.roborisk.officer.models.enums;

/**
 * Implements the database enum for quality types.
 */
public enum QualityTypeEnum {
    RISK,
    COMPLEXITY,
    UNDERSTANDABILITY,
}
