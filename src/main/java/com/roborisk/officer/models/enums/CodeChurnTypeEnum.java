package com.roborisk.officer.models.enums;

/**
 * Type of object analyzed for Code Churn.
 */
public enum CodeChurnTypeEnum {
    FILE,
    CLASS,
    FUNCTION
}
