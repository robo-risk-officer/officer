package com.roborisk.officer.models.enums;

/**
 * Implements the database enum for metric types.
 */
public enum MetricTypeEnum {
    CODEBASE,
    FILE,
    CLASS,
    FUNCTION,
}
