package com.roborisk.officer.models.database;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * BranchFile implements the branch file object model.
 */
@Entity(name = "branch_file")
public class BranchFile extends AbstractFile {

    /**
     * Unique Identifier for a {@link BranchFile} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * isDeleted stores whether the BranchFile is deleted.
     */
    @NotNull
    @Getter @Setter private Boolean isDeleted;

    /**
     * branch stores a reference to the branch that contains this file.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private Branch branch;

    /**
     * lastChangeCommit stores a reference to the last commit in which the file was changed.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private GitCommit lastChangeCommit;

    /**
     * Function that generates a BranchFile object given a path string.
     *
     * @param filePath  Path with file name.
     * @return          BranchFile object of filePath.
     */
    public static BranchFile fromString(String filePath) {
        BranchFile bf = new BranchFile();
        Path path = Paths.get(filePath);

        bf.setFileName(path.getFileName().toString());
        bf.setFilePath(filePath);

        return bf;
    }
}
