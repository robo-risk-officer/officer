package com.roborisk.officer.models.database;

import com.roborisk.officer.models.web.RobotSettingsWeb;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 * The RobotSettings Model represents the "robot_settings" database table.
 */
@Entity(name = "robot_settings")
public class RobotSettings extends AbstractSettings {

    /**
     * The value representing of the MR needs to be automatically reviewed.
     */
    @NotNull
    @Getter @Setter private Boolean automaticReviewOnNewMr;

    /**
     * The value representing if a new commit needs to be automatically reviewed.
     */
    @NotNull
    @Getter @Setter private Boolean automaticReviewOnNewCommit;

    /**
     * The threshold of false positive markings required to set a false positive flag.
     */
    @NotNull
    @Getter @Setter private Integer falsePositiveThreshold;

    /**
     * The value representing the amount of commits to take into account when a repository gets
     * added.
     */
    @NotNull
    @Getter @Setter private Integer reviewHistoryLength;

    /**
     * The repository the settings are linked to.
     */
    @OneToOne
    @Getter @Setter private Repository repository;

    /**
     * Constructor for a {@link RobotSettings} object.
     */
    public RobotSettings() {}

    /**
     * Copy constructor for a {@link RobotSettings} object.
     * This will not copy the repository or id.
     *
     * @param settings  The {@link RobotSettings} to copy.
     */
    public RobotSettings(RobotSettings settings) {
        this.setAutomaticReviewOnNewCommit(settings.getAutomaticReviewOnNewCommit());
        this.setAutomaticReviewOnNewMr(settings.getAutomaticReviewOnNewMr());
        this.setFalsePositiveThreshold(settings.getFalsePositiveThreshold());
        this.setReviewHistoryLength(settings.getReviewHistoryLength());
    }

    /**
     * method to check wheter the contents of a {@link RobotSettingsWeb} object are the same as
     * stored in this object.
     *
     * @param robotSettingsWeb The {@link RobotSettingsWeb} instance to compare against.
     * @return                 True if all the corresponding fields in robotSettingsWeb
     *                         are the same as stored in this object.
     */
    public boolean isEqualToRobotSettingsWeb(RobotSettingsWeb robotSettingsWeb) {
        if (robotSettingsWeb.getId() == null) {
            if (this.id != null) {
                return false;
            }
        } else if (!robotSettingsWeb.getId().equals(this.id)) {
            return false;
        }

        if (!robotSettingsWeb.getAutomaticReviewOnNewCommit()
                .equals(this.automaticReviewOnNewCommit)) {
            return false;
        }
        if (!robotSettingsWeb.getAutomaticReviewOnNewMr()
                .equals(this.automaticReviewOnNewMr)) {
            return false;
        }
        if (!robotSettingsWeb.getFalsePositiveThreshold()
                .equals(this.falsePositiveThreshold)) {
            return false;
        }

        return robotSettingsWeb.getReviewHistoryLength()
                .equals(this.reviewHistoryLength);
    }

    /**
     * Update the values of this based on a {@link RobotSettingsWeb} object.
     *
     * @param robotSettingsWeb The {@link RobotSettingsWeb} instance to update based upon.
     * @post all fields in this are updated according to the data contained in the
     *     robotSettingsWeb.
     */
    public void setAllFromRobotSettingsWeb(RobotSettingsWeb robotSettingsWeb) {
        this.setAutomaticReviewOnNewCommit(robotSettingsWeb.getAutomaticReviewOnNewCommit());
        this.setAutomaticReviewOnNewMr(robotSettingsWeb.getAutomaticReviewOnNewMr());
        this.setFalsePositiveThreshold(robotSettingsWeb.getFalsePositiveThreshold());
        this.setReviewHistoryLength(robotSettingsWeb.getReviewHistoryLength());
    }

    /**
     * Converts a {@link RobotSettings} to a {@link RobotSettingsWeb}.
     *
     * @return  The {@link RobotSettingsWeb} that got converted from the {@link RobotSettings}.
     */
    public RobotSettingsWeb toWebObject() {
        RobotSettingsWeb web = new RobotSettingsWeb();

        web.setId(this.getId());
        web.setAutomaticReviewOnNewCommit(this.getAutomaticReviewOnNewCommit());
        web.setAutomaticReviewOnNewMr(this.getAutomaticReviewOnNewMr());
        web.setReviewHistoryLength(this.getReviewHistoryLength());
        web.setFalsePositiveThreshold(this.getFalsePositiveThreshold());

        return web;
    }
}
