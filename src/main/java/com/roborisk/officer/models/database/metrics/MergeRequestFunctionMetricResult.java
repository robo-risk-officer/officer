package com.roborisk.officer.models.database.metrics;

import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequestClassObservation;
import com.roborisk.officer.models.database.MergeRequestFunctionObservation;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * MergeRequestFunctionMetricResult implements the mr_function_metric_result database table.
 */
@Entity(name = "mr_function_metric_result")
public class MergeRequestFunctionMetricResult extends MergeRequestMetricResult {

    /**
     * Unique Identifier for a {@link MergeRequestFunctionMetricResult} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * mergeRequestFunctionObservation stores the {@link MergeRequestFunctionObservation} associated
     * with this metric result.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE)
    @NotNull @JoinColumn(name = "mr_function_observation_id")
    @Getter @Setter private MergeRequestFunctionObservation mergeRequestFunctionObservation;

    @Override
    public String getArtefactName() {
        return "'" + this.mergeRequestFunctionObservation.getFunctionName() + "' in class '"
            + this.mergeRequestFunctionObservation.getClassName() + "'";
    }

    @Override
    public GitCommit getAssociatedCommit() {
        return this.mergeRequestFunctionObservation.getMergeRequestFile()
            .getMergeRequestReview().getCommit();
    }

    /**
     * Empty constructor.
     */
    public MergeRequestFunctionMetricResult() {

    }

    /**
     * Copy constructor to create an {@link BranchFunctionMetricResult} equivalent to a
     * {@link MergeRequestFunctionMetricResult} object.
     *
     * @param branchFunctionMetricResult        The {@link BranchFunctionMetricResult} to copy.
     * @param mergeRequestFunctionObservation   The {@link MergeRequestClassObservation} object to
     *                                          link the new
     *                                          {@link MergeRequestFunctionMetricResult} to.
     */
    public MergeRequestFunctionMetricResult(BranchFunctionMetricResult branchFunctionMetricResult,
            MergeRequestFunctionObservation mergeRequestFunctionObservation) {
        this.setMergeRequestFunctionObservation(mergeRequestFunctionObservation);
        this.setScore(branchFunctionMetricResult.getScore());
        this.setMetric(branchFunctionMetricResult.getMetric());
        this.setIsLowQuality(branchFunctionMetricResult.getIsLowQuality());
    }
}
