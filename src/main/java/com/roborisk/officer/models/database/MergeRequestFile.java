package com.roborisk.officer.models.database;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * MergeRequestFile implements the merge_request_file database table.
 */
@Entity(name = "merge_request_file")
public class MergeRequestFile extends AbstractFile {

    /**
     * Unique Identifier for a {@link MergeRequestFile} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * mergeRequestReview stores the review of which this file is a part.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private MergeRequestReview mergeRequestReview;

    /**
     * discussionThread stores the {@link DiscussionThread} for the {@link MergeRequestFile}.
     */
    @ManyToOne
    @Getter @Setter private DiscussionThread discussionThread;

    /**
     * Empty constructor.
     */
    public MergeRequestFile() {

    }

    /**
     * Copy constructor to copy an {@link BranchFile} into an equivalent {@link MergeRequestFile}.
     *
     * @param branchFile            The {@link BranchFile} to copy.
     * @param mergeRequestReview    The {@link MergeRequestReview} object
     */
    public MergeRequestFile(BranchFile branchFile, MergeRequestReview mergeRequestReview) {
        this.setMergeRequestReview(mergeRequestReview);
        this.setFilePath(branchFile.getFilePath());
        this.setFileName(branchFile.getFileName());
        this.setDiscussionThread(null);
    }
}
