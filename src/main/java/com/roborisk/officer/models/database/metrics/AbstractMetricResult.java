package com.roborisk.officer.models.database.metrics;

import com.roborisk.officer.models.database.GitCommit;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

/**
 * Abstract representation of a metric result.
 * Please, pretty please, do not instantiate this file; unfortunately, it cannot be made abstract
 * due to the Spring JPA code crashing otherwise.
 */
@MappedSuperclass
public abstract class AbstractMetricResult {

    /**
     * isLowQuality stores whether the metric result is below
     * the defined threshold of the metric being used.
     */
    @NotNull
    @Getter @Setter private Boolean isLowQuality;

    /**
     * metric stores a reference to the object containing the metric for which this object
     * contains the results.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private Metric metric;

    /**
     * Getter for the display name of the {@link Metric}, the result is associated to.
     *
     * @return The display name of the association {@link Metric}.
     */
    public String getMetricDisplayName() {
        return this.metric.getDisplayName();
    }


    /**
     * Function to get the latest {@link GitCommit} at the time of the review.
     *
     * @return The latest {@link GitCommit} at the time of the review.
     */
    public abstract GitCommit getAssociatedCommit();
}
