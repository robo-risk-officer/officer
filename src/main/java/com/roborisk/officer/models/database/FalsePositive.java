package com.roborisk.officer.models.database;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * FalsePositive class implements the false positive label object model.
 */
@Entity(name = "false_positive_marking")
public class FalsePositive {

    /**
     * Unique Identifier for a {@link FalsePositive} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * userId stores the id of the user that placed the false positive.
     */
    @NotNull
    @Getter @Setter private Integer userId;

    /**
     * gitLabAwardId stores the id of the gitLabAward.
     */
    @NotNull @Column(unique = true, name = "gitlab_award_id")
    @Getter @Setter private Integer gitLabAwardId;

    /**
     * note stores a reference to the note object was marked.
     */
    @NotNull
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @JoinColumn(name = "robot_note_id")
    @Getter @Setter private RobotNote note;
}
