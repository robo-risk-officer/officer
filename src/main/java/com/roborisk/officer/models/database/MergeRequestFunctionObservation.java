package com.roborisk.officer.models.database;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * MergeRequestFunctionObservation implements the mr_function_observation database table.
 */
@Entity(name = "mr_function_observation")
public class MergeRequestFunctionObservation extends AbstractMergeRequestObservation {

    /**
     * Unique Identifier for a {@link MergeRequestFunctionObservation} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * Stores the function name of the observation.
     */
    @NotNull
    @Getter @Setter private String functionName;

    /**
     * Stores the name of the class containing the function of the observation.
     */
    @NotNull
    @Getter @Setter private String className;

    /**
     * Empty constructor.
     */
    public MergeRequestFunctionObservation() {

    }

    /**
     * Copy constructor to copy a {@link BranchFunctionObservation} object to an equivalent
     * {@link MergeRequestFunctionObservation} object.
     *
     * @param branchFunctionObservation The {@link BranchFunctionObservation} to copy.
     * @param mergeRequestFile          The {@link MergeRequestFile} to link the new
     *                                  {@link MergeRequestFunctionObservation} to.
     */
    public MergeRequestFunctionObservation(BranchFunctionObservation branchFunctionObservation,
            MergeRequestFile mergeRequestFile) {
        this.setMergeRequestFile(mergeRequestFile);
        this.setFunctionName(branchFunctionObservation.getFunctionName());
        this.setClassName(branchFunctionObservation.getClassName());
        this.setLineNumber(branchFunctionObservation.getLineNumber());
        this.setDiscussionThread(null);
        this.setIsSuppressed(false);
        this.setIsFalsePositive(false);
    }
}
