package com.roborisk.officer.models.database.metrics;

import com.roborisk.officer.models.database.BranchClassObservation;
import com.roborisk.officer.models.database.GitCommit;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * BranchClassMetricResult implements the branch_class_metric_result database table.
 */
@Entity(name = "branch_class_metric_result")
public class BranchClassMetricResult extends BranchMetricResult {

    /**
     * Unique Identifier for a {@link BranchClassMetricResult} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * branchClassObservation stores the BranchClassObservation that this metric result is about.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private BranchClassObservation branchClassObservation;

    @Override
    public GitCommit getAssociatedCommit() {
        return this.branchClassObservation.getGitCommit();
    }
}
