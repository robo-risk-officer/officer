package com.roborisk.officer.models.database.metrics;

import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequestFile;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * The MrFileMetricResult Model represents the "mr_file_metric_result" database table.
 */
@Entity(name = "mr_file_metric_result")
public class MergeRequestFileMetricResult extends MergeRequestMetricResult {

    /**
     * Unique Identifier for a {@link MergeRequestFileMetricResult} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * Description of the result.
     */
    @Getter @Setter private String description;

    /**
     * mergeRequestFile stores a reference to the file of which this
     * result is.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private MergeRequestFile mergeRequestFile;

    @Override
    public String getArtefactName() {
        return "'" + this.mergeRequestFile.getFileName() + "'";
    }

    @Override
    public GitCommit getAssociatedCommit() {
        return this.mergeRequestFile.getMergeRequestReview().getCommit();
    }

    /**
     * Empty constructor.
     */
    public MergeRequestFileMetricResult() {

    }

    /**
     * Copy constructor to copy a {@link BranchFileMetricResult} to an equivalent
     * {@link MergeRequestFileMetricResult}.
     *
     * @param branchFileMetricResult    The {@link BranchFileMetricResult} object to copy.
     * @param mergeRequestFile          The {@link MergeRequestFile} the new
     *                                  {@link MergeRequestFileMetricResult} should be linked to.
     */
    public MergeRequestFileMetricResult(BranchFileMetricResult branchFileMetricResult,
            MergeRequestFile mergeRequestFile) {
        this.setScore(branchFileMetricResult.getScore());
        this.setMetric(branchFileMetricResult.getMetric());
        this.setIsLowQuality(branchFileMetricResult.getIsLowQuality());
        this.setMergeRequestFile(mergeRequestFile);
    }
}
