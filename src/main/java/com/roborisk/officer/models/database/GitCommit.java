package com.roborisk.officer.models.database;

import com.roborisk.officer.models.web.GitCommitWeb;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.text.ParseException;

/**
 * GitCommit implements the Git commit object model.
 */
@Entity(name = "commit")
public class GitCommit implements Comparable<GitCommit> {

    /**
     * Maximum length of commit message after which it will be clipped.
     * This is in accordance with the max length of the corresponding database value.
     */
    private static final int MAX_MESSAGE_LENGTH = 255;

    /**
     * Unique Identifier for a {@link GitCommit} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * hash stores the id that GitLab assigned to the commit.
     */
    @NotNull
    @Getter @Setter private String hash;

    /**
     * message stores the message for the commit.
     */
    @NotNull
    @Getter private String message;

    /**
     * Setter for the message which ensures that the length of the message is below
     * this.maxMessageLength
     *
     * @param  newMessage   value to which this.message should be set
     */
    public void setMessage(String newMessage) {
        int length = Math.min(newMessage.length(), GitCommit.MAX_MESSAGE_LENGTH);
        this.message = newMessage.substring(0, length);
    }

    /**
     * timestamp stores the timestamp at which the commit was committed.
     */
    @NotNull
    @Getter private Integer timestamp;

    /**
     * isFinishedProcessing stores the flag whether the metrics have finished processing the commit.
     */
    @NotNull
    @Getter @Setter private boolean isFinishedProcessing;

    /**
     * parent stores the parent of the current commit.
     */
    @OneToOne
    @Getter @Setter private GitCommit parentCommit;

    /**
     * Setter the timestamp.
     *
     * @param date  The date string.
     * @throws      ParseException When the date string does not represent a valid formatted date.
     */
    public void setTimestamp(String date) throws ParseException {
        this.timestamp = GitLabApiWrapper.getEpochFromString(date);
    }

    /**
     * Setter the timestamp.
     *
     * @param epoch The epoch to use.
     */
    public void setTimestamp(Integer epoch) {
        this.timestamp = epoch;
    }

    /**
     * Empty constructor.
     */
    public GitCommit() {

    }

    /**
     * Copy constructor from a {@link GitCommitWeb} object.
     *
     * @param commit The {@link GitCommitWeb} to copy from.
     */
    public GitCommit(GitCommitWeb commit) {
        this.setTimestamp(commit.getTimestamp());
        this.setHash(commit.getHash());
        this.setMessage(commit.getMessage());
        this.setFinishedProcessing(false);
    }

    @Override
    public int compareTo(GitCommit other) {
        return other.timestamp - this.timestamp;
    }
}
