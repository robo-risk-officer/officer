package com.roborisk.officer.models.database;

import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.web.MetricSettingsWeb;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * The MetricSettings Model represents the "metric_settings" database table.
 */
@Entity(name = "metric_settings")
public class MetricSettings extends AbstractSettings {

    /**
     * The threshold of minimal required artifact quality of the corresponding metric.
     */
    @NotNull
    @Getter @Setter private Double threshold;

    /**
     * Whether the artifact should be above or below the threshold to meet the quality
     * standards.
     */
    @NotNull
    @Getter @Setter private Boolean thresholdIsUpperBound;

    /**
     * The weight the corresponding metric contribute to the overall quality measure.
     */
    @NotNull
    @Getter @Setter private Double weight;

    /**
     * repository stores a reference to the project holding these metric settings.
     */
    @OneToOne @OnDelete(action = OnDeleteAction.CASCADE)
    @Getter @Setter private Repository repository;

    /**
     * Metric stores a reference to the metric for which this object contains the settings.
     */
    @OneToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private Metric metric;

    /**
     * Constructor for creating a {@link MetricSettings} model.
     */
    public MetricSettings() {

    }

    /**
     * Constructor for cloning a {@link MetricSettings} model.
     *
     * @param settings Settings to clone.
     */
    public MetricSettings(MetricSettings settings) {
        this.setThreshold(settings.getThreshold());
        this.setThresholdIsUpperBound(settings.getThresholdIsUpperBound());
        this.setWeight(settings.getWeight());
        this.setRepository(settings.getRepository());
        this.setMetric(settings.getMetric());
    }

    /**
     * Method to check whether the contents of a {@link MetricSettingsWeb} object are the same
     * as stored in this object.
     *
     * @param metricSettingsWeb The {@link MetricSettingsWeb} instance to compare against.
     * @return True if all the corresponding fields in metricsSettingsWeb are the same as
     *     stored in this object.
     */
    public boolean isEqualToMetricSettingsWeb(MetricSettingsWeb metricSettingsWeb) {
        if (!metricSettingsWeb.getId().equals(this.id)) {
            return false;
        }

        if (!metricSettingsWeb.getThresholdIsUpperBound().equals(this.thresholdIsUpperBound)) {
            return false;
        }

        if (!metricSettingsWeb.getThreshold().equals(this.threshold)) {
            return false;
        }

        if (!metricSettingsWeb.getWeight().equals(this.weight)) {
            return false;
        }

        return metricSettingsWeb.getName().equals(this.metric.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MetricSettings that = (MetricSettings) o;
        // comparing repositories don't make sense to determine
        // equality to compare equality between metric settings
        return this.threshold.equals(that.threshold)
            && this.thresholdIsUpperBound.equals(that.thresholdIsUpperBound)
            && this.weight.equals(that.weight)
            && Objects.equals(this.metric.getId(), that.metric.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            this.threshold,
            this.thresholdIsUpperBound,
            this.weight,
            this.metric.getId()
        );
    }

    /**
     * Update the values of this based on a {@link MetricSettingsWeb} object.
     *
     * @param metricSettingsWeb The {@link MetricSettingsWeb} instance to update based upon.
     * @post all fields in this are updated according to the data contained in the
     *     metricSettingsWeb.
     */
    public void setAllFromMetricSettingsWeb(MetricSettingsWeb metricSettingsWeb) {
        this.setThreshold(metricSettingsWeb.getThreshold());
        this.setThresholdIsUpperBound(metricSettingsWeb.getThresholdIsUpperBound());
        this.setWeight(metricSettingsWeb.getWeight());
    }

    /**
     * Whether or not a score is low quality based on the information
     * in this {@link MetricSettings}.
     *
     * @param score The score to compare against.
     * @return      Whether or not the score is low quality based on the settings.
     */
    public boolean isLowQuality(Double score) {
        if (this.getThresholdIsUpperBound()) {
            return score > this.getThreshold();
        }

        return score < this.getThreshold();
    }

    /**
     * Converts the current {@link MetricSettings} object into a {@link MetricSettingsWeb}.
     *
     * @return  The converted {@link MetricSettingsWeb}.
     */
    public MetricSettingsWeb getWebObject() {
        MetricSettingsWeb web = new MetricSettingsWeb();

        web.setId(this.getId());
        web.setThresholdIsUpperBound(this.getThresholdIsUpperBound());
        web.setThreshold(this.getThreshold());
        web.setWeight(this.getWeight());
        web.setName(this.getMetric().getName());

        return web;
    }
}
