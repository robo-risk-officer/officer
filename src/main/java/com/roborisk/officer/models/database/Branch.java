package com.roborisk.officer.models.database;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 * Branch implements the Git branch object model.
 */
@Entity(name = "branch")
public class Branch {
    /**
     * Unique Identifier for a {@link Branch} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * name stores the name of the branch.
     */
    @NotNull
    @Getter @Setter private String name;

    /**
     * repository stores the repository that the branch is part of.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private Repository repository;

    /**
     * latestCommit stores a reference to the latest commit in this Branch.
     */
    @OneToOne @NotNull
    @Getter @Setter private GitCommit latestCommit;

    /**
     * Empty constructor for the Branch class.
     */
    public Branch() {
    }

    /**
     * Branch constructor.
     *
     * @param name          The name of the branch.
     * @param repository    The repository to link the branch to.
     * @param commit        The latest commit on this branch.
     */
    public Branch(String name, Repository repository, GitCommit commit) {
        this();

        this.setName(name);
        this.setRepository(repository);
        this.setLatestCommit(commit);
    }
}
