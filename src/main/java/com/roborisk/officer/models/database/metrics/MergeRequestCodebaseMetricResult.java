package com.roborisk.officer.models.database.metrics;

import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequestReview;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * The MrCodebaseMetricResult Model represents the "mr_codebase_metric_result" database table.
 */
@Entity(name = "mr_codebase_metric_result")
public class MergeRequestCodebaseMetricResult extends MergeRequestMetricResult {

    /**
     * Unique Identifier for a {@link MergeRequestCodebaseMetricResult} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * mergeRequestReview stores a reference to the object containing the review which
     * this metric result is a part of.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private MergeRequestReview mergeRequestReview;

    @Override
    public GitCommit getAssociatedCommit() {
        return this.mergeRequestReview.getCommit();
    }

    @Override
    public String getArtefactName() {
        return "entire codebase";
    }
}
