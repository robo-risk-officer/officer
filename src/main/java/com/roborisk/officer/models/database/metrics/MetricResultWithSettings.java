package com.roborisk.officer.models.database.metrics;

import com.roborisk.officer.models.database.MetricSettings;
import lombok.Getter;
import lombok.Setter;

/**
 * MetricResultWithSettings is a generic class to store {@link AbstractMetricResult}
 * together with the associated {@link MetricSettings}.
 */
public class MetricResultWithSettings<T extends AbstractMetricResult> {

    /**
     * metricResult stores the {@link AbstractMetricResult} of type T.
     */
    @Getter @Setter private T metricResult;

    /**
     * settings stores the {@link MetricSettings} associated to the metricResult for the
     * GitLab repository the metricResult is associated to.
     */
    @Getter @Setter private MetricSettings settings;

    /**
     * Public constructor.
     */
    public MetricResultWithSettings() {

    }
}
