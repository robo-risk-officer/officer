package com.roborisk.officer.models.database.metrics;

import com.roborisk.officer.models.database.BranchFunctionObservation;
import com.roborisk.officer.models.database.GitCommit;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * BranchFunctionMetricResult implements the branch_function_metric_result database table.
 */
@Entity(name = "branch_function_metric_result")
public class BranchFunctionMetricResult extends BranchMetricResult {

    /**
     * Unique Identifier for a {@link BranchFunctionMetricResult} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * branchFunctionObservation stores the BranchFunctionObservation associated with
     * this metric result.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private BranchFunctionObservation branchFunctionObservation;

    @Override
    public GitCommit getAssociatedCommit() {
        return this.branchFunctionObservation.getGitCommit();
    }
}
