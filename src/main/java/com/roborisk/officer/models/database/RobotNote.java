package com.roborisk.officer.models.database;

import com.roborisk.officer.models.enums.PostgreSQLEnumType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * Note implements the Git note object model.
 */
@Entity(name = "robot_note")
@TypeDef(
        name = "pgsql_enum",
        typeClass = PostgreSQLEnumType.class
)
@SuppressWarnings({"ClassFanOutComplexity"})
public class RobotNote {

    /**
     * Unique Identifier for a {@link RobotNote} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * gitLabId stores the GitLab id of the Note.
     */
    @NotNull @Column(unique = true, name = "gitlab_id")
    @Getter @Setter private Integer gitLabId;

    /**
     * discussionThread stores the {@link DiscussionThread} the {@link RobotNote} is associated to.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE)
    @Getter @Setter private DiscussionThread discussionThread;
}
