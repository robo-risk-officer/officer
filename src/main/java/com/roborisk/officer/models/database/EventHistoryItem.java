package com.roborisk.officer.models.database;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * The EventHistoryItem Model represents a hashed incoming webhook request from GitLab.
 * The model is being used to check if an event from GitLab already has been processed.
 */
@Entity(name = "event")
public class EventHistoryItem {

    /**
     * Unique Identifier for an {@link EventHistoryItem} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * The hashed value of the incoming JSON from GitLab.
     */
    @NotNull
    @Getter @Setter private Long hash;

    /**
     * The timestamp of the first event received with this hash.
     */
    @NotNull
    @Getter @Setter private Long timestamp;
}
