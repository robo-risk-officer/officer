package com.roborisk.officer.models.database.metrics;

import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequestClassObservation;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * MergeRequestClassMetricResult implements the mr_class_metric_result database table.
 */
@Entity(name = "mr_class_metric_result")
public class MergeRequestClassMetricResult extends MergeRequestMetricResult {

    /**
     * Unique Identifier for a {@link MergeRequestClassMetricResult} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * mergeRequestClassObservation stores the {@link MergeRequestClassObservation} that this metric
     * result is about.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @JoinColumn(name = "mr_class_observation_id")
    @Getter @Setter private MergeRequestClassObservation mergeRequestClassObservation;

    @Override
    public String getArtefactName() {
        return "'" + this.mergeRequestClassObservation.getClassName()
                + "' in '" + this.mergeRequestClassObservation.getMergeRequestFile()
                    .getFilePath() + "'";
    }

    @Override
    public GitCommit getAssociatedCommit() {
        return this.mergeRequestClassObservation.getMergeRequestFile()
                .getMergeRequestReview().getCommit();
    }

    /**
     * Empty constructor.
     */
    public MergeRequestClassMetricResult() {

    }

    /**
     * Copy constructor to create an {@link MergeRequestClassMetricResult} equivalent to a
     * {@link BranchClassMetricResult} object.
     *
     * @param branchClassMetricResult       The {@link BranchClassMetricResult} to copy.
     * @param mergeRequestClassObservation  The {@link MergeRequestClassObservation} object to
     *                                      link the new {@link MergeRequestClassMetricResult} to.
     */
    public MergeRequestClassMetricResult(BranchClassMetricResult branchClassMetricResult,
            MergeRequestClassObservation mergeRequestClassObservation) {
        this.setMergeRequestClassObservation(mergeRequestClassObservation);
        this.setIsLowQuality(branchClassMetricResult.getIsLowQuality());
        this.setMetric(branchClassMetricResult.getMetric());
        this.setScore(branchClassMetricResult.getScore());
    }
}
