package com.roborisk.officer.models.database;

import com.roborisk.officer.models.web.MergeRequestWeb;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * MergeRequest implements the merge request object model.
 */
@Entity(name = "merge_request")
public class MergeRequest {

    /**
     * Unique Identifier for a {@link MergeRequest} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * gitLabId stores the (all merge request tables) unique GitLab merge request id.
     */
    @NotNull @Column(unique = true, name = "gitlab_id")
    @Getter @Setter private Integer gitLabId;

    /**
     * gitLabIid stores the (project scope) unique GitLab merge request id.
     */
    @NotNull @Column(name = "gitlab_iid")
    @Getter @Setter private Integer gitLabIid;

    /**
     * titles stores the title of the merge request.
     */
    @NotNull
    @Getter @Setter private String title;

    /**
     * createdAt stores the date at which the merge request was created (in GitLab).
     */
    @NotNull
    @Getter @Setter private Integer createdAt;

    /**
     * updatedAt stores the date at which the merge request was last updated (in GitLab).
     */
    @NotNull
    @Getter @Setter private Integer updatedAt;

    /**
     * state stores the current state of the merge request.
     */
    @NotNull
    @Getter @Setter private String state;

    /**
     * mergeStatus stores the mergeability of the merge request.
     */
    @NotNull
    @Getter @Setter private String mergeStatus;

    /**
     * target stores the target Branch of the merge request.
     */
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @NotNull
    @Getter @Setter private Branch targetBranch;

    /**
     * source stores the source Branch of the merge request.
     */
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @NotNull
    @Getter @Setter private Branch sourceBranch;

    /**
     * Empty constructor for the MergeRequest class.
     */
    public MergeRequest() {
    }

    /**
     * Construct a MergeRequest based on a {@link MergeRequestWeb} and
     * the source and target {@link Branch} object.
     *
     * @param web       The {@link MergeRequestWeb} to copy.
     * @param source    The {@link Branch} to link to the source.
     * @param target    The {@link Branch} to link to the target.
     */
    public MergeRequest(MergeRequestWeb web, Branch source, Branch target) {
        this.setCreatedAt(web.getCreatedAt());
        this.setUpdatedAt(web.getUpdatedAt());
        this.setGitLabId(web.getGitLabId());
        this.setGitLabIid(web.getGitLabIid());
        this.setMergeStatus(web.getMergeStatus());
        this.setState(web.getState());
        this.setTitle(web.getTitle());
        this.setSourceBranch(source);
        this.setTargetBranch(target);
    }

    /**
     * Converts the current {@link MergeRequest} object into a {@link MergeRequestWeb}.
     *
     * @return  The converted {@link MergeRequestWeb}.
     */
    public MergeRequestWeb getWebObject() {
        MergeRequestWeb web = new MergeRequestWeb();

        web.setGitLabId(this.getGitLabId());
        web.setCreatedAt(this.getCreatedAt());
        web.setGitLabIid(this.getGitLabIid());
        web.setMergeStatus(this.getMergeStatus());
        web.setSource(this.getSourceBranch().getName());
        web.setState(this.getState());
        web.setTarget(this.getTargetBranch().getName());
        web.setTitle(this.getTitle());
        web.setUpdatedAt(this.getUpdatedAt());
        web.setWip(this.getTitle().startsWith("WIP"));

        return web;
    }
}
