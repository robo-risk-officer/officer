package com.roborisk.officer.models.database;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

/**
 * AbstractFile provides an abstract representation of a file object.
 * Please, pretty please, do not instantiate this file; unfortunately, it cannot be made abstract
 * due to the Spring JPA code crashing otherwise.
 */
@MappedSuperclass
public class AbstractFile {

    /**
     * fileName stores the name of the file.
     */
    @NotNull
    @Getter @Setter private String fileName;

    /**
     * filePath stores the full path of the file.
     */
    @NotNull
    @Getter @Setter private String filePath;
}
