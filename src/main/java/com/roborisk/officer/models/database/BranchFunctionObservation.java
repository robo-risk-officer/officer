package com.roborisk.officer.models.database;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * BranchFunctionObservation implements the branch_function_observation database table.
 */
@Entity(name = "branch_function_observation")
public class BranchFunctionObservation extends AbstractBranchObservation {

    /**
     * Unique Identifier for a {@link BranchFunctionObservation} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * Stores the function name of the observation.
     */
    @NotNull
    @Getter @Setter private String functionName;
}
