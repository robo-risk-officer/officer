package com.roborisk.officer.models.database;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * MergeRequestClassObservation implements the mr_class_observation database table.
 */
@Entity(name = "mr_class_observation")
public class MergeRequestClassObservation extends AbstractMergeRequestObservation {

    /**
     * Unique Identifier for a {@link MergeRequestClassObservation} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * Stores the classname of the observation.
     */
    @NotNull
    @Getter @Setter private String className;

    /**
     * Empty constructor.
     */
    public MergeRequestClassObservation() {

    }

    /**
     * A copy constructor to copy a {@link BranchClassObservation} into an equivalent
     * {@link MergeRequestClassObservation}.
     *
     * @param branchClassObservation    The {@link BranchClassObservation} to copy.
     * @param mergeRequestFile          The {@link MergeRequestFile} to link the new
     *                                  {@link MergeRequestClassObservation} object to.
     */
    public MergeRequestClassObservation(BranchClassObservation branchClassObservation,
            MergeRequestFile mergeRequestFile) {
        this.setClassName(branchClassObservation.getClassName());
        this.setLineNumber(branchClassObservation.getLineNumber());
        this.setMergeRequestFile(mergeRequestFile);
        this.setIsSuppressed(false);
        this.setIsFalsePositive(false);
        this.setDiscussionThread(null);
    }
}
