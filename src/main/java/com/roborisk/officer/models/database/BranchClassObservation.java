package com.roborisk.officer.models.database;

import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * BranchClassObservation implements the branch_class_observation database table.
 */
@Entity(name = "branch_class_observation")
public class BranchClassObservation extends AbstractBranchObservation {

    /**
     * Unique Identifier for a {@link BranchClassObservation} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;
}
