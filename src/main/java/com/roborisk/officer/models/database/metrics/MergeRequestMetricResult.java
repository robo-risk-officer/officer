package com.roborisk.officer.models.database.metrics;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;

/**
 * MergeRequestMetric is an abstraction of all {@link AbstractMetricResult} stored in the
 * database concerning merge requests.
 */
@MappedSuperclass
public abstract class MergeRequestMetricResult extends AbstractMetricResult {

    /**
     * score stores the score received by the metric being used.
     */
    @Getter @Setter private Double score;

    /**
     * Getter for the name of the artefact that the metric result is associated to.
     * This could e.g. be the name of the associated function.
     *
     * @return Name of the associated artefact.
     */
    public abstract String getArtefactName();
}
