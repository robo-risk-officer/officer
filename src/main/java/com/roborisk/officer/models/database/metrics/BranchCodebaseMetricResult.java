package com.roborisk.officer.models.database.metrics;

import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * BranchCodebaseMetricResult implements the branch_codebase_metric_result database table.
 */
@Entity(name = "branch_codebase_metric_result")
public class BranchCodebaseMetricResult extends BranchMetricResult {

    /**
     * Unique Identifier for a {@link BranchCodebaseMetricResult} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * branch stores a reference to the Branch that this metric result is about.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private Branch branch;

    /**
     * lastChangeCommit stores a reference to the commit for which this result was created.
     */
    @ManyToOne @NotNull
    @Getter @Setter private GitCommit lastChangeCommit;

    @Override
    public GitCommit getAssociatedCommit() {
        return this.lastChangeCommit;
    }
}
