package com.roborisk.officer.models.database.metrics;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

/**
 * BranchMetricResult is an abstraction of all {@link AbstractMetricResult} stored in the
 * database concerning branch.
 */
@MappedSuperclass
public abstract class BranchMetricResult extends AbstractMetricResult {

    /**
     * score stores the score received by the metric being used.
     */
    @NotNull
    @Getter @Setter private Double score;
}
