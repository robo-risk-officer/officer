package com.roborisk.officer.models.database;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * {@link SonarAnalysisTracker} implements the SonarQube analysis tracker object model.
 */
@Entity(name = "sonar_analysis_tracker")
public class SonarAnalysisTracker {

    /**
     * Unique identified for a {@link SonarAnalysisTracker} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * workingDirectory stores the full path to the files that SonarQube analysed.
     */
    @NotNull @Column(unique = true)
    @Getter @Setter private String name;

    /**
     * isCompleted stores whether the analysis by SonarQube has been completed.
     */
    @Getter @Setter private Boolean isCompleted;

    /**
     * isSourceBranch stores whether the branch that was analysed by SonarQube was the source
     * branch of the merge request.
     */
    @Getter @Setter private boolean isSourceBranch;

    /**
     * mergeRequestReview stores the review of which this sonar tracker object is a part.
     */
    @ManyToOne @NotNull @OnDelete(action = OnDeleteAction.CASCADE)
    @Getter @Setter private MergeRequestReview mergeRequestReview;
}
