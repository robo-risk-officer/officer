package com.roborisk.officer.models.database;

import com.roborisk.officer.models.web.RepositoryApiWeb;
import com.roborisk.officer.models.web.RepositoryWeb;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * Repository implements the Git repository object model.
 */
@Entity(name = "repository")
public class Repository {

    /**
     * Unique Identifier for a {@link Repository} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * gitLabId stores the id that GitLab assigned to the repository.
     */
    @NotNull @Column(unique = true, name = "gitlab_id")
    @Getter @Setter private Integer gitLabId;

    /**
     * name stores the name of the repository.
     */
    @NotNull
    @Getter @Setter private String name;

    /**
     * sshUrl stores the ssh URL of the repository.
     */
    @Getter @Setter private String sshUrl;

    /**
     * httpUrl stores the http URL of the repository.
     */
    @Getter @Setter private String httpUrl;

    /**
     * namespace stores the namespace of the repository.
     */
    @NotNull
    @Getter @Setter private String namespace;

    /**
     * pathWithNamespace stores the path containing the namespace (without spaces) and the name
     * of the repository.
     */
    @NotNull
    @Getter @Setter private String pathWithNamespace;

    /**
     * isEnabled stores whether the repository is enabled.
     */
    @NotNull
    @Getter @Setter private Boolean isEnabled;

    /**
     * Empty constructor for the Repository class.
     */
    public Repository() { }

    /**
     * Constructor for the Repository class.
     *
     * @param isEnabled Whether this repository is enabled by default.
     */
    public Repository(Boolean isEnabled) {
        this.setIsEnabled(isEnabled);
    }

    /**
     * Construct a {@link Repository} from a {@link RepositoryWeb}.
     *
     * @param web   The {@link RepositoryWeb} object to copy from.
     */
    public Repository(RepositoryWeb web) {
        this.setIsEnabled(true);
        this.setSshUrl(web.getSshUrl());
        this.setHttpUrl(web.getHttpUrl());
        this.setName(web.getName());
        this.setNamespace(web.getNamespace());
        this.setPathWithNamespace(web.getPathWithNamespace());
        this.setGitLabId(web.getGitLabId());
    }

    /**
     * Method to check whether the contents of a {@link RepositoryApiWeb} object are the same as
     * stored in this object.
     *
     * @param repositoryWeb The {@link RepositoryApiWeb} instance to compare against.
     * @return              True if all the corresponding fields in repositoryWeb
     *                      are the same as stored in this object.
     */
    public boolean isEqualToRepositoryWeb(RepositoryApiWeb repositoryWeb) {
        if (!this.id.equals(repositoryWeb.getId())) {
            return false;
        }

        if (!this.name.equals(repositoryWeb.getName())) {
            return false;
        }

        if (!this.httpUrl.equals(repositoryWeb.getHttpUrl())) {
            return false;
        }

        if (!this.sshUrl.equals(repositoryWeb.getSshUrl())) {
            return false;
        }

        if (!this.namespace.equals(repositoryWeb.getNamespace())) {
            return false;
        }

        if (!this.pathWithNamespace.equals(repositoryWeb.getPathWithNamespace())) {
            return false;
        }

        return this.isEnabled == repositoryWeb.isEnabled();
    }

    /**
     * Update a {@link Repository} based on a {@link RepositoryWeb}.
     *
     * @param web The {@link RepositoryWeb} object to use for the update.
     */
    public void update(RepositoryWeb web) {
        this.setGitLabId(web.getGitLabId());
        this.setHttpUrl(web.getHttpUrl());
        this.setSshUrl(web.getSshUrl());
        this.setName(web.getName());
        this.setNamespace(web.getNamespace());
        this.setPathWithNamespace(web.getPathWithNamespace());
    }

    /**
     * Converts the current {@link Repository} object into a {@link RepositoryWeb}.
     *
     * @return  The converted {@link RepositoryWeb}.
     */
    public RepositoryWeb getWebObject() {
        RepositoryWeb web = new RepositoryWeb();

        web.setGitLabId(this.getGitLabId());
        web.setName(this.getName());
        web.setNamespace(this.getNamespace());
        web.setPathWithNamespace(this.getPathWithNamespace());
        web.setHttpUrl(this.getHttpUrl());
        web.setSshUrl(this.getSshUrl());
        web.setEnabled(this.getIsEnabled());

        return web;
    }

    /**
     * Converts the current {@link Repository} object into a {@link RepositoryApiWeb}.
     *
     * @return  The converted {@link RepositoryApiWeb}.
     */
    public RepositoryApiWeb getApiWebObject() {
        RepositoryApiWeb api = new RepositoryApiWeb();

        api.setId(this.getId());
        api.setName(this.getName());
        api.setNamespace(this.getNamespace());
        api.setPathWithNamespace(this.getPathWithNamespace());
        api.setHttpUrl(this.getHttpUrl());
        api.setSshUrl(this.getSshUrl());
        api.setEnabled(this.getIsEnabled());

        return api;
    }
}
