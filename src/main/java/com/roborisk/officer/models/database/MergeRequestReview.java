package com.roborisk.officer.models.database;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 * MergeRequestReview implements the merge request review object model.
 */
@Entity(name = "merge_request_review")
public class MergeRequestReview {

    /**
     * Unique Identifier for a {@link MergeRequestReview} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * mergeRequest stores a reference to the merge request being reviewed in this review.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private MergeRequest mergeRequest;

    /**
     * commit stores a reference to the commit for which this MergeRequestReview was created.
     */
    @ManyToOne @NotNull
    @Getter @Setter private GitCommit commit;

    /**
     * discussionThread stores the {@link DiscussionThread} for this {@link MergeRequestReview}
     * in which the
     * {@link com.roborisk.officer.models.database.metrics.MergeRequestCodebaseMetricResult}s of
     * this {@link MergeRequestReview} are being displayed.
     */
    @ManyToOne
    @Getter @Setter private DiscussionThread discussionThread;

    /**
     * conclusionThread stores the {@link DiscussionThread} for this {@link MergeRequestReview}
     * in which the final conclusion of the {@link MergeRequest} is being displayed.
     */
    @OneToOne
    @Getter @Setter private DiscussionThread conclusionThread;

    /**
     * Constructor for MergeRequestReview class.
     */
    public MergeRequestReview() {
    }
}
