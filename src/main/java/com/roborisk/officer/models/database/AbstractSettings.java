package com.roborisk.officer.models.database;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

/**
 * Abstraction used by various settings that ROBOrisk Officer supports.
 * Each setting is stored in the atabase by an index and is linked to a repository.
 */
@MappedSuperclass
public abstract class AbstractSettings {

    /**
     * Unique Identifier for a {@link AbstractSettings} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter protected Integer id;

    /**
     * The repository the settings are linked to.
     */
    @OneToOne
    @Getter @Setter private Repository repository;
}
