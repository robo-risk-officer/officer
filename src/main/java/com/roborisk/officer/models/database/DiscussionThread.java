package com.roborisk.officer.models.database;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * DiscussionThread implements the "discussion_thread" database table.
 */
@Entity(name = "discussion_thread")
@SuppressWarnings({"ClassFanOutComplexity"})
public class DiscussionThread {

    /**
     * Unique Identifier for a {@link DiscussionThread} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * gitLabId stores the GitLab id of the discussion thread.
     */
    @NotNull @Column(unique = true, name = "gitlab_id")
    @Getter @Setter private String gitLabId;

    /**
     * isResolved stores whether the GitLab thread has been resolved.
     */
    @Getter @Setter private Boolean isResolved;

    /**
     * mergeRequest stores the {@link MergeRequest} objects representing the GitLab discussion in
     * which this {@link DiscussionThread} has been created.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private MergeRequest mergeRequest;

    /**
     * Constructs an empty {@link DiscussionThread}.
     */
    public DiscussionThread() {
    }
}
