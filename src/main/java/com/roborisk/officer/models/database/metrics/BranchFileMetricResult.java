package com.roborisk.officer.models.database.metrics;

import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.GitCommit;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * BranchFileMetricResult implements the branch_file_metric_result database table.
 */
@Entity(name = "branch_file_metric_result")
public class BranchFileMetricResult extends BranchMetricResult {

    /**
     * Unique Identifier for a {@link BranchFileMetricResult} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * branchFile stores a reference to the BranchFile that it is a result on.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private BranchFile branchFile;

    /**
     * lastChangeCommit stores a reference to the commit for which this result was created.
     */
    @ManyToOne @NotNull
    @Getter @Setter private GitCommit lastChangeCommit;

    /**
     * Empty constructor.
     */
    public BranchFileMetricResult() {
    }

    /**
     * Copy constructor, makes a copy of another {@link BranchFileMetricResult}.
     *
     * @param result    The object to copy.
     */
    public BranchFileMetricResult(BranchFileMetricResult result) {
        this.setLastChangeCommit(result.getLastChangeCommit());
        this.setBranchFile(result.getBranchFile());
        this.setScore(result.getScore());
        this.setMetric(result.getMetric());
        this.setIsLowQuality(result.getIsLowQuality());
    }

    @Override
    public GitCommit getAssociatedCommit() {
        return this.lastChangeCommit;
    }
}
