package com.roborisk.officer.models.database;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

/**
 * Abstract representation of a merge request observation.
 * Please, pretty please, do not instantiate this class, as unfortunately it cannot be abstract
 * due to the Spring JPA code crashing otherwise.
 */
@MappedSuperclass
public class AbstractMergeRequestObservation {

    /**
     * Whether the observation has been flagged as a false positive.
     */
    @NotNull
    @Getter @Setter private Boolean isFalsePositive;

    /**
     * Whether the observation has been suppressed.
     */
    @NotNull
    @Getter @Setter private Boolean isSuppressed;

    /**
     * Stores the line number of the observation.
     */
    @NotNull
    @Getter @Setter private int lineNumber;

    /**
     * mergeRequestFile stores a reference to the MergeRequestFile that this observation is about.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private MergeRequestFile mergeRequestFile;

    /**
     * discussionThread stores the {@link DiscussionThread} for the
     * {@link AbstractMergeRequestObservation}.
     */
    @ManyToOne
    @Getter @Setter private DiscussionThread discussionThread;
}
