package com.roborisk.officer.models.database.metrics;

import com.roborisk.officer.models.enums.MetricTypeEnum;
import com.roborisk.officer.models.enums.PostgreSQLEnumType;
import com.roborisk.officer.models.enums.QualityTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * Metric implements the metric database table.
 */
@Entity(name = "metric")
@TypeDef(
        name = "pgsql_enum",
        typeClass = PostgreSQLEnumType.class
)
public class Metric {

    /**
     * Max length of name.This is in accordance with the max length of the corresponding database
     * value.
     */
    private static final int MAX_NAME_LENGTH = 255;

    /**
     * Max length of displayName.This is in accordance with the max length of the corresponding
     * database value.
     */
    private static final int MAX_DISPLAY_NAME_LENGTH = 255;

    /**
     * Max length of displayName.This is in accordance with the max length of the corresponding
     * database value.
     */
    private static final int MAX_VIEW_LENGTH = 255;

    /**
     * Unique Identifier for a {@link Metric} database record.
     */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter private Integer id;

    /**
     * name stores the name of the metric.
     */
    @NotNull
    @Getter private String name;

    /**
     * description stores a description of what the metric does.
     */
    @NotNull
    @Getter @Setter private String description;

    /**
     * Setter for value name which checks the length of the input.
     *
     * @param   newName                     New value to which this.name should be set.
     * @throws  IllegalArgumentException    When newName.length() > this.maxNameLength.
     */
    public void setName(String newName) {
        if (newName.length() > Metric.MAX_NAME_LENGTH) {
            throw new IllegalArgumentException(String.format(
                "The metric name has to be shorter than %s characters", Metric.MAX_VIEW_LENGTH));
        }

        this.name = newName;
    }

    /**
     * name stores the identifier of the view which is supposed to be used as evaluation for this
     * metric.
     */
    @Getter private String viewIdentifier;

    /**
     * Setter for value view which checks the length of the input.
     *
     * @param   newView                     New value to which this.name should be set.
     * @throws  IllegalArgumentException    When newName.length() > this.maxNameLength.
     */
    public void setViewIdentifier(String newView) {
        if (newView.length() > Metric.MAX_VIEW_LENGTH) {
            throw new IllegalArgumentException(String.format(
                "The view identifier has to be shorter than %s characters",
                Metric.MAX_VIEW_LENGTH));
        }

        this.viewIdentifier = newView;
    }

    /**
     * displayName stores the name to be displayed in the frontend.
     */
    @NotNull
    @Getter private String displayName;

    /**
     * Setter for value displayName which checks the length of the input.
     *
     * @param   newDisplayName              New value to which this.displayName should be set.
     * @throws  IllegalArgumentException    When
     *                                      newDisplayName.length() > this.maxDisplayNameLength.
     */
    public void setDisplayName(String newDisplayName) {
        if (newDisplayName.length() > Metric.MAX_DISPLAY_NAME_LENGTH) {
            throw new IllegalArgumentException(String.format(
                "The metric displayName has to be shorter than %s characters",
                Metric.MAX_DISPLAY_NAME_LENGTH));
        }
        this.displayName = newDisplayName;
    }

    /**
     * qualityType stores the type of quality.
     */
    @Enumerated(EnumType.STRING) @Type(type = "pgsql_enum") @NotNull
    @Getter @Setter private QualityTypeEnum qualityType;

    /**
     * metricType stores the type of the metric.
     */
    @Enumerated(EnumType.STRING) @Type(type = "pgsql_enum") @NotNull
    @Getter @Setter private MetricTypeEnum metricType;

    @Override
    public boolean equals(Object o) {
        return (o instanceof Metric) && (((Metric) o).getId()).equals(this.getId());
    }

    @Override
    public int hashCode() {
        return this.getId();
    }
}
