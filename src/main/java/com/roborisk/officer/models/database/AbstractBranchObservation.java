package com.roborisk.officer.models.database;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

/**
 * Abstract representation of a branch observation.
 * Please, pretty please, do not instantiate this class, as unfortunately it cannot be abstract
 * due to the Spring JPA code crashing otherwise.
 */
@MappedSuperclass
public class AbstractBranchObservation {

    /**
     * Stores the line number of the observation.
     */
    @NotNull
    @Getter @Setter private int lineNumber;

    /**
     * branchFile stores a reference to the BranchFile that this observation is about.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull
    @Getter @Setter private BranchFile branchFile;

    /**
     * Stores the classname of the observation.
     */
    @NotNull
    @Getter @Setter private String className;

    /**
     * gitCommit stores the GitCommit that this branch class observation is about.
     */
    @ManyToOne @OnDelete(action = OnDeleteAction.CASCADE) @NotNull @JoinColumn(name = "commit_id")
    @Getter @Setter private GitCommit gitCommit;
}
