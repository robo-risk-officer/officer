package com.roborisk.officer.models.web;

import com.roborisk.officer.handlers.Dispatcher;
import com.roborisk.officer.models.database.SonarAnalysisTracker;
import com.roborisk.officer.models.enums.EventType;
import lombok.Getter;
import lombok.Setter;

/**
 * This extends the {@link AbstractWebhookData} class and is send by the SonarDataParser
 * to the {@link Dispatcher} when a source branch analysis by SonarQube has been completed
 * and its webhook has been received by the Robo Risk Officer.
 */
public class InternalSonarWebhookData extends AbstractWebhookData {

    /**
     * sourceBranchAnalysis stores the {@link SonarAnalysisTracker} of the source branch of
     * the SonarQube analysis.
     */
    @Getter @Setter private SonarAnalysisTracker sourceBranchAnalysis;

    @Override
    public EventType getEventType() {
        return EventType.SONAR_ANALYSIS_FINISHED;
    }
}
