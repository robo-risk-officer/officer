package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.roborisk.officer.models.enums.EventType;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

/**
 * MergeRequestWebhookData implements the merge request webhook data model.
 * MergeRequestWebhookData is the equivalent object of the relevant attributes
 * of the request body JSON that GitLab sends when a merge request is created/updated/merged.
 */
public class MergeRequestWebhookData extends AbstractWebhookData {

    /**
     * user stores the Git user that created/updated/merged the merge request.
     */
    @JsonProperty("user")
    @Getter @Setter private GitUser user;

    /**
     * mergeRequest stores the merge request of the webhook.
     */
    @JsonProperty("object_attributes")
    @Getter private MergeRequestWeb mergeRequest;

    /**
     * Setter for mergeRequest variable.
     *
     * @param map               JSON webhook data.
     * @throws IOException      When the last_commit inside of the map cannot be parsed.
     * @throws ParseException   When a timestamp in the map cannot be parsed.
     */
    public void setMergeRequest(Map<String, Object> map) throws ParseException, IOException {
        this.mergeRequest = MergeRequestWeb.fromJson(map);
    }

    @Override
    public EventType getEventType() {
        String state = this.mergeRequest.getState();
        String action = this.mergeRequest.getAction();

        if (action.equals("opened") && state.equals("open")) {
            return EventType.MERGE_REQUEST_CREATED;
        }

        // The RRO does not process closed or merged merge requests
        if (state.equals("merged") || state.equals("closed")) {
            return EventType.NONE;
        }

        return EventType.MERGE_REQUEST_CHANGED;
    }
}
