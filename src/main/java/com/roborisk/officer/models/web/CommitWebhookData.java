package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.roborisk.officer.models.enums.EventType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * CommitWebhookData implements the commit webhook data object model.
 * CommitWebhookData is the equivalent object of the relevant attributes of the
 * request body JSON that GitLab sends when a push (of commits) occurs (except when pushing tags).
 */
public class CommitWebhookData extends AbstractWebhookData {

    /**
     * before stores the hash of the last commit preceding the commits sent in this push webhook.
     */
    @JsonProperty("before")
    @Getter @Setter private String before;

    /**
     * after stores the hash of the new head of the branch on which the push webhook was sent.
     */
    @JsonProperty("after")
    @Getter @Setter private String after;

    /**
     * The reference of Git towards the resource.
     * E.g. "refs/heads/master"
     */
    @JsonProperty("ref")
    @Getter @Setter private String ref;

    /**
     * checkoutSha stores the checkout hash that would be needed
     * to obtain the state of the branch directly after the push webhook.
     */
    @JsonProperty("checkout_sha")
    @Getter @Setter private String checkoutSha;

    /**
     * userId stores the id of the user that pushed the commit(s) sent in this webhook.
     */
    @JsonProperty("user_id")
    @Getter @Setter private Integer userId;

    /**
     * userName stores the name of the user that pushed the commit(s) sent in this webhook.
     */
    @JsonProperty("user_name")
    @Getter @Setter private String userName;

    /**
     * repositoryId stores the id of the project for which this webhook was sent.
     */
    @JsonProperty("project_id")
    @Getter @Setter private Integer repositoryId;

    /**
     * totalCommitsCount stores the total number of commits that were sent in this push webhook.
     */
    @JsonProperty("total_commits_count")
    @Getter @Setter private Integer totalCommitsCount;

    /**
     * commits stores a list of the Git commits that were sent in this push webhook.
     */
    @JsonProperty("commits")
    @Getter @Setter private List<GitCommitWeb> commits;

    @Override
    public EventType getEventType() {
        return EventType.PUSH_EVENT;
    }

    /**
     * Gets the branch name of this commit.
     *
     * @throws IllegalStateException    If the ref is empty.
     * @return                          The branch name of this commit.
     */
    public String getBranchName() throws IllegalStateException {
        String[] parts = this.getRef().split("/");

        if (parts.length == 0) {
            String msg = "CommitWebhookData.getBranchName: cannot parse ref: " + this.getRef();
            throw new IllegalStateException(msg);
        }

        return parts[parts.length - 1];
    }
}
