package com.roborisk.officer.models.web;

import com.roborisk.officer.models.enums.EventType;

/**
 * Empty event return type to do nothing.
 */
public class EmptyWebhookData extends AbstractWebhookData {

    @Override
    public EventType getEventType() {
        return EventType.NONE;
    }
}
