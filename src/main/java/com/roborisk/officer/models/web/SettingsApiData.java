package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Represents payload used for api requests on path /settings/.
 *
 * <p>
 * Example payload:
 * <pre>
 * {
 *     "name": "Officer",
 *     "code_metrics": [
 *         {
 *             "id" : 1,
 *             "name": "Cyclomatic Complexity",
 *             "threshold": 15,
 *             "threshold_is_upper_bound": false
 *             "weight": 10
 *         },
 *     ],
 *     "settings": {
 *       "automatic_review_on_new_mr": true,
 *       "automatic_review_on_commit": true,
 *       "false_postive_threshold": 10,
 *       "review_history_length": 5,
 *     }
 * }
 * </pre>
 * </p>
 */
public class SettingsApiData {

    /**
     * Name of the repository.
     */
    @JsonProperty("name")
    @Getter @Setter private String name;

    /**
     * List of specific code metrics used for analyzing code.
     */
    @JsonProperty("code_metrics")
    @Getter @Setter private List<MetricSettingsWeb> metricSettings;

    /**
     * List of general robot settings that define Robot's behaviour.
     */
    @JsonProperty("settings")
    @Getter @Setter private RobotSettingsWeb robotSettings;

    public SettingsApiData() {
        this.metricSettings = new ArrayList<>();
    }

    /**
     * Method to add a single {@link MetricSettingsWeb} object to the metricSettings array.
     *
     * @param setting The {@link MetricSettingsWeb} instance to be added.
     */
    public void addToMetricSettings(MetricSettingsWeb setting) {
        this.metricSettings.add(setting);
    }

    /**
     * Method to search for a {@link MetricSettingsWeb} in metricSettings based on an id.
     *
     * @param id {@link Integer} indicating the id of the {@link MetricSettingsWeb} object searched
     *           for.
     * @return The {@link MetricSettingsWeb} object with the same id as passed in, null if not
     *     exists.
     */
    public Optional<MetricSettingsWeb> findMetricSetting(Integer id) {
        for (MetricSettingsWeb metricSettingsWeb : this.metricSettings) {
            if (metricSettingsWeb.getId().equals(id)) {
                return Optional.of(metricSettingsWeb);
            }
        }

        return Optional.empty();
    }
}
