package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Implements the object model representing the response that SonarQube gives when requesting
 * unresolved issues via the SonarApiWrapper getUnresolvedIssues function.
 */
public class SonarUnresolvedIssuesWeb {

    /**
     * totalPages stores the total number of pages.
     */
    @JsonProperty("total")
    @Getter @Setter private Integer totalPages;

    /**
     * currentPage stores the current page.
     */
    @JsonProperty("p")
    @Getter @Setter private Integer currentPage;

    /**
     * pageSize stores the number of issues per page.
     */
    @JsonProperty("ps")
    @Getter @Setter private Integer pageSize;

    /**
     * issues stores a list of {@link SonarIssueWeb} objects that are on the page.
     */
    @JsonProperty("issues")
    @Getter @Setter List<SonarIssueWeb> issues;
}
