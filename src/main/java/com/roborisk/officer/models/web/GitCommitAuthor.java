package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * GitCommitAuthor implements the Git commit author object model for GitLab webhooks.
 */
public class GitCommitAuthor {

    /**
     * name stores the name of the GitCommitAuthor.
     */
    @JsonProperty("name")
    @Getter @Setter private String name;

    /**
     * email stores the email of the GitCommitAuthor.
     */
    @JsonProperty("email")
    @Getter @Setter private String email;
}
