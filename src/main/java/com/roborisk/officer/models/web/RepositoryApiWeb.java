package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * RepositoryApiWeb represents the object used in to communicate
 * repository settings through the /settings/ endpoint.
 */
public class RepositoryApiWeb extends AbstractRepositoryWeb {

    /**
     * gitLabId stores the id that GitLab assigned to the repository.
     */
    @JsonProperty("id")
    @Getter
    @Setter
    private Integer id;

    /**
     * Empty constructor for {@link RepositoryApiWeb}.
     */
    public RepositoryApiWeb() {
        super();
    }
}
