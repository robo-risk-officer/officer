package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Note implements the Git note object model for GitLab webhooks.
 */
public class NoteWeb {

    /**
     * gitLabId stores the GitLab id of the Note.
     */
    @JsonProperty("id")
    @Getter @Setter private Integer gitLabId;

    /**
     * repositoryId stores the id of the project on which the Note was placed.
     */
    @JsonProperty("project_id")
    @Getter @Setter private Integer repositoryId;

    /**
     * noteableId stores the id of the object on which the note is placed.
     */
    @JsonProperty("noteable_id")
    @Getter @Setter private Integer noteableId;

    /**
     * notableType stores the type of the object on which the note is placed.
     */
    @JsonProperty("noteable_type")
    @Getter @Setter private String noteableType;

    /**
     * note stores the string that was placed as note.
     */
    @JsonProperty("note")
    @Getter @Setter private String note;
}
