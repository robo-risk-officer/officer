package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 * Abstraction used to represent settings of Officer on the API level.
 * All settings are stored in the database, so id is for all of them.
 */
@Component
public abstract class AbstractSettingsWeb {

    /**
     * Unique Identifier for a database record.
     */
    @JsonProperty("id")
    @Getter @Setter protected Integer id;
}
