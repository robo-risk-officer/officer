package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Implements web model for issues gotten back via the getUnresolvedIssues function from the
 * {@link com.roborisk.officer.modules.metrics.sonar.SonarApiWrapper SonarApiWrapper}.
 */
public class SonarIssueWeb {

    /**
     * component stores the name of the SonarQube project, the complete file path and the file
     * name. This is in the format {project_name}:{path_and_file_name}.
     */
    @JsonProperty("component")
    @Getter @Setter private String component;

    /**
     * The line number of the issue.
     */
    @JsonProperty("line")
    @Getter @Setter private Integer line;

    /**
     * The message of the issue.
     */
    @JsonProperty("message")
    @Getter @Setter private String message;
}
