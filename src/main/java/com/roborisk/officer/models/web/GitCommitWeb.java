package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.util.List;

/**
 * GitCommitWeb implements the Git commit object model for GitLab webhooks.
 */
public class GitCommitWeb {

    /**
     * hash stores the id that GitLab assigned to the commit.
     */
    @JsonProperty("id")
    @Getter @Setter private String hash;

    /**
     * message stores the message that was supplied with the commit.
     */
    @JsonProperty("message")
    @NotNull
    @Getter @Setter private String message;

    /**
     * modified stores a list of paths to files (strings) that were modified.
     */
    @JsonProperty("modified")
    @Getter @Setter private List<String> modified;

    /**
     * removed stores a list of paths to files (strings) that were removed.
     */
    @JsonProperty("removed")
    @Getter @Setter private List<String> removed;

    /**
     * added stores a list of paths to files (strings) that were added.
     */
    @JsonProperty("added")
    @Getter @Setter private List<String> added;

    /**
     * timestamp stores the timestamp at which the commit was committed.
     */
    @JsonProperty("timestamp")
    @Getter private Integer timestamp;

    /**
     * author stores the GitCommitAuthor that committed the commit.
     */
    @JsonProperty("author")
    @Getter @Setter private GitCommitAuthor author;

    /**
     * Setter the timestamp.
     *
     * @param date  The date string.
     * @throws      ParseException When the date string does not represent a valid formatted date.
     */
    public void setTimestamp(String date) throws ParseException {
        this.timestamp = GitLabApiWrapper.getEpochFromString(date);
    }
}
