package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Represents the json object send by GitLab after sending a request to /oauth/info/token.
 * This is used to verify GitLab authentication tokens.
 * Only the fields relevant to the RRO are added in this class.
 */
public class GitLabTokenData {

    /**
     * Contains the gitLab user id of the owner of this token.
     */
    @JsonProperty("resource_owner_id")
    @Getter @Setter private int resourceOwnerId;
}
