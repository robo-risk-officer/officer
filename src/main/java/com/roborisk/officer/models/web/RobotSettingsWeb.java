package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 * RobotSettingsWeb represents the object used in to communicate robot settings through the
 * /settings/ endpoint.
 */
@Component
public class RobotSettingsWeb extends AbstractSettingsWeb {

    /**
     * The value representing of the MR needs to be automatically reviewed.
     */
    @JsonProperty("automatic_review_on_new_mr")
    @Getter @Setter private Boolean automaticReviewOnNewMr;

    /**
     * The value representing if a new commit needs to be automatically reviewed.
     */
    @JsonProperty("automatic_review_on_commit")
    @Getter @Setter private Boolean automaticReviewOnNewCommit;

    /**
     * The threshold of false positive markings required to set a false positive flag.
     */
    @JsonProperty("false_positive_threshold")
    @Getter @Setter private Integer falsePositiveThreshold;

    /**
     * The value representing the amount of commits to take into account when a repository gets
     * added.
     */
    @JsonProperty("review_history_length")
    @Getter @Setter private Integer reviewHistoryLength;

    /**
     * Basic constructor.
     */
    public RobotSettingsWeb(){}
}
