package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.roborisk.officer.helpers.MergeRequestReviewHelper;
import com.roborisk.officer.models.enums.EventType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

/**
 * CommentWebhookData implements the comment webhook data model.
 * CommentWebhookData is the equivalent object of the relevant attributes of the request body JSON
 * that GitLab sends when a comment is placed.
 */
@Component
// 16 instead of 15
@SuppressWarnings("ClassFanoutComplexity")
public class CommentWebhookData extends AbstractWebhookData {

    /**
     * Helper to check for whether a review has been given on the merge request in the same state.
     */
    @JsonIgnore
    private static MergeRequestReviewHelper MERGE_REQUEST_REVIEW_HELPER;

    /**
     * user stores the GitUser that made the comment.
     */
    @JsonProperty("user")
    @Getter @Setter private GitUser user;

    /**
     * repositoryId stores the project_id of the repository that the comment was placed on.
     */
    @JsonProperty("project_id")
    @Getter @Setter private Integer repositoryId;

    /**
     * mergeRequest stores the MergeRequestWeb on which the comment was placed.
     */
    @JsonProperty("merge_request")
    @Getter private MergeRequestWeb mergeRequest;

    /**
     * note stores the Note (actual comment), represented in object_attributes
     * of GitLab webhook request, which was placed.
     */
    @JsonProperty("object_attributes")
    @Getter @Setter private NoteWeb note;

    /**
     * The username of the RRO GitLab user.
     */
    @JsonIgnore
    public static String ROBOT_NAME;

    /**
     * Constructor for creating an empty {@link CommentWebhookData}.
     */
    public CommentWebhookData() {

    }

    /**
     * Constructor to create a {@link CommentWebhookData} from {@link MergeRequestWebhookData}.
     *
     * @param data  The merge request webhook information to copy over.
     * @param note  The note to be attached to this webhook.
     */
    public CommentWebhookData(MergeRequestWebhookData data, NoteWeb note) {
        this.mergeRequest = data.getMergeRequest();

        this.setType("note");
        this.setRepository(data.getRepository());
        this.setUser(data.getUser());
        this.setRepositoryId(data.getRepository().getGitLabId());
        this.setNote(note);
    }

    /**
     * Setter for mergeRequest variable.
     *
     * @param map               JSON webhook merge_request data.
     * @throws ParseException   When a timestamp in the map cannot be parsed.
     * @throws IOException      When the last_commit inside of the map cannot be parsed.
     */
    public void setMergeRequest(Map<String, Object> map) throws ParseException, IOException {
        this.mergeRequest = MergeRequestWeb.fromJson(map);
    }

    /**
     * Sets the static username of the RRO in {@link CommentWebhookData}.
     *
     * @param robotName The injected username from the config file.
     */
    @Autowired
    public void setRobotName(@Value("${officer.user_name:robo}") String robotName) {
        CommentWebhookData.ROBOT_NAME = robotName;
    }

    @Autowired
    public void setMergeRequestReviewHelper(MergeRequestReviewHelper mergeRequestReviewHelper) {
        CommentWebhookData.MERGE_REQUEST_REVIEW_HELPER = mergeRequestReviewHelper;
    }

    @Override
    public EventType getEventType() {
        String regex = "^@" + CommentWebhookData.ROBOT_NAME
                            + "\\s+review(\\s+(pls|plz|please))?\\s*.?\\s*$";

        if (this.note.getNote().matches(regex) && this.getMergeRequest() != null) {
            // Check if the merge request contains any modifications
            if (CommentWebhookData.MERGE_REQUEST_REVIEW_HELPER.mrWithoutCommits(
                    this.getMergeRequest(), this.repositoryId, this.getNote().getGitLabId())) {
                // No modifications were made. Thus, don't trigger a review
                return EventType.MERGE_REQUEST_CHANGED;
            }

            // Check if the merge request has already been reviewed in this state
            if (CommentWebhookData.MERGE_REQUEST_REVIEW_HELPER.reviewedMrOnSameState(
                    this.getMergeRequest(), this.getNote().getGitLabId())) {
                // It has already been reviewed, so don't trigger a review again
                return EventType.MERGE_REQUEST_CHANGED;
            }

            // It has not been reviewed yet, so trigger the review
            return EventType.REVIEW_REQUESTED;
        } else {
            return EventType.NONE;
        }
    }
}
