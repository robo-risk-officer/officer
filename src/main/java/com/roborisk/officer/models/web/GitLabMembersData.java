package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * This class is used to store the data that GitLab returns when asking for
 * member data of groups or projects.
 * Only the fields relevant to the RRO are added in this class.
 */
public class GitLabMembersData {

    /**
     * Stores the access level of a GitLab user in relation to a GitLab repository
     * or a GitLab group. The mapping if integers to roles can be found at:
     * https://docs.gitlab.com/ee/api/members.html.
     */
    @JsonProperty("access_level")
    @NotNull
    @Getter @Setter private int accessLevel;
}
