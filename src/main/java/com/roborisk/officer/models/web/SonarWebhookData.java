package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.Map;

public class SonarWebhookData {

    /**
     * Stores the key of the SonarQube project for which this webhook was fired.
     */
    @Getter
    private String projectKey;

    @JsonProperty("project")
    private void unpackKey(Map<String,Object> project) {
        this.projectKey = (String) project.get("key");
    }

}
