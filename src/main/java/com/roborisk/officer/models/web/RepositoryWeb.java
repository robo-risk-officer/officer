package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * RepositoryWeb represents the object used in to communicate
 * repository settings from the Web Hooks.
 */
public class RepositoryWeb extends AbstractRepositoryWeb {

    /**
     * gitLabId stores the id that GitLab assigned to the repository.
     */
    @JsonProperty("id")
    @Getter @Setter private Integer gitLabId;

    /**
     * Empty constructor for {@link RepositoryWeb}.
     */
    public RepositoryWeb() {
        super();
    }
}
