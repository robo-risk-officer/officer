package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * GitUser implements the Git user object model for GitLab webhooks.
 */
public class GitUser {

    /**
     * gitLabId stores the GitLab id of the GitUser.
     */
    @JsonProperty("id")
    @Getter @Setter private Integer gitLabId;

    /**
     * name stores the name of the GitUser.
     */
    @JsonProperty("name")
    @Getter @Setter private String name;

    /**
     * username stores the username of the GitUser.
     */
    @JsonProperty("username")
    @Getter @Setter private String username;
}
