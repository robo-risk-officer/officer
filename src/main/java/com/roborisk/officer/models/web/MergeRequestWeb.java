package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.roborisk.officer.helpers.JsonConverter;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * MergeRequestWeb implements the merge request object model for GitLab webhooks.
 */
public class MergeRequestWeb {

    /**
     * gitLabId stores the (all merge request tables) unique GitLab merge request id.
     */
    @Getter @Setter private Integer gitLabId;

    /**
     * gitLabIid stores the (project scope) unique GitLab merge request iid.
     */
    @Getter @Setter private Integer gitLabIid;

    /**
     * target stores the name of the target branch of the merge request.
     */
    @Getter @Setter private String target;

    /**
     * source stores the name of the source branch of the merge request.
     */
    @Getter @Setter private String source;

    /**
     * authorId stores the GitLab id of the author of the merge request.
     */
    @Getter @Setter private Integer authorId;

    /**
     * titles stores the title of the merge request.
     */
    @Getter @Setter private String title;

    /**
     * createdAt stores the date at which the merge request was created (in GitLab).
     */
    @JsonProperty
    @Getter private Integer createdAt;

    /**
     * updatedAt stores the date at which the merge request was last updated (in GitLab).
     */
    @JsonProperty
    @Getter private Integer updatedAt;

    /**
     * state stores the current state of the merge request.
     */
    @Getter @Setter private String state;

    /**
     * mergeStatus stores the mergeability of the merge request.
     */
    @Getter @Setter private String mergeStatus;

    /**
     * action stores the action performed on the merge request e.g. open/close.
     */
    @Getter @Setter private String action;

    /**
     * lastCommit stores the last GitCommitWeb in the merge request.
     */
    @Getter @Setter private GitCommitWeb lastCommit;

    /**
     * wip stores the work_in_progress boolean of the merge request.
     */
    @Getter @Setter private Boolean wip;

    /**
     * Constructs an empty MergeRequestWeb object.
     * This constructor is necessary for jackson to work properly.
     */
    public MergeRequestWeb() { }

    /**
     * Setter for createdAt variable.
     * Sets createdAt to the current date time if date cannot be parsed.
     *
     * @param date              Date value.
     * @throws ParseException   When the given date cannot be parsed to the given dateFormat.
     */
    @JsonIgnore
    public void setCreatedAt(String date) throws ParseException {
        this.createdAt = GitLabApiWrapper.getEpochFromString(date);
    }

    /**
     * Setter for createdAt variable.
     *
     * @param epoch The unix epoch to use.
     */
    @JsonIgnore
    public void setCreatedAt(Integer epoch) {
        this.createdAt = epoch;
    }

    /**
     * Setter for updatedAt variable.
     * Sets updatedAt to current date time if date cannot be parsed.
     *
     * @param date              Date value.
     * @throws ParseException   When the given date cannot be parsed to the given dateFormat.
     */
    @JsonIgnore
    public void setUpdatedAt(String date) throws ParseException {
        this.updatedAt = GitLabApiWrapper.getEpochFromString(date);
    }

    /**
     * Setter for updatedAt variable.
     *
     * @param epoch The unix epoch to use.
     */
    @JsonIgnore
    public void setUpdatedAt(Integer epoch) {
        this.updatedAt = epoch;
    }

    /**
     * Takes a json object and create the corresponding MergeRequest object.
     * This method is needed, over the standard Spring conversion, as the Branch
     * and last_commit models otherwise are not parsed correctly.
     *
     * @param map               JSON merge request object.
     * @throws IOException      When the last_commit inside of the map cannot be parsed.
     * @throws ParseException   When a timestamp in the map cannot be parsed.
     * @return                  MergeRequestWeb object of map.
     */
    public static MergeRequestWeb fromJson(
            Map<String, Object> map)
                throws IOException, ParseException {
        MergeRequestWeb mr = new MergeRequestWeb();

        mr.setGitLabId((Integer) map.get("id"));
        mr.setGitLabIid((Integer) map.get("iid"));

        // Setting target Branch attribute
        mr.setTarget((String) map.get("target_branch"));

        // Setting source Branch attribute
        mr.setSource((String) map.get("source_branch"));

        mr.setAuthorId((Integer) map.get("author_id"));
        mr.setTitle((String) map.get("title"));
        mr.setCreatedAt((String) map.get("created_at"));
        mr.setUpdatedAt((String) map.get("updated_at"));
        mr.setState((String) map.get("state"));
        mr.setAction((String) map.get("action"));
        mr.setMergeStatus((String) map.get("merge_status"));
        mr.setWip((Boolean) map.get("work_in_progress"));

        // Setting lastCommit attribute
        mr.setLastCommit(JsonConverter.convertFromMap(
                (LinkedHashMap) map.get("last_commit"), GitCommitWeb.class)
        );

        return mr;
    }

    /**
     * Outputs the object as a string.
     *
     * @return  A {@link String} with the object's relevant properties.
     */
    public String toString() {
        StringBuilder output = new StringBuilder();
        output.append("{").append(System.lineSeparator())
            .append("gitLabId: ").append(this.gitLabId).append(";").append(System.lineSeparator())
            .append("gitLabIid: ").append(this.gitLabIid).append(";").append(System.lineSeparator())
            .append("target: ").append(this.target).append(";").append(System.lineSeparator())
            .append("source: ").append(this.source).append(";").append(System.lineSeparator())
            .append("createdAt: ").append(this.createdAt).append(";").append(System.lineSeparator())
            .append("updatedAt: ").append(this.updatedAt).append(";").append(System.lineSeparator())
            .append("title: ").append(this.title).append(";").append(System.lineSeparator())
            .append("state: ").append(this.state).append(";").append(System.lineSeparator())
            .append("mergeStatus: ").append(this.mergeStatus)
            .append(";").append(System.lineSeparator());

        return output.toString();
    }

    /**
     * Returns true iff two MergeRequestWeb objects are similar.
     *
     * @param other     Another MergeRequestWeb object to compare {@code this} with.
     * @return          {@code true} iff {@code this} is equal to {@code other}.
     */
    public boolean isEqualToMergeRequestWeb(MergeRequestWeb other) {
        // The 4 properties [wip, authorId, action, lastCommit] are irrelevant
        if (!this.gitLabId.equals(other.getGitLabId())) {
            return false;
        }
        if (!this.gitLabIid.equals(other.getGitLabIid())) {
            return false;
        }
        if (!this.target.equals(other.getTarget())) {
            return false;
        }
        if (!this.source.equals(other.getSource())) {
            return false;
        }
        if (!this.createdAt.equals(other.getCreatedAt())) {
            return false;
        }
        if (!this.updatedAt.equals(other.getUpdatedAt())) {
            return false;
        }
        if (!this.title.equals(other.getTitle())) {
            return false;
        }
        if (!this.state.equals(other.getState())) {
            return false;
        }

        return this.mergeStatus.equals(other.getMergeStatus());
    }
}
