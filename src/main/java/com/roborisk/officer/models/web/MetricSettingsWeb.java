package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;


/**
 * The MetricSettings Model represents the "metric_settings" database table.
 */
@Component
public class MetricSettingsWeb extends AbstractSettingsWeb {

    /**
     * The threshold of minimal required artifact quality of the corresponding metric.
     */
    @JsonProperty("threshold")
    @Getter @Setter private Double threshold;

    /**
     * Whether the artifact should be above or below the threshold to meet the quality
     * standards.
     */
    @JsonProperty("threshold_is_upper_bound")
    @Getter @Setter private Boolean thresholdIsUpperBound;

    /**
     * The weight the corresponding metric contribute to the overall quality measure.
     */
    @JsonProperty("weight")
    @Getter @Setter private Double weight;

    /**
     * The displayName stores the name to be displayed in the frontend.
     */
    @JsonProperty("name")
    @Getter @Setter private String name;

    /**
     * Basic constructor.
     */
    public MetricSettingsWeb(){}
}
