package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Abstraction used to represent repositories inside Officer DB
 * on the API level.
 */
public abstract class AbstractRepositoryWeb {

    /**
     * name stores the name of the repository.
     */
    @JsonProperty("name")
    @Getter @Setter protected String name;

    /**
     * namespace stores the namespace of the repository.
     */
    @JsonProperty("namespace")
    @Getter @Setter protected String namespace;

    /**
     * pathWithNamespace stores the path containing the namespace (without spaces) and the name
     * of the repository.
     */
    @JsonProperty("path_with_namespace")
    @Getter @Setter protected String pathWithNamespace;

    /**
     * sshUrl stores the ssh URL of the repository.
     */
    @JsonProperty("git_ssh_url")
    @Getter @Setter protected String sshUrl;

    /**
     * httpUrl stores the http URL of the repository.
     */
    @JsonProperty("git_http_url")
    @Getter @Setter protected String httpUrl;

    /**
     * isEnabled stores whether the repository is enabled for tracking or not.
     */
    @JsonProperty("isEnabled")
    @Getter @Setter protected boolean isEnabled;
}
