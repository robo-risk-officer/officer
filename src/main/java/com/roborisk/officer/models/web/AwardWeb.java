package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import lombok.Getter;
import lombok.Setter;

import java.text.ParseException;

/**
 * AwardWeb is the model representing an award on GitLab.
 */
public class AwardWeb {

    /**
     * gitLabId stores the (unique) id of the award.
     */
    @JsonProperty("id")
    @Getter @Setter private Integer gitLabId;

    /**
     * name stores the name of the award.
     */
    @JsonProperty("name")
    @Getter @Setter private String name;

    /**
     * user stores the {@link GitUser} that posted the award.
     */
    @JsonProperty("user")
    @Getter @Setter private GitUser user;

    /**
     * createdAt stores when the award was placed.
     */
    @JsonProperty("created_at")
    @Getter private Integer createdAt;

    /**
     * updatedAt stores when the award was last updated.
     */
    @JsonProperty("updated_at")
    @Getter private Integer updatedAt;

    /**
     * gitLabAwardableId stores the GitLab id of the object the award was placed on.
     */
    @JsonProperty("awardable_id")
    @Getter @Setter private Integer gitLabAwardableId;

    /**
     * gitLabAwardableType stores the type of the GitLab object the award was placed on.
     */
    @JsonProperty("awardable_type")
    @Getter @Setter private String gitLabAwardableType;

    /**
     * Setter for createdAt attribute.
     *
     * @param date              Date value.
     * @throws ParseException   When the given date cannot be parsed to the given dateFormat.
     */
    public void setCreatedAt(String date) throws ParseException {
        this.createdAt = GitLabApiWrapper.getEpochFromString(date);
    }

    /**
     * Setter for updatedAt attribute.
     *
     * @param date              Date value.
     * @throws ParseException   When the given date cannot be parsed to the given dateFormat.
     */
    public void setUpdatedAt(String date) throws ParseException {
        this.updatedAt = GitLabApiWrapper.getEpochFromString(date);
    }
}
