package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.roborisk.officer.models.enums.EventType;
import lombok.Getter;
import lombok.Setter;

/**
 * The AbstractWebhookData abstracts the webhook data class used for equivalent
 * objects of GitLab webhook requests.
 */
public abstract class AbstractWebhookData {

    /**
     * repository stores the project attribute of GitLab webhook requests.
     * All variables that were called project* in GitLab webhook requests are called repository*.
     */
    @JsonProperty("project")
    @Getter @Setter private RepositoryWeb repository;

    /**
     * type stores the object_kind attribute of GitLab webhook requests.
     */
    @JsonProperty("object_kind")
    @Getter @Setter private String type;

    /**
     * Abstract function that returns the event type of the webhook.
     *
     * @return The EventType of the webhook.
     */
    public abstract EventType getEventType();
}
