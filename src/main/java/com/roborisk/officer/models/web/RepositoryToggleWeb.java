package com.roborisk.officer.models.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * JSONObject representing the response
 * by the settings API for path /settings/repository/{repositoryId}/toggle.
 */
public class RepositoryToggleWeb {

    /**
     * Whether the repository is enabled for the Officer to analyze.
     */
    @JsonProperty("enabled")
    @Getter @Setter private Boolean isEnabled;
}
