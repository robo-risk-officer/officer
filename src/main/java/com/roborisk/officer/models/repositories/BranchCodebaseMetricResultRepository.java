package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.metrics.BranchCodebaseMetricResult;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The Database Repository for the {@link BranchCodebaseMetricResult}.
 */
public interface BranchCodebaseMetricResultRepository
        extends CrudRepository<BranchCodebaseMetricResult, Integer> {

    /**
     * Find a {@link BranchCodebaseMetricResult} by ID.
     *
     * @param id    The ID of the {@link BranchCodebaseMetricResult}.
     * @return      An Optional {@link BranchCodebaseMetricResult} linked to ID or null when none
     *              exists.
     */
    Optional<BranchCodebaseMetricResult> findById(Integer id);

    /**
     * Find all {@link BranchCodebaseMetricResult} on a {@link Branch}.
     *
     * @param branch    The {@link Branch} to find the {@link BranchCodebaseMetricResult} objects
     *                  for.
     * @return          The list of {@link BranchCodebaseMetricResult} objects on {@link Branch}.
     */
    List<BranchCodebaseMetricResult> findAllByBranch(Branch branch);
}
