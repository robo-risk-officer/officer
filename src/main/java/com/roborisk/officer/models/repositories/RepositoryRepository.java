package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

/**
 * The Database Repository for the Repository.
 */
public interface RepositoryRepository extends PagingAndSortingRepository<Repository, Integer> {

    /**
     * Find a Repository by ID.
     *
     * @param id    The ID of the Repository.
     * @return      An Optional Repository linked to ID or null when none exists.
     */
    Optional<Repository> findById(Integer id);

    /**
     * Find all repositories.
     *
     * @return list of repositories.
     */
    List<Repository> findAll();

    /**
     * Finds a page pf repositories.
     *
     * @param pageable paging configuration
     * @return a page of repositories
     */
    Page<Repository> findAll(Pageable pageable);

    /**
     * Find a repository by GitLab ID.
     *
     * @param gitLabId  The GitLab ID of the repository.
     * @return          An Optional Repository linked to the GitLab ID.
     */
    Optional<Repository> findByGitLabId(Integer gitLabId);
}
