package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.MergeRequestClassObservation;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestReview;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The database repository for {@link MergeRequestClassObservation} objects.
 */
public interface MergeRequestClassObservationRepository
        extends CrudRepository<MergeRequestClassObservation, Integer> {

    /**
     * Find a {@link MergeRequestClassObservation} by ID.
     *
     * @param id    The ID of the {@link MergeRequestClassObservation}.
     * @return      An Optional {@link MergeRequestClassObservation} linked to ID or null when none
     *              exists.
     */
    Optional<MergeRequestClassObservation> findById(Integer id);

    /**
     * Find a {@link MergeRequestClassObservation} based on a {@link MergeRequestFile} and the
     * class name by which it is uniquely identified.
     *
     * @param file      The {@link MergeRequestFile} to identify the object in question.
     * @param className The {@link String} class name to identify the object in question.
     * @return          An {@link Optional} of the object in question.
     */
    Optional<MergeRequestClassObservation> findByMergeRequestFileAndClassName(
            MergeRequestFile file, String className);

    /**
     * Find all {@link MergeRequestClassObservation}s linked to a single {@link MergeRequestFile}.
     *
     * @param mergeRequestFile  The {@link MergeRequestFile} to identify the objects in question.
     * @return                  A {@link List} of the {@link MergeRequestClassObservation}s in
     *                          question.
     */
    List<MergeRequestClassObservation> findAllByMergeRequestFile(
            MergeRequestFile mergeRequestFile);

    /**
     * Find all {@link MergeRequestClassObservation}s based on a {@link MergeRequestReview} ID.
     *
     * @param id    ID of the {@link MergeRequestReview}
     * @return      A list of {@link MergeRequestClassObservation} matching the given ID.
     */
    List<MergeRequestClassObservation> findAllByMergeRequestFileMergeRequestReviewId(Integer id);
}
