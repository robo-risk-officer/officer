package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.MetricSettings;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The Database Repository for the {@link MetricSettings}.
 */
public interface MetricSettingsRepository extends CrudRepository<MetricSettings, Integer> {

    /**
     * Find a {@link MetricSettings} by ID.
     *
     * @param id    The ID of the {@link MetricSettings}
     * @return      An Optional {@link MetricSettings} object linked to ID or null when
     *              none exists.
     */
    Optional<MetricSettings> findById(Integer id);

    /**
     * Find a list of {@link MetricSettings} by repository ID.
     *
     * @param repositoryId      The repository ID to search for.
     * @return                  An Optional {@link MetricSettings} object linked
     *                          to repository ID or null when none exists.
     */
    Optional<List<MetricSettings>> findByRepositoryId(Integer repositoryId);

    /**
     * Find a {@link MetricSettings} based on a repository ID and metric ID.
     *
     * @param repositoryId  The repository ID to search for.
     * @param metricId      The metric ID to search for.
     * @return              An optional {@link MetricSettings} object linked to a repository ID and
     *                      metric ID.
     */
    Optional<MetricSettings> findByRepositoryIdAndMetricId(Integer repositoryId, Integer metricId);
}
