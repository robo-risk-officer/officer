package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.EventHistoryItem;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * The Database Repository for the EventHistoryItem.
 */
public interface EventHistoryItemRepository extends CrudRepository<EventHistoryItem, Integer> {

    /**
     * Find a EventHistoryItem by ID.
     *
     * @param id    The ID of the EventHistoryItem.
     * @return      An Optional EventHistoryItem linked to ID or null when none exists.
     */
    Optional<EventHistoryItem> findById(Integer id);

    /**
     * Find a EventHistoryItem by hash.
     *
     * @param hash  The hash of the EventHistoryItem.
     * @return      An Optional EventHistoryItem corresponding to the hash or null when none exists.
     */
    Optional<EventHistoryItem> findByHash(Long hash);

    /**
     * Store a new EventHistoryItem in the database.
     *
     * @param event     The EventHistoryItem to be stored.
     * @return          The stored EventHistoryItem.
     */
    EventHistoryItem save(EventHistoryItem event);
}
