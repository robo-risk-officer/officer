package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.metrics.MergeRequestCodebaseMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The Database Repository for the MrCodebaseMetricResult.
 */
public interface MergeRequestCodebaseMetricResultRepository
        extends CrudRepository<MergeRequestCodebaseMetricResult, Integer> {

    /**
     * Find a MrCodebaseMetricResult by ID.
     *
     * @param id    The ID of the MrCodebaseMetricResult.
     * @return      An Optional MrCodebaseMetricResult linked to ID or null when none exists.
     */
    Optional<MergeRequestCodebaseMetricResult> findById(Integer id);

    /**
     * Find all {@link MergeRequestCodebaseMetricResult}s which are associated to a single.
     * {@link com.roborisk.officer.models.database.MergeRequestReview}.
     * @param mergeRequestReviewId  The Id of the
     *                              {@link com.roborisk.officer.models.database.MergeRequestReview}
     *                              which identifies the objects in question.
     * @return                      A {@link List} of the
     *                              {@link MergeRequestCodebaseMetricResult}s in question.
     */
    List<MergeRequestCodebaseMetricResult>
            findAllByMergeRequestReviewId(int mergeRequestReviewId);

    /**
     * Check if there are {@link MergeRequestCodebaseMetricResult}s which have a score of null for a
     * given {@link MergeRequestReview}. This would imply that there are still {@link Metric}s of
     * type CODEBASE associated to the review which are calculating.
     *
     * @param mergeRequestReview    The {@link MergeRequestReview} in question.
     * @return                      Whether there are {@link MergeRequestCodebaseMetricResult} with
     *                              score of null.
     */
    boolean existsByMergeRequestReviewAndScoreIsNull(MergeRequestReview mergeRequestReview);
}
