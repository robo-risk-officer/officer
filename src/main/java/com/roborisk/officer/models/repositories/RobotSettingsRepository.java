package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.RobotSettings;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * The Database Repository for the RobotSettings.
 */
public interface RobotSettingsRepository extends CrudRepository<RobotSettings, Integer> {

    /**
     * Find a RobotSettings by ID.
     *
     * @param id    The ID of the RobotSettings
     * @return      An Optional RobotSettings linked to ID or null when none exists.
     */
    Optional<RobotSettings> findById(Integer id);

    /**
     * Find a {@link RobotSettings} by repositoryId.
     *
     * @param repositoryId      The repositoryId to search for.
     * @return                  An Optional RobotSettings linked
     *                              to repositoryId or null when none exists.
     */
    Optional<RobotSettings> findByRepositoryId(Integer repositoryId);

    /**
     * Returns the default Settings.
     *
     * @return An optional containing the default RobotSettings.
     */
    @Query(nativeQuery = true, value = "SELECT * FROM robot_settings WHERE repository_id is null")
    Optional<RobotSettings> getDefault();
}
