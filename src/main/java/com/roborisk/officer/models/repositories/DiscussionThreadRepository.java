package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.DiscussionThread;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * The database repository for {@link DiscussionThread} obects.
 */
public interface DiscussionThreadRepository extends CrudRepository<DiscussionThread, Integer> {

    /**
     * Find a {@link DiscussionThread} by ID.
     *
     * @param id    The ID of the {@link DiscussionThread}.
     * @return      An Optional {@link DiscussionThread} linked to ID or null when none exists.
     */
    Optional<DiscussionThread> findById(Integer id);

    /**
     * Find a {@link DiscussionThread} by GitLab id.
     *
     * @param gitLabId  The GitLab ID of the {@link DiscussionThread}.
     * @return          An Optional {@link DiscussionThread} linked to GitLab ID or null when none
     *                      exists.
     */
    Optional<DiscussionThread> findByGitLabId(String gitLabId);
}
