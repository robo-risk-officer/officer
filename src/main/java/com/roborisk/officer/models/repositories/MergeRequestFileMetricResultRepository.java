package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.metrics.MergeRequestFileMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * The Database Repository for the MergeRequestFileMetricResult.
 */
public interface MergeRequestFileMetricResultRepository
        extends CrudRepository<MergeRequestFileMetricResult, Integer> {

    /**
     * Find a {@link MergeRequestFileMetricResult} by ID.
     *
     * @param id    The ID of the {@link MergeRequestFileMetricResult}.
     * @return      An Optional {@link MergeRequestFileMetricResult}
     *                  linked to ID or null when none exists.
     */
    Optional<MergeRequestFileMetricResult> findById(Integer id);

    /**
     * Find a {@link MergeRequestFileMetricResult} by file path, {@link Metric} id and
     * {@link MergeRequestReview} id.
     *
     * @param filePath                  The filepath of the {@link MergeRequestFile}.
     * @param mergeRequestReviewId      The id of the {@link MergeRequestReview}.
     * @param metricId                  The id of the {@link Metric}.
     * @return                          An Optional {@link MergeRequestFileMetricResult} linked to
     *                                  the filePath, the metricId and the mergeRequestReviewId.
     */
    @Query(value = "SELECT result FROM mr_file_metric_result result where "
            + "result.mergeRequestFile.filePath like ?1 and "
            + "result.mergeRequestFile.mergeRequestReview.id = ?2 and "
            + "result.metric.id = ?3")
    Optional<MergeRequestFileMetricResult> getByFilePathAndMergeRequestReviewAndMetricId(
            String filePath, Integer mergeRequestReviewId, Integer metricId);

    /**
     * Find all {@link MergeRequestFileMetricResult}s which are associated to a single
     * {@link MergeRequestFile}.
     *
     * @param mergeRequestFile  The {@link MergeRequestFile} identifying the objects in question.
     * @return                  A {@link List} of all the {@link MergeRequestFileMetricResult}s
     *                          in question.
     */
    List<MergeRequestFileMetricResult> findAllByMergeRequestFile(MergeRequestFile mergeRequestFile);

    /**
     * Find all {@link MergeRequestFileMetricResult} with a score of null
     * that are linked to a review and metric.
     *
     * @param reviewId  The merge request review ID to filter on.
     * @param metricId  The metric ID to filter on.
     * @return          A list of {@link MergeRequestFileMetricResult} with a null score.
     */
    @Query(value = "SELECT mrfmr FROM mr_file_metric_result mrfmr WHERE "
                    + " mrfmr.metric.id = :metric_id AND"
                    + " mrfmr.mergeRequestFile.mergeRequestReview.id = :review_id AND "
                    + "mrfmr.score is null"
    )
    List<MergeRequestFileMetricResult> findAllNullScoreLinkedToReviewAndMetric(
            @Param("review_id") Integer reviewId, @Param("metric_id") Integer metricId
    );

    /**
     * Check if there are {@link MergeRequestFileMetricResult}s which have a score of null for a
     * given {@link MergeRequestReview}. This would imply that there are still {@link Metric}s of
     * type FILE associated to the review which are calculating.
     *
     * @param mergeRequestReview    The {@link MergeRequestReview} in question.
     * @return                      Whether there are {@link MergeRequestFileMetricResult} with
     *                              score of null.
     */
    boolean existsByMergeRequestFileMergeRequestReviewAndScoreIsNull(
            MergeRequestReview mergeRequestReview);
}
