package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.Branch;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * The Database Repository for the Branch.
 */
public interface BranchRepository extends CrudRepository<Branch, Integer> {

    /**
     * Find a Branch by ID.
     *
     * @param id    The ID of the Branch.
     * @return      An Optional Branch linked to ID or null when none exists.
     */
    Optional<Branch> findById(Integer id);

    /**
     * Find a branch by name & repository ID.
     *
     * @param name          The name of the branch.
     * @param repositoryId  The repository ID.
     * @return              An Optional Branch or null when none exists with the parameters.
     */
    Optional<Branch> findByNameAndRepositoryId(String name, Integer repositoryId);

    /**
     * Checks whether a branch already exists given a name and repository ID.
     *
     * @param name          The name of the branch.
     * @param repositoryId  The repository ID.
     * @return              Whether the branch exists.
     */
    boolean existsByNameAndRepositoryId(String name, Integer repositoryId);
}
