package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.BranchClassObservation;
import com.roborisk.officer.models.database.BranchFile;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The database repository for {@link BranchClassObservation} objects.
 */
public interface BranchClassObservationRepository
        extends CrudRepository<BranchClassObservation, Integer> {

    /**
     * Find a {@link BranchClassObservation} by ID.
     *
     * @param id    The ID of the {@link BranchClassObservation}.
     * @return      An Optional {@link BranchClassObservation} linked to ID or null when none
     *              exists.
     */
    Optional<BranchClassObservation> findById(Integer id);

    /**
     * Finds the first {@link BranchClassObservation} by branch file ID and class name then orders
     * it on ID. This ensures that the last added {@link BranchClassObservation} based on that
     * branch file and class name is returned.
     *
     * @param branchFileId  The branch file ID to search for.
     * @param className     The class name to search for.
     * @return              A optional {@link BranchClassObservation} linked to parameters given.
     */
    Optional<BranchClassObservation> findFirstByBranchFileIdAndClassNameOrderByIdDesc(
            Integer branchFileId, String className);

    /**
     * Finds all {@link BranchClassObservation} objects for a {@link BranchFile}.
     *
     * @param branchFile    The {@link BranchFile} to search for.
     * @return              List of all {@link BranchClassObservation} objects linked to the
     *                      {@link BranchFile} given.
     */
    List<BranchClassObservation> findAllByBranchFile(BranchFile branchFile);
}
