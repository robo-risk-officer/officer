package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.MergeRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

/**
 * The Database Repository for the {@link MergeRequest}.
 */
public interface MergeRequestRepository extends PagingAndSortingRepository<MergeRequest, Integer> {

    /**
     * Find a {@link MergeRequest} by ID.
     *
     * @param id    The ID of the {@link MergeRequest}.
     * @return      An Optional {@link MergeRequest} linked to ID or null when none exists.
     */
    Optional<MergeRequest> findById(Integer id);

    /**
     * Find a {@link MergeRequest} by its GitLab ID.
     *
     * @param gitLabId  The GitLab ID of the {@link MergeRequest}.
     * @return          An Optional {@link MergeRequest} linked to the GitLab ID
     *                          or null when none exists.
     */
    Optional<MergeRequest> findByGitLabId(Integer gitLabId);

    /**
     * Returns all instances of the MergeRequest type.
     *
     * @return All instances of type MergeRequest as a list
     */
    @Override
    List<MergeRequest> findAll();

    /**
     * Returns a Page of entities meeting the paging restriction provided in the Pageable object.
     * A page is a sublist of a list of objects. It allows to gain information about the position
     * of it in the containing entire list.
     *
     * @param pageable  Abstract interface for pagination information.
     * @return          A Page of MergeRequest entities meeting the argument restrictions.
     */
    @Override
    Page<MergeRequest> findAll(Pageable pageable);

    /**
     * Returns a list of {@link MergeRequest}s that have a certain state.
     *
     * @param state     The state to be searched on.
     * @return          A {@link List} of {@link MergeRequest}s having the specified state.
     */
    List<MergeRequest> findAllByState(String state);
}
