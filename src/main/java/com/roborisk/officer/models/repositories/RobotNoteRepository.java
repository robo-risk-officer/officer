package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.DiscussionThread;
import com.roborisk.officer.models.database.RobotNote;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The Database Repository for the Note.
 */
public interface RobotNoteRepository extends CrudRepository<RobotNote, Integer> {

    /**
     * Find a {@link RobotNote} by ID.
     *
     * @param id    The ID of the {@link RobotNote}.
     * @return      An Optional {@link RobotNote} linked to ID or null when none exists.
     */
    Optional<RobotNote> findById(Integer id);

    /**
     * Find a {@link RobotNote} by its GitLab ID.
     *
     * @param gitLabId  The GitLab ID of the {@link RobotNote}.
     * @return          An Optional {@link RobotNote} linked to GitLab ID or null when none exists.
     */
    Optional<RobotNote> findByGitLabId(Integer gitLabId);

    /**
     * Find all {@link RobotNote}s linked to a {@link DiscussionThread}.
     *
     * @param discussionThreadId    The {@link DiscussionThread} ID to search for.
     * @return                      A list of {@link RobotNote}s linked to the
     *                              {@link DiscussionThread}.
     */
    List<RobotNote> findAllByDiscussionThreadId(Integer discussionThreadId);


    /**
     * Find oldest {@link RobotNote}s linked to a {@link DiscussionThread} (where it is assumed
     * the oldest {@link RobotNote} was put into the database first and thus has the lowest id).
     *
     * @param discussionThreadId    The {@link DiscussionThread} ID to search for.
     * @return                      An {@link Optional} containing the oldest {@link RobotNote}
     *                              linked to the {@link DiscussionThread}.
     */
    Optional<RobotNote> findFirstByDiscussionThreadIdOrderByIdAsc(Integer discussionThreadId);
}
