package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.FalsePositive;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * The Database Repository for the FalsePositive.
 */
public interface FalsePositiveRepository extends CrudRepository<FalsePositive, Integer> {

    /**
     * Find a FalsePositive by ID.
     *
     * @param id    The ID of the {@link FalsePositive}.
     * @return      An Optional {@link FalsePositive} linked to ID or null when none exists.
     */
    Optional<FalsePositive> findById(Integer id);
}
