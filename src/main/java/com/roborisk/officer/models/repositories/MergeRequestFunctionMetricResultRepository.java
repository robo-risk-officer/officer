package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.MergeRequestFunctionObservation;
import com.roborisk.officer.models.database.metrics.MergeRequestFunctionMetricResult;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The database repository for {@link MergeRequestFunctionMetricResult} objects.
 */
public interface MergeRequestFunctionMetricResultRepository
        extends CrudRepository<MergeRequestFunctionMetricResult, Integer> {

    /**
     * Find a {@link MergeRequestFunctionMetricResult} by ID.
     *
     * @param id    The ID of the {@link MergeRequestFunctionMetricResult}.
     * @return      An Optional {@link MergeRequestFunctionMetricResult} linked to ID or null when
     *              none exists.
     */
    Optional<MergeRequestFunctionMetricResult> findById(Integer id);

    /**
     * List all {@link MergeRequestFunctionMetricResult} that are associated to a single
     * {@link MergeRequestFunctionObservation}.
     *
     * @param mergeRequestFunctionObservation   The {@link MergeRequestFunctionObservation} to
     *                                          which the results should be associated.
     * @return                                  A {@link List} of all
     *                                          {@link MergeRequestFunctionMetricResult}s in
     *                                          question
     */
    List<MergeRequestFunctionMetricResult> findAllByMergeRequestFunctionObservation(
            MergeRequestFunctionObservation mergeRequestFunctionObservation);

    /**
     * Find all {@link MergeRequestFunctionMetricResult} with a score of null
     * that are linked to a review.
     *
     * @param reviewId  The merge request review ID to filter on.
     * @return          A list of {@link MergeRequestFunctionMetricResult} with a null score.
     */
    @Query(value = "SELECT mrfmr FROM mr_function_metric_result mrfmr WHERE "
            + " mrfmr.mergeRequestFunctionObservation.mergeRequestFile.mergeRequestReview.id = ?1"
            + " AND mrfmr.score is null"
    )
    List<MergeRequestFunctionMetricResult> findAllNullScoreLinkedToReview(Integer reviewId);
}
