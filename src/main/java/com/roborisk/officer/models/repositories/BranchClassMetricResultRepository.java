package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.BranchClassObservation;
import com.roborisk.officer.models.database.metrics.BranchClassMetricResult;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The database repository for {@link BranchClassMetricResult} objects.
 */
public interface BranchClassMetricResultRepository
        extends CrudRepository<BranchClassMetricResult, Integer> {

    /**
     * Find a {@link BranchClassMetricResult} by ID.
     *
     * @param id    The ID of the {@link BranchClassMetricResult}.
     * @return      An Optional {@link BranchClassMetricResult} linked to ID or null when none
     *              exists.
     */
    Optional<BranchClassMetricResult> findById(Integer id);

    /**
     * Finds the first {@link BranchClassMetricResult} by branch class observation ID and metric ID,
     * then orders it on ID. This ensures that the last added {@link BranchClassMetricResult} based
     * on that branch class observation and metric is returned.
     *
     * @param branchClassObservationId  The branch file ID to search for.
     * @param metricId                  The metric ID to search for.
     * @return                          A optional {@link BranchClassMetricResult} linked to
     *                                  parameters given.
     */
    Optional<BranchClassMetricResult> findFirstByBranchClassObservationIdAndMetricIdOrderByIdDesc(
            Integer branchClassObservationId, Integer metricId
    );

    /**
     * Find all {@link BranchClassMetricResult} objects that are linked to a
     * {@link BranchClassObservation}.
     *
     * @param branchClassObservation    The {@link BranchClassObservation} to search for.
     * @return                          The list of {@link BranchClassMetricResult} objects
     *                                  that are linked to the {@link BranchClassObservation}.
     */
    List<BranchClassMetricResult> findAllByBranchClassObservation(
            BranchClassObservation branchClassObservation);
}
