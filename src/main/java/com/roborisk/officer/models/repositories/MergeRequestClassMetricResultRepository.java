package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.MergeRequestClassObservation;
import com.roborisk.officer.models.database.metrics.MergeRequestClassMetricResult;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The database repository for {@link MergeRequestClassMetricResult} objects.
 */
public interface MergeRequestClassMetricResultRepository
        extends CrudRepository<MergeRequestClassMetricResult, Integer> {

    /**
     * Find a {@link MergeRequestClassMetricResult} by ID.
     *
     * @param id    The ID of the {@link MergeRequestClassMetricResult}.
     * @return      An Optional {@link MergeRequestClassMetricResult} linked to ID or null when none
     *              exists.
     */
    Optional<MergeRequestClassMetricResult> findById(Integer id);

    /**
     * Find all {@link MergeRequestClassMetricResult}s associated to a single
     * {@link MergeRequestClassObservation}.
     *
     * @param mergeRequestClassObservation  The {@link MergeRequestClassObservation} to identify
     *                                      the objects in question.
     * @return                              A {@link List} of the
     *                                      {@link MergeRequestClassMetricResult}s in question.
     */
    List<MergeRequestClassMetricResult> findAllByMergeRequestClassObservation(
            MergeRequestClassObservation mergeRequestClassObservation);

    /**
     * Find all {@link MergeRequestClassMetricResult} with a score of null
     * that are linked to a review.
     *
     * @param reviewId  The merge request review ID to filter on.
     * @return          A list of {@link MergeRequestClassMetricResult} with a null score.
     */
    @Query(value = "SELECT mrcmr FROM mr_class_metric_result mrcmr WHERE "
            + " mrcmr.mergeRequestClassObservation.mergeRequestFile.mergeRequestReview.id = ?1"
            + " AND mrcmr.score is null"
    )
    List<MergeRequestClassMetricResult> findAllNullScoreLinkedToReview(Integer reviewId);
}
