package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.metrics.Metric;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * The Database Repository for the Metric.
 */
public interface MetricRepository extends CrudRepository<Metric, Integer> {

    /**
     * Find a {@link Metric} by ID.
     *
     * @param id    The ID of the {@link Metric}.
     * @return      The Metric linked to ID or null when none exists.
     */
    Optional<Metric> findById(Integer id);

    /**
     * Find a {@link Metric} by name.
     *
     * @param name  The name of the {@link Metric}.
     * @return      The Metric linked to the name or null when none exists.
     */
    Optional<Metric> findByName(String name);
}
