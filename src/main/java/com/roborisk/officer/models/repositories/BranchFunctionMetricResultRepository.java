package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.BranchFunctionObservation;
import com.roborisk.officer.models.database.metrics.BranchFunctionMetricResult;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The database repository for {@link BranchFunctionMetricResult} objects.
 */
public interface BranchFunctionMetricResultRepository
        extends CrudRepository<BranchFunctionMetricResult, Integer> {

    /**
     * Find a {@link BranchFunctionMetricResult} by ID.
     *
     * @param id    The ID of the {@link BranchFunctionMetricResult}.
     * @return      An Optional {@link BranchFunctionMetricResult} linked to ID or null when none
     *              exists.
     */
    Optional<BranchFunctionMetricResult> findById(Integer id);

    /**
     * Finds the first {@link BranchFunctionMetricResult} by branch class observation ID and
     * metric ID, then orders it on ID. This ensures that the last added
     * {@link BranchFunctionMetricResult} based on that branch function observation and metric is
     * returned.
     *
     * @param branchFunctionObservationId   The branch file ID to search for.
     * @param metricId                      The metric ID to search for.
     * @return                              A optional {@link BranchFunctionMetricResult} linked to
     *                                      parameters given.
     */
    Optional<BranchFunctionMetricResult>
            findFirstByBranchFunctionObservationIdAndMetricIdOrderByIdDesc(
                    Integer branchFunctionObservationId, Integer metricId
    );

    /**
     * Find all {@link BranchFunctionMetricResult} objects that are linked to a
     * {@link BranchFunctionObservation}.
     *
     * @param branchFunctionObservation The {@link BranchFunctionObservation} to search for.
     * @return                          The list of {@link BranchFunctionMetricResult} objects
     *                                  that are linked to the {@link BranchFunctionObservation}.
     */
    List<BranchFunctionMetricResult> findAllByBranchFunctionObservation(
            BranchFunctionObservation branchFunctionObservation);
}
