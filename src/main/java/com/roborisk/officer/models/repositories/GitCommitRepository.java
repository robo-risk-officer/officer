package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.GitCommit;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * The Database Repository for the GitCommit.
 */
public interface GitCommitRepository extends CrudRepository<GitCommit, Integer> {

    /**
     * Find a {@link GitCommit} by hash.
     *
     * @param id    The ID of the GitCommit.
     * @return      An Optional GitCommit linked to ID or null when none exists.
     */
    Optional<GitCommit> findById(Integer id);

    /**
     * Find a {@link GitCommit} by hash.
     *
     * @param hash  The hash of the commit.
     * @return      An Optional {@link GitCommit} linked to the hash or null when none exists.
     */
    Optional<GitCommit> findByHash(String hash);

    /**
     * Checks whether a commit exists with the given hash.
     *
     * @param hash  The hash to check for.
     * @return      Whether a commit with the given hash exists.
     */
    boolean existsByHash(String hash);
}
