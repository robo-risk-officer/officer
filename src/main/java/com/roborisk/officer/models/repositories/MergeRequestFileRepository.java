package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestReview;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The Database Repository for the MergeRequestFile.
 */
public interface MergeRequestFileRepository extends CrudRepository<MergeRequestFile, Integer> {

    /**
     * Find a {@link MergeRequestFile} by ID.
     *
     * @param id    The ID of the {@link MergeRequestFile}.
     * @return      An Optional {@link MergeRequestFile} linked to ID or null when none exists.
     */
    Optional<MergeRequestFile> findById(Integer id);

    /**
     * Find a {@link MergeRequestFile} by its name, path and {@link MergeRequestReview}.
     *
     * @param fileName              The name of the {@link MergeRequestFile}.
     * @param filePath              The path of the {@link MergeRequestFile}.
     * @param mergeRequestReviewId  The ID of the {@link MergeRequestReview}.
     * @return                      An Optional {@link MergeRequestFile} linked to the fileName,
     *                                      filePath and MergeRequestReviewId or null when none
     *                                      exists.
     */
    Optional<MergeRequestFile> findByFileNameAndFilePathAndMergeRequestReviewId(String fileName,
            String filePath, Integer mergeRequestReviewId);

    /**
     * Find all {@link MergeRequestFile}s that are linked to a given {@link MergeRequestReview}.
     *
     * @param mergeRequestReviewId  The {@link MergeRequestReview} ID to search for.
     * @return                      A list of {@link MergeRequestFile}s linked to a
     *                              {@link MergeRequestReview}.
     */
    List<MergeRequestFile> findAllByMergeRequestReviewId(Integer mergeRequestReviewId);

    /**
     * Find a single {@link MergeRequestFile} based on the Id of the {@link MergeRequestReview}
     * and the path of the file which together uniquely identify a {@link MergeRequestFile}.
     *
     * @param mergeRequestReviewId  The id of the {@link MergeRequestReview} which identifies the
     *                              object.
     * @param filePath              The file path which identifies the object.
     * @return                      An {@link Optional} containing the {@link MergeRequestFile}
     *                              in question.
     */
    Optional<MergeRequestFile> findByMergeRequestReviewIdAndFilePath(Integer mergeRequestReviewId,
                                                                    String filePath);
}
