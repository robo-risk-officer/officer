package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.SonarAnalysisTracker;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The Database Repository for the {@link SonarAnalysisTracker} model.
 */
public interface SonarAnalysisTrackerRepository
        extends CrudRepository<SonarAnalysisTracker, Integer> {

    /**
     * Find a {@link SonarAnalysisTracker} by ID.
     *
     * @param id    The ID of the {@link SonarAnalysisTracker}.
     * @return      An Optional {@link SonarAnalysisTracker} linked to ID, or null when none exists.
     */
    Optional<SonarAnalysisTracker> findById(Integer id);

    /**
     * Find a {@link SonarAnalysisTracker} by its working directory.
     *
     * @param name  The name of a {@link SonarAnalysisTracker}.
     * @return      An Optional {@link SonarAnalysisTracker} linked to the working directory, or
     *              null when non exists.
     */
    Optional<SonarAnalysisTracker> findByName(String name);

    /**
     * Find all {@link SonarAnalysisTracker}s linked to a {@link MergeRequestReview}.
     *
     * @param mergeRequestReviewId  The id of the {@link MergeRequestReview}.
     * @return                      The list of {@link SonarAnalysisTracker}s.
     */
    List<SonarAnalysisTracker> findAllByMergeRequestReviewId(Integer mergeRequestReviewId);
}
