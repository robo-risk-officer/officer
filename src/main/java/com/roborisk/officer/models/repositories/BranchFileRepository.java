package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.BranchFile;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The Database Repository for the BranchFile.
 */
public interface BranchFileRepository extends CrudRepository<BranchFile, Integer> {

    /**
     * Find a BranchFile by ID.
     *
     * @param id    The ID of the {@link BranchFile}.
     * @return      An Optional {@link BranchFile}  linked to ID or null when none exists.
     */
    Optional<BranchFile> findById(Integer id);

    /**
     * Find a {@link BranchFile} based on a branch ID and file path.
     *
     * @param branchId  The branch ID to search for.
     * @param filePath  The file path to search for.
     * @return          An Optional {@link BranchFile} linked to the branch ID and file path.
     */
    Optional<BranchFile> findByBranchIdAndFilePath(Integer branchId, String filePath);

    /**
     * Find all {@link BranchFile} by a {@link Branch} ID.
     *
     * @param branchId  The {@link Branch} ID to search for.
     * @return          A list of {@link BranchFile}s linked to a {@link Branch}.
     */
    List<BranchFile> findAllByBranchId(Integer branchId);
}
