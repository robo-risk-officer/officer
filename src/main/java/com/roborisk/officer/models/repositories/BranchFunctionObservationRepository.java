package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.BranchFunctionObservation;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The Database Repository for {@link BranchFunctionObservation} objects.
 */
public interface BranchFunctionObservationRepository
        extends CrudRepository<BranchFunctionObservation, Integer> {

    /**
     * Find a {@link BranchFunctionObservation} by ID.
     *
     * @param id    The ID of the {@link BranchFunctionObservation}.
     * @return      An Optional {@link BranchFunctionObservation} linked to ID or null when none
     *              exists.
     */
    Optional<BranchFunctionObservation> findById(Integer id);

    /**
     * Finds the first {@link BranchFunctionObservation} by branch file ID and function name then
     * orders it on ID. This ensures that the last added {@link BranchFunctionObservation} based on
     * that branch file and function name is returned.
     *
     * @param branchFileId  The branch file ID to search for.
     * @param functionName  The function name to search for.
     * @return              A optional {@link BranchFunctionObservation} linked to parameters given.
     */
    Optional<BranchFunctionObservation> findFirstByBranchFileIdAndFunctionNameOrderByIdDesc(
            Integer branchFileId, String functionName);

    /**
     * Finds all {@link BranchFunctionObservation} objects for a {@link BranchFile}.
     *
     * @param branchFile    The {@link BranchFile} to search for.
     * @return              List of all {@link BranchFunctionObservation} objects linked to the
     *                      {@link BranchFile} given.
     */
    List<BranchFunctionObservation> findAllByBranchFile(BranchFile branchFile);
}
