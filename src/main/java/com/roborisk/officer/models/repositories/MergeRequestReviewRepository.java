package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestReview;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The Database Repository for the {@link MergeRequestReview}.
 */
public interface MergeRequestReviewRepository extends CrudRepository<MergeRequestReview, Integer> {

    /**
     * Find a {@link MergeRequestReview} by ID.
     *
     * @param id    The ID of the {@link MergeRequestReview}.
     * @return      An Optional {@link MergeRequestReview} linked to ID or null when none exists.
     */
    Optional<MergeRequestReview> findById(Integer id);

    /**
     * Find the latest {@link MergeRequestReview} corresponding to a {@link MergeRequest}.
     *
     * @param mergeRequest      The {@link MergeRequest} to search for.
     * @return                  An optional {@link MergeRequestReview} linked to parameters given.
     */
    Optional<MergeRequestReview> findFirstByMergeRequestOrderByIdDesc(MergeRequest mergeRequest);

    /**
     * Determine whether there exists a review on a merge request, with certain commit as latest.
     *
     * @param hash              Hash of the latest commit.
     * @param mergeRequest      {@link MergeRequest} which to look for a review on.
     * @return                  Whether there is a review done on {@code mergeRequest}
     *                          with latest commit id being {@code hash}.
     */
    Boolean existsByCommitHashAndMergeRequest(String hash, MergeRequest mergeRequest);

    /**
     * Find all {@link MergeRequestReview}s which are associated to a single {@link MergeRequest}.
     *
     * @param mergeRequest  The {@link MergeRequest} to identify the objects in question.
     * @return              A {@link List} of all the {@link MergeRequestReview}s in question.
     */
    List<MergeRequestReview> findAllByMergeRequest(MergeRequest mergeRequest);
}
