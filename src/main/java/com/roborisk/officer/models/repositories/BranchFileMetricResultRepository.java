package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.metrics.BranchFileMetricResult;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The Database Repository for the BranchFileMetricResult.
 */
public interface BranchFileMetricResultRepository
        extends CrudRepository<BranchFileMetricResult, Integer> {

    /**
     * Find a {@link BranchFileMetricResult} by ID.
     *
     * @param id    The ID of the {@link BranchFileMetricResult}.
     * @return      An Optional {@link BranchFileMetricResult} linked to ID or null when none
     *              exists.
     */
    Optional<BranchFileMetricResult> findById(Integer id);

    /**
     * Checks whether a {@link BranchFileMetricResult} exists for a given {@link BranchFile},
     * {@link com.roborisk.officer.models.database.metrics.Metric} and a
     * {@link com.roborisk.officer.models.database.GitCommit}.
     *
     * @param branchFileId  The ID of the {@link BranchFile} to search for.
     * @param metricId      The ID of the
     *                      {@link com.roborisk.officer.models.database.metrics.Metric} to search
     *                      for.
     * @param commitId      The ID of the {@link com.roborisk.officer.models.database.GitCommit} to
     *                      search for.
     * @return              Whether a {@link BranchFileMetricResult} exists for the given arguments.
     */
    boolean existsByBranchFileIdAndMetricIdAndLastChangeCommitId(
            Integer branchFileId, Integer metricId, Integer commitId
    );

    /**
     * Finds the first {@link BranchFileMetricResult} by branch file ID and metric ID
     * then orders it on ID. This ensures that the last added {@link BranchFileMetricResult}
     * based on that branch file and metric is returned.
     *
     * @param branchFileId  The branch file ID to search for.
     * @param metricId      The metric ID to search for.
     * @return              A optional {@link BranchFileMetricResult} linked to parameters given.
     */
    Optional<BranchFileMetricResult> findFirstByBranchFileIdAndMetricIdOrderByIdDesc(
            Integer branchFileId, Integer metricId
    );

    /**
     * Finds all {@link BranchFileMetricResult} objects for a specified {@link BranchFile}.
     *
     * @param branchFile    The {@link BranchFile} to find {@link BranchFileMetricResult} objects
     *                      for.
     * @return              The list of all {@link BranchFileMetricResult} objects related to the
     *                      {@link BranchFile}.
     */
    List<BranchFileMetricResult> findAllByBranchFile(BranchFile branchFile);
}
