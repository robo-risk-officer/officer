package com.roborisk.officer.models.repositories;

import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestFunctionObservation;
import com.roborisk.officer.models.database.MergeRequestReview;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * The database repository for {@link MergeRequestFunctionObservation} objects.
 */
public interface MergeRequestFunctionObservationRepository
        extends CrudRepository<MergeRequestFunctionObservation, Integer> {

    /**
     * Find a {@link MergeRequestFunctionObservation} by ID.
     *
     * @param id    The ID of the {@link MergeRequestFunctionObservation}.
     * @return      An Optional {@link MergeRequestFunctionObservation} linked to ID or null when
     *              none exists.
     */
    Optional<MergeRequestFunctionObservation> findById(Integer id);

    /**
     * Find a {@link MergeRequestFunctionObservation} by a {@link MergeRequestFile}, its name
     * and the class it is in. This triplet uniquely identifies a
     * {@link MergeRequestFunctionObservation}.
     *
     * @param file          The {@link MergeRequestFile} which identifies the
     *                      {@link MergeRequestFunctionObservation}.
     * @param className     A {@link String} className which identifies the
     *                      {@link MergeRequestFunctionObservation}.
     * @param functionName  A {@link String} functionName which identifies the
     *                      {@link MergeRequestFunctionObservation}.
     * @return              An {@link Optional} of the {@link MergeRequestFunctionObservation}.
     */
    Optional<MergeRequestFunctionObservation> findByMergeRequestFileAndClassNameAndFunctionName(
            MergeRequestFile file, String className, String functionName);

    /**
     * Find all {@link MergeRequestFunctionObservation} which are associated to a give
     * {@link MergeRequestFile}.
     *
     * @param mergeRequestFile  The {@link MergeRequestFile} to identify the
     *                          {@link MergeRequestFunctionObservation}s we are looking for.
     * @return                  A {@link List} of {@link MergeRequestFunctionObservation}.
     */
    List<MergeRequestFunctionObservation> findAllByMergeRequestFile(
            MergeRequestFile mergeRequestFile);

    /**
     * Find all {@link MergeRequestFunctionObservation}s based on a {@link MergeRequestReview} ID.
     *
     * @param id    ID of the {@link MergeRequestReview}
     * @return      A list of {@link MergeRequestFunctionObservation} matching the given ID.
     */
    List<MergeRequestFunctionObservation> findAllByMergeRequestFileMergeRequestReviewId(Integer id);
}
