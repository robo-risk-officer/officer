package com.roborisk.officer.filters;

import org.springframework.util.StreamUtils;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Wrapper for requests allowing for reading multiple times.
 * Extends the HttpServletRequestWrapper and supports getInputStream() and getReader() being called
 * multiple times.
 */
public class CachingRequestWrapper extends HttpServletRequestWrapper {

    /**
     * Local cached copy of the body of the request.
     */
    private final byte[] cachedBody;

    /**
     * Constructor for the CachingRequestWrapper.
     *
     * @param   request     request to be wrapped.
     * @throws  IOException if getInputStream throws IOException.
     */
    public CachingRequestWrapper(HttpServletRequest request) throws IOException {
        super(request);

        InputStream requestInputStream = request.getInputStream();

        this.cachedBody = StreamUtils.copyToByteArray(requestInputStream);
    }

    /**
     * Implementation of getInputStream() supporting repeated calling.
     *
     * @return CachingInputStream of the request's body.
     */
    @Override
    public ServletInputStream getInputStream() {
        return new CachingInputStream(this.cachedBody);
    }

    /**
     * Implementation of getReader() supporting repeated calling.
     *
     * @return BufferedReader of the request's body.
     */
    @Override
    public BufferedReader getReader() {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.cachedBody);

        return new BufferedReader(new InputStreamReader(byteArrayInputStream));
    }
}
