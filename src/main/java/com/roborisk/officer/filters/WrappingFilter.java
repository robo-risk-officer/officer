package com.roborisk.officer.filters;

import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Filter which wraps all incoming requests in the CachingRequestWrapper.
 */
@Component
public class WrappingFilter implements Filter {

    /**
     * Method for wrapping incoming requests in the CachingRequestWrapper.
     *
     * @param   servletRequest      request to be wrapped.
     * @param   servletResponse     corresponding response object.
     * @param   filterChain         chain of filters too pass the request forward.
     * @throws  IOException         thrown if CachingRequestWrapper throws IOException.
     * @throws  ServletException    thrown if request or response throws ServletException.
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        // Create CachingRequestWrapper from original ServletRequest
        CachingRequestWrapper wrappedRequest;
        wrappedRequest = new CachingRequestWrapper((HttpServletRequest) servletRequest);

        // Pass the wrapped request to the next filter on the filter chain.
        filterChain.doFilter(wrappedRequest, servletResponse);
    }
}
