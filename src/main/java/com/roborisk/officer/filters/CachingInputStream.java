package com.roborisk.officer.filters;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * InputStream replacement extending ServletInputStream.
 */
public class CachingInputStream extends ServletInputStream {

    /**
     * Local cache of input data.
     */
    private final InputStream cachedInputStream;

    /**
     * Constructor of CachingInputStream.
     *
     * @param data data to be cached and read.
     */
    public CachingInputStream(byte[] data) {
        this.cachedInputStream = new ByteArrayInputStream(data);
    }

    /**
     * Function to read the next byte of data from InputStream.
     *
     * @throws  IOException if cached InputStream throws IOException.
     * @return  Next byte of data from InputStream formatted as an integer.
     */
    @Override
    public int read() throws IOException {
        return this.cachedInputStream.read();
    }

    /**
     * Function to check whether InputStream has read all data.
     *
     * @return whether the number of remaining bytes to be read equals 0.
     */
    @Override
    public boolean isFinished() {
        try {
            return (this.cachedInputStream.available() == 0);
        } catch (IOException e) {
            // Return false if IOException thrown by InputStream.
            return false;
        }
    }

    /**
     * Function to check whether InputStream is ready for reading.
     *
     * @return true - data is set upon creation.
     */
    @Override
    public boolean isReady() {
        return true;
    }

    /**
     * Function overridden to provide valid implementation of ServletInputStream.
     * Not implemented, throws UnsupportedOperationException.
     *
     * @param   readListener                    ReadListener object to be set.
     * @throws  UnsupportedOperationException   to indicate the lack of implementation.
     */
    @Override
    public void setReadListener(ReadListener readListener) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("ReadListener is not implemented");
    }
}
