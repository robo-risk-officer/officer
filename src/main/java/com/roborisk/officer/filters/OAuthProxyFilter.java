package com.roborisk.officer.filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

/**
 * This class provides a filter for Zuul so that the OAuth requests that are forwarded to the GitLab
 * server can be filtered and restricted.
 */
public class OAuthProxyFilter extends ZuulFilter {

    /**
     * allowedEndpointList contains a list of all OAuth endpoints that should be allowed.
     * The list is populated during filter construction and should be in URI format,
     * e.g. /oauth/token/info.
     */
    private List<String> allowedEndpointList;

    /**
     * FILTER_TYPE_PRE defines the constant string should be sent to Zuul to create a pre filter.
     * A pre filter is executed before a request is forwarded to its defined endpoint.
     */
    public static final String FILTER_TYPE_PRE = "pre";

    /**
     * PRIORITY defines the constant integer sent to Zuul to indicate the priority of this filter.
     * The number 10 is a kind-of-arbitrary number (larger than 0 or 1) to indicate that this filter
     * should be run as one of the first ones, as it is authorisation related.
     */
    public static final int PRIORITY = 10;

    /**
     * Constructor for the OAuthProxyFilter class.
     * Adds allowed endpoints to be forwarded.
     */
    public OAuthProxyFilter() {
        // First run the ZuulFilter super constructor to be safe.
        super();

        // Then create and populate the allowedEndpointList
        this.allowedEndpointList = Arrays.asList("/oauth/token/info");
    }

    /**
     * This filter is used to restrict which OAuth requests are forwarded.
     * This means that a "pre" filter needs to be created.
     *
     * @return FILTER_TYPE_PRE to ensure the filter is executed before a request is forwarded.
     */
    @Override
    public String filterType() {
        return OAuthProxyFilter.FILTER_TYPE_PRE;
    }

    /**
     * Indicates the order in which the filter should be run relative to others.
     *
     * @return PRIORITY to indicate a preference to run this filter first, i.e. has high precedence.
     */
    @Override
    public int filterOrder() {
        return OAuthProxyFilter.PRIORITY;
    }

    /**
     * This filter is meant to restrict OAuth requests, so shouldFilter should be true for only
     * those requests.
     */
    @Override
    public boolean shouldFilter() {
        // Get the current request context
        RequestContext requestCtx = RequestContext.getCurrentContext();
        // Extract the request
        HttpServletRequest request = requestCtx.getRequest();
        // And return if its url contains oauth (indicating an OAuth request)
        return request.getRequestURL().toString().contains("oauth");
    }

    /**
     * Perform the filtering action for OAuth requests.
     *
     * @throws ZuulException    In case an exception occurs during filtering.
     * @return                  Can return an arbitrary artifact. Is ignored by Zuul.
     */
    @Override
    public Object run() throws ZuulException {
        // Get the current request context
        RequestContext requestCtx = RequestContext.getCurrentContext();
        // Extract the request
        HttpServletRequest request = requestCtx.getRequest();
        // Extract the request URI from the request
        String requestURI = request.getRequestURI();

        // Check that the requestURI is not forbidden
        // Everything not explicitly allowed is forbidden
        if (!this.allowedEndpointList.contains(requestURI)) {
            // The request is forbidden
            // Set the request not to be forwarded to the proxy destination
            requestCtx.setSendZuulResponse(false);

            // And throw a ZuulException with 401-Unauthorised status code
            // to indicate access to unauthorised endpoint
            throw new ZuulException("Unauthorised access to OAuth endpoint",
                    HttpStatus.UNAUTHORIZED.value(),
                    "Unauthorised access to OAuth endpoint " + requestURI);
        }

        // Return null, because something needs to be returned,
        // but the return value is ignored by Zuul.
        return null;
    }
}
