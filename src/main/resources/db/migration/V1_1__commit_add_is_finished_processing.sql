ALTER TABLE commit ADD COLUMN is_finished_processing BOOLEAN;
UPDATE commit SET is_finished_processing = 'false';
ALTER TABLE commit ALTER COLUMN is_finished_processing SET NOT NULL;
