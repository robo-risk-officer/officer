ALTER TABLE metric ADD COLUMN description text;
UPDATE metric SET description = '';
ALTER TABLE metric ALTER COLUMN description SET NOT NULL;
