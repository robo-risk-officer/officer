CREATE TYPE METRIC_TYPE_ENUM as enum('CODEBASE', 'FILE', 'CLASS', 'FUNCTION');
CREATE TYPE QUALITY_TYPE_ENUM as enum('RISK', 'COMPLEXITY', 'UNDERSTANDABILITY');


CREATE TABLE repository (
    id SERIAL NOT NULL,
    gitlab_id INT NOT NULL UNIQUE,
    name VARCHAR(255) NOT NULL,
    ssh_url VARCHAR(255) NOT NULL,
    http_url VARCHAR(255) NOT NULL,
    namespace VARCHAR(255) NOT NULL,
    is_enabled BOOLEAN NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE commit (
    id SERIAL NOT NULL,
    hash VARCHAR(255) NOT NULL,
    message VARCHAR(255) NOT NULL,
    timestamp INT NOT NULL,
    parent_commit_id INT,
    PRIMARY KEY (id),
    FOREIGN KEY (parent_commit_id) REFERENCES commit(id) ON DELETE SET NULL
);

CREATE TABLE metric (
    id SERIAL NOT NULL,
    name VARCHAR(255) NOT NULL,
    display_name VARCHAR(255) NOT NULL,
    quality_type QUALITY_TYPE_ENUM NOT NULL,
    metric_type METRIC_TYPE_ENUM NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE robot_settings (
    id SERIAL NOT NULL,
    automatic_review_on_new_mr BOOLEAN NOT NULL,
    automatic_review_on_new_commit BOOLEAN NOT NULL,
    false_positive_threshold INT NOT NULL,
    review_history_length INT NOT NULL,
    repository_id INT UNIQUE,
    PRIMARY KEY (id),
    FOREIGN KEY (repository_id) REFERENCES repository(id) ON DELETE CASCADE
);

CREATE TABLE metric_settings (
    id SERIAL NOT NULL,
    threshold FLOAT NOT NULL,
    threshold_is_upper_bound BOOLEAN NOT NULL,
    weight FLOAT NOT NULL,
    repository_id INT,
    metric_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (repository_id) REFERENCES repository(id) ON DELETE CASCADE,
    FOREIGN KEY (metric_id) REFERENCES metric(id) ON DELETE CASCADE
);

CREATE TABLE event (
    id SERIAL NOT NULL,
    hash BIGINT NOT NULL,
    timestamp BIGINT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE branch (
    id SERIAL NOT NULL,
    name VARCHAR(255) NOT NULL,
    repository_id INT NOT NULL,
    latest_commit_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (repository_id) REFERENCES repository(id) ON DELETE CASCADE,
    FOREIGN KEY (latest_commit_id) REFERENCES commit(id) ON DELETE RESTRICT
);

CREATE TABLE branch_file (
    id SERIAL NOT NULL,
    file_name VARCHAR(255) NOT NULL,
    file_path VARCHAR(255) NOT NULL,
    is_deleted BOOLEAN NOT NULL,
    branch_id INT NOT NULL,
    last_change_commit_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (branch_id) REFERENCES branch(id) ON DELETE CASCADE,
    FOREIGN KEY (last_change_commit_id) REFERENCES commit(id) ON DELETE RESTRICT
);

CREATE TABLE merge_request (
    id SERIAL NOT NULL,
    gitlab_id INT NOT NULL UNIQUE,
    gitlab_iid INT NOT NULL,
    title VARCHAR(255) NOT NULL,
    created_at INT NOT NULL,
    updated_at INT NOT NULL,
    state VARCHAR(255) NOT NULL,
    merge_status VARCHAR(255) NOT NULL,
    target_branch_id INT NOT NULL,
    source_branch_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (target_branch_id) REFERENCES branch(id) ON DELETE CASCADE,
    FOREIGN KEY (source_branch_id) REFERENCES branch(id) ON DELETE CASCADE
);

CREATE TABLE branch_codebase_metric_result (
    id SERIAL NOT NULL,
    score FLOAT NOT NULL,
    is_low_quality BOOLEAN NOT NULL,
    branch_id INT NOT NULL,
    metric_id INT NOT NULL,
    last_change_commit_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (branch_id) REFERENCES branch(id) ON DELETE CASCADE,
    FOREIGN KEY (metric_id) REFERENCES metric(id) ON DELETE CASCADE,
    FOREIGN KEY (last_change_commit_id) REFERENCES commit(id) ON DELETE RESTRICT
);

CREATE TABLE branch_file_metric_result (
    id SERIAL NOT NULL,
    score FLOAT NOT NULL,
    is_low_quality BOOLEAN NOT NULL,
    branch_file_id INT NOT NULL,
    metric_id INT NOT NULL,
    last_change_commit_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (branch_file_id) REFERENCES branch_file(id) ON DELETE CASCADE,
    FOREIGN KEY (metric_id) REFERENCES metric(id) ON DELETE CASCADE,
    FOREIGN KEY (last_change_commit_id) REFERENCES commit(id) ON DELETE RESTRICT
);

CREATE TABLE branch_class_observation (
    id SERIAL NOT NULL,
    class_name VARCHAR(255) NOT NULL,
    line_number INT NOT NULL,
    branch_file_id INT NOT NULL,
    commit_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (branch_file_id) REFERENCES branch_file(id) ON DELETE CASCADE,
    FOREIGN KEY (commit_id) REFERENCES commit(id) ON DELETE CASCADE
);

CREATE TABLE branch_class_metric_result (
    id SERIAL NOT NULL,
    score FLOAT NOT NULL,
    is_low_quality BOOLEAN NOT NULL,
    branch_class_observation_id INT NOT NULL,
    metric_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (branch_class_observation_id) REFERENCES branch_class_observation(id) ON DELETE CASCADE,
    FOREIGN KEY (metric_id) REFERENCES metric(id) ON DELETE CASCADE
);

CREATE TABLE branch_function_observation (
    id SERIAL NOT NULL,
    function_name VARCHAR(255) NOT NULL,
    class_name VARCHAR(255) NOT NULL,
    line_number INT NOT NULL,
    branch_file_id INT NOT NULL,
    commit_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (branch_file_id) REFERENCES branch_file(id) ON DELETE CASCADE,
    FOREIGN KEY (commit_id) REFERENCES commit(id) ON DELETE CASCADE
);

CREATE TABLE branch_function_metric_result (
    id SERIAL NOT NULL,
    score FLOAT NOT NULL,
    is_low_quality BOOLEAN NOT NULL,
    branch_function_observation_id INT NOT NULL,
    metric_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (branch_function_observation_id) REFERENCES branch_function_observation(id) ON DELETE CASCADE,
    FOREIGN KEY (metric_id) REFERENCES metric(id) ON DELETE CASCADE
);

CREATE TABLE discussion_thread (
    id SERIAL NOT NULL,
    gitlab_id VARCHAR(255) NOT NULL UNIQUE,
    is_resolved BOOLEAN NOT NULL,
    merge_request_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (merge_request_id) REFERENCES merge_request(id) ON DELETE CASCADE
);

CREATE TABLE robot_note (
    id SERIAL NOT NULL,
    gitlab_id INT NOT NULL UNIQUE,
    discussion_thread_id INT,
    PRIMARY KEY (id),
    FOREIGN KEY (discussion_thread_id) REFERENCES discussion_thread(id) ON DELETE CASCADE
);

CREATE TABLE false_positive_marking (
    id SERIAL NOT NULL,
    user_id INT NOT NULL,
    gitlab_award_id INT NOT NULL UNIQUE,
    robot_note_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (robot_note_id) REFERENCES robot_note(id) ON DELETE CASCADE
);

CREATE TABLE merge_request_review (
    id SERIAL NOT NULL,
    merge_request_id INT NOT NULL,
    commit_id INT NOT NULL,
    discussion_thread_id INT,
    PRIMARY KEY (id),
    FOREIGN KEY (merge_request_id) REFERENCES merge_request(id) ON DELETE CASCADE,
    FOREIGN KEY (commit_id) REFERENCES commit(id) ON DELETE RESTRICT,
    FOREIGN KEY (discussion_thread_id) REFERENCES discussion_thread(id) ON DELETE CASCADE
);

CREATE TABLE mr_codebase_metric_result (
    id SERIAL NOT NULL,
    score FLOAT NOT NULL,
    is_low_quality BOOLEAN NOT NULL,
    merge_request_review_id INT NOT NULL,
    metric_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (merge_request_review_id) REFERENCES merge_request_review(id) ON DELETE CASCADE,
    FOREIGN KEY (metric_id) REFERENCES metric(id) ON DELETE CASCADE
);

CREATE TABLE merge_request_file (
    id SERIAL NOT NULL,
    file_name VARCHAR(255) NOT NULL,
    file_path VARCHAR(255) NOT NULL,
    merge_request_review_id INT NOT NULL,
    discussion_thread_id INT,
    PRIMARY KEY (id),
    FOREIGN KEY (merge_request_review_id) REFERENCES merge_request_review(id) ON DELETE CASCADE,
    FOREIGN KEY (discussion_thread_id) REFERENCES discussion_thread(id) ON DELETE CASCADE
);

CREATE TABLE mr_file_metric_result (
    id SERIAL NOT NULL,
    score FLOAT NOT NULL,
    is_low_quality BOOLEAN NOT NULL,
    merge_request_file_id INT NOT NULL,
    metric_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (merge_request_file_id) REFERENCES merge_request_file(id) ON DELETE CASCADE,
    FOREIGN KEY (metric_id) REFERENCES metric(id) ON DELETE CASCADE
);

CREATE TABLE mr_class_observation (
    id SERIAL NOT NULL,
    class_name VARCHAR(255) NOT NULL,
    line_number INT NOT NULL,
    is_false_positive BOOLEAN NOT NULL,
    is_suppressed BOOLEAN NOT NULL,
    merge_request_file_id INT NOT NULL,
    discussion_thread_id INT,
    PRIMARY KEY (id),
    FOREIGN KEY (merge_request_file_id) REFERENCES merge_request_file(id) ON DELETE CASCADE,
    FOREIGN KEY (discussion_thread_id) REFERENCES discussion_thread(id) ON DELETE CASCADE
);

CREATE TABLE mr_class_metric_result (
    id SERIAL NOT NULL,
    score FLOAT NOT NULL,
    is_low_quality BOOLEAN NOT NULL,
    mr_class_observation_id INT NOT NULL,
    metric_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (mr_class_observation_id) REFERENCES mr_class_observation(id) ON DELETE CASCADE,
    FOREIGN KEY (metric_id) REFERENCES metric(id) ON DELETE CASCADE
);

CREATE TABLE mr_function_observation (
    id SERIAL NOT NULL,
    function_name VARCHAR(255) NOT NULL,
    class_name VARCHAR(255) NOT NULL,
    line_number INT NOT NULL,
    is_false_positive BOOLEAN NOT NULL,
    is_suppressed BOOLEAN NOT NULL,
    merge_request_file_id INT NOT NULL,
    discussion_thread_id INT,
    PRIMARY KEY (id),
    FOREIGN KEY (merge_request_file_id) REFERENCES merge_request_file(id) ON DELETE CASCADE,
    FOREIGN KEY (discussion_thread_id) REFERENCES discussion_thread(id) ON DELETE CASCADE
);

CREATE TABLE mr_function_metric_result (
    id SERIAL NOT NULL,
    score FLOAT NOT NULL,
    is_low_quality BOOLEAN NOT NULL,
    mr_function_observation_id INT NOT NULL,
    metric_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (mr_function_observation_id) REFERENCES mr_function_observation(id) ON DELETE CASCADE,
    FOREIGN KEY (metric_id) REFERENCES metric(id) ON DELETE CASCADE
);

CREATE TABLE sonar_analysis_tracker (
    id SERIAL NOT NULL,
    working_directory VARCHAR(255) NOT NULL,
    is_completed BOOLEAN NOT NULL,
    is_source_branch BOOLEAN NOT NULL,
    merge_request_review_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (merge_request_review_id) REFERENCES merge_request_review(id) ON DELETE CASCADE
);

