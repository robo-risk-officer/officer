/*
    For each metric, default settings are defined here.

    Because this data can be modified by users, conflicts are ignored.
 */

-- FILE CODE CHURN
INSERT INTO metric_settings (threshold, threshold_is_upper_bound, weight, repository_id, metric_id)
SELECT 10, true, 1, null, metric.id
FROM metric
WHERE metric.name = 'file_code_churn'
ON CONFLICT DO NOTHING;

-- CLASS CODE CHURN
INSERT INTO metric_settings (threshold, threshold_is_upper_bound, weight, repository_id, metric_id)
SELECT 10, true, 1, null, metric.id
FROM metric
WHERE metric.name = 'class_code_churn'
ON CONFLICT DO NOTHING;

-- FUNCTION CODE CHURN
INSERT INTO metric_settings (threshold, threshold_is_upper_bound, weight, repository_id, metric_id)
SELECT 10, true, 1, null, metric.id
FROM metric
WHERE metric.name = 'function_code_churn'
ON CONFLICT DO NOTHING;

-- SONAR
INSERT INTO metric_settings (threshold, threshold_is_upper_bound, weight, repository_id, metric_id)
SELECT 0, true, 1, null, metric.id
FROM metric
WHERE metric.name = 'sonarqube_vulnerabilities'
ON CONFLICT DO NOTHING;
