ALTER TABLE repository ADD UNIQUE (ssh_url);
ALTER TABLE repository ADD UNIQUE (http_url);
ALTER TABLE commit ADD UNIQUE (hash);
ALTER TABLE metric ADD UNIQUE (name);
ALTER TABLE event ADD UNIQUE (hash);

ALTER TABLE branch ADD CONSTRAINT one_branch_per_name_per_repo UNIQUE (name, repository_id);
ALTER TABLE branch_file ADD CONSTRAINT one_branch_file_path_per_branch UNIQUE (file_path, branch_id);
ALTER TABLE merge_request ADD CONSTRAINT one_mr_iid_per_source UNIQUE (gitlab_iid, source_branch_id);
ALTER TABLE merge_request ADD CONSTRAINT one_mr_iid_per_target UNIQUE (gitlab_iid, target_branch_id);
ALTER TABLE branch_codebase_metric_result ADD CONSTRAINT one_branch_cb_metric_result_per_metric_per_branch_per_commit
        UNIQUE (metric_id, branch_id, last_change_commit_id);
ALTER TABLE branch_file_metric_result ADD CONSTRAINT one_branch_file_metric_result_per_metric_per_file_per_commit
        UNIQUE (metric_id, branch_file_id, last_change_commit_id);
ALTER TABLE branch_class_observation ADD CONSTRAINT one_branch_class_ob_per_name_per_file_per_commit
        UNIQUE (class_name, branch_file_id, commit_id);
ALTER TABLE branch_class_metric_result ADD CONSTRAINT one_branch_class_metric_result_per_ob_per_metric
        UNIQUE (branch_class_observation_id, metric_id);
ALTER TABLE branch_function_observation ADD CONSTRAINT one_branch_function_ob_per_name_per_class_per_file_per_commit
        UNIQUE (function_name, class_name, branch_file_id, commit_id);
ALTER TABLE branch_function_metric_result ADD CONSTRAINT one_branch_function_metric_result_per_ob_per_metric
        UNIQUE (branch_function_observation_id, metric_id);
ALTER TABLE merge_request_review ADD CONSTRAINT one_mr_review_per_mr_per_commit
        UNIQUE (merge_request_id, commit_id);
ALTER TABLE mr_codebase_metric_result ADD CONSTRAINT one_mr_cb_metric_result_per_review_per_metric
        UNIQUE (merge_request_review_id, metric_id);
ALTER TABLE merge_request_file ADD CONSTRAINT one_mr_file_path_per_review
        UNIQUE (file_path, merge_request_review_id);
ALTER TABLE mr_file_metric_result ADD CONSTRAINT one_mr_file_metric_result_per_file_per_metric
        UNIQUE (merge_request_file_id, metric_id);
ALTER TABLE mr_class_observation ADD CONSTRAINT one_mr_class_ob_name_per_file
        UNIQUE (class_name, merge_request_file_id);
ALTER TABLE mr_class_metric_result ADD CONSTRAINT one_mr_class_metric_result_per_class_per_metric
        UNIQUE (mr_class_observation_id, metric_id);
ALTER TABLE mr_function_observation ADD CONSTRAINT one_mr_function_ob_name_per_class_per_file
        UNIQUE (function_name, class_name, merge_request_file_id);
ALTER TABLE mr_function_metric_result ADD CONSTRAINT one_mr_function_metric_result_per_function_per_metric
        UNIQUE (mr_function_observation_id, metric_id);
ALTER TABLE metric_settings ADD CONSTRAINT one_metric_settings_per_repo_per_metric UNIQUE (repository_id, metric_id);
