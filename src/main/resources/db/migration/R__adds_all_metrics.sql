/*
    This file ensures that all metrics are added upon start-up.

    Metric info is updated
 */

-- FILE CODE CHURN
INSERT INTO metric (name, display_name, quality_type, metric_type, view_identifier, description)
VALUES ('file_code_churn', 'File Code Churn', 'RISK', 'FILE', 'CodeChurnView', 'Code is more prone to vulnerabilities if it changes often. This code churn metric checks how often a particular file is changed over time.')
ON CONFLICT (name) DO UPDATE
SET display_name = 'File Code Churn',
    description = 'Code is more prone to vulnerabilities if it changes often. This code churn metric checks how often a particular file is changed over time.',
    quality_type = 'RISK',
    metric_type = 'FILE',
    view_identifier = 'CodeChurnView';

-- CLASS CODE CHURN
INSERT INTO metric (name, display_name, quality_type, metric_type, view_identifier, description)
VALUES ('class_code_churn', 'Class Code Churn', 'RISK', 'CLASS', 'CodeChurnView', 'Code is more prone to vulnerabilities if it changes often. This code churn metric checks how often a particular class is changed over time.')
ON CONFLICT (name) DO UPDATE
    SET display_name = 'Class Code Churn',
    	description = 'Code is more prone to vulnerabilities if it changes often. This code churn metric checks how often a particular class is changed over time.',
        quality_type = 'RISK',
        metric_type = 'CLASS',
        view_identifier = 'CodeChurnView';

-- FUNCTION CODE CHURN
INSERT INTO metric (name, display_name, quality_type, metric_type, view_identifier, description)
VALUES ('function_code_churn', 'Function Code Churn', 'RISK', 'FUNCTION', 'CodeChurnView', 'Code is more prone to vulnerabilities if it changes often. This code churn metric checks how often a particular function is changed over time.')
ON CONFLICT (name) DO UPDATE
    SET display_name = 'Function Code Churn',
    	description = 'Code is more prone to vulnerabilities if it changes often. This code churn metric checks how often a particular function is changed over time.',
        quality_type = 'RISK',
        metric_type = 'FUNCTION',
        view_identifier = 'CodeChurnView';

-- SONAR
INSERT INTO metric (name, display_name, quality_type, metric_type, view_identifier, description)
VALUES ('sonarqube_vulnerabilities', 'SonarQube Vulnerabilities', 'RISK', 'FILE', 'SonarVulnerabilitiesView', 'This metric shows you all issues in a particular file that the community edition of SonarQube flags as `VULNERABILITY`')
ON CONFLICT (name) DO UPDATE
SET display_name = 'SonarQube Vulnerabilities',
    description = 'This metric shows you all issues in a particular file that the community edition of SonarQube flags as `VULNERABILITY`',
    quality_type = 'RISK',
    metric_type = 'FILE',
    view_identifier = 'SonarVulnerabilitiesView';

