ALTER TABLE branch_file ALTER file_name SET DATA TYPE text;
ALTER TABLE branch_file ALTER file_path SET DATA TYPE text;
ALTER TABLE branch_class_observation ALTER class_name SET DATA TYPE text;
ALTER TABLE branch_function_observation ALTER class_name SET DATA TYPE text;
ALTER TABLE branch_function_observation ALTER function_name SET DATA TYPE text;
ALTER TABLE merge_request_file ALTER file_name SET DATA TYPE text;
ALTER TABLE merge_request_file ALTER file_path SET DATA TYPE text;
ALTER TABLE mr_class_observation ALTER class_name SET DATA TYPE text;
ALTER TABLE mr_function_observation ALTER class_name SET DATA TYPE text;
ALTER TABLE mr_function_observation ALTER function_name SET DATA TYPE text;
ALTER TABLE sonar_analysis_tracker ALTER working_directory SET DATA TYPE text;
