ALTER TABLE mr_codebase_metric_result ALTER COLUMN score DROP NOT NULL;
ALTER TABLE mr_function_metric_result ALTER COLUMN score DROP NOT NULL;
ALTER TABLE mr_class_metric_result ALTER COLUMN score DROP NOT NULL;
ALTER TABLE mr_file_metric_result ALTER COLUMN score DROP NOT NULL;

ALTER TABLE metric ADD view_identifier varchar(255);
