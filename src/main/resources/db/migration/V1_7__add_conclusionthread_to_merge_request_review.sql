ALTER TABLE merge_request_review ADD COLUMN conclusion_thread_id INT;
ALTER TABLE merge_request_review ADD CONSTRAINT conclusion_discussion  FOREIGN KEY (conclusion_thread_id) REFERENCES discussion_thread(id) ON DELETE CASCADE;
