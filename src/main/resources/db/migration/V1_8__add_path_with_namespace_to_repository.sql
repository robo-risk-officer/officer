ALTER TABLE repository ADD COLUMN path_with_namespace text;
UPDATE repository SET path_with_namespace = '';
ALTER TABLE repository ALTER COLUMN path_with_namespace SET NOT NULL;
