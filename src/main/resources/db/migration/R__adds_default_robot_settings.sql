/*
    This file ensures that all default metric settings are added
    every time robo risk is started up.

    Because this data can be modified by users, conflicts are ignored.
 */

CREATE UNIQUE INDEX IF NOT EXISTS i_null ON robot_settings ((repository_id is NULL)) WHERE repository_id IS NULL;

INSERT INTO robot_settings (
    automatic_review_on_new_mr,
    automatic_review_on_new_commit,
    false_positive_threshold,
    review_history_length,
    repository_id
 )
VALUES (
    false,
    false,
    3,
    50,
    null
)
ON CONFLICT DO NOTHING;
