package com.roborisk.officer;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test the main entry class of ROBO Risk Officer.
 */
class OfficerApplicationTests extends OfficerTest {

    /**
     * Example test show how assertj works.
     */
    @Test
    void contextLoads() {
        assertThat("hello").isEqualTo("hello");
    }
}
