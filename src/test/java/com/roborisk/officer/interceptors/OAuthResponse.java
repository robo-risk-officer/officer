package com.roborisk.officer.interceptors;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * The JSON Object representing the OAuth response from a password grant.
 */
public class OAuthResponse {

    /**
     * access_token stores the access token of the OAuth mechanism.
     */
    @JsonProperty("access_token")
    @Getter @Setter private String accessToken;
}
