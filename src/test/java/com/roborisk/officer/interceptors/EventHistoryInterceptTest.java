package com.roborisk.officer.interceptors;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.repositories.EventHistoryItemRepository;
import com.roborisk.officer.models.repositories.RobotSettingsRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * This class contains tests for the Event History Intercept.
 */
@TestPropertySource(properties = {
    "officer.interceptors.event.enabled=true",
    "officer.event.enabled=false"
})
class EventHistoryInterceptTest extends OfficerTest {

    /**
     * Repository interfacing with the database to test against.
     */
    @Autowired
    private EventHistoryItemRepository repository;

    /**
     * Database repository to handle {@link RobotSettingsRepository} objects.
     */
    @Autowired
    private RobotSettingsRepository robotSettingsRepository;

    /**
     * Rest Template used for posting testing requests.
     */
    @Autowired
    private TestRestTemplate testRestTemplate;

    /**
     * Function for retrieving mock push event JSONObject.
     *
     * @throws JSONException    If JSONObject throws JSONException.
     * @return                  Mock JSONObject.
     */
    JSONObject getJSON() throws JSONException {
        // Create object
        JSONObject body = new JSONObject();

        // Add required fields
        body.put("before", "last commit");
        body.put("after", "next commit");
        body.put("checkout_sha", "some sha");
        body.put("user_id", 32447);
        body.put("project_id", 52);
        body.put("total_commits_count", 1);
        body.put("object_kind", "push");

        // Add project
        JSONObject project = new JSONObject();

        project.put("id", 52);
        project.put("name", "repository");
        project.put("namespace", "my namespace");
        project.put("git_ssh_url", "git@git");
        project.put("git_http_url", "git@git");

        body.put("project", project);

        // Add commit
        JSONObject commit = new JSONObject();

        commit.put("id", "next_commit");
        commit.put("message", "commit message");
        commit.put("timestamp", "2011-12-12T14:27:31+02:00");
        commit.put("added", new JSONArray());
        commit.put("removed", new JSONArray());
        commit.put("modified", new JSONArray());

        // Add author to commit
        JSONObject author = new JSONObject();

        author.put("name", "gitgud");
        author.put("email", "gitgud@tue.nl");

        commit.put("author", author);

        body.put("commits", new JSONArray(new JSONObject[]{commit}));

        // Return created object
        return body;
    }

    /**
     * Unit test for two new events being intercepted.
     */
    @Test
    void testNewEvent() throws Exception {
        // Create first request.
        JSONObject body = getJSON();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request1 = new HttpEntity<>(body.toString(), headers);

        // Construct URI string for requests.
        String uri = "http://localhost:" + port + "/webhook/commit";

        // Post first request.
        ResponseEntity<String> result = testRestTemplate.postForEntity(uri, request1, String.class);

        // Evaluate returned status code.
        assertEquals(200, result.getStatusCodeValue());

        // Evaluate number of events in the database.
        assertEquals(1, repository.count());

        // Create second request.
        body.put("after", "different_sha");

        HttpEntity<String> request2 = new HttpEntity<>(body.toString(), headers);

        // Post second request.
        result = testRestTemplate.postForEntity(uri, request2, String.class);

        // Evaluate returned status code.
        assertEquals(200, result.getStatusCodeValue());


        // Evaluate number of events in the database.
        assertEquals(2, repository.count());
    }

    /**
     * Unit test for the same event being intercepted twice.
     */
    @Test
    void testAlreadyInDatabase() throws Exception {
        // Construct URI string for requests.
        String uri = "http://localhost:" + port + "/webhook/commit";

        // Create request object.
        JSONObject body = getJSON();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request = new HttpEntity<>(body.toString(), headers);

        // Post request for the first time.
        ResponseEntity<String> result = testRestTemplate.postForEntity(uri, request, String.class);

        // Evaluate returned status code.
        assertEquals(200, result.getStatusCodeValue());

        // Evaluate number of events in the database.
        assertEquals(1, repository.count());

        // Post request again.
        result = testRestTemplate.postForEntity(uri, request, String.class);

        // Evaluate returned status code.
        assertEquals(200, result.getStatusCodeValue());

        // Evaluate number of events in the database.
        assertEquals(1, repository.count());
    }

    /**
     * Unit test for a non-webhook event being intercepted.
     */
    @Test
    void testNonWebhookEvent() {
        // Create request.
        HttpEntity<String> request = new HttpEntity<>("New non-webhook event");

        // Construct URI string for request.
        String uri = "http://localhost:" + port + "/repository/list";

        // Post request.
        testRestTemplate.postForEntity(uri, request, String.class);

        // Check number of events in the database.
        assertEquals(0, repository.count());
    }
}
