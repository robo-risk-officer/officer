package com.roborisk.officer.interceptors;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * PasswordGrant JSON Request Object.
 */
public class PasswordGrant {

    /**
     * grant_type stores the grant type of the OAuth mechanism.
     */
    @JsonProperty("grant_type")
    @Getter private final String grantType = "password";

    /**
     * username stores the username of the user.
     */
    @JsonProperty("username")
    @Getter @Setter private String username;

    /**
     * password stores the password of the user.
     */
    @JsonProperty("password")
    @Getter @Setter private String password;

    /**
     * The PasswordGrant constructor.
     *
     * @param username  The username of the user.
     * @param password  The password of the user.
     */
    public PasswordGrant(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
