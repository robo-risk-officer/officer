package com.roborisk.officer.interceptors;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.mock.OAuthMock;
import com.roborisk.officer.mock.ProjectsMock;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.RobotSettings;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.repositories.RobotSettingsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Shows how to test a controller that is behind authentication.
 * Remember that it is necessary to set RRO_FAKE_TOKEN in the environment of the system.
 */
@TestPropertySource(properties = {
        "officer.interceptors.auth.enabled=true",
})
class AuthInterceptTest extends OfficerTest {

    /**
     * Http client.
     */
    @Autowired
    private TestRestTemplate template;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link RobotSettings} objects.
     */
    @Autowired
    private RobotSettingsRepository robotSettingsRepository;

    /**
     * Spring environment.
     */
    @Autowired
    private Environment env;

    /**
     * The base url to use to access the GitLab server.
     */
    @Value("${officer.gitlab.baseurl}")
    private String gitLabBaseUrl;

    /**
     * OAuth token from GitLab.
     */
    private String token;

    /**
     * Register the routes for the project.
     */
    @BeforeEach
    void register() {
        ProjectsMock.register();
    }

    /**
     * Makes a request to the /repository/list url and returns the response entity.
     *
     * @param token     OAuth token.
     * @return          The response of the server.
     */
    private ResponseEntity<String> getResponseEntity(String token) {
        // Set the url for repository list
        String url = "http://localhost:" + this.port + "/repository/list";

        // Do request without token
        if (token == null) {
            return this.template.getForEntity(url, String.class);
        }

        // Set headers with given token
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + token);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        // Do request with headers
        return this.template.exchange(url, HttpMethod.GET, entity, String.class);
    }

    /**
     * Test whether the "/repository/list" resource returns the valid response.
     */
    @Test
    public void testAuthenticationOnRepositoryList() {
        OAuthMock.registerHappyFlow();

        // Make request and get response
        ResponseEntity<String> response = getResponseEntity("Token");

        // Check the validity of response
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(response.getBody()).isEmpty()).isEqualTo(false);
    }

    /**
     * Tests if a request without authentication header gets blocked.
     */
    @Test
    public void testAuthenticationNoHeader() {
        // Make request and get response
        ResponseEntity<String> response = getResponseEntity(null);

        // Check that the request was not let through
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    /**
     * Tests if a request with an invalid token gets blocked.
     */
    @Test
    public void testAuthenticationWrongToken() {
        OAuthMock.registerBadFlow();

        // Make request and get response
        ResponseEntity<String> response = getResponseEntity("wrong");

        // Check that the request was not let through
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }
}
