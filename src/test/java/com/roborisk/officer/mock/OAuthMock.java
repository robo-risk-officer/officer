package com.roborisk.officer.mock;

/**
 * Mock the OAuth flow.
 */
public class OAuthMock {

    /**
     * The happy flow of the OAuth.
     */
    public static void registerHappyFlow() {
        // Route to get the token information.
        GitLabMock.registerJSONRoute(
            "/oauth/token/info",
            "oauth/authenticated.json"
        );

        // Route to get the user information.
        GitLabMock.registerJSONRoute(
            "/api/v4/groups/robo-risk-officer/members/1",
            "oauth/member.json"
        );
    }

    /**
     * The bad flow with an invalid token.
     */
    public static void registerBadFlow() {
        // Route to fetch a non-existing token.
        GitLabMock.registerErrorRoute(
            "/oauth/token/info",
            403
        );
    }
}
