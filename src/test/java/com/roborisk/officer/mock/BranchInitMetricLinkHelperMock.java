package com.roborisk.officer.mock;

import com.roborisk.officer.helpers.BranchInitMetricLinkHelperTest;

public class BranchInitMetricLinkHelperMock {

    /**
     * Mocks GitLab API endpoints for the {@link BranchInitMetricLinkHelperTest}.
     */
    public static void register() {
        // Register an endpoint for the merge_base request
        GitLabMock.registerJSONRoute(
            "/api/v4/projects/161/repository/merge_base?refs%5B%5D=master&refs%5B%5D=3cb9217789bb529fc3f704c3d4afff1b29309175",
            "branch_init_link_helper/merge_base_response.json"
        );

        // Register an endpoint for the merge request branch information
        GitLabMock.registerJSONRoute(
            "/api/v4/projects/161/repository/branches/test%2Dfor%2Dwebhook2",
            "branch_init_link_helper/branch_info.json"
        );

        // Register (empty) endpoint for notes
        GitLabMock.registerJSONRoute(
            "/api/v4/projects/161/merge_requests/111/notes",
            "update_awards_event/test_remove_award_notes.json"
        );
    }
}
