package com.roborisk.officer.mock;

import com.roborisk.officer.events.ArtefactChurnReviewEventTest;

/**
 * Mock the routes for {@link ArtefactChurnReviewEventTest}.
 */
public class ArtefactChurnMock {

    /**
     * Registers mock API routes to make tests work.
     */
    public static void register() {
        GitLabMock.registerJSONPostRoute(
            "/api/v4/projects/169/merge_requests/12/discussions",
            "comment_helper/post_thread_success.json"
        );

        GitLabMock.registerJSONPostRoute(
            "/api/v4/projects/169/merge_requests/12/discussions/77b4d2c94e33637a7137ea819ce17dc4939973e6/notes",
            "comment_helper/post_note_success.json"
        );

        GitLabMock.registerJSONRoute(
            "/api/v4/projects/169/merge_requests/12/commits",
            "merge_request_review_helper/get_commits.json"
        );
    }
}
