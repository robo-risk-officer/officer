package com.roborisk.officer.mock;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.put;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;

/**
 * Mock the CommentHelper Routes.
 */
public class CommentHelperMock {

    /**
     * Registers normal comment helper routes.
     */
    public static void register() {
        // Register normal merge request notes route.
        GitLabMock.registerJSONRoute(
            "/api/v4/projects/176/merge_requests/1/notes",
            "comment_helper/merge_request_notes.json"
        );

        GitLabMock.registerJSONPostRoute(
            "/api/v4/projects/176/merge_requests/1/notes",
            "comment_helper/post_note.json"
        );

        GitLabMock.registerJSONPostRoute(
            "/api/v4/projects/176/merge_requests/1/discussions/47379f12df0d7ebaa559f6a41042b01f5eae65cf/notes",
            "comment_helper/post_note.json"
        );

        GitLabMock.registerJSONRoute(
            "/api/v4/projects/176/merge_requests/1/discussions",
            "comment_helper/discussion_thread.json"
        );

        GitLabMock.registerJSONRoute(
            "/api/v4/projects/176/merge_requests/1/discussions/77b4d2c94e33637a7137ea819ce17dc4939973e6",
            "comment_helper/discussion_thread_single.json"
        );

        GitLabMock.registerJSONPostRoute(
            "/api/v4/projects/176/uploads",
            "comment_helper/image_created.json"
        );

        GitLabMock.mock.stubFor(
            put(
                urlPathEqualTo("/api/v4/projects/176/merge_requests/1/discussions/77b4d2c94e33637a7137ea819ce17dc4939973e6")
            )
            .willReturn(
                aResponse()
                .withHeader("content-type", "application/json")
                .withBodyFile("comment_helper/discussion_thread_single.json")
                .withStatus(200)
            )
        );
    }

    /**
     * Registers routes for posting thread and editing thread.
     */
    public static void registerPostThread() {
        GitLabMock.registerJSONPostRoute(
            "/api/v4/projects/176/merge_requests/1/discussions",
            "comment_helper/post_thread_success.json"
        );

        GitLabMock.registerJSONRoute(
            "/api/v4/projects/176/merge_requests/1/discussions",
            "comment_helper/discussion_thread.json"
        );

        GitLabMock.mock.stubFor(
            put(
                urlPathEqualTo("/api/v4/projects/176/merge_requests/1/discussions/77b4d2c94e33637a7137ea819ce17dc4939973e6/notes/5202")
            )
                .willReturn(
                    aResponse()
                        .withHeader("content-type", "application/json")
                        .withStatus(200)
                )
        );
    }

    /**
     * Registers routes for when thread has successfully been resolved.
     */
    public static void registerResolvedThread() {
        GitLabMock.registerJSONRoute(
            "/api/v4/projects/176/merge_requests/1/discussions",
            "comment_helper/discussion_thread_unresolved.json"
        );
    }

    /**
     * Registers comment helper after posting note route.
     */
    public static void registerSuccess() {
        GitLabMock.mock.resetMappings();

        GitLabMock.registerJSONRoute(
            "/api/v4/projects/176/merge_requests/1/notes",
            "comment_helper/merge_request_notes_success.json"
        );

        GitLabMock.registerJSONRoute(
            "/api/v4/projects/176/merge_requests/1/discussions/47379f12df0d7ebaa559f6a41042b01f5eae65cf/notes",
            "comment_helper/merge_request_notes_success.json"
        );

        GitLabMock.registerJSONPostRoute(
            "/api/v4/projects/176/merge_requests/1/notes",
            "comment_helper/post_note_success.json"
        );

        GitLabMock.registerJSONRoute(
            "/api/v4/projects/176/merge_requests/1/discussions",
            "comment_helper/discussion_thread_success_post_note.json"
        );
    }

    /**
     * Registers internal server error route.
     */
    public static void registerInternalServerError() {
        GitLabMock.mock.stubFor(
            put(
                urlPathEqualTo("/api/v4/projects/176/merge_requests/1/discussions/77b4d2c94e33637a7137eaaaace17dc4939973e6")
            )
            .willReturn(
                aResponse()
                .withStatus(500)
            )
        );
    }

    /**
     * Registers wrong repository route.
     */
    public static void registerWrongRepository() {
        // Register wrong repository route.
        GitLabMock.registerErrorRoute(
            "/api/v4/projects/1/merge_requests/1/notes",
            404
        );
    }

    /**
     * Registers routes for when edit was successful.
     */
    public static void registerEditThreadSuccess() {
        GitLabMock.registerJSONRoute(
            "/api/v4/projects/176/merge_requests/1/discussions",
            "comment_helper/discussion_thread_edit_success.json"
        );
    }
}
