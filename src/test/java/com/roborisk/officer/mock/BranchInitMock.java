package com.roborisk.officer.mock;

import com.roborisk.officer.events.BranchInitEvent;

/**
 * Mock the routes for {@link BranchInitEvent}.
 */
public class BranchInitMock {

    /**
     * Register all routes.
     */
    public static void register() {
        GitLabMock.registerJSONRoute(
            "/api/v4/projects/15/repository/branches/development",
            "branch_init_event_mr/development_branch.json"
        );

        GitLabMock.registerJSONRoute(
            "/api/v4/projects/15/repository/branches/master",
            "branch_init_event_mr/master_branch.json"
        );
    }
}
