package com.roborisk.officer.mock;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.put;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;

/**
 * Run a mock server for the GitLab API routes.
 */
public class GitLabMock {

    /**
     * The port of the mock server.
     */
    private static final int PORT = 6063;

    /**
     * The instance of the mock server.
     */
    public static final WireMockServer mock = new WireMockServer(PORT);

    /**
     * Start the mock server up and allow the user route.
     */
    @BeforeAll
    public static void startUp() {
        GitLabMock.mock.start();

        // Register the user route so that the wrapper works.
        GitLabMock.registerJSONRoute(
            "/api/v4/user",
            "user.json"
        );

        GitLabMock.registerJSONRoute(
            "/api/v4/user/keys",
            "ssh_keys/get_sshkeys.json"
        );

        GitLabMock.mock.stubFor(
            post(urlPathEqualTo("/api/v4/user/keys"))
            .willReturn(
                aResponse()
                .withStatus(201)
            )
        );
    }

    /**
     * Cleanup all mappings made.
     */
    @AfterEach
    public void cleanMapping() {
        GitLabMock.mock.resetMappings();
        GitLabMock.mock.resetRequests();
        GitLabMock.mock.resetScenarios();
    }

    /**
     * Clean shutdown after all tests.
     */
    @AfterAll
    static void clean() throws InterruptedException {
        GitLabMock.mock.stop();
    }

    /**
     * Registers a JSON route stub for PUT requests.
     *
     * @param route The route it should stub.
     * @param file  The JSON file it should return.
     */
    public static void registerJSONPutRoute(String route, String file) {
        GitLabMock.mock.stubFor(
            put(
                urlPathEqualTo(route)
            )
            .willReturn(
                aResponse()
                .withHeader("content-type", "application/json")
                .withBodyFile(file)
            )
        );
    }

    /**
     * Registers a JSON route stub for POST requests.
     *
     * @param route The route it should stub.
     * @param file  The JSON file it should return.
     */
    public static void registerJSONPostRoute(String route, String file) {
        GitLabMock.mock.stubFor(
            post(
                urlPathEqualTo(route)
            )
            .willReturn(
                aResponse()
                .withHeader("content-type", "application/json")
                .withBodyFile(file)
            )
        );
    }

    /**
     * Registers a JSON route stub.
     *
     * @param route The route it should stub.
     * @param file  The JSON file it should return.
     */
    public static void registerJSONRoute(String route, String file) {
        GitLabMock.mock.stubFor(
            get(
                urlPathEqualTo(route)
            )
            .willReturn(
                aResponse()
                .withHeader("content-type", "application/json")
                .withBodyFile(file)
            )
        );
    }

    /**
     * Registers a JSON route stub that matches to URL patterns.
     *
     * @param routePattern  The route pattern it should stub.
     * @param file          The JSON file it should return.
     */
    public static void registerJSONRouteMatcher(String routePattern, String file) {
        GitLabMock.mock.stubFor(
            get(
                urlPathMatching(routePattern)
            )
            .willReturn(
                aResponse()
                .withHeader("content-type", "application/json")
                .withBodyFile(file)
            )
        );
    }

    /**
     * Register a route stub that results in an HTTP error.
     *
     * @param route The route to support.
     * @param error The error to return.
     */
    public static void registerErrorRoute(String route, int error) {
        GitLabMock.mock.stubFor(
            get(
                urlPathEqualTo(route)
            )
            .willReturn(
                aResponse()
                .withStatus(error)
            )
        );
    }

    /**
     * Register a route stub that results in an HTTP error.
     *
     * @param route The route to support.
     * @param error The error to return.
     * @param msg   The message body to return.
     */
    public static void registerErrorRoute(String route, int error, String msg) {
        GitLabMock.mock.stubFor(
            get(
                urlPathEqualTo(route)
            )
            .willReturn(
                aResponse()
                .withStatus(error)
                .withBody(msg)
            )
        );
    }
}
