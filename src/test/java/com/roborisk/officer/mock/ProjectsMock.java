package com.roborisk.officer.mock;

/**
 * Mock the Project Routes.
 */
public class ProjectsMock {
    /**
     * Registers the projects mock.
     */
    public static void register() {
        GitLabMock.registerJSONRoute(
            "/api/v4/projects",
            "projects.json"
        );
    }
}
