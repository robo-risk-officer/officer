package com.roborisk.officer.events;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.mock.BranchInitMock;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.JSONObjectBuilder;
import com.roborisk.officer.models.web.MergeRequestWebhookData;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.text.ParseException;

import static org.junit.Assert.assertEquals;

/**
 * Tests the functionality of the {@link BranchInitEvent} event.
 */
public class BranchInitEventTest extends OfficerTest {

    /**
     * The event to test.
     */
    @Autowired
    private BranchInitEvent event;

    /**
     * Database repository to handle {@link com.roborisk.officer.models.database.Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Tests the handling of the {@link BranchInitEvent} event
     * when sending a {@link MergeRequestWebhookData}.
     */
    @Test
    public void testHandler_MergeRequestWebhookData() throws JSONException, IOException, ParseException {
        BranchInitMock.register();

        JSONObject object = JSONObjectBuilder.getMergeRequestWebHook();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        MergeRequestWebhookData data = mapper.readValue(object.toString(), MergeRequestWebhookData.class);

        Repository repository = new Repository(data.getRepository());
        this.repositoryRepository.save(repository);
        GitCommit commit = ModelFactory.getGitCommit();
        this.gitCommitRepository.save(commit);
        Branch master =
                ModelFactory.getBranch(repository, commit, "master");
        this.branchRepository.save(master);

        this.event.handle(EventType.MERGE_REQUEST_CHANGED, data);

        // All we care about it if they are stored.
        assertEquals(2, this.branchRepository.count());
    }

    /**
     * Tests the handling of the {@link BranchInitEvent} event
     * when sending a {@link CommentWebhookData}.
     */
    @Test
    public void testHandler_CommentWebhookData() throws JSONException, IOException, ParseException {
        BranchInitMock.register();

        JSONObject object = JSONObjectBuilder.getCommentWebHook();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommentWebhookData data = mapper.readValue(object.toString(), CommentWebhookData.class);

        Repository repository = new Repository(data.getRepository());
        this.repositoryRepository.save(repository);
        GitCommit commit = ModelFactory.getGitCommit();
        this.gitCommitRepository.save(commit);
        Branch master =
            ModelFactory.getBranch(repository, commit, "master");
        this.branchRepository.save(master);

        this.event.handle(EventType.MERGE_REQUEST_CHANGED, data);

        // All we care about it if they are stored.
        assertEquals(2, this.branchRepository.count());
    }
}
