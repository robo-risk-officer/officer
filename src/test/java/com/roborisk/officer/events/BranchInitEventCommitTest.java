package com.roborisk.officer.events;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.mock.BranchInitMock;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.JSONObjectBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * This class contains tests for the {@link BranchInitEventCommit} trigger.
 */
public class BranchInitEventCommitTest extends OfficerTest {

    /**
     * Http client.
     */
    @Autowired
    private TestRestTemplate template;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * The event to test.
     */
    @Autowired
    private BranchInitEventCommit event;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Tests the addition of new branches when
     * they are seen for the first time. The test is based on the push webhook.
     *
     * @throws JSONException When the JSON cannot be properly constructed.
     */
    @Test
    public void testCommitWebHook() throws JSONException {
        JSONObject body = JSONObjectBuilder.getPushWebHook();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request = new HttpEntity<>(body.toString(), headers);
        String uri = "http://localhost:" + this.port + "/webhook/commit";

        ResponseEntity<String> response = this.template.postForEntity(uri, request, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        this.dispatcher.run();

        assertEquals(1, this.branchRepository.count());
    }

    /**
     * Tests adding a branch twice. The test is based on the push webhook.
     * It should result in only one result, because branches are uniquely
     * defined by the branch name and repository ID.
     *
     * @throws JSONException When the JSON cannot be properly constructed.
     */
    @Test
    public void testCommitWebHookTwice() throws JSONException {
        JSONObject body = JSONObjectBuilder.getPushWebHook();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request = new HttpEntity<>(body.toString(), headers);
        String uri = "http://localhost:" + this.port + "/webhook/commit";

        ResponseEntity<String> response = this.template.postForEntity(uri, request, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        response = this.template.postForEntity(uri, request, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        this.dispatcher.run();

        assertEquals(1, this.branchRepository.count());
    }

    /**
     * Tests adding a branch when there are no commits defined.
     * The test is based on the push webhook.
     *
     * @throws JSONException When the JSON cannot be properly constructed.
     * @throws IOException   When the JSON cannot be deserialized.
     */
    @Test
    public void testWebhookNoCommits() throws JSONException, IOException {
        BranchInitMock.register();

        JSONObject body = JSONObjectBuilder.getPushWebHook();
        body.remove("commits");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommitWebhookData data = mapper.readValue(body.toString(), CommitWebhookData.class);

        Repository repository = new Repository(data.getRepository());
        this.repositoryRepository.save(repository);

        this.event.handle(EventType.PUSH_EVENT, data);

        assertEquals(1, this.branchRepository.count());
    }

    /**
     * Tests the exception thrown when the wrong webhook data is used.
     *
     * @throws JSONException When the JSON cannot be properly constructed.
     * @throws IOException   When the JSON cannot be deserialized.
     */
    @Test
    public void testWrongWebhookType() throws JSONException, IOException {
        BranchInitMock.register();

        JSONObject body = JSONObjectBuilder.getCommentWebHook();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommentWebhookData data = mapper.readValue(body.toString(), CommentWebhookData.class);

        assertThrows(IllegalArgumentException.class, () -> this.event.handle(EventType.PUSH_EVENT, data));
    }

    /**
     * Tests the event handler with a commit webhook data with
     * and empty commit list.
     *
     * @throws JSONException When the JSON cannot be properly constructed.
     * @throws IOException   When the JSON cannot be deserialized.
     */
    @Test
    public void testEmptyCommitList() throws JSONException, IOException {
        BranchInitMock.register();

        JSONObject body = JSONObjectBuilder.getPushWebHook();
        body.remove("commits");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommitWebhookData data = mapper.readValue(body.toString(), CommitWebhookData.class);
        data.setCommits(new ArrayList<>());

        Repository repository = new Repository(data.getRepository());
        this.repositoryRepository.save(repository);

        this.event.handle(EventType.PUSH_EVENT, data);
        assertEquals(1, this.branchRepository.count());
    }
}
