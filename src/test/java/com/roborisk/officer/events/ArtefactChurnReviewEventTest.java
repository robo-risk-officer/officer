package com.roborisk.officer.events;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.mock.ArtefactChurnMock;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.BranchClassObservation;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.BranchFunctionObservation;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestClassObservation;
import com.roborisk.officer.models.database.MergeRequestFunctionObservation;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.metrics.BranchClassMetricResult;
import com.roborisk.officer.models.database.metrics.BranchFunctionMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestClassMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestFunctionMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.BranchClassMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchClassObservationRepository;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import com.roborisk.officer.models.repositories.BranchFunctionMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchFunctionObservationRepository;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestClassMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestClassObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestFunctionMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestFunctionObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MetricRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.JSONObjectBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.text.ParseException;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link ArtefactChurnReviewEvent} class.
 */
public class ArtefactChurnReviewEventTest extends OfficerTest {

    /**
     * Http client.
     */
    @Autowired
    private TestRestTemplate template;

    /**
     * Base path of the JSON files used in test cases.
     */
    private static final String jsonBasePath = "src/test/resources/__files/artifact_review_event";

    /**
     * {@link MergeRequest} object to test with.
     */
    private MergeRequest testMergeRequest;

    /**
     * {@link Repository} object to test with.
     */
    private Repository testRepository;

    /**
     * The GitLab ID of the testRepository.
     * This is required since the ID should match with the json webhook requests used.
     */
    private static final int testRepositoryGitLabId = 169;

    /**
     * Source {@link Branch} for the testMergeRequest.
     */
    private Branch sourceBranch;

    /**
     * Target {@link Branch} for the testMergeRequest.
     */
    private Branch targetBranch;

    /**
     * First {@link GitCommit} for the sourceBranch to test with.
     */
    private GitCommit firstSourceCommit;

    /**
     * Second {@link GitCommit} for the sourceBranch to test with.
     */
    private GitCommit secondSourceCommit;

    /**
     * The hash of the second {@link GitCommit} for the sourceBranch to match with the JSON webhook.
     */
    private static final String secondSourceCommitHash = "f482285255d8aad2f684e232f1fd98750ba44e85";

    /**
     * {@link GitCommit} for the targetBranch to test with.
     */
    private GitCommit targetCommit;

    /**
     * {@link BranchFile} to test with.
     */
    private BranchFile branchFile;

    /**
     * {@link BranchFunctionObservation} to test with.
     */
    private BranchFunctionObservation branchFunctionObservation;

    /**
     * {@link BranchFunctionMetricResult} to test with.
     */
    private BranchFunctionMetricResult branchFunctionMetricResult;

    /**
     * {@link Metric} to test with.
     */
    private Metric metric;

    /**
     * {@link BranchClassObservation} to test with.
     */
    private BranchClassObservation branchClassObservation;

    /**
     * {@link BranchClassMetricResult} to test with.
     */
    private BranchClassMetricResult branchClassMetricResult;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link BranchFile} objects.
     */
    @Autowired
    private BranchFileRepository branchFileRepository;

    /**
     * Database repository to handle {@link BranchFunctionObservation} objects.
     */
    @Autowired
    private BranchFunctionObservationRepository branchFunctionObservationRepository;

    /**
     * Database repository to handle {@link BranchFunctionMetricResult} objects.
     */
    @Autowired
    private BranchFunctionMetricResultRepository branchFunctionMetricResultRepository;

    /**
     * Database repository to handle {@link Metric} objects.
     */
    @Autowired
    private MetricRepository metricRepository;

    /**
     * Database repository to handle {@link BranchClassObservation} objects.
     */
    @Autowired
    private BranchClassObservationRepository branchClassObservationRepository;

    /**
     * Database repository to handle {@link BranchClassMetricResult} objects.
     */
    @Autowired
    private BranchClassMetricResultRepository branchClassMetricResultRepository;

    /**
     * Database repository to handle {@link MergeRequestFunctionObservation} objects.
     */
    @Autowired
    private MergeRequestFunctionObservationRepository mergeRequestFunctionObservationRepository;

    /**
     * Database repository to handle {@link MergeRequestFunctionMetricResult} objects.
     */
    @Autowired
    private MergeRequestFunctionMetricResultRepository mergeRequestFunctionMetricResultRepository;

    /**
     * Database repository to handle {@link MergeRequestClassObservationRepository} objects.
     */
    @Autowired
    private MergeRequestClassObservationRepository mergeRequestClassObservationRepository;

    /**
     * Database repository to handle {@link MergeRequestClassMetricResult} objects.
     */
    @Autowired
    private MergeRequestClassMetricResultRepository mergeRequestClassMetricResultRepository;

    /**
     * Set up API mock routes to prevent test case failures due to comment posting.
     */
    @BeforeEach
    public void registerRoutes() {
        ArtefactChurnMock.register();
    }

    /**
     * Sets up all database objects that are necessary to perform a test.
     *
     * @param mrGitLabId    Specify the GitLab ID of the merge request that is created.
     *                              Required to match the GitLab ID in the JSON webhook request.
     * @param mrGitLabIid   Specify the GitLab ID of the merge request that is created.
     *                              Required to match the GitLab ID in the JSON webhook request.
     */
    public void setup(int mrGitLabId, int mrGitLabIid) throws ParseException {
        // The below objects need to be created, since the tests assume that the merge request
        // is already present in the database.
        this.targetCommit = ModelFactory.getGitCommit();
        this.targetCommit.setFinishedProcessing(true);
        this.gitCommitRepository.save(this.targetCommit);
        this.firstSourceCommit = ModelFactory.getGitCommit(this.targetCommit);
        this.firstSourceCommit.setFinishedProcessing(true);
        this.gitCommitRepository.save(this.firstSourceCommit);
        this.secondSourceCommit = ModelFactory.getGitCommit(this.firstSourceCommit);
        this.secondSourceCommit.setHash(this.secondSourceCommitHash);
        this.secondSourceCommit.setFinishedProcessing(true);
        this.gitCommitRepository.save(this.secondSourceCommit);
        this.testRepository = ModelFactory.getRepository(this.testRepositoryGitLabId);
        this.repositoryRepository.save(this.testRepository);
        this.sourceBranch = ModelFactory.getBranch(this.testRepository, this.secondSourceCommit,
                "setup");
        this.branchRepository.save(this.sourceBranch);
        this.targetBranch = ModelFactory.getBranch(this.testRepository, this.targetCommit,
                "master");
        this.branchRepository.save(this.targetBranch);
        this.testMergeRequest = ModelFactory.getMergeRequest(this.sourceBranch, this.targetBranch,
            mrGitLabId, mrGitLabIid);
        this.mergeRequestRepository.save(this.testMergeRequest);

        // Also create a fake branch_function_observation and supporting objects to run tests with
        this.branchFile = ModelFactory.getBranchFile(sourceBranch, this.secondSourceCommit);
        this.branchFileRepository.save(this.branchFile);
        this.metric = ModelFactory.getMetric();
        this.metricRepository.save(metric);
        this.branchFunctionObservation =
                ModelFactory.getBranchFunctionObservation(this.secondSourceCommit, this.branchFile);
        this.branchFunctionObservationRepository.save(this.branchFunctionObservation);
        this.branchFunctionMetricResult = ModelFactory.getBranchFunctionMetricResult(
                this.branchFunctionObservation, this.metric);
        this.branchFunctionMetricResultRepository.save(this.branchFunctionMetricResult);

        // And a fake branch_class_observation and supporting objects to run tests with
        this.branchClassObservation =
                ModelFactory.getBranchClassObservation(this.secondSourceCommit, this.branchFile);
        this.branchClassObservationRepository.save(this.branchClassObservation);
        this.branchClassMetricResult = ModelFactory.getBranchClassMetricResult(
                this.branchClassObservation, this.metric);
        this.branchClassMetricResultRepository.save(this.branchClassMetricResult);
    }

    /**
     * Tests that the correct branch-based artifact churn observations/results are copied and
     * associated to merge-request based artifact churn observation/results.
     */
    @Test
    public void testCorrectAssociation() throws ParseException, IOException, JSONException {
        // First, set the properties that define this test (to match the JSON files for webhooks)
        int gitLabMrId = 66;
        int gitLabMrIid = 12;

        // And initialise the database such that there is a merge request in it
        this.setup(gitLabMrId, gitLabMrIid);

        // Generate a webhook to initialise the merge request
        JSONObject mrBody = JSONObjectBuilder.fromFile(this.jsonBasePath
            + "/mr_webhook_merge_request.json");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> mrRequest = new HttpEntity<>(mrBody.toString(), headers);
        String mrUri = "http://localhost:" + port + "/webhook/merge-request";
        ResponseEntity<String> mrResponse = template.postForEntity(mrUri, mrRequest, String.class);

        // Check that the webhook returns success
        assertEquals(HttpStatus.OK, mrResponse.getStatusCode());

        // Generate a webhook to ask for a review of the merge request so that the
        // ArtefactChurnReviewEvent is fired
        JSONObject commentBody = JSONObjectBuilder.fromFile(this.jsonBasePath
            + "/mr_webhook_request_review_comment.json");
        HttpEntity<String> commentRequest = new HttpEntity<>(commentBody.toString(), headers);
        String commentUri = "http://localhost:" + port + "/webhook/comments";
        ResponseEntity<String> commentRepsonse = template.postForEntity(commentUri, commentRequest,
                String.class);

        // Check that the webhook returns success
        assertEquals(HttpStatus.OK, commentRepsonse.getStatusCode());

        this.dispatcher.run();

        // Check that in the database, a MergeRequestFunctionObservation was created that
        // is equivalent to the BranchFunctionObservation used for testing
        assertEquals(1, this.mergeRequestFunctionObservationRepository.count());
        MergeRequestFunctionObservation mrfo =
                this.mergeRequestFunctionObservationRepository.findAll().iterator().next();
        assertEquals(this.branchFunctionObservation.getClassName(), mrfo.getClassName());
        assertEquals(this.branchFunctionObservation.getFunctionName(), mrfo.getFunctionName());
        assertEquals(this.branchFunctionObservation.getLineNumber(), mrfo.getLineNumber());
        assertEquals(this.branchFunctionObservation.getBranchFile().getFileName(),
                mrfo.getMergeRequestFile().getFileName());
        assertEquals(this.branchFunctionObservation.getBranchFile().getFilePath(),
            mrfo.getMergeRequestFile().getFilePath());

        // Check that in the database, a MergeRequestFunctionMetricResult was created that is
        // equivalent to the BranchFunctionMetricResult used for testing
        assertEquals(1, this.mergeRequestFunctionMetricResultRepository.count());
        MergeRequestFunctionMetricResult mrfmr = this.mergeRequestFunctionMetricResultRepository
                .findAll().iterator().next();
        assertEquals(this.branchFunctionMetricResult.getIsLowQuality(), mrfmr.getIsLowQuality());
        assertEquals(this.branchFunctionMetricResult.getScore(), mrfmr.getScore());
        assertEquals(this.branchFunctionMetricResult.getMetric().getId(),
                mrfmr.getMetric().getId());
        assertEquals(mrfo.getId(), mrfmr.getMergeRequestFunctionObservation().getId());

        // Check that in the database a MergeRequestClassObservation was created that is equivalent
        // to the BranchClassObservation used for testing
        assertEquals(1, this.mergeRequestClassObservationRepository.count());
        MergeRequestClassObservation mrco = this.mergeRequestClassObservationRepository.findAll()
                .iterator().next();
        assertEquals(this.branchClassObservation.getClassName(), mrco.getClassName());
        assertEquals(this.branchClassObservation.getLineNumber(), mrco.getLineNumber());
        assertEquals(this.branchClassObservation.getBranchFile().getFileName(),
                mrco.getMergeRequestFile().getFileName());
        assertEquals(this.branchClassObservation.getBranchFile().getFilePath(),
                mrco.getMergeRequestFile().getFilePath());

        // Check that in the database, a MergeRequestClassMetricResult was created that is
        // equivalent to the BranchClassMetricResult used for testing
        assertEquals(1, this.mergeRequestClassMetricResultRepository.count());
        MergeRequestClassMetricResult mrcmr = this.mergeRequestClassMetricResultRepository
                .findAll().iterator().next();
        assertEquals(this.branchClassMetricResult.getIsLowQuality(), mrcmr.getIsLowQuality());
        assertEquals(this.branchClassMetricResult.getScore(), mrcmr.getScore());
        assertEquals(this.branchClassMetricResult.getMetric().getId(), mrcmr.getMetric().getId());
        assertEquals(mrco.getId(), mrcmr.getMergeRequestClassObservation().getId());

        // Test passed
    }

}
