package com.roborisk.officer.events;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.mock.BranchInitMock;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.JSONObjectBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * This class contains tests for the {@link BranchLatestCommitEvent} trigger.
 */
public class BranchLatestCommitEventTest extends OfficerTest {

    /**
     * Http client.
     */
    @Autowired
    private TestRestTemplate template;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * The event to test.
     */
    @Autowired
    private BranchLatestCommitEvent event;

    /**
     * Check whether the processing of the event succeeded.
     *
     * @param body              A {@link JSONObject} with the push webhook.
     * @throws JSONException    When the JSON cannot be properly constructed.
     */
    private void checkSuccess(JSONObject body) throws JSONException {
        Repository repository = this.repositoryRepository
            .findByGitLabId(body.getInt("project_id")).orElseThrow();

        Branch branch = this.branchRepository.findByNameAndRepositoryId(
            "master",
            repository.getId()
        ).orElseThrow();

        assertEquals(body.get("after"), branch.getLatestCommit().getHash());
    }

    /**
     * Tests altering the branch latest commit.
     * The test is based on the push webhook.
     *
     * @throws JSONException When the JSON cannot be properly constructed.
     */
    @Test
    public void testCommitWebHook() throws JSONException {
        JSONObject body = JSONObjectBuilder.getPushWebHook();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request = new HttpEntity<>(body.toString(), headers);
        String uri = "http://localhost:" + port + "/webhook/commit";

        ResponseEntity<String> response = template.postForEntity(uri, request, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        this.dispatcher.run();

        this.checkSuccess(body);
    }

    /**
     * Tests the exception thrown when the wrong webhook data is used.
     *
     * @throws JSONException When the JSON cannot be properly constructed.
     * @throws IOException   When the JSON cannot be deserialized.
     */
    @Test
    public void testWrongWebhookType() throws JSONException, IOException {
        BranchInitMock.register();

        JSONObject body = JSONObjectBuilder.getCommentWebHook();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommentWebhookData data = mapper.readValue(body.toString(), CommentWebhookData.class);

        assertThrows(IllegalArgumentException.class, () -> this.event.handle(EventType.PUSH_EVENT, data));
    }

    /**
     * Tests the exception when the commit list is empty.
     *
     * @throws JSONException When the JSON cannot be properly constructed.
     * @throws IOException   When the JSON cannot be deserialized.
     */
    @Test
    public void testEmptyCommitsInPushWebhook() throws JSONException, IOException {
        JSONObject body = JSONObjectBuilder.getPushWebHook();
        body.remove("commits");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommitWebhookData data = mapper.readValue(body.toString(), CommitWebhookData.class);
        data.setCommits(new ArrayList<>());

        assertThrows(IllegalStateException.class, () -> this.event.handle(EventType.PUSH_EVENT, data));
    }
}
