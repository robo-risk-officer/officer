package com.roborisk.officer.events;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.JSONObjectBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class MergeRequestReviewInitEventTest extends OfficerTest {

    /**
     * Local instance of the {@link MergeRequestReviewInitEvent} class.
     */
    @Autowired
    MergeRequestReviewInitEvent event;

    /**
     * Repository for handling {@link com.roborisk.officer.models.database.MergeRequest} objects.
     */
    @Autowired
    MergeRequestRepository mergeRequestRepository;

    /**
     * Test for checking whether the correct exception is thrown if the wrong type of
     * AbstractWebhookData is supplied.
     */
    @Test
    public void handleWrongAbstractWebhookDataType() {
        CommitWebhookData data = new CommitWebhookData();

        try {
            this.event.handle(EventType.REVIEW_REQUESTED, data);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("MergeRequestReviewInitEvent.handle: Only supports " +
                "CommentWebhookData", e.getMessage());
        }
    }

    /**
     * Test for checking whether the correct exception is thrown if the merge request cannot
     * be found in the database.
     */
    @Test
    public void handleMergeRequestNotFound() throws JSONException, IOException {
        JSONObject object = JSONObjectBuilder.getCommentWebHook();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommentWebhookData data = mapper.readValue(object.toString(), CommentWebhookData.class);

        this.mergeRequestRepository.deleteAll();

        try {
            this.event.handle(EventType.REVIEW_REQUESTED, data);
            fail();
        } catch (IllegalStateException e) {
            assertTrue(e.getMessage().startsWith("MergeRequestReviewInitEvent.handle: Cannot find MergeRequest with GitLab ID"));
        }
    }
}
