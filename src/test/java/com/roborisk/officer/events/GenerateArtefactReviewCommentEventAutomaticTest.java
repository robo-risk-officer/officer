package com.roborisk.officer.events;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.*;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.InternalSonarWebhookData;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class providing tests for the {@link GenerateArtefactReviewCommentEvent} class.
 * Not to be confused with {@link GenerateArtefactReviewCommentEventTest}, which can only be
 * evaluated manually and is therefore disabled.
 */
public class GenerateArtefactReviewCommentEventAutomaticTest extends OfficerTest {

    @Autowired
    GenerateArtefactReviewCommentEvent generateArtefactReviewCommentEvent;

    /**
     * Test for checking that the correct exception is thrown if the wrong type of
     * AbstractWebhookData is supplied.
     */
    @Test
    void handleNotCommentWebhookData() {
        CommitWebhookData commitWebhookData = new CommitWebhookData();

        // Submit commitWebhookData
        try {
            generateArtefactReviewCommentEvent.handle(EventType.REVIEW_REQUESTED, commitWebhookData);
            // Error should have been thrown
            fail();
        } catch (IllegalArgumentException e) {
            // Check that the correct exception was thrown
            assertEquals("GenerateArtefactReviewCommentEvent.handle: Only CommentWebhookData and "
                + "InternalSonarWebhookData are supported", e.getMessage());
        }
    }

    /**
     * Test for checking that the correct error is thrown if the merge request associated to the
     * CommentWebhookData is not found in the database.
     */
    @Test
    void handleCommentWebhookDataMRNotFound() throws ParseException, IOException {
        CommentWebhookData commentWebhookData = new CommentWebhookData();

        // Create maps representing the JSON objects.
        Map<String, Object> authormap = new LinkedHashMap<>();
        Map<String, Object> commitmap = new LinkedHashMap<>();
        Map<String, Object> map = new LinkedHashMap<>();

        // Add random data
        authormap.put("name", "name");
        authormap.put("email", "email");

        // Add more random data
        commitmap.put("id", "hashsishe");
        commitmap.put("message", "some message");
        commitmap.put("modified", new ArrayList<>());
        commitmap.put("removed", new ArrayList<>());
        commitmap.put("added", new ArrayList<>());
        commitmap.put("timestamp", "2015-04-08T21:00:25-07:00");
        commitmap.put("author", authormap);

        // Add more more random data
        map.put("id", 1);
        map.put("iid", 2);
        map.put("target_branch", "target");
        map.put("source_branch", "source");
        map.put("author_id", 4);
        map.put("title", "Some merge request");
        map.put("created_at", "2015-04-08T21:00:25-07:00");
        map.put("updated_at", "2015-04-08T21:00:25-07:00");
        map.put("state", "merged");
        map.put("action", "marked");
        map.put("merge_status", "can_be_merged");
        map.put("work_in_progress", false);
        map.put("last_commit", commitmap);

        commentWebhookData.setMergeRequest(map);

        // Submit commentWebhookData
        try {
            generateArtefactReviewCommentEvent.handle(EventType.REVIEW_REQUESTED, commentWebhookData);
            // Error should have been thrown
            fail();
        } catch (IllegalStateException e) {
            // Check that the correct error is thrown
            assertTrue(e.getMessage().startsWith("GenerateArtefactReviewCommentEvent.hanlde: Could not find" +
                " MergeRequest with GitLab ID"));
        }
    }

    /**
     * Test for checking that the correct error is thrown if the merge request associated to the
     * InternalSonarWebhookData is not found in the database.
     */
    @Test
    void handleInternalSonarWebhookDataMRNotFound() throws ParseException {
        InternalSonarWebhookData internalSonarWebhookData = new InternalSonarWebhookData();

        // Instantiate objects
        // Don't store them in the database
        Repository repository = ModelFactory.getRepository();
        GitCommit gitCommit = ModelFactory.getGitCommit();
        Branch sourceBranch = ModelFactory.getBranch(repository, gitCommit);
        Branch targetBranch = ModelFactory.getBranch(repository, gitCommit);
        MergeRequest mergeRequest = ModelFactory.getMergeRequest(sourceBranch, targetBranch);

        MergeRequestReview mergeRequestReview = ModelFactory.getMergeRequestReview(gitCommit, mergeRequest);

        SonarAnalysisTracker sonarAnalysisTracker = new SonarAnalysisTracker();
        sonarAnalysisTracker.setMergeRequestReview(mergeRequestReview);

        internalSonarWebhookData.setSourceBranchAnalysis(sonarAnalysisTracker);

        // Submit internalSonarWebhookData
        try {
            generateArtefactReviewCommentEvent.handle(EventType.REVIEW_REQUESTED, internalSonarWebhookData);
            // Error should have been thrown
            fail();
        } catch (IllegalStateException e) {
            // Check that the correct error is thrown
            assertTrue(e.getMessage().startsWith("GenerateArtefactReviewCommentEvent.handle: Could not " +
                "find MergeRequest with GitLab ID"));
        }
    }
}
