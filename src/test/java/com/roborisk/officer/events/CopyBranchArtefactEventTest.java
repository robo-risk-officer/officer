package com.roborisk.officer.events;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.mock.BranchInitMock;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.BranchClassObservation;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.BranchFunctionObservation;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestFunctionObservation;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.BranchClassObservationRepository;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import com.roborisk.officer.models.repositories.BranchFunctionObservationRepository;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestClassObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestFileRepository;
import com.roborisk.officer.models.repositories.MergeRequestFunctionObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.JSONObjectBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * This class tests the functionality of the {@link CopyBranchArtefactEvent}.
 */
public class CopyBranchArtefactEventTest extends OfficerTest {

    /**
     * The event to get tested.
     */
    @Autowired
    private CopyBranchArtefactEvent event;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link MergeRequestReview} objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Database repository to handle
     * {@link com.roborisk.officer.models.database.MergeRequestFile} objects.
     */
    @Autowired
    private MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * Database repository to handle {@link BranchFile} objects.
     */
    @Autowired
    private BranchFileRepository branchFileRepository;

    /**
     * Database repository to handle {@link BranchFunctionObservation} objects.
     */
    @Autowired
    private BranchFunctionObservationRepository branchFunctionObservationRepository;

    /**
     * Database repository to handle {@link MergeRequestFunctionObservation} objects.
     */
    @Autowired
    private MergeRequestFunctionObservationRepository mergeRequestFunctionObservationRepository;

    /**
     * Database repository to handle {@link BranchFunctionObservation} objects.
     */
    @Autowired
    private BranchClassObservationRepository branchClassObservationRepository;

    /**
     * Database repository to handle {@link MergeRequestFunctionObservation} objects.
     */
    @Autowired
    private MergeRequestClassObservationRepository mergeRequestClassObservationRepository;

    public Pair<GitCommit, BranchFile> prepare(CommentWebhookData data) throws JSONException, IOException {
        Repository repository = new Repository(data.getRepository());
        GitCommit commit = new GitCommit(data.getMergeRequest().getLastCommit());
        Branch source = new Branch(data.getMergeRequest().getSource(), repository, commit);
        Branch target = new Branch(data.getMergeRequest().getTarget(), repository, commit);
        MergeRequest mergeRequest = new MergeRequest(data.getMergeRequest(), source, target);

        MergeRequestReview review = new MergeRequestReview();
        review.setMergeRequest(mergeRequest);
        review.setCommit(commit);

        this.gitCommitRepository.save(commit);
        this.repositoryRepository.save(repository);
        this.branchRepository.save(source);
        this.branchRepository.save(target);
        this.mergeRequestRepository.save(mergeRequest);
        this.mergeRequestReviewRepository.save(review);

        BranchFile file = ModelFactory.getBranchFile(source, commit);
        this.branchFileRepository.save(file);

        return new Pair<>(commit, file);
    }

    /**
     * Test the handle of the {@link CopyBranchArtefactEvent}.
     *
     * @throws JSONException    When the JSON Object cannot be created.
     * @throws IOException      When the JSON object cannot be deserialized.
     */
    @Test
    public void testHandle_FileCopy() throws JSONException, IOException {
        JSONObject body = JSONObjectBuilder.getCommentWebHook();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommentWebhookData data = mapper.readValue(body.toString(), CommentWebhookData.class);

        this.prepare(data);
        this.event.handle(EventType.REVIEW_REQUESTED, data);

        assertTrue(this.mergeRequestFileRepository.count() > 0);
    }

    /**
     * Test the handle of the {@link CopyBranchArtefactEvent}.
     * It tests the copy {@link BranchFunctionObservation} to a
     * {@link MergeRequestFunctionObservation}.
     *
     * @throws JSONException    When the JSON Object cannot be created.
     * @throws IOException      When the JSON object cannot be deserialized.
     */
    @Test
    public void testHandle_FunctionCopy() throws JSONException, IOException {
        JSONObject body = JSONObjectBuilder.getCommentWebHook();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommentWebhookData data = mapper.readValue(body.toString(), CommentWebhookData.class);

        Pair<GitCommit, BranchFile> pair = this.prepare(data);

        GitCommit commit = pair.getKey();
        BranchFile file = pair.getValue();

        BranchFunctionObservation observation = ModelFactory.getBranchFunctionObservation(commit, file);
        this.branchFunctionObservationRepository.save(observation);

        this.event.handle(EventType.REVIEW_REQUESTED, data);

        assertTrue(this.mergeRequestFunctionObservationRepository.count() > 0);
    }

    /**
     * Test the handle of the {@link CopyBranchArtefactEvent}.
     *
     * @throws JSONException    When the JSON Object cannot be created.
     * @throws IOException      When the JSON object cannot be deserialized.
     */
    @Test
    public void testHandle_ClassCopy() throws JSONException, IOException {
        JSONObject body = JSONObjectBuilder.getCommentWebHook();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommentWebhookData data = mapper.readValue(body.toString(), CommentWebhookData.class);

        Pair<GitCommit, BranchFile> pair = this.prepare(data);

        GitCommit commit = pair.getKey();
        BranchFile file = pair.getValue();

        BranchClassObservation observation = ModelFactory.getBranchClassObservation(commit, file);
        this.branchClassObservationRepository.save(observation);

        this.event.handle(EventType.REVIEW_REQUESTED, data);

        assertTrue(this.mergeRequestClassObservationRepository.count() > 0);
    }

    /**
     * Tests the exception thrown when the wrong webhook data is used.
     *
     * @throws JSONException When the JSON cannot be properly constructed.
     * @throws IOException   When the JSON cannot be deserialized.
     */
    @Test
    public void testWrongWebhookType() throws JSONException, IOException {
        BranchInitMock.register();

        JSONObject body = JSONObjectBuilder.getPushWebHook();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommitWebhookData data = mapper.readValue(body.toString(), CommitWebhookData.class);

        assertThrows(IllegalArgumentException.class, () -> this.event.handle(EventType.PUSH_EVENT, data));
    }
}
