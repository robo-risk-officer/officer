package com.roborisk.officer.events;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.mock.BranchInitMock;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.metrics.BranchFileMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.BranchFileMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import com.roborisk.officer.models.repositories.MetricRepository;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.JSONObjectBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * This class contains tests for the {@link CodeChurnEvent} trigger.
 */
public class CodeChurnEventTest extends OfficerTest {

    /**
     * Http client.
     */
    @Autowired
    private TestRestTemplate template;

    /**
     * Database repository to handle {@link Metric} objects.
     */
    @Autowired
    private MetricRepository metricRepository;

    /**
     * Database repository to handle {@link BranchFileMetricResult} objects.
     */
    @Autowired
    private BranchFileMetricResultRepository branchFileMetricResultRepository;

    /**
     * Database repository to handle {@link BranchFile} objects.
     */
    @Autowired
    private BranchFileRepository branchFileRepository;

    /**
     * The event to test.
     */
    @Autowired
    private CodeChurnEvent event;

    /**
     * Tests the addition and modification of a file in the same push, but different commits.
     * The bug this tests for was caused due to the fact that the file was only stored after
     * the entire push event was handled. This caused it to lose track of the changes that were
     * done in the same push event.
     */
    @Test
    public void testCommitWebhook_AdditionAndModificationInSamePush() throws JSONException {
        JSONObject body = JSONObjectBuilder.getPushWebHook();

        body.remove("commits");

        List<String> files = Collections.singletonList("src/test.js");

        JSONObject firstCommit = new JSONObject();
        firstCommit.put("id", "baccd4f9f8e4e1afde3d0f65afba458753ac7ef8");
        firstCommit.put("message", "First commit");
        firstCommit.put("timestamp", "2020-06-04T18:58:44+02:00");
        firstCommit.put("url", "test.git");
        firstCommit.put("author", JSONObjectBuilder.getAuthor());
        firstCommit.put("added", new JSONArray(files));
        firstCommit.put("removed", new JSONArray());
        firstCommit.put("modified", new JSONArray());

        JSONObject secondCommit = new JSONObject();
        secondCommit.put("id", "33e483150c69cad51380c0835c19efa51f2fcfc8");
        secondCommit.put("message", "Second commit");
        secondCommit.put("timestamp", "2020-06-04T18:58:45+02:00");
        secondCommit.put("url", "test.git");
        secondCommit.put("author", JSONObjectBuilder.getAuthor());
        secondCommit.put("added", new JSONArray());
        secondCommit.put("removed", new JSONArray());
        secondCommit.put("modified", new JSONArray(files));

        JSONArray commits = new JSONArray();
        commits.put(firstCommit);
        commits.put(secondCommit);

        body.put("commits", commits);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request = new HttpEntity<>(body.toString(), headers);
        String uri = "http://localhost:" + port + "/webhook/commit";

        ResponseEntity<String> response = template.postForEntity(uri, request, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        this.dispatcher.run();

        assertEquals(2, this.branchFileMetricResultRepository.count());

        BranchFile branchFile = this.branchFileRepository.findAll().iterator().next();
        Metric metric = this.metricRepository.findByName(CodeChurnEvent.METRIC_NAME_FILE)
            .orElseThrow();

        BranchFileMetricResult result =
            this.branchFileMetricResultRepository.findFirstByBranchFileIdAndMetricIdOrderByIdDesc(
                branchFile.getId(),
                metric.getId()
        ).orElseThrow();

        assertNotNull(result.getScore());

        Double score = 2D;
        assertEquals(score, result.getScore());
    }

    /**
     * Tests the bug that appeared when two additions of the same file happened in different
     * commits on the same branch. The code initialized two different branch files. However,
     * branch files should be uniquely identified based on branch.id and file_path. This caused
     * the code to crash.
     *
     * @throws JSONException    When the JSON cannot be properly constructed.
     */
    @Test
    public void testCommitWebhook_AdditionAppearsTwice() throws JSONException {
        JSONObject body = JSONObjectBuilder.getPushWebHook();

        body.remove("commits");

        List<String> files = Collections.singletonList("src/test.js");

        JSONObject firstCommit = new JSONObject();
        firstCommit.put("id", "baccd4f9f8e4e1afde3d0f65afba458753ac7ef8");
        firstCommit.put("message", "First commit");
        firstCommit.put("timestamp", "2020-06-04T18:58:44+02:00");
        firstCommit.put("url", "test.git");
        firstCommit.put("author", JSONObjectBuilder.getAuthor());
        firstCommit.put("added", new JSONArray(files));
        firstCommit.put("removed", new JSONArray());
        firstCommit.put("modified", new JSONArray());

        JSONObject secondCommit = new JSONObject();
        secondCommit.put("id", "33e483150c69cad51380c0835c19efa51f2fcfc8");
        secondCommit.put("message", "Second commit");
        secondCommit.put("timestamp", "2020-06-04T18:58:45+02:00");
        secondCommit.put("url", "test.git");
        secondCommit.put("author", JSONObjectBuilder.getAuthor());
        secondCommit.put("added", new JSONArray(files));
        secondCommit.put("removed", new JSONArray());
        secondCommit.put("modified", new JSONArray());

        JSONArray commits = new JSONArray();
        commits.put(firstCommit);
        commits.put(secondCommit);

        body.put("commits", commits);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request = new HttpEntity<>(body.toString(), headers);
        String uri = "http://localhost:" + port + "/webhook/commit";

        ResponseEntity<String> response = template.postForEntity(uri, request, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        this.dispatcher.run();

        assertEquals(1, this.branchFileRepository.count());

        BranchFile branchFile = this.branchFileRepository.findAll().iterator().next();
        Metric metric = this.metricRepository.findByName(CodeChurnEvent.METRIC_NAME_FILE)
                .orElseThrow();

        BranchFileMetricResult result =
            this.branchFileMetricResultRepository.findFirstByBranchFileIdAndMetricIdOrderByIdDesc(
                branchFile.getId(),
                metric.getId()
            ).orElseThrow();

        assertNotNull(result.getScore());

        Double score = 2D;
        assertEquals(score, result.getScore());
    }

    /**
     * Tests the exception thrown when the wrong webhook data is used.
     *
     * @throws JSONException When the JSON cannot be properly constructed.
     * @throws IOException   When the JSON cannot be deserialized.
     */
    @Test
    public void testWrongWebhookType() throws JSONException, IOException {
        BranchInitMock.register();

        JSONObject body = JSONObjectBuilder.getPushWebHook();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommentWebhookData data = mapper.readValue(body.toString(), CommentWebhookData.class);

        assertThrows(IllegalArgumentException.class, () -> this.event.handle(EventType.PUSH_EVENT, data));
    }
}
