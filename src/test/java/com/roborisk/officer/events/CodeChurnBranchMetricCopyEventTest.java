package com.roborisk.officer.events;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.mock.BranchInitMock;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.metrics.BranchFileMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestFileMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.BranchFileMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestFileMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestFileRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.repositories.MetricRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.JSONObjectBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests the {@link CodeChurnBranchMetricCopyEvent} event trigger.
 */
public class CodeChurnBranchMetricCopyEventTest extends OfficerTest {

    /**
     * The event to get tested.
     */
    @Autowired
    private CodeChurnBranchMetricCopyEvent event;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link MergeRequestFile} objects.
     */
    @Autowired
    private MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * Database repository to handle {@link MergeRequestReview} objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Database repository to handle
     * {@link MergeRequestFileMetricResult} objects.
     */
    @Autowired
    private MergeRequestFileMetricResultRepository mergeRequestFileMetricResultRepository;

    /**
     * Database repository to handle {@link BranchFile} objects.
     */
    @Autowired
    private BranchFileRepository branchFileRepository;

    /**
     * Database repository to handle {@link BranchFile} objects.
     */
    @Autowired
    private BranchFileMetricResultRepository branchFileMetricResultRepository;

    /**
     * Database repository to handle {@link Metric} objects.
     */
    @Autowired
    private MetricRepository metricRepository;

    /**
     * Tests the handle of the {@link CodeChurnBranchMetricCopyEvent} event.
     *
     * @throws JSONException    When the JSON Object cannot be created.
     * @throws IOException      When the JSON object cannot be deserialized.
     */
    @Test
    public void testHandle() throws JSONException, IOException {
        JSONObject body = JSONObjectBuilder.getCommentWebHook();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommentWebhookData data = mapper.readValue(body.toString(), CommentWebhookData.class);

        Repository repository = new Repository(data.getRepository());
        GitCommit commit = new GitCommit(data.getMergeRequest().getLastCommit());
        Branch source = new Branch(data.getMergeRequest().getSource(), repository, commit);
        Branch target = new Branch(data.getMergeRequest().getTarget(), repository, commit);
        MergeRequest mergeRequest = new MergeRequest(data.getMergeRequest(), source, target);

        MergeRequestReview review = new MergeRequestReview();
        review.setMergeRequest(mergeRequest);
        review.setCommit(commit);

        this.gitCommitRepository.save(commit);
        this.repositoryRepository.save(repository);
        this.branchRepository.save(source);
        this.branchRepository.save(target);
        this.mergeRequestRepository.save(mergeRequest);
        this.mergeRequestReviewRepository.save(review);

        Metric metric = this.metricRepository.findByName(CodeChurnEvent.METRIC_NAME_FILE)
                .orElseThrow();

        BranchFile file = ModelFactory.getBranchFile(source, commit);
        this.branchFileRepository.save(file);

        BranchFileMetricResult result = ModelFactory.getBranchFileMetricResult(commit, file, metric);

        this.branchFileMetricResultRepository.save(result);

        MergeRequestFile mergeRequestFile = new MergeRequestFile(file, review);
        this.mergeRequestFileRepository.save(mergeRequestFile);

        this.event.handle(EventType.REVIEW_REQUESTED, data);

        assertTrue(this.mergeRequestFileMetricResultRepository.count() > 0);
    }

    /**
     * Tests the exception thrown when the wrong webhook data is used.
     *
     * @throws JSONException When the JSON cannot be properly constructed.
     * @throws IOException   When the JSON cannot be deserialized.
     */
    @Test
    public void testWrongWebhookType() throws JSONException, IOException {
        BranchInitMock.register();

        JSONObject body = JSONObjectBuilder.getPushWebHook();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommitWebhookData data = mapper.readValue(body.toString(), CommitWebhookData.class);

        assertThrows(IllegalArgumentException.class, () -> this.event.handle(EventType.PUSH_EVENT, data));
    }
}
