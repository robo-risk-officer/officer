package com.roborisk.officer.events;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestClassObservation;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestFunctionObservation;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.metrics.MergeRequestClassMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestCodebaseMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestFileMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestFunctionMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.enums.MetricTypeEnum;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestClassMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestClassObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestCodebaseMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestFileMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestFileRepository;
import com.roborisk.officer.models.repositories.MergeRequestFunctionMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestFunctionObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.repositories.MetricRepository;
import com.roborisk.officer.models.repositories.MetricSettingsRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.JSONObjectBuilder;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Discussion;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Tests for the {@link GenerateArtefactReviewCommentEvent}.
 * The test generates results for NO_REVIEWS of reviews based on a seeded pseudo-random function.
 * Once they are generated, the test case can define how many of those should be stored in the
 * database to check different numbers of reviews.
 *
 * The test case is disabled since it is not automatically evaluated.
 */
@Disabled
public class GenerateArtefactReviewCommentEventTest extends OfficerTest {

    /**
     * Random which is used to set seeded pseudo-random values for database objects.
     */
    private Random random;

    /**
     * Number of reviews that are being generated.
     */
    private final int NO_REVIEWS = 20;

    /**
     * Number of codebase metrics for which results are being generated.
     */
    private final int NO_CODEBASE_METRICS = 0;

    /**
     * Number of codebase metrics for which results are being generated.
     */
    private final int NO_FILE_METRICS = 3;

    /**
     * Number of codebase metrics for which results are being generated.
     */
    private final int NO_CLASS_METRICS = 2;

    /**
     * Number of codebase metrics for which results are being generated.
     */
    private final int NO_FUNCTION_METRICS = 2;

    /**
     * Number of files for which results are being generated.
     */
    private final int NO_FILES = 4;

    /**
     * Number of classes per file. The size of this list should be at least NO_FILES.
     */
    private final int NO_CLASSES[] = {2, 0, 0, 0};

    /**
     * Number of classes per function. The size of this list should be at least NO_FILES.
     */
    private final int NO_FUNCTIONS[] = {1, 0, 0, 0};

    /**
     * The event to be tested.
     */
    @Autowired
    private GenerateArtefactReviewCommentEvent event;

    @Autowired
    private GenerateConclusionCommentEvent conclusionEvent;

    /**
     * Database repository to handle {@link Metric} objects.
     */
    @Autowired
    private MetricRepository metricRepository;

    /**
     * The {@link Repository} used for the tests.
     */
    private Repository repository;

    /**
     * The {@link MergeRequest} used for the tests.
     */
    private MergeRequest mergeRequest;

    /**
     * Array of all the {@link GitCommit} objects used for the test.
     */
    private List<GitCommit> commits;

    /**
     * Array of all the {@link MergeRequestReview} objects used for the test.
     */
    private List<MergeRequestReview> mergeRequestReviews;

    /**
     * Array of all the {@link MergeRequestReview} objects used for the test.
     */
    private List<MergeRequestFile> mergeRequestFiles;

    /**
     * Array of all the {@link MergeRequestReview} objects used for the test.
     */
    private List<MergeRequestClassObservation> classObservations;

    /**
     * Array of all the {@link MergeRequestReview} objects used for the test.
     */
    private List<MergeRequestFunctionObservation> functionObservations;

    /**
     * Array of all the {@link Metric} objects which are related to the codebase used for the test.
     */
    private List<Metric> codebaseMetrics;

    /**
     * Array of all the {@link Metric} objects which are related to files used for the test.
     */
    private List<Metric> fileMetrics;

    /**
     * Array of all the {@link Metric} objects which are related to classes used for the test.
     */
    private List<Metric> classMetrics;

    /**
     * Array of all the {@link Metric} objects which are related to functions used for the test.
     */
    private List<Metric> functionMetrics;

    /**
     * Array of all the {@link MetricSettings} objects used for the test.
     */
    private List<MetricSettings> metricSettings;

    /**
     * Array of all the {@link MergeRequestCodebaseMetricResult} objects used for the test.
     */
    private List<MergeRequestCodebaseMetricResult> codebaseMetricResults;

    /**
     * Array of all the {@link MergeRequestFileMetricResult} objects used for the test.
     */
    private List<MergeRequestFileMetricResult> fileMetricResults;

    /**
     * Array of all the {@link MergeRequestClassMetricResult} objects used for the test.
     */
    private List<MergeRequestClassMetricResult> classMetricResults;

    /**
     * Array of all the {@link MergeRequestFunctionMetricResult} objects used for the test.
     */
    private List<MergeRequestFunctionMetricResult> functionMetricResults;

    /**
     * {@link GitCommit} that will be used as the latest commit of the source branch.
     */
    private GitCommit sourceCommit;

    /**
     * {@link GitCommit} that will be used as the latest commit of the target branch.
     */
    private GitCommit targetCommit;

    /**
     * {@link Branch} that will be used as the source for the {@link MergeRequest}.
     */
    private Branch sourceBranch;

    /**
     * {@link Branch} that will be used as the target for the {@link MergeRequest}.
     */
    private Branch targetBranch;

    /**
     * Repository for {@link Repository}s used to add objects to the database.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Repository for {@link GitCommit}s used to add objects to the database.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Repository for {@link Branch}s used to add objects to the database.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Repository for {@link MergeRequest}s used to add objects to the database.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Repository for {@link MergeRequestReview}s used to add objects to the database.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Repository for {@link MergeRequestFile}s used to add objects to the database.
     */
    @Autowired
    private MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * Repository for {@link MergeRequestClassObservation}s used to add objects to the database.
     */
    @Autowired
    private MergeRequestClassObservationRepository mergeRequestClassObservationRepository;

    /**
     * Repository for {@link MergeRequestFunctionObservation}s used to add objects to the database.
     */
    @Autowired
    private MergeRequestFunctionObservationRepository mergeRequestFunctionObservationRepository;

    /**
     * Repository for {@link MetricSettings}s used to add objects to the database.
     */
    @Autowired
    private MetricSettingsRepository metricSettingsRepository;

    /**
     * Repository for {@link MergeRequestCodebaseMetricResult}s used to add objects to the database.
     */
    @Autowired
    private MergeRequestCodebaseMetricResultRepository mergeRequestCodebaseMetricResultRepository;

    /**
     * Repository for {@link MergeRequestFileMetricResult}s used to add objects to the database.
     */
    @Autowired
    private MergeRequestFileMetricResultRepository mergeRequestFileMetricResultRepository;

    /**
     * Repository for {@link MergeRequestClassMetricResult}s used to add objects to the database.
     */
    @Autowired
    private MergeRequestClassMetricResultRepository mergeRequestClassMetricResultRepository;

    /**
     * Repository for {@link MergeRequestFunctionMetricResult}s used to add objects to the database.
     */
    @Autowired
    private MergeRequestFunctionMetricResultRepository mergeRequestFunctionMetricResultRepository;

    /**
     * Create a repository and the needed {@link Branch} and {@link GitCommit} objects.
     *
     * @throws ParseException   When an error occurs creating the {@link GitCommit}.
     */
    private void createMergeRequestRelatedObjects() throws ParseException {
        // setup general stuff
        this.repository = ModelFactory.getRepository();
        this.repository.setGitLabId(181);
        this.repository.setNamespace("robo-risk-officer-testing");
        this.repository.setName("artefactreviewcommentevent");

        this.sourceCommit = ModelFactory.getGitCommit();
        this.targetCommit = ModelFactory.getGitCommit();

        this.sourceBranch = ModelFactory.getBranch(this.repository, this.sourceCommit);
        this.targetBranch = ModelFactory.getBranch(this.repository, this.targetCommit);

        this.mergeRequest =
            ModelFactory.getMergeRequest(this.sourceBranch, this.targetBranch);
        this.mergeRequest.setGitLabIid(1);
        this.mergeRequest.setGitLabId(214);
    }

    /**
     * Create all Merge request reviews and the associated commits.
     *
     * @throws ParseException When an exception occurs when creating the {@link GitCommit}s.
     */
    private void createAllMergeRequestReviews() throws ParseException {
        this.commits = new ArrayList<>();
        this.mergeRequestReviews = new ArrayList<>();
        // create commits and merge request reviews to which the commits are associated
        for (int i = 0; i < NO_REVIEWS; i++) {
            this.commits.add(ModelFactory.getGitCommit());

            if (i > 0) {
                this.commits.get(i).setTimestamp(this.commits.get(i - 1).getTimestamp()
                    + random.nextInt(100000) + 1000);
            }

            this.mergeRequestReviews.add(ModelFactory.getMergeRequestReview(this.commits.get(i),
                this.mergeRequest));
        }
    }

    /**
     * Create files, classes and functions (the number of which is defined at the beginning of
     * this test) per review.
     */
    private void createAllFilesClassesAndFunctions() {
        this.mergeRequestFiles = new ArrayList<>();
        this.classObservations = new ArrayList<>();
        this.functionObservations = new ArrayList<>();
        this.fileMetricResults = new ArrayList<>();
        this.classMetricResults = new ArrayList<>();
        this.functionMetricResults = new ArrayList<>();

        for (MergeRequestReview review : this.mergeRequestReviews) {
            for (int i = 0; i < NO_FILES; i++) {
                MergeRequestFile file = ModelFactory.getMergeRequestFile(review);
                file.setFileName("fileName" + i + ".java");
                file.setFilePath("filePath/fileName" + i + ".java");
                this.mergeRequestFiles.add(file);

                for (int j = 0; j < NO_CLASSES[i]; j++) {
                    MergeRequestClassObservation classObservation =
                        ModelFactory.getMergeRequestClassObservation(file);
                    classObservation.setClassName("class" + i + "" + j);
                    classObservation.setMergeRequestFile(file);
                    this.classObservations.add(classObservation);
                }

                for (int j = 0; j < NO_FUNCTIONS[i]; j++) {
                    MergeRequestFunctionObservation functionObservation =
                        ModelFactory.getMergeRequestFunctionObservation(file);
                    functionObservation.setClassName("class" + i + "" + j);
                    functionObservation.setFunctionName("function" + j);
                    functionObservation.setMergeRequestFile(file);
                    this.functionObservations.add(functionObservation);
                }
            }
        }
    }

    /**
     * Create all the {@link Metric} objects of the specified type.
     *
     * @param type      The type of which the metrics should be.
     * @param noMetrics The number of metrics that should be created.
     * @param name      The name which should preceed the metrics.
     * @param list      The {@link List} to which the created metrics should be added. Thus this
     *                  list is being extended.
     */
    private void createMetrics(MetricTypeEnum type, int noMetrics, String name,
            List<Metric> list) {
        for (int i = 0; i < noMetrics; i++) {
            list.add(ModelFactory.getMetric());
            list.get(list.size() - 1).setMetricType(type);
            list.get(list.size() - 1).setDisplayName(name + i);

            MetricSettings settings = ModelFactory.getMetricSettings(this.repository,
                list.get(list.size() - 1));
            settings.setThresholdIsUpperBound(random.nextBoolean());
            settings.setThreshold(random.nextDouble()*100);

            this.metricSettings.add(settings);
        }
    }

    /**
     * Create all the metrics and add them to the respective list.
     */
    private void createAllMetrics() {
        this.codebaseMetrics = new ArrayList<>();
        this.fileMetrics = new ArrayList<>();
        this.classMetrics = new ArrayList<>();
        this.functionMetrics = new ArrayList<>();

        this.metricSettings = new ArrayList<>();

        createMetrics(MetricTypeEnum.CODEBASE, NO_CODEBASE_METRICS, "Codebase metric ", this.codebaseMetrics);
        createMetrics(MetricTypeEnum.FILE, NO_FILE_METRICS, "File metric ", this.fileMetrics);
        createMetrics(MetricTypeEnum.CLASS, NO_CLASS_METRICS, "Class metric ", this.classMetrics);
        createMetrics(MetricTypeEnum.FUNCTION, NO_FUNCTION_METRICS, "Function metric ", this.functionMetrics);

        if (NO_FILE_METRICS > 0) {
            this.fileMetrics.get(0).setDisplayName("File Code Churn");
            this.fileMetrics.get(0).setDescription("This metric measures in how many commits the " +
                "file has changed");
            this.fileMetrics.get(0).setViewIdentifier("CodeChurnView");
        }
    }

    /**
     * Create all the {@link MergeRequestCodebaseMetricResult} objects for all required
     * observations.
     */
    private void createMetricResultsForCodebaseMetrics() {
        codebaseMetricResults = new ArrayList<>();
        for (MergeRequestReview mergeRequestReview : this.mergeRequestReviews) {
            for (MetricSettings codebaseMetricSettings : this.metricSettings) {
                if (codebaseMetricSettings.getMetric().getMetricType() == MetricTypeEnum.CODEBASE) {
                    MergeRequestCodebaseMetricResult nextResult =
                        ModelFactory.getMRCodebaseMetric(mergeRequestReview, codebaseMetricSettings.getMetric(),
                            codebaseMetricSettings);

                    nextResult.setScore(random.nextDouble() * 100);
                    nextResult.setIsLowQuality(codebaseMetricSettings
                        .isLowQuality(nextResult.getScore()));

                    codebaseMetricResults.add(nextResult);
                }
            }
        }
    }

    /**
     * Create all the {@link MergeRequestFileMetricResult} objects for all required
     * observations.
     */
    private void createMetricResultsForFileMetrics() {
        fileMetricResults = new ArrayList<>();
        for (MergeRequestFile mergeRequestFile : this.mergeRequestFiles) {
            for (MetricSettings fileMetricSettings : this.metricSettings) {
                if (fileMetricSettings.getMetric().getMetricType() == MetricTypeEnum.FILE) {
                    MergeRequestFileMetricResult nextResult =
                        ModelFactory.getMergeRequestFileMetricResult(mergeRequestFile, fileMetricSettings.getMetric(),
                            fileMetricSettings);

                    nextResult.setScore(random.nextDouble() * 100);
                    nextResult.setIsLowQuality(fileMetricSettings
                        .isLowQuality(nextResult.getScore()));

                    fileMetricResults.add(nextResult);
                }
            }
        }
    }

    /**
     * Create all the {@link MergeRequestClassMetricResult} objects for all required
     * observations.
     */
    private void createMetricResultsForClassMetrics() {
        classMetricResults = new ArrayList<>();
        for (MergeRequestClassObservation classObservation : this.classObservations) {
            for (MetricSettings classMetricSettings : this.metricSettings) {
                if (classMetricSettings.getMetric().getMetricType() == MetricTypeEnum.CLASS) {
                    MergeRequestClassMetricResult nextResult =
                        ModelFactory.getMergeRequestClassMetricResult(
                            classObservation,
                            classMetricSettings.getMetric(),
                            classMetricSettings
                        );

                    nextResult.setScore(0.0);
                    nextResult.setIsLowQuality(classMetricSettings
                            .isLowQuality(nextResult.getScore()));

                    classMetricResults.add(nextResult);
                }
            }
        }
    }

    /**
     * Create all the {@link MergeRequestFunctionMetricResult} objects for all required
     * observations.
     */
    private void createMetricResultsForFunctionMetrics() {
        functionMetricResults = new ArrayList<>();
        for (MergeRequestFunctionObservation functionObservation : this.functionObservations) {
            for (MetricSettings functionMetricSettings : this.metricSettings) {
                if (functionMetricSettings.getMetric().getMetricType() == MetricTypeEnum.FUNCTION) {
                    MergeRequestFunctionMetricResult nextResult =
                        ModelFactory.getMergeRequestFunctionMetricResult(
                            functionObservation,
                            functionMetricSettings.getMetric(),
                            functionMetricSettings
                        );

                    nextResult.setScore(random.nextDouble() * 100);
                    nextResult.setIsLowQuality(functionMetricSettings
                        .isLowQuality(nextResult.getScore()));

                    functionMetricResults.add(nextResult);
                }
            }
        }
    }

    /**
     * Create all the required metric results and add them to their respective lists.
     */
    private void createAllMetricResults() {
        this.createMetricResultsForCodebaseMetrics();
        this.createMetricResultsForFileMetrics();
        this.createMetricResultsForClassMetrics();
        this.createMetricResultsForFunctionMetrics();
    }

    /**
     * Save the {@link Repository}, {@link MergeRequest} and the {@link Branch}es +
     * {@link GitCommit} needed for those.
     */
    private void saveMergeRequestRelatedObjects() {
        this.repositoryRepository.save(this.repository);
        gitCommitRepository.save(sourceCommit);
        gitCommitRepository.save(targetCommit);
        branchRepository.save(this.sourceBranch);
        branchRepository.save(this.targetBranch);
        this.mergeRequestRepository.save(mergeRequest);
    }

    private void saveAllMetricsAndMetricSettings() {
        List<Metric> allMetrics = new ArrayList<>();
        allMetrics.addAll(this.codebaseMetrics);
        allMetrics.addAll(this.fileMetrics);
        allMetrics.addAll(this.classMetrics);
        allMetrics.addAll(this.functionMetrics);

        for (Metric metric : allMetrics) {
            this.metricRepository.save(metric);
        }

        for (MetricSettings setting : this.metricSettings) {
            this.metricSettingsRepository.save(setting);
        }
    }

    /**
     * Save the first noReviews {@link MergeRequestReview} objects in the database.s
     * @param noReviews
     */
    private void saveMergeRequestReviewsAndAssociatedResults(int noReviews) {
        for (int i = 0; i < noReviews; i++) {
            this.gitCommitRepository.save(this.commits.get(i));
            this.mergeRequestReviewRepository.save(this.mergeRequestReviews.get(i));
        }
    }

    /**
     * Save all the {@link MergeRequestFile}, {@link MergeRequestClassObservation} and
     * {@link MergeRequestFunctionObservation} objects that are associated to
     * {@link MergeRequestReview}s that are already stored in the
     * database.
     */
    private void saveFilesClassesAndFunction() {
        // save all files associated to reviews that have been saved
        for (MergeRequestFile file : this.mergeRequestFiles) {
            // check if the review has been saved in the DB
            if (file.getMergeRequestReview().getId() != null) {
                this.mergeRequestFileRepository.save(file);
            }
        }

        // save all classes associated to files that have been saved
        for (MergeRequestClassObservation current : this.classObservations) {
            // check if the file has been saved in the DB
            if (current.getMergeRequestFile().getId() != null) {
                this.mergeRequestClassObservationRepository.save(current);
            }
        }

        // save all functions associated to files that have been saved
        for (MergeRequestFunctionObservation current : this.functionObservations) {
            // check if the file has been saved in the DB
            if (current.getMergeRequestFile().getId() != null) {
                this.mergeRequestFunctionObservationRepository.save(current);
            }
        }
    }

    /**
     * Save all the metric results which are associated to observations that are already stored
     * in the database.
     */
    private void saveMetricResults() {
        for (MergeRequestCodebaseMetricResult result : this.codebaseMetricResults) {
            // check if the review has been saved in the DB
            if (result.getMergeRequestReview().getId() != null) {
                this.mergeRequestCodebaseMetricResultRepository.save(result);
            }
        }

        for (MergeRequestFileMetricResult result : this.fileMetricResults) {
            // check if the file has been saved in the DB
            if (result.getMergeRequestFile().getId() != null) {
                this.mergeRequestFileMetricResultRepository.save(result);
            }
        }

        for (MergeRequestClassMetricResult result : this.classMetricResults) {
            // check if the observation has been saved in the DB
            if (result.getMergeRequestClassObservation().getId() != null) {
                this.mergeRequestClassMetricResultRepository.save(result);
            }
        }

        for (MergeRequestFunctionMetricResult result : this.functionMetricResults) {
            // check if the observation has been saved in the DB
            if (result.getMergeRequestFunctionObservation().getId() != null) {
                this.mergeRequestFunctionMetricResultRepository.save(result);
            }
        }
    }


    /**
     * Function to create all the required data and save the data that definitely should be saved.
     *
     * @throws ParseException   If any errors occur in the object creation.
     */
    @BeforeEach
    private void createAndSaveDB() throws ParseException {
        this.random = new Random(2);

        // create all objects that are always needed.
        this.createMergeRequestRelatedObjects();
        this.createAllMetrics();

        // save all objects that are always needed.
        this.saveMergeRequestRelatedObjects();
        this.saveAllMetricsAndMetricSettings();

        // create all objects that are sometimes needed.
        this.createAllMergeRequestReviews();
        this.createAllFilesClassesAndFunctions();
        this.createAllMetricResults();
    }

    /**
     * Test the correct functioning of the {@link GenerateArtefactReviewCommentEvent} by calling it
     * with valid data.
     */
    @Test
    public void testWithGenerateArtefactReviewCommentEvent() throws JSONException, IOException, InterruptedException {
        this.saveMergeRequestReviewsAndAssociatedResults(10);
        this.saveFilesClassesAndFunction();
        this.saveMetricResults();

        JSONObject body = JSONObjectBuilder.getCommentWebHook();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommentWebhookData data = mapper.readValue(body.toString(), CommentWebhookData.class);

        data.getMergeRequest().setGitLabId(214);


        MergeRequestReview latestReview =
                this.mergeRequestReviewRepository
                    .findFirstByMergeRequestOrderByIdDesc(this.mergeRequest).get();
        List<MergeRequestFile> files =
            this.mergeRequestFileRepository
                .findAllByMergeRequestReviewId(latestReview.getId());

        List<MergeRequestFileMetricResult> results =
            this.mergeRequestFileMetricResultRepository.findAllByMergeRequestFile(files.get(0));

        double previousScore = results.get(0).getScore();
        results.get(0).setScore(null);
        this.mergeRequestFileMetricResultRepository.save(results.get(0));


        this.event.handle(EventType.REVIEW_REQUESTED, data);
        this.conclusionEvent.handle(EventType.REVIEW_REQUESTED, data);

        System.out.println("Everything is posted now.");
        System.out.println("We are now waiting.");

        int waitSeconds = 15;

        for (int i = 0; i < waitSeconds; i++) {
            Thread.sleep(1000);
            System.out.println("waiting " + i + " out of " + waitSeconds);
        }

        results.get(0).setScore(previousScore);
        this.mergeRequestFileMetricResultRepository.save(results.get(0));

        this.event.handle(EventType.REVIEW_REQUESTED, data);
    }

    /**
     * GitLabWrapper which is used to clean the comments in the merge request. This is only down
     * here as it will only be used in that "test" which is executed manually.
     */
    @Autowired GitLabApiWrapper gitLabApiWrapper;

    /**
     * Test which grabs all the threads in the merge request discussion and deletes them.
     *
     * @throws GitLabApiException If any exception with the GitLabApi occur.
     */
    @Test
    public void cleanUpComments() throws GitLabApiException {
        List<Discussion> discussions =
            this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi().getMergeRequestDiscussions(181, 1);


        for (Discussion discussion : discussions) {
            this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi().deleteMergeRequestThreadNote(181, 1, discussion.getId(), discussion.getNotes().get(0).getId());
        }
    }

}
