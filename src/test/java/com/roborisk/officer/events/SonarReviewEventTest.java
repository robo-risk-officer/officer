package com.roborisk.officer.events;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.web.CommitWebhookData;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Class providing tests for the {@link SonarReviewEvent} class.
 */
public class SonarReviewEventTest extends OfficerTest {

    /**
     * Local instance of the {@link SonarReviewEvent} class.
     */
    @Autowired
    SonarReviewEvent sonarReviewEvent;

    /**
     * Test for checking whether the correct exception is thrown if a AbstractWebhookData of the
     * wrong type is supplied.
     */
    @Test
    void handleWrongAbstractWebhookData() {
        CommitWebhookData data = new CommitWebhookData();

        assertThrows(IllegalArgumentException.class, () -> {
            sonarReviewEvent.handle(EventType.REVIEW_REQUESTED, data);
        });
    }
}
