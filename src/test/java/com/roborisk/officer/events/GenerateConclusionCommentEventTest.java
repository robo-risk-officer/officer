package com.roborisk.officer.events;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.*;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.InternalSonarWebhookData;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class with tests for the {@link GenerateConclusionCommentEvent} class.
 */
public class GenerateConclusionCommentEventTest extends OfficerTest {

    /**
     * Access point for the {@link GenerateArtefactReviewCommentEvent}.
     */
    @Autowired
    GenerateConclusionCommentEvent generateConclusionCommentEvent;

    /**
     * Repository for handling {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Repository for handling {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * Repository for handling {@link Branch} objects
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Repository for handling {@link MergeRequest} objects.
     */
    @Autowired
    MergeRequestRepository mergeRequestRepository;

    /**
     * Test for checking that the correct error is thrown if an unsupported type of
     * AbstractWebhookData is passed as an argument.
     */
    @Test
    void handleNotCommentWebhookData() {
        CommitWebhookData commitWebhookData = new CommitWebhookData();

        // Submit commitWebhookData
        try {
            generateConclusionCommentEvent.handle(EventType.REVIEW_REQUESTED, commitWebhookData);
            // Exception should have been thrown
            fail();
        } catch (IllegalArgumentException e) {
            // Check that the correct exception has been thrown
            assertEquals("GenerateConclusionCommentEvent.handle: " +
                "Only supports CommentWebhookData and InternalSonarWebhookData.", e.getMessage());
        }
    }

    /**
     * Test for checking that the correct error is thrown if the merge request associated to the
     * CommentWebhookData is not found in the database.
     */
    @Test
    void handleCommentWebhookDataMRNotFound() throws ParseException, IOException {
        CommentWebhookData commentWebhookData = new CommentWebhookData();

        // Maps representing the JSON
        Map<String, Object> authormap = new LinkedHashMap<>();
        Map<String, Object> commitmap = new LinkedHashMap<>();
        Map<String, Object> map = new LinkedHashMap<>();

        // Add random data
        authormap.put("name", "name");
        authormap.put("email", "email");

        // Add more random data
        commitmap.put("id", "hashsishe");
        commitmap.put("message", "some message");
        commitmap.put("modified", new ArrayList<>());
        commitmap.put("removed", new ArrayList<>());
        commitmap.put("added", new ArrayList<>());
        commitmap.put("timestamp", "2015-04-08T21:00:25-07:00");
        commitmap.put("author", authormap);

        // Add more more random data
        map.put("id", 1);
        map.put("iid", 2);
        map.put("target_branch", "target");
        map.put("source_branch", "source");
        map.put("author_id", 4);
        map.put("title", "Some merge request");
        map.put("created_at", "2015-04-08T21:00:25-07:00");
        map.put("updated_at", "2015-04-08T21:00:25-07:00");
        map.put("state", "merged");
        map.put("action", "marked");
        map.put("merge_status", "can_be_merged");
        map.put("work_in_progress", false);
        map.put("last_commit", commitmap);

        commentWebhookData.setMergeRequest(map);

        // Submit commentWebhookData
        try {
            generateConclusionCommentEvent.handle(EventType.REVIEW_REQUESTED, commentWebhookData);
            // Exception should have been thrown
            fail();
        } catch (IllegalStateException e) {
            // Check that the correct exception is thrown
            assertTrue(e.getMessage().startsWith("GenerateConclusionCommentEvent.handle: Could not " +
                "find MergeRequest with GitLab ID"));
        }
    }

    /**
     * Test for checking that the correct error is thrown if the merge request associated to the
     * InternalSonarWebhookData is not found in the database.
     */
    @Test
    void handleInternalSonarWebhookDataMRNotFound() throws ParseException {
        InternalSonarWebhookData internalSonarWebhookData = new InternalSonarWebhookData();

        // Add random data
        // Don't save to database
        Repository repository = ModelFactory.getRepository();
        GitCommit gitCommit = ModelFactory.getGitCommit();
        Branch sourceBranch = ModelFactory.getBranch(repository, gitCommit);
        Branch targetBranch = ModelFactory.getBranch(repository, gitCommit);
        MergeRequest mergeRequest = ModelFactory.getMergeRequest(sourceBranch, targetBranch);

        MergeRequestReview mergeRequestReview = ModelFactory.getMergeRequestReview(gitCommit, mergeRequest);

        SonarAnalysisTracker sonarAnalysisTracker = new SonarAnalysisTracker();
        sonarAnalysisTracker.setMergeRequestReview(mergeRequestReview);

        internalSonarWebhookData.setSourceBranchAnalysis(sonarAnalysisTracker);

        // Submit internalSonarWebhookData
        try {
            generateConclusionCommentEvent.handle(EventType.REVIEW_REQUESTED, internalSonarWebhookData);
            // Exception should have been thrown
            fail();
        } catch (IllegalStateException e) {
            // Check that the correct exception is thrown
            assertTrue(e.getMessage().startsWith("GenerateConclusionCommentEvent.handle: Could not " +
                "find MergeRequest with GitLab ID"));
        }
    }

    /**
     * Test for checking that the correct error is thrown if the merge request review cannot be found
     * in the database.
     */
    @Test
    void handleMRReviewNotFound() throws ParseException {
        InternalSonarWebhookData internalSonarWebhookData = new InternalSonarWebhookData();

        // Add random data
        // Also save to database
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        GitCommit gitCommit = ModelFactory.getGitCommit();
        gitCommitRepository.save(gitCommit);

        Branch sourceBranch = ModelFactory.getBranch(repository, gitCommit);
        branchRepository.save(sourceBranch);
        Branch targetBranch = ModelFactory.getBranch(repository, gitCommit);
        branchRepository.save(targetBranch);

        MergeRequest mergeRequest = ModelFactory.getMergeRequest(sourceBranch, targetBranch);
        mergeRequestRepository.save(mergeRequest);

        // Do not save MergeRequestReview to database
        MergeRequestReview mergeRequestReview = ModelFactory.getMergeRequestReview(gitCommit, mergeRequest);

        SonarAnalysisTracker sonarAnalysisTracker = new SonarAnalysisTracker();
        sonarAnalysisTracker.setMergeRequestReview(mergeRequestReview);

        internalSonarWebhookData.setSourceBranchAnalysis(sonarAnalysisTracker);

        // Submit internalSonarWebhookData
        try {
            generateConclusionCommentEvent.handle(EventType.REVIEW_REQUESTED, internalSonarWebhookData);
            // Exception should have been thrown
            fail();
        } catch (IllegalStateException e) {
            // Check that the correct exception has been thrown
            assertTrue(e.getMessage().startsWith("GenerateConclusionCommentEvent.handle: Cannot find "
                + "MergeRequestReview with MergeRequest that has ID"));
        }
    }
}
