package com.roborisk.officer.events;

/**
 * Simple implementation of a pair.
 *
 * @param <T> The class type of the key.
 * @param <G> The class type of the value.
 */
public class Pair<T, G> {
    /**
     * The key of the pair.
     */
    private T key;

    /**
     * The value of the pair.
     */
    private G value;

    /**
     * Constructs a pair.
     *
     * @param key   The key of the pair.
     * @param value The value of the pair.
     */
    public Pair(T key, G value) {
        this.key = key;
        this.value = value;
    }

    /**
     * Get the key.
     *
     * @return  Return the key of the pair.
     */
    public T getKey() {
        return this.key;
    }

    /**
     * Get the value.
     *
     * @return  Return the value of the pair.
     */
    public G getValue() {
        return this.value;
    }
}
