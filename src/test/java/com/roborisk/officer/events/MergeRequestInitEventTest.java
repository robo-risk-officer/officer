package com.roborisk.officer.events;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.mock.BranchInitMock;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.enums.EventType;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.JSONObjectBuilder;
import com.roborisk.officer.models.web.MergeRequestWebhookData;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * This class contains tests for the {@link MergeRequestInitEvent} trigger.
 */
public class MergeRequestInitEventTest extends OfficerTest {

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * The event to test.
     */
    @Autowired
    private MergeRequestInitEvent event;

    /**
     * Tests the handling of the {@link MergeRequestInitEvent} event
     * when sending a {@link com.roborisk.officer.models.web.MergeRequestWebhookData}.
     */
    @Test
    public void testHandler_MergeRequestWebhookData() throws JSONException, IOException {
        BranchInitMock.register();

        JSONObject object = JSONObjectBuilder.getMergeRequestWebHook();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        MergeRequestWebhookData data = mapper.readValue(object.toString(), MergeRequestWebhookData.class);

        Repository repository = new Repository(data.getRepository());
        GitCommit commit = new GitCommit(data.getMergeRequest().getLastCommit());
        Branch source = new Branch(data.getMergeRequest().getSource(), repository, commit);
        Branch target = new Branch(data.getMergeRequest().getTarget(), repository, commit);

        this.repositoryRepository.save(repository);
        this.gitCommitRepository.save(commit);
        this.branchRepository.save(source);
        this.branchRepository.save(target);

        this.event.handle(EventType.MERGE_REQUEST_CHANGED, data);

        // All we care about it if they are stored.
        assertEquals(1, this.mergeRequestRepository.count());
    }

    /**
     * Tests the handling of the {@link MergeRequestInitEvent} event
     * when sending a {@link CommentWebhookData}.
     */
    @Test
    public void testHandler_CommentWebhookData() throws JSONException, IOException {
        BranchInitMock.register();

        JSONObject object = JSONObjectBuilder.getCommentWebHook();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        CommentWebhookData data = mapper.readValue(object.toString(), CommentWebhookData.class);

        Repository repository = new Repository(data.getRepository());
        GitCommit commit = new GitCommit(data.getMergeRequest().getLastCommit());
        Branch source = new Branch(data.getMergeRequest().getSource(), repository, commit);
        Branch target = new Branch(data.getMergeRequest().getTarget(), repository, commit);

        this.repositoryRepository.save(repository);
        this.gitCommitRepository.save(commit);
        this.branchRepository.save(source);
        this.branchRepository.save(target);

        this.event.handle(EventType.REVIEW_REQUESTED, data);

        // All we care about it if they are stored.
        assertEquals(1, this.mergeRequestRepository.count());
    }
}
