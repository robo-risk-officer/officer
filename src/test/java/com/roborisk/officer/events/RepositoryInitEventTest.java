package com.roborisk.officer.events;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.RobotSettings;
import com.roborisk.officer.models.repositories.MetricSettingsRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.repositories.RobotSettingsRepository;
import com.roborisk.officer.models.web.JSONObjectBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * This class contains tests for the {@link RepositoryInitEvent} trigger.
 */
public class RepositoryInitEventTest extends OfficerTest {

    /**
     * Http client.
     */
    @Autowired
    private TestRestTemplate template;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link RobotSettings} objects.
     */
    @Autowired
    private RobotSettingsRepository robotSettingsRepository;

    /**
     * Database repository to handle {@link MetricSettings} objects.
     */
    @Autowired
    private MetricSettingsRepository metricSettingsRepository;

    /**
     * Tests the addition of new repositories and default settings when
     * they are seen for the first time. The test is based on the merge request changed webhook.
     *
     * @throws JSONException When the JSON cannot be properly constructed.
     */
    @Test
    public void testMergeRequestWebhookEvent() throws JSONException {
        RobotSettings _default = this.robotSettingsRepository.getDefault()
            .orElse(ModelFactory.getRobotSettings(null));

        JSONObject body = JSONObjectBuilder.getMergeRequestWebHook();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request = new HttpEntity<>(body.toString(), headers);
        String uri = "http://localhost:" + port + "/webhook/merge-request";

        ResponseEntity<String> response = template.postForEntity(uri, request, String.class);

        this.dispatcher.run();

        assertEquals(HttpStatus.OK, response.getStatusCode());

        Integer project_id = ((JSONObject) body.get("project")).getInt("id");

        Optional<Repository> optional = this.repositoryRepository.findByGitLabId(project_id);

        if (optional.isEmpty()) {
            fail("New repository is not saved!");
        }

        Repository repository = optional.get();
        Optional<RobotSettings> optionalClone = this.robotSettingsRepository.findByRepositoryId(repository.getId());

        if (optionalClone.isEmpty()) {
            fail("Settings are not properly stored!");
        }

        RobotSettings settings = optionalClone.get();

        assertNotEquals(_default.getId(), settings.getId());
        assertEquals(_default.getAutomaticReviewOnNewCommit(), settings.getAutomaticReviewOnNewCommit());
        assertEquals(_default.getAutomaticReviewOnNewMr(), settings.getAutomaticReviewOnNewMr());
        assertEquals(_default.getFalsePositiveThreshold(), settings.getFalsePositiveThreshold());
        assertEquals(_default.getReviewHistoryLength(), settings.getReviewHistoryLength());

        List<MetricSettings> defaultSettings = this.metricSettingsRepository.findByRepositoryId(null)
            .orElseThrow(() -> new IllegalStateException("Default MetricSettings have not been seeded."));

        Optional<List<MetricSettings>> optionalMetricSettings = this.metricSettingsRepository
            .findByRepositoryId(repository.getId());

        if (optionalMetricSettings.isEmpty()) {
            fail("No MetricSettings are linked to the new repository.");
        }

        List<MetricSettings> repositoryMetricSettings = optionalMetricSettings.get();

        assertEquals(repositoryMetricSettings.size(), defaultSettings.size());

        for (MetricSettings defaultSetting : defaultSettings) {
            boolean anyMatch = repositoryMetricSettings.stream()
                .anyMatch(setting ->
                    setting.equals(defaultSetting));

            assertTrue(anyMatch);
        }
    }
}
