package com.roborisk.officer.scheduler;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.mock.CommentHelperMock;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests the {@link AwardScraperScheduledTask}.
 */
public class AwardScraperScheduledTaskTest extends OfficerTest {

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    @Autowired
    private AwardScraperScheduledTask task;

    /**
     * Test scrape awards. Does not assert anything, checks execution success.
     */
    @Test
    public void testScrapeAwards() throws ParseException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        CommentHelperMock.register();

        GitCommit commit = ModelFactory.getGitCommit();
        Repository repository = ModelFactory.getRepository();
        repository.setGitLabId(176);

        Branch branch = ModelFactory.getBranch(repository, commit);
        MergeRequest mergeRequest = ModelFactory.getMergeRequest(branch, branch);
        mergeRequest.setState("opened");
        mergeRequest.setGitLabId(1);
        mergeRequest.setGitLabIid(1);

        this.repositoryRepository.save(repository);
        this.gitCommitRepository.save(commit);
        this.branchRepository.save(branch);
        this.mergeRequestRepository.save(mergeRequest);

        Method method = this.task.getClass().getDeclaredMethod("scrapeAwards");
        method.setAccessible(true);
        method.invoke(this.task);
    }
}
