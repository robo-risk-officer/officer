package com.roborisk.officer.filters;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;

public class CachingInputStreamTest {

    /**
     * Method for testing constructor and reading of CachingInputStream.
     *
     * @throws IOException if reading CachingInputStream throws IOException.
     */
    @Test
    void testCachingInputStream() throws IOException {
        // Construct testing data.
        String testString = "Important data";
        byte[] data = testString.getBytes();

        // Initialize CachingInputStream with testing data.
        CachingInputStream cachingInputStream = new CachingInputStream(data);

        // Initialize reader for cachingInputStream.
        InputStreamReader inputStreamReader1;
        inputStreamReader1 = new InputStreamReader(cachingInputStream, StandardCharsets.UTF_8);

        // Read content of CachingInputStream.
        String readContent = new BufferedReader(inputStreamReader1).readLine();

        // Evaluate read content.
        assertEquals(testString, readContent);
    }
}
