package com.roborisk.officer.filters;

import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * This class contains tests for the Caching Request Wrapper.
 */
public class CachingRequestWrapperTest {

    /**
     * Test for getReader() function of the CachingRequestWrapper class.
     *
     * @throws IOException if CachingRequestWrapper throws IOException.
     */
    @Test
    void CachingRequestWrapperGetReaderTest() throws IOException {

        // Create mock request.
        String testingData = "Mock request";
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setContent(testingData.getBytes());

        // Wrap mock request in CachingRequestWrapper.
        CachingRequestWrapper wrappedRequest = new CachingRequestWrapper(request);

        // Read content of request for the first time.
        String readContent1 = wrappedRequest.getReader().readLine();

        // Evaluate first read content.
        assertEquals(testingData, readContent1);

        // Read content of request for the second time.
        String readContent2 = wrappedRequest.getReader().readLine();

        /// Evaluate second read content.
        assertEquals(testingData, readContent2);
    }

    /**
     * Test for getInputStream() function of the CachingRequestWrapper class.
     *
     * @throws IOException if CachingRequestWrapper throws IOException.
     */
    @Test
    void CachingRequestWrapperGetInputStreamTest() throws IOException {

        // Create mock request.
        String testingData = "Mock request";
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setContent(testingData.getBytes());

        // Wrap mock request in CachingRequestWrapper.
        CachingRequestWrapper wrappedRequest = new CachingRequestWrapper(request);

        // Retrieve first reader for InputStream.
        InputStream inputStream1 = wrappedRequest.getInputStream();
        InputStreamReader inputStreamReader1;
        inputStreamReader1 = new InputStreamReader(inputStream1, StandardCharsets.UTF_8);

        // Read content of first InputStream.
        String readContent1 = new BufferedReader(inputStreamReader1).readLine();

        // Evaluate first read content.
        assertEquals(testingData, readContent1);

        // Retrieve second reader for InputStream.
        InputStream inputStream2 = wrappedRequest.getInputStream();
        InputStreamReader inputStreamReader2;
        inputStreamReader2 = new InputStreamReader(inputStream2, StandardCharsets.UTF_8);

        // Read content of second InputStream.
        String readContent2 = new BufferedReader(inputStreamReader2).readLine();

        // Evaluate second read content.
        assertEquals(testingData, readContent2);
    }
}
