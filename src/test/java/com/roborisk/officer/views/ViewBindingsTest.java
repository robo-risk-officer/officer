package com.roborisk.officer.views;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.metrics.*;
import com.roborisk.officer.models.enums.MetricTypeEnum;
import com.roborisk.officer.views.metrics.CodeChurnView;
import com.roborisk.officer.views.metrics.GeneralMetricEvaluationView;
import com.roborisk.officer.views.metrics.SonarVulnerabilitiesView;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Class providing tests for the {@link ViewBindings} class.
 */
public class ViewBindingsTest extends OfficerTest {

    /**
     * Singleton instance of the {@link ViewBindings} class.
     */
    @Autowired
    ViewBindings viewBindings;

    /**
     * Test to check that the correct view is returned for the code churn metric for files.
     */
    @Test
    void getViewForMetricCodeChurnFile() {
        Metric metric = ModelFactory.getMetric();
        metric.setViewIdentifier("CodeChurnView");
        metric.setMetricType(MetricTypeEnum.FILE);

        int gitLabId = 13;

        GeneralMetricEvaluationView returned = viewBindings.getViewForMetric(metric, gitLabId);

        assertThat(returned instanceof CodeChurnView);
    }

    /**
     * Test to check that the correct view is returned for the code churn metric for classes.
     */
    @Test
    void getViewForMetricCodeChurnClass() {
        Metric metric = ModelFactory.getMetric();
        metric.setViewIdentifier("CodeChurnView");
        metric.setMetricType(MetricTypeEnum.CLASS);

        int gitLabId = 13;

        GeneralMetricEvaluationView returned = viewBindings.getViewForMetric(metric, gitLabId);

        assertThat(returned instanceof CodeChurnView);
    }

    /**
     * Test to check that the correct view is returned for the code churn metric for functions.
     */
    @Test
    void getViewForMetricCodeChurnFunction() {
        Metric metric = ModelFactory.getMetric();
        metric.setViewIdentifier("CodeChurnView");
        metric.setMetricType(MetricTypeEnum.FUNCTION);

        int gitLabId = 13;

        GeneralMetricEvaluationView returned = viewBindings.getViewForMetric(metric, gitLabId);

        assertThat(returned instanceof CodeChurnView);
    }

    /**
     * Test to check that the correct error is thrown for the code churn metric for codebases.
     */
    @Test
    void getViewForMetricCodeChurnCodebase() {
        Metric metric = ModelFactory.getMetric();
        metric.setViewIdentifier("CodeChurnView");
        metric.setMetricType(MetricTypeEnum.CODEBASE);

        int gitLabId = 13;

        try {
            viewBindings.getViewForMetric(metric, gitLabId);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().startsWith("ViewBindings.createCodeChurnView: CodeChurnView requested for unsupported type"));
        }
    }

    /**
     * Test to check that the correct view is returned for the SonarQube metrics.
     */
    @Test
    void getViewForMetricSonar() {
        Metric metric = ModelFactory.getMetric();
        metric.setViewIdentifier("SonarVulnerabilitiesView");
        metric.setMetricType(MetricTypeEnum.FILE);

        int gitLabId = 5923424;

        GeneralMetricEvaluationView returned = viewBindings.getViewForMetric(metric, gitLabId);

        assertThat(returned instanceof SonarVulnerabilitiesView);
    }

    /**
     * Test to check that the correct error is thrown if the SonarQube metrics are requested on a metrictype other than file.
     */
    @Test
    void getViewForMetricSonarWrongMetricType() {
        Metric metric = ModelFactory.getMetric();
        metric.setViewIdentifier("SonarVulnerabilitiesView");
        metric.setMetricType(MetricTypeEnum.CODEBASE);

        int gitLabId = 31415926;

        try {
            viewBindings.getViewForMetric(metric, gitLabId);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().startsWith("ViewBindings.getViewForMetric: Cannot return SonarVulnerabilitiesView for MetricTypeEnum"));
        }
    }

    /**
     * Test to check that a view is correctly returned if a metric without a ViewIdentifier is passed as an argument.
     */
    @Test
    void getViewForMetricDefault() {
        Metric metric = ModelFactory.getMetric();

        int gitLabId = 0101010101;

        GeneralMetricEvaluationView returned = viewBindings.getViewForMetric(metric, gitLabId);
    }

    /**
     * Test to check that the correct view is returned for a GeneralMetricReevalutaionView for files.
     */
    @Test
    void getViewForMetricGeneralFile() {
        Metric metric = ModelFactory.getMetric();
        metric.setViewIdentifier("GeneralMetricReevaluationView");
        metric.setMetricType(MetricTypeEnum.FILE);

        int gitLabId = 42;

        GeneralMetricEvaluationView<MergeRequestFileMetricResult> returned = viewBindings.getViewForMetric(metric, gitLabId);
    }

    /**
     * Test to check that the correct view is returned for a GeneralMetricReevalutaionView for functions.
     */
    @Test
    void getViewForMetricGeneralFunction() {
        Metric metric = ModelFactory.getMetric();
        metric.setViewIdentifier("GeneralMetricReevaluationView");
        metric.setMetricType(MetricTypeEnum.FUNCTION);

        int gitLabId = 42;

        GeneralMetricEvaluationView<MergeRequestFunctionMetricResult> returned = viewBindings.getViewForMetric(metric, gitLabId);
    }

    /**
     * Test to check that the correct view is returned for a GeneralMetricReevalutaionView for classes.
     */
    @Test
    void getViewForMetricGeneralClass() {
        Metric metric = ModelFactory.getMetric();
        metric.setViewIdentifier("GeneralMetricReevaluationView");
        metric.setMetricType(MetricTypeEnum.CLASS);

        int gitLabId = 42;

        GeneralMetricEvaluationView<MergeRequestClassMetricResult> returned = viewBindings.getViewForMetric(metric, gitLabId);
    }

    /**
     * Test to check that the correct view is returned for a GeneralMetricReevalutaionView for codebases.
     */
    @Test
    void getViewForMetricGeneralCodebase() {
        Metric metric = ModelFactory.getMetric();
        metric.setViewIdentifier("GeneralMetricReevaluationView");
        metric.setMetricType(MetricTypeEnum.CODEBASE);

        int gitLabId = 42;

        GeneralMetricEvaluationView<MergeRequestCodebaseMetricResult> returned = viewBindings.getViewForMetric(metric, gitLabId);
    }
}
