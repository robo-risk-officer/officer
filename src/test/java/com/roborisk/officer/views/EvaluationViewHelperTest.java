package com.roborisk.officer.views;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.*;
import com.roborisk.officer.models.database.metrics.*;
import com.roborisk.officer.models.repositories.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class providing tests for the {@link EvaluationViewHelper} class.
 */
public class EvaluationViewHelperTest extends OfficerTest {

    /**
     * Repository for handling {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Repository for handling {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * Repository for handling {@link Branch} objects.
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Repository for handling {@link MergeRequest} objects.
     */
    @Autowired
    MergeRequestRepository mergeRequestRepository;

    /**
     * Repository for handling {@link MergeRequestReview} objects.
     */
    @Autowired
    MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Repository for handling {@link MergeRequestCodebaseMetricResult} objects.
     */
    @Autowired
    MergeRequestCodebaseMetricResultRepository mergeRequestCodebaseMetricResultRepository;

    /**
     * Repository for handling {@link MergeRequestFileMetricResult} objects.
     */
    @Autowired
    MergeRequestFileMetricResultRepository mergeRequestFileMetricResultRepository;

    /**
     * Repository for handling {@link MergeRequestClassMetricResult} objects.
     */
    @Autowired
    MergeRequestClassMetricResultRepository mergeRequestClassMetricResultRepository;

    /**
     * Repository for handling {@link MergeRequestFunctionMetricResult} objects.
     */
    @Autowired
    MergeRequestFunctionMetricResultRepository mergeRequestFunctionMetricResultRepository;

    /**
     * Repository for handling {@link Metric} objects.
     */
    @Autowired
    MetricRepository metricRepository;

    /**
     * Repository for handling {@link MergeRequestFile} objects.
     */
    @Autowired
    MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * Repository for handling {@link MergeRequestClassObservationRepository} objects.
     */
    @Autowired
    MergeRequestClassObservationRepository mergeRequestClassObservationRepository;

    /**
     * Repository for handling {@link MergeRequestFunctionObservation} objects.
     */
    @Autowired
    MergeRequestFunctionObservationRepository mergeRequestFunctionObservationRepository;

    /**
     * Singleton instance of the {@link EvaluationViewHelper} class.
     */
    @Autowired
    EvaluationViewHelper evaluationViewHelper;

    /**
     * Local instance of the {@link MergeRequestReview} objects.
     */
    MergeRequestReview mergeRequestReview;

    /**
     * Local instance of the {@link Metric} objects.
     */
    Metric metric;

    /**
     * Method for setting up the basic objects used in the tests.
     *
     * @throws ParseException
     */
    @BeforeEach
    void setup() throws ParseException {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        GitCommit gitCommit = ModelFactory.getGitCommit();
        gitCommitRepository.save(gitCommit);

        Branch sourceBranch = ModelFactory.getBranch(repository, gitCommit);
        Branch targetBranch = ModelFactory.getBranch(repository, gitCommit);
        branchRepository.save(sourceBranch);
        branchRepository.save(targetBranch);

        MergeRequest mergeRequest = ModelFactory.getMergeRequest(sourceBranch, targetBranch);
        mergeRequestRepository.save(mergeRequest);

        this.mergeRequestReview = ModelFactory.getMergeRequestReview(gitCommit, mergeRequest);
        mergeRequestReviewRepository.save(this.mergeRequestReview);

        this.metric = ModelFactory.getMetric();
        metricRepository.save(metric);
    }

    /**
     * Test for checking the functionality if nothing is calculating.
     */
    @Test
    void mergeRequestIsCalculatingAllFalse() {
        assertFalse(evaluationViewHelper.mergeRequestReviewIsCalculating(this.mergeRequestReview));
    }

    /**
     * Test for checking the functionality if a codebase metric is still calculating.
     */
    @Test
    void mergeRequestIsCalculatingCodebase() {
        MergeRequestCodebaseMetricResult result = new MergeRequestCodebaseMetricResult();
        result.setMergeRequestReview(this.mergeRequestReview);
        result.setScore(null);
        result.setIsLowQuality(true);
        result.setMetric(this.metric);

        mergeRequestCodebaseMetricResultRepository.save(result);

        assertTrue(evaluationViewHelper.mergeRequestReviewIsCalculating(this.mergeRequestReview));

        mergeRequestCodebaseMetricResultRepository.delete(result);
    }

    /**
     * Test for checking the functionality if a file metric is still calculating.
     */
    @Test
    void mergeRequestIsCalculatingFile() {
        MergeRequestFileMetricResult result = new MergeRequestFileMetricResult();

        MergeRequestFile mergeRequestFile = ModelFactory.getMergeRequestFile(this.mergeRequestReview);

        result.setMergeRequestFile(mergeRequestFile);
        result.setScore(null);
        result.setIsLowQuality(true);
        result.setMetric(this.metric);

        mergeRequestFileRepository.save(mergeRequestFile);
        mergeRequestFileMetricResultRepository.save(result);

        assertTrue(evaluationViewHelper.mergeRequestReviewIsCalculating(this.mergeRequestReview));

        mergeRequestFileMetricResultRepository.delete(result);
        mergeRequestFileRepository.delete(mergeRequestFile);
    }

    /**
     * Test for checking the functionality if a class metric is still calculating.
     */
    @Test
    void mergeRequestIsCalculatingClass() {
        MergeRequestClassMetricResult result = new MergeRequestClassMetricResult();

        MergeRequestFile mergeRequestFile = ModelFactory.getMergeRequestFile(this.mergeRequestReview);
        MergeRequestClassObservation mergeRequestClassObservation = ModelFactory.getMergeRequestClassObservation(mergeRequestFile);

        result.setMergeRequestClassObservation(mergeRequestClassObservation);
        result.setScore(null);
        result.setIsLowQuality(true);
        result.setMetric(this.metric);

        mergeRequestFileRepository.save(mergeRequestFile);
        mergeRequestClassObservationRepository.save(mergeRequestClassObservation);
        mergeRequestClassMetricResultRepository.save(result);

        assertTrue(evaluationViewHelper.mergeRequestReviewIsCalculating(this.mergeRequestReview));

        mergeRequestClassMetricResultRepository.delete(result);
        mergeRequestClassObservationRepository.delete(mergeRequestClassObservation);
        mergeRequestFileRepository.delete(mergeRequestFile);
    }

    /**
     * Test for checking the functionality if a function metric is still calculating.
     */
    @Test
    void mergeRequestIsCalculatingFunction() {
        MergeRequestFunctionMetricResult result = new MergeRequestFunctionMetricResult();

        MergeRequestFile mergeRequestFile = ModelFactory.getMergeRequestFile(this.mergeRequestReview);
        MergeRequestFunctionObservation mergeRequestFunctionObservation = ModelFactory.getMergeRequestFunctionObservation(mergeRequestFile);

        result.setMergeRequestFunctionObservation(mergeRequestFunctionObservation);
        result.setScore(null);
        result.setIsLowQuality(true);
        result.setMetric(this.metric);

        mergeRequestFileRepository.save(mergeRequestFile);
        mergeRequestFunctionObservationRepository.save(mergeRequestFunctionObservation);
        mergeRequestFunctionMetricResultRepository.save(result);

        assertTrue(evaluationViewHelper.mergeRequestReviewIsCalculating(this.mergeRequestReview));

        mergeRequestFunctionMetricResultRepository.delete(result);
        mergeRequestFunctionObservationRepository.delete(mergeRequestFunctionObservation);
        mergeRequestFileRepository.delete(mergeRequestFile);
    }




}
