package com.roborisk.officer.views;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.events.CodeChurnEvent;
import com.roborisk.officer.events.SonarVulnerabilitiesEvent;
import com.roborisk.officer.helpers.MetricModelHelper;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.DiscussionThread;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestClassObservation;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestFunctionObservation;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.RobotNote;
import com.roborisk.officer.models.database.metrics.MergeRequestClassMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestFileMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestFunctionMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.DiscussionThreadRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestClassMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestClassObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestFileMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestFileRepository;
import com.roborisk.officer.models.repositories.MergeRequestFunctionMetricResultRepository;
import com.roborisk.officer.models.repositories.MergeRequestFunctionObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.repositories.MetricSettingsRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.repositories.RobotNoteRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;

import static org.junit.Assert.assertTrue;

public class GeneralEvaluationViewTest extends OfficerTest {

    @Autowired
    private GeneralEvaluationView generalEvaluationView;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Database repository to handle {@link MetricSettings} objects.
     */
    @Autowired
    private MetricSettingsRepository metricSettingsRepository;

    /**
     * Database repository to handle {@link MetricSettings} objects.
     */
    @Autowired
    private MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * Database repository to handle {@link MergeRequestClassMetricResult} objects.
     */
    @Autowired
    private MergeRequestClassMetricResultRepository mergeRequestClassMetricResultRepository;

    /**
     * Database repository to handle {@link MergeRequestClassObservation} objects.
     */
    @Autowired
    private MergeRequestClassObservationRepository mergeRequestClassObservationRepository;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link MergeRequestReview} objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Database repository to handle {@link RobotNote} objects.
     */
    @Autowired
    private RobotNoteRepository robotNoteRepository;

    /**
     * Database repository to handle {@link DiscussionThread} objects.
     */
    @Autowired
    private DiscussionThreadRepository discussionThreadRepository;

    /**
     * Database repository to handle {@link MergeRequestFileMetricResult} objects.
     */
    @Autowired
    private MergeRequestFileMetricResultRepository mergeRequestFileMetricResultRepository;

    /**
     * Database repository to handle {@link MergeRequestFunctionObservation} objects.
     */
    @Autowired
    private MergeRequestFunctionObservationRepository mergeRequestFunctionObservationRepository;

    /**
     * Database repository to handle {@link MergeRequestFunctionMetricResult} objects.
     */
    @Autowired
    private MergeRequestFunctionMetricResultRepository mergeRequestFunctionMetricResultRepository;

    /**
     * Tests the generateComment function.
     *
     * @throws ParseException   if {@link ModelFactory} errors on creation of {@link GitCommit}.
     */
    @Test
    public void testGenerateComment() throws ParseException {
        // Get models
        GitCommit commit = ModelFactory.getGitCommit();
        Repository repository = ModelFactory.getRepository();
        Branch source = ModelFactory.getBranch(repository, commit);
        Branch target = ModelFactory.getBranch(repository, commit);
        MergeRequest mergeRequest = ModelFactory.getMergeRequest(source, target);
        MergeRequestReview mrr = ModelFactory.getMergeRequestReview(commit, mergeRequest);

        // Initialise discussion
        DiscussionThread discussion = new DiscussionThread();
        discussion.setGitLabId("heyooo");
        discussion.setMergeRequest(mergeRequest);
        discussion.setIsResolved(false);

        // Initialise note
        RobotNote note = new RobotNote();
        note.setGitLabId(5000);
        note.setDiscussionThread(discussion);

        // Save models in database
        this.gitCommitRepository.save(commit);
        this.repositoryRepository.save(repository);
        this.branchRepository.save(source);
        this.branchRepository.save(target);
        this.mergeRequestRepository.save(mergeRequest);
        this.mergeRequestReviewRepository.save(mrr);
        this.discussionThreadRepository.save(discussion);
        this.robotNoteRepository.save(note);

        // Get file and func
        MergeRequestFile file = ModelFactory.getMergeRequestFile(mrr);
        file.setDiscussionThread(discussion);
        MergeRequestClassObservation func = ModelFactory.getMergeRequestClassObservation(file);
        func.setDiscussionThread(discussion);

        assertTrue(generalEvaluationView.generateComment(mergeRequest, mrr).contains("not discovered"));
    }

    /**
     * Tests the generateComment function.
     *
     * @throws ParseException   if {@link ModelFactory} errors on creation of {@link GitCommit}.
     */
    @Test
    public void testGenerateLowQualityClass() throws ParseException {
        // Get models
        GitCommit commit = ModelFactory.getGitCommit();
        Repository repository = ModelFactory.getRepository();
        Branch source = ModelFactory.getBranch(repository, commit);
        Branch target = ModelFactory.getBranch(repository, commit);
        MergeRequest mergeRequest = ModelFactory.getMergeRequest(source, target);
        MergeRequestReview mrr = ModelFactory.getMergeRequestReview(commit, mergeRequest);

        // Initialise discussion
        DiscussionThread discussion = new DiscussionThread();
        discussion.setGitLabId("heyooo");
        discussion.setMergeRequest(mergeRequest);
        discussion.setIsResolved(false);

        // Initialise note
        RobotNote note = new RobotNote();
        note.setGitLabId(5000);
        note.setDiscussionThread(discussion);

        // Save models in database
        this.gitCommitRepository.save(commit);
        this.repositoryRepository.save(repository);
        this.branchRepository.save(source);
        this.branchRepository.save(target);
        this.mergeRequestRepository.save(mergeRequest);
        this.mergeRequestReviewRepository.save(mrr);
        this.discussionThreadRepository.save(discussion);
        this.robotNoteRepository.save(note);

        mrr.setDiscussionThread(discussion);

        // Get file and func
        MergeRequestFile file = ModelFactory.getMergeRequestFile(mrr);
        file.setDiscussionThread(discussion);
        MergeRequestClassObservation _class = ModelFactory.getMergeRequestClassObservation(file);
        _class.setDiscussionThread(discussion);

        this.mergeRequestFileRepository.save(file);

        this.mergeRequestClassObservationRepository.save(_class);

        // Register a low quality metric for class churn.
        Metric metric = MetricModelHelper.getMetric(CodeChurnEvent.METRIC_NAME_CLASS);

        MetricSettings settings = ModelFactory.getMetricSettings(repository, metric);
        this.metricSettingsRepository.save(settings);

        MergeRequestClassMetricResult result = ModelFactory.getMergeRequestClassMetricResult(_class, metric, settings);
        result.setIsLowQuality(true);
        this.mergeRequestClassMetricResultRepository.save(result);

        assertTrue(generalEvaluationView.generateComment(mergeRequest, mrr).contains("artefacts are of low quality"));
    }

    /**
     * Tests the generateComment function.
     *
     * @throws ParseException   if {@link ModelFactory} errors on creation of {@link GitCommit}.
     */
    @Test
    public void testGenerateLowQualityFunction() throws ParseException {
        // Get models
        GitCommit commit = ModelFactory.getGitCommit();
        Repository repository = ModelFactory.getRepository();
        Branch source = ModelFactory.getBranch(repository, commit);
        Branch target = ModelFactory.getBranch(repository, commit);
        MergeRequest mergeRequest = ModelFactory.getMergeRequest(source, target);
        MergeRequestReview mrr = ModelFactory.getMergeRequestReview(commit, mergeRequest);

        // Initialise discussion
        DiscussionThread discussion = new DiscussionThread();
        discussion.setGitLabId("heyooo");
        discussion.setMergeRequest(mergeRequest);
        discussion.setIsResolved(false);

        // Initialise note
        RobotNote note = new RobotNote();
        note.setGitLabId(5000);
        note.setDiscussionThread(discussion);

        // Save models in database
        this.gitCommitRepository.save(commit);
        this.repositoryRepository.save(repository);
        this.branchRepository.save(source);
        this.branchRepository.save(target);
        this.mergeRequestRepository.save(mergeRequest);
        this.mergeRequestReviewRepository.save(mrr);
        this.discussionThreadRepository.save(discussion);
        this.robotNoteRepository.save(note);

        mrr.setDiscussionThread(discussion);

        // Get file and func
        MergeRequestFile file = ModelFactory.getMergeRequestFile(mrr);
        file.setDiscussionThread(discussion);

        this.mergeRequestFileRepository.save(file);

        // Register a low quality metric for function churn.
        Metric metric = MetricModelHelper.getMetric(CodeChurnEvent.METRIC_NAME_FUNCTION);
        MetricSettings settings = ModelFactory.getMetricSettings(repository, metric);
        this.metricSettingsRepository.save(settings);

        MergeRequestFunctionObservation functionObservation = ModelFactory.getMergeRequestFunctionObservation(file);
        functionObservation.setDiscussionThread(discussion);
        this.mergeRequestFunctionObservationRepository.save(functionObservation);

        MergeRequestFunctionMetricResult functionMetricResult = ModelFactory.getMergeRequestFunctionMetricResult(functionObservation, metric, settings);
        functionMetricResult.setIsLowQuality(true);
        this.mergeRequestFunctionMetricResultRepository.save(functionMetricResult);


        assertTrue(generalEvaluationView.generateComment(mergeRequest, mrr).contains("artefacts are of low quality"));
    }

    /**
     * Tests the generateComment function.
     *
     * @throws ParseException   if {@link ModelFactory} errors on creation of {@link GitCommit}.
     */
    @Test
    public void testGenerateLowQualityFile() throws ParseException {
        // Get models
        GitCommit commit = ModelFactory.getGitCommit();
        Repository repository = ModelFactory.getRepository();
        Branch source = ModelFactory.getBranch(repository, commit);
        Branch target = ModelFactory.getBranch(repository, commit);
        MergeRequest mergeRequest = ModelFactory.getMergeRequest(source, target);
        MergeRequestReview mrr = ModelFactory.getMergeRequestReview(commit, mergeRequest);

        // Initialise discussion
        DiscussionThread discussion = new DiscussionThread();
        discussion.setGitLabId("heyooo");
        discussion.setMergeRequest(mergeRequest);
        discussion.setIsResolved(false);

        // Initialise note
        RobotNote note = new RobotNote();
        note.setGitLabId(5000);
        note.setDiscussionThread(discussion);

        // Save models in database
        this.gitCommitRepository.save(commit);
        this.repositoryRepository.save(repository);
        this.branchRepository.save(source);
        this.branchRepository.save(target);
        this.mergeRequestRepository.save(mergeRequest);
        this.mergeRequestReviewRepository.save(mrr);
        this.discussionThreadRepository.save(discussion);
        this.robotNoteRepository.save(note);

        // Get file and func
        MergeRequestFile file = ModelFactory.getMergeRequestFile(mrr);
        file.setDiscussionThread(discussion);

        this.mergeRequestFileRepository.save(file);

        // Register a low quality metric for file churn.
        Metric metric = MetricModelHelper.getMetric(CodeChurnEvent.METRIC_NAME_FILE);
        MetricSettings settings = ModelFactory.getMetricSettings(repository, metric);
        this.metricSettingsRepository.save(settings);

        MergeRequestFileMetricResult resultfile = ModelFactory.getMergeRequestFileMetricResult(file, metric, settings);
        resultfile.setIsLowQuality(true);
        this.mergeRequestFileMetricResultRepository.save(resultfile);

        System.out.println(generalEvaluationView.generateComment(mergeRequest, mrr));
        assertTrue(generalEvaluationView.generateComment(mergeRequest, mrr).contains("Analysis suggests that 1 artefact is of low quality."));
    }

    /**
     * Tests the generateComment function.
     *
     * @throws ParseException   if {@link ModelFactory} errors on creation of {@link GitCommit}.
     */
    @Test
    public void testGenerateProcessing() throws ParseException {
        // Get models
        GitCommit commit = ModelFactory.getGitCommit();
        Repository repository = ModelFactory.getRepository();
        Branch source = ModelFactory.getBranch(repository, commit);
        Branch target = ModelFactory.getBranch(repository, commit);
        MergeRequest mergeRequest = ModelFactory.getMergeRequest(source, target);
        MergeRequestReview mrr = ModelFactory.getMergeRequestReview(commit, mergeRequest);

        // Initialise discussion
        DiscussionThread discussion = new DiscussionThread();
        discussion.setGitLabId("heyooo");
        discussion.setMergeRequest(mergeRequest);
        discussion.setIsResolved(false);

        // Initialise note
        RobotNote note = new RobotNote();
        note.setGitLabId(5000);
        note.setDiscussionThread(discussion);

        // Save models in database
        this.gitCommitRepository.save(commit);
        this.repositoryRepository.save(repository);
        this.branchRepository.save(source);
        this.branchRepository.save(target);
        this.mergeRequestRepository.save(mergeRequest);
        this.mergeRequestReviewRepository.save(mrr);
        this.discussionThreadRepository.save(discussion);
        this.robotNoteRepository.save(note);

        mrr.setDiscussionThread(discussion);

        // Get file and func
        MergeRequestFile file = ModelFactory.getMergeRequestFile(mrr);
        file.setDiscussionThread(discussion);

        this.mergeRequestFileRepository.save(file);

        // Register a processing metric for sonar.
        Metric metric = MetricModelHelper.getMetric(SonarVulnerabilitiesEvent.METRIC_NAME);
        MetricSettings settings = ModelFactory.getMetricSettings(repository, metric);
        this.metricSettingsRepository.save(settings);
        MergeRequestFileMetricResult sonarResult = ModelFactory.getMergeRequestFileMetricResult(file, metric, settings);
        sonarResult.setScore(null);
        this.mergeRequestFileMetricResultRepository.save(sonarResult);

        assertTrue(generalEvaluationView.generateComment(mergeRequest, mrr).contains("artefacts are of low quality"));
    }
}
