package com.roborisk.officer.views;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.helpers.CommentHelper;
import com.roborisk.officer.helpers.UploadWrapper;
import com.roborisk.officer.mock.CommentHelperMock;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.metrics.AbstractMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestCodebaseMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.MetricRepository;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import com.roborisk.officer.views.metrics.GeneralMetricEvaluationView;
import org.gitlab4j.api.GitLabApiException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Tests for the {@link ArtefactEvaluationView}.
 *
 * The test case is disabled since it is not automatically evaluated.
 */
@Disabled
public class ArtefactEvaluationViewTest extends OfficerTest {

    /**
     * Object to be tested.
     */
    @Autowired
    private ArtefactEvaluationView artefactEvaluationView;

    /**
     * CommentHelper which is used to upload the files
     */
    @Autowired
    private CommentHelper commentHelper;

    /**
     * Database repository to handle {@link Metric} objects.
     */
    @Autowired
    private MetricRepository metricRepository;

    /**
     * Number of {@link GitCommit} objects for which the view should be tested.
     */
    private int noCommits;

    /**
     * Number of {@link Metric} objects for which the view should be tested.
     */
    private int noMetrics;

    /**
     * The {@link Repository} used for the tests.
     */
    private Repository repository;

    /**
     * The {@link MergeRequest} used for the tests.
     */
    private MergeRequest mergeRequest;

    /**
     * Array of all the {@link GitCommit} objects used for the test.
     */
    private GitCommit commits[];

    /**
     * Array of all the {@link MergeRequestReview} objects used for the test.
     */
    private MergeRequestReview mergeRequestReviews[];

    /**
     * Array of all the {@link Metric} objects used for the test.
     */
    private Metric metrics[];

    /**
     * Array of all the {@link MetricSettings} objects used for the test.
     */
    private MetricSettings metricSettings[];

    /**
     * Array of all the {@link AbstractMetricResult} objects used for the test.
     */
    private List<AbstractMetricResult> metricResults;

    /**
     * Prepare the database with supporting objects before running each test.
     *
     * @throws ParseException   If the {@link ModelFactory} throws an exception while creating
     *                              a {@link GitCommit} object.
     */
    @BeforeEach
    public void setUpDatabaseObjects() throws ParseException {
        this.noCommits = ThreadLocalRandom.current().nextInt(5, 10);
        this.noMetrics = ThreadLocalRandom.current().nextInt(4, 5);

        this.repository = ModelFactory.getRepository();
        this.mergeRequest =
                ModelFactory.getMergeRequest(ModelFactory.getBranch(this.repository,
                    ModelFactory.getGitCommit()),
                    ModelFactory.getBranch(this.repository, ModelFactory.getGitCommit())
                );
        this.mergeRequest.setGitLabIid(75);

        // ------ setup the commits --------
        this.commits = new GitCommit[this.noCommits];
        this.mergeRequestReviews = new MergeRequestReview[this.noCommits];
        // create commits
        for (int i = 0; i < this.noCommits; i++) {
            this.commits[i] = ModelFactory.getGitCommit();
            this.mergeRequestReviews[i] = ModelFactory.getMergeRequestReview(this.commits[i],
                    this.mergeRequest);
            this.commits[i].setHash("61b97b9623b7ffc813f12fd035e899f4cb3ec2dc");
        }
        // set timestamp on commits such that they are all unique
        for (int i = 1; i < this.noCommits; i++) {
            this.commits[i].setTimestamp(this.commits[i-1].getTimestamp()
                + ThreadLocalRandom.current().nextInt(1000, 1000000));
        }

        // ------ setup the metrics ---------
        this.metrics = new Metric[this.noMetrics];
        this.metricSettings = new MetricSettings[this.noMetrics];
        for (int i = 0; i < this.noMetrics; i++) {
            this.metrics[i] = ModelFactory.getMetric();
            this.metrics[i].setDisplayName("Metric no. " + i);
            this.metricSettings[i] = ModelFactory.getMetricSettings(this.repository,
                this.metrics[i]);
            metricRepository.save((this.metrics[i]));
        }

        // ------ setup the results --------
        metricResults = new ArrayList<>();
        for (MergeRequestReview mergeRequestReview : this.mergeRequestReviews) {
            for (int i = 0; i < this.noMetrics; i++) {
                MergeRequestCodebaseMetricResult nextResult =
                    ModelFactory.getMRCodebaseMetric(mergeRequestReview, metrics[i],
                        metricSettings[i]);
                metricResults.add(nextResult);
            }
        }
    }

    /**
     * Tests the ArtefactEvaluationView by calling it with the given views.
     *
     * @param views                 The views to pass to the ArtefactEvaluationView.
     * @throws IOException          When there is an exception happening with file handling.
     * @throws GitLabApiException   If any exception occurs within the {@link GitLabApiWrapper}.
     */
    public void generalTest(Map<Metric, GeneralMetricEvaluationView> views)
                            throws IOException, GitLabApiException {
        List<MetricSettings> settings = new ArrayList<>();
        for (MetricSettings setting : metricSettings) {
            settings.add(setting);
        }

        System.out.println(this.artefactEvaluationView.generateComment(this.metricResults, settings,
                views, mergeRequest));
    }

    /**
     * Test the correct functioning of the GraphReevaluationView by calling it and
     * printing the returned string.
     */
    @Test
    public void testWithGraphEvaluationView() throws IOException, GitLabApiException {
        // register for the file-upload function
        CommentHelperMock.register();

        Map<Metric, GeneralMetricEvaluationView> views = new HashMap<>();

        for (Metric metric : this.metrics) {
            views.put(metric, new GeneralMetricEvaluationView(new UploadWrapper() {
                @Override
                protected int getGitLabId() {
                    return 176;
                }

                @Override
                protected CommentHelper getCommentHelper() {
                    return commentHelper;
                }
            }));
        }

        this.generalTest(views);
    }
}
