package com.roborisk.officer.controllers;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.RobotSettings;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.MetricRepository;
import com.roborisk.officer.models.repositories.MetricSettingsRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.repositories.RobotSettingsRepository;
import com.roborisk.officer.models.web.MetricSettingsWeb;
import com.roborisk.officer.models.web.RepositoryToggleWeb;
import com.roborisk.officer.models.web.RobotSettingsWeb;
import com.roborisk.officer.models.web.SettingsApiData;
import org.bouncycastle.math.raw.Mod;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests the SettingsApiController
 */
@AutoConfigureMockMvc
public class SettingsApiControllerTest extends OfficerTest {

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link MetricSettings} objects.
     */
    @Autowired
    MetricSettingsRepository metricSettingsRepository;

    /**
     * Database repository to handle {@link RobotSettings} objects.
     */
    @Autowired
    RobotSettingsRepository robotSettingsRepository;

    /**
     * Database repository to handle {@link Metric} objects.
     */
    @Autowired
    MetricRepository metricRepository;

    /**
     * The rest template to test against.
     */
    @Autowired
    private TestRestTemplate template;

    /**
     * {@link RobotSettings} object for testing.
     */
    private RobotSettings robotSettings;

    /**
     * Array of {@link Metric} objects for testing.
     */
    private Metric[] metrics;

    /**
     * Array of {@link MetricSettings} objects for testing.
     */
    private MetricSettings[] metricSettings;

    /**
     * Get the uri for getting and putting repositories based on the repository for which the
     * action is being taken.
     *
     * @param repository Repository for which the URI should be generated.
     * @return String containing the required URI.
     */
    private String getURI(Repository repository) {
        String uri;
        if (repository != null) {
            uri = "http://localhost:" + port + "/settings/repository/" + repository.getId();
        } else {
            uri = "http://localhost:" + port + "/settings/default";
        }
        return uri;
    }

    /**
     * Populate the database with valid {@link Metric}, {@link MetricSettings} and {@link
     * RobotSettings} objects and store them in the corresponding gloabl variables.
     *
     * @param repository       Repository for which database entries should be generated
     *                         (should already be saved in database).
     * @param noMetricSettings integer indicating how many {@link MetricSettings} and {@link
     *                         Metric} objects should be generated.
     */
    private void fillDatabase(Repository repository, int noMetricSettings) {
        robotSettings = ModelFactory.getRobotSettings(repository);
        robotSettingsRepository.save(robotSettings);

        metrics = new Metric[noMetricSettings];
        metricSettings = new MetricSettings[noMetricSettings];

        for (int i = 0; i < noMetricSettings; i++) {
            metrics[i] = ModelFactory.getMetric();
            metricRepository.save(metrics[i]);
            metricSettings[i] = ModelFactory.getMetricSettings(repository, metrics[i]);
            metricSettingsRepository.save(metricSettings[i]);
        }
    }

    /**
     * Create a valid get-request to get settings and assert that the returned data is correct.
     *
     * @param repository       Repository for which the get get-request should be generated
     *                         (can be null to test for default settings).
     * @param noMetricSettings number of metrics that should be added to the database and
     *                         asserted to be received upon the get-request.
     */
    private void checkGetRequest_pass(Repository repository, int noMetricSettings) {
        if (repository != null) {
            fillDatabase(repository, noMetricSettings);
        } else {
            this.robotSettings = this.robotSettingsRepository.findByRepositoryId(null)
                .orElseThrow(() -> {
                    String msg = "SettingsApiControllerTest: Database is not seeded correctly.";

                    throw new IllegalStateException(msg);
                });

            this.metricSettings = this.metricSettingsRepository.findByRepositoryId(null)
                .orElseThrow(() -> {
                    String msg = "SettingsApiControllerTest: Database is not seeded correctly.";

                    throw new IllegalStateException(msg);
                }).toArray(MetricSettings[]::new);
        }

        String uri = getURI(repository);
        // Post first request.
        ResponseEntity<SettingsApiData> response = template.getForEntity(uri,
            SettingsApiData.class);
        assertEquals(200, response.getStatusCodeValue());
        assertTrue(response.hasBody());

        SettingsApiData returnData = response.getBody();

        // check return data of repository
        assertNotNull(returnData.getRobotSettings());
        assertTrue(robotSettings.isEqualToRobotSettingsWeb(returnData.getRobotSettings()));

        // check return data of metric settings
        assertEquals(noMetricSettings, returnData.getMetricSettings().size());
        for (int i = 0; i < noMetricSettings; i++) {
            Optional<MetricSettingsWeb> returnedSetting =
                returnData.findMetricSetting(metricSettings[i].getId());
            assertTrue(returnedSetting.isPresent());
            assertTrue(metricSettings[i].isEqualToMetricSettingsWeb(returnedSetting.get()));
        }
    }

    /**
     * Create an {@link SettingsApiData} object which would be a valid update for the given
     * settings assocationed to a {@link Repository}.
     *
     * @param repository          Repository for which the {@link SettingsApiData} object
     *                            should be generated.
     * @param metrics             the {@link Metric} for which a {@link MetricSettingsWeb}
     *                            update object may be generated.
     * @param metricSettings      the {@link MetricSettings} for which a {@link
     *                            MetricSettingsWeb} update object may be generated.
     * @param robotSettings       the {@link RobotSettings} for which a {@link
     *                            RobotSettingsWeb} update object may be generated.
     * @param forceMetricSettings boolean whether there should definitely be an update to the
     *                            {@link RobotSettings} object.
     * @return                    SettingsApiData object representing a happy flow update.
     */
    private SettingsApiData createGoodUpdate(Repository repository, Metric[] metrics,
                                             MetricSettings[] metricSettings,
                                             RobotSettings robotSettings,
                                             boolean forceRobotSettings,
                                             boolean forceMetricSettings) {
        SettingsApiData settingsApiData = new SettingsApiData();

        if (ThreadLocalRandom.current().nextBoolean() || forceRobotSettings) {
            RobotSettingsWeb updateRobotSettings =
                ModelFactory.getRobotSettingsWeb(ModelFactory.getRobotSettings(repository));
            updateRobotSettings.setId(robotSettings.getId());
            settingsApiData.setRobotSettings(updateRobotSettings);
        }

        for (int i = 0; i < metrics.length; i++) {
            if (ThreadLocalRandom.current().nextBoolean() && !forceMetricSettings) {
                continue;
            }
            forceMetricSettings = false;

            MetricSettingsWeb updateMetricSettings =
                ModelFactory.getMetricSettingsWeb(ModelFactory.getMetricSettings(repository,
                    metrics[i]));
            updateMetricSettings.setId(metricSettings[i].getId());
            settingsApiData.addToMetricSettings(updateMetricSettings);
        }

        return settingsApiData;
    }

    /**
     * Create and send put request.
     *
     * @param uri  the URI to which the put-request should be send.
     * @param data A {@link SettingsApiData} object which will be send in the put-request body.
     * @return     The {@link ResponseEntity} created by the put-request.
     */
    private ResponseEntity sendPutRequest(String uri, SettingsApiData data) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<SettingsApiData> entity = new HttpEntity<SettingsApiData>(data, headers);

        return template.exchange(uri, HttpMethod.PUT, entity,
            Object.class, ThreadLocalRandom.current().nextInt());
    }

    /**
     * Send the settingsApiData parameter as a put-request to the repositoryID of the
     * repository and assert a BAD_REQUEST response.
     *
     * @param repository      Repository for to which the put-request should be send (can be
     *                        null to test for default settings).
     * @param settingsApiData the data which will be send in the put-request body.
     */
    private void putRepository_badRequest(Repository repository,
                                          SettingsApiData settingsApiData) {
        String uri = getURI(repository);

        ResponseEntity response = sendPutRequest(uri, settingsApiData);
        assertEquals(400, response.getStatusCodeValue());
    }

    /**
     * Create and send a valid repository update and assert its correct execution.
     *
     * @param repository       Repository for to which the put-request should be send (can be
     *                         null to test for default settings).
     * @param noMetricSettings number of {@link Metric} and {@link MetricSettings} which should
     *                         be stored in the database for the repository.
     */
    private void updateRepository_pass(Repository repository, int noMetricSettings) {
        if (repository != null) {
            fillDatabase(repository, noMetricSettings);
        } else {
            this.robotSettings = this.robotSettingsRepository.findByRepositoryId(null)
                .orElseThrow(() -> {
                    String msg = "SettingsApiControllerTest: Database is not seeded correctly.";

                    throw new IllegalStateException(msg);
                });

            this.metricSettings = this.metricSettingsRepository.findByRepositoryId(null)
                .orElseThrow(() -> {
                    String msg = "SettingsApiControllerTest: Database is not seeded correctly.";

                    throw new IllegalStateException(msg);
                }).toArray(MetricSettings[]::new);

            this.metrics = new Metric[Math.toIntExact(this.metricRepository.count())];

            int index = 0;
            for (Metric currentMetric : this.metricRepository.findAll()) {
                this.metrics[index] = currentMetric;
                index++;
            }
        }

        String uri = getURI(repository);

        // create the settingsApiData containing the data to be updated
        SettingsApiData settingsApiData = createGoodUpdate(repository, metrics, metricSettings,
            robotSettings, false, false);

        ResponseEntity response = sendPutRequest(uri, settingsApiData);

        assertEquals(200, response.getStatusCodeValue());

        if (settingsApiData.getRobotSettings() != null) {
            RobotSettings updatedRobotSettings =
                robotSettingsRepository.findById(robotSettings.getId()).get();
            assertTrue(updatedRobotSettings.isEqualToRobotSettingsWeb(
                settingsApiData.getRobotSettings()));
        }

        for (MetricSettingsWeb updateMetricSetting : settingsApiData.getMetricSettings()) {
            MetricSettings updatedMetricSetting =
                metricSettingsRepository.findById(updateMetricSetting.getId()).get();
            assertTrue(updatedMetricSetting.isEqualToMetricSettingsWeb(updateMetricSetting));
        }

    }

    /**
     * Create and send an invalid repository update where the repositoryID of the {@link
     * RobotSettingsWeb} object does not match the repository stored in the database.
     *
     * @param repository Repository for to which the put-request should be send (can be null
     *                   to test for default settings).
     */
    public void testPutRepository_unknownRobotSetting(Repository repository) {
        fillDatabase(repository, ThreadLocalRandom.current().nextInt(0, 20));

        SettingsApiData data = createGoodUpdate(repository, metrics, metricSettings,
            robotSettings, true, false);
        Integer unknownId = ThreadLocalRandom.current().nextInt();
        while (unknownId.equals(data.getRobotSettings().getId())) {
            unknownId = ThreadLocalRandom.current().nextInt();
        }

        data.getRobotSettings().setId(unknownId);
        putRepository_badRequest(repository, data);
    }

    /**
     * Create and send an invalid repository update where the repositoryID of a {@link
     * MetricSettingsWeb} object does not match the repository stored in the database.
     *
     * @param repository Repository for to which the put-request should be send (can be null
     *                   to test for default settings).
     */
    public void testPutRepository_unknownMetricSetting(Repository repository) {
        fillDatabase(repository, ThreadLocalRandom.current().nextInt(1, 20));

        SettingsApiData data = createGoodUpdate(repository, metrics, metricSettings,
            robotSettings, false, true);
        Integer unknownId = ThreadLocalRandom.current().nextInt();
        while (unknownId.equals(data.getMetricSettings().get(0).getId())) {
            unknownId = ThreadLocalRandom.current().nextInt();
        }

        data.getMetricSettings().get(0).setId(unknownId);
        putRepository_badRequest(repository, data);
    }

    /**
     * Create and send an invalid repository update where the repositoryID does not match the
     * data of the settingsApiData object and we have at least one {@link MetricSettings} object.
     *
     * @param repository Repository for to which the put-request should be send (can be null
     *                   to test for default settings), while the settings are actually
     *                   associated to a different repository.
     */
    public void testPutRepository_WrongRepoWithMetricSettings(Repository repository) {
        Repository otherRepo = ModelFactory.getRepository();
        repositoryRepository.save(otherRepo);

        int noMetricSettings = ThreadLocalRandom.current().nextInt(1, 20);
        fillDatabase(otherRepo, noMetricSettings);
        SettingsApiData data = createGoodUpdate(repository, metrics, metricSettings,
            robotSettings, false, true);

        putRepository_badRequest(repository, data);
    }

    /**
     * Create and send an invalid repository update where the repositoryID does not match the
     * data of the settingsApiData object and we have a {@link RobotSettings} object.
     *
     * @param repository Repository for to which the patch-request should be send (can be null
     *                   to test for default settings), while the settings are actually
     *                   associated to a different repository.
     */
    public void testPutRepository_WrongRepoWithRobotSettings(Repository repository) {
        Repository otherRepo = ModelFactory.getRepository();
        repositoryRepository.save(otherRepo);

        int noMetricSettings = 0;
        fillDatabase(otherRepo, noMetricSettings);
        SettingsApiData data = createGoodUpdate(repository, metrics, metricSettings,
            robotSettings, true, false);

        putRepository_badRequest(repository, data);
    }

    /**
     * Test whether the "/settings/repository/{id}" resource returns the valid data of repo.
     */
    @Test
    public void testGetRepository_pass() {
        int noMetricSettings = ThreadLocalRandom.current().nextInt(0, 20);
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);
        checkGetRequest_pass(repository, noMetricSettings);
    }

    /**
     * Test whether the "/settings/default" resource returns the valid data on default
     * settings.
     */
    @Test
    public void testGetDefaultRepository_pass() {
        int noMetricSettings = this.metricSettingsRepository.findByRepositoryId(null)
            .orElseThrow(() -> {
                String msg = "SettingsApiControllerTest: Database is not seeded correctly.";

                throw new IllegalStateException(msg);
            }).size();
        checkGetRequest_pass(null, noMetricSettings);
    }

    /**
     * Test whether the "/settings/repository/{id}" get-method throws the right error when
     * called on an non-existent repository.
     */
    @Test
    public void testGetRepository_unknownRepository() {
        String uri =
            "http://localhost:" + port + "/settings/repository/"
                + ThreadLocalRandom.current().nextInt();
        ResponseEntity<SettingsApiData> response = template.getForEntity(uri,
            SettingsApiData.class);
        assertEquals(404, response.getStatusCodeValue());
    }

    /**
     * Test whether the "/settings/repository/{id}" put-method works correct with valid
     * data.
     */
    @Test
    public void testPutRepository_pass() {
        int noMetricSettings = ThreadLocalRandom.current().nextInt(1, 20);
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);
        updateRepository_pass(repository, noMetricSettings);
    }

    /**
     * Test whether the "/settings/repository/{id}" method works correctly with valid data for
     * the default repository.
     */
    @Test
    public void testPutDefaultRepository_pass() {
        int noMetricSettings = ThreadLocalRandom.current().nextInt(0, 20);
        updateRepository_pass(null, noMetricSettings);
    }

    /**
     * Test whether the "/settings/repository/{id}" put-method throws correct error when
     * called for a non-existent repository.
     */
    @Test
    public void testPutRepository_unknownRepository() {
        String uri =
            "http://localhost:" + port + "/settings/repository/"
                + ThreadLocalRandom.current().nextInt();

        // create the settingsApiData containing the data to be updated
        SettingsApiData settingsApiData = new SettingsApiData();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<SettingsApiData> entity =
            new HttpEntity<SettingsApiData>(settingsApiData, headers);

        ResponseEntity response = template.exchange(uri, HttpMethod.PUT, entity,
            Object.class, ThreadLocalRandom.current().nextInt());
        assertEquals(404, response.getStatusCodeValue());
    }

    /**
     * Test whether the "/settings/repository/{id}" put-method throws correct error when
     * called for a robotsettingsweb object which is not in the database for an existent
     * repository and for the default settings.
     */
    @Test
    public void testPutRepository_unknownRobotSetting() {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);
        testPutRepository_unknownRobotSetting(repository);
    }

    /**
     * Test whether the "/settings/repository/{id}" put-method throws correct error when
     * called for a MetricSettingsWeb object which is not in the database for an existent
     * repository and for the default settings.
     */
    @Test
    public void testPutRepository_unknownMetricSetting() {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);
        testPutRepository_unknownMetricSetting(repository);
    }

    /**
     * Test whether the "/settings/repository/{id}" put-method throws correct error when
     * called an existent repository that does not match the to-be-updated settings in the
     * SettingsApiData object.
     */
    @Test
    public void testPutRepository_WrongRepo() {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        testPutRepository_WrongRepoWithMetricSettings(repository);
        testPutRepository_WrongRepoWithMetricSettings(null);
        testPutRepository_WrongRepoWithRobotSettings(repository);
        testPutRepository_WrongRepoWithRobotSettings(null);
    }

    /**
     * Test whether the "/settings/repository/{id}/toggle" method works correctly with valid
     * data.
     */
    @Test
    public void testToggleRepository_pass() {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);
        boolean currentStatus = repository.getIsEnabled();

        String url = "http://localhost:" + port + "/settings/repository/{repositoryId}/toggle";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>("", headers);

        ResponseEntity<RepositoryToggleWeb> response = template.exchange(
            url,
            HttpMethod.PUT,
            entity,
            RepositoryToggleWeb.class,
            repository.getId());

        assertEquals(200, response.getStatusCodeValue());
        assertNotEquals(response.getBody().getIsEnabled(), currentStatus);
        Repository updated = repositoryRepository.findById(repository.getId()).get();
        assertNotEquals(updated.getIsEnabled(), currentStatus);
    }

    /**
     * Test whether the "/settings/repository/{id}/toggle" throws correct error when called on
     * an non-existent repository.
     */
    @Test
    public void testToggleRepository_fail() {
        String url = "http://localhost:" + port + "/settings/repository/{repositoryId}/toggle";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>("", headers);

        ResponseEntity<Boolean> response = template.exchange(url, HttpMethod.PUT, entity,
            Boolean.class, ThreadLocalRandom.current().nextInt());

        assertEquals(404, response.getStatusCodeValue());
    }

    /**
     * Test whether the "/settings/repository/{id}/toggle" method works correctly with valid
     * data.
     */
    @Test
    public void testToggleRepositorySetEnabled_pass() {
        Repository repository = ModelFactory.getRepository();
        repository.setIsEnabled(false);
        this.repositoryRepository.save(repository);
        boolean currentStatus = repository.getIsEnabled();

        String url = "http://localhost:" + this.port + "/settings/repository/{repositoryId}/toggle";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>("", headers);

        ResponseEntity<RepositoryToggleWeb> response = template.exchange(
            url,
            HttpMethod.PUT,
            entity,
            RepositoryToggleWeb.class,
            repository.getId());

        assertEquals(200, response.getStatusCodeValue());
        assertNotEquals(response.getBody().getIsEnabled(), currentStatus);
        Repository updated = repositoryRepository.findById(repository.getId()).get();
        assertNotEquals(updated.getIsEnabled(), currentStatus);
    }

    /**
     * Tests the internal server error when a repository has no settings linked to it.
     */
    @Test
    public void testEmptyRobotSettingsGet() {
        Repository repository = ModelFactory.getRepository();
        this.repositoryRepository.save(repository);

        String url = this.getURI(repository);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>("", headers);

        ResponseEntity<String> response = this.template.exchange(url, HttpMethod.GET, entity,
            String.class);

        assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Tests the update functionality when there are no robot settings sent.
     */
    @Test
    public void testEmptyRobotSettingsPut() {
        Repository repository = ModelFactory.getRepository();
        this.repositoryRepository.save(repository);

        String url = this.getURI(repository);

        this.fillDatabase(repository, ThreadLocalRandom.current().nextInt(0, 20));

        SettingsApiData data = createGoodUpdate(repository, metrics, metricSettings,
            robotSettings, true, false);
        data.setRobotSettings(null);


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<SettingsApiData> entity = new HttpEntity<SettingsApiData>(data, headers);

        ResponseEntity<Object> response = template.exchange(url, HttpMethod.PUT, entity,
            Object.class, ThreadLocalRandom.current().nextInt());

        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }
}
