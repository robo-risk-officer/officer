package com.roborisk.officer.controllers;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.mock.ProjectsMock;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.RobotSettings;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.repositories.RobotSettingsRepository;
import com.roborisk.officer.models.web.RepositoryApiWeb;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests the Repository List Controller.
 */
@TestPropertySource(properties = {
    "officer.interceptors.auth.enabled=false",
})
class RepositoryAPIControllerTest extends OfficerTest {

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link RobotSettings} objects.
     */
    @Autowired
    private RobotSettingsRepository robotSettingsRepository;

    /**
     * GitLabApiWrapper for making calls to GitLab.
     */
    @Autowired
    private GitLabApiWrapper gitLabApiWrapper;

    /**
     * The rest template to test against.
     */
    @Autowired
    private TestRestTemplate template;

    /**
     * List of {@link Repository} objects for testing.
     */
    private List<Repository> repositoryTestList;

    /**
     * Register the routes for the projects.
     */
    @BeforeEach
    void register() {
        ProjectsMock.register();
    }

    /**
     * Returns the URI to be used in queries.
     *
     * @param autoFill    Whether to use the auto fill functionality or not.
     * @return            The correct URI.
     */
    private String getUri(boolean autoFill, String extraParam){
        if (autoFill) {
            return "http://localhost:" + this.port + "/repository/list" + extraParam;
        } else {
            return "http://localhost:" + this.port + "/repository/list?autoFill=false" + extraParam;
        }
    }

    /**
     * Returns the URI to be used in pagination queries.
     *
     * @return            The correct URI.
     */
    private String getPagesUri() {
        return "http://localhost:" + this.port + "repository/pages";
    }

    /**
     * Populate the database with valid {@link Repository} objects and
     * store them in the corresponding global variable.
     *
     * @param noRepository Integer indicating how many {@link Repository} objects
     *                     should be generated.
     */
    private void fillDatabase(int noRepository) {
        this.repositoryTestList = new ArrayList<>();

        for (int i = 0; i < noRepository; i++) {
            Repository rep = ModelFactory.getRepository();
            this.repositoryRepository.save(rep);
            this.repositoryTestList.add(rep);
        }
    }

    /**
     * Tests whether the controller returns a valid response.
     */
    @Test
    void listValidResponse() {
        ResponseEntity<RepositoryApiWeb[]> response = template
            .getForEntity(getUri(true, ""), RepositoryApiWeb[].class);

        List<RepositoryApiWeb> responseBody = Arrays.asList(response.getBody());

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(responseBody);
    }

    /**
     * Tests whether the returned repositories are present in Gitlab.
     *
     * @throws GitLabApiException if something is wrong with the Api Wrapper.
     */
    @Test
    void listCorrespondingProjects() throws GitLabApiException {
        ResponseEntity<RepositoryApiWeb[]> response = template
            .getForEntity(getUri(true, ""), RepositoryApiWeb[].class);

        List<RepositoryApiWeb> responseBody = Arrays.asList(response.getBody());
        List<Project> projects = gitLabApiWrapper.getGitLabApi().getProjectApi().getProjects();
        List<Repository> repositories = repositoryRepository.findAll();

        responseBody.forEach(res -> {
            Optional<Repository> r = repositories.stream()
                    .filter(repo -> repo.getId().equals(res.getId())).findFirst();

            // There is a corresponding repo.
            assertTrue(r.isPresent());

            Optional<Project> p = projects.stream()
                    .filter(proj -> proj.getId().equals(r.get().getGitLabId())).findFirst();

            // There is a corresponding project in the Gitlab mock.
            assertTrue(p.isPresent());
        });
    }

    /**
     * Tests if the API itself works in general, without Auto Fill.
     */
    @Test
    void basicApiTest() {
        int noRepository = ThreadLocalRandom.current().nextInt(1, 30);
        this.fillDatabase(noRepository);

        ResponseEntity<RepositoryApiWeb[]> response = template
            .getForEntity(getUri(false, "&pageSize=" + noRepository), RepositoryApiWeb[].class);

        List<RepositoryApiWeb> responseBody = Arrays.asList(response.getBody());

        assertEquals(responseBody.size(), repositoryTestList.size());

        repositoryTestList.forEach(repository -> {
            Optional<RepositoryApiWeb> returnedRepo =
                responseBody.stream().filter(res -> res.getId().equals(repository.getId())).findFirst();

            assertTrue(returnedRepo.isPresent());
            assertTrue(repository.isEqualToRepositoryWeb(returnedRepo.get()));
        });
    }

    /**
     * Tests that pagination works when the number of repositories
     * is larger than the page size.
     */
    @Test
    void paginationTest() {
        int noRepository = ThreadLocalRandom.current().nextInt(11, 30);
        this.fillDatabase(noRepository);

        ResponseEntity<RepositoryApiWeb[]> response = template
            .getForEntity(getUri(false, ""), RepositoryApiWeb[].class);

        List<RepositoryApiWeb> responseBody = Arrays.asList(response.getBody());

        assertEquals(10, responseBody.size());
    }

    /**
     * Tests that the Api returns a status code of 404 when the response is empty
     * and the page size is larger than 0.
     */
    @Test
    void paginationTest_404() {
        int noRepository = ThreadLocalRandom.current().nextInt(11, 30);
        this.fillDatabase(noRepository);

        ResponseEntity<RepositoryApiWeb[]> response = template
            .getForEntity(getUri(false, "&pageNo=5"), RepositoryApiWeb[].class);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * Tests that the API returns the correct number of repositories.
     */
    @Test
    void numberOfRepositoriesTest() {
        int noRepository = ThreadLocalRandom.current().nextInt(0, 30);
        this.fillDatabase(noRepository);

        ResponseEntity<RepositoryApiWeb[]> response = template
            .getForEntity(getUri(false, ""), RepositoryApiWeb[].class);

        List<RepositoryApiWeb> responseBody = Arrays.asList(response.getBody());

        String noElements = response.getHeaders().get("X-Total").get(0);

        assertEquals(Integer.parseInt(noElements), noRepository);
    }
}
