package com.roborisk.officer.controllers;

import com.roborisk.officer.OfficerTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests the hello world controller.
 */
class HelloSpringControllerTest extends OfficerTest {


    /**
     * The rest template to test against.
     */
    @Autowired
    private TestRestTemplate template;

    /**
     * Test whether the "/" resource returns the valid string.
     */
    @Test
    public void testGreetingReturned() {
        assertThat(this.template.getForObject("http://localhost:" + port + "/", String.class))
                .isEqualTo(HelloSpringController.WELCOME_MESSAGE);
    }
}
