package com.roborisk.officer.controllers;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.MergeRequestWeb;
import org.apache.commons.lang.NotImplementedException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * MergeRequestApiControllerTest tests the functionality of {@link MergeRequestApiController}.
 */
class MergeRequestApiControllerTest extends OfficerTest {

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * The rest template to test against.
     */
    @Autowired
    private TestRestTemplate template;

    /**
     * Array of database merge requests for testing.
     */
    private MergeRequest[] mergeRequests;

    /**
     * Constructs the URI for getting a single merge request.
     *
     * @param mergeRequestID    The id of the {@link MergeRequest} for which the URI is generated.
     * @return                  The String containing the required URI.
     */
    private String getGetUri(int mergeRequestID) {
        return "http://localhost:" + this.port + "/merge-request/" + mergeRequestID;
    }

    /**
     * Constructs the URI for getting the paginated list of all merge requests.
     *
     * @return  The String containing the required URI.
     */
    private String getListUri() {
        return "http://localhost:" + this.port + "/merge-request/list";
    }

    /**
     * Constructs the URI for getting a specific page of merge requests.
     *
     * @param pageNo    The number of the page.
     * @return          The String containing the required URI.
     */
    private String getPageListUri(int pageNo) {
        return "http://localhost:" + this.port + "/merge-request/list?pageNo=" + pageNo;
    }

    /**
     * Populate the database with valid {@link Repository}, {@link GitCommit}, {@link Branch}
     * and {@link MergeRequest} objects and store them in the corresponding class variables.
     *
     * @param noMergeRequests Integer indicating how many {@link MergeRequest} should be generated.
     * @throws ParseException If the getGitCommit method throws a {@link ParseException}.
     */
    private void fillDatabase(int noMergeRequests) throws ParseException {
        // Add a repository
        Repository repository = ModelFactory.getRepository();
        this.repositoryRepository.save(repository);

        // Add commit
        GitCommit commit = ModelFactory.getGitCommit();
        this.gitCommitRepository.save(commit);

        // Add source and target branches with given commit

        Branch sourceBranch = ModelFactory.getBranch(repository, commit);
        Branch targetBranch = ModelFactory.getBranch(repository, commit);

        this.branchRepository.save(sourceBranch);
        this.branchRepository.save(targetBranch);

        // Add merge requests
        this.mergeRequests = new MergeRequest[noMergeRequests];
        for (int i = 0; i < noMergeRequests; i++) {
            this.mergeRequests[i] = ModelFactory.getMergeRequest(sourceBranch, targetBranch);
            this.mergeRequestRepository.save(mergeRequests[i]);
        }
    }

    /**
     * Checks for the expected content of a GET request for a merge request.
     *
     * @param noMergeRequests   The number of {@link MergeRequest} objects to use during the test.
     */
    private void checkGetGetRequest(int noMergeRequests) {
        // Take a random MR as specimen
        int index = ThreadLocalRandom.current().nextInt(0, noMergeRequests);
        MergeRequest mr = mergeRequestRepository.findAll().get(index);
        String uri = this.getGetUri(mr.getId());

        // Get response body and ensure existence
        ResponseEntity<MergeRequestWeb> response = this.template
            .getForEntity(uri, MergeRequestWeb.class);
        assertTrue(response.hasBody());
        MergeRequestWeb responseBody = response.getBody();

        // Basic response checks
        assertEquals(HttpStatus.OK, response.getStatusCode());

        // Check return data of merge requests
        MergeRequestWeb tempWebMergeRequest = mergeRequests[index].getWebObject();
        assertNotNull(responseBody);
        assertTrue(responseBody.isEqualToMergeRequestWeb(tempWebMergeRequest));
    }

    /**
     * Checks for the expected response of a GET request for a given Merge Request id.
     *
     * @param noMergeRequests   The number of {@link MergeRequest} objects to use during the test.
     * @param id                The id of the {@link MergeRequest} to check for presence.
     */
    private void checkPresenceGetRequest(int noMergeRequests, int id) {
        String uri = this.getGetUri(id);

        // Get response body and check existence
        ResponseEntity<MergeRequestWeb> response = this.template
            .getForEntity(uri, MergeRequestWeb.class);

        if (mergeRequestRepository.findById(id).isPresent()) {
            // By chance, the MR with given id is actually present in the database
            assertEquals(HttpStatus.OK, response.getStatusCode());
        } else {
            // The given id does not correspond to an existent Merge Request
            assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        }
    }

    /**
     * Checks for the expected content of a GET request for list of merge requests.
     *
     * @param noMergeRequests   The number of {@link MergeRequest} objects to use during the test.
     */
    private void checkListGetRequest(int noMergeRequests) {
        String uri = this.getListUri();

        // Get response body and ensure existence
        ResponseEntity<MergeRequestWeb[]> response = this.template
            .getForEntity(uri, MergeRequestWeb[].class);
        assertTrue(response.hasBody());
        List<MergeRequestWeb> responseBody = Arrays.asList(Objects
            .requireNonNull(response.getBody()));

        // Basic response checks
        assertEquals(HttpStatus.OK, response.getStatusCode());

        // Check return data page content for default pageSize = 10
        int noPageElements = Math.min(noMergeRequests, 10);

        assertEquals(noPageElements, responseBody.size());
        for (int i = 0; i < noPageElements; i ++) {
            MergeRequestWeb tempWebMergeRequest = mergeRequests[i].getWebObject();
            MergeRequestWeb responseMergeRequest = responseBody.get(i);
            assertTrue(responseMergeRequest.isEqualToMergeRequestWeb(tempWebMergeRequest));
        }
    }

    /**
     * Checks for the expected content of a given page containing a list of merge requests.
     *
     * @param noMergeRequests   The number of {@link MergeRequest} objects to use during the test.
     * @param pageNo            The number of the page requested in the URL.
     * @throws ParseException   When the fillDatabase method throws a {@link ParseException}.
     */
    private void checkListPagination(int noMergeRequests, int pageNo)
        throws ParseException {
        String uri = this.getPageListUri(pageNo);

        // Get response body and ensure existence
        ResponseEntity<MergeRequestWeb[]> response = this.template
            .getForEntity(uri, MergeRequestWeb[].class);
        List<MergeRequestWeb> responseBody = Arrays.asList(Objects
            .requireNonNull(response.getBody()));

        // Check that given page contains the necessary elements
        int firstId = pageNo * 10; // The id of the first element in a page

        for (int i = 0; i < responseBody.size(); i ++) {
            MergeRequestWeb tempWebMergeRequest = mergeRequests[firstId + i].getWebObject();
            MergeRequestWeb responseMergeRequest = responseBody.get(i);
            assertTrue(responseMergeRequest.isEqualToMergeRequestWeb(tempWebMergeRequest));
        }
    }

    /**
     * Checks for the expected content of a GET request for an empty page.
     *
     * @param noMergeRequests   The number of {@link MergeRequest} objects to use during the test.
     * @param pageNo            The number of the page requested in the URL.
     * @throws ParseException   When the fillDatabase method throws a {@link ParseException}.
     */
    private void checkListEmptyPage(int noMergeRequests, int pageNo)
            throws ParseException {
        String uri = this.getPageListUri(pageNo);

        // Get response body and ensure existence
        ResponseEntity<MergeRequestWeb[]> response = this.template
            .getForEntity(uri, MergeRequestWeb[].class);

        // Check that page is not found
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * Test to check that the Api returns the correct status code when a random merge request id
     * is requested in the URL.
     *
     * @throws ParseException   If a {@link ParseException} is generated during the
     *                              test for expected endpoint behaviour.
     */
    @Test
    public void getPresenceTest() throws ParseException {
        int noMergeRequests = ThreadLocalRandom.current().nextInt(1, 10);
        int nTests = 100; // number of times to repeat test

        this.fillDatabase(noMergeRequests);
        for (int i = 0; i < nTests; i ++) {
            int mergeRequestID = ThreadLocalRandom.current().nextInt();
            this.checkPresenceGetRequest(noMergeRequests, mergeRequestID);
        }
    }

    /**
     * Test to check that the single merge request endpoint behaves as expected.
     *
     * @throws ParseException   If a {@link ParseException} is generated during the
     *                              test for expected endpoint behaviour.
     */
    @Test
    public void getContentTest() throws ParseException {
        int noMergeRequests = ThreadLocalRandom.current().nextInt(1, 10);

        this.fillDatabase(noMergeRequests);
        this.checkGetGetRequest(noMergeRequests);
    }

    /**
     * Test to check that the page list merge request endpoint behaves as expected.
     *
     * @throws ParseException   If a {@link ParseException} is generated during the
     *                              test for expected endpoint behaviour.
     */
    @Test
    public void listPageContentTest() throws ParseException {
        int noMergeRequests = ThreadLocalRandom.current().nextInt(0, 30);

        this.fillDatabase(noMergeRequests);
        this.checkListGetRequest(noMergeRequests);
    }

    /**
     * Test to check that the pagination procedure behaves as expected.
     *
     * @throws ParseException   If a {@link ParseException} is generated during the
     *                              test for expected endpoint behaviour.
     */
    @Test
    public void listPaginationTest() throws ParseException {
        int noMergeRequests = ThreadLocalRandom.current().nextInt(41, 50);
        int pageNo = ThreadLocalRandom.current().nextInt(0, 4);

        this.fillDatabase(noMergeRequests);
        this.checkListPagination(noMergeRequests, pageNo);
    }

    /**
     * Test to check that the API returns a status code of 404 when the requested page is empty.
     *
     * @throws ParseException   If a {@link ParseException} is generated during the
     *                              test for expected endpoint behaviour.
     */
    @Test
    public void listPageNotFoundTest() throws ParseException {
        int noMergeRequests = ThreadLocalRandom.current().nextInt(0, 20);
        int pageNo = 5; // Must be greater than noMergeRequest div 10

        this.fillDatabase(noMergeRequests);
        this.checkListEmptyPage(noMergeRequests, pageNo);
    }

    /**
     * Test to check that the not supported operation throws the correct exception.
     */
    @Test
    public void deleteFlagTest() {
        MergeRequestApiController mergeRequestApiController = new MergeRequestApiController();

        try {
            mergeRequestApiController.deleteFlag(1);
            fail();
        } catch (NotImplementedException e) {
            assertEquals("MergeRequestApiController.deleteFlag: Not yet implemented", e.getMessage());
        }
    }
}
