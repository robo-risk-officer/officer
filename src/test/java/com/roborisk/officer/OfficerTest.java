package com.roborisk.officer;

import com.roborisk.officer.handlers.Dispatcher;
import com.roborisk.officer.mock.GitLabMock;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;

/**
 * Base class for the tests of the RRO.
 */
@TestPropertySource(properties = {
    "spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect",
    "spring.jpa.hibernate.ddl-auto=none",
    "spring.datasource.initialization-mode=always",
    "spring.datasource.continue-on-error=true",
    "spring.jpa.properties.hibernate.enable_lazy_load_no_trans=true",
    "spring.jackson.deserialization.use-long-for-ints=false",
    "ribbon.eureka.enabled=false",
    "officer.gitlab.baseurl=http://localhost:6063",
    "zuul.routes.oauth.url=${officer.gitlab.baseurl}/oauth",
    "officer.cors.allowed-origins=http://localhost:8080",
    "officer.gitlab.robogroup=robo-risk-officer",
    "officer.gitlab.robo_risk_officer.id=18",
    "officer.interceptors.event.enabled=false",
    "officer.interceptors.auth.enabled=false",
    "officer.event.enabled=true",
    "spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true",
    "officer.sonar.enabled=false",
    "officer.sonar.baseurl=localhost:9000",
    "officer.sonar.ssl_enabled=false",
    "officer.sonar.officer_baseurl=http:172.17.0.1:8090",
    "officer.sonar.webhook_key=verysecret",
    "officer.dispatcher.threaded=false"
})
@AutoConfigureEmbeddedDatabase
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OfficerTest extends GitLabMock {

    /**
     * The port that the spring boot server runs on.
     */
    @LocalServerPort
    protected int port;

    /**
     * The flyway instance to use.
     */
    @Autowired
    private Flyway flyway;

    /**
     * The event dispatcher system.
     */
    @Autowired
    protected Dispatcher dispatcher;

    /**
     * Clean flyway before each test.
     */
    @BeforeEach
    public void clean() {
        this.flyway.clean();
        this.flyway.migrate();
    }
}
