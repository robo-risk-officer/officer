package com.roborisk.officer.helpers;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.mock.CommentHelperMock;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.DiscussionThread;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.RobotNote;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.DiscussionThreadRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.repositories.RobotNoteRepository;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Discussion;
import org.gitlab4j.api.models.FileUpload;
import org.gitlab4j.api.models.Note;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the Comment Helper.
 */
public class CommentHelperTest extends OfficerTest {

    /**
     * Id of the GitLab repository for the test.
     */
    private final int repositoryGitLabId = 176;

    /**
     * String to leave on the notes.
     */
    private String noteTestMessage;

    /**
     * GitLabApiWrapper to check if {@link CommentHelper} has executed correctly.
     */
    @Autowired
    GitLabApiWrapper gitLabApiWrapper;

    /**
     * The GitLab repository on which notes are left during the tests.
     */
    private Repository repository;

    /**
     * The GitLab merge request on which notes are left during the tests.
     */
    private MergeRequest mergeRequest;

    /**
     * The GitLab discussion thread used during the tests.
     */
    private DiscussionThread discussionThread1, discussionThread2;

    /**
     * The {@link MergeRequestReview} used during the tests.
     */
    private MergeRequestReview mergeRequestReview;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link MergeRequestReview} objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Database repository to handle {@link DiscussionThread} objects.
     */
    @Autowired
    private DiscussionThreadRepository discussionThreadRepository;

    /**
     * Database repository to handle {@link RobotNote} objects.
     */
    @Autowired
    private RobotNoteRepository robotNoteRepository;

    /**
     * The {@link CommentHelper} which is being tested.
     */
    @Autowired
    private CommentHelper helper;

    /**
     * Handles the general setup of the database objects, (randomized) strings and cleans the
     * GitLab repository.
     */
    @BeforeEach
    private void setUp() {
        this.noteTestMessage = "This is an automatically left thread";

        this.setupDatabaseModels();
    }

    /**
     * Sets up all the database models based on hard coded values form the CommentHelperTestRepo.
     */
    private void setupDatabaseModels() {
        this.repository = new Repository();
        this.repository.setIsEnabled(true);
        this.repository.setGitLabId(this.repositoryGitLabId);
        this.repository.setHttpUrl("http://git.example.nl/robo-risk-officer-testing/" +
                                  "commenthelpertestrepo/edit");
        this.repository.setName("CommentHelperTestRepo");
        this.repository.setNamespace("dontThinkThisIsNecessaryForTheseTests");
        this.repository.setSshUrl("git@git.example.nl:robo-risk-officer-testing/" +
                                 "commenthelpertestrepo.git");
        this.repository.setPathWithNamespace("test-example/testing");

        this.repositoryRepository.save(this.repository);

        GitCommit initial = new GitCommit();
        initial.setParentCommit(null);
        initial.setHash("8c92c55508f36fc0bd704383e0c396f586309cdb");
        initial.setMessage("Hello World!");
        initial.setTimestamp(10000);

        this.gitCommitRepository.save(initial);

        GitCommit update = new GitCommit();
        update.setParentCommit(initial);
        update.setHash("4e94edb0bd361c8ad749635ae26f18e2709fdcdf");
        update.setMessage("Good bye world </3");
        update.setTimestamp(10001);

        this.gitCommitRepository.save(update);

        try {
            initial.setTimestamp("2015-04-08T21:00:25-07:00");
            update.setTimestamp("2015-04-08T21:00:25-07:02");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Branch target = new Branch();
        target.setLatestCommit(initial);
        target.setRepository(this.repository);
        target.setName("master");

        this.branchRepository.save(target);

        Branch source = new Branch();
        source.setRepository(this.repository);
        source.setName("testBranch");
        source.setLatestCommit(update);

        this.branchRepository.save(source);

        this.mergeRequest = new MergeRequest();
        this.mergeRequest.setGitLabId(1);
        this.mergeRequest.setGitLabIid(1);
        this.mergeRequest.setTargetBranch(target);
        this.mergeRequest.setSourceBranch(source);
        this.mergeRequest.setState("OPENED");
        this.mergeRequest.setTitle("Added some lines to the test file");
        this.mergeRequest.setUpdatedAt(2);
        this.mergeRequest.setCreatedAt(0);
        this.mergeRequest.setMergeStatus("OPENED");

        this.mergeRequest = this.mergeRequestRepository.save(this.mergeRequest);

        this.mergeRequestReview = new MergeRequestReview();
        this.mergeRequestReview.setMergeRequest(this.mergeRequest);
        this.mergeRequestReview.setCommit(initial);

        this.mergeRequestReviewRepository.save(this.mergeRequestReview);

        this.discussionThread1 = new DiscussionThread();
        this.discussionThread1.setGitLabId("77b4d2c94e33637a7137ea819ce17dc4939973e6");
        this.discussionThread1.setIsResolved(false);
        this.discussionThread1.setMergeRequest(this.mergeRequest);

        this.discussionThread2 = new DiscussionThread();
        this.discussionThread2.setGitLabId("47379f12df0d7ebaa559f6a41042b01f5eae65cf");
        this.discussionThread2.setIsResolved(false);
        this.discussionThread2.setMergeRequest(this.mergeRequest);
    }

    /**
     * Tests the correct functioning of the CommentHandler.postThread functionality by calling the
     * function and then checking that the MR does indeed have such a note posted.
     *
     * @throws GitLabApiException when an error with the GitLab calls occurs.
     */
    @Test
    public void testPostThread_pass() throws GitLabApiException {
        CommentHelperMock.registerPostThread();

        try {
            this.helper.postNoteForMergeRequestReview(this.mergeRequestReview, this.noteTestMessage);
        } catch (GitLabApiException e) {
            e.printStackTrace();
            fail();
        }

        // --- check if thread has successfully been posted ----

        // grab current discussions and check if it has been set correctly
        List<Discussion> discussions =
            this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi()
                .getMergeRequestDiscussions(this.repositoryGitLabId, this.mergeRequest.getGitLabIid());
        assertNotNull(mergeRequestReview.getDiscussionThread());

        assertTrue(discussions.size() > 0);

        Discussion foundThread = null;
        for (Discussion currentDiscussion : discussions) {
            if (currentDiscussion.getId().equals(mergeRequestReview.getDiscussionThread().getGitLabId())) {
                foundThread = currentDiscussion;
                break;
            }
        }

        assertNotNull(foundThread);
        assertEquals(
                foundThread.getNotes().get(0).getId(),
                robotNoteRepository.findFirstByDiscussionThreadIdOrderByIdAsc(
                        mergeRequestReview.getDiscussionThread().getId()).get().getGitLabId()
        );
    }

    /**
     * Tests the correct error response of CommentHandler.postThread(...) when the GitLabId of the
     * repository is set incorrectly.
     *
     * @throws GitLabApiException when an error with the GitLab calls occurs.
     */
    @Test
    public void testPostThread_wrongRepository() throws GitLabApiException {
        CommentHelperMock.register();

        List<Note> previousNotes =
            this.gitLabApiWrapper.getGitLabApi().getNotesApi()
                .getMergeRequestNotes(this.repositoryGitLabId, this.mergeRequest.getGitLabIid());

        CommentHelperMock.registerWrongRepository();

        // change the repositories GitLab id to something invalid
        this.mergeRequest.getTargetBranch().getRepository().setGitLabId(1);

        // execute query
        try {
            this.helper.postNoteForMergeRequestReview(this.mergeRequestReview,
                this.noteTestMessage);
            fail();
        } catch (GitLabApiException e) {
            e.printStackTrace();
        }
        assertNull(mergeRequestReview.getDiscussionThread());

        // check GitLab
        List<Note> currentNotes = this.gitLabApiWrapper.getGitLabApi().getNotesApi()
                .getMergeRequestNotes(this.repositoryGitLabId, this.mergeRequest.getGitLabIid());
        assertEquals(currentNotes.size(), previousNotes.size());
    }

    /**
     * Tests the correct error response of CommentHandler.postThread(...) when the GitLabIid of the
     * merge request is set incorrectly.
     *
     * @throws GitLabApiException when an error with the GitLab calls occurs.
     */
    @Test
    public void testPostThread_wrongMergeRequest() throws GitLabApiException {
        CommentHelperMock.register();

        int originalRepoId = this.mergeRequest.getGitLabIid();
        this.mergeRequest.setGitLabIid(100000);
        // ---- change the merge request GitLab iid to something invalid
        List<Note> previousNotes =
            this.gitLabApiWrapper.getGitLabApi().getNotesApi().getMergeRequestNotes(
                    this.repositoryGitLabId, originalRepoId);
        // ---- execute query
        try {
            this.helper.postNoteForMergeRequestReview(this.mergeRequestReview,
                this.noteTestMessage);
            fail();
        } catch (GitLabApiException e) {
            e.printStackTrace();
        }
        assertNull(mergeRequestReview.getDiscussionThread());
        // check GitLab
        List<Note> currentNotes = this.gitLabApiWrapper.getGitLabApi().getNotesApi()
                .getMergeRequestNotes(this.repositoryGitLabId,
                originalRepoId);
        assertEquals(currentNotes.size(), previousNotes.size());
    }

    /**
     * Takes in the resolved state of each note and checks if this corresponds to what is
     * actually the case on the GitLab merge request page.
     *
     * @param isResolved Map from the hash of all notes to whether they are resolved or not
     * @throws GitLabApiException when an error with the GitLab calls occurs.
     */
    private void checkMergeRequestResolves(Map<String, Boolean> isResolved)
        throws GitLabApiException {
        List<Discussion> discussions =
            this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi()
                .getMergeRequestDiscussions(this.repositoryGitLabId, this.mergeRequest.getGitLabIid());
        assertEquals(discussions.size(), isResolved.size());
        for (Discussion discussion : discussions) {
            List<Note> notes = discussion.getNotes();
            for (Note note : notes) {
                assertEquals(isResolved.get(discussion.getId()), note.getResolved());
            }
        }
    }

    /**
     * Queries the GitLab merge request for all notes and creates a map from their hash to
     * whether they are currently resolved or not.
     *
     * @throws GitLabApiException when an error with the GitLab calls occurs.
     * @return Map from the hash of all notes to whether they are resolved or not
     */
    private Map<String, Boolean> getMergeRequestResolved() throws GitLabApiException {
        List<Discussion> discussions =
            this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi()
                .getMergeRequestDiscussions(this.repositoryGitLabId, this.mergeRequest.getGitLabIid());
        Map<String, Boolean> isResolved = new HashMap<>();
        for (Discussion discussion : discussions) {
            isResolved.put(discussion.getId(),
                discussion.getNotes().get(0).getResolved());
        }

        return isResolved;
    }

    /**
     * Tests the correct functioning of CommentHandler.addNoteToThread(...) by querying the
     * GitLab page for all notes, then calling the function and then querying the GitLab again to
     * check if it has been posted correctly.
     *
     * @throws GitLabApiException when an error with the GitLab calls occurs.
     */
    @Test
    public void testAddNoteToThread_pass() throws GitLabApiException {
        CommentHelperMock.register();

        // grab previous notes to compare against
        List<Note> previousNotes =
                this.gitLabApiWrapper.getGitLabApi().getNotesApi().getMergeRequestNotes(
                    this.repositoryGitLabId, this.mergeRequest.getGitLabIid());

        // save instances in database such that the CommentHelper won't complain
        this.repositoryRepository.save(this.repository);
        this.mergeRequestRepository.save(this.mergeRequest);
        this.discussionThreadRepository.save(this.discussionThread2);

        // execute query
        RobotNote note = this.helper.addNoteToThread(this.discussionThread2,
                this.noteTestMessage);

        // check results of query
        assertNotNull(note);

        CommentHelperMock.registerSuccess();

        List<Note> currentNotes = this.gitLabApiWrapper.getGitLabApi().getNotesApi()
                                      .getMergeRequestNotes(this.repositoryGitLabId, this.mergeRequest.getGitLabIid());

        assertEquals(previousNotes.size() + 1, currentNotes.size());
        boolean foundPostedNote = false;
        for (Note currentNote : currentNotes) {
            if (currentNote.getId().equals(note.getGitLabId())) {
                foundPostedNote = true;
                assertEquals(currentNote.getBody(), this.noteTestMessage);
            }
        }
        assertTrue(foundPostedNote);
    }

    /**
     * Tests the correct error response of CommentHandler.addNoteToThread(...) when the GitLabId of
     * the discussion id is set incorrectly.
     */
    @Test
    public void testAddNoteToThread_wrongDiscussionId() {
        this.discussionThread2.setGitLabId("77b4d2c94e33637a7137eaaaace17dc4939973e6"); //this value
        // is wrong

        try {
            this.helper.addNoteToThread(this.discussionThread2,
                    this.noteTestMessage);
            fail();
        } catch (GitLabApiException e) {
            assertEquals(404, e.getHttpStatus());
        }
    }

    /**
     * Tests the correct error response of CommentHandler.addNoteToThread(...) when the GitLabId of
     * the merge request id is set incorrectly.
     */
    @Test
    public void testAddNoteToThread_wrongMRId() {
        this.mergeRequest.setGitLabIid(100000);

        try {
            this.helper.addNoteToThread(this.discussionThread2,
                this.noteTestMessage);
            fail();
        } catch (GitLabApiException e) {
            assertEquals(404, e.getHttpStatus());
        }
    }

    /**
     * Tests the correct error response of CommentHandler.addNoteToThread(...) when the GitLabId of
     * the repository id is set incorrectly.
     */
    @Test
    public void testAddNoteToThread_wrongRepositoryId() {
        this.repository.setGitLabId(1000);

        try {
            this.helper.addNoteToThread(this.discussionThread2,
                this.noteTestMessage);
            fail();
        } catch (GitLabApiException e) {
            assertEquals(404, e.getHttpStatus());
        }
    }


    /**
     * Tests the correct execution of CommentHandler.uploadFile(...).
     */
    @Test
    public void uploadFile_pass() throws GitLabApiException, IOException {
        File file = new File("test.png");
        file.createNewFile();
        file.deleteOnExit();

        CommentHelperMock.register();

        FileUpload upload = this.helper.uploadImage(this.repositoryGitLabId, file);

        System.out.println(upload.getMarkdown());
        assertNotNull(upload);
    }

    /**
     * Tests the correct error response of CommentHandler.uploadFile(...) when the repository's
     * GitLab id is invalid.
     */
    @Test
    public void uploadFile_wrongRepoId()  {
        File file = new File("test.png");
        file.deleteOnExit();

        FileUpload upload = null;
        try {
            upload = this.helper.uploadImage(1232213, file);
            fail();
        } catch (GitLabApiException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tests the correct error response of CommentHandler.uploadFile(...) when the file to be
     * uploaded is null.
     */
    @Test
    public void uploadFile_noFile()  {
        try {
            this.helper.uploadImage(1232213, null);
            fail();
        } catch (GitLabApiException e) {
            e.printStackTrace();
        }
    }

    /**
     * Edit a message and check if it was in fact correctly edited.
     */
    private void checkEditMessage(String message, DiscussionThread discussionThread) throws GitLabApiException {
        // change message of the first note to firstMessage and check
        this.helper.modifyDiscussionThread(discussionThreadRepository.findByGitLabId(discussionThread.getGitLabId()).get(), null,
            message);

        List<Discussion> discussions =
            this.gitLabApiWrapper.getGitLabApi().getDiscussionsApi()
                .getMergeRequestDiscussions(this.repositoryGitLabId, this.mergeRequest.getGitLabIid());

        Optional<Discussion> gitLabDiscussion =
            discussions.stream().filter(discussion -> discussion.getId().equals(discussionThread.getGitLabId())).findFirst();

        assertNotNull(gitLabDiscussion.get());
        assertEquals(gitLabDiscussion.get().getNotes().get(0).getBody(), message);
    }

    /**
     * Test for the CommentHelper.modifyDiscussionThread(...) which modifies a discussion thread
     * twice and each time checks if it has been modified correctly.
     *
     * @throws GitLabApiException   If any exceptions occur with the {@link GitLabApiWrapper}.
     */
    @Test
    public void testEditComment_pass() throws GitLabApiException {
        CommentHelperMock.registerPostThread();

        DiscussionThread discussionThread = new DiscussionThread();
        discussionThread.setMergeRequest(mergeRequest);
        discussionThread.setIsResolved(false);
        discussionThread.setGitLabId("77b4d2c94e33637a7137ea819ce17dc4939973e6");

        RobotNote note = new RobotNote();
        note.setDiscussionThread(discussionThread);
        note.setGitLabId(5202);

        discussionThreadRepository.save(discussionThread);
        robotNoteRepository.save(note);

        checkEditMessage("hey this is message no 1", discussionThread);
        CommentHelperMock.registerEditThreadSuccess();
        checkEditMessage("hey this is message no 2 which is different from 1", discussionThread);
    }
}
