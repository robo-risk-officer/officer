package com.roborisk.officer.helpers;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.web.CommentWebhookData;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.MergeRequestWeb;
import com.roborisk.officer.models.web.MergeRequestWebhookData;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class providing tests for the {@link MergeRequestWebHelper} class.
 */
public class MergeRequestWebHelperTest extends OfficerTest {

    /**
     * Singleton instance of the {@link MergeRequestWebHelper} class.
     */
    @Autowired
    MergeRequestWebHelper mergeRequestWebHelper;

    /**
     * Retrieve map representing JSON representing a merge request.
     *
     * @return {@link Map}<{@link String}, {@link Object}> representing the JSON.
     */
    private Map<String, Object> getMRJson() {
        // Maps representing the JSON
        Map<String, Object> authormap = new LinkedHashMap<>();
        Map<String, Object> commitmap = new LinkedHashMap<>();
        Map<String, Object> map = new LinkedHashMap<>();

        // Add random data
        authormap.put("name", "name");
        authormap.put("email", "email");

        // Add more random data
        commitmap.put("id", "hashsishe");
        commitmap.put("message", "some message");
        commitmap.put("modified", new ArrayList<>());
        commitmap.put("removed", new ArrayList<>());
        commitmap.put("added", new ArrayList<>());
        commitmap.put("timestamp", "2015-04-08T21:00:25-07:00");
        commitmap.put("author", authormap);

        // Add more more random data
        map.put("id", 1);
        map.put("iid", 2);
        map.put("target_branch", "target");
        map.put("source_branch", "source");
        map.put("author_id", 4);
        map.put("action", "opened");
        map.put("state", "open");
        map.put("title", "Some merge request");
        map.put("created_at", "2015-04-08T21:00:25-07:00");
        map.put("updated_at", "2015-04-08T21:00:25-07:00");
        map.put("merge_status", "can_be_merged");
        map.put("work_in_progress", false);
        map.put("last_commit", commitmap);

        return map;
    }

    /**
     * Test to check that a normal CommentWebhookData object is correctly handled.
     *
     * @throws IOException
     * @throws ParseException
     */
    @Test
    void extractMergeRequestCommentWebhookData() throws IOException, ParseException {
        CommentWebhookData commentWebhookData = new CommentWebhookData();

        commentWebhookData.setMergeRequest(this.getMRJson());

        MergeRequestWeb result = mergeRequestWebHelper.extractMergeRequest(commentWebhookData);

        assertNotNull(result);
    }

    /**
     * Test to check that a CommentWebhookData object without a merge request throws the correct
     * exception.
     *
     * @throws IOException
     * @throws ParseException
     */
    @Test
    void extractMergeRequestCommentWebhookDataWithoutMR() {
        CommentWebhookData commentWebhookData = new CommentWebhookData();

        try {
            mergeRequestWebHelper.extractMergeRequest(commentWebhookData);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("MergeRequestWebHelper.extractMergeRequest "
                + "CommentWebhookData does not have a merge request.", e.getMessage());
        }
    }

    /**
     * Test to check that a normal MergeRequestWebhookData object is correctly handled.
     *
     * @throws IOException
     * @throws ParseException
     */
    @Test
    void extractMergeRequestMergeRequestWebhookData() throws IOException, ParseException {
        MergeRequestWebhookData mergeRequestWebhookData = new MergeRequestWebhookData();

        mergeRequestWebhookData.setMergeRequest(this.getMRJson());

        MergeRequestWeb result = mergeRequestWebHelper.extractMergeRequest(mergeRequestWebhookData);

        assertNotNull(result);
    }

    /**
     * Test to check that an unsupported AbstractWebhookData object throws the correct exception.
     *
     * @throws IOException
     * @throws ParseException
     */
    @Test
    void extractMergeRequestWrongType() {
        CommitWebhookData commitWebhookData = new CommitWebhookData();

        try {
            mergeRequestWebHelper.extractMergeRequest(commitWebhookData);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("MergeRequestWebHelper.extractMergeRequest "
                + "only supports CommentWebhookData and MergeRequestWebhookData.", e.getMessage());
        }
    }





}
