package com.roborisk.officer.helpers.comments;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.*;
import com.roborisk.officer.models.repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Class providing tests for the {@link ArtefactReviewCommentHelper} class.
 */
public class ArtefactReviewCommentHelperTest extends OfficerTest {

    /**
     * Repository for storing {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Repository for storing {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * Repository for storing {@link Branch} objects.
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Repository for storing {@link MergeRequest} objects.
     */
    @Autowired
    MergeRequestRepository mergeRequestRepository;

    /**
     * Local instance of the {@link ArtefactReviewCommentHelper} class.
     */
    @Autowired
    ArtefactReviewCommentHelper artefactReviewCommentHelper;

    /**
     * Check that the correct exception is thrown if no merge request review is found for the
     * supplied merge request.
     *
     * @throws ParseException
     */
    @Test
    void getAllFilesForLatestReviewReviewNotFound() throws ParseException {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        GitCommit gitCommit = ModelFactory.getGitCommit();
        gitCommitRepository.save(gitCommit);

        Branch source = ModelFactory.getBranch(repository, gitCommit);
        branchRepository.save(source);
        Branch target = ModelFactory.getBranch(repository, gitCommit);
        branchRepository.save(target);

        MergeRequest mergeRequest = ModelFactory.getMergeRequest(source, target);
        mergeRequestRepository.save(mergeRequest);

        try {
            artefactReviewCommentHelper.getAllFilesForLatestReview(mergeRequest);
            fail();
        } catch (IllegalStateException e) {
            assertEquals("ArtefactReviewCommentHelper.getAllFilesForLatestReview: could" +
                " not retrieve latest review for merge request with id " + mergeRequest.getId(),
                e.getMessage());
        }
    }

}
