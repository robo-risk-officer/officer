package com.roborisk.officer.helpers;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.mock.BranchInitMetricLinkHelperMock;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.BranchClassObservation;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.BranchFunctionObservation;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.metrics.BranchClassMetricResult;
import com.roborisk.officer.models.database.metrics.BranchCodebaseMetricResult;
import com.roborisk.officer.models.database.metrics.BranchFileMetricResult;
import com.roborisk.officer.models.database.metrics.BranchFunctionMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.enums.MetricTypeEnum;
import com.roborisk.officer.models.repositories.BranchClassMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchClassObservationRepository;
import com.roborisk.officer.models.repositories.BranchCodebaseMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchFileMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import com.roborisk.officer.models.repositories.BranchFunctionMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchFunctionObservationRepository;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MetricRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.JSONObjectBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Tests the functionality of {@link BranchInitMetricLinkHelper}
 */
public class BranchInitMetricLinkHelperTest extends OfficerTest {

    /**
     * Singleton instance of the {@link BranchInitMetricLinkHelper} class.
     */
    @Autowired
    BranchInitMetricLinkHelper branchInitMetricLinkHelper;

    /**
     * Http client.
     */
    @Autowired
    private TestRestTemplate template;

    /**
     * Base path of the JSON files used in test cases.
     */
    private static final String jsonBasePath = "src/test/resources/__files/branch_init_link_helper";

    /**
     * @{link Repository} to test with.
     */
    private Repository repository;

    /**
     * GitLab id of the repository to test with.
     */
    private static final int repositoryId = 161;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Master {@link Branch} to test with.
     */
    private Branch masterBranch;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * {@link GitCommit} for the master branch.
     */
    private GitCommit masterBranchCommit;

    /**
     * The hash of the master branch's commit, to match the JSON API response.
     */
    private static final String masterBranchCommitHash = "369edf06e7fe19fd415c98850951c8208f1e5c00";

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * {@link BranchFile} to test with.
     */
    private BranchFile branchFile;

    /**
     * Database repository to handle {@link BranchFile} objects.
     */
    @Autowired
    private BranchFileRepository branchFileRepository;

    /**
     * {@link BranchClassMetricResult} to test with.
     */
    private BranchClassMetricResult branchClassMetricResult;

    /**
     * Database repository to handle {@link BranchClassMetricResult} objects.
     */
    @Autowired
    private BranchClassMetricResultRepository branchClassMetricResultRepository;

    /**
     * {@link BranchClassObservation} to test with.
     */
    private BranchClassObservation branchClassObservation;

    /**
     * Database repository to handle {@link BranchClassObservation} objects.
     */
    @Autowired
    private BranchClassObservationRepository branchClassObservationRepository;

    /**
     * {@link BranchCodebaseMetricResult} to test with.
     */
    private BranchCodebaseMetricResult branchCodebaseMetricResult;

    /**
     * Database repository to handle {@link BranchCodebaseMetricResult} objects.
     */
    @Autowired
    private BranchCodebaseMetricResultRepository branchCodebaseMetricResultRepository;

    /**
     * {@link BranchFileMetricResult} to test with.
     */
    private BranchFileMetricResult branchFileMetricResult;

    /**
     * Database repository to handle {@link BranchFileMetricResult} objects.
     */
    @Autowired
    private BranchFileMetricResultRepository branchFileMetricResultRepository;

    /**
     * {@link BranchFunctionMetricResult} to test with.
     */
    private BranchFunctionMetricResult branchFunctionMetricResult;

    /**
     * Database repository to handle {@link BranchFunctionMetricResult} objects.
     */
    @Autowired
    private BranchFunctionMetricResultRepository branchFunctionMetricResultRepository;

    /**
     * {@link BranchFunctionObservation} to test with.
     */
    private BranchFunctionObservation branchFunctionObservation;

    /**
     * Database repository to handle {@link BranchFunctionObservation} objects.
     */
    @Autowired
    private BranchFunctionObservationRepository branchFunctionObservationRepository;

    /**
     * {@link Metric} to test with.
     */
    private Metric metric;

    /**
     * Database repository to handle {@link Metric} objects.
     */
    @Autowired
    private MetricRepository metricRepository;

    /**
     * The name of the newly created branch.
     * Should match the JSON webhook definitions.
     */
    private static final String newBranchName = "test-for-webhook2";

    /**
     * The hash of the commit on the new branch.
     * Should match the JSON webhook definitions.
     */
    private static final String newBranchCommitHash = "3cb9217789bb529fc3f704c3d4afff1b29309175";

    /**
     * Method to register mock GitLab API endpoints before each test case and initialise the
     * database structures necessary for testing.
     */
    @BeforeEach
    public void setup() throws ParseException {
        // Register API endpoints
        BranchInitMetricLinkHelperMock.register();

        // Setup database
        // Create the repository to test with
        this.repository = ModelFactory.getRepository(this.repositoryId);
        this.repositoryRepository.save(this.repository);
        // Create the branch that supposedly was branched off (i.e. master branch)
        // Also create supporting objects
        this.masterBranchCommit = ModelFactory.getGitCommit(this.masterBranchCommitHash);
        this.gitCommitRepository.save(this.masterBranchCommit);
        this.masterBranch = ModelFactory.getBranch(this.repository, this.masterBranchCommit,
                "master");
        this.branchRepository.save(this.masterBranch);

        // Add all objects that should be copied to the master branch
        this.branchFile = ModelFactory.getBranchFile(this.masterBranch, this.masterBranchCommit);
        this.branchFileRepository.save(this.branchFile);

        this.branchClassObservation = ModelFactory.getBranchClassObservation(
                this.masterBranchCommit, this.branchFile);
        this.branchClassObservationRepository.save(this.branchClassObservation);
        this.metric = ModelFactory.getMetric();
        this.metric.setMetricType(MetricTypeEnum.FILE);
        this.metricRepository.save(metric);
        this.branchClassMetricResult = ModelFactory.getBranchClassMetricResult(
                this.branchClassObservation, this.metric);
        this.branchClassMetricResult.setIsLowQuality(true);
        this.branchClassMetricResultRepository.save(this.branchClassMetricResult);

        this.branchCodebaseMetricResult = ModelFactory.getBranchCodebaseMetricResult(
                this.masterBranch, this.masterBranchCommit, this.metric);
        this.branchCodebaseMetricResultRepository.save(this.branchCodebaseMetricResult);

        this.branchFileMetricResult = ModelFactory.getBranchFileMetricResult(
                this.masterBranchCommit, this.branchFile, this.metric);
        this.branchFileMetricResultRepository.save(this.branchFileMetricResult);

        this.branchFunctionObservation = ModelFactory.getBranchFunctionObservation(
                masterBranchCommit, branchFile);
        this.branchFunctionObservationRepository.save(this.branchFunctionObservation);
        this.branchFunctionMetricResult = ModelFactory.getBranchFunctionMetricResult(
                this.branchFunctionObservation, this.metric);
        this.branchFunctionMetricResult.setIsLowQuality(true);
        this.branchFunctionMetricResultRepository.save(this.branchFunctionMetricResult);
    }

    /**
     * Test case to test correct {@link Branch} initialisation on receipt of
     * {@link com.roborisk.officer.models.web.CommitWebhookData}.
     */
    @Test
    public void CommitTest() throws IOException, JSONException {
        // Setup the commit webhook for a new branch in an existing repository
        JSONObject body = JSONObjectBuilder.fromFile(this.jsonBasePath
                + "/commit_webhook.json");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(body.toString(), headers);
        String uri = "http://localhost:" + port + "/webhook/commit";

        // Fire the webhook
        ResponseEntity<String> response = this.template.postForEntity(uri, request, String.class);
        // and check that the webhook returns success
        assertEquals(HttpStatus.OK, response.getStatusCode());

        this.dispatcher.run();

        // Verify that the correct objects were copied to the new branch
        this.verifyObjectCreation();
    }

    /**
     * Test case to test correct {@link Branch} initialisation on receipt of
     * {@link com.roborisk.officer.models.web.MergeRequestWebhookData}.
     */
    @Test
    public void MergeTest() throws IOException, JSONException {
        // Setup the commit webhook for a new branch in an existing repository
        JSONObject body = JSONObjectBuilder.fromFile(this.jsonBasePath
                + "/mergerequest_webhook.json");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(body.toString(), headers);
        String uri = "http://localhost:" + port + "/webhook/merge-request";

        // Fire the webhook
        ResponseEntity<String> response = this.template.postForEntity(uri, request, String.class);
        // and check that the webhook returns success
        assertEquals(HttpStatus.OK, response.getStatusCode());

        this.dispatcher.run();

        // Verify that the correct objects were copied to the new branch
        this.verifyObjectCreation();
    }

    /**
     * Method to verify that the necessary objects were copied to the new branch after a
     * webhook is called.
     */
    private void verifyObjectCreation() {
        // First, check that the branch has been created
        Optional<Branch> databaseNewBranch = this.branchRepository.findByNameAndRepositoryId(
                this.newBranchName, this.repository.getId());
        assertTrue(databaseNewBranch.isPresent());
        // And extract it
        Branch newBranch = databaseNewBranch.get();

        // And that the commit has been stored
        Optional<GitCommit> databaseNewBranchCommit = this.gitCommitRepository.findByHash(
                this.newBranchCommitHash);
        assertFalse(databaseNewBranchCommit.isEmpty());
        // And extract it
        GitCommit newBranchCommit = databaseNewBranchCommit.get();

        // Next, check that the branch file was copied over
        Optional<BranchFile> databaseNewBranchFile = this.branchFileRepository
                .findByBranchIdAndFilePath(newBranch.getId(), this.branchFile.getFilePath());
        assertFalse(databaseNewBranchFile.isEmpty());
        // And that it has the correct properties
        assertEquals(this.branchFile.getFileName(), databaseNewBranchFile.get().getFileName());
        assertEquals(this.branchFile.getFilePath(), databaseNewBranchFile.get().getFilePath());
        assertEquals(this.branchFile.getIsDeleted(), databaseNewBranchFile.get().getIsDeleted());
        assertEquals(newBranch.getId(), databaseNewBranchFile.get().getBranch().getId());

        // Check that the BranchFileMetricResult was copied over
        List<BranchFileMetricResult> databaseBfimrList =
            this.branchFileMetricResultRepository.findAllByBranchFile(databaseNewBranchFile.get());
        assertEquals(1, databaseBfimrList.size());
        // Extract it and verify its properties
        BranchFileMetricResult bfimr = databaseBfimrList.get(0);
        assertNotNull(bfimr);
        assertEquals(this.branchFileMetricResult.getMetric().getId(), bfimr.getMetric().getId());
        assertEquals(false, bfimr.getIsLowQuality());
        assertEquals(this.branchFileMetricResult.getScore(), bfimr.getScore());
        assertEquals(newBranch.getId(), bfimr.getLastChangeCommit().getId());

        // Next check that the BranchClassMetricResult was copied over
        // This is done by checking that the associated class observation was copied correctly
        // and then retrieving the results associated to it
        List<BranchClassObservation> databaseBcobList = this.branchClassObservationRepository
                .findAllByBranchFile(databaseNewBranchFile.get());
        assertEquals(1, databaseBcobList.size());
        // Extract it and verify its properties
        BranchClassObservation bcob = databaseBcobList.get(0);
        assertNotNull(bcob);
        assertEquals(this.branchClassObservation.getClassName(), bcob.getClassName());
        assertEquals(this.branchClassObservation.getLineNumber(), bcob.getLineNumber());

        // Next get the BranchClassMetricResult
        List<BranchClassMetricResult> databaseBcmrList =
                this.branchClassMetricResultRepository.findAllByBranchClassObservation(bcob);
        assertEquals(1, databaseBcmrList.size());
        // Extract it, and verify its properties
        BranchClassMetricResult bcmr = databaseBcmrList.get(0);
        assertNotNull(bcmr);
        assertEquals(this.branchClassMetricResult.getMetric().getId(), bcmr.getMetric().getId());
        assertFalse(bcmr.getIsLowQuality());
        assertEquals(this.branchClassMetricResult.getScore(), bcmr.getScore());

        // Similarly check that the BranchFunctionMetricResult was copied over
        // This is done by checking that the associated function observation was copied correctly
        // and then retrieving the results associated to it
        List<BranchFunctionObservation> databaseBfuoList = this.branchFunctionObservationRepository
                .findAllByBranchFile(databaseNewBranchFile.get());
        assertEquals(1, databaseBfuoList.size());
        // Extract it and verify its properties
        BranchFunctionObservation bfuo = databaseBfuoList.get(0);
        assertNotNull(bfuo);
        assertEquals(this.branchFunctionObservation.getLineNumber(), bfuo.getLineNumber());
        assertEquals(this.branchFunctionObservation.getClassName(), bfuo.getClassName());
        assertEquals(this.branchFunctionObservation.getFunctionName(), bfuo.getFunctionName());

        // Next get teh BranchFunctionMetricResult
        List<BranchFunctionMetricResult> databaseBfumrList =
            this.branchFunctionMetricResultRepository.findAllByBranchFunctionObservation(bfuo);
        assertEquals(1, databaseBfumrList.size());
        // Extract it and verify its properties
        BranchFunctionMetricResult bfumr = databaseBfumrList.get(0);
        assertNotNull(bfumr);
        assertEquals(this.branchFunctionMetricResult.getMetric().getId(),
            bfumr.getMetric().getId());
        assertEquals(this.branchFunctionMetricResult.getScore(), bfumr.getScore());
        assertEquals(false, bfumr.getIsLowQuality());

        // Finally check that the BranchCodebaseMetricResult was copied over
        List<BranchCodebaseMetricResult> databaseBcbmrList =
                this.branchCodebaseMetricResultRepository.findAllByBranch(newBranch);
        assertEquals(1, databaseBcbmrList.size());
        // Extract it and verify its properties
        BranchCodebaseMetricResult bcbmr = databaseBcbmrList.get(0);
        assertNotNull(bcbmr);
        assertEquals(this.branchCodebaseMetricResult.getMetric().getId(),
                bcbmr.getMetric().getId());
        assertEquals(false, bcbmr.getIsLowQuality());
        assertEquals(this.branchCodebaseMetricResult.getScore(), bcbmr.getScore());
        assertEquals(newBranch.getId(), bcbmr.getBranch().getId());

        // Test passed
    }

    /**
     * Test for checking that the correct exception is thrown if the master branch is not present.
     */
    @Test
    void noMasterBranch() {
        this.branchRepository.deleteAll();

        Branch newBranch = ModelFactory.getBranch(this.repository, this.masterBranchCommit);
        newBranch.setName(newBranchName);

        try {
            this.branchInitMetricLinkHelper.initialiseBranch(newBranch);
            fail();
        } catch (IllegalStateException e) {
            assertTrue(e.getMessage().startsWith("BranchInitMetricLinkHelper.initialiseBranch: could not"
                + " locate the master branch for the repository with ID"));
        }
    }
}
