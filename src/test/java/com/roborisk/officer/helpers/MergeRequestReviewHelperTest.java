package com.roborisk.officer.helpers;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.GitCommitWeb;
import com.roborisk.officer.models.web.MergeRequestWeb;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * This class tests the {@link MergeRequestReviewHelper}.
 */
public class MergeRequestReviewHelperTest extends OfficerTest {

    /**
     * The class to test.
     */
    @Autowired
    private MergeRequestReviewHelper mrrHelper;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Database repository to handle {@link MergeRequestReview} objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Tests the reviewedMrOnSameState function for when it should return true.
     * Throws a GitLabAPIException when getting the discussion id it should not comment.
     *
     * @throws ParseException   When time conversion errors.
     */
    @Test
    public void testReviewedMrOnSameStateTrue() throws ParseException {
        // Initialising all database models.
        GitCommit commit = ModelFactory.getGitCommit();
        Repository repository = ModelFactory.getRepository();
        Branch source = ModelFactory.getBranch(repository, commit, "source");
        Branch target = ModelFactory.getBranch(repository, commit, "target");
        MergeRequest mr = ModelFactory.getMergeRequest(source, target);
        MergeRequestReview mrr = ModelFactory.getMergeRequestReview(commit, mr);

        // Initialising merge request web.
        MergeRequestWeb mergeRequestWeb =  mr.getWebObject();
        GitCommitWeb commitWeb = new GitCommitWeb();
        commitWeb.setHash(commit.getHash());
        mergeRequestWeb.setLastCommit(commitWeb);

        // Saving all models in the database.
        this.gitCommitRepository.save(commit);
        this.repositoryRepository.save(repository);
        this.branchRepository.save(source);
        this.branchRepository.save(target);
        this.mergeRequestRepository.save(mr);
        this.mergeRequestReviewRepository.save(mrr);

        // Call the helper function.
        boolean check = this.mrrHelper.reviewedMrOnSameState(mergeRequestWeb, 10);

        // Check result.
        assertTrue(check);
    }

    /**
     * Test should return false because the merge request is not in the database.
     *
     * @throws ParseException   When time conversion errors.
     */
    @Test
    public void testReviewedMrOnSameStateFalse() throws ParseException {
        GitCommit commit = ModelFactory.getGitCommit();
        Repository repository = ModelFactory.getRepository();
        Branch source = ModelFactory.getBranch(repository, commit, "source");
        Branch target = ModelFactory.getBranch(repository, commit, "target");
        MergeRequest mr = ModelFactory.getMergeRequest(source, target);

        // Initialising merge request web.
        MergeRequestWeb mergeRequestWeb =  mr.getWebObject();

        // Call the helper function.
        boolean check = this.mrrHelper.reviewedMrOnSameState(mergeRequestWeb, 10);
        assertFalse(check);
    }

    /**
     * Test should fail because the merge request review is not in the database.
     *
     * @throws ParseException   When time conversion errors.
     */
    @Test
    public void testReviewedMrOnSameStateFalse2() throws ParseException {
        GitCommit commit = ModelFactory.getGitCommit();
        Repository repository = ModelFactory.getRepository();
        Branch source = ModelFactory.getBranch(repository, commit, "source");
        Branch target = ModelFactory.getBranch(repository, commit, "target");
        MergeRequest mr = ModelFactory.getMergeRequest(source, target);

        // Initialising merge request web.
        MergeRequestWeb mergeRequestWeb =  mr.getWebObject();

        // Saving all models in the database.
        this.gitCommitRepository.save(commit);
        this.repositoryRepository.save(repository);
        this.branchRepository.save(source);
        this.branchRepository.save(target);
        this.mergeRequestRepository.save(mr);

        // Call the helper function.
        boolean check = this.mrrHelper.reviewedMrOnSameState(mergeRequestWeb, 10);
        assertFalse(check);
    }

    /**
     * Test should fail because the hashes are not equal.
     *
     * @throws ParseException   When time conversion errors.
     */
    @Test
    public void testReviewedMrOnSameStateFalse3() throws ParseException {
        // Initialising all database models.
        GitCommit commit = ModelFactory.getGitCommit();
        Repository repository = ModelFactory.getRepository();
        Branch source = ModelFactory.getBranch(repository, commit, "source");
        Branch target = ModelFactory.getBranch(repository, commit, "target");
        MergeRequest mr = ModelFactory.getMergeRequest(source, target);
        MergeRequestReview mrr = ModelFactory.getMergeRequestReview(commit, mr);
        mrr.getCommit().setHash("something");

        // Initialising merge request web.
        MergeRequestWeb mergeRequestWeb =  mr.getWebObject();
        GitCommitWeb commitWeb = new GitCommitWeb();
        commitWeb.setHash("something else");
        mergeRequestWeb.setLastCommit(commitWeb);

        // Saving all models in the database.
        this.gitCommitRepository.save(commit);
        this.repositoryRepository.save(repository);
        this.branchRepository.save(source);
        this.branchRepository.save(target);
        this.mergeRequestRepository.save(mr);
        this.mergeRequestReviewRepository.save(mrr);

        // Call the helper function.
        boolean check = this.mrrHelper.reviewedMrOnSameState(mergeRequestWeb, 10);
        assertFalse(check);
    }
}
