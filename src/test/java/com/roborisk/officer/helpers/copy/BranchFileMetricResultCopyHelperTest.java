package com.roborisk.officer.helpers.copy;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.*;
import com.roborisk.officer.models.database.metrics.BranchFileMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.enums.MetricTypeEnum;
import com.roborisk.officer.models.repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Class providing tests for the {@link BranchFileMetricResultCopyHelper} class.
 */
public class BranchFileMetricResultCopyHelperTest extends OfficerTest {

    /**
     * Local instance of the {@link BranchFileMetricResultCopyHelper} class.
     */
    @Autowired
    BranchFileMetricResultCopyHelper branchFileMetricResultCopyHelper;

    /**
     * Repository for handling {@link Metric} objects.
     */
    @Autowired
    MetricRepository metricRepository;

    /**
     * Repository for handling {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Repository for handling {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * Repository for handling {@link Branch} objects.
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Repository for handling {@link BranchFile} objects.
     */
    @Autowired
    BranchFileRepository branchFileRepository;

    /**
     * Repository for handling {@link BranchFileMetricResult} objects.
     */
    @Autowired
    BranchFileMetricResultRepository branchFileMetricResultRepository;

    /**
     * Test checking whether the function correctly handles the case where the result has already been copied.
     *
     * @throws ParseException
     */
    @Test
    void copyAlreadyPresent() throws ParseException {
        this.metricRepository.deleteAll();

        Metric metric = ModelFactory.getMetric();
        metric.setMetricType(MetricTypeEnum.FILE);
        this.metricRepository.save(metric);

        Repository repository = ModelFactory.getRepository();
        this.repositoryRepository.save(repository);

        GitCommit commit = ModelFactory.getGitCommit();
        this.gitCommitRepository.save(commit);

        Branch branch = ModelFactory.getBranch(repository, commit);
        this.branchRepository.save(branch);

        BranchFile source = ModelFactory.getBranchFile(branch, commit);
        this.branchFileRepository.save(source);
        BranchFile target = ModelFactory.getBranchFile(branch, commit);
        this.branchFileRepository.save(target);

        BranchFileMetricResult sourceBranchFileMetricResult = ModelFactory.getBranchFileMetricResult(source, metric);
        this.branchFileMetricResultRepository.save(sourceBranchFileMetricResult);
        BranchFileMetricResult targetBranchFileMetricResult = ModelFactory.getBranchFileMetricResult(target, metric);
        this.branchFileMetricResultRepository.save(targetBranchFileMetricResult);

        this.branchFileMetricResultCopyHelper.copy(source, target);

        assertEquals(2, this.branchFileMetricResultRepository.count());
    }

}
