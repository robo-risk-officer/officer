package com.roborisk.officer.helpers;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.*;
import com.roborisk.officer.models.repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BranchFileObservationHelperTest extends OfficerTest {

    /**
     * Local instance of the {@link BranchFileObservationHelper} class.
     */
    @Autowired
    BranchFileObservationHelper branchFileObservationHelper;

    /**
     * Repository for handling {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Repository for handling {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * Repository for handling {@link Branch} objects.
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Repository for handling {@link BranchFile} objects.
     */
    @Autowired
    BranchFileRepository branchFileRepository;

    /**
     * Repository for handling {@link BranchFunctionObservation} objects.
     */
    @Autowired
    BranchFunctionObservationRepository branchFunctionObservationRepository;

    /**
     * Repository for handling {@link BranchClassObservation} objects.
     */
    @Autowired
    BranchClassObservationRepository branchClassObservationRepository;

    /**
     * Test for checking that the case of multiple observations for a single branch file with the
     * same class name is correctly handled.
     */
    @Test
    void getLatestClassObservationsMultipleSameClassTest() throws ParseException {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        GitCommit gitCommit1 = ModelFactory.getGitCommit();
        gitCommitRepository.save(gitCommit1);
        GitCommit gitCommit2 = ModelFactory.getGitCommit();
        gitCommit2.setTimestamp(gitCommit1.getTimestamp() + 500);
        gitCommitRepository.save(gitCommit2);
        GitCommit gitCommit3 = ModelFactory.getGitCommit();
        gitCommit3.setTimestamp(gitCommit1.getTimestamp() - 500);
        gitCommitRepository.save(gitCommit3);


        Branch branch = ModelFactory.getBranch(repository, gitCommit1);
        branchRepository.save(branch);

        BranchFile branchFile = ModelFactory.getBranchFile(branch, gitCommit1);
        branchFileRepository.save(branchFile);

        BranchClassObservation branchClassObservation1 = ModelFactory.getBranchClassObservation(gitCommit1, branchFile);
        branchClassObservationRepository.save(branchClassObservation1);
        BranchClassObservation branchClassObservation2 = ModelFactory.getBranchClassObservation(gitCommit2, branchFile);
        branchClassObservation2.setClassName(branchClassObservation1.getClassName());
        branchClassObservationRepository.save(branchClassObservation2);
        BranchClassObservation branchClassObservation3 = ModelFactory.getBranchClassObservation(gitCommit3, branchFile);
        branchClassObservation3.setClassName(branchClassObservation1.getClassName());
        branchClassObservationRepository.save(branchClassObservation3);

        List<BranchClassObservation> result = branchFileObservationHelper.getLatestClassObservations(branchFile);

        assertEquals(1, result.size());

        assertEquals(branchClassObservation2.getId(), result.get(0).getId());
    }

    /**
     * Test for checking that the case of multiple observations for a single branch file with the
     * same function name is correctly handled.
     */
    @Test
    void getLatestFunctionObservationsMultipleSameFunctionTest() throws ParseException {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        GitCommit gitCommit1 = ModelFactory.getGitCommit();
        gitCommitRepository.save(gitCommit1);
        GitCommit gitCommit2 = ModelFactory.getGitCommit();
        gitCommit2.setTimestamp(gitCommit1.getTimestamp() + 500);
        gitCommitRepository.save(gitCommit2);
        GitCommit gitCommit3 = ModelFactory.getGitCommit();
        gitCommit3.setTimestamp(gitCommit1.getTimestamp() - 500);
        gitCommitRepository.save(gitCommit3);


        Branch branch = ModelFactory.getBranch(repository, gitCommit1);
        branchRepository.save(branch);

        BranchFile branchFile = ModelFactory.getBranchFile(branch, gitCommit1);
        branchFileRepository.save(branchFile);

        BranchFunctionObservation branchFunctionObservation1 = ModelFactory.getBranchFunctionObservation(gitCommit1, branchFile);
        branchFunctionObservationRepository.save(branchFunctionObservation1);
        BranchFunctionObservation branchFunctionObservation2 = ModelFactory.getBranchFunctionObservation(gitCommit2, branchFile);
        branchFunctionObservation2.setClassName(branchFunctionObservation1.getClassName());
        branchFunctionObservation2.setFunctionName(branchFunctionObservation1.getFunctionName());
        branchFunctionObservationRepository.save(branchFunctionObservation2);
        BranchFunctionObservation branchFunctionObservation3 = ModelFactory.getBranchFunctionObservation(gitCommit3, branchFile);
        branchFunctionObservation3.setClassName(branchFunctionObservation1.getClassName());
        branchFunctionObservation3.setFunctionName(branchFunctionObservation1.getFunctionName());
        branchFunctionObservationRepository.save(branchFunctionObservation3);

        List<BranchFunctionObservation> result = branchFileObservationHelper.getLatestFunctionObservations(branchFile);

        assertEquals(1, result.size());

        assertEquals(branchFunctionObservation2.getId(), result.get(0).getId());
    }
}
