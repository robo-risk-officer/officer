package com.roborisk.officer.helpers;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.*;
import com.roborisk.officer.models.repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Class providing tests for the {@link ObservationHelper} class.
 */
public class ObservationHelperTest extends OfficerTest {

    /**
     * Repository for storing {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Repository for storing {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * Repository for storing {@link Branch} objects.
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Repository for storing {@link BranchFile} objects.
     */
    @Autowired
    BranchFileRepository branchFileRepository;

    /**
     * Repository for storing {@link BranchFunctionObservation} objects.
     */
    @Autowired
    BranchFunctionObservationRepository branchFunctionObservationRepository;

    /**
     * Repository for storing {@link BranchClassObservation} objects.
     */
    @Autowired
    BranchClassObservationRepository branchClassObservationRepository;

    /**
     * Repository for storing {@link MergeRequest} objects.
     */
    @Autowired
    MergeRequestRepository mergeRequestRepository;

    /**
     * Repository for storing {@link MergeRequestReview} objects.
     */
    @Autowired
    MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Repository for storing {@link MergeRequestFile} objects.
     */
    @Autowired
    MergeRequestFileRepository mergeRequestFileRepository;


    /**
     * Repository for storing {@link MergeRequestFunctionObservation} objects.
     */
    @Autowired
    MergeRequestFunctionObservationRepository mergeRequestFunctionObservationRepository;

    /**
     * Repository for storing {@link MergeRequestClassObservation} objects.
     */
    @Autowired
    MergeRequestClassObservationRepository mergeRequestClassObservationRepository;

    /**
     * Local instance of the {@link ObservationHelper} class.
     */
    @Autowired
    ObservationHelper observationHelper;

    /**
     * Test for checking whether already existing function observations are correctly handled.
     */
    @Test
    void copyFunctionObservationsAlreadyExists() throws ParseException {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        GitCommit commit = ModelFactory.getGitCommit();
        gitCommitRepository.save(commit);

        Branch branch = ModelFactory.getBranch(repository, commit);
        branchRepository.save(branch);
        Branch branch1 = ModelFactory.getBranch(repository, commit);
        branchRepository.save(branch1);

        BranchFile branchFile = ModelFactory.getBranchFile(branch, commit);
        branchFileRepository.save(branchFile);

        BranchFunctionObservation branchFunctionObservation = ModelFactory.getBranchFunctionObservation(commit, branchFile);
        branchFunctionObservationRepository.save(branchFunctionObservation);

        MergeRequest mergeRequest = ModelFactory.getMergeRequest(branch, branch1);
        mergeRequestRepository.save(mergeRequest);

        MergeRequestReview mergeRequestReview = ModelFactory.getMergeRequestReview(commit, mergeRequest);
        mergeRequestReviewRepository.save(mergeRequestReview);

        MergeRequestFile mergeRequestFile = ModelFactory.getMergeRequestFile(mergeRequestReview);
        mergeRequestFileRepository.save(mergeRequestFile);

        MergeRequestFunctionObservation mergeRequestFunctionObservation = ModelFactory.getMergeRequestFunctionObservation(mergeRequestFile);
        mergeRequestFunctionObservation.setClassName(branchFunctionObservation.getClassName());
        mergeRequestFunctionObservation.setFunctionName(branchFunctionObservation.getFunctionName());
        mergeRequestFunctionObservationRepository.save(mergeRequestFunctionObservation);

        observationHelper.copyFunctionObservations(branchFile, mergeRequestFile);

        assertEquals(1, mergeRequestFunctionObservationRepository.count());
    }

    /**
     * Test for checking whether already existing function observations are correctly handled.
     */
    @Test
    void copyClassObservationsAlreadyExists() throws ParseException {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        GitCommit commit = ModelFactory.getGitCommit();
        gitCommitRepository.save(commit);

        Branch branch = ModelFactory.getBranch(repository, commit);
        branchRepository.save(branch);
        Branch branch1 = ModelFactory.getBranch(repository, commit);
        branchRepository.save(branch1);

        BranchFile branchFile = ModelFactory.getBranchFile(branch, commit);
        branchFileRepository.save(branchFile);

        BranchClassObservation branchClassObservation = ModelFactory.getBranchClassObservation(commit, branchFile);
        branchClassObservationRepository.save(branchClassObservation);

        MergeRequest mergeRequest = ModelFactory.getMergeRequest(branch, branch1);
        mergeRequestRepository.save(mergeRequest);

        MergeRequestReview mergeRequestReview = ModelFactory.getMergeRequestReview(commit, mergeRequest);
        mergeRequestReviewRepository.save(mergeRequestReview);

        MergeRequestFile mergeRequestFile = ModelFactory.getMergeRequestFile(mergeRequestReview);
        mergeRequestFileRepository.save(mergeRequestFile);

        MergeRequestClassObservation mergeRequestClassObservation = ModelFactory.getMergeRequestClassObservation(mergeRequestFile);
        mergeRequestClassObservation.setClassName(branchClassObservation.getClassName());
        mergeRequestClassObservationRepository.save(mergeRequestClassObservation);

        observationHelper.copyClassObservations(branchFile, mergeRequestFile);

        assertEquals(1, mergeRequestClassObservationRepository.count());
    }
}
