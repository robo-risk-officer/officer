package com.roborisk.officer.helpers.translate;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.helpers.SettingsApiDataHelper;
import com.roborisk.officer.models.database.AbstractSettings;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.RobotSettings;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.MetricSettingsWeb;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class providing tests for the {@link SettingsApiDataHelper} class.
 */
public class SettingsApiDataHelperTest extends OfficerTest {

    /**
     * Repository for handling {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Test for checking whether an empty list is correctly returned if the ID of a repository is
     * given which does not exist in the database.
     */
    @Test
    void getCurrentMetricSettingsAsWebNotFound() {
        List<MetricSettingsWeb> returned = SettingsApiDataHelper.getCurrentMetricSettingsAsWeb(31415);

        assertTrue(returned.isEmpty());
    }

    /**
     * Test for checking whether the correct exception is thrown if default settings are requested
     * in combination with a specific repository.
     */
    @Test
    void getSettingsFromDatabaseRepositoryNull() {
        RobotSettings settings = new RobotSettings();

        Optional<RobotSettings> currentSettings = Optional.of(settings);

        try {
            SettingsApiDataHelper.getSettingsFromDatabase(currentSettings, 500);
        } catch (IllegalStateException e) {
            assertEquals("No abstract setting for repositoryId: 500", e.getMessage());
        }
    }

    /**
     * Test for checking whether the correct exception is thrown if repository-specific settings are
     * requested in combination with a different specific repository.
     */
    @Test
    void getSettingsFromDatabaseDifferentRepository() {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        RobotSettings settings = ModelFactory.getRobotSettings(repository);

        Optional<RobotSettings> currentSettings = Optional.of(settings);

        try {
            SettingsApiDataHelper.getSettingsFromDatabase(currentSettings, 500);
        } catch (IllegalStateException e) {
            assertEquals("No abstract setting for repositoryId: 500", e.getMessage());
        }
    }

    /**
     * Test for checking whether the default settings are correctly returned.
     */
    @Test
    void getSettingsFromDatabaseDefaultSettings() {
        RobotSettings settings = new RobotSettings();

        Optional<RobotSettings> currentSettings = Optional.of(settings);

        AbstractSettings returned = SettingsApiDataHelper.getSettingsFromDatabase(currentSettings, null);

        assertEquals(settings, returned);
    }
}
