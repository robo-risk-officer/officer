package com.roborisk.officer.helpers.translate;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.*;
import com.roborisk.officer.models.repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Class providing tests for the {@link BranchClassObservationTranslationHelper} class.
 */
public class BranchClassObservationTranslationHelperTest extends OfficerTest {

    /**
     * Local instance of the {@link BranchClassObservationTranslationHelper} class.
     */
    @Autowired
    BranchClassObservationTranslationHelper helper;

    /**
     * Repository for handling {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Repository for handling {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * Repository for handling {@link Branch} objects.
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Repository for handling {@link BranchFile} objects.
     */
    @Autowired
    BranchFileRepository branchFileRepository;


    /**
     * Repository for handling {@link BranchClassObservation} objects.
     */
    @Autowired
    BranchClassObservationRepository branchClassObservationRepository;

    /**
     * Repository for handling {@link MergeRequest} objects.
     */
    @Autowired
    MergeRequestRepository mergeRequestRepository;

    /**
     * Repository for handling {@link MergeRequestReview} objects.
     */
    @Autowired
    MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Repository for handling {@link MergeRequestFile} objects.
     */
    @Autowired
    MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * Repository for handling {@link MergeRequestClassObservation} objects.
     */
    @Autowired
    MergeRequestClassObservationRepository mergeRequestClassObservationRepository;

    /**
     * Test for checking that the Class executes correctly if it is not found in the database.
     *
     * @throws ParseException
     */
    @Test
    void translateNotPresent() throws ParseException {
        Repository repository = ModelFactory.getRepository();
        this.repositoryRepository.save(repository);

        GitCommit commit = ModelFactory.getGitCommit();
        this.gitCommitRepository.save(commit);

        Branch sourceBranch = ModelFactory.getBranch(repository, commit);
        this.branchRepository.save(sourceBranch);
        Branch targetBranch = ModelFactory.getBranch(repository, commit);
        this.branchRepository.save(targetBranch);

        BranchFile branchFile = ModelFactory.getBranchFile(sourceBranch, commit);
        this.branchFileRepository.save(branchFile);

        BranchClassObservation branchClassObservation = ModelFactory.getBranchClassObservation(commit, branchFile);
        this.branchClassObservationRepository.save(branchClassObservation);

        MergeRequest mergeRequest = ModelFactory.getMergeRequest(sourceBranch, targetBranch);
        this.mergeRequestRepository.save(mergeRequest);

        MergeRequestReview mergeRequestReview = ModelFactory.getMergeRequestReview(commit, mergeRequest);
        this.mergeRequestReviewRepository.save(mergeRequestReview);

        MergeRequestFile mergeRequestFile = ModelFactory.getMergeRequestFile(mergeRequestReview);
        this.mergeRequestFileRepository.save(mergeRequestFile);

        helper.translate(branchClassObservation, mergeRequestFile);

        assertEquals(1, this.mergeRequestClassObservationRepository.count());
    }
}
