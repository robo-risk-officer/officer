package com.roborisk.officer.helpers;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests the Time Conversion of GitLab.
 */
public class TimeConverterTest extends OfficerTest {

    /**
     * Test whether TimeConverter.posixDate works correctly for format used for createdAt and updatedAt.
     */
    @Test
    public void testPosixDate() throws ParseException {
        assertThat(GitLabApiWrapper.getEpochFromString("2015-03-01 20:12:53 UTC")).isEqualTo(1425240773);
    }

    /**
     * Test whether TimeConverter.posixDate works correctly for format used for timestamp.
     */
    @Test
    public void testPosixDate2() throws ParseException {
        assertThat(GitLabApiWrapper.getEpochFromString("2015-04-08T21:00:25-07:00")).isEqualTo(1428552025);
    }
}
