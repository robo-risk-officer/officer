package com.roborisk.officer.helpers;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.web.GitCommitWeb;
import com.roborisk.officer.modules.gitlab.GitLabApiWrapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.LinkedHashMap;

import static org.junit.Assert.assertEquals;

/**
 * Tests the JsonConverter helper.
 */
public class JsonConverterTest extends OfficerTest {

    /**
     * Test for convertFromMap regarding the MergeRequestWeb last_commit object.
     *
     * @throws IOException      If convertFromMap errors.
     * @throws ParseException   If posixDate for transforming the dates errors.
     */
    @Test
    public void testConvertFromMapLastCommit() throws IOException, ParseException {

        // Create LinkedHashMap representing a last_commit of a webhook
        LinkedHashMap<String, Object> cm = new LinkedHashMap();
        cm.put("id", "da1560886d4f094c3e6c9ef40349f7d38b5d27d7");
        cm.put("message", "fixed readme");
        cm.put("timestamp", "2012-01-03T23:36:29+02:00");
        cm.put("url", "http://example.com/awesome_space/awesome_project/commits/da1560886d4f094c3e6c9ef40349f7d38b5d27d7");

        // Creating author LinkedHashMap and adding it to cm
        LinkedHashMap<String, Object> author = new LinkedHashMap<>();
        author.put("name", "GitLab dev user");
        author.put("email", "gitlabdev@dv6700.(none)");
        cm.put("author", author);

        // transform LinkedHashMap using convertFromMap
        GitCommitWeb gcw = JsonConverter.convertFromMap(cm, GitCommitWeb.class);

        // Assert if all data has been processed correctly.
        assertEquals("da1560886d4f094c3e6c9ef40349f7d38b5d27d7", gcw.getHash());
        assertEquals("fixed readme", gcw.getMessage());
        assertEquals(GitLabApiWrapper.getEpochFromString("2012-01-03T23:36:29+02:00"), gcw.getTimestamp());
        assertEquals("GitLab dev user", gcw.getAuthor().getName());
        assertEquals("gitlabdev@dv6700.(none)", gcw.getAuthor().getEmail());
    }
}
