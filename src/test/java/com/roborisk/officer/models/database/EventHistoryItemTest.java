package com.roborisk.officer.models.database;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.repositories.EventHistoryItemRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * This class contains tests for the {@link EventHistoryItem} database model.
 */
public class EventHistoryItemTest extends OfficerTest {

    /**
     * Database repository to handle {@link EventHistoryItem} objects.
     */
    @Autowired
    private EventHistoryItemRepository eventHistoryItemRepository;

    /**
     * Tests storing an eventHistoryItem. Creates, stores and deletes it.
     */
    @Test
    public void testStorageOfEvent() {
        // Prepare history item
        EventHistoryItem original = new EventHistoryItem();
        original.setHash(Long.parseLong("ea168491", 16));
        original.setTimestamp(1242498L);

        // Save history event item to database
        eventHistoryItemRepository.save(original);

        // Get history event from database
        Optional<EventHistoryItem> eventHistoryItemOptional;
        eventHistoryItemOptional = eventHistoryItemRepository.findById(original.getId());

        // Check if there is an entry in the database for the object
        if (eventHistoryItemOptional.isEmpty()) {
            fail("EventHistoryItemTest.testStorageOfEvent: " +
                    "the created event was not found in the database");
        } else {
            // Compare the object in the database to the original object
            EventHistoryItem storedVersion = eventHistoryItemOptional.get();

            assertEquals(original.getId(), storedVersion.getId());
            assertEquals(original.getHash(), storedVersion.getHash());
            assertEquals(original.getTimestamp(), storedVersion.getTimestamp());
        }

        // Clean up database
        eventHistoryItemRepository.delete(original);

        // Check if the item is indeed removed from the database
        eventHistoryItemOptional = eventHistoryItemRepository.findById(original.getId());
        if (eventHistoryItemOptional.isPresent()) {
            fail("EventHistoryItemTest.testStorageOfEvent: database is not empty after removal");
        }
    }
}
