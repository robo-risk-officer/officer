package com.roborisk.officer.models.database;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.repositories.RobotSettingsRepository;
import com.roborisk.officer.models.web.RobotSettingsWeb;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * This class contains tests for the {@link RobotSettings} database model.
 */
class RobotSettingsTest extends OfficerTest {

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link RobotSettings} objects.
     */
    @Autowired
    RobotSettingsRepository robotSettingsRepository;

    /**
     * {@link RobotSettings} object for testing.
     */
    RobotSettings settings;

    /**
     * Method to initialise database-related things before a test.
     */
    @BeforeEach
    public void prepareTest() {
        Repository repository = ModelFactory.getRepository();

        this.settings = ModelFactory.getRobotSettings(repository);

        this.repositoryRepository.save(this.settings.getRepository());
        this.robotSettingsRepository.save(this.settings);
    }

    /**
     * Test the storage of a simple default Robot Setting (without repository).
     */
    @Test
    public void testStorageOfRobotSettings() {
        // Create RobotSettings object
        RobotSettings original = this.settings;

        // Get the robotSettings object from the database
        Optional<RobotSettings> optionalRobotSettings;
        optionalRobotSettings = robotSettingsRepository.findById(original.getId());

        if (optionalRobotSettings.isEmpty()) {
            Assertions.fail("RobotSettingsTest.testStorageOfRobotSettings: " +
                "the created robot settings object was not found in the database");
        } else {
            // Compare the object in the database to the original object
            RobotSettings storedVersion = optionalRobotSettings.get();

            assertEquals(original.getId(), storedVersion.getId());
            assertEquals(original.getAutomaticReviewOnNewCommit(),
                storedVersion.getAutomaticReviewOnNewCommit());
            assertEquals(original.getAutomaticReviewOnNewMr(),
                storedVersion.getAutomaticReviewOnNewMr());
            assertEquals(original.getFalsePositiveThreshold(),
                storedVersion.getFalsePositiveThreshold());
            assertEquals(original.getReviewHistoryLength(), storedVersion.getReviewHistoryLength());
        }
    }

    /**
     * Test the passing of the function isEqualToRobotSettingsWeb.
     */
    @Test
    public void isEqualToRobotSettingsWeb_pass() {
        RobotSettingsWeb robotSettingsWeb = ModelFactory.getRobotSettingsWeb(settings);
        assert (settings.isEqualToRobotSettingsWeb(robotSettingsWeb));
    }

    /**
     * Test the failing of the function isEqualToRobotSettingsWeb.
     */
    @Test
    public void isEqualToRobotSettingsWeb_fail() {
        RobotSettingsWeb robotSettingsWebDifferingId = ModelFactory.getRobotSettingsWeb(settings);
        Integer newId = ThreadLocalRandom.current().nextInt();
        while (newId.equals(settings.getRepository().getId())) {
            newId = ThreadLocalRandom.current().nextInt();
        }
        robotSettingsWebDifferingId.setId(newId);
        assert (!settings.isEqualToRobotSettingsWeb(robotSettingsWebDifferingId));

        RobotSettingsWeb robotSettingsWebDifferingThreshold =
            ModelFactory.getRobotSettingsWeb(settings);
        int newThreshold = ThreadLocalRandom.current().nextInt();
        while (newThreshold == settings.getFalsePositiveThreshold()) {
            newThreshold = ThreadLocalRandom.current().nextInt();
        }
        robotSettingsWebDifferingThreshold.setFalsePositiveThreshold(newThreshold);
        assert (!settings.isEqualToRobotSettingsWeb(robotSettingsWebDifferingThreshold));

        RobotSettingsWeb robotSettingsWebDifferingReviewOnMR =
            ModelFactory.getRobotSettingsWeb(settings);
        robotSettingsWebDifferingReviewOnMR
            .setAutomaticReviewOnNewMr(!settings.getAutomaticReviewOnNewMr());
        assert (!settings.isEqualToRobotSettingsWeb(robotSettingsWebDifferingReviewOnMR));

        RobotSettingsWeb robotSettingsWebDifferingReviewOnCommit =
            ModelFactory.getRobotSettingsWeb(settings);
        robotSettingsWebDifferingReviewOnCommit
            .setAutomaticReviewOnNewCommit(!settings.getAutomaticReviewOnNewCommit());
        assert (!settings.isEqualToRobotSettingsWeb(robotSettingsWebDifferingReviewOnCommit));

        RobotSettingsWeb robotSettingsWebDifferingHistory =
            ModelFactory.getRobotSettingsWeb(settings);
        int historyLength = 1;
        while (historyLength == settings.getReviewHistoryLength()) {
            historyLength = ThreadLocalRandom.current().nextInt();
        }

        robotSettingsWebDifferingHistory.setReviewHistoryLength(historyLength);
        assert (!settings.isEqualToRobotSettingsWeb(robotSettingsWebDifferingHistory));
    }

    /**
     * Test if update works correctly.
     */
    @Test
    public void testUpdate() {
        RobotSettingsWeb updateSettings = ModelFactory
            .getRobotSettingsWeb(ModelFactory.getRobotSettings(settings.getRepository()));
        updateSettings.setId(settings.getId());
        settings.setAllFromRobotSettingsWeb(updateSettings);
        assertTrue(settings.isEqualToRobotSettingsWeb(updateSettings));
    }
}
