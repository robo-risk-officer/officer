package com.roborisk.officer.models.database;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.MetricRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * This class contains tests for the {@link Metric} database model.
 */
public class MetricTest extends OfficerTest {

    /**
     * Max length of the name of a metric. This is in accordance with the variable of the same
     * name in {@link Metric}, which is private and can thus not be accessed.
     */
    private static final int MAX_NAME_LENGTH = 255;

    /**
     * Max length of the display name of a metric. This is in accordance with the variable of the
     * same name in {@link Metric}, which is private and can thus not be accessed.
     */
    private static final int MAX_DISPLAY_NAME_LENGTH = 255;

    /**
     * Max length of the view of a metric. This is in accordance with the variable of the
     * same name in {@link Metric}, which is private and can thus not be accessed.
     */
    private static final int MAX_VIEW_LENGTH = 255;

    /**
     * Repository to store the {@link Metric} that is being tested.
     */
    @Autowired
    private MetricRepository metricRepository;

    /**
     * Returns a random string with the specified length from character of the alphabet as
     * defined in the function.
     *
     * @param length    Expected length of the returned string.
     * @return          A random string.
     */
    private String generateRandomString(int length) {
        StringBuilder builder = new StringBuilder();

        String alphabet = "123xyz";

        for (int i = 0; i < length; i++) {
            builder.append(alphabet.charAt(ThreadLocalRandom.current().nextInt(0,
                alphabet.length())));
        }

        return builder.toString();
    }

    /**
     * Test that the metric name is stored correctly when having exactly the max length.
     */
    @Test
    public void testMetricNameWithMaxLenght() {
        Metric metric = ModelFactory.getMetric();

        String string = this.generateRandomString(MetricTest.MAX_NAME_LENGTH);

        metric.setName(string);
        metricRepository.save(metric);

        Metric fetchedMetric = metricRepository.findById(metric.getId()).get();
        assertNotNull(fetchedMetric);

        assertEquals(fetchedMetric.getName(), string);
    }

    /**
     * Test that the metric name throws the right error when trying to set a name that is too long.
     */
    @Test
    public void testMetricNameWithMoreThanMaxLenght() {
        Metric metric = ModelFactory.getMetric();

        String string = this.generateRandomString(MetricTest.MAX_NAME_LENGTH + 1);

        try {
            metric.setName(string);
            fail();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test that the metric name is stored correctly when having exactly the max length.
     */
    @Test
    public void testMetricViewWithMaxLenght() {
        Metric metric = ModelFactory.getMetric();

        String string = this.generateRandomString(MetricTest.MAX_NAME_LENGTH);

        metric.setViewIdentifier(string);
        metricRepository.save(metric);

        Metric fetchedMetric = metricRepository.findById(metric.getId()).get();
        assertNotNull(fetchedMetric);

        assertEquals(fetchedMetric.getViewIdentifier(), string);
    }

    /**
     * Test that the metric name throws the right error when trying to set a name that is too long.
     */
    @Test
    public void testMetricViewWithMoreThanMaxLenght() {
        Metric metric = ModelFactory.getMetric();

        String string = this.generateRandomString(MetricTest.MAX_NAME_LENGTH + 1);

        try {
            metric.setViewIdentifier(string);
            fail();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test that the metric display name is stored correctly when having exactly the max length.
     */
    @Test
    public void testMetricDispalyNameWithMaxLenght() {
        Metric metric = ModelFactory.getMetric();

        String string = this.generateRandomString(MetricTest.MAX_DISPLAY_NAME_LENGTH);

        metric.setDisplayName(string);
        metricRepository.save(metric);

        Metric fetchedMetric = metricRepository.findById(metric.getId()).get();
        assertNotNull(fetchedMetric);

        assertEquals(fetchedMetric.getDisplayName(), string);
    }

    /**
     * Test that the metric name throws the right error when trying to set a name that is too long.
     */
    @Test
    public void testMetricDisplayNameWithMoreThanMaxLenght() {
        Metric metric = ModelFactory.getMetric();

        String string = this.generateRandomString(MetricTest.MAX_DISPLAY_NAME_LENGTH + 1);

        try {
            metric.setDisplayName(string);
            fail();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
