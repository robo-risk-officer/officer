package com.roborisk.officer.models.database;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.RepositoryApiWeb;
import com.roborisk.officer.models.web.RepositoryWeb;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * This class contains tests for the {@link Repository} database model.
 */
public class RepositoryTest extends OfficerTest {

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * {@link Repository} object for testing.
     */
    Repository repository;

    /**
     * Prepare the test before each run.
     */
    @BeforeEach
    public void prepareTest() {
        this.repository = ModelFactory.getRepository();
        this.repositoryRepository.save(this.repository);
    }

    /**
     * Test storing a repository.
     */
    @Test
    public void testIfRepositoryStored() {
        Optional<Repository> optional = this.repositoryRepository.findById(this.repository.getId());
        assertTrue(optional.isPresent());

        Repository clone = optional.get();
        assertSame(this.repository.getId(), clone.getId());
    }

    /**
     * Test for checking the retrieval of a RepositoryWeb object based on the repository.
     */
    @Test
    public void getWebObject() {
        RepositoryWeb web = this.repository.getWebObject();
        assertEquals(web.getGitLabId(), this.repository.getGitLabId());
        assertEquals(web.getName(), this.repository.getName());
        assertEquals(web.getNamespace(), this.repository.getNamespace());
        assertEquals(web.getPathWithNamespace(), this.repository.getPathWithNamespace());
        assertEquals(web.getHttpUrl(), this.repository.getHttpUrl());
        assertEquals(web.getSshUrl(), this.repository.getSshUrl());
        assertEquals(web.isEnabled(), this.repository.getIsEnabled());
    }

    /**
     * Test to check isEqualToRepositoryWeb.
     */
    @Test
    public void isEqualToRepositoryWeb() {
        RepositoryApiWeb web = this.repository.getApiWebObject();

        assertTrue(this.repository.isEqualToRepositoryWeb(web));

        web.setId(100001);
        assertFalse(this.repository.isEqualToRepositoryWeb(web));
        web.setId(this.repository.getId());

        web.setName("NOTTHEREALNAME");
        assertFalse(this.repository.isEqualToRepositoryWeb(web));
        web.setName(this.repository.getName());

        web.setHttpUrl("FAKEURL");
        assertFalse(this.repository.isEqualToRepositoryWeb(web));
        web.setHttpUrl(this.repository.getHttpUrl());

        web.setSshUrl("FAKEURL");
        assertFalse(this.repository.isEqualToRepositoryWeb(web));
        web.setSshUrl(this.repository.getSshUrl());

        web.setNamespace("SOMESTRANGENAMESPACE");
        assertFalse(this.repository.isEqualToRepositoryWeb(web));
        web.setNamespace(this.repository.getNamespace());

        web.setPathWithNamespace("HAHA");
        assertFalse(this.repository.isEqualToRepositoryWeb(web));
        web.setPathWithNamespace(this.repository.getPathWithNamespace());

        web.setEnabled(!this.repository.getIsEnabled());
        assertFalse(this.repository.isEqualToRepositoryWeb(web));
    }


}
