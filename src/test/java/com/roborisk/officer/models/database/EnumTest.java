package com.roborisk.officer.models.database;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.enums.MetricTypeEnum;
import com.roborisk.officer.models.enums.QualityTypeEnum;
import com.roborisk.officer.models.repositories.MetricRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

/**
 * Class to test storage of Java object that contain enums in the database.
 */
public class EnumTest extends OfficerTest {

    /**
     * Database repository to handle {@link Metric} objects.
     */
    @Autowired
    MetricRepository metricRepository;

    /**
     * Test case to store a Metric object with MetricType and QualityType enums in the database.
     * ObservationTypeEnum is not tested here, but will implicitly be tested in the Note test class.
     */
    @Test
    public void storeJavaEnumObjects() {
        // Create a new Metric
        Metric metric = new Metric();

        // Set its attributes
        metric.setName("superEpicTestMetric");
        metric.setDisplayName("Test Metric");
        metric.setDescription("Some description");
        metric.setMetricType(MetricTypeEnum.FUNCTION);
        metric.setQualityType(QualityTypeEnum.RISK);

        // Store it in the database
        metricRepository.save(metric);

        // Retrieve the metric from the database again
        Optional<Metric> optionalStoredMetric = metricRepository.findById(metric.getId());

        // Check it exists
        if (optionalStoredMetric.isEmpty()) {
            fail("EnumTest.storeJavaEnumObjects: could not retrieve the stored metric"
                    + " from database");
        } else {
            // It exists, so lets get it
            Metric storedMetric = optionalStoredMetric.get();

            // Compare if the metric and its stored version are actually equal
            assertSame(metric.getId(), storedMetric.getId());
            assertEquals(metric.getName(), storedMetric.getName());
            assertEquals(metric.getDisplayName(), storedMetric.getDisplayName());
            assertEquals(metric.getMetricType(), storedMetric.getMetricType());
            assertEquals(metric.getQualityType(), storedMetric.getQualityType());
        }
    }
}
