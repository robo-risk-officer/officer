package com.roborisk.officer.models.database;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.DiscussionThreadRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestClassObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestFileRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.repositories.RobotNoteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * This class contains tests for the {@link MergeRequestReview} database model.
 */
public class MergeRequestReviewTest extends OfficerTest {

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link MergeRequestReview} objects.
     */
    @Autowired
    MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * Database repository to handle {@link MergeRequestFile} objects.
     */
    @Autowired
    MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Database repository to handle {@link MergeRequestClassObservation} objects.
     */
    @Autowired
    MergeRequestClassObservationRepository mergeRequestClassObservationRepository;

    /**
     * Database repository to handle {@link RobotNote} objects.
     */
    @Autowired
    RobotNoteRepository robotNoteRepository;

    /**
     * Database repository to handle {@link DiscussionThread} objects.
     */
    @Autowired
    DiscussionThreadRepository discussionThreadRepository;

    /**
     * {@link Repository} object for testing.
     */
    Repository testRepository;

    /**
     * First {@link GitCommit} object for testing.
     */
    GitCommit firstCommit;

    /**
     * Second {@link GitCommit} object for testing.
     */
    GitCommit secondCommit;

    /**
     * Source {@link Branch} object for testing.
     */
    Branch sourceBranch;

    /**
     * Target {@link Branch} object for testing.
     */
    Branch targetBranch;

    /**
     * {@link MergeRequest} object for testing.
     */
    MergeRequest testMergeRequest;

    /**
     * Method to set up the database environment before each test.
     */
    @BeforeEach
    public void setup() throws ParseException {
        // Firstly, create a test Repository
        testRepository = ModelFactory.getRepository();
        // And add it to the database
        repositoryRepository.save(testRepository);

        // Create two commits for testing
        firstCommit = ModelFactory.getGitCommit();
        secondCommit = ModelFactory.getGitCommit();
        secondCommit.setParentCommit(firstCommit);
        // And add them to the database
        gitCommitRepository.save(firstCommit);
        gitCommitRepository.save(secondCommit);

        // Create a source and target branch
        sourceBranch = ModelFactory.getBranch(testRepository, firstCommit);
        targetBranch = ModelFactory.getBranch(testRepository, secondCommit);
        // And add them to the database
        branchRepository.save(sourceBranch);
        branchRepository.save(targetBranch);

        // Finally, create a test MergeRequest
        testMergeRequest = ModelFactory.getMergeRequest(sourceBranch, targetBranch);
        // And store it in the database
        mergeRequestRepository.save(testMergeRequest);
    }

    /**
     * Test that list of{@link MergeRequestFile}s can be retrieved for MergeRequestReview.
     */
    @Test
    public void testMergeRequestFiles() {
        // Firstly, create a MergeRequestReview
        MergeRequestReview testMergeRequestReview = ModelFactory.getMergeRequestReview(secondCommit,
                testMergeRequest);
        // Add it to the database
        mergeRequestReviewRepository.save(testMergeRequestReview);

        // Then, add a MergeRequestFile to test things
        MergeRequestFile testMergeRequestFile =
                ModelFactory.getMergeRequestFile(testMergeRequestReview);
        // And add it to the database
        mergeRequestFileRepository.save(testMergeRequestFile);

        // Then, retrieve the testMergeRequestReview from the databae
        Optional<MergeRequestReview> optDatabaseMergeRequestReview =
                mergeRequestReviewRepository.findById(testMergeRequestReview.getId());

        // Check if it exists
        if (optDatabaseMergeRequestReview.isEmpty()) {
            fail("MergeRequestReviewTest.testMergeRequestFiles: testMergeRequestReview was not "
                    + "found in the database");
        } else {
            // If it exists, get the actual object
            MergeRequestReview databaseMergeRequestReview = optDatabaseMergeRequestReview.get();

            // Get the list of associated files
            List<MergeRequestFile> files = this.mergeRequestFileRepository.findAllByMergeRequestReviewId(
                    databaseMergeRequestReview.getId()
            );

            // Then, check it is not empty
            assertTrue(files.size() > 0);

            // And that the first file in it is actually the same as the one created
            MergeRequestFile dbRetrievedFile = files.get(0);
            assertEquals(testMergeRequestFile.getId(), dbRetrievedFile.getId());
            assertEquals(testMergeRequestFile.getFileName(), dbRetrievedFile.getFileName());
            assertEquals(testMergeRequestFile.getFilePath(), dbRetrievedFile.getFilePath());
            // And also execute a simple check of equality for the MergeRequestReview
            assertEquals(dbRetrievedFile.getMergeRequestReview().getId(), testMergeRequestReview.getId());
        }
    }

    /**
     * Test that list of{@Link Note}s can be retrieved for MergeRequestReview.
     */
    @Test
    public void testMergeRequestNotes() {
        // Firstly, create a MergeRequestReview
        MergeRequestReview testMergeRequestReview = ModelFactory.getMergeRequestReview(secondCommit,
            testMergeRequest);
        // Add it to the database
        mergeRequestReviewRepository.save(testMergeRequestReview);

        // Then, add a MergeRequestFile to test things
        MergeRequestFile testMergeRequestFile =
            ModelFactory.getMergeRequestFile(testMergeRequestReview);
        // And add it to the database
        mergeRequestFileRepository.save(testMergeRequestFile);

        // Then, create a merge request class observation on the file
        MergeRequestClassObservation testObservation =
                ModelFactory.getMergeRequestClassObservation(testMergeRequestFile);
        // And add it to the database
        mergeRequestClassObservationRepository.save(testObservation);

        //Create a discussion thread
        DiscussionThread discussionThread = ModelFactory
                                                .getSimpleDiscussionThread(testMergeRequest);
        discussionThreadRepository.save(discussionThread);

        // Then, create a note
        RobotNote testNote = ModelFactory.getSimpleNote(discussionThread);
        // And add it to the database
        robotNoteRepository.save(testNote);

        // add note to discussion thread and update
        discussionThread = discussionThreadRepository.findById(discussionThread.getId()).get();

        testNote.setDiscussionThread(discussionThread);
        robotNoteRepository.save(testNote);

        testMergeRequestReview.setDiscussionThread(discussionThread);
        mergeRequestReviewRepository.save(testMergeRequestReview);

        // Then, retrieve the testMergeRequestReview from the database
        Optional<MergeRequestReview> optDatabaseMergeRequestReview =
            mergeRequestReviewRepository.findById(testMergeRequestReview.getId());

        // Check if it exists
        if (optDatabaseMergeRequestReview.isEmpty()) {
            fail("MergeRequestReviewTest.testMergeRequestNotes: testMergeRequestReview was not "
                + "found in the database");
        } else {
            // If it exists, get the actual object
            MergeRequestReview databaseMergeRequestReview = optDatabaseMergeRequestReview.get();
            DiscussionThread dbRetrievedDiscussionThread = databaseMergeRequestReview.getDiscussionThread();

            // Then, check it is not empty
            assertTrue(this.robotNoteRepository.findAllByDiscussionThreadId(dbRetrievedDiscussionThread.getId()).size() > 0);

            RobotNote dbRetrievedNote = this.robotNoteRepository.findAllByDiscussionThreadId(dbRetrievedDiscussionThread.getId()).get(0);
            // And that the first file in it is actually the same as the one created
            assertEquals(testNote.getId(), dbRetrievedNote.getId());
            assertEquals(testNote.getGitLabId(), dbRetrievedNote.getGitLabId());
        }
    }
}
