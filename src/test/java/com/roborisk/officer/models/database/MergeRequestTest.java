package com.roborisk.officer.models.database;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Tests for the {@link MergeRequest} database object class.
 */
public class MergeRequestTest extends OfficerTest {

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * {@link Repository} object for testing.
     */
    Repository testRepo;

    /**
     * Source {@link Branch} object for testing.
     */
    Branch sourceBranch;

    /**
     * Target {@link Branch} object for testing.
     */
    Branch targetBranch;

    /**
     * {@link GitCommit} object for testing.
     */
    GitCommit testCommit;

    /**
     * Method to initialise database-related things before a test.
     */
    @BeforeEach
    public void prepareTest() throws ParseException {
        // Create a test repository
        testRepo = ModelFactory.getRepository();

        // Store test repository in database
        repositoryRepository.save(testRepo);

        // Create a test commit
        testCommit = new GitCommit();
        testCommit.setHash("skjhgfyuijhgygh");
        testCommit.setParentCommit(null);
        testCommit.setTimestamp("2015-04-08T21:00:25-07:00");
        testCommit.setMessage("Commit message");

        // Store the commit
        gitCommitRepository.save(testCommit);

        // Create branches
        sourceBranch = new Branch();
        sourceBranch.setName("SourceBranch");
        sourceBranch.setRepository(testRepo);
        sourceBranch.setLatestCommit(testCommit);

        targetBranch = new Branch();
        targetBranch.setName("TargetBranch");
        targetBranch.setRepository(testRepo);
        targetBranch.setLatestCommit(testCommit);

        // Store the branches
        branchRepository.save(sourceBranch);
        branchRepository.save(targetBranch);
    }

    /**
     * Test to check if a merge request can be stored in and retrieved from the database.
     */
    @Test
    public void testStorageOfMergeRequest() {
        // Create a merge request for the test
        MergeRequest original = new MergeRequest();

        // Set its contents
        original.setGitLabId(ThreadLocalRandom.current().nextInt());
        original.setGitLabIid(ThreadLocalRandom.current().nextInt());
        original.setTitle("Yippiejaja-yippie-yippie-yeah.");
        original.setCreatedAt(23572034);
        original.setUpdatedAt(10276423);
        original.setState("open");
        original.setMergeStatus("not-merged");
        original.setTargetBranch(targetBranch);
        original.setSourceBranch(sourceBranch);

        // Insert the MR into the database
        mergeRequestRepository.save(original);

        // Retrieve the inserted MR from the database
        Optional<MergeRequest> optStoredVersion = mergeRequestRepository.findById(original.getId());

        if (optStoredVersion.isEmpty()) {
            fail("MergeRequestTest.testStorageOfMergeRequest: the created MR was not stored in DB");
        } else {
            // Prevent the lazy evaluation to mess with us.
            MergeRequest storedVersion = optStoredVersion.get();

            assertEquals(original.getId(), storedVersion.getId());
        }
    }
}
