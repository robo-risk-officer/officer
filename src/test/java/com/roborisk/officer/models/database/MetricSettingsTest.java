package com.roborisk.officer.models.database;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.MetricRepository;
import com.roborisk.officer.models.repositories.MetricSettingsRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.MetricSettingsWeb;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class contains tests for the {@link MetricSettings} database model.
 */
class MetricSettingsTest extends OfficerTest {

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link Metric} objects.
     */
    @Autowired
    MetricRepository metricRepository;

    /**
     * Database repository to handle {@link MetricSettings} objects.
     */
    @Autowired
    MetricSettingsRepository metricSettingsRepository;

    /**
     * {@link MetricSettings} object for testing.
     */
    MetricSettings settings;

    /**
     * Method to initialise database-related things before a test.
     */
    @BeforeEach
    public void prepareTest() {
        Repository repository = ModelFactory.getRepository();
        Metric metric = ModelFactory.getMetric();
        this.settings = ModelFactory.getMetricSettings(repository, metric);

        this.repositoryRepository.save(this.settings.getRepository());
        this.metricRepository.save(this.settings.getMetric());
        this.metricSettingsRepository.save(this.settings);
    }

    /**
     * Test the storage of a simple default Robot Setting (without repository).
     */
    @Test
    public void testStorageOfRobotSettings() {
        // Create RobotSettings object
        MetricSettings original = this.settings;

        // Get the robotSettings object from the database
        Optional<MetricSettings> optionalMetricSettings;
        optionalMetricSettings = metricSettingsRepository.findById(original.getId());

        if (optionalMetricSettings.isEmpty()) {
            Assertions.fail("RobotSettingsTest.testStorageOfRobotSettings: " +
                                "the created robot settings object was not found in the database");
        } else {
            // Compare the object in the database to the original object
            MetricSettings storedVersion = optionalMetricSettings.get();

            assertEquals(original.getId(), storedVersion.getId());
            assertEquals(original.getThreshold(),
                storedVersion.getThreshold());
            assertEquals(original.getWeight(),
                storedVersion.getWeight());
            assertEquals(original.getThresholdIsUpperBound(),
                storedVersion.getThresholdIsUpperBound());
            assertEquals(original.getMetric().getId(), storedVersion.getMetric().getId());
            assertEquals(original.getRepository().getId(), storedVersion.getRepository().getId());
        }
    }

    /**
     * Test the passing of the function isEqualToRobotSettingsWeb.
     */
    @Test
    public void isEqualToMetricSettingsWeb_pass() {
        MetricSettingsWeb metricSettingsWeb = ModelFactory.getMetricSettingsWeb(settings);
        assert (settings.isEqualToMetricSettingsWeb(metricSettingsWeb));
    }

    /**
     * Test the failing of the function isEqualToRobotSettingsWeb.
     */
    @Test
    public void isEqualToMetricSettingsWeb_fail() {
        MetricSettingsWeb metricSettingsWebDifferingId = ModelFactory.getMetricSettingsWeb(settings);
        Integer newId = ThreadLocalRandom.current().nextInt();;
        while (newId.equals(settings.getRepository().getId())) {
            newId = ThreadLocalRandom.current().nextInt();
        }
        metricSettingsWebDifferingId.setId(newId);
        assert (!settings.isEqualToMetricSettingsWeb(metricSettingsWebDifferingId));

        MetricSettingsWeb metricSettingsWebDifferingWeight = ModelFactory.getMetricSettingsWeb(settings);
        double newWeight = ThreadLocalRandom.current().nextDouble();;
        while (newWeight == settings.getWeight()) {
            newWeight = ThreadLocalRandom.current().nextDouble();
        }
        metricSettingsWebDifferingWeight.setWeight(newWeight);
        assert (!settings.isEqualToMetricSettingsWeb(metricSettingsWebDifferingWeight));

        MetricSettingsWeb metricSettingsWebDifferingThreshold = ModelFactory.getMetricSettingsWeb(settings);
        double threshold = ThreadLocalRandom.current().nextDouble();
        while (threshold == settings.getThreshold()) {
            threshold = ThreadLocalRandom.current().nextDouble();
        }
        metricSettingsWebDifferingThreshold.setThreshold(threshold);
        assert(!settings.isEqualToMetricSettingsWeb(metricSettingsWebDifferingThreshold));


        MetricSettingsWeb metricSettingsWebDifferentIsUpper = ModelFactory.getMetricSettingsWeb(settings);
        metricSettingsWebDifferentIsUpper.setThresholdIsUpperBound(!settings.getThresholdIsUpperBound());
        assert(!settings.isEqualToMetricSettingsWeb(metricSettingsWebDifferentIsUpper));
    }

    /**
     * Test if update works correctly.
     */
    @Test
    public void testUpdate() {
        MetricSettingsWeb updateSettings = ModelFactory.getMetricSettingsWeb(ModelFactory.getMetricSettings(settings.getRepository(), settings.getMetric()));
        updateSettings.setId(settings.getId());
        settings.setAllFromMetricSettingsWeb(updateSettings);
        assert (settings.isEqualToMetricSettingsWeb(updateSettings));
    }

    /**
     * Test for checking whether the equals() method is reflexive.
     */
    @Test
    public void testEqualsReflexivity() {
        assertTrue(this.settings.equals(this.settings));
    }

    /**
     * Test for checking whether the equals() method handles a null object correctly.
     */
    @Test
    public void testEqualsNull() {
        MetricSettings nullSettings = null;
        assertFalse(this.settings.equals(nullSettings));
    }

    /**
     * Test for checking whether the equals() method handles objects of a different type correctly.
     */
    @Test
    public void testEqualsDifferentType() {
        Integer integer = 100;
        assertFalse(this.settings.equals(integer));
    }

    /**
     * Test for checking whether the equals() method checks the threshold field correctly.
     */
    @Test
    public void testEqualsThreshold() {
        MetricSettings copy = new MetricSettings(this.settings);
        copy.setThreshold(copy.getThreshold() + 14.0);
        assertFalse(this.settings.equals(copy));
    }

    /**
     * Test for checking whether the equals() method checks the thresholdIsUpperBound field correctly.
     */
    @Test
    public void testEqualsThresholdUpperBound() {
        MetricSettings copy = new MetricSettings(this.settings);
        copy.setThresholdIsUpperBound(!copy.getThresholdIsUpperBound());
        assertFalse(this.settings.equals(copy));
    }

    /**
     * Test for checking whether the equals() method checks the weight field correctly.
     */
    @Test
    public void testEqualsWeight() {
        MetricSettings copy = new MetricSettings(this.settings);
        copy.setWeight(copy.getWeight() - 12.0);
        assertFalse(this.settings.equals(copy));
    }

    /**
     * Test for checking whether the equals() method checks the metric's ID field correctly.
     */
    @Test
    public void testEqualsMetricId() {
        MetricSettings copy = new MetricSettings(this.settings);
        Metric metric = ModelFactory.getMetric();
        metricRepository.save(metric);
        copy.setMetric(metric);

        assertFalse(this.settings.equals(copy));
    }

    /**
     * Test for checking whether the equals() method correctly verifies a copy of the object.
     */
    @Test
    public void testEqualsCopy() {
        MetricSettings copy = new MetricSettings(this.settings);

        assertTrue(this.settings.equals(copy));
    }

    /**
     * Test for checking whether the isLowQuality() method is correctly evaluated if the threshold
     * is an upper bound.
     */
    @Test
    public void testIsLowQualityUpperbound() {
        this.settings.setThresholdIsUpperBound(true);
        this.settings.setThreshold(1.0);
        assertFalse(this.settings.isLowQuality(0.5));
        assertTrue(this.settings.isLowQuality(1.5));
    }

    /**
     * Test for checking whether the isLowQuality() method is correctly evaluated if the threshold
     * is a lower bound.
     */
    @Test
    public void testIsLowQualityLowerbound() {
        this.settings.setThresholdIsUpperBound(false);
        this.settings.setThreshold(1.0);
        assertFalse(this.settings.isLowQuality(1.5));
        assertTrue(this.settings.isLowQuality(0.5));
    }
}
