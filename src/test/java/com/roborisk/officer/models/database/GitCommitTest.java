package com.roborisk.officer.models.database;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * This class contains tests for the {@link GitCommit} database model.
 */
class GitCommitTest extends OfficerTest {

    /**
     * Max length of GitCommit which should be equal to the private final int MAX_MESSAGE_LENGTH
     * in gitCommit
     */
    private static final int MAX_MESSAGE_LENGTH = 255;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Function that tests the deletion of parent commits.
     * When a commit has a parent commit, the parent field of the child
     * should be set to null when the parent is deleted.
     */
    @Test
    public void testGitCommitStorage() {
        GitCommit original = null;
        try {
            original = ModelFactory.getGitCommit();
        } catch (ParseException e) {
            e.printStackTrace();
            fail("Could not get git commit from model factory");
        }

        GitCommit child = new GitCommit();
        child.setHash("fjaksdlfjaksldfjkalsdjfklasdCHILDjksdlfajl");
        child.setParentCommit(original);
        child.setMessage("another commit message");

        try {
            child.setTimestamp("2001-02-08T21:24:15-07:00");
        } catch (ParseException e) {
            e.printStackTrace();
            fail("Could not parse timestamp");
        }

        // Store both commits
        gitCommitRepository.save(original);
        gitCommitRepository.save(child);

        // Get both commits from the database
        Optional<GitCommit> optionalOriginal = gitCommitRepository.findById(original.getId());
        Optional<GitCommit> optionalChild = gitCommitRepository.findById(child.getId());

        if (optionalOriginal.isEmpty() || optionalChild.isEmpty()) {
            fail("GitCommitTest.testGitCommitStorage: one of the created commits" +
                    " was not found in the database");
        }

        GitCommit storedChild = optionalChild.get();

        // Check if the database child has a parent
        assertNotNull(storedChild.getParentCommit(), "Child commit should have a parent");

        // Delete the parent
        gitCommitRepository.delete(original);

        // Get the child from the database again
        optionalChild = gitCommitRepository.findById(child.getId());

        if (optionalChild.isEmpty()) {
            fail("Child was removed from the database while it should still be in there");
        }
        storedChild = optionalChild.get();

        // Check that the database child does not have a parent anymore
        assertNull(storedChild.getParentCommit(), "Child commit should have null as parent");
    }


    /**
     * Test that the commit message is stored correctly when having exactly the max length.
     */
    @Test
    public void testCommitMessageWithMaxLenght() throws ParseException {
        GitCommit commit = ModelFactory.getGitCommit();

        StringBuilder builder = new StringBuilder();

        String alphabet = "123xyz";

        for (int i = 0; i < GitCommitTest.MAX_MESSAGE_LENGTH; i++) {
            builder.append(alphabet.charAt(ThreadLocalRandom.current().nextInt(0,
                alphabet.length())));
        }

        commit.setMessage(builder.toString());

        gitCommitRepository.save(commit);

        GitCommit fetchedCommit = gitCommitRepository.findById(commit.getId()).get();
        assertNotNull(fetchedCommit);

        assertEquals(fetchedCommit.getMessage(), builder.toString());
    }

    /**
     * Test that the commit message is stored correctly when having more than the max length.
     */
    @Test
    public void testCommitMessageWithMoreThanMaxLenght() throws ParseException {
        GitCommit commit = ModelFactory.getGitCommit();

        StringBuilder builder = new StringBuilder();

        String alphabet = "123xyz";

        for (int i = 0; i < GitCommitTest.MAX_MESSAGE_LENGTH + 1; i++) {
            builder.append(alphabet.charAt(ThreadLocalRandom.current().nextInt(0,
                alphabet.length())));
        }

        commit.setMessage(builder.toString());

        gitCommitRepository.save(commit);

        GitCommit fetchedCommit = gitCommitRepository.findById(commit.getId()).get();
        assertNotNull(fetchedCommit);

        assertEquals(fetchedCommit.getMessage(),
            builder.toString().substring(0, GitCommitTest.MAX_MESSAGE_LENGTH));
    }
}
