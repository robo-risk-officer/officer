package com.roborisk.officer.models.database;

import com.roborisk.officer.models.database.metrics.BranchClassMetricResult;
import com.roborisk.officer.models.database.metrics.BranchCodebaseMetricResult;
import com.roborisk.officer.models.database.metrics.BranchFileMetricResult;
import com.roborisk.officer.models.database.metrics.BranchFunctionMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestClassMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestCodebaseMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestFileMetricResult;
import com.roborisk.officer.models.database.metrics.MergeRequestFunctionMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.enums.MetricTypeEnum;
import com.roborisk.officer.models.enums.QualityTypeEnum;
import com.roborisk.officer.models.web.MetricSettingsWeb;
import com.roborisk.officer.models.web.RobotSettingsWeb;
import com.roborisk.officer.models.web.SettingsApiData;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * This class is used to generate valid objects of all model classes.
 * These valid objects can be used in testing.
 */
public class ModelFactory {
    /**
     * Static integer to be counted up from 1 every time a unique number is needed.
     */
    private static int counter = 1;

    /**
     * Simple function to always return a different number (simply in increasing order) which can
     * be used to guarantee unique GitLab ids
     *
     * @return  integer which has not been returned by this function yet
     */
    private static int getNextCounter() {
        ModelFactory.counter++;

        return ModelFactory.counter;
    }

    /**
     * Creates a fake {@link Repository} object with random data but a fixed GitLab ID.
     *
     * @param gitLabRepositoryId    The ID to use as the GitLab ID for the repository.
     * @return                      A fake repository with the fixed GitLab ID set.
     */
    public static Repository getRepository(int gitLabRepositoryId) {
        // Create repository object
        Repository repository = new Repository();

        // Set all relevant fields
        repository.setGitLabId(gitLabRepositoryId);
        repository.setNamespace("example");
        repository.setSshUrl("git@gitlab.com:example/example" + ModelFactory.getNextCounter()
                + ".git");
        repository.setHttpUrl("https://gitlab.com/example/example" + ModelFactory.getNextCounter()
                + ".git");
        repository.setIsEnabled(true);
        repository.setName("testRepo");
        repository.setPathWithNamespace("test-namespace/example");

        return repository;
    }

    /**
     * Creates a fake {@link Repository} object with random data.
     *
     * @return  A fake repository.
     */
    public static Repository getRepository() {
        return ModelFactory.getRepository(ThreadLocalRandom.current().nextInt());
    }

    /**
     * Creates a fake {@link Metric} object with random data.
     *
     * @return A fake {@link Metric}.
     */
    public static Metric getMetric() {
        // Create repository object
        Metric metric = new Metric();

        // Fill with fake values
        metric.setDisplayName("testMetric");
        metric.setDescription("This is some almost random description.");
        metric.setMetricType(MetricTypeEnum.values()
            [ThreadLocalRandom.current().nextInt(0, MetricTypeEnum.values().length)]);
        metric.setName("testMetricInternalName" + ModelFactory.getNextCounter());
        metric.setQualityType(QualityTypeEnum.values()
            [ThreadLocalRandom.current().nextInt(0, QualityTypeEnum.values().length)]);
        return metric;
    }

    /**
     * Creates a fake {@link RobotSettings} object with random data.
     *
     * @param repository    The {@link Repository} the settings are associated to.
     * @return              A fake {@link RobotSettings}.
     */
    public static RobotSettings getRobotSettings(Repository repository) {
        // Create robot settings object
        RobotSettings robotSettings = new RobotSettings();

        // Fill with fake values
        robotSettings.setAutomaticReviewOnNewMr(ThreadLocalRandom.current().nextBoolean());
        robotSettings.setAutomaticReviewOnNewCommit(ThreadLocalRandom.current().nextBoolean());
        robotSettings.setFalsePositiveThreshold(ThreadLocalRandom.current().nextInt());
        robotSettings.setReviewHistoryLength(ThreadLocalRandom.current().nextInt());
        robotSettings.setRepository(repository);

        return robotSettings;
    }

    /**
     * Creates a fake {@link RobotSettingsWeb} object with data based on a {@link RobotSettings}.
     *
     * @param robotSettings The {@link RobotSettings} the web settings are based on.
     * @return              A fake {@link RobotSettingsWeb} with the same values as the
     *                      {@link RobotSettings}.
     */
    public static RobotSettingsWeb getRobotSettingsWeb(RobotSettings robotSettings) {
        // Create robot settings object
        RobotSettingsWeb robotSettingsWeb = new RobotSettingsWeb();

        // Fill with values
        robotSettingsWeb.setId(robotSettings.getId());
        robotSettingsWeb.setAutomaticReviewOnNewCommit(robotSettings.getAutomaticReviewOnNewCommit());
        robotSettingsWeb.setAutomaticReviewOnNewMr(robotSettings.getAutomaticReviewOnNewMr());
        robotSettingsWeb.setFalsePositiveThreshold(robotSettings.getFalsePositiveThreshold());
        robotSettingsWeb.setReviewHistoryLength(robotSettings.getReviewHistoryLength());

        return robotSettingsWeb;
    }

    /**
     * Creates a fake {@link MetricSettings} object with random data.git s
     *
     * @param repository    The {@link Repository} the settings are associated to.
     * @param metric        The {@link Metric} the settings are associated to.
     * @return              A fake {@link MetricSettings}.
     */
    public static MetricSettings getMetricSettings(Repository repository, Metric metric) {
        // Create metric settings object
        MetricSettings metricSettings = new MetricSettings();

        // Fill with fake values
        metricSettings.setMetric(metric);
        metricSettings.setRepository(repository);
        metricSettings.setThreshold(ThreadLocalRandom.current().nextDouble(0, 100));
        metricSettings.setThresholdIsUpperBound(ThreadLocalRandom.current().nextBoolean());
        metricSettings.setWeight(ThreadLocalRandom.current().nextDouble());

        return metricSettings;
    }

    /**
     * Creates a fake {@link MetricSettingsWeb} object with data based on a {@link MetricSettings}.
     *
     * @param metricSettings    The {@link MetricSettings} the web settings are based upon.
     * @return                  A fake {@link MetricSettings} with the same values as the
     *                          {@link MetricSettings}.
     */
    public static MetricSettingsWeb getMetricSettingsWeb(MetricSettings metricSettings) {
        // Create metric settings web object
        MetricSettingsWeb metricSettingsWeb = new MetricSettingsWeb();

        // Fill with values
        metricSettingsWeb.setId(metricSettings.getId());
        metricSettingsWeb.setName(metricSettings.getMetric().getName());
        metricSettingsWeb.setThreshold(metricSettings.getThreshold());
        metricSettingsWeb.setThresholdIsUpperBound(metricSettings.getThresholdIsUpperBound());
        metricSettingsWeb.setWeight(metricSettings.getWeight());

        return metricSettingsWeb;
    }

    /**
     * Creates a fake {@link SettingsApiData} object with data based on the parameter.
     *
     * @param robotSettings     The {@link RobotSettings} to be contained in the SettingsApiData.
     * @param metricSettings    The {@link MetricSettings} to be contained in the SettingsApiData.
     * @return                  A fake {@link SettingsApiData} with data based on the parameter.
     */
    public static SettingsApiData getSettingsApiData(RobotSettings robotSettings, List<MetricSettings> metricSettings) {
        RobotSettingsWeb robotSettingsWeb = getRobotSettingsWeb(robotSettings);

        List<MetricSettingsWeb> metricSettingsWeb = new ArrayList<>();

        for (int i = 0; i < metricSettings.size(); i++) {
            metricSettingsWeb.add(getMetricSettingsWeb(metricSettings.get(i)));
        }

        return getSettingsApiData(robotSettingsWeb, metricSettingsWeb);
    }

    /**
     * Creates a fake {@link SettingsApiData} object with data based on the parameter.
     *
     * @param robotSettingsWeb  The {@link RobotSettingsWeb} to be contained in the
     *                          {@link SettingsApiData}.
     * @param metricSettingsWeb The {@link MetricSettingsWeb} to be contained in the
     *                          {@link SettingsApiData}.
     * @return                  A fake {@link SettingsApiData} with data based on the parameter.
     */
    public static SettingsApiData getSettingsApiData(RobotSettingsWeb robotSettingsWeb,
            List<MetricSettingsWeb> metricSettingsWeb) {
        SettingsApiData settingsApiData = new SettingsApiData();

        settingsApiData.setRobotSettings(robotSettingsWeb);
        settingsApiData.setMetricSettings(metricSettingsWeb);

        return null;
    }

    /**
     * Create a fake {@link GitCommit} object with random data.
     *
     * @param parentCommit      The parent of this commit.
     * @param hash              The hash to set for the commit.
     * @throws ParseException   When the TimeStamp cannot be parsed.
     * @return                  A fake {@link GitCommit} with the parentCommit set as parent.
     */
    public static GitCommit getGitCommit(GitCommit parentCommit, String hash)
            throws ParseException {
        GitCommit gitCommit = new GitCommit();
        gitCommit.setHash(hash);
        gitCommit.setParentCommit(parentCommit);
        gitCommit.setTimestamp("2015-04-08T21:00:25-07:00");
        gitCommit.setMessage("This is my very long git commit message which I wrote that " +
            "extends to way longer than we can display on a normal screen and thus probably has " +
            "to be shortened");
        return gitCommit;
    }

    /**
     * Create a fake {@link GitCommit} object with random data but a set hash.
     *
     * @param hash              The hash to set for the commit.
     * @throws ParseException   When the TimeStamp cannot be parsed.
     * @return                  A fake {@link GitCommit} with a null parent commit.
     */
    public static GitCommit getGitCommit(String hash) throws ParseException {
        return ModelFactory.getGitCommit(null, hash);
    }

    /**
     * Create a fake {@link GitCommit} object with random data but a set parent commit.
     *
     * @param parentCommit      The parent of this commit.
     * @throws ParseException   When the TimeStamp cannot be parsed.
     * @return                  A fake {@link GitCommit} with the parentCommit set as parent.
     */
    public static GitCommit getGitCommit(GitCommit parentCommit) throws ParseException {
        return ModelFactory.getGitCommit(parentCommit,
                Integer.toString(ThreadLocalRandom.current().nextInt()));
    }

    /**
     * Create a fake {@link GitCommit} object with random data.
     *
     * @throws ParseException   When the TimeStamp cannot be parsed.
     * @return                  A fake {@link GitCommit} with a null parent commit.
     */
    public static GitCommit getGitCommit() throws ParseException {
        return ModelFactory.getGitCommit(null,
                Integer.toString(ThreadLocalRandom.current().nextInt()));
    }

    /**
     * Create a fake {@link Branch} object.
     *
     * @param repository    The {@link Repository} to link to the branch.
     * @param commit        The {@link GitCommit} to use as a last commit.
     * @return              A fake {@link Branch}.
     */
    public static Branch getBranch(Repository repository, GitCommit commit) {
        return ModelFactory.getBranch(repository, commit,
                Integer.toString(ThreadLocalRandom.current().nextInt()));
    }

    /**
     * Create a fake {@link Branch} object with random data but a fixed name.
     *
     * @param repository    The {@link Repository} to link to the branch.
     * @param commit        The {@link GitCommit} to use as a last commit.
     * @param name          The name the {@link Branch} should get.
     * @return              A fake {@link Branch}.
     */
    public static Branch getBranch(Repository repository, GitCommit commit, String name) {
        Branch branch = new Branch();
        branch.setName(name);
        branch.setRepository(repository);
        branch.setLatestCommit(commit);

        return branch;
    }

    /**
     * Create a fake {@link BranchFile} object with random data.
     *
     * @param branch    The {@link Branch} this file belongs to.
     * @param gitCommit The {@link GitCommit} where this file was last changed.
     * @return          A fake {@link BranchFile}.
     */
    public static BranchFile getBranchFile(Branch branch, GitCommit gitCommit) {
        BranchFile branchFile = new BranchFile();
        branchFile.setBranch(branch);
        branchFile.setIsDeleted(false);
        branchFile.setLastChangeCommit(gitCommit);
        int fileNumber =  ModelFactory.getNextCounter();
        branchFile.setFileName("abcadfs" + fileNumber);
        branchFile.setFilePath("asdf/as/e/d/abcadfs" + fileNumber);

        return branchFile;
    }

    /**
     * Create a fake {@link BranchFileMetricResult} object with random data.
     *
     * @param file      The {@link BranchFile} this result belongs to.
     * @param metric    The {@link Metric} this result belongs to.
     * @return          A fake {@link BranchFileMetricResult}.
     */
    public static BranchFileMetricResult getBranchFileMetricResult(BranchFile file, Metric metric) {
        BranchFileMetricResult result = new BranchFileMetricResult();

        result.setBranchFile(file);
        result.setMetric(metric);
        result.setLastChangeCommit(file.getBranch().getLatestCommit());
        result.setIsLowQuality(ThreadLocalRandom.current().nextBoolean());
        result.setScore(ThreadLocalRandom.current().nextDouble());

        return result;
    }

    /**
     * Create a fake {@link MergeRequestReview} object with random data.
     *
     * @param gitCommit     The {@link GitCommit} the merge request review belongs to.
     * @param mergeRequest  The {@link MergeRequest} the merge request review belongs to.
     * @return              A fake {@link MergeRequest}.
     */
    public static MergeRequestReview getMergeRequestReview(GitCommit gitCommit,
            MergeRequest mergeRequest) {
        MergeRequestReview mrReview = new MergeRequestReview();
        mrReview.setCommit(gitCommit);
        mrReview.setMergeRequest(mergeRequest);

        return mrReview;
    }

    /**
     * Create a {@link DiscussionThread} object with random data.
     *
     * @param mergeRequest  The {@link MergeRequest} that should be linked to the discussion.
     * @return              A fake {@link DiscussionThread}.
     */
    public static DiscussionThread getSimpleDiscussionThread(MergeRequest mergeRequest) {
        return ModelFactory.getSimpleDiscussionThread(mergeRequest,
                ThreadLocalRandom.current().nextBoolean());
    }

    /**
     * Create a {@link DiscussionThread} object with random data but a fixed resolution status.
     *
     * @param mergeRequest  The {@link MergeRequest} that should be linked to the discussion.
     * @param isResolved    The resolved status the {@link DiscussionThread} should get.
     * @return              A fake {@link DiscussionThread}.
     */
    public static DiscussionThread getSimpleDiscussionThread(MergeRequest mergeRequest,
             boolean isResolved) {
        return ModelFactory.getSimpleDiscussionThread(mergeRequest,
                ThreadLocalRandom.current().nextBoolean(),
                String.valueOf(ModelFactory.getNextCounter()));
    }

    /**
     * Create a {@link DiscussionThread} object with random data but a fixed resolution status.
     *
     * @param mergeRequest  The {@link MergeRequest} that should be linked to the discussion.
     * @param isResolved    The resolved status the {@link DiscussionThread} should get.
     * @param gitLabId      The GitLab ID that should be assigned to the {@link DiscussionThread}.
     * @return              A fake {@link DiscussionThread}.
     */
    public static DiscussionThread getSimpleDiscussionThread(MergeRequest mergeRequest,
            boolean isResolved, String gitLabId) {
        DiscussionThread discussionThread = new DiscussionThread();
        discussionThread.setGitLabId(gitLabId);
        discussionThread.setIsResolved(isResolved);
        discussionThread.setMergeRequest(mergeRequest);

        return discussionThread;
    }

    /**
     * Create a {@link RobotNote} object with random data.
     *
     * @param discussionThread  The {@link DiscussionThread} linked to the note.
     * @return                  A fake {@link RobotNote}.
     */
    public static RobotNote getSimpleNote(DiscussionThread discussionThread) {
        return ModelFactory.getSimpleNote(discussionThread, ThreadLocalRandom.current().nextInt());
    }

    /**
     * Create a {@link RobotNote} object with random data but a specific GitLab ID.
     *
     * @param discussionThread  The {@link DiscussionThread} linked to the note.
     * @param gitLabId          The GitLab ID to set for the {@link RobotNote}.
     * @return                  A fake {@link RobotNote}.
     */
    public static RobotNote getSimpleNote(DiscussionThread discussionThread, int gitLabId) {
        RobotNote note = new RobotNote();
        note.setGitLabId(gitLabId);
        note.setDiscussionThread(discussionThread);

        return note;
    }

    /**
     * Create a {@link MergeRequestClassObservation} object with random data.
     *
     * @param mergeRequestFile  The {@link MergeRequestFile} the observation belongs to.
     * @return                  A fake {@link MergeRequestClassObservation}.
     */
    public static MergeRequestClassObservation getMergeRequestClassObservation(MergeRequestFile mergeRequestFile) {
        MergeRequestClassObservation mergeRequestClassObservation = new MergeRequestClassObservation();
        mergeRequestClassObservation.setClassName("haha");
        mergeRequestClassObservation.setLineNumber(1);
        mergeRequestClassObservation.setIsFalsePositive(false);
        mergeRequestClassObservation.setIsSuppressed(true);
        mergeRequestClassObservation.setMergeRequestFile(mergeRequestFile);

        return mergeRequestClassObservation;
    }

    /**
     * Create a {@link MergeRequestFile} object with random data.
     *
     * @param mergeRequestReview    The {@link MergeRequestReview} the file belongs to.
     * @return                      A fake {@link MergeRequestFile}.
     */
    public static MergeRequestFile getMergeRequestFile(MergeRequestReview mergeRequestReview) {
        MergeRequestFile mergeRequestFile = new MergeRequestFile();
        mergeRequestFile.setFileName("abc.txt");
        mergeRequestFile.setFilePath("bleep/abc.txt");
        mergeRequestFile.setMergeRequestReview(mergeRequestReview);

        return mergeRequestFile;
    }

    /**
     * Create a {@link MergeRequest} object with random data.
     *
     * @param sourceBranch  The source {@link Branch} the merge request belongs to.
     * @param targetBranch  The target {@link Branch} the merge request belongs to.
     * @return              A fake {@link MergeRequest}.
     */
    public static MergeRequest getMergeRequest(Branch sourceBranch, Branch targetBranch) {
        return ModelFactory.getMergeRequest(sourceBranch, targetBranch,
                ThreadLocalRandom.current().nextInt(), ThreadLocalRandom.current().nextInt());
    }

    /**
     * Create a {@link MergeRequest} object with random data but fixed GitLab ID and GitLab IID.
     *
     * @param sourceBranch  The source {@link Branch} the merge request belongs to.
     * @param targetBranch  The target {@link Branch} the merge request belongs to.
     * @param gitLabId      The GitLab ID of the merge request.
     * @param gitLabIid     The GitLab IID of the merge request.
     * @return              A fake {@link MergeRequest} with the specified GitLab ID and IID.
     */
    public static MergeRequest getMergeRequest(Branch sourceBranch, Branch targetBranch,
            int gitLabId, int gitLabIid) {
        MergeRequest mergeRequest = new MergeRequest();
        mergeRequest.setCreatedAt(ThreadLocalRandom.current().nextInt());
        mergeRequest.setGitLabId(gitLabId);
        mergeRequest.setGitLabIid(gitLabIid);
        mergeRequest.setMergeStatus("done");
        mergeRequest.setState("STATE");
        mergeRequest.setTitle("lovely title");
        mergeRequest.setUpdatedAt(ThreadLocalRandom.current().nextInt());
        mergeRequest.setSourceBranch(sourceBranch);
        mergeRequest.setTargetBranch(targetBranch);

        return mergeRequest;
    }

    /**
     * Create a {@link BranchFunctionObservation} with random data.
     *
     * @param gitCommit     The {@link GitCommit} object that should be linked to the observation.
     * @param branchFile    The {@link BranchFile} object that should be linked to the observation.
     * @return              A fake {@link BranchFunctionObservation} linked to the specified
     *                      {@link GitCommit} and {@link BranchFile} objects.
     */
    public static BranchFunctionObservation getBranchFunctionObservation(GitCommit gitCommit,
            BranchFile branchFile) {
        BranchFunctionObservation branchFunctionObservation = new BranchFunctionObservation();

        branchFunctionObservation.setClassName(
                Long.toString(ThreadLocalRandom.current().nextLong()));
        branchFunctionObservation.setFunctionName(
                Long.toString(ThreadLocalRandom.current().nextLong()));
        branchFunctionObservation.setGitCommit(gitCommit);
        branchFunctionObservation.setBranchFile(branchFile);
        branchFunctionObservation.setLineNumber(
                ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE));

        return branchFunctionObservation;
    }

    /**
     * Create a {@link BranchFunctionMetricResult} with random data.
     *
     * @param branchFunctionObservation The {@link BranchFunctionObservation} the result is about.
     * @param metric                    The {@link Metric} that yielded the result.
     * @return                          A fake {@link BranchFunctionMetricResult} linked to the
     *                                  specified {@link BranchFunctionObservation} and
     *                                  {@link Metric} objects.
     */
    public static BranchFunctionMetricResult getBranchFunctionMetricResult(
            BranchFunctionObservation branchFunctionObservation, Metric metric) {
        BranchFunctionMetricResult branchFunctionMetricResult = new BranchFunctionMetricResult();

        branchFunctionMetricResult.setBranchFunctionObservation(branchFunctionObservation);
        branchFunctionMetricResult.setIsLowQuality(ThreadLocalRandom.current().nextBoolean());
        branchFunctionMetricResult.setMetric(metric);
        branchFunctionMetricResult.setScore(ThreadLocalRandom.current().nextDouble());

        return branchFunctionMetricResult;
    }

    /**
     * Create a {@link BranchClassObservation} with random data.
     *
     * @param gitCommit     The {@link GitCommit} object that should be linked to the observation.
     * @param branchFile    The {@link BranchFile} object that should be linked to the observation.
     * @return              A fake {@link BranchClassObservation} linked to the specified
     *                      {@link GitCommit} and {@link BranchFile} objects.
     */
    public static BranchClassObservation getBranchClassObservation(GitCommit gitCommit,
            BranchFile branchFile) {
        BranchClassObservation branchClassObservation = new BranchClassObservation();

        branchClassObservation.setClassName(
                Long.toString(ThreadLocalRandom.current().nextLong()));
        branchClassObservation.setGitCommit(gitCommit);
        branchClassObservation.setBranchFile(branchFile);
        branchClassObservation.setLineNumber(
                ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE));

        return branchClassObservation;
    }

    /**
     * Create a {@link BranchClassMetricResult} with random data.
     *
     * @param branchClassObservation    The {@link BranchClassObservation} the result is about.
     * @param metric                    The {@link Metric} that yielded the result.
     * @return                          A fake {@link BranchClassMetricResult} linked to the
     *                                  specified {@link BranchClassObservation} and
     *                                  {@link Metric} objects.
     */
    public static BranchClassMetricResult getBranchClassMetricResult(
            BranchClassObservation branchClassObservation, Metric metric) {
        BranchClassMetricResult branchClassMetricResult = new BranchClassMetricResult();

        branchClassMetricResult.setBranchClassObservation(branchClassObservation);
        branchClassMetricResult.setIsLowQuality(ThreadLocalRandom.current().nextBoolean());
        branchClassMetricResult.setMetric(metric);
        branchClassMetricResult.setScore(ThreadLocalRandom.current().nextDouble());

        return branchClassMetricResult;
    }

    /**
     * Create a {@link MergeRequestCodebaseMetricResult} with random data.
     *
     * @param mergeRequestReview    The {@link MergeRequestReview} to link to the result.
     * @param metric                The {@link Metric} to link to the result.
     * @param metricSettings        The {@link MetricSettings} to link to the result.
     * @return                      A fake {@link MergeRequestCodebaseMetricResult} with the
     *                              specified parameters set and random data otherwise.
     */
    public static MergeRequestCodebaseMetricResult getMRCodebaseMetric(
            MergeRequestReview mergeRequestReview, Metric metric, MetricSettings metricSettings) {
        MergeRequestCodebaseMetricResult mrCodebaseMetricResult =
                new MergeRequestCodebaseMetricResult();

        mrCodebaseMetricResult.setMergeRequestReview(mergeRequestReview);
        mrCodebaseMetricResult.setMetric(metric);
        mrCodebaseMetricResult.setScore(ThreadLocalRandom.current()
                .nextDouble(0, 100));
        mrCodebaseMetricResult.setIsLowQuality(
                metricSettings.isLowQuality(mrCodebaseMetricResult.getScore()));

        return mrCodebaseMetricResult;
    }

    /**
     * Create a {@link BranchCodebaseMetricResult} with random data.
     *
     * @param branch    The {@link Branch} that should be associated to the result.
     * @param commit    The {@link GitCommit} that should be associated to the result.
     * @param metric    The {@link Metric} that should be associated to the result.
     * @return          The fake {@link BranchCodebaseMetricResult}.
     */
    public static BranchCodebaseMetricResult getBranchCodebaseMetricResult(Branch branch,
            GitCommit commit, Metric metric) {
        BranchCodebaseMetricResult branchCodebaseMetricResult = new BranchCodebaseMetricResult();

        branchCodebaseMetricResult.setBranch(branch);
        branchCodebaseMetricResult.setLastChangeCommit(commit);
        branchCodebaseMetricResult.setMetric(metric);
        branchCodebaseMetricResult.setIsLowQuality(ThreadLocalRandom.current().nextBoolean());
        branchCodebaseMetricResult.setScore(ThreadLocalRandom.current().nextDouble());

        return branchCodebaseMetricResult;
    }

    /**
     * Create a {@link BranchFileMetricResult} with random data.
     *
     * @param commit    The {@link GitCommit} that should be associated to the result.
     * @param file      The {@link BranchFile} that should be associated to the result.
     * @param metric    The {@link Metric} that should be associated to the result.
     * @return          The fake {@link BranchFileMetricResult}.
     */
    public static BranchFileMetricResult getBranchFileMetricResult(GitCommit commit,
            BranchFile file, Metric metric) {
        BranchFileMetricResult branchFileMetricResult = new BranchFileMetricResult();

        branchFileMetricResult.setLastChangeCommit(commit);
        branchFileMetricResult.setBranchFile(file);
        branchFileMetricResult.setMetric(metric);
        branchFileMetricResult.setIsLowQuality(ThreadLocalRandom.current().nextBoolean());
        branchFileMetricResult.setScore(ThreadLocalRandom.current().nextDouble());

        return branchFileMetricResult;
    }

    /**
     * Create a {@link MergeRequestFileMetricResult} based on the associated
     * {@link MergeRequestFile}, {@link Metric} and {@link MetricSettings} passed as parameters.
     *
     * @param mergeRequestFile  The {@link MergeRequestFile} for which a result should be created.
     * @param metric            The {@link Metric} for which a result should be created.
     * @param metricSettings    The {@link MetricSettings} based on which the result will be
     *                          evaluated.
     * @return                  The newly created {@link MergeRequestFileMetricResult}.
     */
    public static MergeRequestFileMetricResult getMergeRequestFileMetricResult(
            MergeRequestFile mergeRequestFile, Metric metric, MetricSettings metricSettings) {
        MergeRequestFileMetricResult result = new MergeRequestFileMetricResult();

        result.setMergeRequestFile(mergeRequestFile);
        result.setDescription("This is one of the file metrics which evaluates files randomly.");
        result.setScore(ThreadLocalRandom.current().nextDouble(0, 100));
        result.setIsLowQuality(metricSettings.isLowQuality(result.getScore()));
        result.setMetric(metric);

        return result;
    }

    /**
     * Create a {@link MergeRequestFunctionObservation} which is associated to the specified
     * {@link MergeRequestFile}.
     *
     * @param file  The {@link MergeRequestFile} to which the newly created
     *              {@link MergeRequestFunctionObservation} should be associated to.
     * @return      The newly created {@link MergeRequestFunctionObservation}.
     */
    public static MergeRequestFunctionObservation getMergeRequestFunctionObservation(
            MergeRequestFile file) {
        MergeRequestFunctionObservation functionObservation = new MergeRequestFunctionObservation();

        functionObservation.setClassName("className");
        functionObservation.setFunctionName("functionName");
        functionObservation.setMergeRequestFile(file);
        functionObservation.setIsFalsePositive(false);
        functionObservation.setIsSuppressed(false);

        return functionObservation;
    }

    /**
     * Create a new {@link MergeRequestClassMetricResult} based on a
     * {@link MergeRequestClassObservation}, a {@link Metric} and a {@link MetricSettings}.
     *
     * @param classObservation      The {@link MergeRequestClassObservation} for which the result
     *                              should be created..
     * @param metric                The {@link Metric} for which the result should be created.
     * @param classMetricSettings   The {@link MetricSettings} based on which the result will be
     *                              evaluated.
     * @return                      The newly created {@link MergeRequestClassMetricResult} object.
     */
    public static MergeRequestClassMetricResult getMergeRequestClassMetricResult (
            MergeRequestClassObservation classObservation,
            Metric metric,
            MetricSettings classMetricSettings) {
        MergeRequestClassMetricResult metricResult = new MergeRequestClassMetricResult();

        metricResult.setMergeRequestClassObservation(classObservation);
        metricResult.setMetric(metric);
        metricResult.setScore(ThreadLocalRandom.current().nextDouble(0, 100));
        metricResult.setIsLowQuality(classMetricSettings.isLowQuality(metricResult.getScore()));

        return metricResult;
    }

    /**
     * Create a new {@link MergeRequestFunctionMetricResult} based on a
     * {@link MergeRequestFunctionObservation}, a {@link Metric} and a {@link MetricSettings}.
     *
     * @param functionObservation      The {@link MergeRequestFunctionObservation} for which the
     *                                 result should be created..
     * @param metric                   The {@link Metric} for which the result should be created.
     * @param functionMetricSettings   The {@link MetricSettings} based on which the result will be
     *                                 evaluated.
     * @return                         The newly created {@link MergeRequestFunctionMetricResult}
     *                                 object.
     */
    public static MergeRequestFunctionMetricResult getMergeRequestFunctionMetricResult (
            MergeRequestFunctionObservation functionObservation,
            Metric metric,
            MetricSettings functionMetricSettings) {
        MergeRequestFunctionMetricResult metricResult = new MergeRequestFunctionMetricResult();

        metricResult.setMergeRequestFunctionObservation(functionObservation);
        metricResult.setMetric(metric);
        metricResult.setScore(ThreadLocalRandom.current().nextDouble(0, 100));
        metricResult.setIsLowQuality(functionMetricSettings.isLowQuality(metricResult.getScore()));

        return metricResult;
    }
}
