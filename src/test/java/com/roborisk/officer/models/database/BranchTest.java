package com.roborisk.officer.models.database;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * This class contains tests for the {@link Branch} database model.
 */
class BranchTest extends OfficerTest {

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Database repository to handle {@link BranchFile} objects.
     */
    @Autowired
    BranchFileRepository branchFileRepository;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * {@link Repository} object for testing.
     */
    Repository myRepo;

    /**
     * {@link GitCommit} object for testing.
     */
    GitCommit gitCommit;

    /**
     * Create and store the objects needed to test the branch.
     */
    @BeforeEach
    public void prepareTest() {
        // Create the extra utility objects
        myRepo = ModelFactory.getRepository();
        try {
            gitCommit = ModelFactory.getGitCommit();
        } catch (ParseException e) {
            fail("Could not get commit");
            e.printStackTrace();
        }

        // Save them to the database
        repositoryRepository.save(myRepo);
        gitCommitRepository.save(gitCommit);
    }

    /**
     * Create a branch and test its links with connected objects
     */
    @Test
    public void testBranchStorage() {
        // Create a branch
        Branch myBranch = ModelFactory.getBranch(myRepo, gitCommit);

        // Give the branch a list of branch files
        BranchFile branchFile1 = ModelFactory.getBranchFile(myBranch, gitCommit);
        BranchFile branchFile2 = ModelFactory.getBranchFile(myBranch, gitCommit);

        // Save the branch to the database
        branchRepository.save(myBranch);

        // Save the branch files to the database
        // Must do this after the branch is saved to the db!
        branchFileRepository.save(branchFile1);
        branchFileRepository.save(branchFile2);

        // Get the stored branch from the database
        Optional<Branch> branchOptional = branchRepository.findById(myBranch.getId());

        // Make sure it exists
        if (branchOptional.isEmpty()) {
            fail("BranchTest.testBranchStorage: branch was not found in database");
        }

        Branch storedBranch = branchOptional.get();

        // Check if the linked objects have the correct Id
        assertEquals(storedBranch.getLatestCommit().getId(), gitCommit.getId());
        assertEquals(storedBranch.getRepository().getId(), myRepo.getId());

        // Check if the branch files are also in the object
        List<BranchFile> files = this.branchFileRepository.findAllByBranchId(storedBranch.getId());
        // The assertions below are this way because the order in which the files are retrieved
        // from the database is non-deterministic
        assertTrue(files.get(0).getId().equals(branchFile1.getId()) ||
                files.get(0).getId().equals(branchFile2.getId()));
        assertTrue(files.get(1).getId().equals(branchFile1.getId()) ||
                files.get(1).getId().equals(branchFile2.getId()));

        // Delete the branch
        branchRepository.delete(myBranch);

        // Check if the branch files are also deleted now
        Optional<BranchFile> optionalBranchFile;
        optionalBranchFile = branchFileRepository.findById(branchFile1.getId());
        assertTrue(optionalBranchFile.isEmpty());
    }
}
