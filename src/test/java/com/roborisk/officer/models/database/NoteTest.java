package com.roborisk.officer.models.database;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.DiscussionThreadRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestClassObservationRepository;
import com.roborisk.officer.models.repositories.MergeRequestFileRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.repositories.RobotNoteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * This class contains tests for the {@link RobotNote} database model.
 */
class NoteTest extends OfficerTest {

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link MergeRequestReview} objects.
     */
    @Autowired
    MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Database repository to handle {@link MergeRequestFile} objects.
     */
    @Autowired
    MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * Database repository to handle {@link MergeRequestClassObservation} objects.
     */
    @Autowired
    MergeRequestClassObservationRepository mergeRequestClassObservationRepository;

    /**
     * Database repository to handle {@link RobotNote} objects.
     */
    @Autowired
    RobotNoteRepository robotNoteRepository;

    /**
     * Database repository to handle {@link RobotNote} objects.
     */
    @Autowired
    DiscussionThreadRepository discussionThreadRepository;

    /**
     * Repository model to link to the {@link MergeRequest}.
     */
    Repository repository;

    /**
     * Commit models to link to the {@link Branch}.
     */
    GitCommit gitCommit1, gitCommit2;

    /**
     * Branche models to link to the {@link MergeRequest}.
     */
    Branch sourceBranch, targetBranch;

    /**
     * Merge request model to connect all other models to.
     */
    MergeRequest mergeRequest;

    /**
     * Merge request review model to connect to the {@link MergeRequest}.
     */
    MergeRequestReview mergeRequestReview;

    /**
     * Merge request file model to link to the {@link MergeRequest}.
     */
    MergeRequestFile mergeRequestFile;

    /**
     * Class observation model to use for the {@link MergeRequestReview}.
     */
    MergeRequestClassObservation mergeRequestClassObservation;

    /**
     * Class observation model to use for the {@link DiscussionThread}.
     */
    DiscussionThread discussionThread;

    /**
     * Create all objects needed to create a functioning Note object.
     */
    @BeforeEach
    public void prepareTest() {
        // Create all objects needed to create a note
        try {
            // Git commit 1 is the first commit, commit 2 builds on top of it in a different branch
            gitCommit1 = ModelFactory.getGitCommit();
            gitCommit2 = ModelFactory.getGitCommit();
            gitCommit2.setParentCommit(gitCommit1);
        } catch (ParseException e) {
            fail("Failed to parse git commit");
            e.printStackTrace();
        }

        // Set up the repo and branches
        repository = ModelFactory.getRepository();
        sourceBranch = ModelFactory.getBranch(repository, gitCommit2);
        targetBranch = ModelFactory.getBranch(repository, gitCommit1);

        // Create the merge request, and review objects
        mergeRequest = ModelFactory.getMergeRequest(sourceBranch, targetBranch);
        mergeRequestReview = ModelFactory.getMergeRequestReview(gitCommit2, mergeRequest);
        mergeRequestFile = ModelFactory.getMergeRequestFile(mergeRequestReview);
        mergeRequestClassObservation = ModelFactory.getMergeRequestClassObservation(mergeRequestFile);
        discussionThread = ModelFactory.getSimpleDiscussionThread(mergeRequest);

        // Store all objects in the database (in the correct order!)
        gitCommitRepository.save(gitCommit1);
        gitCommitRepository.save(gitCommit2);
        repositoryRepository.save(repository);
        branchRepository.save(sourceBranch);
        branchRepository.save(targetBranch);
        mergeRequestRepository.save(mergeRequest);
        mergeRequestReviewRepository.save(mergeRequestReview);
        mergeRequestFileRepository.save(mergeRequestFile);
        mergeRequestClassObservationRepository.save(mergeRequestClassObservation);
        discussionThreadRepository.save(discussionThread);
    }

    /**
     * Test creation of a simple note and the corresponding needed objects.
     * Also tests if deletion is done properly in the database.
     * On deletion of the repository everything (except commits) related to that
     * repo should be deleted form the db too.
     */
    @Test
    public void simpleNoteTest() {
        // Create a note based on a class observation
        RobotNote note = ModelFactory.getSimpleNote(discussionThread);
        // Save the note to the database
        robotNoteRepository.save(note);

        // Get the stored note from the database
        Optional<RobotNote> optionalNote = robotNoteRepository.findById(note.getId());
        assertTrue(optionalNote.isPresent());
        RobotNote clone = optionalNote.get();

        // Check if everything gets removed on deletion of the repository
        repositoryRepository.delete(repository);

        // Check if the note is removed from the database
        optionalNote = robotNoteRepository.findById(note.getId());
        assertTrue(optionalNote.isEmpty());

        // Check if all setup objects have been deleted
        checkProperDeletion();
    }

    /**
     * Checks if all the setup objects have been deleted from the database
     */
    private void checkProperDeletion() {
        // Check if classObservation is deleted
        Optional<MergeRequestClassObservation> optionalClassObservation =
            mergeRequestClassObservationRepository.findById(mergeRequestClassObservation.getId());
        assertTrue(optionalClassObservation.isEmpty());

        // Check if merge request file is deleted
        Optional<MergeRequestFile> optionalMergeRequestFile = mergeRequestFileRepository.findById(mergeRequestFile.getId());
        assertTrue(optionalMergeRequestFile.isEmpty());

        // Check if merge request review is deleted
        Optional<MergeRequestReview> optionalMergeRequestReview =
                mergeRequestReviewRepository.findById(mergeRequestReview.getId());
        assertTrue(optionalMergeRequestReview.isEmpty());

        // Check if merge request is deleted
        Optional<MergeRequest> optionalMergeRequest = mergeRequestRepository.findById(mergeRequest.getId());
        assertTrue(optionalMergeRequest.isEmpty());

        // Check if one of the branches is deleted (other one will be too)
        Optional<Branch> optionalBranch = branchRepository.findById(sourceBranch.getId());
        assertTrue(optionalBranch.isEmpty());

        /*
         * Currently GitCommit objects are not linked to much in the database.
         * This means that they stay when related objects get deleted in the database.
         * It is possibly wanted to change this at some point.
         */
        Optional<GitCommit> optionalGitCommit = gitCommitRepository.findById(gitCommit2.getId());
        assertFalse(optionalGitCommit.isEmpty());

        // Check if the repository is indeed gone
        Optional<Repository> optionalRepository = repositoryRepository.findById(repository.getId());
        assertTrue(optionalRepository.isEmpty());
    }
}
