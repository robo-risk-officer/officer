package com.roborisk.officer.models.repositories;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * This class contains tests for the {@link RepositoryRepository} repository.
 */
public class RepositoryRepositoryTest extends OfficerTest {

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * {@link Repository} object for testing.
     */
    private Repository repository;

    /**
     * Initialize a repository to query against.
     */
    @BeforeEach
    public void insertRepository() {
        this.repository = ModelFactory.getRepository();

        this.repositoryRepository.save(this.repository);
    }

    /**
     * Test finding a repository by GitLab ID.
     */
    @Test
    public void testFindRepositoryByGitLabId() {
        Optional<Repository> optional = this.repositoryRepository.findByGitLabId(this.repository.getGitLabId());
        if (optional.isEmpty()) {
            fail("Repository not found via GitLab id.");
        }

        Repository clone = optional.get();

        assertEquals(this.repository.getId(), clone.getId());
        assertEquals(this.repository.getGitLabId(), clone.getGitLabId());
    }
}
