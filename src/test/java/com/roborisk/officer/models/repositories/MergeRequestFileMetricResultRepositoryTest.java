package com.roborisk.officer.models.repositories;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestFile;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.metrics.MergeRequestFileMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Tests the {@link MergeRequestFileMetricResultRepository} class.
 */
public class MergeRequestFileMetricResultRepositoryTest extends OfficerTest {

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link MergeRequestReview} objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Database repository to handle {@link MergeRequestFile} objects.
     */
    @Autowired
    private MergeRequestFileRepository mergeRequestFileRepository;

    /**
     * Database repository to handle {@link MergeRequestFileMetricResult} objects.
     */
    @Autowired
    private MergeRequestFileMetricResultRepository mergeRequestFileMetricResultRepository;

    /**
     * Database repository to handle {@link Metric} objects.
     */
    @Autowired
    private MetricRepository metricRepository;

    /**
     * Test case to check whether the complicated query works.
     *
     * @throws ParseException   if commit generation errors.
     */
    @Test
    public void testGetByFilePathAndMergeRequestReviewId() throws ParseException {
        String filePath = "path/to/file";

        // Get all models needed.
        GitCommit commit = ModelFactory.getGitCommit();
        Repository repository = ModelFactory.getRepository();
        Branch source = ModelFactory.getBranch(repository, commit);
        Branch target = ModelFactory.getBranch(repository, commit);
        MergeRequest mergeRequest = ModelFactory.getMergeRequest(source, target);
        MergeRequestReview review = ModelFactory.getMergeRequestReview(commit, mergeRequest);
        MergeRequestFile file = ModelFactory.getMergeRequestFile(review);
        file.setFilePath(filePath);
        Metric metric = ModelFactory.getMetric();

        // Get a result
        MergeRequestFileMetricResult result = new MergeRequestFileMetricResult();
        result.setMetric(metric);
        result.setIsLowQuality(false);
        result.setMergeRequestFile(file);

        // Save everything in the db
        metric = this.metricRepository.save(metric);
        this.gitCommitRepository.save(commit);
        this.repositoryRepository.save(repository);
        this.branchRepository.save(source);
        this.branchRepository.save(target);
        this.mergeRequestRepository.save(mergeRequest);
        review = this.mergeRequestReviewRepository.save(review);
        this.mergeRequestFileRepository.save(file);
        this.mergeRequestFileMetricResultRepository.save(result);

        // Call the method
        Optional<MergeRequestFileMetricResult> optionalDbResult = this
                .mergeRequestFileMetricResultRepository
                .getByFilePathAndMergeRequestReviewAndMetricId(
                    filePath, review.getId(), metric.getId());

        // Analyse the result
        if (optionalDbResult.isEmpty()) {
            fail();
        } else {
            MergeRequestFileMetricResult dbResult = optionalDbResult.get();

            assertEquals(filePath, dbResult.getMergeRequestFile().getFilePath());
            assertEquals(review.getId(), dbResult.getMergeRequestFile().getMergeRequestReview().getId());
            assertEquals(metric.getId(), dbResult.getMetric().getId());
        }
    }

    /**
     * Tests the findAllNullScoreToZeroLinkedToReviewAndMetric function.
     *
     * @throws ParseException   When the date cannot be passed.
     */
    @Test
    public void testFindAllNullScoreToZeroLinkedToReviewAndMetric_OneResult() throws ParseException {
        String filePath = "path/to/file";

        // Get all models needed.
        GitCommit commit = ModelFactory.getGitCommit();
        Repository repository = ModelFactory.getRepository();
        Branch source = ModelFactory.getBranch(repository, commit);
        Branch target = ModelFactory.getBranch(repository, commit);
        MergeRequest mergeRequest = ModelFactory.getMergeRequest(source, target);
        MergeRequestReview review = ModelFactory.getMergeRequestReview(commit, mergeRequest);
        MergeRequestFile file = ModelFactory.getMergeRequestFile(review);
        file.setFilePath(filePath);
        Metric metric = ModelFactory.getMetric();

        // Get a result
        MergeRequestFileMetricResult result = new MergeRequestFileMetricResult();
        result.setMetric(metric);
        result.setIsLowQuality(false);
        result.setMergeRequestFile(file);
        result.setScore(null);

        // Save everything in the db
        this.metricRepository.save(metric);
        this.gitCommitRepository.save(commit);
        this.repositoryRepository.save(repository);
        this.branchRepository.save(source);
        this.branchRepository.save(target);
        this.mergeRequestRepository.save(mergeRequest);
        this.mergeRequestReviewRepository.save(review);
        this.mergeRequestFileRepository.save(file);
        this.mergeRequestFileMetricResultRepository.save(result);

        List<MergeRequestFileMetricResult> results = this.mergeRequestFileMetricResultRepository
                .findAllNullScoreLinkedToReviewAndMetric(
                    review.getId(),
                    metric.getId()
        );

        assertEquals(1, results.size());
    }
}
