package com.roborisk.officer.models.repositories;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.Optional;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;

/**
 * This class contains tests for the {@link BranchRepository} repository.
 */
public class BranchRepositoryTest extends OfficerTest {

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * {@link Repository} object for testing.
     */
    Repository repository;

    /**
     * {@link Branch} object for testing.
     */
    Branch branch;

    /**
     * Startup before each test.
     * Store the repository, branch and commit used in the rest of the file.
     *
     * @throws ParseException when the date cannot be parsed.
     */
    @BeforeEach
    public void startup() throws ParseException {
        GitCommit commit = ModelFactory.getGitCommit();
        this.gitCommitRepository.save(commit);

        this.repository = ModelFactory.getRepository();
        this.branch = ModelFactory.getBranch(repository, commit);

        this.repositoryRepository.save(this.repository);
        this.branchRepository.save(this.branch);
    }

    /**
     * Tests finding a branch by name and repository ID.
     */
    @Test
    public void testFindByNameAndRepositoryId() {
        Optional<Branch> optional = this.branchRepository.findByNameAndRepositoryId(
            this.branch.getName(),
            this.repository.getId()
        );

        if (optional.isEmpty()) {
            fail("Cannot find branch by name and gitlabID");
        }

        Branch clone = optional.get();

        assertEquals(this.branch.getId(), clone.getId());
        assertEquals(this.branch.getName(), clone.getName());
        assertEquals(this.branch.getRepository().getId(), clone.getRepository().getId());
    }
}
