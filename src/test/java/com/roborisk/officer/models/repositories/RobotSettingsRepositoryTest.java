package com.roborisk.officer.models.repositories;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.RobotSettings;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * This class contains tests for the {@link RobotSettingsRepository} repository.
 */
public class RobotSettingsRepositoryTest extends OfficerTest {

    /**
     * Database repository to handle {@link RobotSettings} objects.
     */
    @Autowired
    RobotSettingsRepository robotSettingsRepository;

    /**
     * Database repository to handle {@link RepositoryRepository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Test finding a {@link RobotSettings} by repository ID.
     */
    @Test
    public void testFindByRepositoryId() {
        Repository repository = ModelFactory.getRepository();
        this.repositoryRepository.save(repository);

        RobotSettings settings = ModelFactory.getRobotSettings(repository);
        this.robotSettingsRepository.save(settings);

        Optional<RobotSettings> optional = this.robotSettingsRepository.findByRepositoryId(settings.getRepository().getId());
        if (optional.isEmpty()) {
            fail("RobotSettings could not be found via Repository ID.");
        }

        RobotSettings clone = optional.get();
        assertEquals(settings.getId(), clone.getId());
        assertEquals(settings.getRepository().getId(), repository.getId());
    }

    /**
     * Test finding the default {@link RobotSettings}.
     */
    @Test
    public void testGetDefault() {
        Optional<RobotSettings> optional = this.robotSettingsRepository.getDefault();
        if (optional.isEmpty()) {
            fail("Default RobotSettings could not be found.");
        }

        RobotSettings clone = optional.get();
        assertNull(clone.getRepository());
    }
}
