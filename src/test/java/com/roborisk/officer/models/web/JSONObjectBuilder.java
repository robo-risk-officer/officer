package com.roborisk.officer.models.web;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * JSONObjectBuilder implements the basic JSON objects needed for testing.
 */
public class JSONObjectBuilder {

    /**
     * Constructs a list of random file names.
     *
     * @return A list of random file names.
     */
    private static List<String> getFiles() {
        List<String> files = new ArrayList<>();

        for (int i = 0; i < ThreadLocalRandom.current().nextInt(1, 10); i++) {
            files.add(ThreadLocalRandom.current().nextLong() + ".java");
        }

        return files;
    }

    /**
     * Constructs a project object.
     *
     * @return                  A JSON Object representing the project.
     * @throws JSONException    If the JSON cannot be constructed.
     */
    public static JSONObject getProject() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("id", 15);
        object.put("name", "GitLab Test");
        object.put("description", "GitLab Test Repository");
        object.put("web_url", "http://example.com/gitlabhq/gitlab-test");
        object.put("git_ssh_url", "git@example.com:gitlabhq/gitlab-test.git");
        object.put("git_http_url", "http://example.com/gitlabhq/gitlab-test.git");
        object.put("namespace", "GitLabHQ");
        object.put("visibility_level", 20);
        object.put("path_with_namespace", "gitlabhq/gitlab-test");
        object.put("default_branch", "master");
        object.put("homepage", "http://example.com/gitlabhq/gitlab-test");
        object.put("url", "http://example.com/gitlabhq/gitlab-test.git");
        object.put("ssh_url", "git@example.com:gitlabhq/gitlab-test.git");
        object.put("http_url", "http://example.com/gitlabhq/gitlab-test.git");

        return object;
    }

    /**
     * Constructs a repository object.
     *
     * @return                  A JSON Object representing the repository.
     * @throws JSONException    If the JSON cannot be constructed.
     */
    public static JSONObject getRepository() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("name", "GitLab Test");
        object.put("url", "http://example.com/gitlabhq/gitlab-test.git");
        object.put("description", "Testing");
        object.put("homepage", "http://example.com/gitlabhq/gitlab-test");
        object.put("git_http_url", "http://example.com/mike/diaspora.git");
        object.put("git_ssh_url", "git@example.com:mike/diaspora.git");
        object.put("visibility_level", 0);

        return object;
    }

    /**
     * Constructs a branch object.
     *
     * @return                  A JSON Object representing the branch.
     * @throws JSONException    If the JSON cannot be constructed.
     */
    public static JSONObject getBranch() throws JSONException {
        // The basics are in here
        JSONObject object = new JSONObject();

        object.put("name", "GitLab Test");
        object.put("description", "GitLab Test Repository");
        object.put("web_url", "http://example.com/gitlabhq/gitlab-test");
        object.put("git_ssh_url", "git@example.com:gitlabhq/gitlab-test.git");
        object.put("git_http_url", "http://example.com/gitlabhq/gitlab-test.git");
        object.put("namespace", "GitLabHQ");
        object.put("avatar_url", "https://example.com");
        object.put("visibility_level", 20);
        object.put("path_with_namespace", "gitlabhq/gitlab-test");
        object.put("default_branch", "master");
        object.put("homepage", "http://example.com/gitlabhq/gitlab-test");
        object.put("url", "http://example.com/gitlabhq/gitlab-test.git");
        object.put("ssh_url", "git@example.com:gitlabhq/gitlab-test.git");
        object.put("http_url", "http://example.com/gitlabhq/gitlab-test.git");

        return object;
    }

    /**
     * Constructs a author object.
     *
     * @return                  A JSON Object representing the author.
     * @throws JSONException    If the JSON cannot be constructed.
     */
    public static JSONObject getAuthor() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("name", "I am a human!");
        object.put("email", "space@world.com");

        return object;
    }

    /**
     * Constructs a commit object.
     *
     * @return                  A JSON Object representing the commit.
     * @throws JSONException    If the JSON cannot be constructed.
     */
    public static JSONObject getCommit() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("id", "da1560886d4f094c3e6c9ef40349f7d38b5d27d7");
        object.put("message", "commit message");
        object.put("timestamp", "2011-12-12T14:27:31+02:00");
        object.put("added", new JSONArray(JSONObjectBuilder.getFiles()));
        object.put("removed", new JSONArray(JSONObjectBuilder.getFiles()));
        object.put("modified", new JSONArray(JSONObjectBuilder.getFiles()));
        object.put("author", JSONObjectBuilder.getAuthor());

        return object;
    }

    /**
     * Constructs a user object.
     *
     * @return                  A JSON Object representing the user.
     * @throws JSONException    If the JSON cannot be constructed.
     */
    public static JSONObject getUser() throws JSONException {
        JSONObject object = JSONObjectBuilder.getAuthor();

        object.put("avatar_url", "https://example.com");

        return object;
    }

    /**
     * Constructs a merge request webhook object.
     *
     * @return                  A JSON Object representing the merge request.
     * @throws JSONException    If the JSON cannot be constructed.
     */
    public static JSONObject getMergeRequest() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("id", ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE));
        object.put("target_branch", "master");
        object.put("source_branch", "development");
        object.put("source_project_id", ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE));
        object.put("author_id", ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE));
        object.put("assignee_id", ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE));
        object.put("title", "I am gonna ruin master");
        object.put("created_at", "2013-12-03T17:15:43Z");
        object.put("updated_at", "2013-12-03T17:15:43Z");
        object.put("milestone_id", ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE));
        object.put("state", "opened");
        object.put("merge_status", "unchecked");
        object.put("target_project_id", ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE));
        object.put("iid", ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE));
        object.put("description", "Merge me!");
        object.put("source", JSONObjectBuilder.getBranch());
        object.put("target", JSONObjectBuilder.getBranch());
        object.put("last_commit", JSONObjectBuilder.getCommit());
        object.put("work_in_progress", ThreadLocalRandom.current().nextBoolean());
        object.put("url", "http://example.com/gitlabhq/merge_request/1");
        object.put("action", "open");
        object.put("assignee", JSONObjectBuilder.getUser());

        return object;
    }

    /**
     * Constructs a label object.
     *
     * @return                  A JSON Object representing the label.
     * @throws JSONException    If the JSON cannot be constructed.
     */
    public static JSONObject getLabel() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("id", ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE));
        object.put("title", "API");
        object.put("color", "#fff");
        object.put("project_id", ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE));
        object.put("created_at", "2013-12-03T17:15:43Z");
        object.put("updated_at", "2013-12-03T17:15:43Z");
        object.put("template", false);
        object.put("description", "API Related issues");
        object.put("type", "ProjectLabel");
        object.put("group_id", ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE));

        return object;
    }

    /**
     * Constructs a updated_by_id object.
     *
     * @return                  A JSON Object representing the updated_by_id.
     * @throws JSONException    If the JSON cannot be constructed.
     */
    public static JSONObject getUpdatedById() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("previous", null);
        object.put("current", ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE));

        return object;
    }

    public static JSONObject getUpdatedAt() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("previous", "2017-09-15 16:50:55 UTC");
        object.put("current", "2017-09-15 16:52:55 UTC");

        return object;
    }

    /**
     * Constructs a label changes object.
     *
     * @return                  A JSON Object representing the label changes.
     * @throws JSONException    If the JSON cannot be constructed.
     */
    public static JSONObject getLabelChanges() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("previous", new JSONArray(Collections.singletonList(JSONObjectBuilder.getLabel())));
        object.put("current", new JSONArray(Collections.singletonList(JSONObjectBuilder.getLabel())));

        return object;
    }

    /**
     * Constructs a changes object.
     *
     * @return                  A JSON Object representing the changes.
     * @throws JSONException    If the JSON cannot be constructed.
     */
    public static JSONObject getChanges() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("updated_by_id", JSONObjectBuilder.getUpdatedById());
        object.put("updated_at", JSONObjectBuilder.getUpdatedAt());
        object.put("labels", JSONObjectBuilder.getLabelChanges());

        return object;
    }

    /**
     * Constructs a merge request webhook object.
     *
     * @return                  A JSON Object representing the merge request webhook.
     * @throws JSONException    If the JSON cannot be constructed.
     */
    public static JSONObject getMergeRequestWebHook() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("object_kind", "merge_request");
        object.put("user", JSONObjectBuilder.getUser());
        object.put("project", JSONObjectBuilder.getProject());
        object.put("repository", JSONObjectBuilder.getRepository());
        object.put("object_attributes", JSONObjectBuilder.getMergeRequest());
        object.put("labels", new JSONArray(Collections.singletonList(JSONObjectBuilder.getLabel())));
        object.put("changes", JSONObjectBuilder.getChanges());

        return object;
    }

    /**
     * Constructs a json object from a file.
     *
     * @param filePath          The file to read the json object from.
     * @throws IOException      If the file at filePath cannot be read into a string.
     * @throws JSONException    If the file at filePath cannot be parsed into a JSON Object.
     * @return                  The JSON Object from the file at filePath.
     */
    public static JSONObject fromFile(String filePath) throws IOException, JSONException {
        InputStream fileInputStream = new FileInputStream(filePath);
        String jsonString = IOUtils.toString(fileInputStream);
        JSONObject object = new JSONObject(jsonString);

        return object;
    }

    /**
     * Constructs a push webhook object.
     *
     * @return                  A JSON Object representing the push webhook.
     * @throws JSONException    If the JSON cannot be constructed.
     */
    public static JSONObject getPushWebHook() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("object_kind", "push");
        object.put("before", "95790bf891e76fee5e1747ab589903a6a1f80f22");
        object.put("after", "da1560886d4f094c3e6c9ef40349f7d38b5d27d7");
        object.put("ref", "refs/heads/master");
        object.put("checkout_sha", "da1560886d4f094c3e6c9ef40349f7d38b5d27d7");
        object.put("user_id", 4);
        object.put("user_name", "John");
        object.put("user_username", "jsmith");
        object.put("user_email", "john@example.com");
        object.put("user_avatar", "https://http.cat/404.jpg");
        object.put("project_id", 15);
        object.put("project", JSONObjectBuilder.getProject());
        object.put("repository", JSONObjectBuilder.getRepository());

        List<JSONObject> commits = Collections.singletonList(JSONObjectBuilder.getCommit());
        object.put("commits", new JSONArray(commits));

        return object;
    }

    /**
     * Construct the position attribute of a comment webhook.
     *
     * @throws JSONException    When the {@link JSONObject} cannot be created.
     * @return                  The constructed position {@link JSONObject}.
     */
    public static JSONObject getPosition() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("base_sha", null);
        object.put("start_sha", null);
        object.put("head_sha", null);
        object.put("old_path", null);
        object.put("new_path", null);
        object.put("position_type", "text");
        object.put("old_line", null);
        object.put("new_line", null);
        object.put("line_range", null);

        return object;
    }

    /**
     * Construct the "object_attributes" of a comment webhook.
     *
     * @throws JSONException    When the {@link JSONObject} cannot be created.
     * @return                  The constructed "object_attributes" {@link JSONObject}.
     */
    public static JSONObject getObjectAttributesComment() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("attachment", null);
        object.put("author_id", 5);
        object.put("change_position", JSONObjectBuilder.getPosition());
        object.put("commit_id", null);
        object.put("created_at", "2020-06-16 10:53:23 UTC");
        object.put("discussion_id", "f626e3e1259508510521ae6945cf76875bad1d8d");
        object.put("id", 10);
        object.put("line_code", "f626e3e1259508510521ae6945cf76875bad1d8d");
        object.put("note", "@robo review pls");
        object.put("notable_id", 194);
        object.put("notable_type", "MergeRequest");
        object.put("original_position", JSONObjectBuilder.getPosition());
        object.put("position", JSONObjectBuilder.getPosition());
        object.put("project_id", 15);
        object.put("resolved_at", "2020-06-16 10:53:23 UTC");
        object.put("resolved_by_id", 9);
        object.put("resolved_by_push", false);
        object.put("st_diff", null);
        object.put("system", false);
        object.put("type", "DiffNote");
        object.put("updated_at", "2020-06-16 10:53:23 UTC");
        object.put("updated_by_id", null);
        object.put("description", "@robo review pls");

        return object;
    }

    /**
     * Constructs a comment webhook object.
     *
     * @throws JSONException    When the {@link JSONObject} cannot be constructed.
     * @return                  The constructed comment webhook object.
     */
    public static JSONObject getCommentWebHook() throws JSONException {
        JSONObject object = new JSONObject();

        object.put("object_kind", "note");
        object.put("event_type", "note");
        object.put("user", JSONObjectBuilder.getUser());
        object.put("project_id", 15);
        object.put("project", JSONObjectBuilder.getProject());
        object.put("object_attributes", JSONObjectBuilder.getObjectAttributesComment());
        object.put("repository", JSONObjectBuilder.getRepository());
        object.put("merge_request", JSONObjectBuilder.getMergeRequest());

        return object;
    }
}
