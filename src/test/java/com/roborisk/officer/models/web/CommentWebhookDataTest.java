package com.roborisk.officer.models.web;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.enums.EventType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Class providing tests for the {@link CommentWebhookData} class.
 */
public class CommentWebhookDataTest extends OfficerTest {

    /**
     * Test for checking that comments not requesting reviews are correctly handled.
     */
    @Test
    void getEventTypeNoneWrongComment() {
        MergeRequestWebhookData data = new MergeRequestWebhookData();
        RepositoryWeb repositoryWeb = new RepositoryWeb();
        data.setRepository(repositoryWeb);
        NoteWeb noteWeb = new NoteWeb();
        noteWeb.setNote("SOME NOTE");
        CommentWebhookData commentWebhookData = new CommentWebhookData(data, noteWeb);

        EventType result = commentWebhookData.getEventType();
        assertEquals(EventType.NONE, result);
    }

    /**
     * Test for checking that comments without a merge request set are correctly handled.
     */
    @Test
    void getEventTypeNoneNullRepository() {
        MergeRequestWebhookData data = new MergeRequestWebhookData();
        RepositoryWeb repositoryWeb = new RepositoryWeb();
        data.setRepository(repositoryWeb);
        NoteWeb noteWeb = new NoteWeb();
        noteWeb.setNote("@" + CommentWebhookData.ROBOT_NAME + " review");
        CommentWebhookData commentWebhookData = new CommentWebhookData(data, noteWeb);

        EventType result = commentWebhookData.getEventType();
        assertEquals(EventType.NONE, result);
    }
}
