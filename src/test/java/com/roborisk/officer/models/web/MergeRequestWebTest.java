package com.roborisk.officer.models.web;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the merge request web model.
 */
public class MergeRequestWebTest {

    /**
     * Tests the isEqualToMergeRequestWeb function.
     *
     * @throws ParseException   When the given date cannot be parsed to the given dateFormat.
     */
    @Test
    public void testEquals() throws ParseException {
        Repository repository = ModelFactory.getRepository();
        GitCommit commit = ModelFactory.getGitCommit();

        Branch branch = ModelFactory.getBranch(repository, commit);

        MergeRequest mergeRequest = ModelFactory.getMergeRequest(
            branch,
            branch,
            10,
            100
        );

        MergeRequestWeb web = mergeRequest.getWebObject();
        MergeRequestWeb other = mergeRequest.getWebObject();

        assertTrue(web.isEqualToMergeRequestWeb(other));

        other.setState("");
        assertFalse(web.isEqualToMergeRequestWeb(other));

        other.setTitle("");
        assertFalse(web.isEqualToMergeRequestWeb(other));

        other.setUpdatedAt(10);
        assertFalse(web.isEqualToMergeRequestWeb(other));

        other.setCreatedAt(10);
        assertFalse(web.isEqualToMergeRequestWeb(other));

        other.setSource("");
        assertFalse(web.isEqualToMergeRequestWeb(other));

        other.setTarget("");
        assertFalse(web.isEqualToMergeRequestWeb(other));

        other.setGitLabIid(-1);
        assertFalse(web.isEqualToMergeRequestWeb(other));

        other.setGitLabId(-1);
        assertFalse(web.isEqualToMergeRequestWeb(other));
    }
}
