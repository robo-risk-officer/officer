package com.roborisk.officer.models.web;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.enums.EventType;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Class providing tests for the {@link CommentWebhookData} class.
 */
public class MergeRequestWebhookDataTest extends OfficerTest {

    /**
     * Retrieve map representing JSON representing a merge request.
     * Only misses the 'state' and 'action' fields.
     *
     * @return {@link Map}<{@link String}, {@link Object}> representing the JSON.
     */
    private Map<String, Object> getPartialJson() {
        // Maps representing the JSON
        Map<String, Object> authormap = new LinkedHashMap<>();
        Map<String, Object> commitmap = new LinkedHashMap<>();
        Map<String, Object> map = new LinkedHashMap<>();

        // Add random data
        authormap.put("name", "name");
        authormap.put("email", "email");

        // Add more random data
        commitmap.put("id", "hashsishe");
        commitmap.put("message", "some message");
        commitmap.put("modified", new ArrayList<>());
        commitmap.put("removed", new ArrayList<>());
        commitmap.put("added", new ArrayList<>());
        commitmap.put("timestamp", "2015-04-08T21:00:25-07:00");
        commitmap.put("author", authormap);

        // Add more more random data
        map.put("id", 1);
        map.put("iid", 2);
        map.put("target_branch", "target");
        map.put("source_branch", "source");
        map.put("author_id", 4);
        map.put("title", "Some merge request");
        map.put("created_at", "2015-04-08T21:00:25-07:00");
        map.put("updated_at", "2015-04-08T21:00:25-07:00");
        map.put("merge_status", "can_be_merged");
        map.put("work_in_progress", false);
        map.put("last_commit", commitmap);

        return map;
    }

    /**
     * Test for checking that the correct EventType is returned if a merge request is created.
     */
    @Test
    void getEventTypeMRCreatedTest() throws IOException, ParseException {
        Map<String, Object> mergeRequest = this.getPartialJson();
        mergeRequest.put("state", "open");
        mergeRequest.put("action", "opened");

        MergeRequestWebhookData mergeRequestWebhookData = new MergeRequestWebhookData();
        mergeRequestWebhookData.setMergeRequest(mergeRequest);

        assertEquals(EventType.MERGE_REQUEST_CREATED, mergeRequestWebhookData.getEventType());
    }

    /**
     * Test for checking that the correct EventType is returned if a merge request is changed.
     */
    @Test
    void getEventTypeMRChangedTest() throws IOException, ParseException {
        Map<String, Object> mergeRequest = this.getPartialJson();
        mergeRequest.put("state", "random");
        mergeRequest.put("action", "opened");

        MergeRequestWebhookData mergeRequestWebhookData = new MergeRequestWebhookData();
        mergeRequestWebhookData.setMergeRequest(mergeRequest);

        assertEquals(EventType.MERGE_REQUEST_CHANGED, mergeRequestWebhookData.getEventType());
    }

    /**
     * Test for checking that the correct EventType is returned if a merge request is closed.
     */
    @Test
    void getEventTypeClosedTest() throws IOException, ParseException {
        Map<String, Object> mergeRequest = this.getPartialJson();
        mergeRequest.put("state", "closed");
        mergeRequest.put("action", "closed");

        MergeRequestWebhookData mergeRequestWebhookData = new MergeRequestWebhookData();
        mergeRequestWebhookData.setMergeRequest(mergeRequest);

        assertEquals(EventType.NONE, mergeRequestWebhookData.getEventType());
    }

    /**
     * Test for checking that the correct EventType is returned if a merge request is merged.
     */
    @Test
    void getEventTypeMergedTest() throws IOException, ParseException {
        Map<String, Object> mergeRequest = this.getPartialJson();
        mergeRequest.put("state", "merged");
        mergeRequest.put("action", "closed");

        MergeRequestWebhookData mergeRequestWebhookData = new MergeRequestWebhookData();
        mergeRequestWebhookData.setMergeRequest(mergeRequest);

        assertEquals(EventType.NONE, mergeRequestWebhookData.getEventType());
    }




}
