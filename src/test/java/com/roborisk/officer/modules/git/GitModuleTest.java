package com.roborisk.officer.modules.git;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * This class will implement relevant test cases to test the functionality of the GitModule class.
 */
@Disabled
public class GitModuleTest {

    // Settings that should be set in order to test the GitModule

    // HTTP Authentication related settings

    /**
     * HTTP testing repository.
     */
    private static final String httpRepositoryUri =
            "https://git.example.nl/robo-risk-officer-testing/git-module-test.git";

    /**
     * HTTP username for Git testing account.
     */
    private static final String httpUsername = System.getenv("RRO_HTTP_USERNAME");

    /**
     * HTTP password for Git testing account.
     */
    private static final String httpUserPassword = System.getenv("RRO_HTTP_PASSWORD");

    //SSH Authentication related settings

    /**
     * SSH testing repository.
     */
    private static final String sshRepositoryUri =
            "git@git.example.nl:robo-risk-officer-testing/git-module-test.git";

    /**
     * Authorized SSH key file path.
     */
    private static final String sshKeyFilePath = System.getenv("RRO_SSH_KEYFILEPATH");

    /**
     * Unauthorized SSH key file path.
     */
    private static final String sshKeyFilePathUnauthorised =
            System.getenv("RRO_SSH_KEYFILEPATH_UNAUTH");

    /**
     * Test case to check that null repository URIs are not accepted by the GitModule constructor.
     */
    @Test
    public void testConstructorNullUri() {
        try (GitModule gm = new GitModule(null)) {
            // Necessary for the try-with-resources idiom
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        } catch (Exception e) {
            assertTrue(e instanceof NullPointerException);
            assertTrue(e.getMessage().contains("repo uri cannot be null"));
        }
    }

    /**
     * Test case to check that empty repository URIs are not accepted by the GitModule constructor.
     */
    @Test
    public void testConstructorEmptyUri() {
        try (GitModule gm = new GitModule("")) {
            // Necessary for the try-with-resources idiom
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
            assertTrue(e.getMessage().contains("repo uri cannot be empty"));
        }
    }

    /**
     * Test case to check that HTTP authentication succeeds with provided credentials.
     */
    @Test
    public void testAuthHttp() {
        // First, wet up a GitModule with the set HTTP URI
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication and check it succeeds
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Because authentication succeeds, both the git and repository objects of gm should
            // not be null by the invariant
            assertNotNull(gm.getGit());
            assertNotNull(gm.getRepository());
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check that HTTP authentication fails for dummy credentials.
     */
    @Test
    public void testFailAuthHttp() {
        // First, wet up a GitModule with the set HTTP URI
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication and check it throws the correct exception
            try {
                gm.authHttp("dummy", "should not work");
            } catch (Exception e) {
                assertTrue(e instanceof IllegalStateException);
                assertTrue(e.getMessage().contains("not authorized"));
            }

            // Because authentication fails, both the git and repository objects of gm should be
            // null by the invariant
            assertNull(gm.getGit());
            assertNull(gm.getRepository());
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check that an exception is thrown for HTTP authentication on SSH URI.
     */
    @Test
    public void testExceptionHttpAuthOnSshUri() {
        try (GitModule gm = new GitModule(sshRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials to get the exception
            gm.authHttp(httpUsername, httpUserPassword);
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        } catch (Exception e) {
            assertTrue(e instanceof IllegalStateException);
            assertTrue(e.getMessage().contains("the repository does not have an HTTP URI"));
        }
    }

    /**
     * Test case to check that authentication cannot be called multiple times.
     */
    @Test
    public void testExceptionHttpAuthTwice() {
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Now, try to authenticate another time
            try {
                gm.authHttp(httpUsername, httpUserPassword);
            } catch (Exception e) {
                assertTrue(e instanceof IllegalStateException);
                assertTrue(e.getMessage().contains("git has already been initialised"));
            }
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check that SSH authentication succeeds with provided key file.
     */
    @Test
    public void testAuthSsh() {
        // First, wet up a GitModule with the set SSH URI
        try (GitModule gm = new GitModule(sshRepositoryUri)) {
            // Then, execute SSH authentication and check it succeeds
            assertTrue(gm.authSsh(sshKeyFilePath));

            // Because authentication succeeds, both the git and repository objects of gm should
            // not be null by the invariant
            assertNotNull(gm.getGit());
            assertNotNull(gm.getRepository());
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check that SSH authentication fails with invalid key file.
     */
    @Test
    public void testFailAuthSsh() {
        // First, wet up a GitModule with the set SSH URI
        try (GitModule gm = new GitModule(sshRepositoryUri)) {
            // Then, execute SSH authentication and check it throws the correct exception
            try {
                gm.authSsh(sshKeyFilePathUnauthorised);
            } catch (Exception e) {
                assertTrue(e instanceof IllegalStateException);
                assertTrue(e.getMessage().contains("Auth fail"));
            }

            // Because authentication fails, both the git and repository objects of gm should
            // be null by the invariant
            assertNull(gm.getGit());
            assertNull(gm.getRepository());
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check that an exception is thrown for SSH authentication on HTTP URI.
     */
    @Test
    public void testExceptionSshAuthOnHttpUri() {
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute SSH authentication with valid credentials to get the exception
            gm.authSsh(sshKeyFilePath);
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        } catch (Exception e) {
            assertTrue(e instanceof IllegalStateException);
            assertTrue(e.getMessage().contains("the repository does not have an SSH URI"));
        }
    }

    /**
     * Test case to check that authentication cannot be called multiple times.
     */
    @Test
    public void testExceptionSshAuthTwice() {
        try (GitModule gm = new GitModule(sshRepositoryUri)) {
            // Then, execute SSH authentication with valid credentials (should not fail)
            assertTrue(gm.authSsh(sshKeyFilePath));

            // Now, try to authenticate another time
            try {
                gm.authSsh(sshKeyFilePath);
            } catch (Exception e) {
                assertTrue(e instanceof IllegalStateException);
                assertTrue(e.getMessage().contains("git has already been initialised"));
            }
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check that the diff command fails for two non-existent commits.
     */
    @Test
    public void testDiffFailsNonExistentCommits() {
        // First, create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Finally, do the diff command for two random SHA1 commit hashes that should
            // not exist and check the diff command throws the correct exception
            try {
                gm.diff("964d9257c8ad13576e7ffba160fd704eb0996a4a",
                    "E061A16B9A92B0F1B5EC88023BA16FDB2F92CE84", false);
            } catch (Exception e) {
                assertTrue(e instanceof IOException);
                assertTrue(e.getMessage()
                        .contains("Missing unknown 964d9257c8ad13576e7ffba160fd704eb0996a4a"));
            }
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check that the diff command succeeds for two existing commits.
     */
    @Test
    public void testDiffSucceedsForExistentCommits() {
        // First, create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Finally, do the diff command for two existent SHA1 commit hashes
            // and check the diff command returns a non-null response indicating success
            assertNotNull(gm.diff("fe312278f8e8948edc65b802b18ee951ad1fa736",
                "ccfbab5a16492d961f90f19286fd18765020f4a0", false));
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check that the diff command fails when a branch does not exist.
     */
    @Test
    public void testDiffFailsForNonexistentBranches() {
        // First, create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Finally, do the diff command for one random branch name that should
            // not exist and check the diff command throws the correct exception
            try {
                gm.diff("master","nonExistentBranch", true);
            } catch (Exception e) {
                assertTrue(e instanceof IllegalStateException);
                assertTrue(e.getMessage().contains("checkout of nonExistentBranch failed"));
            }
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check that the diff command succeeds for two existing branches.
     */
    @Test
    public void testDiffSucceedsForExistentBranches() {
        // First, create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Finally, do the diff command for two existent branch names
            // and check the diff command returns a non-null response indicating success
            assertNotNull(gm.diff("master",
                "extraBranch", true));
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check the changes command fails for a non-existent commit.
     */
    @Test
    public void testChangesFailsForNonexistentCommit() {
        // First, create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Finally, do the changes command for a random SHA1 commit hash that should
            // not exist and check the changes command throws the correct exception
            try {
                gm.changes("964d9257c8ad13576e7ffba160fd704eb0996a4a");
            } catch (Exception e) {
                assertTrue(e instanceof IOException);
                assertTrue(e.getMessage()
                        .contains("Missing unknown 964d9257c8ad13576e7ffba160fd704eb0996a4a"));
            }
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check the changes command succeeds for the first commit in a repository.
     */
    @Test
    public void testChangesSucceedsForFirstCommit() {
        // First, create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Finally, do the changes command for the first commit of the test repository
            // and check the changes command returns a non-null response indicating success
            assertNotNull(gm.changes("fe312278f8e8948edc65b802b18ee951ad1fa736"));
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check the changes command succeeds for a commit not on master of a repository.
     */
    @Test
    public void testChangesSucceedsForOtherBranchCommit() {
        // First, create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Finally, do the changes command for the first commit of the test repository
            // and check the changes command returns a non-null response indicating success
            assertNotNull(gm.changes("fb7af0fc7fdf428e3344383c2255d3626191fcdf"));
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check the fetch command executes normally without specified remote.
     */
    @Test
    public void testFetchExecutesWithoutSpec() {
        // First, create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Finally, check that fetch does not throw an exception without specified remote
            // indicating success
            try {
                gm.fetch(null);
            } catch (Exception e) {
                fail("Method should not have thrown an exception");
            }
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check the fetch command executes normally with specified remote.
     */
    @Test
    public void testFetchExecutesWithSpec() {
        // First, create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Finally, check that fetch does not throw an exception with specified remote
            // indicating success
            try {
                gm.fetch("origin");
            } catch (Exception e) {
                fail("Method should not have thrown an exception");
            }
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check the fetch command fails gracefully with specified non-existent remote.
     */
    @Test
    public void testFetchGracefulFail() {
        // First, create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Finally, check fetch throws the correct exception with non-existent remote
             try {
                 gm.fetch("nonexistentremote");
             } catch (Exception e) {
                 assertTrue(e instanceof IllegalStateException);
                 assertTrue(e.getMessage().contains("Invalid remote: nonexistentremote"));
             }
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check pull works with the default state after initializing the Git Module.
     */
    @Test
    public void testPullDefaultStateSuccess() {
        // First create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Finally, check pull does not throw an exception in the default state,
            // indicating success
            try {
                gm.pull(null, null);
            } catch (Exception e) {
                fail("Method should not have thrown an exception");
            }
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check pull works with specified remote and unspecified branchName.
     */
    @Test
    public void testPullExistentRemoteDefaultBranchSuccess() {
        // First create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Finally, check pull does not throw an exception with an existent remote
            // indicating success
            try {
                gm.pull("origin", null);
            } catch (Exception e) {
                fail("Method should not have thrown an exception");
            }
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check pull works with unspecified remote and specified branchName.
     */
    @Test
    public void testPullDefaultRemoteExistentBranchSuccess() {
        // First create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Finally, check pull does not throw an exception for an adapted branch,
            // indicating success
            try {
                gm.pull(null, "extraBranch");
            } catch (Exception e) {
                fail("Method should not have thrown an exception");
            }
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check pull works with specified remote and specified branchName.
     */
    @Test
    public void testPullSpecifiedRemoteExistentBranchSuccess() {
        // First create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Check pull does not throw an exception for specified remote and adapted branch
            // indicating success
            try {
                gm.pull("origin", "extraBranch");
            } catch (Exception e) {
                fail("Method should not have thrown an exception");
            }
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check pull fails gracefully with non-existent remote (but existent branchName).
     */
    @Test
    public void testPullNonexistentRemoteFail() {
        // First create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Finally, check pull throws the right exception for a non-existent remote
            try {
                gm.pull("fictitiousRemote", "extraBranch");
            } catch (Exception e) {
                assertTrue(e instanceof IllegalStateException);
                assertTrue(e.getMessage().contains("No value for key remote.fictitiousRemote.url"));
            }
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }

    /**
     * Test case to check pull fails gracefully with existent remote but non-existent branchName.
     */
    @Test
    public void testPullNonexistentBranchFail() {
        // First create a GitModule instance
        try (GitModule gm = new GitModule(httpRepositoryUri)) {
            // Then, execute HTTP authentication with valid credentials (should not fail)
            assertTrue(gm.authHttp(httpUsername, httpUserPassword));

            // Finally, check pull throws the right exception for a non-existent branch
            try {
                gm.pull("origin", "fictitousBranch");
            } catch (Exception e) {
                assertTrue(e instanceof IllegalStateException);
                assertTrue(e.getMessage().contains(
                        "Remote origin did not advertise Ref for branch fictitousBranch."));
            }
        } catch (IOException e) {
            // If an IO exception is thrown, something went wrong with removing the working dir
            fail("Could not clean up working directory:\n\n" + e.getMessage());
        }
    }
}
