package com.roborisk.officer.modules.sonar.commands;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.modules.git.GitModule;
import com.roborisk.officer.modules.metrics.sonar.SonarWebHelper;
import com.roborisk.officer.modules.metrics.sonar.commands.SonarJavaCommand;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Test {@link SonarJavaCommand} output.
 */
public class SonarJavaCommandTest extends OfficerTest {

    /**
     * The project name.
     */
    private static final String PROJECT_NAME = "Hello World";

    /**
     * The command string to expect.
     */
    private static final String COMMAND_STRUCTURE = "mvn compile; mvn sonar:sonar -Dsonar.projectKey=%s -Dsonar.host.url=null -Dsonar.login=null";

    /**
     * The fake repository URL.
     */
    private static final String REPOSITORY_URL = "git@git.example.com:test.git";

    /**
     * Helper for SonarQube.
     */
    @Autowired
    private SonarWebHelper webHelper;

    /**
     * Test the getMavenCommand function from the {@link SonarJavaCommand}.
     *
     * @throws IOException                  When the GitModule cannot be initialized.
     */
    @Test
    public void testGetMavenCommand() throws IOException {
        GitModule module = new GitModule(REPOSITORY_URL);

        Path path = Paths.get(module.getWorkingDirectory().getPath(), "src");
        File file = new File(path.toString());
        file.mkdir();

        file = new File(Paths.get(file.getPath(), "pom.xml").toString());
        file.createNewFile();

        SonarJavaCommand command = new SonarJavaCommand(this.webHelper, module);
        assertEquals(
                String.format(
                        SonarJavaCommandTest.COMMAND_STRUCTURE,
                        SonarJavaCommandTest.PROJECT_NAME
                ),
                command.getCommand(SonarJavaCommandTest.PROJECT_NAME)
        );

        module.close();
    }

    /**
     * Tests the failure when the SonarJavaCommand is created on a non-maven repository.
     *
     * @throws IOException  When the GitModule cannot be initialized.
     */
    @Test
    public void testCommandFailureWhenNonMavenProject() throws IOException {
        GitModule module = new GitModule(REPOSITORY_URL);

        Path path = Paths.get(module.getWorkingDirectory().getPath(), "src");
        File file = new File(path.toString());
        file.mkdir();

        assertThrows(IllegalStateException.class, () -> new SonarJavaCommand(this.webHelper, module));

        module.close();
    }

    /**
     * Tests the functionality of getCommand when buildType is NONE.
     *
     * @throws IOException              When the file cannot be created.
     * @throws NoSuchFieldException     When no such field exists.
     * @throws IllegalAccessException   When the method is private.
     */
    @Test
    public void testFailureNonMavenInGetCommand() throws IOException, NoSuchFieldException, IllegalAccessException {
        GitModule module = new GitModule(REPOSITORY_URL);

        Path path = Paths.get(module.getWorkingDirectory().getPath(), "pom.xml");
        File file = new File(path.toString());
        file.createNewFile();

        SonarJavaCommand command = new SonarJavaCommand(this.webHelper, module);

        // Make the field non-private.
        Field field = command.getClass().getDeclaredField("buildType");
        field.setAccessible(true);

        // Remove the final modifier from the class.
        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        // Set the enum to NONE.
        field.set(command, Enum.valueOf((Class<Enum>) field.getType(), "NONE"));

        assertEquals(null, command.getCommand(SonarJavaCommandTest.PROJECT_NAME));

        module.close();
    }
}
