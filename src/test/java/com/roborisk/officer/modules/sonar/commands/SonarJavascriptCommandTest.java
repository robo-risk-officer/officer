package com.roborisk.officer.modules.sonar.commands;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.modules.metrics.sonar.SonarWebHelper;
import com.roborisk.officer.modules.metrics.sonar.commands.SonarJavascriptCommand;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Test {@link SonarJavascriptCommand} output.
 */
public class SonarJavascriptCommandTest extends OfficerTest {

    /**
     * The project name.
     */
    private static final String PROJECT_NAME = "Hello World";

    /**
     * The command string to expect.
     */
    private static final String COMMAND_STRUCTURE = "sonar-scanner -Dsonar.projectKey=%s -Dsonar.sources=. -Dsonar.host.url=null -Dsonar.login=null";

    /**
     * Helper for SonarQube.
     */
    @Autowired
    private SonarWebHelper webHelper;

    /**
     * Test the getCommand function from the {@link SonarJavascriptCommand}.
     */
    @Test
    public void testGetCommand() {
        String original = System.getProperty("os.name");

        System.setProperty("os.name", "Decent Operating System :)");

        SonarJavascriptCommand command = new SonarJavascriptCommand(this.webHelper);
        assertEquals(
                String.format(
                        SonarJavascriptCommandTest.COMMAND_STRUCTURE,
                        SonarJavascriptCommandTest.PROJECT_NAME
                ),
                command.getCommand(SonarJavascriptCommandTest.PROJECT_NAME)
        );

        System.setProperty("os.name", original);
    }

    /**
     * Tests the failure when it is executed on a windows machine.
     */
    @Test
    public void testFailureOnWindows() {
        String original = System.getProperty("os.name");

        System.setProperty("os.name", "Windows");

        assertThrows(IllegalStateException.class, () -> {
            new SonarJavascriptCommand(this.webHelper);
        });

        System.setProperty("os.name", original);
    }
}
