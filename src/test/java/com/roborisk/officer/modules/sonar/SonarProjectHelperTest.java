package com.roborisk.officer.modules.sonar;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.repositories.SonarAnalysisTrackerRepository;
import com.roborisk.officer.modules.git.GitModule;
import com.roborisk.officer.modules.git.GitModuleWrapper;
import com.roborisk.officer.modules.metrics.sonar.SonarProjectHelper;
import com.roborisk.officer.modules.metrics.sonar.SonarWebHelper;
import com.roborisk.officer.modules.metrics.sonar.commands.SonarCommand;
import com.roborisk.officer.modules.metrics.sonar.commands.SonarJavaCommand;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * This test class tests the testable functions of the SonarProjectHelper. Tests are disabled as
 * they pull projects from the live GitLab server making them heavy to run and some of them require
 * a live instance of SonarQube to run.
 */
@Disabled
@SuppressWarnings({"ClassFanOutComplexity"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SonarProjectHelperTest {

    /**
     * The GitModule wrapper to get authenticated GitModule instances.
     */
    @Autowired
    private GitModuleWrapper gitModuleWrapper;

    /**
     * The SonarProjectHelper that is tested in this file.
     */
    @Autowired
    private SonarProjectHelper sonarProjectHelper;

    /**
     * Instance of SonarWebHelper to get variables from the web helper.
     */
    @Autowired
    private SonarWebHelper sonarWebHelper;

    @Autowired
    private SonarAnalysisTrackerRepository sonarAnalysisTrackerRepository;

    /**
     * MergeRequestReviewRepository for database interaction.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Branch repository for database interaction.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Git commit repository for database interaction.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Merge request repository for database interaction.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Repository repository for database interaction.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Test to gather the commands for a Maven project.
     *
     * @throws IOException  if the {@link GitModule} errors upon initialising or closing.
     */
    @Test
    public void testGetJavaCommands() throws IOException {
        GitModule gitModule = gitModuleWrapper.getGitModule(
                "https://git.example.nl/robo-risk-officer/officer.git");

        String sourceProjectName = "sourceBranch";
        String targetProjectName = "targetBranch";

        SonarCommand command = new SonarJavaCommand(sonarWebHelper, gitModule);

        assertEquals("mvn compile; mvn sonar:sonar -Dsonar.projectKey=" + sourceProjectName
                + " -Dsonar.host.url=" + sonarWebHelper.getSonarBaseUrl() + " -Dsonar.login="
                + sonarWebHelper.getSonarAccessToken(), command.getCommand(sourceProjectName));

        assertEquals("mvn compile; mvn sonar:sonar -Dsonar.projectKey=" + targetProjectName
                + " -Dsonar.host.url=" + sonarWebHelper.getSonarBaseUrl() + " -Dsonar.login="
                + sonarWebHelper.getSonarAccessToken(), command.getCommand(targetProjectName));

        gitModule.close();
    }

    /**
     * Tests whether the SonarProjectHelper.initialiseTracker function works correctly.
     *
     * @throws IOException  When the GitModule errors on initialisation or closing.
     */
    @Test
    public void testInitialiseTracker() throws IOException {
        // Initialise a merge request review
        // Intialise all necessary objects.
        MergeRequestReview mergeRequestReview = new MergeRequestReview();
        MergeRequest mergeRequest = new MergeRequest();
        Branch source = new Branch();
        Branch target = new Branch();
        Repository repo = new Repository();
        GitCommit commit = new GitCommit();

        // Set attributes of repo
        repo.setGitLabId(161);
        repo.setSshUrl("git@git.example.nl:robo-risk-officer/officer.git");
        repo.setHttpUrl("https://git.example.nl/robo-risk-officer/officer.git");
        repo.setName("Officer");
        repo.setNamespace("namespace");
        repo.setIsEnabled(true);

        // Set attributes of commit
        commit.setHash("latestCommit");
        commit.setTimestamp(1234567);

        // Set attributes of source and target branch
        source.setRepository(repo);
        source.setLatestCommit(commit);
        source.setName("T11.16-The-Second-Metric");
        target.setRepository(repo);
        target.setLatestCommit(commit);
        target.setName("master");

        // Set attributes of mergeRequest.
        mergeRequest.setSourceBranch(source);
        mergeRequest.setMergeStatus("happy");
        mergeRequest.setTargetBranch(target);
        mergeRequest.setGitLabId(68);
        mergeRequest.setGitLabIid(68);
        mergeRequest.setTitle("The-Second-Metric");
        mergeRequest.setCreatedAt(12345);
        mergeRequest.setUpdatedAt(12345);
        mergeRequest.setState("happy");

        // Set attributes of mergeRequestReview.
        mergeRequestReview.setMergeRequest(mergeRequest);
        mergeRequestReview.setCommit(commit);

        // Save all instances in the right order in the database.
        this.gitCommitRepository.save(commit);
        this.repositoryRepository.save(repo);
        this.branchRepository.save(source);
        this.branchRepository.save(target);
        this.mergeRequestRepository.save(mergeRequest);
        this.mergeRequestReviewRepository.save(mergeRequestReview);

        // Initialise GitModule.
        GitModule gitModule = gitModuleWrapper.getGitModule(
            "https://git.example.nl/robo-risk-officer/officer.git");

        // Check if tracker is not in database.
        assertTrue(this.sonarAnalysisTrackerRepository.findByName(
                gitModule.getWorkingDirectory().getName()).isEmpty());

        // Initialise tracker.
        sonarProjectHelper.initialiseTracker(gitModule, mergeRequestReview, true, "test :)");

        // Check if tracker is in database.
        assertTrue(this.sonarAnalysisTrackerRepository.findByName(
                gitModule.getWorkingDirectory().getName()).isPresent());

        // Close the GitModule.
        gitModule.close();
    }

    /**
     * Tests 3 functions sequentially, namely createSonarQubeProject, setupWebhook and
     * deleteSonarQubeProject.
     */
    public void testCreateSonarQubeProjectSetupWebhookAndDeleteSonarQubeProject() {
        // Initialise name of project.
        String projectName = "testProject";

        // Create project in sonar.
        sonarProjectHelper.createSonarQubeProject(projectName);

        // Initialise url to get project from SonarQube API.
        String getProject = "/api/projects/search?projects=testProject";

        // Get list of projects named testProject from Sonar.
        JsonNode project = sonarWebHelper.exchangeRequest(
                getProject, HttpMethod.GET, JsonNode.class).getBody();

        // Check if there is exactly 1.
        assertEquals(1, project.at("/paging/total").asInt());

        // Setup the webhook.
        sonarProjectHelper.setupWebhook(projectName);

        // Initialise url to get all webhooks of project testProject.
        String webhookUrl = "/api/webhooks/list?project=testProject";

        // Get list of webhooks of project testProject.
        JsonNode webhook = sonarWebHelper.exchangeRequest(webhookUrl, HttpMethod.GET,
                JsonNode.class).getBody();

        // Check if there is exactly 1.
        assertEquals(1, ((ArrayNode) webhook.get("webhooks")).size());

        // Delete project in Sonar.
        sonarProjectHelper.deleteSonarQubeProject(projectName);

        // Get list of projects named testProject from Sonar.
        JsonNode noProject = sonarWebHelper.exchangeRequest(getProject, HttpMethod.GET,
                JsonNode.class).getBody();

        // Check if there is 0.
        assertEquals(0, noProject.at("/paging/total").asInt());
    }
}
