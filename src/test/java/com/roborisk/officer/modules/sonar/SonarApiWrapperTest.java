package com.roborisk.officer.modules.sonar;

import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MergeRequest;
import com.roborisk.officer.models.database.MergeRequestReview;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MergeRequestRepository;
import com.roborisk.officer.models.repositories.MergeRequestReviewRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.models.web.SonarIssueWeb;
import com.roborisk.officer.modules.metrics.sonar.SonarApiWrapper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;

/**
 * This class contains tests for the {@link SonarApiWrapper}.
 * This test class is disabled as the tests are not meant for automated testing but rather in case
 * it is needed to run through each of the functions on a step by step basis.
 */
@Disabled
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SonarApiWrapperTest {

    /**
     * {@link SonarApiWrapper} instance to use for testing.
     */
    @Autowired
    public SonarApiWrapper sonarApiWrapper;

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Database repository to handle {@link Branch} objects.
     */
    @Autowired
    private BranchRepository branchRepository;

    /**
     * Database repository to handle {@link Repository} objects.
     */
    @Autowired
    private RepositoryRepository repositoryRepository;

    /**
     * Database repository to handle {@link MergeRequest} objects.
     */
    @Autowired
    private MergeRequestRepository mergeRequestRepository;

    /**
     * Database repository to handle {@link MergeRequestReview} objects.
     */
    @Autowired
    private MergeRequestReviewRepository mergeRequestReviewRepository;

    /**
     * Test gathers all the unresolved vulnerabilities, sorted on file line, from
     * a project in SonarQube called 'officer'. This tests requires the tester to already have a
     * project in a running instance of SonarQube which is called 'officer'.
     * This test case is not meant to be done automatically but to serve as a ready to go test case
     * in case it is needed to go through the getting of unresolved issues on a step by step basis.
     */
    @Test
    public void testGetUnresolvedIssues() {
        HashMap<String, String> params = new HashMap<>();

        params.put("componentKeys", "officer");
        params.put("s", "FILE_LINE"); // Sort on file line
        params.put("resolved", "false");
        params.put("types", "VULNERABILITY");
        params.put("ps", "100");

        List<SonarIssueWeb> issues = this.sonarApiWrapper.getUnresolvedIssues(params);
    }

    /**
     * Test initialises many different objects and then fires the initialiseProjectForAnalysis
     * function. This test case is not meant to be done automatically but to serve as a ready
     * to go test case in case it is needed to go through the initialisation on a step by step
     * basis.
     */
    @Test
    public void testInitialiseProjectForAnalysis() {
        // Intialise all necessary objects.
        MergeRequestReview mrr = new MergeRequestReview();
        MergeRequest mr = new MergeRequest();
        Branch sourceBranch = new Branch();
        Branch targetBranch = new Branch();
        Repository repository = new Repository();
        GitCommit latestCommit = new GitCommit();

        // Set attributes of repository
        repository.setGitLabId(161);
        repository.setSshUrl("git@git.example.nl:robo-risk-officer/officer.git");
        repository.setHttpUrl("https://git.example.nl/robo-risk-officer/officer.git");
        repository.setName("Officer");
        repository.setNamespace("namespace");
        repository.setIsEnabled(true);

        // Set attributes of latestCommit
        latestCommit.setHash("latestCommit");
        latestCommit.setTimestamp(1234567);

        // Set attributes of source and target branch
        sourceBranch.setRepository(repository);
        sourceBranch.setLatestCommit(latestCommit);
        sourceBranch.setName("T11.16-The-Second-Metric");
        targetBranch.setRepository(repository);
        targetBranch.setLatestCommit(latestCommit);
        targetBranch.setName("master");

        // Set attributes of mr.
        mr.setSourceBranch(sourceBranch);
        mr.setMergeStatus("happy");
        mr.setTargetBranch(targetBranch);
        mr.setGitLabId(68);
        mr.setGitLabIid(68);
        mr.setTitle("The-Second-Metric");
        mr.setCreatedAt(12345);
        mr.setUpdatedAt(12345);
        mr.setState("happy");

        // Set attributes or mrr.
        mrr.setMergeRequest(mr);
        mrr.setCommit(latestCommit);

        // Save all instances in the right order in the database.
        this.gitCommitRepository.save(latestCommit);
        this.repositoryRepository.save(repository);
        this.branchRepository.save(sourceBranch);
        this.branchRepository.save(targetBranch);
        this.mergeRequestRepository.save(mr);
        this.mergeRequestReviewRepository.save(mrr);

        // Initialise the projects
        try {
            this.sonarApiWrapper.initialiseProjectForAnalysis(mrr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
