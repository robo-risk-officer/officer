package com.roborisk.officer.modules.codechurn;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.GitCommitWeb;
import com.roborisk.officer.modules.metrics.codechurn.FileCodeChurnHelper;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the file churn metric helper class.
 */
public class FileCodeChurnHelperTest extends OfficerTest {

    /**
     * Data used to test the file churn metric helper.
     */
    private static final CommitWebhookData data = new CommitWebhookData();

    /**
     * Database repository to handle {@link GitCommit} objects.
     */
    @Autowired
    private GitCommitRepository gitCommitRepository;

    /**
     * Helper class to get lists of added/modified/deleted {@link BranchFile} objects.
     */
    @Autowired
    private FileCodeChurnHelper fileCodeChurnHelper;

    /**
     * Setter for initializing the data for the tests.
     */
    @BeforeEach
    public void setData() throws ParseException {
        List<GitCommitWeb> commits = new ArrayList<>();

        // initialize 2 GitCommitWeb objects
        GitCommitWeb commit1 = new GitCommitWeb();
        GitCommitWeb commit2 = new GitCommitWeb();

        // set properties of commit1
        commit1.setAdded(Collections.singletonList("CHANGELOG"));
        commit1.setModified(Collections.singletonList("app/controller/application.rb"));
        commit1.setRemoved(Collections.emptyList());
        commit1.setHash("First");
        commit1.setMessage("Hallooo");
        commit1.setTimestamp("2001-02-08T21:24:15-07:00");

        // store a version of commit1 in the database
        GitCommit gitCommit1 = new GitCommit(commit1);
        this.gitCommitRepository.save(gitCommit1);

        // set properties of commit2
        commit2.setAdded(Arrays.asList("src/main/rro.java", "src/main/application.java"));
        commit2.setModified(Arrays.asList("src/something/something.smth", "this/could/be/a.file"));
        commit2.setRemoved(Collections.emptyList());
        commit2.setHash("Second");
        commit2.setMessage("Doeiii");
        commit2.setTimestamp("2001-02-08T21:24:15-08:00");

        // store a version of commit2 in the database
        GitCommit gitCommit2 = new GitCommit(commit2);
        this.gitCommitRepository.save(gitCommit2);

        // add commit1 and commit2 to commits
        commits.add(commit1);
        commits.add(commit2);

        // add commits to data
        data.setCommits(commits);
    }

    /**
     * Function that tests the getAddedFiles function.
     */
    @Test
    public void getAddedFilesTest() {
        List<Pair<GitCommit, List<BranchFile>>> addedFiles = this.fileCodeChurnHelper.getAddedFiles(data);

        AtomicInteger size = new AtomicInteger();
        addedFiles.forEach(pair -> size.addAndGet(pair.getRight().size()));

        assertEquals(3, size.get());

        Optional<Pair<GitCommit, List<BranchFile>>> firstTuple = addedFiles.stream().filter(pair -> pair.getLeft().getHash().equals("First")).findFirst();
        assertTrue(firstTuple.isPresent());
        assertEquals("CHANGELOG", firstTuple.get().getRight().get(0).getFileName());
        assertEquals("CHANGELOG", firstTuple.get().getRight().get(0).getFilePath());

        Optional<Pair<GitCommit, List<BranchFile>>> secondTuple = addedFiles.stream().filter(pair -> pair.getLeft().getHash().equals("Second")).findFirst();
        assertTrue(secondTuple.isPresent());
        assertEquals("rro.java", secondTuple.get().getRight().get(0).getFileName());
        assertEquals("src/main/rro.java", secondTuple.get().getRight().get(0).getFilePath());
        assertEquals("application.java", secondTuple.get().getRight().get(1).getFileName());
        assertEquals("src/main/application.java", secondTuple.get().getRight().get(1).getFilePath());
    }

    /**
     * Function that tests the getModifiedFiles function.
     */
    @Test
    public void getModifiedFilesTest() {
        List<Pair<GitCommit, List<BranchFile>>> modifiedFiles = this.fileCodeChurnHelper.getModifiedFiles(data);

        AtomicInteger size = new AtomicInteger();
        modifiedFiles.forEach(pair -> size.addAndGet(pair.getRight().size()));

        assertEquals(3, size.get());

        Optional<Pair<GitCommit, List<BranchFile>>> firstTuple = modifiedFiles.stream().filter(pair -> pair.getLeft().getHash().equals("First")).findFirst();
        assertTrue(firstTuple.isPresent());
        assertEquals("application.rb", firstTuple.get().getRight().get(0).getFileName());
        assertEquals("app/controller/application.rb", firstTuple.get().getRight().get(0).getFilePath());

        Optional<Pair<GitCommit, List<BranchFile>>> secondTuple = modifiedFiles.stream().filter(pair -> pair.getLeft().getHash().equals("Second")).findFirst();
        assertTrue(secondTuple.isPresent());
        assertEquals("something.smth", secondTuple.get().getRight().get(0).getFileName());
        assertEquals("src/something/something.smth", secondTuple.get().getRight().get(0).getFilePath());
        assertEquals("a.file", secondTuple.get().getRight().get(1).getFileName());
        assertEquals("this/could/be/a.file", secondTuple.get().getRight().get(1).getFilePath());
    }

    /**
     * Function that tests the getRemovedFiles function.
     */
    @Test
    public void getRemovedFilesTest() {

        List<Pair<GitCommit, List<BranchFile>>> removedFiles = this.fileCodeChurnHelper.getRemovedFiles(data);

        AtomicInteger size = new AtomicInteger();
        removedFiles.forEach(pair -> size.addAndGet(pair.getRight().size()));

        assertEquals(0, size.get());
    }
}
