package com.roborisk.officer.modules.codechurn;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.BranchClassObservation;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.metrics.BranchClassMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.BranchClassMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchClassObservationRepository;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MetricRepository;
import com.roborisk.officer.models.repositories.MetricSettingsRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.modules.metrics.codechurn.ClassCodeChurnComputer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Class for testing the {@link ClassCodeChurnComputer} class.
 */
public class ClassCodeChurnComputerTest extends OfficerTest {

    /**
     * Repository interface for {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Repository interface for {@link Branch} objects.
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Repository interface for {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * Repository interface for {@link BranchFile} objects.
     */
    @Autowired
    BranchFileRepository branchFileRepository;

    /**
     * Repository interface for {@link Metric} objects.
     */
    @Autowired
    MetricRepository metricRepository;

    /**
     * Repository interface for {@link MetricSettings} objects.
     */
    @Autowired
    MetricSettingsRepository metricSettingsRepository;

    /**
     * Repository interface for {@link BranchClassObservation} objects.
     */
    @Autowired
    BranchClassObservationRepository branchClassObservationRepository;

    /**
     * Repository interface for {@link BranchClassMetricResult} objects.
     */
    @Autowired
    BranchClassMetricResultRepository branchClassMetricResultRepository;

    /**
     * {@link ClassCodeChurnComputer} used for testing.
     */
    @Autowired
    ClassCodeChurnComputer classCodeChurnComputer;

    /**
     * Test for all functionality in the {@link ClassCodeChurnComputer} class.
     *
     * @throws ParseException if database entries cannot be parsed.
     */
    @Test
    void analyzeFileInCommitTest() throws ParseException, com.github.javaparser.ParseException {
        // Construct String objects mocking the contents of files.
        String fileOld = "public class Unchanged {\n" +
            "    void helloWorld() {\n" +
            "        System.out.println(\"HELLO WORLD\");\n" +
            "    }\n" +
            "}\n" +
            "\n" +
            "public class Changed {\n" +
            "    void helloWorldOld() {\n" +
            "        System.out.println(\"HELLO WORLD OLD\");\n" +
            "    }\n" +
            "}\n" +
            "\n" +
            "public class OldWithoutResult {\n" +
            "    void helloWorld() {\n" +
            "        System.out.println(\"HELLO WORLD\");\n" +
            "    }\n" +
            "}\n";

        String fileNew = "public class Unchanged {\n" +
            "    void helloWorld() {\n" +
            "        System.out.println(\"HELLO WORLD\");\n" +
            "    }\n" +
            "}\n" +
            "\n" +
            "public class Changed {\n" +
            "    void helloWorldNew() {\n" +
            "        System.out.println(\"HELLO WORLD NEW\");\n" +
            "    }\n" +
            "}\n" +
            "\n" +
            "public class New {\n" +
            "    void helloWorldBorn() {\n" +
            "        System.out.println(\"HELLO WORLD BORN\");\n" +
            "    }\n" +
            "}\n" +
            "\n" +
            "public class OldWithoutResult {\n" +
            "    void helloWorld() {\n" +
            "        System.out.println(\"HELLO WORLD CHANGED\");\n" +
            "    }\n" +
            "}\n";

        // Construct default objects using the ModelFactory.
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        GitCommit previousCommit = ModelFactory.getGitCommit();
        gitCommitRepository.save(previousCommit);

        GitCommit newCommit = ModelFactory.getGitCommit();
        gitCommitRepository.save(newCommit);

        Branch branch = ModelFactory.getBranch(repository, previousCommit);
        branchRepository.save(branch);

        BranchFile branchFile = ModelFactory.getBranchFile(branch, previousCommit);
        branchFileRepository.save(branchFile);

        Metric metric = ModelFactory.getMetric();
        metricRepository.save(metric);

        MetricSettings settings = ModelFactory.getMetricSettings(repository, metric);
        metricSettingsRepository.save(settings);

        BranchClassObservation branchClassObservationUnchanged = new BranchClassObservation();
        branchClassObservationUnchanged.setBranchFile(branchFile);
        branchClassObservationUnchanged.setClassName("Unchanged");
        branchClassObservationUnchanged.setLineNumber(1);
        branchClassObservationUnchanged.setGitCommit(previousCommit);
        branchClassObservationRepository.save(branchClassObservationUnchanged);

        BranchClassObservation branchClassObservationChanged = new BranchClassObservation();
        branchClassObservationChanged.setBranchFile(branchFile);
        branchClassObservationChanged.setClassName("Changed");
        branchClassObservationChanged.setLineNumber(7);
        branchClassObservationChanged.setGitCommit(previousCommit);
        branchClassObservationRepository.save(branchClassObservationChanged);

        BranchClassObservation branchClassObservationOldWithoutResult = new BranchClassObservation();
        branchClassObservationOldWithoutResult.setBranchFile(branchFile);
        branchClassObservationOldWithoutResult.setClassName("OldWithoutResult");
        branchClassObservationOldWithoutResult.setLineNumber(13);
        branchClassObservationOldWithoutResult.setGitCommit(previousCommit);
        branchClassObservationRepository.save(branchClassObservationOldWithoutResult);

        BranchClassMetricResult branchClassMetricResultUnchanged = new BranchClassMetricResult();
        branchClassMetricResultUnchanged.setBranchClassObservation(branchClassObservationUnchanged);
        branchClassMetricResultUnchanged.setScore(10.0);
        branchClassMetricResultUnchanged.setMetric(metric);
        branchClassMetricResultUnchanged.setIsLowQuality(false);
        branchClassMetricResultRepository.save(branchClassMetricResultUnchanged);

        BranchClassMetricResult branchClassMetricResultChanged = new BranchClassMetricResult();
        branchClassMetricResultChanged.setBranchClassObservation(branchClassObservationChanged);
        branchClassMetricResultChanged.setScore(5.0);
        branchClassMetricResultChanged.setMetric(metric);
        branchClassMetricResultChanged.setIsLowQuality(false);
        branchClassMetricResultRepository.save(branchClassMetricResultChanged);

        classCodeChurnComputer.analyzeFileInCommit(fileOld, fileNew, branchFile, newCommit, metric,
                settings);

        // Test whether all new BranchClassObservation objects were created.
        assertEquals(7, branchClassObservationRepository.count());

        // Test whether all new BranchClassMetricResult objects were created.
        assertEquals(6, branchClassMetricResultRepository.count());

        // Retrieve added observation for the 'Unchanged' class.
        Optional<BranchClassObservation> optionalObservationUnchanged =
                branchClassObservationRepository.findFirstByBranchFileIdAndClassNameOrderByIdDesc(
                        branchFile.getId(),
                        "Unchanged"
                );

        assertTrue(optionalObservationUnchanged.isPresent());

        BranchClassObservation observationUnchanged = optionalObservationUnchanged.get();

        // Check whether line number was correctly identified.
        assertEquals(1, observationUnchanged.getLineNumber());

        // Retrieve added result for the 'Unchanged' class.
        Optional<BranchClassMetricResult> optionalResultUnchanged =
                branchClassMetricResultRepository
                    .findFirstByBranchClassObservationIdAndMetricIdOrderByIdDesc(
                        observationUnchanged.getId(),
                        metric.getId()
                    );

        assertTrue(optionalResultUnchanged.isPresent());

        BranchClassMetricResult resultUnchanged = optionalResultUnchanged.get();

        // Check whether the score was correctly set.
        assertEquals(10, resultUnchanged.getScore().intValue());


        // Retrieve added observation for the 'Changed' class.
        Optional<BranchClassObservation> optionalObservationChanged =
            branchClassObservationRepository.findFirstByBranchFileIdAndClassNameOrderByIdDesc(
                branchFile.getId(),
                "Changed"
            );

        assertTrue(optionalObservationChanged.isPresent());

        BranchClassObservation observationChanged = optionalObservationChanged.get();

        // Check whether line number was correctly identified.
        assertEquals(7, observationChanged.getLineNumber());

        // Retrieve added result for the 'Changed' class.
        Optional<BranchClassMetricResult> optionalResultChanged =
            branchClassMetricResultRepository
                .findFirstByBranchClassObservationIdAndMetricIdOrderByIdDesc(
                    observationChanged.getId(),
                    metric.getId()
                );

        assertTrue(optionalResultChanged.isPresent());

        BranchClassMetricResult resultChanged = optionalResultChanged.get();

        // Check whether the score was correctly set.
        assertEquals(6, resultChanged.getScore().intValue());


        // Retrieve added observation for the 'New' class.
        Optional<BranchClassObservation> optionalObservationNew =
            branchClassObservationRepository.findFirstByBranchFileIdAndClassNameOrderByIdDesc(
                branchFile.getId(),
                "New"
            );

        assertTrue(optionalObservationNew.isPresent());

        BranchClassObservation observationNew = optionalObservationNew.get();

        // Check whether line number was correctly identified.
        assertEquals(13, observationNew.getLineNumber());

        // Retrieve added result for the 'New' class.
        Optional<BranchClassMetricResult> optionalResultNew =
            branchClassMetricResultRepository
                .findFirstByBranchClassObservationIdAndMetricIdOrderByIdDesc(
                    observationNew.getId(),
                    metric.getId()
                );

        assertTrue(optionalResultNew.isPresent());

        BranchClassMetricResult resultNew = optionalResultNew.get();

        // Check whether the score was correctly set.
        assertEquals(1, resultNew.getScore().intValue());


        // Retrieve added observation for the 'OldWithoutResult' class.
        Optional<BranchClassObservation> optionalObservationOldWithoutResult =
            branchClassObservationRepository.findFirstByBranchFileIdAndClassNameOrderByIdDesc(
                branchFile.getId(),
                "OldWithoutResult"
            );

        assertTrue(optionalObservationOldWithoutResult.isPresent());

        BranchClassObservation observationOldWithoutResult = optionalObservationOldWithoutResult.get();

        // Check whether line number was correctly identified.
        assertEquals(19, observationOldWithoutResult.getLineNumber());

        // Retrieve added result for the 'OldWithoutResult' class.
        Optional<BranchClassMetricResult> optionalResultOldWithoutResult =
            branchClassMetricResultRepository
                .findFirstByBranchClassObservationIdAndMetricIdOrderByIdDesc(
                    observationOldWithoutResult.getId(),
                    metric.getId()
                );

        assertTrue(optionalResultOldWithoutResult.isPresent());

        BranchClassMetricResult resultOldWithoutresult = optionalResultOldWithoutResult.get();

        // Check whether the score was correctly set.
        assertEquals(1, resultOldWithoutresult.getScore().intValue());
    }
}
