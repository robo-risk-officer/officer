package com.roborisk.officer.modules.codechurn;

import com.github.javaparser.Position;
import com.github.javaparser.Range;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.roborisk.officer.modules.metrics.codechurn.FunctionVisitor;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class providing tests for the {@link FunctionVisitor} class.
 */
public class FunctionVisitorTest {

    /**
     * Local instance of the {@link FunctionVisitor} class.
     */
    FunctionVisitor functionVisitor = new FunctionVisitor();

    /**
     * Test for checking whether the correct exception is thrown if JavaParser cannot find the
     * beginning of the function.
     */
    @Test
    void testBeginEmpty() {
        MethodDeclaration methodDeclaration = new MethodDeclaration();

        Map<String, Triple<Long, Integer, String>> map = new HashMap<>();
        try {
            this.functionVisitor.visit(methodDeclaration, map);
            fail();
        } catch (IllegalStateException e) {
            assertEquals("metrics.churn.FunctionVisitor: Begin of method not found", e.getMessage());
        }
    }

    /**
     * Test for checking whether a function without a parent node is correctly handled.
     */
    @Test
    void testNoParentNode() {
        MethodDeclaration methodDeclaration = new MethodDeclaration();
        Range range = new Range(new Position(1, 0), new Position(10, 0));
        methodDeclaration.setRange(range);

        Map<String, Triple<Long, Integer, String>> map = new HashMap<>();
        this.functionVisitor.visit(methodDeclaration, map);

        assertEquals(map.values().iterator().next().getRight(), "Unidentifiable Class");
    }

    /**
     * Test for checking whether an anonymous function is handled correctly.
     */
    @Test
    void testAnonymousFunction() {
        MethodDeclaration methodDeclaration = new MethodDeclaration();
        Range range = new Range(new Position(1, 0), new Position(10, 0));
        methodDeclaration.setRange(range);

        Node notClassNode = new CompilationUnit();
        methodDeclaration.setParentNode(notClassNode);

        Map<String, Triple<Long, Integer, String>> map = new HashMap<>();
        this.functionVisitor.visit(methodDeclaration, map);

        assertTrue(map.isEmpty());
    }
}
