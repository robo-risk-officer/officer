package com.roborisk.officer.modules.codechurn;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.Branch;
import com.roborisk.officer.models.database.BranchFile;
import com.roborisk.officer.models.database.BranchFunctionObservation;
import com.roborisk.officer.models.database.GitCommit;
import com.roborisk.officer.models.database.MetricSettings;
import com.roborisk.officer.models.database.ModelFactory;
import com.roborisk.officer.models.database.Repository;
import com.roborisk.officer.models.database.metrics.BranchFunctionMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.BranchFileRepository;
import com.roborisk.officer.models.repositories.BranchFunctionMetricResultRepository;
import com.roborisk.officer.models.repositories.BranchFunctionObservationRepository;
import com.roborisk.officer.models.repositories.BranchRepository;
import com.roborisk.officer.models.repositories.GitCommitRepository;
import com.roborisk.officer.models.repositories.MetricRepository;
import com.roborisk.officer.models.repositories.MetricSettingsRepository;
import com.roborisk.officer.models.repositories.RepositoryRepository;
import com.roborisk.officer.modules.metrics.codechurn.FunctionCodeChurnComputer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Class for testing the {@link FunctionCodeChurnComputer} class.
 */
public class FunctionCodeChurnComputerTest extends OfficerTest {

    /**
     * Repository interface for {@link Repository} objects.
     */
    @Autowired
    RepositoryRepository repositoryRepository;

    /**
     * Repository interface for {@link Branch} objects.
     */
    @Autowired
    BranchRepository branchRepository;

    /**
     * Repository interface for {@link GitCommit} objects.
     */
    @Autowired
    GitCommitRepository gitCommitRepository;

    /**
     * Repository interface for {@link BranchFile} objects.
     */
    @Autowired
    BranchFileRepository branchFileRepository;

    /**
     * Repository interface for {@link Metric} objects.
     */
    @Autowired
    MetricRepository metricRepository;

    /**
     * Repository interface for {@link MetricSettings} objects.
     */
    @Autowired
    MetricSettingsRepository metricSettingsRepository;

    /**
     * Repository interface for {@link BranchFunctionObservation} objects.
     */
    @Autowired
    BranchFunctionObservationRepository branchFunctionObservationRepository;

    /**
     * Repository interface for {@link BranchFunctionMetricResult} objects.
     */
    @Autowired
    BranchFunctionMetricResultRepository branchFunctionMetricResultRepository;

    /**
     * {@link FunctionCodeChurnComputer} used for testing.
     */
    @Autowired
    FunctionCodeChurnComputer functionCodeChurnComputer;

    /**
     * Test for all functionality in the {@link FunctionCodeChurnComputer} class.
     *
     * @throws ParseException if database entries cannot be parsed.
     */
    @Test
    void analyzeFileInCommitTest() throws ParseException, com.github.javaparser.ParseException {
        // Construct String objects mocking the contents of files.
        String fileOld = "public class ContainingClass {\n" +
            "    void helloWorldUnchanged() {\n" +
            "        System.out.println(\"HELLO WORLD\");\n" +
            "    }\n" +
            "    \n" +
            "    void helloWorldChanged() {\n" +
            "        System.out.println(\"HELLO WORLD OLD\");\n" +
            "    }\n" +
            "    void helloWorldChangedWithoutOldResult() {\n" +
            "        System.out.println(\"HELLO WORLD OLD\");\n" +
            "    }\n" +
            "}";

        String fileNew = "public class ContainingClass {\n" +
            "    void helloWorldUnchanged() {\n" +
            "        System.out.println(\"HELLO WORLD\");\n" +
            "    }\n" +
            "    \n" +
            "    void helloWorldChanged() {\n" +
            "        System.out.println(\"HELLO WORLD NEW\");   \n" +
            "    }\n" +
            "    \n" +
            "    void helloWorldNew() {\n" +
            "        System.out.println(\"HELLO WORLD BORN\");\n" +
            "    }\n" +
            "    void helloWorldChangedWithoutOldResult() {\n" +
            "        System.out.println(\"HELLO WORLD NEW\");\n" +
            "    }\n" +
            "}";

        // Construct default objects using the ModelFactory.
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        GitCommit previousCommit = ModelFactory.getGitCommit();
        gitCommitRepository.save(previousCommit);

        GitCommit newCommit = ModelFactory.getGitCommit();
        gitCommitRepository.save(newCommit);

        Branch branch = ModelFactory.getBranch(repository, previousCommit);
        branchRepository.save(branch);

        BranchFile branchFile = ModelFactory.getBranchFile(branch, previousCommit);
        branchFileRepository.save(branchFile);

        Metric metric = ModelFactory.getMetric();
        metricRepository.save(metric);

        MetricSettings settings = ModelFactory.getMetricSettings(repository, metric);
        metricSettingsRepository.save(settings);

        BranchFunctionObservation branchFunctionObservationUnchanged = new BranchFunctionObservation();
        branchFunctionObservationUnchanged.setFunctionName(" void helloWorldUnchanged()");
        branchFunctionObservationUnchanged.setClassName("ContainingClass");
        branchFunctionObservationUnchanged.setLineNumber(2);
        branchFunctionObservationUnchanged.setGitCommit(previousCommit);
        branchFunctionObservationUnchanged.setBranchFile(branchFile);
        branchFunctionObservationRepository.save(branchFunctionObservationUnchanged);

        BranchFunctionObservation branchFunctionObservationChanged = new BranchFunctionObservation();
        branchFunctionObservationChanged.setFunctionName(" void helloWorldChanged()");
        branchFunctionObservationChanged.setClassName("ContainingClass");
        branchFunctionObservationChanged.setLineNumber(6);
        branchFunctionObservationChanged.setGitCommit(previousCommit);
        branchFunctionObservationChanged.setBranchFile(branchFile);
        branchFunctionObservationRepository.save(branchFunctionObservationChanged);

        BranchFunctionObservation branchFunctionObservationChangedWithoutOldResult = new BranchFunctionObservation();
        branchFunctionObservationChangedWithoutOldResult.setFunctionName(" void helloWorldChangedWithoutOldResult()");
        branchFunctionObservationChangedWithoutOldResult.setClassName("ContainingClass");
        branchFunctionObservationChangedWithoutOldResult.setLineNumber(6);
        branchFunctionObservationChangedWithoutOldResult.setGitCommit(previousCommit);
        branchFunctionObservationChangedWithoutOldResult.setBranchFile(branchFile);
        branchFunctionObservationRepository.save(branchFunctionObservationChangedWithoutOldResult);

        BranchFunctionMetricResult branchFunctionMetricResultUnchanged = new BranchFunctionMetricResult();
        branchFunctionMetricResultUnchanged.setBranchFunctionObservation(branchFunctionObservationUnchanged);
        branchFunctionMetricResultUnchanged.setScore(10.0);
        branchFunctionMetricResultUnchanged.setMetric(metric);
        branchFunctionMetricResultUnchanged.setIsLowQuality(false);
        branchFunctionMetricResultRepository.save(branchFunctionMetricResultUnchanged);

        BranchFunctionMetricResult branchFunctionMetricResultChanged = new BranchFunctionMetricResult();
        branchFunctionMetricResultChanged.setBranchFunctionObservation(branchFunctionObservationChanged);
        branchFunctionMetricResultChanged.setScore(5.0);
        branchFunctionMetricResultChanged.setMetric(metric);
        branchFunctionMetricResultChanged.setIsLowQuality(false);
        branchFunctionMetricResultRepository.save(branchFunctionMetricResultChanged);

        functionCodeChurnComputer.analyzeFileInCommit(fileOld, fileNew, branchFile, newCommit, metric,
                settings);

        // Test whether all new BranchFunctionObservation objects were created.
        assertEquals(7, branchFunctionObservationRepository.count());

        // Test whether all new BranchFunctionMetricResult objects were created.
        assertEquals(6, branchFunctionMetricResultRepository.count());

        // Retrieve added observation for the 'Unchanged' Function.
        Optional<BranchFunctionObservation> optionalObservationUnchanged =
                branchFunctionObservationRepository.findFirstByBranchFileIdAndFunctionNameOrderByIdDesc(
                        branchFile.getId(),
                        " void helloWorldUnchanged()"
                );

        assertTrue(optionalObservationUnchanged.isPresent());

        BranchFunctionObservation observationUnchanged = optionalObservationUnchanged.get();

        // Check whether line number was correctly identified.
        assertEquals(2, observationUnchanged.getLineNumber());

        // Retrieve added result for the 'Unchanged' Function.
        Optional<BranchFunctionMetricResult> optionalResultUnchanged =
                branchFunctionMetricResultRepository
                    .findFirstByBranchFunctionObservationIdAndMetricIdOrderByIdDesc(
                        observationUnchanged.getId(),
                        metric.getId()
                    );

        assertTrue(optionalResultUnchanged.isPresent());

        BranchFunctionMetricResult resultUnchanged = optionalResultUnchanged.get();

        // Check whether the score was correctly set.
        assertEquals(10, resultUnchanged.getScore().intValue());


        // Retrieve added observation for the 'Changed' Function.
        Optional<BranchFunctionObservation> optionalObservationChanged =
            branchFunctionObservationRepository.findFirstByBranchFileIdAndFunctionNameOrderByIdDesc(
                branchFile.getId(),
                " void helloWorldChanged()"
            );

        assertTrue(optionalObservationChanged.isPresent());

        BranchFunctionObservation observationChanged = optionalObservationChanged.get();

        // Check whether line number was correctly identified.
        assertEquals(6, observationChanged.getLineNumber());

        // Retrieve added result for the 'Changed' Function.
        Optional<BranchFunctionMetricResult> optionalResultChanged =
            branchFunctionMetricResultRepository
                .findFirstByBranchFunctionObservationIdAndMetricIdOrderByIdDesc(
                    observationChanged.getId(),
                    metric.getId()
                );

        assertTrue(optionalResultChanged.isPresent());

        BranchFunctionMetricResult resultChanged = optionalResultChanged.get();

        // Check whether the score was correctly set.
        assertEquals(6, resultChanged.getScore().intValue());


        // Retrieve added observation for the 'New' Function.
        Optional<BranchFunctionObservation> optionalObservationNew =
            branchFunctionObservationRepository.findFirstByBranchFileIdAndFunctionNameOrderByIdDesc(
                branchFile.getId(),
                " void helloWorldNew()"
            );

        assertTrue(optionalObservationNew.isPresent());

        BranchFunctionObservation observationNew = optionalObservationNew.get();

        // Check whether line number was correctly identified.
        assertEquals(10, observationNew.getLineNumber());

        // Retrieve added result for the 'New' Function.
        Optional<BranchFunctionMetricResult> optionalResultNew =
            branchFunctionMetricResultRepository
                .findFirstByBranchFunctionObservationIdAndMetricIdOrderByIdDesc(
                    observationNew.getId(),
                    metric.getId()
                );

        assertTrue(optionalResultNew.isPresent());

        BranchFunctionMetricResult resultNew = optionalResultNew.get();

        // Check whether the score was correctly set.
        assertEquals(1, resultNew.getScore().intValue());


        // Retrieve added observation for the 'ChangedWithoutOldResult' Function.
        Optional<BranchFunctionObservation> optionalObservationChangedWithoutOldResult =
            branchFunctionObservationRepository.findFirstByBranchFileIdAndFunctionNameOrderByIdDesc(
                branchFile.getId(),
                " void helloWorldChangedWithoutOldResult()"
            );

        assertTrue(optionalObservationChangedWithoutOldResult.isPresent());

        BranchFunctionObservation observationChangedWithoutOldResult = optionalObservationChangedWithoutOldResult.get();

        // Check whether line number was correctly identified.
        assertEquals(13, observationChangedWithoutOldResult.getLineNumber());

        // Retrieve added result for the 'New' Function.
        Optional<BranchFunctionMetricResult> optionalResultChangedWithoutOldResult =
            branchFunctionMetricResultRepository
                .findFirstByBranchFunctionObservationIdAndMetricIdOrderByIdDesc(
                    observationChangedWithoutOldResult.getId(),
                    metric.getId()
                );

        assertTrue(optionalResultChangedWithoutOldResult.isPresent());

        BranchFunctionMetricResult resultChangedWithoutOldResult = optionalResultChangedWithoutOldResult.get();

        // Check whether the score was correctly set.
        assertEquals(1, resultChangedWithoutOldResult.getScore().intValue());
    }
}
