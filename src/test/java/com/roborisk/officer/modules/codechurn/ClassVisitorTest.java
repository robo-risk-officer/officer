package com.roborisk.officer.modules.codechurn;

import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.roborisk.officer.modules.metrics.codechurn.ClassVisitor;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Class providing tests for the {@link ClassVisitor} class.
 */
public class ClassVisitorTest {

    /**
     * Local instance of the {@link ClassVisitor} class.
     */
    ClassVisitor classVisitor = new ClassVisitor();

    /**
     * Test for checking whether the correct exception is thrown if JavaParser cannot find the
     * beginning of the class.
     */
    @Test
    void testBeginEmpty() {
        ClassOrInterfaceDeclaration classOrInterfaceDeclaration = new ClassOrInterfaceDeclaration();

        Map<String, Pair<Long, Integer>> map = new HashMap<>();
        try {
            this.classVisitor.visit(classOrInterfaceDeclaration, map);
            fail();
        } catch (IllegalStateException e) {
            assertEquals("metrics.churn.ClassVisitor: Begin of class not found", e.getMessage());
        }
    }
}
