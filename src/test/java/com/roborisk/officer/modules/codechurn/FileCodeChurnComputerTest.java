package com.roborisk.officer.modules.codechurn;

import com.roborisk.officer.OfficerTest;
import com.roborisk.officer.models.database.*;
import com.roborisk.officer.models.database.metrics.BranchFileMetricResult;
import com.roborisk.officer.models.database.metrics.Metric;
import com.roborisk.officer.models.repositories.*;
import com.roborisk.officer.models.web.CommitWebhookData;
import com.roborisk.officer.models.web.GitCommitWeb;
import com.roborisk.officer.modules.metrics.codechurn.FileCodeChurnComputer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Testing class for the {@link FileCodeChurnComputer} class.
 */
public class FileCodeChurnComputerTest extends OfficerTest {

    @Autowired
    private FileCodeChurnComputer fileCodeChurnComputer;

    @Autowired
    private RepositoryRepository repositoryRepository;

    @Autowired
    private GitCommitRepository gitCommitRepository;

    @Autowired
    BranchRepository branchRepository;

    @Autowired
    BranchFileRepository branchFileRepository;

    @Autowired
    MetricRepository metricRepository;

    @Autowired
    MetricSettingsRepository metricSettingsRepository;

    @Autowired
    BranchFileMetricResultRepository branchFileMetricResultRepository;

    /**
     * Test checking that the correct error is thrown by handleSingleAddition() if a metric is
     * supplied that does not exist in the database
     *
     * @throws ParseException
     */
    @Test
    void handleSingleAdditionMetricNotFound() throws ParseException {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        GitCommit commit = ModelFactory.getGitCommit();
        gitCommitRepository.save(commit);

        Branch branch = ModelFactory.getBranch(repository, commit);
        Metric metric = ModelFactory.getMetric();
        MetricSettings metricSettings = ModelFactory.getMetricSettings(repository, metric);


        GitCommitWeb gitCommitWeb = new GitCommitWeb();
        gitCommitWeb.setAdded(List.of("newFile.java"));
        gitCommitWeb.setTimestamp("2015-04-08T21:00:25-07:00");
        gitCommitWeb.setHash(commit.getHash());

        CommitWebhookData commitWebhookData = new CommitWebhookData();
        commitWebhookData.setCommits(List.of(gitCommitWeb));


        metric.setName("NON_EXISTENT_METRIC");

        try {
            fileCodeChurnComputer.handle(metricSettings, metric, branch, commitWebhookData);
            fail();
        } catch (IllegalStateException e) {
            assertEquals("FileCodeChurnComputer.handleSingleAddition: Metric not found", e.getMessage());
        }
    }

    /**
     * Test checking that the correct error is thrown by handleSingleAddition() if the branch file
     * does not exist in the database.
     *
     * @throws ParseException
     */
    @Test
    void handleSingleAdditionFileNotFound() throws ParseException {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        GitCommit commit = ModelFactory.getGitCommit();
        gitCommitRepository.save(commit);

        Branch branch = ModelFactory.getBranch(repository, commit);
        Metric metric = ModelFactory.getMetric();
        metricRepository.save(metric);
        MetricSettings metricSettings = ModelFactory.getMetricSettings(repository, metric);


        GitCommitWeb gitCommitWeb = new GitCommitWeb();
        gitCommitWeb.setAdded(List.of("newFile.java"));
        gitCommitWeb.setTimestamp("2015-04-08T21:00:25-07:00");
        gitCommitWeb.setHash(commit.getHash());

        CommitWebhookData commitWebhookData = new CommitWebhookData();
        commitWebhookData.setCommits(List.of(gitCommitWeb));

        try {
            fileCodeChurnComputer.handle(metricSettings, metric, branch, commitWebhookData);
            fail();
        } catch (IllegalStateException e) {
            assertEquals("FileCodeChurnComputer.handleSingleAddition: File not found", e.getMessage());
        }
    }

    /**
     * Test checking that the correct error is thrown by handleDelete() if the branch file
     * does not exist in the database.
     *
     * @throws ParseException
     */
    @Test
    void handleDeleteFileNotFound() throws ParseException {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        GitCommit commit = ModelFactory.getGitCommit();
        gitCommitRepository.save(commit);

        Branch branch = ModelFactory.getBranch(repository, commit);
        Metric metric = ModelFactory.getMetric();
        metricRepository.save(metric);
        MetricSettings metricSettings = ModelFactory.getMetricSettings(repository, metric);


        GitCommitWeb gitCommitWeb = new GitCommitWeb();
        gitCommitWeb.setAdded(new ArrayList<>());
        gitCommitWeb.setRemoved(List.of("deletedFile.java"));
        gitCommitWeb.setTimestamp("2015-04-08T21:00:25-07:00");
        gitCommitWeb.setHash(commit.getHash());

        CommitWebhookData commitWebhookData = new CommitWebhookData();
        commitWebhookData.setCommits(List.of(gitCommitWeb));

        try {
            fileCodeChurnComputer.handle(metricSettings, metric, branch, commitWebhookData);
            fail();
        } catch (IllegalStateException e) {
            assertEquals("FileCodeChurnComputer.handleDelete: File not found", e.getMessage());
        }
    }

    /**
     * Test checking that a branch file is correctly ignored if it is of an unsupported file type.
     *
     * @throws ParseException
     */
    @Test
    void handleDeleteNotSupportedFileType() throws ParseException {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        GitCommit commit = ModelFactory.getGitCommit();
        gitCommitRepository.save(commit);

        Branch branch = ModelFactory.getBranch(repository, commit);
        Metric metric = ModelFactory.getMetric();
        metricRepository.save(metric);
        MetricSettings metricSettings = ModelFactory.getMetricSettings(repository, metric);


        GitCommitWeb gitCommitWeb = new GitCommitWeb();
        gitCommitWeb.setAdded(new ArrayList<>());
        gitCommitWeb.setModified(new ArrayList<>());
        gitCommitWeb.setRemoved(List.of("deletedFile.unsupported"));
        gitCommitWeb.setTimestamp("2015-04-08T21:00:25-07:00");
        gitCommitWeb.setHash(commit.getHash());

        CommitWebhookData commitWebhookData = new CommitWebhookData();
        commitWebhookData.setCommits(List.of(gitCommitWeb));

        fileCodeChurnComputer.handle(metricSettings, metric, branch, commitWebhookData);

        assertEquals(0, branchFileMetricResultRepository.count());
    }

    /**
     * Test checking that no BranchFileMetricResult is created for a file of an unsupported
     * filetype.
     *
     * @throws ParseException
     */
    @Test
    void handleSingleModificationNotSupportedFileType() throws ParseException {
        Repository repository = ModelFactory.getRepository();
        repositoryRepository.save(repository);

        GitCommit commit = ModelFactory.getGitCommit();
        gitCommitRepository.save(commit);

        Branch branch = ModelFactory.getBranch(repository, commit);
        branchRepository.save(branch);

        Metric metric = ModelFactory.getMetric();
        metricRepository.save(metric);

        BranchFile branchFile = ModelFactory.getBranchFile(branch, commit);
        branchFile.setFileName("hello.notsupportedtype");
        branchFileRepository.save(branchFile);

        MetricSettings metricSettings = ModelFactory.getMetricSettings(repository, metric);

        GitCommitWeb gitCommitWeb = new GitCommitWeb();
        gitCommitWeb.setAdded(new ArrayList<>());
        gitCommitWeb.setRemoved(new ArrayList<>());
        gitCommitWeb.setModified(List.of(branchFile.getFilePath()));
        gitCommitWeb.setTimestamp("2015-04-08T21:00:25-07:00");
        gitCommitWeb.setHash(commit.getHash());

        CommitWebhookData commitWebhookData = new CommitWebhookData();
        commitWebhookData.setCommits(List.of(gitCommitWeb));

        fileCodeChurnComputer.handle(metricSettings, metric, branch, commitWebhookData);

        assertEquals(0, branchFileMetricResultRepository.count());
    }
}
