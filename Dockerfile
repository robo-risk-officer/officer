# Source: https://stackoverflow.com/questions/27767264/how-to-dockerize-maven-project-and-how-many-ways-to-accomplish-it
# Source: https://docs.docker.com/develop/develop-images/multistage-build/

#
# Build Stage
#
# Use build image (that is also used for the rest of the CI pipeline)
FROM x0rz3q/maven:latest AS build
# Copy the source files into the build image while being careful not to copy in any secrets (from configuration files)
COPY src/main/java /home/rro/src/main/java
# Copy the example configuration file to the application.properties file to enable environment variable configuration
COPY src/main/resources/application-example.properties /home/rro/src/main/resources/application.properties
# Copy the migrations
COPY src/main/resources/db /home/rro/src/main/resources/db
# Copy the maven file
COPY pom.xml /home/rro
# And the checkstyle files, because otherwise the maven package will fail
COPY roborisk_checks.xml /home/rro
# Finally execute the maven package command to generate a deployable jar
# Tests are skipped, because a lot of environment variables need to be set for these to work
# e.g. database credentials, GitLab urls etc.
RUN mvn -f /home/rro/pom.xml clean package -Dmaven.test.skip=true

#
# Package stage
#
# Use the openjdk jdk image as a base for the RRO docker image
FROM openjdk:11-jdk-slim
# Copy the packaged runnable jar from the build stage and put it in the rro docker image
COPY --from=build /home/rro/target/officer-0.0.1-SNAPSHOT.jar /usr/local/lib/rro.jar
# Makes sure the maven install works
RUN mkdir -p /usr/share/man/man1
# Install maven which is used for SonarQube analysis
RUN apt update && apt install -y maven openjdk-11-jdk wget unzip
# Download and install sonar-scanner
RUN wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.3.0.2102-linux.zip -O sonar-scanner.zip \
    && unzip sonar-scanner.zip -d /usr/local/sonar_scanner \
    && mv /usr/local/sonar_scanner/sonar-scanner-4.3.0.2102-linux/* /usr/local/sonar_scanner \
    && rm -rf /usr/local/sonar_scanner/sonar-scanner-4.3.0.2102-linux \
    && ln -s /usr/local/sonar_scanner/bin/sonar-scanner /usr/bin/
# Copy the startup script for the container
ADD ./dockerEntryPoint.sh /root
RUN chmod +x /root/dockerEntryPoint.sh
# Create the file where the api token will be in, so it can overwritten by the docker-compose file
RUN mkdir /root/.rro
RUN touch /root/.rro/gitlab_token /root/.rro/sonar_token /root/.rro/id_rsa /root/.rro/id_rsa.pub
# The backend runs on port 8090 so expose that port
EXPOSE 8090
# Run the rro backend application on container startup
ENTRYPOINT ["/root/dockerEntryPoint.sh"]
