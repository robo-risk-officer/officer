# Robo Risk Officer

The goal of the Robo Risk Officer is to develop a GitLab integrated merge request risk assessment robot which communicates through the GitLab merge request interface. The tool helps developers by placing merge request reviews close-to-the-code, assessing the quality of a merge request using code metrics, visualising the evolution of a merge request over time, taking flagged false positives into consideration and communicating in an almost human-like manner. Developers can request code reviews via the GitLab merge request comments section and users with maintainer privileges (or higher) can configure the robot using the external Control Panel.


# Development
The Robo Risk Officer was developed with UNIX Systems in mind. However, it is able to develop on windows. Moreover, it cannot be guaranteed that everything will run smoothly and the steps that need to be taken can deviate from the development setup guide.

To run the Robo Risk Officer Java 11 & Maven is required to be installed.

```bash
git clone git@gitlab.com:robo-risk-officer/officer.git # checkout
cd officer # change directory to the project
mvn install # install dependencies
mvn package -DskipTests # make the jar
```

To be able to run the tests a few files need to be created that are required to start the Spring Boot Instance. These files are checked on boot, if the files do not exist or the permissions are incorrect the system will simply not start. The files are as follows:

- SSH Private & Public Key
- Sonar Token
- GitLab Token

For development the Sonar Token and the GitLab token can be simply a random string. To setup the files execute the following steps:

```bash
mkdir -p ~/.rro # create the directory for the files
ssh-keygen -t RSA -m PEM -b 4096 -f ~/.rro/id_rsa -N ”” # JGit expects an RSA key in PEM format
echo "Test Key" >> ~/.rro/{sonar_token,gitlab_token} # create the dummy keys
chmod 400 ~/.ssh/* # set the correct permissions
```

After these steps are successfully executed the tests can be run simply run `mvn test` to execute said tests.

# Contributing
The project was developed with open-source in mind, this implies that the project follow the general open-source development cycle. To contribute simply fork the project and make a merge request, these will be reviewed by the right maintainer and constructive feedback will be given where needed.

When creating a merge request on the Robo Risk Officer make sure to read the [JavaDoc Standard](https://gitlab.com/robo-risk-officer/officer/-/wikis/JavaDoc-Standard) in the wiki. This will help both the maintainers and the developer in their review and speeds up the entire merge request. 

## Database
All database adjustments are done using the Flyway tooling, for more information see the [documentation](https://flywaydb.org/documentation/migrations) of flyway. For examples see the older migrations in `src/main/resources/db/migration`.

## Debugging
The easiest way to debug is either by using a Test Driven Development approach or by sending the web hooks via CURL or PostMan. The system relies heavily on the GitLab web hooks as this is the main integration. For example tests see `src/test/java/com/roborisk/officer/` make sure to extend from the `OfficerTest` class, this class handles the database setup and the start-up of the tomcat server.

## New to the project?
If you're new to the project feel free to check out the issues tagged as `first-good-issue`, these issues are small and will introduce you to the project slowly.

# License
The project is licensed under the MIT License, for more information see the [LICENSE](https://gitlab.com/robo-risk-officer/officer/-/blob/master/LICENSE) file.