#!/usr/bin/env python3

import argparse
import csv
import sys
import re

# Name of metric -> threshold such that x <= threshold is still ok
settings = {
        'AvgCyclomatic': { 'comparison': 'smaller', 'threshold': 10, 'filter': 'Package' } ,
        'RatioCommentToCode': { 'comparison': 'bigger', 'threshold': 0.15, 'filter': 'File'},
        'MaxCyclomatic': { 'comparison': 'smaller', 'threshold': 20, 'filter': '.*'}
}


parser = argparse.ArgumentParser(description='Parse understand CSV output and interpret it')

parser.add_argument('--file', dest='filepath', help='path to the understand csv file', required=True)

args = parser.parse_args()

filepath = args.filepath

exit_code = 0

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def measure_valid(validity_info, measure):
    if (validity_info['comparison'] == 'bigger'):
        return measure > validity_info['threshold']
    elif (validity_info['comparison'] == 'smaller'):
        return measure < validity_info['threshold']

def process_row(row):
    global exit_code

    for metric, validity_info in settings.items():
        regex = re.compile(validity_info['filter'])

        if not regex.match(row['Kind']):
            continue

        if metric not in row:
            print(f"No {metric} found in the csv file. See if it is enabled during the understand analysis.")
            exit_code = 1
        else:
            if not isfloat(row[metric]):
                continue

            measured = float(row[metric])

            if not measure_valid(validity_info, measured):
                exit_code = 1
                print(f"Measure {metric} of instance {row['Kind']} {row['Name']} is not satisfied. Evaluated to {measured}. Should be {validity_info['comparison']} than {validity_info['threshold']}")

with open(filepath, newline='') as f:
    reader = csv.DictReader(f)
    for row in reader:
        process_row(row)

if (exit_code == 1):
    print("Code does not pass all thresholds as defined. See above for more information.")

sys.exit(exit_code)

