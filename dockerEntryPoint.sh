#!/bin/bash
# Set owner and permissions of the key-file
chown root:root /root/.rro/gitlab_token
chmod 400 /root/.rro/gitlab_token
chown root:root /root/.rro/sonar_token
chmod 400 /root/.rro/sonar_token
chown root:root /root/.rro/id_rsa
chmod 400 /root/.rro/id_rsa
chown root:root /root/.rro/id_rsa.pub
chmod 400 /root/.rro/id_rsa.pub
# Run the backend application
java -jar /usr/local/lib/rro.jar
